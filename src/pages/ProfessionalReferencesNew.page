<apex:page controller="TRHC.ProfessionalReferencesController" docType="html-5.0"
        showHeader="false" sideBar="false" standardStylesheets="true"
        cache="false" language="{!selectLanguage}">
        <!--
<!--
How to use date picker extend
lowerLimit    : Number by which you need to make year start. Accept 100 max
higherLimit   : Number by which you need to make year end. Accept 100 max
startYear     : Year by which you need to make start year.If given lowerLimit/higherLimit ignored. Must be less than endYear. Difference between startYear/endYear must not exceed 300.
endYear       : Year by which you need to make end year.If given lowerLimit/higherLimit ignored. Must be greater than startYear. Difference between startYear/endYear must not exceed 300.
-->
<c:DatePickerExtend lowerLimit="50" higherLimit="15"/>


<script>
    if ('{!nextPageUrl}'.trim()) {
        top.location = '{!nextPageUrl}';
    }
    
    // jQuery workarounds to make it work.
    var j$ = jQuery.noConflict();
    function fixId(myid) {
        return '#' + myid.replace(new RegExp("(:|\\.)", "g"), '\\\$1');
    } 
</script> 

<style>
    <apex:outputText value="{!cmsCssCustom}" />
    
</style>

<div class="container-fluid">
<!-- <ul class="nav nav-tabs nav-justified">
                    <li><a href="#" onclick="doRedirect(top.location, { page:'PersonalInformation' }); return false;">Personal Information</a></li>
                    <li><a href="#" onclick="doRedirect(top.location, { page:'Certifications' }); return false;"> Certification</a></li>
                    <li><a href="#" onclick="doRedirect(top.location, { page:'EducationHistory' }); return false;">Education History</a></li>
                    <li><a href="#" onclick="doRedirect(top.location, { page:'EmploymentHistory' }); return false;">Employment History</a></li>
                    <li class="active"><a href="#">Professional Ref</a></li>
                     <li><a href="#" onclick="doRedirect(top.location, { page:'ESignature' }); return false;">E-Signature</a></li>
    </ul> -->
    <apex:form styleClass="form-horizontal" rendered="{!ISNULL(nextPageUrl)}">
        <div class="row">
            <apex:pageMessages escape="false" />
            <script>
                j$('.infoM3').addClass('alert alert-success');
                j$('.errorM3').addClass('alert alert-danger');
            </script>
            
            <div id="errors-alert" class="alert alert-danger" style="display:none">
                <h4>{!$Label.TRHC__Required_fields}:</h4>
                <ul>
                    
                </ul>
            </div>
        </div>
         
        <h2>{!$Label.TRHC__Professional_References}</h2>
        <h5>(*<span class="required-color">{!$Label.TRHC__Required_fields}</span>)</h5>
        
        <p>Please add 2 or 3 professional clinical supervisors with whom you have worked with in the past 12 months.<br/>
      </p>
        <br/>
        
        <apex:variable var="index" value="0" />
        <apex:repeat value="{!formFieldSettings}" var="formFieldSetting">
            <div class="form-group col-sm-6">
                <label class="col-sm-6 control-label">
                    <apex:outputPanel styleClass="required" rendered="{!formFieldSetting.TRHC__Is_Mandatory__c}">*</apex:outputPanel>
                    {!$ObjectType.TRHC__References__c.fields[formFieldSetting.Name].Label}
                </label>
                <div class="col-sm-6">
                    <apex:outputPanel rendered="{!$ObjectType.TRHC__References__c.fields[formFieldSetting.Name].Type == 'boolean'}">
                        <apex:selectRadio styleClass="radioalign-PRS" value="{!reference[formFieldSetting.Name]}" >
                            <apex:selectOptions value="{!YesNo}"/>
                        </apex:selectRadio>
                    </apex:outputPanel>
                    <apex:outputPanel rendered="{!$ObjectType.TRHC__References__c.fields[formFieldSetting.Name].Type != 'boolean'}">
                        <apex:inputField value="{!reference[formFieldSetting.Name]}" styleClass="form-control" />
                    </apex:outputPanel>
                </div>
            </div>
            
            <apex:outputPanel layout="block" rendered="{!MOD(VALUE(index), 2) != 0}" styleClass="clearfix" />
            
            <apex:variable var="index" value="{!VALUE(index) + 1}"/>
        </apex:repeat>
        
        <div class="row">
            <div class="col-md-12 text-right">
                <apex:commandButton onclick="window.scrollTo(0, 0);parent.scrollTo(0, 0);"
                        action="{!save}"
                        value="{!IF(isUpdate, $Label.trhc__update_record, 'Save and Add Record')}"
                        styleClass="btn btn-primary"/>
                 <!--<apex:commandButton onclick="window.scrollTo(0, 0);parent.scrollTo(0, 0);"
                        action="{!saveAndNext}"
                        value="{!$Label.trhc__save_and_next}"
                        styleClass="btn btn-primary"/> -->
            </div>
        </div>
        
        <div class="row text-center" style="display:none">
            Adding {!savedRecords.size + 1} of {!max} required references
        </div>
        
        <div class="row">
            <h3>{!$Label.trhc__saved_records}</h3>
            
            <div class="table-responsive">
                <table class="table table-bordered">
                <thead>
                <tr>
                    <apex:repeat value="{!tableFieldSettings}" var="tableFieldSetting">
                    <th class="header">
                        {!$ObjectType.TRHC__References__c.fields[tableFieldSetting.Name].Label}
                    </th>
                    </apex:repeat>
                    <th align="center" valign="top" class="header">
                    <apex:outputText value="{!$Label.trhc__action}"/>
                    </th>
                </tr>
                </thead>
                <tbody>
                <apex:repeat value="{!savedRecords}" var="savedRecord">
                <tr>
                    <apex:repeat value="{!tableFieldSettings}" var="tableFieldSetting">
                    <td data-title="{!$ObjectType.TRHC__References__c.fields[tableFieldSetting.Name].Label}">
                        {!savedRecord[tableFieldSetting.Name]}
                    </td>
                    </apex:repeat>
                    <td data-title="{!$Label.trhc__action}">
                        <apex:commandLink value="{!$Label.trhc__update}" action="{!upd}" immediate="true">
                            <apex:param name="savedRecordId" value="{!savedRecord.Id}"/>
                        </apex:commandLink>
                        /&nbsp;
                        <apex:commandLink value="{!$Label.trhc__remove}" action="{!remove}" 
                                onclick="return confirm('Are you sure you want to proceed?');" immediate="true">
                            <apex:param name="savedRecordId" value="{!savedRecord.Id}"/>
                        </apex:commandLink>
                    </td>
                </tr>
                </apex:repeat>
                </tbody>
                </table>
            </div>
        </div>
        
    </apex:form>
</div>

<script>
    parent.autoResizeIframe(document);
</script>

<apex:includeScript value="{!URLFOR($Resource.TRHC__Jquerymin)}" />

<apex:includeScript value="{!URLFOR($Resource.TRHC__doRedirect)}" />
<apex:stylesheet value="{!URLFOR($Resource.TRHC__Bootstrap, 'bootstrap-3.3.6-dist/css/bootstrap.min.css')}" />

</apex:page>