/*
*@Author    :chaitanya.b@targetrecruit.net
*@desc  :this trigger is for sending mail to recruiter who spoken with candidate before registration on portal
*/
trigger RecruitersMailNotificationTrigger on Contact(after Update) {
    //reading the custom setting
    Boolean isRun = false;
    if(TRHC__Config_Settings__c.getInstance('Default') != null){
        isRun = TRHC__Config_Settings__c.getInstance('Default').Enable_RecruitersMailNotificationTrigger__c;
    }
    //checking trigger is enabled or disabled 
    if (isRun) {
    
        if (Trigger.isAfter) {
            if (Trigger.isUpdate) {
                //helper class for single email but bulk messages
                HelperRecruitersMailNotificationTrigger.sendEmail(trigger.new);
            }
        }
    }
}