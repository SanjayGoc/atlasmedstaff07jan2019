trigger HousingInfoForCandi on TRHC__Job__c (before insert, after update) {
	HousingInfoForCandiHandler housing = new HousingInfoForCandiHandler();
    housing.AddHousingInformation(Trigger.New);
}