trigger trackEventActivityonContact on Event (after insert, after update) {
    
    Boolean isDisabled = false;

    TRHC__Config_Settings__c config = TRHC__Config_Settings__c.getOrgDefaults();
        if (config != null)
        isDisabled = config.DisabletrackEventActivityonContact__c;
    
        if(isDisabled ) 
        return;
    
    Set<ID> whoIdSet = new Set<ID>(); 
    for(Event e: trigger.new){
        if(e.whoId != null){
            whoIdSet.add(e.WhoId); 
        }
    }
    //retreive contacts
    Map<ID, Contact> contactMap = new Map<ID, Contact>([SELECT ActivityNotes__c, LastActivityDate__c  
                                                        FROM Contact WHERE Id IN :whoIdSet]); 
                                                        
    //update meeting date value 
    List<Contact> updContactList = new List<Contact>(); 
    for(Event e: trigger.new){
        if(e.whoId != null && contactMap.containsKey(e.whoId)){
            Contact c1 = contactMap.get(e.whoId); 
            //create date from datetime 
            //Date d1 = Date.newInstance(e.CreatedDate.year(), e.CreatedDate.month(), e.CreatedDate.day());  
            //System.debug('Date :::::'+d1);
            c1.LastActivityDate__c = e.LastModifiedDate;
            c1.ActivityNotes__c = e.Description; 
            updContactList.add(c1); 
        }
    }
    if(updContactList.size()>0){
        update updContactList; 
    }
    
}