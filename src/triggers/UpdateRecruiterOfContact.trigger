trigger UpdateRecruiterOfContact on Contact (before update) {
    //Reading the custom setting(boolean value)
    Boolean isRunOfRecruiterUpdate = false;
    if(TRHC__Config_Settings__c.getInstance('Default') != null){
        isRunOfRecruiterUpdate = TRHC__Config_Settings__c.getInstance('Default').Enable_Update_Recruiter__c;
    }
    if(isRunOfRecruiterUpdate){
        if(Trigger.isBefore && Trigger.isUpdate)
        for(Contact con:Trigger.new){
            if(con.Recruiters__c != null){
                //query the user based on name
                List < User > recContact = [SELECT id, Email 
                                            FROM User 
                                            WHERE Name = : con.Recruiters__c];
                if(recContact.size()==1){
                    //assigning the value
                    con.TRHC__Recruiter__c = recContact[0].id;
                }
            }
            
        }  
    }
}