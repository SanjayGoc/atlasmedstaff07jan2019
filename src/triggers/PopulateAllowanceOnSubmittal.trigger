trigger PopulateAllowanceOnSubmittal on TRHC__Job_Applicant__c (before insert,before update) {
	Boolean run = TRHC__config_Settings__c.getInstance('Default').Enable_TriggerUpdatePlacementCountOnJOb__c;
    if(run){
    	if(trigger.isInsert || trigger.isUpdate){
        PopulateAllowanceOnSubmittalHandler.populateAllance(Trigger.new);
    	}
    }
}