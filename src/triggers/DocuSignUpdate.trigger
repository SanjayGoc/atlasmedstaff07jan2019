trigger DocuSignUpdate on dsfs__DocuSign_Status__c (after Insert, after update) {
    DocuSignUpdateHandler.isAfterUpdate = false;
    if(trigger.isAfter && Trigger.isInsert) {
        DocuSignUpdateHandler.updateContact(Trigger.New, Trigger.NewMap);
    }
    if(trigger.isAfter && Trigger.isUpdate && !DocuSignUpdateHandler.isAfterUpdate) {
        DocuSignUpdateHandler.sendEmail(Trigger.New, Trigger.NewMap);
    }
}