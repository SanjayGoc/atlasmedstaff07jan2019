trigger JobAlertDelimiterModifier on TRHC__Job_Search_Agent__c (before insert,before update) {
    TRHC__Config_Settings__c configSet = TRHC__Config_Settings__c.getInstance('Default');
    Boolean flagtToTriiger = configSet.Enable_JobAlertDelimiterModifier__c;
    
    if (Userinfo.getName() !='Candidate Portal Site Guest User' && flagtToTriiger){
        system.debug('@@@'+Userinfo.getName());
        for(TRHC__Job_Search_Agent__c jobAgent : trigger.new){
            
            String str = jobAgent.TRHC__Search_Criteria__c.replace(';',',');
            if(str.contains('Specialty')){
                integer sta = str.indexOf('Specialty')+11;
                if(str.lastIndexOf(':') == sta-1){
                    String substr = str.substring(sta ,str.length()-2);
                    str = str.replace(substr,substr.replace(',', ';') );
                }
                else{
                    integer lst = str.indexof(':',sta);
                    system.debug(str.indexOf('Specialty')+10);
                    system.debug(lst+str.subString(sta,lst));
                    String replaceble = str.subString(sta,lst);
                    integer lastReplaceble = replaceble.lastIndexOf(',');
                    String replacebleNew = str.subString(sta,sta+lastReplaceble);
                    system.debug('nnnn@@'+replacebleNew);
                    str = str.replace(replacebleNew , replacebleNew.replace(',',';'));
                }
                
            }
            jobAgent.TRHC__Search_Criteria__c = str;
        }
    }
}