/**
* @author
* @date
* @description : Trigger to add expanses to the scratch sheet based on state of the scratch sheet.
*/
trigger Trigger_Scratchsheet_AutoAddExpense on TRHC__Scratch_Sheet__c (after insert) {
    
    /**
* @author Avankia103
* @date
* @description : Reading custom setting value to check the enable status
*/
    TRHC__Config_Settings__c configSettings = TRHC__Config_Settings__c.getValues('Default');
    Boolean isEnabled = configSettings == null ? True : (Boolean) configSettings.Trigger_Enable_ScratchSheetAutoAddExp__c;
    List<String> exptIdsList = configSettings == null ? new List<Id>() :configSettings.ExceptConfigIds__c.split(';');
    List<String> exptAccountList = configSettings == null ? new List<String>() :configSettings.Except_Hospitals__c.split(';');
    if (trigger.isInsert && isEnabled) {
        System.debug('exptIdsList@@'+exptIdsList);
        String configFields = ObjectFieldsHelper.getAllFields('TRHC__Config__c').get('TRHC__Config__c');
        
        String expanseFields = ObjectFieldsHelper.getAllFields('TRHC__Expense__c').get('TRHC__Expense__c');
        
        Set<String> states = new Set<String>();
        
        Set<String> scratchSheetIds = new Set<String>();
        
        for(TRHC__Scratch_Sheet__c scratchSheet : trigger.new) {
            system.debug('scratchSheet.State_pic__c@@@'+scratchSheet.State_pic__c);
            if(scratchSheet.State_pic__c!= null) {
                states.add('\''+scratchSheet.State_pic__c+'\'');
            }
            System.debug('Trigger ScratchsheetTrigger scratchSheet.Id :' + scratchSheet.Id);
            scratchSheetIds.add(scratchSheet.Id);
        }
        
        if(states.size() == 0){
            return;
        }
        Map<String, Set<TRHC__Config__c>> configMap = new Map<String, Set<TRHC__Config__c>>();
        
        List<String> statesList = new List<String> (states);
        String query = 'SELECT ' + configFields+ ' FROM TRHC__Config__c WHERE TRHC__State__c IN ' + statesList ;
        
        String configQuery = 'SELECT ID, TRHC__Scratch_Sheet__c, TRHC__Expense_Name__c, TRHC__Config__c FROM TRHC__Expense__c WHERE TRHC__Scratch_Sheet__c IN :scratchSheetIds';
        
        if(configSettings != null) {
            String criteria = configSettings.AutoAddExpenseCriteria__c;
            System.debug('Trigger ScratchsheetTrigger Value of AutoAddExpenseCriteria__c: '+ criteria);
            if(criteria != null) {
                List<String> criterias = criteria.split(',');
                for(String rule : criterias){
                    query += ' AND ' + rule +' ';
                }
            }
        }
        
        // getting configs where state equal to scratch sheet state
        
        List<TRHC__Config__c> configs = Database.query(query);
        
        System.debug('Trigger ScratchsheetTrigger Config Query : '+ configQuery);
        List<TRHC__Expense__c> availableExpenses = Database.query(configQuery);
        System.debug('Trigger ScratchsheetTrigger Config Count : '+ configs.size());
        System.debug('Trigger ScratchsheetTrigger Avaialbe Expenses Count : '+ availableExpenses.size());
        if(configs != null && configs.size() > 0) {
            
            List<Config_To_Expense__c> configToExpense = [Select Config_Field_API_Name_Custom__c, Expanse_Field_API_Name__c From Config_To_Expense__c];
            
            for(TRHC__Config__c config : configs) {
                Set<TRHC__Config__c> configObjs = configMap.get(config.TRHC__State__c);
                if(configObjs == null) {
                    configObjs = new Set<TRHC__Config__c>();
                }
                configObjs.add(config);
                configMap.put(config.TRHC__State__c, configObjs);
                System.debug('Config Object : '+ config.ID);
            }
            
            System.debug('Trigger ScratchsheetTrigger Config Map Count : '+ configMap.size());
            Set<TRHC__Expense__c> expenses = new Set<TRHC__Expense__c>();
            for(TRHC__Scratch_Sheet__c scratchSheet : trigger.new) {
                if(scratchSheet.State_pic__c== null) { 
                    continue;
                }
                
                Set<TRHC__Config__c> configIds = configMap.get(scratchSheet.State_pic__c);
                List<String> nameLst = new List<String> ();
                List<String> scrExpList = new List<String>();
                for(TRHC__Config__c configId : configIds) {
                    boolean isExist = false;
                    boolean addCityExpense = false;
                    System.debug('scratchSheet.City__c@@'+scratchSheet.City__c);
                    System.debug('configId.TRHC__City__c @@'+configId.TRHC__City__c);
                    System.debug('configId.id @@'+configId.id);
                    system.debug('exptIdsList.contains(configId.id)@@'+exptIdsList.contains(configId.id));
                    if(exptIdsList.contains(configId.id) && scratchSheet.City__c != configId.TRHC__City__c){
                        System.debug('Comming to return true');
                        isExist = true;
                    }
                    else if(exptIdsList.contains(configId.id) ){
                        nameLst.add(configId.TRHC__Name__c);
                        addCityExpense = true;
                    }
                    System.debug('ScratchSheet Id :'+ scratchSheet.Id + ' Config Id :' + configId.id);
                    for(TRHC__Expense__c avaExp : availableExpenses) {
                        System.debug('AvaExp :'+ avaExp.TRHC__Scratch_Sheet__c +' avaExp :'+ avaExp.TRHC__Config__c);
                        System.debug('nameLst @@@@ '+nameLst);
                        System.debug('avaExp.TRHC__Expense_Name__c @@@ '+avaExp.TRHC__Expense_Name__c);
                        if(avaExp.TRHC__Scratch_Sheet__c == scratchSheet.Id && avaExp.TRHC__Config__c == configId.id) {
                            isExist = true;
                        }
                    }
                    if(isExist) {
                        continue;
                    }
                    TRHC__Expense__c expense = new TRHC__Expense__c();
                    for(Config_To_Expense__c configToExp : configToExpense){
                        String expanseField = configToExp.Expanse_Field_API_Name__c;
                        system.debug('expanseField @@@'+expanseField );
                        String configField = configToExp.Config_Field_API_Name_Custom__c;
                        system.debug('configField@@@'+configField);
                        if(configFields.contains(configField)&& expanseFields.contains(expanseField)){
                            
                            system.debug('comming to inside if @@@');
                            
                            if(configField == 'TRHC__Value__c'){
                                expense.TRHC__Value__c = Decimal.valueOf(configId.TRHC__Value__c);
                            }else{
                                expense.put(expanseField,configId.get(configField));
                                
                            }
                            
                        }
                    }
                    expense.TRHC__Config__c = configId.id;
                    expense.TRHC__Scratch_Sheet__c  = scratchSheet.Id;
                    
                    expenses.add(expense);
                    scrExpList.add(expense.TRHC__Expense_Name__c);
                    //List<String> expNameList = new List<String>();
                    system.debug('expense$$$'+expense);   
                    system.debug('expenses$$$'+expenses);                  
                }
                /*code*/
                
                /*for(TRHC__Expense__c expence:expenses){
                    scrExpList.add(expence.TRHC__Expense_Name__c);
                }*/
                set<String> setTempString = new set<String>();
                set<String> duplicateTempString = new set<String>();
                for(String s:scrExpList){
                    if(!setTempString.add(s)){
                        duplicateTempString .add(s);
                    }
                }
                for(TRHC__Expense__c expence:expenses){
                    for(String str:duplicateTempString){
                        /*if(str == expence.TRHC__Expense_Name__c){
                            if(!exptIdsList.contains(expence.TRHC__Config__c)){
                                expenses.remove(expence); 
                            }
                        }*/
                        if(str == expence.TRHC__Expense_Name__c){
                            System.debug('Account Name@@@'+scratchSheet.Account_Name__c);
                            if(exptAccountList.contains(scratchSheet.Account_Name__c)){
                                System.debug('Account name second time');
                                if(exptIdsList.contains(expence.TRHC__Config__c)){
                                    expenses.remove(expence);  
                                }
                                else{
                                    expence.TRHC__Value__c = 0.0;
                                }
                            }
                            else if(!exptIdsList.contains(expence.TRHC__Config__c)){
                                expenses.remove(expence); 
                            }
                        }
                    }
                }
                /* code */
                for(TRHC__Expense__c exp :expenses){
                    System.debug('New Jersey - tax'+exp.TRHC__Expense_Name__c);
                    if(exp.TRHC__Expense_Name__c != null && exp.TRHC__Expense_Name__c.contains('Sales Tax') && exptAccountList.contains(scratchSheet.Account_Name__c)){
                        exp.TRHC__Value__c = 0.0;
                        System.debug('Comming plus3');
                    }
                }
                /* code */
                system.debug('duplicateTempString >>'+duplicateTempString);
                /*code*/
                If(expenses.size()>0) {
                    insert new List<TRHC__Expense__c>(expenses);
                }
            }
        }
    }
}