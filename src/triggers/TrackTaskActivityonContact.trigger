/*	Description: Uodate the latest activity Description in Contact as last activity note,
   	Events: after insert and after update,
	Custom setting to Swith : Config settings --> DisabletrackTaskActivityonContact
*/
trigger TrackTaskActivityonContact on Task (after insert, after update) { 
    
    boolean isDisabled = false;
    TRHC__Config_Settings__c config = TRHC__Config_Settings__c.getOrgDefaults();
    if ( config != null )
        isDisabled = config.DisabletrackTaskActivityonContact__c;
    if( isDisabled ) 
        return;
    Set< ID > whoIdSet = new Set< ID >();
    set< ID > whatIdSet = new Set< ID >();
    for( Task t: trigger.new ){
        if( t.whoId != null ){
            whoIdSet.add ( t.WhoId ); 
        }
        if( t.WhatId != null ){
            whatIdSet.add ( t.WhatId );
        }
    }
    //retreive contacts
    Map< ID, Contact > contactMap = new Map< ID, Contact >([
        SELECT ActivityNotes__c, LastActivityDate__c
        FROM Contact 
        WHERE Id IN :whoIdSet]);
    Map< ID, Account > accountMap = new Map< ID, Account >([
        SELECT ActivityNotes__c, LastActivityDate__c
        FROM Account 
        WHERE Id IN :whatIdSet]);
    
    //update LastActivity date and Notes value 
    Set < Contact > ConList = new Set< Contact >();
    Set < Account > accountList = new Set < Account >();
    for(Task t: trigger.new){
        if( t.whoId != null && contactMap.containsKey( t.whoId ) ){
            Contact c1 = contactMap.get( t.whoId ); 
            
            c1.LastActivityDate__c = t.LastModifiedDate;
            
            if(t.Description!=null){
                if(t.Description.length()<249){
                    c1.ActivityNotes__c    = t.Description;
                }
                else{
                    c1.ActivityNotes__c    = t.Description.subString(0,249);
                }
            }
            ConList.add(c1);
        }
        if(t.WhatId != null  && accountMap.containsKey(t.WhatId)){
            Account A1 = accountMap.get(t.WhatId);
            A1.LastActivityDate__c = t.LastModifiedDate;
            if(t.Subject!=null){
                A1.ActivityNotes__c		= t.Subject;
            }
            accountList.add(A1);
        }
    }
    if(ConList.size()>0){
        update new List<Contact> ( ConList );
    }
    if(accountList.size()>0){
        update new List<Account> ( accountList );
    }
}