trigger TriggerUpdatePlacementCountOnJOb on TRHC__Placement__c (After insert, After update, After undelete, before delete) {
    Boolean run = TRHC__config_Settings__c.getInstance('Default').Enable_TriggerUpdatePlacementCountOnJOb__c;
    if(run){
        UpdateJobPlacementcount updateJob = new UpdateJobPlacementcount();
        if(trigger.isDelete){
            updateJob.UpdateJobPlacementcount(Trigger.old);
        }else{
            updateJob.UpdateJobPlacementcount(Trigger.new);
        }
    }
}