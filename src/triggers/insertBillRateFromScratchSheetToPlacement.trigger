trigger insertBillRateFromScratchSheetToPlacement on TRHC__Placement__c (before insert) {
	Boolean run = TRHC__config_Settings__c.getInstance('Default').Enable_TriggerUpdatePlacementCountOnJOb__c;
    if(run){
    	if(trigger.isInsert){	
    		insertBillRateFromScratchToPlaceHandler.insertBillRateOnPlacement(Trigger.new);
    	}
    }
}