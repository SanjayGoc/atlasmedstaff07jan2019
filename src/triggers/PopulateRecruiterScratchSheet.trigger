trigger PopulateRecruiterScratchSheet on TRHC__Job__c (before insert , after update, before update) {
    
    PopulateRecruiterScratchSheetHandler popolateRec = new PopulateRecruiterScratchSheetHandler();
    
    if(trigger.isBefore && trigger.isInsert || trigger.isAfter && trigger.isUpdate){
        
        popolateRec.PopolateRecruiter(Trigger.new);
    }
    
    
    /*
    if(trigger.isAfter && trigger.isUpdate){
        if(checkRecursive.runOnce()){

            popolateRec.getDistanceFromAccount(trigger.newMap.keyset());
        }
    }
    */
}