trigger UpdateCandidateLastNote on Note (before insert,before update) {
       Boolean isDisabled = false;

    TRHC__Config_Settings__c config = TRHC__Config_Settings__c.getOrgDefaults();
        if (config != null)
        isDisabled = config.DisabletrackNoteonContact__c;
    
        if(isDisabled ) 
        return;
            
   Set<ID> whoIdSet = new Set<ID>(); 
    for(Note t: trigger.new){
        if(t.parentId != null){
            whoIdSet.add(t.parentId); 
        }
    }
    //retreive contacts
    Map<ID, Contact> contactMap = new Map<ID, Contact>([SELECT ID,NotesDate__c
                                                        FROM Contact Where Id IN :whoIdSet]); 
    //update LastActivity date and Notes value 
    List<Contact> ConList = new List<Contact>(); 
    for(Note t: trigger.new){
        if(t.parentid != null && contactMap.containsKey(t.parentid)){
            Contact c1 = contactMap.get(t.parentId); 
            //system.debug('LastModifiedDate'+t.LastModifiedDate);
            c1.NotesDate__c = Date.today();
            system.debug('c1'+ c1);
            ConList.add(c1);
            }      
        }
    
    if(ConList.size()>0){
        update ConList; 
    }
}