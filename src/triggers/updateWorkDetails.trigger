trigger updateWorkDetails on TRHC__Placement__c (before insert) {
    system.debug('===> before insert');
    TRHC__Config_Settings__c configSet = TRHC__Config_Settings__c.getINstance('Default');
    boolean enableTrigger = configSet.Enable_UpdateWorkDetails_trigger__c;
    if(enableTrigger)
        updateWorkDetailsHandler.updateWorkOrder(Trigger.new);
}