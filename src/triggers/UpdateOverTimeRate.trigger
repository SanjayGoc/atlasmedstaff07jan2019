/*
*Trigger to update Overtime Bill Rate value in job, based on the values 'Bill Rate Multiplier' and 'Bill Rate Fixed Dollar Amount' field values in account
*/
trigger UpdateOverTimeRate on TRHC__Job__c(before insert, before update) {

    Boolean isRun = TRHC__Config_Settings__c.getInstance('Default').EnableUpdateOverTimeRate__c;

    if (isRun) {
        if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)) {
            UpdateOverTimeRateHandler.updateOvertimeRateAndPayRate(Trigger.New,Trigger.oldMap);
        }  
    }  


   /* //reading the custom setting (configdetting :EnableUpdateOverTimeRate__c), to know the triggerr is enabled or disable
    Boolean isRun = TRHC__Config_Settings__c.getInstance('Default').EnableUpdateOverTimeRate__c;

    if (isRun) {
        Set<Id> accountIds = new Set<Id>();
        for (TRHC__Job__c singleJob : Trigger.new) {
            accountIds.add(singleJob.TRHC__Account_LookUp__c);
        }

        if(Trigger.isInsert){
            for (TRHC__Job__c mJob : Trigger.new) {
                if(mJob.TRHC__Overtime_Bill_Rate__c != null)
                    mJob.TRHC__Overtime_Pay_Rate__c = mJob.TRHC__Overtime_Bill_Rate__c * 0.7;
            }
        }else{
            for (TRHC__Job__c mJob : Trigger.new) {
                if(mJob.TRHC__Overtime_Pay_Rate__c == null){ 
                    mJob.TRHC__Overtime_Pay_Rate__c = 0.00;
                }
                if(mJob.TRHC__Overtime_Bill_Rate__c != null && mJob.TRHC__Overtime_Bill_Rate__c != 0.00 && mJob.TRHC__Overtime_Pay_Rate__c/mJob.TRHC__Overtime_Bill_Rate__c <= 0.7){
                    mJob.TRHC__Overtime_Pay_Rate__c = mJob.TRHC__Overtime_Bill_Rate__c * 0.7;       
                }
            }
        }

        Map<Id,Account> mapAccount = new Map<Id,Account>([SELECT Id,Bill_Rate_Multiplier__c, Bill_Rate_Fixed_Dollar_Amount__c FROM Account WHERE Id IN: accountIds]);
        for (TRHC__job__c trhcJob: Trigger.new) {
            //it should run only job had Account 
            if (trhcJob.TRHC__Account_LookUp__c != null) {
                //Account account = [Select id, Bill_Rate_Multiplier__c, Bill_Rate_Fixed_Dollar_Amount__c from Account where id = : trhcJob.TRHC__Account_LookUp__c];
                //this triggered because of insert or not
                if (Trigger.isInsert) {
                    //checking the condition 
                    if (mapAccount.get(trhcJob.TRHC__Account_LookUp__c).Bill_Rate_Multiplier__c != null && mapAccount.get(trhcJob.TRHC__Account_LookUp__c).Bill_Rate_Fixed_Dollar_Amount__c == null && trhcJob.TRHC__Regular_Bill_Rate__c != null) {

                        trhcJob.TRHC__Overtime_Bill_Rate__c = (mapAccount.get(trhcJob.TRHC__Account_LookUp__c).Bill_Rate_Multiplier__c) * (trhcJob.TRHC__Regular_Bill_Rate__c);
                        System.debug('@@@@@ value' + trhcJob.TRHC__Regular_Bill_Rate__c);

                    } else {
                    //checking the condition
                        if (mapAccount.get(trhcJob.TRHC__Account_LookUp__c).Bill_Rate_Multiplier__c == null && mapAccount.get(trhcJob.TRHC__Account_LookUp__c).Bill_Rate_Fixed_Dollar_Amount__c != null && trhcJob.TRHC__Regular_Bill_Rate__c != null)
                            trhcJob.TRHC__Overtime_Bill_Rate__c = mapAccount.get(trhcJob.TRHC__Account_LookUp__c).Bill_Rate_Fixed_Dollar_Amount__c + trhcJob.TRHC__Regular_Bill_Rate__c;
                            System.debug('@@@@@ value' + trhcJob.TRHC__Regular_Bill_Rate__c);
                        }
                } else if (trigger.isUpdate) {
                    
                    TRHC__job__c trOldJob = trigger.OldMap.get(trhcJob.id);
                    //check the account field in job got changed or not
                    if (trOldJob.TRHC__Account_LookUp__c != trhcJob.TRHC__Account_LookUp__c || trOldJob.TRHC__Regular_Bill_Rate__c != trhcJob.TRHC__Regular_Bill_Rate__c) {
                        //either 'Bill_Rate_Multiplier__c' or Bill_Rate_Fixed_Dollar_Amount__c only one should have value and in job Regular bill rate shold not null
                        if (mapAccount.get(trhcJob.TRHC__Account_LookUp__c).Bill_Rate_Multiplier__c != null && mapAccount.get(trhcJob.TRHC__Account_LookUp__c).Bill_Rate_Fixed_Dollar_Amount__c == null && trhcJob.TRHC__Regular_Bill_Rate__c != null) {
                            trhcJob.TRHC__Overtime_Bill_Rate__c = (mapAccount.get(trhcJob.TRHC__Account_LookUp__c).Bill_Rate_Multiplier__c) * (trhcJob.TRHC__Regular_Bill_Rate__c);
                            System.debug('@@@@@ value' + trhcJob.TRHC__Regular_Bill_Rate__c);
                        } else {
                            if (mapAccount.get(trhcJob.TRHC__Account_LookUp__c).Bill_Rate_Multiplier__c == null && mapAccount.get(trhcJob.TRHC__Account_LookUp__c).Bill_Rate_Fixed_Dollar_Amount__c != null && trhcJob.TRHC__Regular_Bill_Rate__c != null)
                                //trhcJob.TRHC__Overtime_Bill_Rate__c = mapAccount.get(trhcJob.TRHC__Account_LookUp__c).Bill_Rate_Fixed_Dollar_Amount__c + trhcJob.TRHC__Regular_Bill_Rate__c;
                                //trhcJob.TRHC__Overtime_Bill_Rate__c = trhcJob.TRHC__Overtime_Pay_Rate__c * 0.7;
                                System.debug('@@@@@ value' + trhcJob.TRHC__Regular_Bill_Rate__c);
                        }
                    }
                }
            }
        }
    }*/
}