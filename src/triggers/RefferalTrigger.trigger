trigger RefferalTrigger on Contact (before Update) {
    TRHC__Config_Settings__c config = TRHC__Config_Settings__c.getInstance('Default');
    Boolean ref = config.Enable_RefferalTrigger__c;
    if(ref){
        Map<Id, Contact> mapContacts = Trigger.oldMap;
        Set<Contact> listReferalContacts = new Set<Contact>();
        List<String> referalSourceList = new List<String> ();
        Map<String, Id> referalContactsMap = new Map<String, Id>();
        for(Contact con :trigger.new){
               
            if(con.TRHC__Referral_Source__c != null&& con.TRHC__Referral_Source__c != con.Name && con.TRHC__Referral_Source__c != con.FirstName +' '+con.LastName && con.TRHC__Referral_Source__c != con.FirstName && con.TRHC__Referral_Source__c != con.LastName && con.TRHC__Referred_By__c == Null){
                
                listReferalContacts.add(con);
                referalSourceList.add(con.TRHC__Referral_Source__c);
            } 
        }
        for(Contact con : [SELECT Id, TRHC__Referral_Source__c, Name, FirstName, LastName, Email FROM Contact WHERE Name IN :referalSourceList OR FirstName IN :referalSourceList OR LastName IN :referalSourceList OR Email IN :referalSourceList Limit 50000]){
            if(referalSourceList.contains(con.Name)){
                referalContactsMap.put(con.Name, con.id);  
            }else if(referalSourceList.contains(con.FirstName)){
                referalContactsMap.put(con.FirstName, con.id);
            }else if(referalSourceList.contains(con.LastName)){
                referalContactsMap.put(con.LastName, con.id);
            } else if(referalSourceList.contains(con.Email)){
                referalContactsMap.put(con.Email, con.id);
            }
            
        }
        for(Contact con:listReferalContacts){
            if(referalContactsMap.keySet().contains(con.TRHC__Referral_Source__c))
                con.TRHC__Referred_By__c = referalContactsMap.get(con.TRHC__Referral_Source__c);
        }
        
    }
}