trigger PopolaterecruiterPlacement on TRHC__Placement__c (before insert , after update) {
    TRHC__config_Settings__c  configSet = TRHC__config_Settings__c.getInstance('Default');
    Boolean isRun = configSet.Disable_PopolaterecruiterPlacement__c;
    if(isRun){
      PopolaterecruiterPlacementHandler popolateRecPlac = new PopolaterecruiterPlacementHandler();
      popolateRecPlac.recruiterPopolateJob(Trigger.new);
    }
   }