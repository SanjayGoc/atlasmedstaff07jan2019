trigger NotifyRecruitersOnPlacementCreation on TRHC__Placement__c (After insert) {
    TRHC__Config_Settings__c configSet = TRHC__Config_Settings__c.getINstance('Default');
    boolean Notify = configSet.Enable_NotifyRecruitersOnPlacementCreati__c;
    if(Notify){
        set < id > appIdSet = new set < id > ();
        Set < id > jobIdSet = new set < id > ();
        for ( TRHC__Placement__c placement : trigger.new ) {
            if( placement.TRHC__Job__r.TRHC__Number_of_Positions__c == placement.TRHC__Job__r.TRHC__Number_of_Placements__c) {
                appIdSet.add ( placement.TRHC__Job_Applicant__c ); 
                jobIdSet.add( placement.TRHC__Job__c);
            }
        }
        EmailTemplate et = [Select id from EmailTemplate where name = : 'NotifyOnPlacementCreation'];
        List < Id > appIdList = new List< Id > ( appIdSet );
        List<id> jobIdList = new List< Id > ( jobIdSet );
        List < TRHC__Job_Applicant__c > applicants = [
                                                Select id, TRHC__Recruiter__c, TRHC__Contact_Candidate__c  
                                                From TRHC__Job_Applicant__c
                                                where  id NOT IN :appIdSet and TRHC__Job__c IN :jobIdList 
                                                        and TRHC__Stage__c != 'Hired' 
                                                        and TRHC__Stage__c != 'Placed'];
        system.debug('@@@@>>>>>'+applicants);
        List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage> ();
        for( TRHC__Job_Applicant__c jobApp :applicants ){
            Messaging.SingleEmailMessage singleMail = new Messaging.SingleEmailMessage();
            List<OrgWideEmailAddress> owea = [select id, DisplayName, Address from OrgWideEmailAddress Where DisplayName = 'Atlas MedStaff' Limit 1];
            if (owea.size() > 0 ) {
                system.debug('owea@@@'+owea);
                singleMail.setOrgWideEmailAddressId(owea.get(0).Id);
            }
            //setting the target object
            singleMail.setTargetObjectId(jobApp.TRHC__Contact_Candidate__c);
            //setting template id 
            singleMail.setTemplateId(et.Id);
            singleMail.setWhatId(jobApp.Id);
            singleMail.setSaveAsActivity(false);
            singleMail.setTreatTargetObjectAsRecipient(FALSE);
            singleMail.toAddresses = new String[] {
                jobApp.TRHC__Recruiter__c
                    };
                        if(jobApp.TRHC__Recruiter__c != null)
                        mailList.add(singleMail);
        }
        Messaging.sendEmail(mailList);
    }
}