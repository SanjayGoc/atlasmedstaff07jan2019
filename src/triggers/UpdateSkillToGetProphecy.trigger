trigger UpdateSkillToGetProphecy on TRHC__Skill_Answers__c (before Insert) {
    if(Trigger.isbefore && Trigger.isInsert) {
        UpdateSkillToGetProphecyHandler.updateModifiedID(Trigger.New);
    }
}