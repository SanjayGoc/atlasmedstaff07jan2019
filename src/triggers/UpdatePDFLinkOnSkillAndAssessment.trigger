trigger UpdatePDFLinkOnSkillAndAssessment on Attachment (after insert,after update) {
    PDF_URL_Update_Setting__c setting = PDF_URL_Update_Setting__c.getInstance();
    if (setting.Enable__c) {
        String skillsKeyPrefix = TRHC__Skill_Answers__c.sobjecttype.getDescribe().getKeyPrefix();
        String assessmentKeyPrefix = TRHC__Assessment__c.sobjecttype.getDescribe().getKeyPrefix();
        Set<Id> skillIdSet = new Set<Id> ();
        Set<Id> assessmentIdSet = new Set<Id> ();
        Map<Id,Id> skillAndAttchmentMap = new Map<Id,Id> ();
        Map<Id,Id> assessmentAndAttchmentMap = new Map<Id,Id> ();
        Map<Id,Id> contactAndAttachmentMap = new Map <Id,Id> ();
        List<TRHC__Skill_Answers__c> skillsToUpdate = new List <TRHC__Skill_Answers__c> ();
        List<TRHC__Assessment__c> asssessmentsToUpdate = new List <TRHC__Assessment__c> ();
        for (Attachment eachAttach: Trigger.New) {
            if (String.valueOf(eachAttach.ParentId).startsWith(skillsKeyPrefix)) {
                skillIdSet.add(eachAttach.ParentId);
                skillAndAttchmentMap.put(eachAttach.ParentId,eachAttach.Id);
            } else if (String.valueOf(eachAttach.ParentId).startsWith(assessmentKeyPrefix)) {
                assessmentIdSet.add(eachAttach.ParentId);
                assessmentAndAttchmentMap.put(eachAttach.ParentId,eachAttach.Id);
            }
        }
        for (TRHC__Skill_Answers__c eachSkill: [SELECT
                                                    TRHC__Contact__c,PDF_URL__C
                                                FROM
                                                    TRHC__Skill_Answers__c
                                                WHERE
                                                    ID IN: skillIdSet]) {
            eachSkill.PDF_URL__C = System.URL.getSalesforceBaseUrl().toExternalForm()+'/servlet/servlet.FileDownload?file='+skillAndAttchmentMap.get(eachSkill.Id);
            skillsToUpdate.add(eachSkill);
        }
        for (TRHC__Assessment__c eachAssessment: [SELECT
                                                    TRHC__Contact__c,PDF_URL__C
                                                FROM
                                                    TRHC__Assessment__c
                                                WHERE
                                                    ID IN: assessmentIdSet]) {
            eachAssessment.PDF_URL__C = System.URL.getSalesforceBaseUrl().toExternalForm()+'/servlet/servlet.FileDownload?file='+assessmentAndAttchmentMap.get(eachAssessment.Id);
            asssessmentsToUpdate.add(eachAssessment);
        }
        if (skillsToUpdate.size()>0) {
            update skillsToUpdate;
        }
        if (asssessmentsToUpdate.size()>0) {
            update asssessmentsToUpdate;
        }
    }
}