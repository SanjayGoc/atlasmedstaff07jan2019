trigger RestrictDelation on Contact (before delete) {
    if(trigger.isBefore && trigger.isDelete){
        TRHC__Config_Settings__c configSet = TRHC__Config_Settings__c.getInstance('Default');
        String cid = configSet.con_not_delete_coverpage__c != null?configSet.con_not_delete_coverpage__c.trim():'';
        for(Contact con:trigger.old){
            if(cid != '' && con.id == cid)
            con.addError('We can not delete these candidate');
        }
    }

}