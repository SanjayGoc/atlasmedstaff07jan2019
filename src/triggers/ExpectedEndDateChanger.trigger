trigger ExpectedEndDateChanger on TRHC__Job__c(before update,before insert){
        
        
        //custom setting to trigger enable or not
        boolean triggerRun = (boolean)TRHC__Config_Settings__c.getInstance('Default').get('Trigger_Enable_Expected_End_Date_Changer__c');
        
        
            for(TRHC__Job__c job:Trigger.new){
                if(Trigger.isInsert){
        if(job.TRHC__State__c != null || job.TRHC__Account_Lookup__c != null){
        
        //job.State_pic__c = job.TRHC__State__c;
          
          String code = job.TRHC__State__c;
          String state = MapStateUtil.getStateForCode(code);
          if(state!=null){
              job.State_pic__c = state;
          }
        
        }
        }
                if(triggerRun){
        
            //variable to store old term value of job in case of update
        String term='';
        //Variable to store old start date value of job in case of update
        Date tDate =Date.today();
        //checking like trigger event is update or not 
        if(Trigger.IsUpdate){
        //getting old job by using the job id   
        TRHC__Job__c oldJob=Trigger.oldMap.get(job.Id);
        //old term value , to check that field is changed or not
        term=oldJob.TRHC__Job_Term__c;
        
        //old start date field to check this field is changed or not
        tDate =oldJob.TRHC__Start_Date__c;
        
        }
        //logic for finding the last saturday
            
            //Map to hold the data
        Map<String, Integer> dayMap=new Map<String, Integer>();
        /* based on current date and week we desiding
            how many days we need to go back for sat 
            eg: current week is Sunady 
            
        */
        
        dayMap.put('Tuesday',3);
        dayMap.put('Wednesday',4);
        dayMap.put('Thursday',5);
        dayMap.put('Friday',6);
        dayMap.put('Monday',2);
        dayMap.put('Sunday',1);
        String Jobterm=job.TRHC__Job_Term__c;
        system.debug('jobstr@@'+job.TRHC__Start_Date__c);
        system.debug('Tdate@@'+tDate );
        
        if((term!=job.TRHC__Job_Term__c ||(tDate!=null && tDate!=job.TRHC__Start_Date__c) )&& Jobterm !=null)        
        {
        
        
       
        system.debug('Jobterm'+Jobterm);
        String[] weekCount = jobterm.split(' ');
        integer totalDaysHasToAdd=integer.valueOf(weekCount[0])*7;
        
        Date currentDate=Date.today();
        system.debug('currentDate'+currentDate);
        Date expStartDate = job.TRHC__Start_Date__c;
        if(expStartDate!=null) {
            system.debug('expStartDate'+expStartDate);
            currentDate=expStartDate ;
        }
        
        currentDate+=totalDaysHasToAdd;
        
        Datetime dt=DateTime.newInstance(currentDate,Time.newInstance(0,0,0,0));
        system.debug('dt'+dt);
        String dayOfWeek=dt.format('EEEE');
        system.debug('dayOfWeek'+dayOfWeek);
        if(dayOfWeek=='saturday'){
        system.debug('problome ?');
        job.TRHC__Expected_End_Date__c=currentDate;
        }
        else{
        job.TRHC__Expected_End_Date__c=currentDate-dayMap.get(dayOfWeek);
        }
        }

        }
        }
        }