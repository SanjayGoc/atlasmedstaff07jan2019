trigger AttachmentBatch on Attachment__c (After insert) {
    TRHC__Config_Settings__c configSet = TRHC__Config_Settings__c.getINstance('Default');
    boolean enableTrigger = configSet.Enable_ContactAttachFile_trigger__c;
    if(enableTrigger) {
        AttachmentBatchHandler attachBatch = new AttachmentBatchHandler(Trigger.NewMap.keySet());
        database.executeBatch(attachBatch, 1);
    }
}