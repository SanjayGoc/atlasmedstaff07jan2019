trigger AutoMateJobRatesTrigger on TRHC__Job__c (before update) {
    
    TRHC__Config_Settings__c config = TRHC__Config_Settings__c.getInstance('Default');
    if(config.Enable_AutoMateJobRates__c)
        for(TRHC__Job__c scr:trigger.new){
            if(!scr.California_OT_After_8__c){
                if(((scr.TRHC__Hours_Per_Week__c != 0.00 && scr.TRHC__Hours_Per_Week__c != Null) || scr.TRHC__Hours_to_be_consider_per_day__c != Null || scr.TRHC__Number_of_Days_Per_Week__c != Null)){
                    
                    if((scr.TRHC__Hours_Per_Week__c != 0.00 && scr.TRHC__Hours_Per_Week__c != Null) && scr.TRHC__Hours_to_be_consider_per_day__c == Null && scr.TRHC__Number_of_Days_Per_Week__c == Null){
                        System.debug('&&&&&&'+scr.TRHC__Hours_Per_Week__c);
                        if(scr.TRHC__Hours_Per_Week__c == 40.00){
                            scr.TRHC__Number_of_Days_Per_Week__c = 5.00;
                            scr.TRHC__Hours_to_be_consider_per_day__c = 8;
                        }else if(scr.TRHC__Hours_Per_Week__c == 36.00){
                            scr.TRHC__Number_of_Days_Per_Week__c = 3.00;
                            scr.TRHC__Hours_to_be_consider_per_day__c = 12; 
                        }
                    }else if((scr.TRHC__Hours_Per_Week__c == Null || scr.TRHC__Hours_Per_Week__c == 0.00) && scr.TRHC__Hours_to_be_consider_per_day__c != Null && scr.TRHC__Number_of_Days_Per_Week__c == Null){
                        if(scr.TRHC__Hours_to_be_consider_per_day__c == 12.00){
                            system.debug('inside 12');
                            scr.TRHC__Hours_Per_Week__c = 36;
                            scr.TRHC__Number_of_Days_Per_Week__c = 3.00;   
                        }else if(scr.TRHC__Hours_to_be_consider_per_day__c == 8.00){
                            scr.TRHC__Hours_Per_Week__c = 40;
                            scr.TRHC__Number_of_Days_Per_Week__c = 5.00;
                        }
                    }
                    else if((scr.TRHC__Hours_Per_Week__c == Null || scr.TRHC__Hours_Per_Week__c == 0.00) && scr.TRHC__Hours_to_be_consider_per_day__c == Null && scr.TRHC__Number_of_Days_Per_Week__c != Null){
                        if(scr.TRHC__Number_of_Days_Per_Week__c == 3.00){
                            scr.TRHC__Hours_Per_Week__c = 36;
                            scr.TRHC__Hours_to_be_consider_per_day__c = 12.00;
                        }else if(scr.TRHC__Number_of_Days_Per_Week__c == 5.00){
                            scr.TRHC__Hours_Per_Week__c = 40.00;
                            scr.TRHC__Hours_to_be_consider_per_day__c = 8.00;
                        }
                    }
                    else if((scr.TRHC__Hours_Per_Week__c == Null || scr.TRHC__Hours_Per_Week__c == 0.00) && scr.TRHC__Hours_to_be_consider_per_day__c != Null && scr.TRHC__Number_of_Days_Per_Week__c != Null){
                       
                        if(scr.TRHC__Number_of_Days_Per_Week__c == 5.00 && scr.TRHC__Hours_to_be_consider_per_day__c == 8){
                            scr.TRHC__Hours_Per_Week__c = 40.00;
                        }else if(scr.TRHC__Number_of_Days_Per_Week__c == 3.00 && scr.TRHC__Hours_to_be_consider_per_day__c == 12){
                            scr.TRHC__Hours_Per_Week__c = 36.00;
                        }
                    }
                    
                    else if((scr.TRHC__Hours_Per_Week__c != Null && scr.TRHC__Hours_Per_Week__c != 0.00) && scr.TRHC__Hours_to_be_consider_per_day__c == Null && scr.TRHC__Number_of_Days_Per_Week__c != Null){
                        if(scr.TRHC__Hours_Per_Week__c == 36.00 && scr.TRHC__Number_of_Days_Per_Week__c == 3.00){
                            scr.TRHC__Hours_to_be_consider_per_day__c = 12;
                        } else if(scr.TRHC__Hours_Per_Week__c == 40.00 && scr.TRHC__Number_of_Days_Per_Week__c == 5.00){
                            scr.TRHC__Hours_to_be_consider_per_day__c = 8;
                        }
                    }
                    else if((scr.TRHC__Hours_Per_Week__c != Null&& scr.TRHC__Hours_Per_Week__c != 0.00) && scr.TRHC__Hours_to_be_consider_per_day__c != Null && scr.TRHC__Number_of_Days_Per_Week__c == Null){
                        if(scr.TRHC__Hours_Per_Week__c/scr.TRHC__Hours_to_be_consider_per_day__c <= 7.00 && math.mod(Math.round( scr.TRHC__Hours_Per_Week__c), Math.round(scr.TRHC__Hours_to_be_consider_per_day__c)) == 0.00){
                            scr.TRHC__Number_of_Days_Per_Week__c = scr.TRHC__Hours_Per_Week__c/scr.TRHC__Hours_to_be_consider_per_day__c;
                        }
                    }
                }
                
            }
        }
}