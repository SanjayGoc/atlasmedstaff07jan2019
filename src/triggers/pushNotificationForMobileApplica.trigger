trigger pushNotificationForMobileApplica on TRHC__Job__c (after insert) {
    List<id> newJobList = new List<id>();

    For(TRhc__Job__c jobobj : trigger.New){
       
       if(jobobj.TRHC__Hiring_Manager__c != null)
           newJobList.add(jobobj.id);
       
    }
    FireBasePushNotificationClass.postHTTPFireBaseClass(newJobList);
    
}