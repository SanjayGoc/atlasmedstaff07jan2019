trigger UpdateGSAServiceTrigger on TRHC__Job__c (after insert) {
    TRHC__Healthcare_Setting__c customSetting = TRHC__Healthcare_Setting__c.getAll().get('Default');
    Boolean checkGSAService = (Boolean)customSetting.get('TRHC__Enable_GSA_Service_to_Update_M_I_Rate__c');

    if(checkGSAService == False){
        GetGSAServiceForJobBatch getGSA = new GetGSAServiceForJobBatch(trigger.newmap.keyset());
        Database.executebatch(getGSA,1);
    }
}