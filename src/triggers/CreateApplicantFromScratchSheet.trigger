/*
    *Trigger to create a job applicant from scratch sheet if status is submited,
    * candidate is taged, jobApplocant is false and job is tagged
*/
trigger CreateApplicantFromScratchSheet on TRHC__Scratch_Sheet__c(before update) {

    TRHC__Config_Settings__c config = TRHC__Config_Settings__c.getValues('Default');
    Boolean triggerSwitch = config.Trigger_Enable_Applicant_From_SCR_Sheet__c;
    Boolean triggerSwitch1 = config.Update_Pay_Burden__c;
    String fieldNames = config.Nurse_Gross_Weekly_Pay__c;
    Set<String> setOfString = new Set<String>();
    if (fieldNames != null) {
        setOfString = New Set<String>(fieldNames.split(','));
    }

    List <TRHC__Scratch_Sheet__c> scrList = Trigger.new;
    Set<Id> scracthIdSet = new Set<Id>();
    List<TRHC__Job_Applicant__c> jobApplicantList = new List<TRHC__Job_Applicant__c>();
    if (triggerSwitch) {

        Map<Id, TRHC__Job__c> jobMap = new Map<Id, TRHC__Job__c>();
        TRHC__Job_Applicant__c jobSubmital = new TRHC__Job_Applicant__c();
        Set<Id> Root_jobIds = new set<Id>();
        for (TRHC__Scratch_Sheet__c scr : scrList) {
            Root_jobIds.add(scr.Root_job__c);
        }

        for (TRHC__Job__c jobScr : [SELECT TRHC__Job_Title__c, TRHC__Account_Lookup__c FROM TRHC__Job__c where Id In : Root_jobIds]) {
            jobMap.put(jobScr.Id, jobScr);
        }

        for (TRHC__Scratch_Sheet__c scr : scrList) {
            TRHC__Scratch_Sheet__c scrOld = trigger.oldMap.get(scr.id);

            if (scr.TRHC__Candidate__c != null && scr.TRHC__Status__c == 'Submitted' && scr.IsJobApplicant__c != true && scr.Root_job__c != null && (scrOld.TRHC__Candidate__c != scr.TRHC__Candidate__c || scrOld.TRHC__Status__c != 'Submitted')) {
                scracthIdSet.add(scr.Root_job__c);

                try {
                    if (jobMap.containsKey(scr.Root_job__c)) {
                        jobSubmital.TRHC__Job__c = scr.Root_job__c;
                        jobSubmital.TRHC__Contact_Candidate__c = scr.TRHC__Candidate__c;
                        jobSubmital.TRHC__Scratch_Sheet__c = scr.id;
                        jobSubmital.TRHC__Job_Title__c = jobMap.get(scr.Root_job__c).TRHC__Job_Title__c;
                        jobSubmital.TRHC__Account_Job__c = jobMap.get(scr.Root_job__c).TRHC__Account_Lookup__c;
                        jobSubmital.TRHC__Stage__c = 'Submitted to Client Manager';
                        jobSubmital.TRHC__Type__c = 'Recruiter';
                        jobApplicantList.add(jobSubmital);

                        if (jobSubmital != null) {
                            scr.IsJobApplicant__c = true;
                        }
                    }

                } catch (Exception exp) {

                }
            }
        }

        if (!jobApplicantList.isEmpty()) {
            insert jobApplicantList;
        }
    }

    /*
    *@ desc: Handling the scratch sheet Pay Burden value
    */
    List<TRHC__Expense__c> expanseList = new List<TRHC__Expense__c>();
    Map<Id, List<TRHC__Expense__c>> expenseMap = new Map<Id, List<TRHC__Expense__c>>();
    if (triggerSwitch1) {

        for (TRHC__Expense__c exp : [select id, Total_Amount_Custom__c,Total_Amount_Difference__c, TRHC__Scratch_Sheet__c, TRHC__Weekly_Amount__c, TRHC__Grouping_Total__c, TRHC__Expense_Name__c  From TRHC__Expense__c where TRHC__Scratch_Sheet__c = : scrList]) {
            expanseList.add(exp);
            expenseMap.put(exp.TRHC__Scratch_Sheet__c, expanseList);
        }

        for (TRHC__Scratch_Sheet__c scr : scrList) {
            Decimal weekAmnt = 0;
            if (scr.TRHC__Hours_Per_Week__c != null && scr.TRHC__Pay_Rate__c != null) {
                weekAmnt  =  scr.TRHC__Hours_Per_Week__c * scr.TRHC__Pay_Rate__c;
                If( scr.over_time__c != null  && scr.TRHC__Overtime_Pay_Rate__c != null) {
                    weekAmnt  =  scr.TRHC__Hours_Per_Week__c * scr.TRHC__Pay_Rate__c + scr.over_time__c * scr.TRHC__Overtime_Pay_Rate__c;
                }
            } else {
                If( scr.over_time__c != null  && scr.TRHC__Overtime_Pay_Rate__c != null) {
                    weekAmnt  = scr.over_time__c * scr.TRHC__Overtime_Pay_Rate__c;
                }
            }
            Decimal payBurdenIni = 0;
            Decimal WeeklyAmount = 0;
            Decimal amountDiff = 0;
            if(expenseMap.get(scr.id) != null){
                for (TRHC__Expense__c expnse : expenseMap.get(scr.id)) {
                    if (expnse.TRHC__Grouping_Total__c == 'Pay Burden' && expnse.Total_Amount_Custom__c > 0) {

                        payBurdenIni = payBurdenIni + expnse.Total_Amount_Custom__c;
                        scr.Pay_Burden_Custom__c = payBurdenIni;
                    }
                    if (expnse.TRHC__Grouping_Total__c == 'Non Taxable Expense' && expnse.Total_Amount_Custom__c > 0) {
                        amountDiff = amountDiff + expnse.Total_Amount_Difference__c; 
                    }
                    

                    If(setOfString.contains(expnse.TRHC__Expense_Name__c) && expnse.Total_Amount_Custom__c != null) {
                        WeeklyAmount = WeeklyAmount + expnse.TRHC__Weekly_Amount__c;
                        scr.Nurse_Gross_weekly_pay__c = weekAmnt + WeeklyAmount;
                    }
                }

                scr.NonTaxable_Expense_OT__c = amountDiff + scr.Non_Taxable_Expense__c;
            }
        }
    }
}