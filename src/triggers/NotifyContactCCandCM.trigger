trigger NotifyContactCCandCM on Task (before update) {
	TRHC__Config_Settings__c config = TRHC__Config_Settings__c.getInstance('Default');
	boolean b = config.Enable_NotifyContactCCandCM__c;
	If(b){
		Set<String> conIdSet = new Set<String>();
		Set<String> accountSet = new Set<String>();
		Map<id,Task> oldtaskMap = trigger.oldMap;
		for(task task :trigger.new){
			if(task.WhoId != null && ((String)task.WhoId).startsWith('003')){
				conIdSet.add(task.whoid);
			}
			else if(((String)task.WhatId).startsWith('001')){
				accountSet.add(task.WhatId);
			}
		}
		Map<Id, Account> accountMap = new Map<Id, Account>([Select Id, Name, TRHC__Account_Manager__c, TRHC__Account_Manager__r.Name From Account Where id = :accountSet]);
		Map<Id, Contact> contactMap = New Map<Id, Contact>([Select Id, Name, TRHC__Recruiter__c, TRHC__Recruiter__r.Name, TRHC__Account_Manager__c From contact where id =:conIdSet]);
		List<Messaging.SingleEmailMessage> mails =  new List<Messaging.SingleEmailMessage>();
		List<OrgWideEmailAddress> owaList = [select id, DisplayName, Address from OrgWideEmailAddress Where DisplayName = 'Atlas MedStaff' Limit 1];
		for(Task conTask : trigger.new){
			if(conTask.Status == 'Completed' && oldtaskMap.get(conTask.id).Status != conTask.Status && (conTask.Subject == 'Atlas Adventures' || conTask.Subject == 'Act of Kindness' || conTask.Subject == 'Airfare' || conTask.Subject == 'Housing' || conTask.Subject == 'Birthday')){
				system.debug('contact object');
				List<String> sendTo = new List<String>();
				if(conTask.WhoId != null && ((String)conTask.WhoId).startsWith('003')){
					Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
					Contact relContact = contactMap.get(conTask.whoId);
					system.debug('@@@'+relContact);
					//sendTo.add(relContact.TRHC__Recruiter__c);
					mail.setSubject('Purchase Request For '+relContact.Name+' Completed');
					mail.setHtmlBody('Hi '+relContact.TRHC__Recruiter__r.Name +','+'<br/><br/>' +'Purchase request ' +'<a href=https://na35.salesforce.com/'+conTask.Id+'>'+conTask.Subject+'</a>' +' for your '+
    						' <a href=https://na35.salesforce.com/'+relContact.Id+'>'+relContact.Name+'</a> has now been completed. <br/><br/>'+'Thank You,<br/>Marketing Team');
					//mail.setToAddresses(sendTo);
					mail.setTargetObjectId(relContact.TRHC__Recruiter__c);
                    mail.saveAsActivity = false;
                    if(owaList.size() >0){
                    	mail.setOrgWideEmailAddressId(owaList[0].id);
                    }
					mails.add(mail);	
				}
				else If(conTask.WhatId != Null && ((String)conTask.WhatId).startsWith('001')){
					Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
					Account relAccount = accountMap.get(conTask.WhatId);
					//sendTo.add(relAccount.TRHC__Account_Manager__c);
					//mail.setToAddresses(sendTo);
					mail.setTargetObjectId(relAccount.TRHC__Account_Manager__c);
                    if(owaList.size() >0){
                    	mail.setOrgWideEmailAddressId(owaList[0].id);
                    }
					mail.setSubject('Purchase Request For '+relAccount.Name+' Completed');
					mail.setHtmlBody('Hi '+relAccount.TRHC__Account_Manager__r.Name +','+'<br/><br/>' +'Purchase request ' +'<a href=https://na35.salesforce.com/'+conTask.Id+'>'+conTask.Subject+'</a>' +' for your '+
    						' <a href=https://na35.salesforce.com/'+relAccount.Id+'>'+relAccount.Name+'</a> has now been completed. <br/><br/>'+'Thank You,<br/>Marketing Team');
					mail.setWhatId(relAccount.id);
					mail.saveAsActivity = false;
					system.debug('mail@@@'+mail);
					mails.add(mail);	
				}
			}
		}
        
		if(mails.size()>0 && !Test.isRunningTest())
		Messaging.sendEmail(mails);
	} 
	
}