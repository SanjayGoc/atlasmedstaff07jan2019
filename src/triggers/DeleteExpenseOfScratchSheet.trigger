trigger DeleteExpenseOfScratchSheet on TRHC__Scratch_Sheet__c (before delete) {
    
    TRHC__Config_Settings__c config = TRHC__Config_Settings__c.getInstance('Default'); 
    boolean b = Config.Expense_deletion__c ;
    if(b){
        List<Id> scrIdList = new List<Id> ();
        for(TRHC__Scratch_Sheet__c scr : trigger.old){
            scrIdList.add(scr.id);
        }
        List<TRHC__Expense__c> expList = [select id, name From TRHC__Expense__c Where TRHC__Scratch_Sheet__c IN :scrIdList];
        delete expList;  
    }
}