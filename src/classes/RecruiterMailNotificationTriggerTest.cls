@istest
public with sharing class RecruiterMailNotificationTriggerTest {
    @isTest
    private static void conTest(){
        TRHC__Config_Settings__c config = new TRHC__Config_Settings__c();
        
        config.name = 'Default';
        config.Enable_RecruitersMailNotificationTrigger__c= true;
        insert config;
        //insert Account
        Account account1 = new Account();
        account1.Name='abc';
        insert account1;
        
        //Insert User
        Profile p = [SELECT Id FROM Profile WHERE Name='Candidate Portal Profile']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Site Guest User', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Chicago', UserName='candidate_portal@atlasmedstaff1.com',firstName = 'Candidate Portal');

        System.runAs(u) {
            // The following code runs as user 'u' 
            System.debug('Current User: ' + UserInfo.getUserName());
            System.debug('Current Profile: ' + UserInfo.getProfileId()); 
        }
        
        
        //Creation of Contact
        ID cid = [      
                SELECT id, Name 
                from RecordType 
                where Name = 'Candidate'].Id;
        Contact testContact1 = new Contact();
        testContact1.RecordTypeId = cid;
        testContact1.lastName = 'Tr';
        testcontact1.ownerID = System.Label.SiteGuestUserId;
        testcontact1.Account = account1;
        insert testContact1;
        Contact testContact = new Contact();
        testContact.RecordTypeId = cid;
        testContact.lastName = 'Tr';
        testcontact.ownerID = System.Label.SiteGuestUserId;
        testcontact.Account = account1;
        //insert operation
        insert testContact;
        //testContact.Recruiters__c = 'Aaron biddle';
        testContact.TRHC__Permanent_City__c = 'Hyd';
        testContact.Permanent_State_Province1__c =SObjectType.Contact.Fields.Permanent_State_Province1__c.PicklistValues[0].getValue();
        testContact.TRHC__Permanent_Zip_Postal_Code__c ='53340';
        
        //update operation
        update testContact;
        
        testContact1.Recruiters__c = 'Aaron biddle';
        update testContact1;
        /*testContact.TRHC__Permanent_City__c = '';
    
        testContact.TRHC__Permanent_Zip_Postal_Code__c =null;
        update testContact;*/
    
    }
}