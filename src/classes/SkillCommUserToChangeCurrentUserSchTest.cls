/*
========================================================================
Description: Unit Class for SkillCommUserToChangeCurrentUserSchedule class 
========================================================================
*/

@istest
public class SkillCommUserToChangeCurrentUserSchTest {
    static testmethod void SkillCommUserToChangeCurrentUserSchTest() {
        Test.StartTest();
        SkillCommUserToChangeCurrentUserSchedule schTest = new SkillCommUserToChangeCurrentUserSchedule();      
        String sch = '0 0 23 * * ?';
        system.schedule('update InActive Skills', sch, schTest);
        Test.stopTest();
    }
}