/*******************************************************************
* @Author       : Hari S
* @Created Date : 07/25/2016
* @Description  : Webservice used to connect with Prophecy to retrive the results and updates 
                    In assessment/checklist records
********************************************************************/
public class ProphecyWebService{
    /*
    * @Author       : Hari S
    * @Description  : Holds the details of resposne received from prophecy
    * @return parameters : String
    */
    public class GetResultsByCaregiverAssessmentIDResponse_element {
        public String GetResultsByCaregiverAssessmentIDReturn;
        private String[] GetResultsByCaregiverAssessmentIDReturn_type_info = new String[]{'GetResultsByCaregiverAssessmentIDReturn','http://prophecyconnect',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://prophecyconnect','true','false'};
        private String[] field_order_type_info = new String[]{'GetResultsByCaregiverAssessmentIDReturn'};
    }
    /*
    * @Author       : Hari S
    * @Description  : Holds the details of request to receive details from prophecy
    * @parameters : Security code, assessmentcaregiver Id, Assessment Type
    */
    public class GetResultsByCaregiverAssessmentID_element {
        public String SecurityCode;
        public Double CaregiverAssessmentID;
        public String AssessmentType;
        private String[] SecurityCode_type_info = new String[]{'SecurityCode','http://prophecyconnect',null,'1','1','false'};
        private String[] CaregiverAssessmentID_type_info = new String[]{'CaregiverAssessmentID','http://prophecyconnect',null,'1','1','false'};
        private String[] AssessmentType_type_info = new String[]{'AssessmentType','http://prophecyconnect',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://prophecyconnect','true','false'};
        private String[] field_order_type_info = new String[]{'SecurityCode','CaregiverAssessmentID','AssessmentType'};
    }
    /*
    * @Author       : Hari S
    * @Description  : Class to connect with prophecy by taking the endpoint from custom setting 
    */
    public class ProphecyConnectXML_cfc {
        //public String endpoint_x = 'http://apiconnect.prophecyhealth.com/ProphecyConnect/ProphecyConnect.cfc';
        TRHC__Prophecy_Settings__c  v_ProphecyCustomSettings = TRHC__Prophecy_Settings__c.getinstance(UserInfo.getUserId());
        public String endpoint_x = v_ProphecyCustomSettings.TRHC__EndPointUrl_Results_Update__c;
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://rpc.xml.coldfusion', 'ProphecyCaregiver_rpcXmlColdfusion', 'http://xml.apache.org/xml-soap', 'ProphecyCaregiver_xmlApacheOrgXmlSoap', 'http://prophecyconnect', 'ProphecyWebService'};
        
        /*
    * @Author       : Hari S
    * @Description  : Used to initiate the prophecy process to get the results
    * @Parameters   : Security code, caregiverassessment id and type 
    */
        public String GetResultsByCaregiverAssessmentID(String SecurityCode,Double CaregiverAssessmentID,String AssessmentType) {
            ProphecyWebService.GetResultsByCaregiverAssessmentID_element request_x = new ProphecyWebService.GetResultsByCaregiverAssessmentID_element();
            request_x.SecurityCode = SecurityCode;
            request_x.CaregiverAssessmentID = CaregiverAssessmentID;
            request_x.AssessmentType = AssessmentType;
            ProphecyWebService.GetResultsByCaregiverAssessmentIDResponse_element response_x;
            Map<String, ProphecyWebService.GetResultsByCaregiverAssessmentIDResponse_element> response_map_x = new Map<String, ProphecyWebService.GetResultsByCaregiverAssessmentIDResponse_element>();
            response_map_x.put('response_x', response_x);
            if (!Test.isRunningTest()) {
            WebServiceCallout.invoke(this,request_x,response_map_x,new String[]{endpoint_x,'','http://prophecyconnect','GetResultsByCaregiverAssessmentID','http://prophecyconnect','GetResultsByCaregiverAssessmentIDResponse','ProphecyWebService.GetResultsByCaregiverAssessmentIDResponse_element'});
            response_x = response_map_x.get('response_x');
            return response_x.GetResultsByCaregiverAssessmentIDReturn; 
            } else {
                return null;
            }
        }
    }
}