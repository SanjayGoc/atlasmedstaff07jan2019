@RestResource(urlMapping='/mobile/SetReadNotification/*')
global class CommunitySetReadNotiRESTOperation {

    @HttpGet
    global static ResponseHandler CommunitySetReadNotiRESTOperation(){
        ResponseHandler res = new ResponseHandler();
        String apiToken = RestContext.request.params.get('apiToken');
        String isRead = RestContext.request.params.get('isRead');
        String NotificationId = RestContext.request.params.get('NotificationId');
        
        List<Contact> userList = [Select Id,
                                         Name,
                                         apiToken__c
                                From Contact 
                                Where apiToken__c =: apiToken
                                ];
                                
        if(!userList.isEmpty()){
            List<Notification__c> notificationList = [select id,name,Read_Unread__c from Notification__c where id =: NotificationId ];
                
            if(notificationList.isEmpty()){
                res.success = 0;
                res.message = new string[]{'Notification not Found.'};
                return res;
            }
            if(!notificationList.isEmpty()){
                if(isRead == '1'){
                    notificationList[0].Read_Unread__c = true;
                    update notificationList[0];
                }
                
                res.success = 1;
                res.message = new string[]{'Success'};
                res.isReadNoti = notificationList[0].Read_Unread__c;
                res.NotificationId = NotificationId;
                return res;
            }
            
        }
        res.success = 2;
        res.message = new string[]{'Unauthorized User.'};
        return res;
    }
    
    global class ResponseHandler {
        global integer success;
        global string[] message;
        global string NotificationId;
        global boolean isReadNoti;
        global ResponseHandler (){}
    }
}