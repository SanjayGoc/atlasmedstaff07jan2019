public class CMSLogsDeleterBatch implements Database.Batchable<Sobject>{

    public List<Sobject> start(Database.BatchableContext ctx){
        Integer dayLimit = Integer.valueOf(System.Label.DaysLimitForCMSLogs);
        Date deadLineDate = date.today()-dayLimit;
        List<TRHC__CMSSiteSetup__c> cmsLogs = [Select id, name From TRHC__CMSSiteSetup__c Where createdDate < :deadLineDate];
        return cmsLogs;
    }
    
    public void execute(Database.BatchableContext ctx, List<Sobject> cmsLogs){
        delete cmsLogs;
    }
    
    public void finish(Database.BatchableContext ctx){
        
    }
}