@isTest
public class pushNotificationForMobileApplicaTest {
    public static testmethod void pushNotification(){

        TRHC__Config_Settings__c configSettings = new TRHC__Config_Settings__c();
        configSettings.name='Default';
        configSettings.Enable_AutoMateJobRates__c = true;
        configSettings.Trigger_Enable_ScratchSheetAutoAddExp__c = false;
        configSettings.AutoAddExpenseCriteria__c='TRHC__Margin_Type__c=\'Expense\'';
        configSettings.Except_Hospitals__c = 'Atlas Development';
        configSettings.Trigger_Enable_Expected_End_Date_Changer__c = false;
        configSettings.Trigger_Enable_Applicant_From_SCR_Sheet__c = true;
        configSettings.Update_Pay_Burden__c = false;
        configSettings.Enable_RefferalTrigger__c = true;
        insert configSettings;

        List<Account> accountList = TestUtilityClass.getTestMultipleAccount();
        List<Contact> ContactList = TestUtilityClass.getTestMultipleContact(accountList[0]);

        TRHC__Config_Settings__c config = new TRHC__Config_Settings__c();
        config.Name = 'Default';
        config.Trigger_Enable_Applicant_From_SCR_Sheet__c = True;
        config.Update_Pay_Burden__c = True;
        config.Nurse_Gross_Weekly_Pay__c = 'Test Custom Setting';
        insert config;

        TRHC__Healthcare_Setting__c customSetting = new TRHC__Healthcare_Setting__c();
        customSetting.Name = 'Default';
        customSetting.TRHC__Enable_GSA_Service_to_Update_M_I_Rate__c = true;
        insert customSetting;

    	FireBaseServerKeyMobile__c server  = new FireBaseServerKeyMobile__c();
    	server.Name = 'Default';
    	server.serverKey__c = 'AAAAr1KJK-k:APA91bFeuugxXEKJwAEKQDbo7yQ76RXAzxH0wpsGAKr0Df6C1eXItHrb6hd6HH_QsYXGgPIPQs3baLLsu6VEDRgTAa366YjGT8_Pg6mHganNtNDl1zRh289wYu6-uWpfo65azYNzqf2D';
    	insert server;

        List<id> jobList = new List<id>();
        
        TRHC__Job__c job = new TRHC__Job__c();
        job.TRHC__Stage__c = 'closed';
        job.TRHC__Hiring_Manager__c = ContactList[0].id;
        job.CreatedDate = Date.today();
        job.TRHC__Number_of_Placements__c = 1;
        insert job;

        Notification__c notifi = new Notification__c();
        notifi.Contact__c = ContactList[0].id;
        notifi.Job__c = job.id;
        insert notifi;

        jobList.add(job.id);
        
		FireBasePushNotificationClass.postHTTPFireBaseClass(jobList);

		Test.setMock(HttpCalloutMock.class, new MockHttpResponseFireBasePush ());
		
		
		
    }
}