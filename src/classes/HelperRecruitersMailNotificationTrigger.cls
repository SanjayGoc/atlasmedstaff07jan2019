/*
 *@author : chaitanya.b@targetrecruit.net
 *@desc   : Helper class for RecruitersMailNotificationTrigger
 */
public with sharing class HelperRecruitersMailNotificationTrigger {
	/*
	*@static method ;
	*@parameters : List<Contact>;
	*@Return type : void;
	*/
	public static void sendEmail(List < Contact > contacts) {
		//query to get the record type id of candidate
		ID cid = [SELECT id from RecordType where Name = 'Candidate'].Id;
		//query to get the emailtemplate by name
		EmailTemplate et = [Select id from EmailTemplate where name = : 'Contacted Recruiter HTML'];
		//variable to hold all the emails
		List < Messaging.SingleEmailMessage > emails = new List < Messaging.SingleEmailMessage > ();
		for (Contact con: contacts) {
			//Old values of the contact
			Contact conOld = (Contact) Trigger.oldMap.get(con.id);
               ID Id = System.Label.SiteGuestUserId;
			if (conOld.Recruiters__c == null && con.Recruiters__c != null && con.OwnerId == Id) {
				//query to fetch the details of the contacta(recruiter) by name 
				List < User > recContact = [
						select id, Email 
						from User 
						where Name = : con.Recruiters__c];
				//Messaging object instantiation 
				Messaging.SingleEmailMessage singleMail = new Messaging.SingleEmailMessage();
				//setting the target object
				singleMail.setTargetObjectId(con.Id);
				//setting template id 
				singleMail.setTemplateId(et.Id);

				singleMail.setSaveAsActivity(false);
				singleMail.setTreatTargetObjectAsRecipient(FALSE);
				//by that reruiter name how many contacts are exist
				if (recContact.size() == 1) {
				singleMail.toAddresses = new String[] {
				recContact[0].id
				};
				emails.add(singleMail);
				} else {
				List < String > sendTo = new List < String > ();
				sendTo.add('contactus@atlasmedstaff.com');
				singleMail.setToAddresses(sendTo);
				emails.add(singleMail);
				}

			} else {
				if (con.Recruiters__c == null && conOld.TRHC__Permanent_City__c == null && con.TRHC__Permanent_City__c != null 
						&&  con.Permanent_State_Province1__c!=null && conOld.Permanent_State_Province1__c==null
						&& con.TRHC__Permanent_Zip_Postal_Code__c !=null && conOld.TRHC__Permanent_Zip_Postal_Code__c==null ) {
					Messaging.SingleEmailMessage singleMail = new Messaging.SingleEmailMessage();
					singleMail.setTargetObjectId(con.Id);
					singleMail.setTemplateId(et.Id);
					singleMail.setSaveAsActivity(false);
					singleMail.setTreatTargetObjectAsRecipient(FALSE);
					List < String > sendTo = new List < String > ();
					sendTo.add('contactus@atlasmedstaff.com');
					singleMail.setToAddresses(sendTo);
					emails.add(singleMail);
				}
			}
		}
		//sending mails
		if(emails.size()>0)
		Messaging.sendEmail(emails);
	}
}