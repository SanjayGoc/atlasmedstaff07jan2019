global with sharing class FluSeasonEmailBatch implements Database.Batchable<TRHC__Placement__c> {
    
    public Id specificId;
    public List<TRHC__Placement__c> start(Database.BatchableContext ctx){
        List<TRHC__Placement__c> placements = new List<TRHC__Placement__c>();
        if(specificId != null){
            placements = [
                Select id, TRHC__Contact_Candidate__r.Email, TRHC__Contact_Candidate__r.Name 
                From TRHC__Placement__c 
                Where TRHC__Status__c = 'Active' And id =:specificId]; 
        }else{
            placements = [
                Select id, TRHC__Contact_Candidate__r.Email, TRHC__Contact_Candidate__r.Name 
                From TRHC__Placement__c 
                Where TRHC__Status__c = 'Active'];
        }
        return placements;
    }
    public void execute(Database.BatchableContext ctx, List<TRHC__Placement__c> plcs){
        List<Messaging.SingleEmailMessage> sMessages = new List<Messaging.SingleEmailMessage>();
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'info@atlasmedstaff.com'];
        List<StaticResource> objPDF = [Select body, name from StaticResource where Name = 'Attachments'];
        for(TRHC__Placement__c plc :plcs){
            Messaging.SingleEmailMessage mse = new Messaging.SingleEmailMessage();
            Messaging.EmailFileAttachment[] objEmailAttachments = new Messaging.EmailFileAttachment[1];
            Messaging.EmailFileAttachment objPDFAttachment = new Messaging.EmailFileAttachment();
            objPDFAttachment.setBody(objPDF[0].Body);
            objPDFAttachment.setFileName('Flu Consent.pdf');
            objEmailAttachments[0] = objPDFAttachment;
            mse.setFileAttachments(objEmailAttachments);
            mse.setSubject('Flu Season is Upon Us – Must Read');
            String[] toAddresses = new String[] {plc.TRHC__Contact_Candidate__r.Email};
                mse.setToAddresses(toAddresses);
            if ( owea.size() > 0 ) {
                mse.setOrgWideEmailAddressId(owea.get(0).Id);
            }
            String emailbody1 = '<html xmlns="http://www.w3.org/1999/xhtml">'+
                '<head>'+
                '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />'+
                '<title></title>'+
                '<meta name="viewport" content="width=device-width, initial-scale=1.0"/>'+
                '</head>'+
                '<body style="margin: 0; padding: 0;">'+
                '<table align="center" border="0" cellpadding="0" cellspacing="0" width="600">'+
                '<tr style = "border: 0;">'+
                '<td align="center" bgcolor=""  style="padding: 40px 0 30px 0;width:100%; border:0px;">'+
                '<img src="https://atlasmedstaff.secure.force.com/resource/1515851764000/Logo1" align="middle" alt="Creating Email Magic" width="150" height="115" style="display: block;">'+
                '</td>'+
                '</tr>'+
                '<tr style = "border: 0;font-family: arial;font-size: 12pt;"><td bgcolor="#ffffff" style="padding-left:30px;padding-right:30px; padding-bottom:10px;">'+
                '<p>Dear '+ plc.TRHC__Contact_Candidate__r.Name +',<br/><br/>'+
                'It’s that time again! Atlas is required to send in documentation of a flu vaccine to the facility you are currently working on assignment.'+
                'Please stop by and get your 2018-2019 flu vaccine - I do need this by 9/30/18.<br/><br/>'+
                
                '<div align="center"><img src="https://atlasmedstaff.secure.force.com/resource/1535479260000/Flu_Logo" width="150" height="115" style="display: block;"></div>'+'<br/><br/><br/>'+
                ' If you have Atlas insurance – BCBS will cover the cost of the flu shot.  You can go to a Walgreens, CVS, Walmart, etc.  If you don’t have '+
                ' Atlas insurance, your insurance should still cover the cost of the flu shot.  **Please make sure you have this before 10/01/2018. If your '+
                ' nsurance won’t cover the flu shot, they are running about $35.00. Please email compliance@atlasmedstaff.com and your recruiter the '+
                'flu documentation and receipt for reimbursement.  You may also have it completed in employee health at your current facility, so'+
                'please make sure you obtain documentation.<br/><br/><br/>'+
                'Each Flu shot documentation needs to include the following by completing the attached form:'+
                ' Name,'+
                ' Date given (must be current flu season),'+
                ' Signature of the person that gave the shot'+
                ' Lot # and expiration date,'+
                ' Site given (right/left arm)<br/><br/><br/>'+
                'Please feel free to send any questions to compliance@atlasmedstaff.com or reach out to your recruiter. Thank you for helping us keep'+
                'your file compliant!<br/><br/><br/>'+
                'Thank you! '+
                '</p></tr>'+
                '</table>'+
                '</body></html>';
            mse.setHtmlBody(emailbody1);
            sMessages.add(mse);
        } 
        Messaging.sendEmail(sMessages);
    }
    public void finish(Database.BatchableContext ctx){
        
    }
    
}