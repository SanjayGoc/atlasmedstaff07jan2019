public class objectController
{
    public String selectedObject {get; set;}
    public String selectedField {get; set;}
    public List<String> lstPickvals {get;set;}
    public Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
    public objectController() 
    {   
        selectedObject = 'none';
    }
    public List<SelectOption> getObjectNames() 
    {
        List<SelectOption> objNames = new List<SelectOption>();
        List<String> entities = new List<String>(schemaMap.keySet());
        entities.sort();
        objNames.add(new SelectOption('none', '-- Select -- '));
        for (Schema.SObjectType item1: schemaMap.values()) 
        {
            String name = item1.getDescribe().getName();
            if (!item1.getDescribe().isCustomSetting() && item1.getDescribe().getRecordTypeInfos().size() > 0 && item1.getDescribe().isCreateable() &&
                !name.containsignorecase('history') && !name.containsignorecase('tag') && !name.containsignorecase('share') && !name.containsignorecase('feed')) {
                objNames.add(new SelectOption(item1.getDescribe().getName(), item1.getDescribe().getLabel()));
            }
        }
        return objNames;
    }
    public void getSelctedObjectFields(){
        lstPickvals=new List<String>();
    }
    public List<SelectOption> getObjectFields() {
        List<SelectOption> fieldNames = new List<SelectOption>();
        fieldNames.add(new SelectOption('none', '-- Select -- '));
        if(selectedObject != 'none') 
        {
            Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
            Schema.SObjectType ObjectSchema = schemaMap.get(selectedObject);
            Map<String, Schema.SObjectField> fieldMap = ObjectSchema.getDescribe().fields.getMap();
            for(String fieldName : fieldMap.keySet()) 
            {  
                Schema.DisplayType fielddataType = fieldMap.get(fieldName).getDescribe().getType();
                if(fielddataType == Schema.DisplayType.PICKLIST || 
                    fielddataType == Schema.DisplayType.MultiPicklist)
                {
                    fieldNames.add(new SelectOption(fieldName, fieldName));
                }
            }
        }
        return fieldNames;
    } 

    public void piklistValues() 
    {
        if(selectedField != 'none')
        {
            System.debug('===selectedField==='+selectedField);
            lstPickvals=new List<String>();
            Schema.SObjectType targetType = Schema.getGlobalDescribe().get(selectedObject);//From the Object Api name retrieving the SObject
            Sobject Object_name = targetType.newSObject();
            Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
            Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
            Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
            List<Schema.PicklistEntry> pick_list_values = field_map.get(selectedField).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
            for (Schema.PicklistEntry a : pick_list_values) 
            { 
                lstPickvals.add(a.getValue());//add the value  to our final list
            }    
        }
        
    }
}