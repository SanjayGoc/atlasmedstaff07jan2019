/**
* @author  chaitanya ( chaitanya.b@targetrecruit.net )
* @date  05-03-2018
* @description  Controller is used to generate an pdf file by using vf page  ResumeTempltePdf
*/
global with sharing class ResumeTempltePdfController{
    
    public List<TRHC__Employment_History__c> employementHistories { get;set; }
    public List<TRHC__Credentials__c> credLicenses { get;set; }
    public List<TRHC__Credentials__c> credCertifications { get;set; }
    Public List<TRHC__Educational_History__c > educationHistories { get;set; }
    private static string conFields = '';
    public String dateTimeValue { get;private set; }
    public String candiId { get;set; }
    public contact con { get;set; }
    public String conCharted { get;set; }
    public string conCertifications { get;set; }
    /**
     * @Author chaitanya bollamreddi
     * @date 05-03-2018
     * @name getEmployeedetails
     * @description Candidate deatails like Employment History, Education Histories, 
     *      Credentials like certifications and candidate necessary iformation.
     * @Return void
     */
    public void getEmployeedetails(){
        dateTimeValue = System.Now().format('MM/dd/yy HH:mm a', 'CST');
        employementHistories = new List< TRHC__Employment_History__c > ();
        educationHistories = new List< TRHC__Educational_History__c > ();
        credCertifications = new List<TRHC__Credentials__c>();
        credLicenses = new List<TRHC__Credentials__c>();
        
        candiId = ApexPages.currentPage().getParameters().get('Id');
        if(conFields == null || conFields == ''){
            Map<String, Schema.SObjectField> conMap= Schema.SObjectType.Contact.fields.getMap();
            for(String conField :conMap.keySet()){
                conFields = conFields +  conField + ' ,' ;
            }
        }    
        String conQuery =
            ' SELECT '+
            conFields.removeEnd(',')+
            ' FROM CONTACT'+
            ' WHERE ID = '+'\''+candiId+'\''+
            ' Limit 1';
        system.debug('conQuery @@@'+conQuery);
        con = Database.query(conQuery);
        /* Request from client */
        if(con.EMR__c!=null && con.EMR__c!='')
        conCharted = String.valueOf(con.EMR__c).replace(';',' ;');
        if(con.TRHC__Certifications__c != null && con.TRHC__Certifications__c != '')
        conCertifications = String.valueOf(con.TRHC__Certifications__c).replace(';','; ');
        /* Request from client */
        
        
        //Cache.OrgPartition orgPart = Cache.Org.getPartition('local.AtlasCandidateCoverage');
        Cache.SessionPartition orgPart = Cache.Session.getPartition('local.medstaffcache');

        if(orgPart.get('credCertifications')!= null && orgPart.contains('credCertifications')){
            system.debug('comming');
            credCertifications = (List<TRHC__Credentials__c>)orgPart.get('credCertifications');
            orgPart.remove('credCertifications');
        }
        if(orgPart.get('credLicenses')!= null && orgPart.contains('credLicenses')){
            system.debug('comming');
            credLicenses = (List<TRHC__Credentials__c>)orgPart.get('credLicenses');
            orgPart.remove('credLicenses');
        }
        if(orgPart.get('educationHistories')!= null && orgPart.contains('educationHistories')){
            system.debug('comming');
            educationHistories = (List<TRHC__Educational_History__c>)orgPart.get('educationHistories');
            orgPart.remove('educationHistories');
        }
        if(orgPart.get('employementHistories')!= null && orgPart.contains('employementHistories')){
            system.debug('comming');
            employementHistories = (List<TRHC__Employment_History__c>)orgPart.get('employementHistories');
            orgPart.remove('employementHistories');
        }
        
    }
}