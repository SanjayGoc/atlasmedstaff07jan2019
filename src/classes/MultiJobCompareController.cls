global class MultiJobCompareController{

    //public String job { get; set; }
global List<TRHC__JOb__c> selectedJobList {get;set;}
global Contact contact {get; set;}
global String cmsCssCustom { get; set;}

private static Map<String, Schema.SObjectField> avtrrtMap =
            Schema.SObjectType.TRHC__Job__c.fields.getMap();

global void SelectedJobs() {

           cmsCssCustom = TRHC.CMSHelper.getCSSStyles();
           selectedJobList = new List<TRHC__JOb__c>();
            
            contact = TRHC.CMSHelper.getLoggedInContact();

            //selectLanguage = UserInfo.getLocale();
            //cmsCssCustom = CMSHelper.getCSSStyles();

            //Logger.addMessage('JobDetailControllerNew', 'init', 'cmsCssCustom=' + cmsCssCustom);

            List<Schema.SObjectField> mapValuesList = avtrrtMap.values();

            
            String queryString1 = '';
            String queryString2;
            String finalQuery;
            for (Integer i = 0; i < mapValuesList.size(); i++) {
                if (('' + mapValuesList[i]).startsWith('INVALID FIELD: '))
                            continue;
                queryString1 = queryString1+','+mapValuesList[i];
            }

            

            queryString2 = queryString1.subString(queryString1.indexOf(',') + 1, queryString1.length());

            

            String jobId = String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get('jobIds'));
            system.debug('jobId@@@@'+jobId);
            

            if (jobId != null && jobId != '') {
                List<String> jobIdList = jobId.split(';');
                system.debug('jobIdList@@@'+jobIdList);
                if (jobIdList.size() >1 ) {

                    finalQuery = 'SELECT ' + queryString2 +
                            ',TRHC__Job__c.TRHC__Recruiter__r.Email' +
                            ' FROM TRHC__Job__c WHERE id in:jobIdList';

                    
                    system.debug('finalQuery@@@'+finalQuery);
                    
                    selectedJobList = Database.query(finalQuery);
                    
                }
            
                
    }
    system.debug('selectedJobList@@@'+selectedJobList);
   
        }

   

}