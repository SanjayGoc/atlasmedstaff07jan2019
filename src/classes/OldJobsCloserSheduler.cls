global class OldJobsCloserSheduler implements schedulable {
    global void execute(SchedulableContext sc){
        OldJobsCloserBatch closerJobBatch = new OldJobsCloserBatch();
        Database.executeBatch(closerJobBatch, 5);
    }
}