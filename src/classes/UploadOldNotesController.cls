public class UploadOldNotesController {
    /*private static List<Note__b> oldNotesList; 

public UploadOldNotesController()
{
setoldNotesList(getOldNotes());
} 

public List<Note__b> getOldNotes()
{
return [SELECT Title__c, Note_Body__c , Related_Contact__c,
Last_Modified_Date__c, Date_Of_Created__c,Created_BY__c
FROM Note__b
WHERE Related_Contact__c =: ApexPages.currentPage().getParameters().get('cid')
];
}

public static List<Note__b> getoldNotesList()
{
return oldNotesList;
}

public static void setoldNotesList(List<Note__b> value)
{
oldNotesList = value;
}*/
    
    private List<Note__b> oldNotesList; 
    
    //private String sortDirection = 'DESC';
    //private String sortExp = 'Date_Of_Created__c';
    private String sortDirection = 'ASC';
    private String sortExp = 'Related_Contact__c';

    
    public String sortExpression
    {
        get
        {
            return sortExp;
        }
        set
        {
            //if the column is clicked on then switch between Ascending and Descending modes
            if (value == sortExp)
                sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
            else
                sortDirection = 'ASC';
            sortExp = value;
        }
    }
    
    public String getSortDirection()
    {
        //if not column is selected 
        if (sortExpression == null || sortExpression == '')
            return 'ASC';
        else
            return sortDirection;
    }
    
    public void setSortDirection(String value)
    {  
        sortDirection = value;
    }
    
    public pagereference getOldNotes()
    {
        List<Note__b> bigNotesSorted = new List<Note__b>();
        string sortFullExp = sortExpression  + ' ' + sortDirection;
        system.debug('sortFullExp'+sortFullExp);
        system.debug('exp'+'SELECT Title__c, Note_Body__c , Related_Contact__c, Last_Modified_Date__c, Date_Of_Created__c,Created_BY__c FROM Note__b WHERE Related_Contact__c = '+'\''+ApexPages.currentPage().getParameters().get('cid')+'\''+' order by '+sortFullExp);
        if(oldNotesList.size()>0){
            if(sortExpression == 'Date_Of_Created__c'){
                sortByCreatedDate();
            }
            if(sortExpression == 'Title__c'){
                sortByTitle();
            }
            if(sortExpression == 'Note_Body__c'){
                sortByBody();
            }
            if(sortExpression == 'Related_Contact__c'){
                sortByContact();
            }
            if(sortExpression == 'Created_BY__c'){
                sortByCreatedUser();
            }
        }else{
            oldNotesList = Database.query('SELECT Title__c, Note_Body__c , Related_Contact__c, Last_Modified_Date__c, Date_Of_Created__c,Created_BY__c FROM Note__b WHERE Related_Contact__c = '+'\''+ApexPages.currentPage().getParameters().get('cid')+'\''+' order by '+sortFullExp );
        }
        
        return null;
    }
    
    public List<Note__b> getoldNotesList()
    {
        return oldNotesList;
    }
    
    public void setoldNotesList(List<Note__b> value)
    {
        oldNotesList = value;
    }
    
    public List<Note__b> sortByCreatedDate(){
        return null;
    }
    
    public List<Note__b> sortByTitle(){
        return null;
    }
    
    public List<Note__b> sortByBody(){
        return null;
    }
    
    public List<Note__b> sortByCreatedUser(){
        return null;
    }
    
    public List<Note__b> sortByContact(){
        return null;
    }
    
    
}