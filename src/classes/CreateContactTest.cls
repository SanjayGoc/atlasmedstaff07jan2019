@isTest
public class CreateContactTest {
    @isTest static void contactTestMethod() {
        
        String JSONMsg = '{"crtLists" : {"firstName": "Ankur","lastName": "Garg","phoneNumber": "123456789","email":"test@test.com","about": "Admin","discipline": "RN","primarySpecialty": "Cath Lab RN","primarySpecialtyYears": 300,"secondarySpecialty": "Cath Lab RN","secondarySpecialtyYears": 200,"startDate": "2018-11-01","targetWeeklyPay": "","shiftPreference": "12 hour nights","homeState": "Ohio","documentLists": [{"type" : ["resume"],"firstName" : "documentFile","fileStr" : "https://nursefly-prod-uploads.s3.amazonaws.com/uploads%2F90c1f6c4-bdc3-4632-b2fc-4943cbeb94ea%2F%2F1ohMmrxosc8bU4B%2FIMG_0003.JPG"}],"jobList": [{"id" : "1234576789","hospitalName": "Taj","hospitalCity": "NJ","hospitalState": "Ohio"}],"recuiterList": [{"firstName": "Ankur","lastName": "Garg","email": "ankur.garg@gmail.com"}]}}';
        
        Test.startTest();
        
        TRHC__Config_Settings__c config = new TRHC__Config_Settings__c();
        config.name = 'Default';
        config.Enable_RefferalTrigger__c = true;
        config.Enable_ContactAttachFile_trigger__c = true;
        insert config;
        
        //Create Wrapper Data
        List<String> typesList = new List<String>();
        typesList.add('resume');
        List<CreateContact.CreateDocument> docWrapperList = new List<CreateContact.CreateDocument>();
        CreateContact.CreateDocument docWrapper = new CreateContact.CreateDocument();
        docWrapper.firstName = 'Test Doc';
        docWrapper.type = typesList;
        docWrapper.fileStr = 'https://nursefly-prod-uploads.s3.amazonaws.com/uploads%2F90c1f6c4-bdc3-4632-b2fc-4943cbeb94ea%2F%2F1ohMmrxosc8bU4B%2FIMG_0003.JPG';
        docWrapperList.add(docWrapper);

        List<CreateContact.CreateRecuiter> recWrapperList = new List<CreateContact.CreateRecuiter>();
        CreateContact.CreateRecuiter recWrapper = new CreateContact.CreateRecuiter();
        recWrapper.firstName = 'Test';
        recWrapper.lastName = 'Contact';
        recWrapper.email = 'test@testgmailgmail123.com';
        recWrapperList.add(recWrapper);
        
        CreateContact.CreateDetails conWrapper = new CreateContact.CreateDetails();
        conWrapper.firstName = 'Test';
        conWrapper.lastName = 'Contact';
        conWrapper.email = 'test@testgmailgmail123.com';
        conWrapper.about = 'Test Description';
        conWrapper.phoneNumber = '123456789';
        conWrapper.discipline = 'RN';
        conWrapper.primarySpecialty = 'Cath Lab RN';
        conWrapper.primarySpecialtyYears = 50;
        conWrapper.secondarySpecialty = 'Cath Lab RN';
        conWrapper.secondarySpecialtyYears = 50;
        conWrapper.targetWeeklyPay = '1800.00';
        conWrapper.startDate = '2018-11-10';
        conWrapper.homeState = 'Ohio';
        conWrapper.documentLists = docWrapperList;
        conWrapper.recuiterList = recWrapperList;
        List<String> shiftList = new List<String>{'12 hour nights'};
        conWrapper.shiftPreference = shiftList;
        List<String> locationList = new List<String>{'12'};
        conWrapper.licenseLocations = locationList;
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/NurseFlyer/';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(JSONMsg);
        
        RestContext.request = req;
        RestContext.response= res;
        
        CreateContact.createContactMethod(conWrapper);
        Test.StopTest();   
    }
}