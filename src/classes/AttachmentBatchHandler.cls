global class AttachmentBatchHandler implements Database.Batchable<sObject>, Database.AllowsCallouts {
    public List<Attachment> attachmentList;
    public List<Attachment__c> contactAttachList;
    
    public AttachmentBatchHandler(Set<Id> attachIds) {
        contactAttachList = [SELECT Id, Name, 
                                Contact_Nurse__c, Document_File_Source__c 
                                FROM Attachment__c WHERE Id In: attachIds];
        attachmentList = new List<Attachment>();
    }
    //start
    global List<Attachment__c> start(Database.BatchableContext BC) {
        return contactAttachList;
    }
    //execute
    global void execute(Database.BatchableContext BC, List<Attachment__c> contactAttachList) {
        Http h;
        HttpRequest req;
        HttpResponse res;
        Attachment attach;
        try {
            for(Attachment__c attachNurse : contactAttachList) {
                attach = new Attachment(); 
                attach.ParentId = attachNurse.Contact_Nurse__c;
                attach.Name = attachNurse.Name; 
                h = new Http();
                req = new HttpRequest();
                req.setEndpoint(attachNurse.Document_File_Source__c);
                req.setMethod('GET');
                req.setHeader('Content-Type', 'application/json'); // Optional
                req.setCompressed(true);
                req.setTimeout(60000);
                res = h.send(req);
                if(res.getStatusCode() == 200) {
                    attach.Body = res.getBodyAsBlob();
                }
                attachmentList.add(attach);
            }
            if(!attachmentList.isEmpty()) {
                insert attachmentList;
            }
        } catch (Exception e) {
            System.debug('===> Exception ' + e);
        }
    }
    //finish
    global void finish(Database.BatchableContext BC) {
    }  
}