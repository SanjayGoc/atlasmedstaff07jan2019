/*************************************************************
*       CreatedBy   : Hari S
*       Created Date: 7/20/2016
*       Description : 1.Controller is used to for portal skills checklist details display
                        2. Send assessment request to prophecy
                        3. Creates caregiver Id if not exists
***************************************************************/
global class ProphecyCheckListController {
    public String careGiverId;
    public Contact v_ContactSobj { get; set; }
    public String GetCountValueFromSearchCaregiverResponse { get; set; }
    public String GetIsActiveValueFromSearchCaregiverResponse { get; set; }
    public String GetCareGiverIdValueFromCreateCaregiverResponse { get; set; }
    public String ResponseOfSearchCaregivers { get; set; }
    public String ResponseOfCreateCaregivers { get; set; }
    public String ResponseOfSearchCaregiversForCountEqualsOne { get; set; }
    public String ResponseOfSearchCaregiversForIsActiveEqualsFalse { get; set; }
    public TRHC__Prophecy_Settings__c v_ProphecyCustomSettings { get; set; }
    public String resultantURL { get; set; }
    public Map< String, Object > responseURL { get; set; }
    public String resultantURLWithHttps { get; set; }
    public List<ProphecySendAssessments.DynamicAssessmentObject > AssessmentList {get; set;}
    public String selectLanguage { get; set; }
    public Contact contact { get; set; }
    public Contact realContact;
    public String oldPassword;
    public String cmsCssCustom { get; set; }
    public ProphecySendAssessments assessmentInstance = null;
    /*
        * Author : Hari
        * Initial method to display the assessments on page load
    */
    public void init() {
        AssessmentList = new List<ProphecySendAssessments.DynamicAssessmentObject >();
        realContact = TRHC.CMSHelper.getLoggedInContact();
        System.debug(':::::::::::' + realContact);
        if (Test.isRunningTest()) {
            realContact = NEW Contact ();
            Contact testContact1 = new Contact();
            testContact1.LastName = 'Tes11t';
            testContact1.TRHC__UserName__c = 'test1@test.com';
            testContact1.TRHC__Password__c = 'test';
            testContact1.TRHC__Prophecy_Caregiver_ID__c = '111233';
           // testContact1.SSN__c = 'test';
            testContact1.Email = 'sai231@test.com';
           // testContact1.Department = 'test';
           // testContact1.Driver_Licence_Number__c = '234123445';
            testContact1.Phone = '9876323210';
            insert testContact1;
            realContact = testContact1;
        }
        System.debug(':::::::::::' + realContact);
        contact = realContact.clone(false, false);
       
        if(!Test.isRunningTest()) {
            cmsCssCustom = TRHC.CMSHelper.getCSSStyles();
        }
        responseURL = NEW Map< String, Object > ();
        v_ProphecyCustomSettings=TRHC__Prophecy_Settings__c.getinstance(UserInfo.getUserId());
        v_ContactSobj =[SELECT 
                           TRHC__Prophecy_Caregiver_ID__c
                              
                        FROM 
                            Contact 
                        WHERE
                            id =: realContact.id];
        careGiverId = v_ContactSobj.TRHC__Prophecy_Caregiver_ID__c;
        resultantUrl = '';
        resultantURLWithHttps = '';
        ApexPages.currentPage().getParameters().put('id',realContact.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(realContact);
        assessmentInstance = new ProphecySendAssessments();
      //  assessmentInstance.SelectedAssessmentType = 'Checklist';
        assessmentInstance.Showassessments();
       AssessmentList = assessmentInstance.AssessmentList;
    //    system.debug('AssessmentListAssessmentList' + assessmentInstance.AssessmentList);  
    resultantURLWithHttps = assessmentInstance.candidateByPassLoginURL; 
    assessmentInstance.SendAssess(); 
    }
    /*
        * Author : Hari
        * Method is used to call the prophecysendassessment to send the assessment details to 
                prophecy to initiate the assessments 
    */
    global pagereference processAssessment () {
        Boolean selectedAtLeastOne = false;
        for (ProphecySendAssessments.DynamicAssessmentObject indivAssessment: assessmentInstance.AssessmentList) {
            if (indivAssessment.selected == true) {
                selectedAtLeastOne = true;
            }
        }
        if (selectedAtLeastOne == true) {
            assessmentInstance.fromPortal = true;
            ApexPages.currentPage().getParameters().put('id',realContact.id);
            assessmentInstance.SendAssess();
            System.debug('aaaaaaaaaaaaaa'); 
            resultantURLWithHttps = assessmentInstance.candidateByPassLoginURL;
            System.debug('aaaaaaaaaaaaaa'+resultantURLWithHttps); 
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Checklist has been initiated successfullly');
            ApexPages.addMessage(myMsg);
            return null;
        } else {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Checklist has been initiated successfullly');
            ApexPages.addMessage(myMsg);
            return null;
        }
        
    }
    global void redirectToProphecy () {
        
    } 
}