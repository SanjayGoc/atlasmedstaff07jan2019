@isTest
public class NotifyContactCCandCMTest {
	@isTest
	public static void notifyRecruiterTest(){
		TRHC__Config_Settings__c config = new TRHC__Config_Settings__c(Name = 'Default');
		config.Enable_Update_Recruiter__c = FALSE;
		config.Enable_NotifyContactCCandCM__c = True;
		insert config;
		
		Profile adminProf = [select id from profile where name='System Administrator']; 
		User au = new User(alias = 'Admint', email='AdminUser@azugatask.com',emailencodingkey='UTF-8',FirstName='Admin',
				lastname='task',languagelocalekey='en_US',localesidkey='en_US',profileid = adminProf.Id,
				timezonesidkey='America/Los_Angeles',username='AdminUser@azugatask.com');
		insert au;
		Contact contact = new contact();
		contact.lastName = 'chai';
		contact.TRHC__Recruiter__c = au.id;
		insert contact;

		Account account = new Account();
		account.Name = 'Test';
		account.TRHC__Account_Manager__c = au.id;
		insert account;
		
		Task task = new Task();
		task.Subject = 'Atlas Adventures';
		task.OwnerId = au.id;
		task.WhoId = contact.id;
		task.status = 'Not Started';
		task.priority ='Normal';
		insert task;
		
		Task task1 = new Task();
		task1.Subject = 'Atlas Adventures';
		task1.WhatId = account.id;
		task1.status = 'Not Started';
		task1.OwnerId = au.id;
		task1.priority ='Normal';
		insert task1;
		test.startTest();
		task.status = 'Completed';
		update task;
		task1.status = 'Completed';
		update task1;
		test.stopTest();
	}
}