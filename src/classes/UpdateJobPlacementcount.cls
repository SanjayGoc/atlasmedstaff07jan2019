public class UpdateJobPlacementcount {
    private List<Id> jId { get;set; }
    private List<Id> pId { get;set; }
    Private Map<Id,list<TRHC__Placement__c>> placMap { get;set; }
    public void UpdateJobPlacementcount(List<TRHC__Placement__c> placments){
        placMap = new Map<Id,list<TRHC__Placement__c>> ();
        jid = new List<Id> ();
        pId = new List<Id> ();
        if(Placments != null && placments.size() >0)
        updateMap(placments);
        updateJobs(jId);
        List<TRHC__job__c> jobsToUpdate = new List<TRHC__job__c>();
        for(TRHC__job__c job :[SELECT Id, Name, TRHC__Number_of_Placements__c FROM TRHC__Job__c WHERE Id IN :jId]){
            if(job.TRHC__Number_of_Placements__c != placMap.get(job.id).size()){
                job.TRHC__Number_of_Placements__c = placMap.get(job.id).size();
                jobsToUpdate.add(job);
            }
        }
        Update jobsToUpdate;
    }
    public void updateJobs(List<ID> jobIds){
        List<TRHC__Placement__c> plcList = [SELECT id, TRHC__Job__c FROM TRHC__Placement__c WHERE TRHC__Job__c IN :jId AND id NOT IN :pId] ;
        if(plcList.size()>0)
        updateMap(plcList);
    }
    public void updateMap(List<TRHC__Placement__c> placments){
        for(TRHC__Placement__c plac :placments){
            pId.add(plac.id);
            jid.add(plac.trhc__job__c);
            if(placMap.get(plac.trhc__job__c)!= null){
                List<TRHC__Placement__c> placLists = PlacMap.get(plac.trhc__job__c);
                if(!placLists.contains(plac)){
                    placLists.add(plac);
                    placMap.put(plac.trhc__job__c, placLists);  
                }
            }
            else{
                List<TRHC__Placement__c> newList = new List<TRHC__Placement__c>();
                newList.add(plac);
                placMap.put(plac.trhc__job__c, newList);
            }
        }
    }
}