public with sharing class ProphecyExpiryNotificationCustom implements Database.batchable<sObject>{

    Public String query;

    public Database.QueryLocator start(Database.BatchableContext bc) {
        Date expiryNotificationDate = Date.today()+30; 
        String status = 'Active';
        query = 'SELECT TRHC__Contact__c, TRHC__Skill_Checklist_Name__c, ' +
        'Expiry_date__c ,TRHC__Skill_Answers__c.TRHC__Contact__r.id, TRHC__Skill_Answers__c.TRHC__Contact__r.Compliance_Coordinater__c, TRHC__Skill_Answers__c.TRHC__Contact__r.TRHC__Recruiter__c, TRHC__Skill_Answers__c.TRHC__Contact__r.Email, Expiration_Notification_Sent__c ' +
        'FROM TRHC__Skill_Answers__c  WHERE Expiry_date__c = :expiryNotificationDate '+ 
        ' And TRHC__Skill_Answers__c.TRHC__Contact__r.TRHC__Candidate_Status__c =:status';
        system.debug('query####'+query);
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext bc, List<TRHC__Skill_Answers__c> records) {
        system.debug('records' +records);
        if (records.size() > 0) {
            List < Messaging.SingleEmailMessage > emails = new List < Messaging.SingleEmailMessage > ();

            EmailTemplate et = [Select id from EmailTemplate where name = 'Skill Checklist Expiring Notification'];
            for (TRHC__Skill_Answers__c s : records) {

                Messaging.SingleEmailMessage singleMail = new Messaging.SingleEmailMessage();
                singleMail.setTargetObjectId(s.TRHC__Contact__c);
                singleMail.setTemplateId(et.Id);
                singleMail.setWhatId(s.id);
                singleMail.setSaveAsActivity(false);
                OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'info@atlasmedstaff.com'];
                if(owea.size()>0){
                    singleMail.setOrgWideEmailAddressId(owea.get(0).Id);
                    singleMail.setTreatTargetObjectAsRecipient(FALSE);
                }

                List<String> toAddList = new List<String>();

                if(s.TRHC__Contact__c != null) {
                    toAddList.add(s.TRHC__Contact__c);
                    if(s.TRHC__Contact__r.TRHC__Recruiter__c != null){
                        toAddList.add(s.TRHC__Contact__r.TRHC__Recruiter__c);
                    }
                    if(s.TRHC__Contact__r.Compliance_Coordinater__c != null){
                        toAddList.add(s.TRHC__Contact__r.Compliance_Coordinater__c);
                    }
                    singleMail.toAddresses = toAddList;
                }
                if(s.Expiration_Notification_Sent__c == false){
                    emails.add(singleMail);
                    s.Expiration_Notification_Sent__c = True;
                
                }
            }
            system.debug('=========== emails ==========='+emails);
            Messaging.sendEmail(emails);
            
            update records; 
        }
    }

    public void finish(Database.BatchableContext bc) {

    }
}