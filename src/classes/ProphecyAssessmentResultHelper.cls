public class ProphecyAssessmentResultHelper {
  public String Success;
  public String Count;
  public cls_Results[] Results;
    public class cls_Results {
    public String AssessmentID;
    public String AssessmentType;
    public String CaregiverID;
    public String CaregiverAssessmentID;
    public String DateStarted;
    public String DateFinished; 
    public String DateSent;
    public String OverallComprehension;
    public String OverallFrequency;
    public String RequestID;
    public String ResultsURL;
    public String Score;
    public String TotalCorrect;
    public String TotalQuestions;
    public String TotalTime;
  }
  public static ProphecyAssessmentResultHelper parse(String json){
    return (ProphecyAssessmentResultHelper) System.JSON.deserialize(json, ProphecyAssessmentResultHelper.class);
  }
}