/**
* @author : Chiatanya Bollamreddi
* @date : 19 April 2018
* @description : this class is used as test class for the
* 					 controller CandidateUpdateInformationController
*/
@isTest
public class CandidateUpdateInformationControllerTest {
    /**
* @author : Chaitanya Bollamreddi
* @date : 19 April 2018
* @name :contactFieldsTest
* @description : Static method
*/
    @isTest
    public static void contactFieldsTest(){
        Test.startTest();
        Atlas_Update_Info_req__c req = new Atlas_Update_Info_req__c();
        req.fieldsChosen__c = 'Id, Name, FirstName, LastName, Email, Phone, TRHC__Password__c, Discipline1__c, Primary_Speciality1__c, Temporary_Address_1__c, TRHC__Temporary_Street__c, TRHC__Temporary_City__c, TRHC__Temporary_State_Province__c, TRHC__Temporary_Zip_Postal_Code__c';
        req.filterConditions__c = 'recordtypeid = '+'\''+'01241000000y2YC'+'\''+'And TRHC__Candidate_Status__c = '+'\''+'Active'+'\'';
        req.Crypto__c = 'Atlasmedstaff123';
        req.status__c = 'Active';
        req.mailSubject__c = 'Quetionier';
        insert req;
        
        TRHC__Config_Settings__c setting = new TRHC__Config_Settings__c();
        setting.Name = 'Default';
        setting.DisabletrackEventActivityonContact__c = false;
        setting.DisabletrackTaskActivityonContact__c = false;
        setting.Enable_Update_Recruiter__c = false;
        setting.DisabletrackNoteonContact__c = false;
        setting.Trigger_Enable_Expected_End_Date_Changer__c = false;
        setting.Enable_Update_Recruiter__c = true;
        insert setting;
        Contact con = new Contact();
        con.LastName = 'Test';
        con.TRHC__Temporary_Street__c = 'Test';
        con.TRHC__Veteran_Status__c = 'Test';
        con.TRHC__Veteran_Status__c = 'Test';
        con.TRHC__Gender__c = 'Test';
        //con.Discipline1__c = 'Test';
        con.Phone = 'Test';
        con.Primary_Speciality1__c = 'ER';
        con.Email = 'Test@tr.com';
        //con.Secondary_Speciality__c = 'Test';
        con.HomePhone = 'Test';
        con.TRHC__Shift_Preference__c = 'Test';
        con.TRHC__Referral_Source__c = 'Test';
        //con.Recruiters__c = 'Test';
        con.TRHC__Permanent_Zip_Postal_Code__c = 'Test';
        con.TRHC__Permanent_City__c = 'Test';
        //con.State_pic__c = 'Test';
        con.Permanant_Address__c = 'Test';
        con.Birthdate = Date.newInstance(1991, 01, 23);
        con.TRHC__Temporary_City__c = 'Temporary_Address_1__c';
        //con.temporary_state__c = 'Temporary_Address_1__c';
        con.TRHC__Temporary_Zip_Postal_Code__c = 'Temporary_Address_1__c';
        con.SSN_Text__c = 'qwertyuiop1';
        con.TRHC__Password__c = 'abcdefg';
        con.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Candidate').getRecordTypeId();
        con.TRHC__Candidate_Status__c = 'Active';
        insert con;
        
        String key1 = 'Atlasmedstaff123';
        Boolean isencrypt = true;
        String password1 = AtlasCryptohelper.aes128(con.TRHC__Password__c, key1, isencrypt);
        
        CandidateUpdateInformationController controller = new CandidateUpdateInformationController();
        controller.password = password1;
        PageReference page = new PageReference('CandidateUpdateInformation');
        test.setCurrentPage(page);
        PageReference pg = ApexPages.currentPage();
        pg.getParameters().put('CandidateId',con.Id);
        controller.UpdateMypersonalInfoInIt();
        controller.updateMypersonalInfo();
        controller.UpdateRecord();
        
        SendReuestToUpdateInformationBatch batch = new SendReuestToUpdateInformationBatch();
        Database.executeBatch(batch);
        SendReuestToUpdateInformationSheduler srt = new SendReuestToUpdateInformationSheduler();
        String sch = '0 0 23 * * ?';
        system.schedule('Test Check', sch, srt);
        Test.stopTest();
        
    }
}