public With sharing class AssignmentWorkOrderPdfController{
    
    ApexPages.StandardController controller;
    TRHC__Placement__c placement;
    Public Decimal  perDiemValue { get; set; }
    public Decimal  housing { get; set; }
    
    public AssignmentWorkOrderPdfController(ApexPages.StandardController standardController) {
        controller = standardController;
        placement = new TRHC__Placement__c();
        placement = (TRHC__Placement__c)standardController.getRecord();
        perDiemValue = 0.00;
        housing = 0.00;
    }
    
    
    public void capturePlacementData(){
        List<String> expNameList = new List<String> ();
        expNameList.add('Per Diem');
        expNameList.add('Housing Stipend');
        List<TRHC__Expense__c> expList = [SELECT id, TRHC__Expense_Name__c, TRHC__Hourly_Amount__c, TRHC__Weekly_Amount__c FROM TRHC__Expense__c WHERE TRHC__Expense_Name__c IN :expNameList AND TRHC__Scratch_Sheet__c = :placement.TRHC__Scratch_Sheet__c];
        if(expList.size()>0){
            for(TRHC__Expense__c expense :expList){
                if(expense.TRHC__Expense_Name__c == 'Per Diem'){
                    perDiemValue = expense.TRHC__Weekly_Amount__c;
                }
                if(expense.TRHC__Expense_Name__c == 'Housing Stipend'){
                    housing = expense.TRHC__Weekly_Amount__c;
                }
            }
        }
    }
}