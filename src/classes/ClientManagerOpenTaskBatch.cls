global class ClientManagerOpenTaskBatch implements Database.Batchable<sObject> {
    
    String query;
    
    global ClientManagerOpenTaskBatch() {
        String clientManager = (!String.isBlank(System.Label.User_Title)) ? System.Label.User_Title : '';
        query = 'SELECT Id,Name, Title,Email FROM User WHERE Title =\''+clientManager+'\' AND isActive = true';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        List<Id> userIds = new List<Id>();
        for(User singleUser : (List<User>) scope) {
            userIds.add(singleUser.Id);
        }
        List<Task> allTasks = [SELECT Id,OwnerId,Owner.Email,Owner.Name,Status,Who.Name,What.Name,Subject,ActivityDate FROM Task WHERE OwnerId IN: userIds AND IsClosed = false];
        Map<Id,List<Task>> mapOfTask = new Map<Id,List<Task>>();
        for(Task singleTask : allTasks) {
            if(!mapOfTask.containsKey(singleTask.OwnerId)) {
                mapOfTask.put(singleTask.OwnerId,new List<Task>{singleTask});
            }else {
                mapOfTask.get(singleTask.OwnerId).add(singleTask);
            }
        }
        OrgWideEmailAddress[] orgWideEmailAdd = [SELECT Id ,Address from OrgWideEmailAddress where Address = 'info@atlasmedstaff.com'];
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        EmailTemplate emailTemplate = [SELECT Id,HtmlValue,Name,Subject FROM EmailTemplate WHERE DeveloperName =:'Client_Manager_Open_Task'];
        for(User singleUserTask : (List<User>) scope) {
            if(mapOfTask.containsKey(singleUserTask.Id)) {
                String newEmailBody = emailTemplate.HtmlValue;
                newEmailBody = newEmailBody.replace('{!User.Name}',singleUserTask.Name);
                String newEmailSubject = emailTemplate.Subject;
                newEmailSubject = newEmailSubject.replace('{!User.Name}',singleUserTask.Name);
                String html = '';
                Integer counter = 1;

                for(Task singleTaskOfUser : mapOfTask.get(singleUserTask.Id)) {
                    String dateFormat = (singleTaskOfUser.ActivityDate != null) ?singleTaskOfUser.ActivityDate.format() : '';
                    String hospitalName = (!String.isBlank(singleTaskOfUser.What.Name)) ? singleTaskOfUser.What.Name : '';
                    String candidateName = (!String.isBlank(singleTaskOfUser.Who.Name)) ? singleTaskOfUser.Who.Name : '';
                    html += '<tr align="center">'
                            +'<td style="border:2px solid black;">'+counter+'</td>'
                            +'<td style="border:2px solid black;">'+'<a href="'+ System.URL.getSalesforceBaseUrl().toExternalForm() + '/' + singleTaskOfUser.Id + '" target="_blank">'+singleTaskOfUser.Subject+'</a>'+'</td>'
                            +'<td style="border:2px solid black;">'+hospitalName+'</td>'
                            +'<td style="border:2px solid black;">'+candidateName+'</td>'
                            +'<td style="border:2px solid black;">'+dateFormat+'</td>';
                    html += + '</tr>';
                    counter++;
                }
                newEmailBody = newEmailBody.replace('{userTasks}',html);
                messaging.singleEmailMessage mail = new messaging.singleEmailMessage();
                mail.setOrgWideEmailAddressId(orgWideEmailAdd[0].Id);
                String[] toAddresses = new String[] {singleUserTask.Email};
                mail.setToAddresses(toAddresses);
                mail.setBccAddresses(new String[] {'Minkesh.patel@getoncrm.com'});
                mail.setSubject(newEmailSubject);
                mail.setHtmlBody(newEmailBody);
                mails.add(mail);
            }
        }
        if(!mails.isEmpty() && !Test.isRunningTest()) {
            Messaging.sendEmail(mails);
        }

    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
    
}