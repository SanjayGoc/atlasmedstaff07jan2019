/**
* @author Ranjeeth
* @date 10/4/2013
* @description Testclass for BackGroundCheckV2 class
*/

@isTest
private class TestBackGroundCheckV2 {
/**
* @description :Intialiazation Of Contact Eductaional  and Employment History
*/

    static testmethod void checkSize() {
        test.startTest();
        
        
        TRHC__Config_Settings__c acs = new TRHC__Config_Settings__c();
        acs.TRHC__Job_Applicant_field_Stage__c = 'sdsd';
        acs.name = 'true';
        acs.TRHC__Enable_Universal_Background_Check__c = true;
        insert acs;
        
        TRHC__Universals_Background_Check__c backgroundcheck = new TRHC__Universals_Background_Check__c();
        backgroundcheck.TRHC__Account_Number__c = '123';
        backgroundcheck.TRHC__Endpoint_URL__c = 'www.google.com';
        backgroundcheck.TRHC__Package__c = 'sun';
        backgroundcheck.TRHC__Partner_Reference__c = '123app';
        backgroundcheck.TRHC__User_Name__c = 'abc';
        insert backgroundcheck;
        Educational_Credentials__c newEC = new Educational_Credentials__c();
        newEC.name = 'Address';
        newEC.Field_Name__c = 'TRHC__Address__c';
        insert newEC;
        Educational_Credentials__c newEC1 = new Educational_Credentials__c();
        newEC1.name = 'Address1';
        newEC1.Field_Name__c = 'TRHC__Address__c+TRHC__Address__c';
        insert newEC1;
        Educational_Credentials__c newEC2 = new Educational_Credentials__c();
        newEC2.name = 'TRHC__Address__c=\"YYYY\"';
        newEC2.Field_Name__c = 'TRHC__Address__c';
        insert newEC2;
        TRHC__Credential_Verifications__c newCVRecord = new TRHC__Credential_Verifications__c();
        newCVRecord.name = 'Education';
        newCVRecord.TRHC__Credential_Name__c = 'Educational_Credentials__c';
        newCVRecord.TRHC__Object_Name__c = 'TRHC__Educational_History__c';
        newCVRecord.TRHC__Root_Element__c = 'School type=\"highschool\"';
        newCVRecord.TRHC__Root_Tag__c = 'EducationHistory';
        newCVRecord.TRHC__Where_Clause__c = 'TRHC__Candidate__c';
        insert newCVRecord;
        
        List<TRHC__Credential_Verifications__c> AVTRRT = TRHC__Credential_Verifications__c.getall().values();
        Contact newcontact = new Contact();
        newcontact.Lastname = 'l';
        newcontact.Birthdate = date.ValueOf('2011-11-21');
        insert newcontact;
        System.assertequals('l', newcontact.Lastname);
         
        TRHC__Employment_History__c employmenthistory = new TRHC__Employment_History__c();
        employmenthistory.TRHC__Candidate__c = newcontact.id;
        employmenthistory.TRHC__EmployerOrgName__c = 'test';
        insert employmenthistory;
        System.assertequals('test', employmenthistory.TRHC__EmployerOrgName__c);  
        
        TRHC__Educational_History__c educationalhistory = new TRHC__Educational_History__c();
        educationalhistory .TRHC__Candidate__c = newcontact.id;
        insert educationalhistory ;
             
        ApexPages.currentPage().getParameters().put('id', newcontact.id);
        ApexPages.StandardController con = new ApexPages.StandardController(newcontact);
        BackGroundCheckV2 cs = new BackGroundCheckV2(con);
        cs.init();
        test.stoptest();
    }
}