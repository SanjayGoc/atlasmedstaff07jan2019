/**
* @author Chaitanya Bollamreddi
* @date 06 April, 2018
* @description Btach Class for create task on Credentials Expiration On Daily Basis.
* @params List<Credentials> Credentials
*/

global class ExpirationTask implements Database.Batchable<sObject>{
    
    global List<TRHC__Credentials__c> start(Database.BatchableContext BC){
        Date todd = date.today();
        List<TRHC__Credentials__c> credeList = [SELECT ID, TRHC__Expiration_Date__c, TRHC__Contact__c, TRHC__Contact__r.Name, TRHC__Contact__r.Compliance_Coordinater__c, Name, TRHC__Credential_Master_Name__c  FROM TRHC__Credentials__c WHERE TRHC__Expiration_Date__c =:todd AND TRHC__Contact__r.Compliance_Coordinater__c != Null];
        return credeList;
    }
    /**
* @author Chaitanya Bollamreddi
* @date 06 April, 2018
* @description Create tasks for Candidates. Whos Credentils are expiring Today.
* @param List<Credentials> Credentials
*/
    global void execute(Database.BatchableContext BC, List<TRHC__Credentials__c> credentials){
        Set<Task> taskSet = new Set<Task>();
        Map<Id,Task> conTask = New Map<Id,Task>();
        for(TRHC__Credentials__c cred : credentials){
            if(cred.TRHC__Contact__r.Compliance_Coordinater__c != Null){
                if(conTask.get(cred.TRHC__Contact__c) != null){
                    Task task = conTask.get(cred.TRHC__Contact__c);
                    if(task.OwnerId == cred.TRHC__Contact__r.Compliance_Coordinater__c){
                        task.description += '<b><u>Credential Information</b></u> '+'\n'+'\n'+
                            'EmployeeName  -   '+cred.TRHC__Contact__r.Name+'\n'+'\n'+
                            'Credentials ID      -   '+cred.Name+'\n'+'\n'+
                            'Credential Master    -   '+cred.TRHC__Credential_Master_Name__c+'\n'+'\n'+
                            'Expiration Date	 -'+cred.TRHC__Expiration_Date__c+'\n'+'\n';
                        conTask.put(cred.TRHC__Contact__c,task);    
                    }
                }else{
                    Task task = new Task();
                    task.Whoid  = cred.TRHC__Contact__c;
                    task.WhatId = cred.Id;
                    task.Subject = 'Credential Expiring-'+cred.TRHC__Contact__r.Name;
                    task.priority= 'Normal';
                    task.status = 'Not Started';
                    task.description = '<b><u>Credential Information</b></u> '+'\n'+'\n'+
                        'EmployeeName  -   '+cred.TRHC__Contact__r.Name+'\n'+'\n'+
                        'Credentials ID      -   '+cred.Name+'\n'+'\n'+
                        'Credential Master    -   '+cred.TRHC__Credential_Master_Name__c+'\n'+'\n'+
                        'Expiration Date	 -'+cred.TRHC__Expiration_Date__c+'\n'+'\n';
                    task.OwnerId= cred.TRHC__Contact__r.Compliance_Coordinater__c;
                    conTask.put(cred.TRHC__Contact__c,task);
                }
            }
        }
        taskSet = new Set<task>(conTask.values());
        upsert new List<Task>(taskSet);
    }
    
    global void finish(Database.BatchableContext BC){
    }
}