@isTest
public class CandidateQuestionnaireExtnTest{
    static testmethod void CandidateQuestionnaireExtnTest(){
        
        Account testAccount = new Account();
        testAccount.Name='Test Account' ;
        insert testAccount;
        
        Contact testContact = new Contact();
        testContact.LastName='Test Contact ' ;
        insert testContact;
        
        Test.StartTest(); 
        ApexPages.StandardController sc = new ApexPages.StandardController(testContact);
        CandidateQuestionnaireExtn CQuestionnaire = new CandidateQuestionnaireExtn(sc);
        CQuestionnaire.save1();
        CQuestionnaire.submit();
        PageReference pageRef = Page.CandidateQuestionnaire;
        pageRef.getParameters().put('id', String.valueOf(testContact.Id));
        Test.setCurrentPage(pageRef);
        
        Test.StopTest();
    }
}