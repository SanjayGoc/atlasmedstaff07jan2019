@istest()
public class BatchToDeleteScratchSheetsTest {
	@istest()
    public static void scratchSheetTest(){
        
        TRHC__Config_Settings__c configSettings = new TRHC__Config_Settings__c();
        configSettings.name='Default';
        configSettings.Trigger_Enable_ScratchSheetAutoAddExp__c = true;
        configSettings.AutoAddExpenseCriteria__c='TRHC__Margin_Type__c=\'Expense\'';
        insert configSettings;
        String limitLabel = System.label.DaysLimitForScratchSheet;
        
		TRHC__Scratch_Sheet__c newScratchSheetTemp = new TRHC__Scratch_Sheet__c();
		newScratchSheetTemp.TRHC__County__c = 'Kalvin';
		newScratchSheetTemp.TRHC__State__c='California';
		newScratchSheetTemp.State_pic__c = 'California';
		newScratchSheetTemp.Name='Demo ScratchSheet';
		String scrtchSheetTempRecordTypeId1 = [Select Id From RecordType Where SobjectType = 'TRHC__Scratch_Sheet__c' and Name = 'Scratchsheet'].Id;
		newScratchSheetTemp.RecordTypeId=scrtchSheetTempRecordTypeId1 ;
        newScratchSheetTemp.TRHC__State__c='California';
        newScratchSheetTemp.LastModifiedDate = date.today()-40;
        newScratchSheetTemp.CreatedDate = date.today()-41;
		insert newScratchSheetTemp;
        test.startTest();
        BatchToDeleteScratchSheetsAndExpenses btch = new BatchToDeleteScratchSheetsAndExpenses();
        Database.executeBatch(btch);
        test.stopTest();
    }
}