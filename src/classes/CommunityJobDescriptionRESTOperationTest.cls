@isTest
public class CommunityJobDescriptionRESTOperationTest{
    public static testmethod void jobdes(){

        TRHC__Config__c configObj1 = new TRHC__Config__c();
        configObj1.TRHC__Name__c = 'check';
        configObj1.TRHC__Margin_Type__c = 'Expense';
        configObj1.TRHC__Frequency__c = 'Weekly';
        configObj1.TRHC__State__c =   'California';
        configObj1.TRHC__Include_in_payroll__c= True;
        configObj1.TRHC__Pay_Period_to_be_paid__c='First Check';
        configObj1.TRHC__Active__c = True;
        configObj1.TRHC__Value__c = '350.0';
        configObj1.TRHC__Grouping_Total__c ='Pay Burden';
        configObj1.TRHC__City__c = 'test';
        
        configObj1.RecordTypeId =[Select Id From RecordType Where SobjectType = 'TRHC__Config__c' and Name = 'Margin calculation Line Item'].Id;
        
        insert configObj1;

        TRHC__Config_Settings__c configSettings = new TRHC__Config_Settings__c();
        configSettings.name='Default';
        configSettings.Enable_AutoMateJobRates__c = true;
        configSettings.Trigger_Enable_ScratchSheetAutoAddExp__c = true;
        configSettings.AutoAddExpenseCriteria__c='TRHC__Margin_Type__c=\'Expense\'';
        configSettings.Except_Hospitals__c = 'Atlas Development';
        configSettings.Trigger_Enable_Expected_End_Date_Changer__c = false;
        configSettings.Trigger_Enable_Applicant_From_SCR_Sheet__c = true;
        configSettings.Update_Pay_Burden__c = false;
        configSettings.ExceptConfigIds__c = configObj1.Id;
        configSettings.Enable_RefferalTrigger__c = true;
        configSettings.TRHC__Enable_Geosearch_for_Account__c = false;
        configSettings.Nurse_Gross_Weekly_Pay__c = 'Test Custom Setting';
        insert configSettings;

        TRHC__Healthcare_Setting__c customSetting = NEW TRHC__Healthcare_Setting__c(Name = 'Default');
        customSetting.TRHC__Enable_GSA_Service_to_Update_M_I_Rate__c = true;
        insert customSetting;
        
        FireBaseServerKeyMobile__c server  = new FireBaseServerKeyMobile__c();
        server.Name = 'Default';
        server.serverKey__c = 'AAAAr1KJK-k:APA91bFeuugxXEKJwAEKQDbo7yQ76RXAzxH0wpsGAKr0Df6C1eXItHrb6hd6HH_QsYXGgPIPQs3baLLsu6VEDRgTAa366YjGT8_Pg6mHganNtNDl1zRh289wYu6-uWpfo65azYNzqf2D';
        insert server;

        List<Account> accountList = TestUtilityClass.getTestMultipleAccount();
        
        contact conobj = new contact();
        conobj.lastName = 'sharma';
        conobj.TRHC__Password__c = '12345678';
        conobj.Email = 'HIREN.GOC@GMail.com';
        conobj.apiToken__c = 'GUEST';
        insert conobj;

        TRHC__Scratch_Sheet__c newScratchSheetTemp = new TRHC__Scratch_Sheet__c();
        newScratchSheetTemp.TRHC__County__c = 'Kalvin';
        newScratchSheetTemp.TRHC__State__c='California';
        newScratchSheetTemp.State_pic__c = 'California';
        newScratchSheetTemp.Name='Demo ScratchSheet';
        String scrtchSheetTempRecordTypeId1 = [Select Id From RecordType Where SobjectType = 'TRHC__Scratch_Sheet__c' and Name = 'Scratchsheet'].Id;
        newScratchSheetTemp.RecordTypeId=scrtchSheetTempRecordTypeId1 ;
        newScratchSheetTemp.TRHC__State__c='California';
        newScratchSheetTemp.LastModifiedDate = date.today()-40;
        newScratchSheetTemp.CreatedDate = date.today()-41;
        newScratchSheetTemp.Nurse_Gross_weekly_pay__c = 851.00;
        insert newScratchSheetTemp;

        TRHC__Job__c joblist = new TRHC__Job__c();
        joblist.California_OT_After_8__c = true;
        joblist.TRHC__Account_Lookup__c = accountList[0].id;
        joblist.TRHC__County__c = 'india';
        joblist.TRHC__State__c = 'Wisconsin';
        joblist.TRHC__Job_Title__c = 'indiatest';
        joblist.TRHC__Job_City__c = 'Marshfield';
        joblist.state_Pic__c ='California';
        joblist.TRHC__Discipline__c = 'Tech';
        joblist.TRHC__County__c ='text';
        joblist.Speciality1__c = 'CVICU';
        joblist.TRHC__Hours_Per_Week__c = 36;
        joblist.TRHC__Job_Postal_Code__c = '54449';
        joblist.TRHC__Scratch_Sheet__c = newScratchSheetTemp.id;
        joblist.California_OT_After_8__c = false;
        insert joblist;

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/mobile/JobDescription';
        req.addParameter('apiToken', conobj.apiToken__c);
        req.addParameter('jobId',joblist.id);
        req.httpMethod = 'GET';

        RestContext.request = req;
        RestContext.response = res;

        CommunityJobDescriptionRESTOperation.ResponseHandler wrapperobj = CommunityJobDescriptionRESTOperation.CommunityJobDescriptionRESTOperation();

    }


    public static testmethod void jobdes1(){

        TRHC__Config__c configObj1 = new TRHC__Config__c();
        configObj1.TRHC__Name__c = 'check';
        configObj1.TRHC__Margin_Type__c = 'Expense';
        configObj1.TRHC__Frequency__c = 'Weekly';
        configObj1.TRHC__State__c =   'California';
        configObj1.TRHC__Include_in_payroll__c= True;
        configObj1.TRHC__Pay_Period_to_be_paid__c='First Check';
        configObj1.TRHC__Active__c = True;
        configObj1.TRHC__Value__c = '350.0';
        configObj1.TRHC__Grouping_Total__c ='Pay Burden';
        configObj1.TRHC__City__c = 'test';
        
        configObj1.RecordTypeId =[Select Id From RecordType Where SobjectType = 'TRHC__Config__c' and Name = 'Margin calculation Line Item'].Id;
        
        insert configObj1;

        TRHC__Config_Settings__c configSettings = new TRHC__Config_Settings__c();
        configSettings.name='Default';
        configSettings.Enable_AutoMateJobRates__c = true;
        configSettings.Trigger_Enable_ScratchSheetAutoAddExp__c = true;
        configSettings.AutoAddExpenseCriteria__c='TRHC__Margin_Type__c=\'Expense\'';
        configSettings.Except_Hospitals__c = 'Atlas Development';
        configSettings.Trigger_Enable_Expected_End_Date_Changer__c = false;
        configSettings.Trigger_Enable_Applicant_From_SCR_Sheet__c = true;
        configSettings.Update_Pay_Burden__c = false;
        configSettings.ExceptConfigIds__c = configObj1.Id;
        configSettings.Enable_RefferalTrigger__c = true;
        configSettings.TRHC__Enable_Geosearch_for_Account__c = false;
        configSettings.Nurse_Gross_Weekly_Pay__c = 'Test Custom Setting';
        insert configSettings;

        TRHC__Healthcare_Setting__c customSetting = NEW TRHC__Healthcare_Setting__c(Name = 'Default');
        customSetting.TRHC__Enable_GSA_Service_to_Update_M_I_Rate__c = true;
        insert customSetting;

        FireBaseServerKeyMobile__c server  = new FireBaseServerKeyMobile__c();
        server.Name = 'Default';
        server.serverKey__c = 'AAAAr1KJK-k:APA91bFeuugxXEKJwAEKQDbo7yQ76RXAzxH0wpsGAKr0Df6C1eXItHrb6hd6HH_QsYXGgPIPQs3baLLsu6VEDRgTAa366YjGT8_Pg6mHganNtNDl1zRh289wYu6-uWpfo65azYNzqf2D';
        insert server;

        List<Account> accountList = TestUtilityClass.getTestMultipleAccount();
        contact conobj = new contact();
        conobj.lastName = 'sharma';
        conobj.TRHC__Password__c = '12345678';
        conobj.Email = 'HIREN.GOC@GMail.com';
        conobj.apiToken__c = '1111111111111111';
        insert conobj;

        TRHC__Scratch_Sheet__c newScratchSheetTemp = new TRHC__Scratch_Sheet__c();
        newScratchSheetTemp.TRHC__County__c = 'Kalvin';
        newScratchSheetTemp.TRHC__State__c='California';
        newScratchSheetTemp.State_pic__c = 'California';
        newScratchSheetTemp.Name='Demo ScratchSheet';
        String scrtchSheetTempRecordTypeId1 = [Select Id From RecordType Where SobjectType = 'TRHC__Scratch_Sheet__c' and Name = 'Scratchsheet'].Id;
        newScratchSheetTemp.RecordTypeId=scrtchSheetTempRecordTypeId1 ;
        newScratchSheetTemp.TRHC__State__c='California';
        newScratchSheetTemp.LastModifiedDate = date.today()-40;
        newScratchSheetTemp.CreatedDate = date.today()-41;
        newScratchSheetTemp.Nurse_Gross_weekly_pay__c = 851.00;
        insert newScratchSheetTemp;

        TRHC__Job__c joblist = new TRHC__Job__c();
        joblist.California_OT_After_8__c = true;
        joblist.TRHC__Account_Lookup__c = accountList[0].id;
        joblist.TRHC__County__c = 'india';
        joblist.TRHC__State__c = 'Wisconsin';
        joblist.TRHC__Job_Title__c = 'indiatest';
        joblist.TRHC__Job_City__c = 'Marshfield';
        joblist.state_Pic__c ='California';
        joblist.TRHC__Discipline__c = 'Tech';
        joblist.TRHC__County__c ='text';
        joblist.Speciality1__c = 'CVICU';
        joblist.TRHC__Hours_Per_Week__c = 36;
        joblist.TRHC__Job_Postal_Code__c = '54449';
        joblist.TRHC__Scratch_Sheet__c = newScratchSheetTemp.id;
        joblist.California_OT_After_8__c = false;
        insert joblist;


        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/mobile/JobDescription';
        req.addParameter('apiToken', conobj.apiToken__c);
        req.addParameter('jobId',joblist.id);
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        CommunityJobDescriptionRESTOperation.ResponseHandler wrapperobj1 = CommunityJobDescriptionRESTOperation.CommunityJobDescriptionRESTOperation();
    }
}