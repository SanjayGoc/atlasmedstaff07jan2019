public with sharing class KPIPerformanceController {
    private Id placementRecTypeId;
    public Integer placementcount                                            {get; set;}
    private Map<String, Integer> jobtotalnumber;            
    private Map<String, Integer> jobnumberplacement; 
    private Map<String, Integer> jobMonthMap;    
    public list<KPIPerformanceRatioWrapper> kpiwrapper                       {get; set;}
    public list<TRHC__Placement__c> placementrec;                  
    private list<string> allmonthlistjob;
    public String newString{get;set;}
    public static final String STRING_FILLED = 'filled';


    public KPIPerformanceController () {
        placementcount = 0;
        jobnumberplacement = new Map<String, Integer>();
        allmonthlistjob = new list<string>();
        kpiwrapper = new list<KPIPerformanceRatioWrapper>();
        joborderpipeline();
        placementRecTypeId = Schema.SObjectType.TRHC__Placement__c.getRecordTypeInfosByName().get('Placement').getRecordTypeId();
    }

    /*//status vs job count
    public List<TotalJobVsClosedJob> getJobData () {
        List<TotalJobVsClosedJob> datajob = new List<TotalJobVsClosedJob>();
        //List<TRHC__Job__c> jobtotallist = new List<TRHC__Job__c>();
        AggregateResult[] groupedResults = [Select count(id)c, TRHC__Stage__c from TRHC__Job__c where TRHC__Stage__c = 'Open' or TRHC__Stage__c = 'Closed' group by TRHC__Stage__c];
        
        for(AggregateResult ar  : groupedResults) {
            datajob.add(new TotalJobVsClosedJob(Integer.valueOf(ar.get('c')), String.valueOf(ar.get('TRHC__Stage__c'))));
        }
        return datajob;
    }*/
    
     //job vs account chart
    /*public List<JobWithAccountName> getJobAccountName () {
        List<JobWithAccountName> jobwithAccount = new List<JobWithAccountName>();
        Set<String> accountids =  new Set<String>();
        for(AggregateResult groupResultAccount : [Select count(id)co, calendar_month(createdDate) accMonth,TRHC__Account_Lookup__c, TRHC__Account_Lookup__r.Name from TRHC__Job__c where TRHC__Account_Lookup__r.Name != null and TRHC__Account_Lookup__r.Name != ''  group by TRHC__Account_Lookup__r.Name,TRHC__Account_Lookup__c, calendar_month(createdDate)  order by count(id) DESC limit 5]) {
            
            accountids.add(string.valueof(groupResultAccount.get('TRHC__Account_Lookup__c')));

            system.debug('account>>>>>>>>>>>>>>>>'+groupResultAccount.get('co'));
            system.debug('account>>>>>>>>>>>>>>>>'+groupResultAccount);

            string monthVal = string.valueOf(groupResultAccount.get('accMonth'));
                string mon = (monthVal == '1') ? 'Jan':
                (monthVal == '2') ? 'Feb':
                (monthVal == '3') ? 'Mar':
                (monthVal == '4') ? 'Apr':
                (monthVal == '5') ? 'May':
                (monthVal == '6') ? 'Jun':
                (monthVal == '7') ? 'Jul':
                (monthVal == '8') ? 'Aug':
                (monthVal == '9') ? 'Sep':
                (monthVal == '01') ? 'Jan':
                (monthVal == '02') ? 'Feb':
                (monthVal == '03') ? 'Mar':
                (monthVal == '04') ? 'Apr':
                (monthVal == '05') ? 'May':
                (monthVal == '06') ? 'Jun':
                (monthVal == '07') ? 'Jul':
                (monthVal == '08') ? 'Aug':
                (monthVal == '09') ? 'Sep':
                (monthVal == '10') ? 'Oct':
                (monthVal == '11') ? 'Nov':
                (monthVal == '12') ? 'Dec':
                'Invalid Month';    
                                        //                        (Integer) ar.get('m'), (Integer) ar.get('c')
                //jobwithAccount.add(new JobWithAccountName(Integer.valueOf(groupResultAccount.get('co')),mon, String.valueOf(groupResultAccount.get('Name'))));
                    //Integer(groupResultAccount.get('co')),String.valueOf(groupResultAccount.get('accMonth')), String.valueOf(groupResultAccount.get('Name'))));
                //system.debug('jobwithAccount'+jobwithAccount);
        } 

        list<AggregateResult> statewithSpeciality = new List<AggregateResult>();
        
        statewithSpeciality = [Select count(id)co,calendar_month(createdDate) accMonth, TRHC__Account_Lookup__r.Name from TRHC__Job__c where TRHC__Account_Lookup__c in: accountids GROUP BY TRHC__Account_Lookup__r.Name, calendar_month(createdDate) order by calendar_month(createdDate),TRHC__Account_Lookup__r.name];
        
        Map<String,Integer> monthwiseAccountMap = new Map<string,Integer>();
        String key = ''; 
        system.debug('statewithSpeciality+++++++++>>'+statewithSpeciality);
        for(AggregateResult jobR : statewithSpeciality) {
            
            string monthVal = string.valueOf(jobR.get('accMonth'));
                string mon = (monthVal == '1') ? 'Jan':
                (monthVal == '2') ? 'Feb':
                (monthVal == '3') ? 'Mar':
                (monthVal == '4') ? 'Apr':
                (monthVal == '5') ? 'May':
                (monthVal == '6') ? 'Jun':
                (monthVal == '7') ? 'Jul':
                (monthVal == '8') ? 'Aug':
                (monthVal == '9') ? 'Sep':
                (monthVal == '01') ? 'Jan':
                (monthVal == '02') ? 'Feb':
                (monthVal == '03') ? 'Mar':
                (monthVal == '04') ? 'Apr':
                (monthVal == '05') ? 'May':
                (monthVal == '06') ? 'Jun':
                (monthVal == '07') ? 'Jul':
                (monthVal == '08') ? 'Aug':
                (monthVal == '09') ? 'Sep':
                (monthVal == '10') ? 'Oct':
                (monthVal == '11') ? 'Nov':
                (monthVal == '12') ? 'Dec':
                'Invalid Month';  
            jobwithAccount.add(new JobWithAccountName(Integer.valueOf(jobR.get('co')),mon, String.valueOf(jobR.get('Name'))));
            system.debug('jobwithAccount>>>>>>>>>>'+jobwithAccount);
        }
            
        //System.debug('======monthwiseAccountMap=======>' + monthwiseAccountMap.values());
        //System.debug('======monthwiseAccountMap=======>' + monthwiseAccountMap.values());
        //System.debug('======monthwiseAccountMap=======>' + monthwiseAccountMap.keyset());

        //jobwithAccount.add(Integer(monthwiseAccountMap.values()),String.valueOf(), String.valueOf());
        //system.debug('jobwithAccount'+jobwithAccount);

        return jobwithAccount;
    }*/
    
    public List<JobAcoountRelaMonth> getAccountMonthWiseJob () {
        List<JobAcoountRelaMonth> jobwithAccountMonth = new List<JobAcoountRelaMonth>();
        Set<String> accountids =  new Set<String>();
        for(AggregateResult groupResultAccount : [Select count(id)co, calendar_month(createdDate) accMonth,TRHC__Account_Lookup__c, TRHC__Account_Lookup__r.Name from TRHC__Job__c where TRHC__Account_Lookup__r.Name != null and TRHC__Account_Lookup__r.Name != ''  group by TRHC__Account_Lookup__r.Name,TRHC__Account_Lookup__c, calendar_month(createdDate)  order by count(id) DESC limit 5]) {
            
            accountids.add(string.valueof(groupResultAccount.get('TRHC__Account_Lookup__c')));

            //System.debug('account>>>>>>>>>>>>>>>>'+groupResultAccount.get('co'));
            //System.debug('account>>>>>>>>>>>>>>>>'+groupResultAccount);

            string monthVal = string.valueOf(groupResultAccount.get('accMonth'));
                string mon = (monthVal == '1') ? 'Jan':
                (monthVal == '2') ? 'Feb':
                (monthVal == '3') ? 'Mar':
                (monthVal == '4') ? 'Apr':
                (monthVal == '5') ? 'May':
                (monthVal == '6') ? 'Jun':
                (monthVal == '7') ? 'Jul':
                (monthVal == '8') ? 'Aug':
                (monthVal == '9') ? 'Sep':
                (monthVal == '01') ? 'Jan':
                (monthVal == '02') ? 'Feb':
                (monthVal == '03') ? 'Mar':
                (monthVal == '04') ? 'Apr':
                (monthVal == '05') ? 'May':
                (monthVal == '06') ? 'Jun':
                (monthVal == '07') ? 'Jul':
                (monthVal == '08') ? 'Aug':
                (monthVal == '09') ? 'Sep':
                (monthVal == '10') ? 'Oct':
                (monthVal == '11') ? 'Nov':
                (monthVal == '12') ? 'Dec':
                'Invalid Month';    
                                        //                        (Integer) ar.get('m'), (Integer) ar.get('c')
                //jobwithAccount.add(new JobWithAccountName(Integer.valueOf(groupResultAccount.get('co')),mon, String.valueOf(groupResultAccount.get('Name'))));
                    //Integer(groupResultAccount.get('co')),String.valueOf(groupResultAccount.get('accMonth')), String.valueOf(groupResultAccount.get('Name'))));
                ////System.debug('jobwithAccount'+jobwithAccount);
        } 

        list<AggregateResult> statewithSpeciality = new List<AggregateResult>();
        
        statewithSpeciality = [Select count(id)co,calendar_month(createdDate) accMonth, TRHC__Account_Lookup__c,TRHC__Account_Lookup__r.Name from TRHC__Job__c where TRHC__Account_Lookup__c in: accountids GROUP BY TRHC__Account_Lookup__r.Name,TRHC__Account_Lookup__c, calendar_month(createdDate) order by calendar_month(createdDate),TRHC__Account_Lookup__r.name];
        
        Map<String,Integer> monthwiseAccountMap = new Map<string,Integer>();
        String key = ''; 
        //system.debug('statewithSpeciality+++++++++>>'+statewithSpeciality);
        for(AggregateResult jobR : statewithSpeciality) {
            
            string monthVal = string.valueOf(jobR.get('accMonth'));
                string mon = (monthVal == '1') ? 'Jan':
                (monthVal == '2') ? 'Feb':
                (monthVal == '3') ? 'Mar':
                (monthVal == '4') ? 'Apr':
                (monthVal == '5') ? 'May':
                (monthVal == '6') ? 'Jun':
                (monthVal == '7') ? 'Jul':
                (monthVal == '8') ? 'Aug':
                (monthVal == '9') ? 'Sep':
                (monthVal == '01') ? 'Jan':
                (monthVal == '02') ? 'Feb':
                (monthVal == '03') ? 'Mar':
                (monthVal == '04') ? 'Apr':
                (monthVal == '05') ? 'May':
                (monthVal == '06') ? 'Jun':
                (monthVal == '07') ? 'Jul':
                (monthVal == '08') ? 'Aug':
                (monthVal == '09') ? 'Sep':
                (monthVal == '10') ? 'Oct':
                (monthVal == '11') ? 'Nov':
                (monthVal == '12') ? 'Dec':
                'Invalid Month';  

                system.debug('jobR>>>>>>>>>>>>>'+jobR);
            jobwithAccountMonth.add(new JobAcoountRelaMonth(mon,(Integer.valueOf(jobR.get('co'))),
                                                                (Integer.valueOf(jobR.get('co'))),
                                                                (Integer.valueOf(jobR.get('co'))),
                                                                (Integer.valueOf(jobR.get('co'))),
                                                                (Integer.valueOf(jobR.get('co'))),
                                                                (Integer.valueOf(jobR.get('co'))),
                                                                (Integer.valueOf(jobR.get('co'))),
                                                                (Integer.valueOf(jobR.get('co'))),
                                                                (Integer.valueOf(jobR.get('co'))),
                                                                (Integer.valueOf(jobR.get('co'))),
                                                                (Integer.valueOf(jobR.get('co'))),
                                                                (Integer.valueOf(jobR.get('co')))));
           //system.debug('jobwithAccount>>>>>>>>>>'+jobwithAccountMonth);
        }
        return jobwithAccountMonth;
    }


    public class JobAcoountRelaMonth {
        public string monthNamejob {get;set;}
        public Integer jan {get;set;}
        public Integer feb {get;set;}
        public Integer mar {get;set;}
        public Integer apr {get;set;}
        public Integer may {get;set;}
        public Integer jun {get;set;}
        public Integer jul {get;set;}
        public Integer aug {get;set;}
        public Integer sep {get;set;}
        public Integer oct {get;set;}
        public Integer nov {get;set;}
        public Integer dec {get;set;}

        JobAcoountRelaMonth(string monthNamejob, Integer jan, Integer feb, Integer mar, 
                                                Integer apr, Integer may, Integer jun, 
                                                Integer jul, Integer aug, Integer sep, 
                                                Integer oct, Integer nov, Integer dec) {
            this.monthNamejob = monthNamejob;
            this.jan = jan;
            this.feb = feb;
            this.mar = mar;
            this.apr = apr;
            this.may = may;
            this.jun = jun;
            this.jul = jul;
            this.aug = aug;
            this.sep = sep;
            this.oct = oct;
            this.nov = nov;
            this.dec = dec;
            
        }
    }
    //job with most of account number of placements
    /*public class JobWithAccountName {
        public Integer totalJobAccount   {get;set;}
        public String accDataMonthWise   {get;set;}
        public String accountjobName     {get;set;}
        JobWithAccountName(Integer totalJobAccount, String accDataMonthWise, String accountjobName) {
            this.totalJobAccount = totalJobAccount;
            this.accDataMonthWise = accDataMonthWise;
            this.accountjobName = accountjobName;
        }
    }*/

    public List<TotalJobByMonth> getDisplayStatusToTotalJob(){
        List<TotalJobByMonth> lstTotalJobsByMonth = new List<TotalJobByMonth>();
        List<TRHC__Job__c> lstTotalJobs = [Select Id,TRHC__Stage__c,CreatedDate from TRHC__Job__c where TRHC__Stage__c = 'Open' or TRHC__Stage__c = 'Closed'];
        if(lstTotalJobs != null && lstTotalJobs.size() > 0){
            Map<Integer,Integer> mapMonthToOpenJobCnt = new Map<Integer,Integer>();
            Map<Integer,Integer> mapMonthToClosedJobCnt = new Map<Integer,Integer>();
            
            for(TRHC__Job__c jb : lstTotalJobs){
                if(!mapMonthToOpenJobCnt.containsKey(jb.createdDate.Month()) && jb.TRHC__Stage__c == 'Open'){
                    mapMonthToOpenJobCnt.put(jb.createdDate.Month(),1);
                }else if(mapMonthToOpenJobCnt.containsKey(jb.createdDate.Month()) && jb.TRHC__Stage__c == 'Open'){
                    Integer cnt = mapMonthToOpenJobCnt.get(jb.createdDate.Month()) + 1;
                    mapMonthToOpenJobCnt.put(jb.createdDate.Month(),cnt);
                }
                
                if(!mapMonthToClosedJobCnt.containsKey(jb.createdDate.Month()) && jb.TRHC__Stage__c == 'Closed'){
                    mapMonthToClosedJobCnt.put(jb.createdDate.Month(),1);
                }else if(mapMonthToClosedJobCnt.containsKey(jb.createdDate.Month()) && jb.TRHC__Stage__c == 'Closed'){
                    Integer cnt = mapMonthToClosedJobCnt.get(jb.createdDate.Month()) + 1;
                    mapMonthToClosedJobCnt.put(jb.createdDate.Month(),cnt);
                }
            }
            
            for(Integer i = 1; i <= 12; i++ ){
                TotalJobByMonth tb = new TotalJobByMonth();
                tb.monthName = tb.mapMonthNumtoName.get(i);
                if(mapMonthToOpenJobCnt.containsKey(i)){
                    tb.openCount = mapMonthToOpenJobCnt.get(i);
                }else if(!mapMonthToOpenJobCnt.containsKey(i)){
                    tb.openCount = 0;
                }
                
                if(mapMonthToClosedJobCnt.containsKey(i)){
                    tb.closeCount = mapMonthToClosedJobCnt.get(i);
                }else if(!mapMonthToClosedJobCnt.containsKey(i)){
                    tb.closeCount = 0;
                }
                
                lstTotalJobsByMonth.add(tb);
            }
            System.debug(lstTotalJobsByMonth);
        }
        return lstTotalJobsByMonth;
    }
    
    public class TotalJobByMonth {
        public String monthName {get;set;}
        public Integer openCount {get;set;}
        public Integer closeCount {get;set;}
        
        public Map<Integer,String> mapMonthNumtoName {get;set;}
        
        public TotalJobByMonth() {
            mapMonthNumtoName = new Map<Integer,String>{1=>'Jan',2=>'Feb',3=>'Mar',4=>'Apr',5=>'May',6=>'Jun',7=>'Jul',8=>'Aug',9=>'Sep',10=>'Oct',11=>'Nov',12=>'Dec'};
        }
    }
    
    public List<JobWithLowestSpecialityCount> getSpecialityJobdataLowestCount () {
        List<JobWithLowestSpecialityCount> datajobSpecialityLowest = new List<JobWithLowestSpecialityCount>();

        List<TRHC__Job__c> jobTotalListSpeLow = new List<TRHC__Job__c>();
        set<Integer> jobCountSet = new Set<Integer>();
        Map<String,Integer> specialityMultiPickLow = new Map<String,Integer>();

        Schema.DescribeFieldResult fieldResultlow = Schema.sObjectType.TRHC__Job__c.fields.Speciality1__c.getSObjectField().getDescribe();
           List<Schema.PicklistEntry> pleLow = fieldResultlow.getPicklistValues();

           for(Schema.PicklistEntry f : pleLow) {        
                specialityMultiPickLow.PUT(F.getLabel(),0);   
                //system.debug('********PICK*******'+F.getLabel());     
            }

        jobTotalListSpeLow = [SELECT id,name, Speciality1__c FROM TRHC__Job__c];
        //system.debug('jobTotalListSpeLow'+TRHC__Job__c.Speciality1__c);
        Integer score = 0;
        //system.debug('0000'+jobTotalListSpe);
        jobTotalListSpeLow.sort();
        
        //system.debug('1111'+jobTotalListSpeLow);
        list<string> strLstLow;// = jobTotalListSpeLow.TRHC__Specialty__c.split(';');
        //system.debug('jobTotalListSpeLow'+jobTotalListSpeLow);

        for(TRHC__Job__c jobObj :jobTotalListSpeLow){
            if(jobObj.Speciality1__c != null ) {
                //system.debug('********value'+jobObj.Speciality1__c); 
                //system.debug('********id'+jobObj.id);
                strLstLow = jobObj.Speciality1__c.split(';');

                if(specialityMultiPickLow.containsKey(jobObj.Speciality1__c)) {
                    ////system.debug('jobObj'+strLst[0]);
                    score = specialityMultiPickLow.get(strLstLow[0]);
                    //system.debug('score'+score);
                    score ++;
                    specialityMultiPickLow.PUT(strLstLow[0],score);
                }
            }
        }


        for(String jobvalue :specialityMultiPickLow.keySet()) {
            jobCountSet.add(specialityMultiPickLow.get(jobvalue));           
        }

        List<Integer> jobCountlistLow = new List<Integer>();

        jobCountlistLow.addAll(jobCountSet);
        jobCountlistLow.sort();

        set<Integer> jobCountfinalListLow = new Set<Integer>();
        Integer loopendLow = jobCountlistLow.size();

        if(jobCountlistLow.size() > 5 ){
            loopendLow = 5; 
        }
        //system.debug('jobCountlistLow'+jobCountlistLow);
        //System.debug('jobCountfinalListLow'+jobCountfinalListLow);
        

        for(Integer i = 0; i < loopendLow; i++) {
            jobCountfinalListLow.add(jobCountlistLow.get(i));   
        }
        
        //System.debug('jobCountlistLow'+jobCountlistLow);
        //System.debug('jobCountfinalListLow'+jobCountfinalListLow);
        for(String jobvalue :specialityMultiPickLow.keySet()) {
            if(jobCountfinalListLow.contains(specialityMultiPickLow.get(jobvalue))){
                //system.debug('jobvalue'+jobvalue);
                datajobSpecialityLowest.add(new JobWithLowestSpecialityCount(specialityMultiPickLow.get(jobvalue),jobvalue));
            }   
        }
        return datajobSpecialityLowest;
    }

    //job vs speciality count TOP JOB
    public List<JobWithSpeciality> getSpecialityJobdata () {
        List<JobWithSpeciality> datajobSpeciality = new List<JobWithSpeciality>();

        List<TRHC__Job__c> jobTotalListSpe = new List<TRHC__Job__c>();
        set<Integer> jobCountSet = new Set<Integer>();
        Map<String,Integer> specialityMultiPick = new Map<String,Integer>();

        Schema.DescribeFieldResult fieldResult = Schema.sObjectType.TRHC__Job__c.fields.Speciality1__c.getSObjectField().getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

            for(Schema.PicklistEntry f : ple) {        
                specialityMultiPick.PUT(F.getLabel(),0);   
            }

        jobTotalListSpe = [SELECT id,name, Speciality1__c FROM TRHC__Job__c];
        Integer score = 0;
        jobTotalListSpe.sort();
        
        list<string> strLst;
        
        for(TRHC__Job__c jobObj :jobTotalListSpe){
            if(jobObj.Speciality1__c != null ) {
                strLst = jobObj.Speciality1__c.split(';');

                if(specialityMultiPick.containsKey(jobObj.Speciality1__c)) {
                    score = specialityMultiPick.get(strLst[0]);
                    score ++;
                    specialityMultiPick.PUT(strLst[0],score);
                }
            }
        }


        for(String jobvalue :specialityMultiPick.keySet()) {
            jobCountSet.add(specialityMultiPick.get(jobvalue));           
        }

        List<Integer> jobCountlist = new List<Integer>();

        jobCountlist.addAll(jobCountSet);
        jobCountlist.sort();
        
        Set<Integer> jobCountfinalList = new Set<Integer>();
        Integer loopend = 0;
        if(jobCountlist.size() > 5 ){
            loopend = jobCountlist.size() - 5;
        }

        for(Integer i = jobCountlist.size()-1; i >= loopend; i--) {
            jobCountfinalList.add(jobCountlist.get(i));   
        }
        //System.debug('jobCountfinalList'+jobCountfinalList);
        for(String jobvalue :specialityMultiPick.keySet()) {
          if(jobCountfinalList.contains(specialityMultiPick.get(jobvalue))){
                datajobSpeciality.add(new JobWithSpeciality(specialityMultiPick.get(jobvalue),jobvalue));
           }    
        }
        return datajobSpeciality;
    }

   

    //KPI performance month vs ratio
    public void joborderpipeline() {
        Id placementRecTypeId = Schema.SObjectType.TRHC__Placement__c.getRecordTypeInfosByName().get('Placement').getRecordTypeId();
        jobtotalnumber = new Map<String, Integer>();
        jobnumberplacement = new Map<String, Integer>();
        Set<Id> jobtoplacementsIds = new Set<Id>();
        List<Datetime> jobCreatedDateList = new List<Datetime>();
        List<Datetime> jobNumberPlacementList = new List<Datetime>();
        KPIPerformanceRatioWrapper singleKPIWrap;
        List<TRHC__Job__c> jobs = [select Id, 
                                        Name, 
                                        CreatedDate, 
                                        TRHC__Number_of_Placements__c 
                                        from TRHC__Job__c 
                                        where TRHC__Stage__c = 'closed' ORDER BY CreatedDate DESC limit 10000];
        for(TRHC__Job__c job : jobs) {
            jobtoplacementsIds.add(job.Id);
            jobCreatedDateList.add(job.CreatedDate); 
        }
        jobtotalnumber = getJobTotalByMonth(jobCreatedDateList);
        Set<string> allmonth = new set<string>(jobtotalnumber.keyset());
        List<String> sortingMonthList = new List<String>();
        List<TRHC__Placement__c> placementrec = [Select Id, Name, TRHC__Date_Accepted__c
                                                    from TRHC__Placement__c 
                                                    where (TRHC__Job__c  in : jobtoplacementsIds or TRHC__Status__c =: STRING_FILLED)
                                                    AND TRHC__Date_Accepted__c != null AND RecordTypeId =: placementRecTypeId ORDER BY CreatedDate DESC limit 10000];
        for(TRHC__Placement__c pla : placementrec) {
            jobNumberPlacementList.add(pla.TRHC__Date_Accepted__c);
        }
        jobnumberplacement = getJobTotalByMonth(jobNumberPlacementList);
        for(string key : jobnumberplacement.keySet()) {
            allmonth.add(key);
        }
        sortingMonthList.addAll(allmonth);
        sortingMonthList.sort();
        for(String month : sortingMonthList) {
            singleKPIWrap = new KPIPerformanceRatioWrapper(month.right(3),(jobtotalnumber.get(month) == null) ? 0 : jobtotalnumber.get(month),(jobnumberplacement.get(month) == null) ? 0 : jobnumberplacement.get(month));
            kpiwrapper.add(singleKPIWrap);
        }
        allmonthlistjob = new list<string>(allmonth);
    }

    //***** get data for month vs kpi ratio *****//
    private Map<String, Integer> getJobTotalByMonth(List<Datetime> jobCreatedDate) {
        jobMonthMap = new Map<String, Integer>();
        for(Datetime singleCreatedDate : jobCreatedDate) {
            Integer monthOfJob = singleCreatedDate.month();
            Integer mapTotal = 0;
            Integer totalJobs = 1; 
            if(monthOfJob == 1 ) {
                if(jobMonthMap.containsKey('01Jan')) {
                    mapTotal = jobMonthMap.get('01Jan') + 1;
                    jobMonthMap.put('01Jan',mapTotal);
                }else {
                    jobMonthMap.put('01Jan',totalJobs);
                }
            }
            else if(monthOfJob == 2) {
                if(jobMonthMap.containsKey('02Feb')) {
                    mapTotal = jobMonthMap.get('02Feb') + 1;
                    jobMonthMap.put('02Feb',mapTotal);
                }else{
                    jobMonthMap.put('02Feb',totalJobs);
                }
            }
            else if(monthOfJob == 3) {
                if(jobMonthMap.containsKey('03Mar')) {
                    mapTotal = jobMonthMap.get('03Mar') + 1;
                    jobMonthMap.put('03Mar',mapTotal);
                }else{
                    jobMonthMap.put('03Mar',totalJobs);
                }
            }
            else if(monthOfJob == 4) {
                if(jobMonthMap.containsKey('04Apr')) {
                    mapTotal = jobMonthMap.get('04Apr') + 1;
                    jobMonthMap.put('04Apr',mapTotal);
                }else{
                    jobMonthMap.put('04Apr',totalJobs);
                }
            }
            else if(monthOfJob == 5) {
                if(jobMonthMap.containsKey('05May')) {
                    mapTotal = jobMonthMap.get('05May') + 1;
                    jobMonthMap.put('05May',mapTotal);
                }else{
                    jobMonthMap.put('05May',totalJobs);
                }
            }
            else if(monthOfJob == 6) {
                if(jobMonthMap.containsKey('06Jun')) {
                    mapTotal = jobMonthMap.get('06Jun') + 1;
                    jobMonthMap.put('06Jun',mapTotal);
                }else{
                    jobMonthMap.put('06Jun',totalJobs);
                }
            }
            else if(monthOfJob == 7) {
                if(jobMonthMap.containsKey('07Jul')) {
                    mapTotal = jobMonthMap.get('07Jul') + 1;
                    jobMonthMap.put('07Jul',mapTotal);
                }else{
                    jobMonthMap.put('07Jul',totalJobs);
                }
            }
            else if(monthOfJob == 8) {
                if(jobMonthMap.containsKey('08Aug')) {
                    mapTotal = jobMonthMap.get('08Aug') + 1;
                    jobMonthMap.put('08Aug',mapTotal);
                }else{
                    jobMonthMap.put('08Aug',totalJobs);
                }
            }
            else if(monthOfJob == 9) {
                if(jobMonthMap.containsKey('09Sep')) {
                    mapTotal = jobMonthMap.get('09Sep') + 1;
                    jobMonthMap.put('09Sep',mapTotal);
                }else{
                    jobMonthMap.put('09Sep',totalJobs);
                }
            }
            else if(monthOfJob == 10) {
                if(jobMonthMap.containsKey('10Oct')) {
                    mapTotal = jobMonthMap.get('10Oct') + 1;
                    jobMonthMap.put('10Oct',mapTotal);
                }else{
                    jobMonthMap.put('10Oct',totalJobs);
                }
            }
            else if(monthOfJob == 11) {
                if(jobMonthMap.containsKey('11Nov')) {
                    mapTotal = jobMonthMap.get('11Nov') + 1;
                    jobMonthMap.put('11Nov',mapTotal);
                }else{
                    jobMonthMap.put('11Nov',totalJobs);
                }
            }
            else if(monthOfJob == 12) {
                if(jobMonthMap.containsKey('12Dec')) {
                    mapTotal = jobMonthMap.get('12Dec') + 1;
                    jobMonthMap.put('12Dec',mapTotal);
                }else{
                    jobMonthMap.put('12Dec',totalJobs);
                }
            }
        }
        return jobMonthMap;
    }

    //**************** Display Avg Placement To Respective month start******************//
    public List<AvgPlcaementToMonth> getCountAvgPlacemntPerMonth(){
        List<AvgPlcaementToMonth> lstAvgPlcmnts = new List<AvgPlcaementToMonth>();
        List<TRHC__Placement__c> lstPlacement = [select id, createddate, TRHC__Job__c, TRHC__Job__r.createddate from TRHC__Placement__c where createddate = this_year and TRHC__Job__c != null order by createddate asc];
        //system.debug('lstPlacement'+lstPlacement.size());

        if(lstPlacement != null && lstPlacement.size() > 0) {
            map<Integer,List<TRHC__Placement__c>> mapMonthToAvgPlcmnt = new Map<Integer,List<TRHC__Placement__c>>();
            for(TRHC__Placement__c plc : lstPlacement){
                if(plc.TRHC__Job__c != null && plc.TRHC__Job__r.CreatedDate != null){
                    if(!mapMonthToAvgPlcmnt.containsKey(plc.CreatedDate.Month())){
                        mapMonthToAvgPlcmnt.put(plc.CreatedDate.Month(), new List<TRHC__Placement__c>());
                    }
                    mapMonthToAvgPlcmnt.get(plc.CreatedDate.Month()).add(plc);
                    //system.debug('plc>>>>>>>>>>>>>'+plc);
                }
            }
            
            for(Integer i = 1; i<= 12 ; i++){
                AvgPlcaementToMonth avm = new AvgPlcaementToMonth();
                if(mapMonthToAvgPlcmnt.containsKey(i) && mapMonthToAvgPlcmnt.get(i) != null && mapMonthToAvgPlcmnt.get(i).size() > 0){
                    Integer count = 0;
                    for(TRHC__Placement__c pl : mapMonthToAvgPlcmnt.get(i)){
                        Date plcDate = Date.valueOf(pl.CreatedDate);
                        Date jobDate = Date.valueOf(pl.TRHC__Job__r.CreatedDate);
                        count += jobDate.daysBetween(plcDate);
                    }
                    
                    avm.monthName = avm.mapMonthNumtoName.get(i);
                    avm.avgPlacement = count/mapMonthToAvgPlcmnt.get(i).size();
                    //avm.avgPlacement = mapMonthToAvgPlcmnt.get(i).size();
                    //system.debug('jobmonth>>>>>'+mapMonthToAvgPlcmnt.get(i).size());
                    //system.debug('count>>>>>'+count);
                    //system.debug('Averageplacement>>>>>'+avm.avgPlacement);
                }else {
                    avm.monthName = avm.mapMonthNumtoName.get(i);
                    avm.avgPlacement = 0;
                }
                lstAvgPlcmnts.add(avm);
            }
        }
        return lstAvgPlcmnts;
    }
    
    //***** Wrapper class for Month Vs Average placement Chart *****//
    public class AvgPlcaementToMonth {
        public String monthName {get;set;}
        public Double avgPlacement {get;set;}
        public Map<Integer,String> mapMonthNumtoName {get;set;}
        
        public AvgPlcaementToMonth(){
            mapMonthNumtoName = new Map<Integer,String>{1=>'Jan',2=>'Feb',3=>'Mar',4=>'Apr',5=>'May',6=>'Jun',7=>'Jul',8=>'Aug',9=>'Sep',10=>'Oct',11=>'Nov',12=>'Dec'};
        }
    }
    
    //**************** Display Avg Placement To Respective month end******************//
    
    //***** Wrapper class for Month Vs Placement count Chart *****//
    public class MonthPlacementcount {
        public String placementForPeriod                      {get; set;}
        public String monthforplacement                       {get; set;}
        public Integer monthintegervalue                      {get; set;}
        public Integer countplac                              {get; set;}
        MonthPlacementcount(String placementForPeriod, String monthforplacement, Integer monthintegervalue, Integer countplac) {
            this.placementForPeriod = placementForPeriod;
            this.monthforplacement = monthforplacement;
            this.monthintegervalue = monthintegervalue;
            this.countplac = countplac;
        }
    }

   /* // Job Vs status of job chart wrapper class
    public class TotalJobVsClosedJob {
        public Integer totalJobChrt  {get;set;}
        public String statusjob     {get;set;}
        TotalJobVsClosedJob(Integer totalJobChrt, String statusjob) {
            this.totalJobChrt = totalJobChrt;
            this.statusjob = statusjob;
        }

    }*/

    //job with speciality chart with TOP JOB Value
    public class JobWithSpeciality {
        public Integer totalJobSpeciality  {get;set;}
        //public Integer totalJobSpecialityDesc {get;set;}
        public String specialityJob     {get;set;}
        JobWithSpeciality(Integer totalJobSpeciality, String specialityJob) {
            this.totalJobSpeciality = totalJobSpeciality;
            this.specialityJob = specialityJob;
            //this.totalJobSpecialityDesc = totalJobSpecialityDesc;
        }
    }

    //job with speciality chart with TOP JOB Value
    public class JobWithLowestSpecialityCount {
        //public Integer totalJobSpeciality  {get;set;}
        public  Integer totalJobSpecialityDesc {get;set;}
        public String specialityJobForLowestJob     {get;set;}
        JobWithLowestSpecialityCount(Integer totalJobSpeciality, String specialityJobForLowestJob) {
            //this.totalJobSpeciality = totalJobSpeciality;
            this.totalJobSpecialityDesc = totalJobSpeciality ;
            this.specialityJobForLowestJob = specialityJobForLowestJob;
            
        }
    }

    //***** Wrapper class for Month Vs KPI Ratio Chart *****//
    public class KPIPerformanceRatioWrapper{
        public String jobPlacementMonth                      {get; set;}
        public Integer totalJobs                             {get; set;}
        public Integer totalPlacements                       {get; set;}
        public Decimal ratio                                 {get; set;}
        KPIPerformanceRatioWrapper(String jobPlacementMonth, Integer totalJobs, Integer totalPlacements) {
            this.jobPlacementMonth = jobPlacementMonth;
            this.totalJobs = totalJobs;
            this.totalPlacements = totalPlacements;
            if(totalPlacements != null && totalJobs != null && totalPlacements != 0 && totalJobs != 0)
                this.ratio = ((Decimal.valueof(totalPlacements)/Decimal.valueof(totalJobs))*100).SetScale(2); //
            else 
                this.ratio = 0.00;
        }
    }

}