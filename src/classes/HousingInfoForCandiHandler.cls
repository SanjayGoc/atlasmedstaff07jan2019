public class HousingInfoForCandiHandler {

    public void AddHousingInformation(List<TRHC__Job__c> jobList) {
        Map<string,TRHC__Job__c> statesMap = new  Map<string,TRHC__Job__c>();
        Map<String,Housing__c> housingMap = new Map<String,Housing__c>();
        List<TRHC__Housing__c> housingList = new List<TRHC__Housing__c>();
        List<TRHC__Job__c> jobHousingList = new List<TRHC__Job__c>();
        Map<Id,TRHC__Job_Applicant__c> jobAppicantMap = new Map<Id,TRHC__Job_Applicant__c>();
        
        for(TRHC__Job__c job: jobList) {
            if(job.TRHC__Stage__c == 'filled'){
                statesMap.put(job.State_pic__c,job);
            }        
        }

        for(TRHC__Job_Applicant__c applicant : [select id,name,TRHC__Job__c,TRHC__Contact_Candidate__c,TRHC__Contact_Candidate__r.Name from TRHC__Job_Applicant__c where TRHC__Job__c in: statesMap.values()]){
            if(!jobAppicantMap.containsKey(applicant.TRHC__Job__c)){
                jobAppicantMap.put(applicant.TRHC__Job__c,applicant);
            }
        }

        List<Housing__c> nearHousingList = new List<Housing__c>();
        TRHC__Housing__c targetHousing;
        List<Housing__c> housingRecordList = new List<Housing__c>();
        Housing__c housingObj;
        for(Housing__c housing : [Select id,name,State__c,Housing_Name__c,Housing_Facility_Used_Time__c,Candidate_Used_Housing_Facilities__c,Housing_Location__Latitude__s,City__c,Zipcode__c,Street__c,Country__c,Type__c,Housing_Location__Longitude__s from Housing__c where  State__c in: statesMap.keyset()]) {
            if(housingMap.containsKey(housing.State__c)){
                
                Double latHospital = statesMap.get(housing.State__c).TRHC__Account_Lookup__r.BillingLatitude;
                Double lnghospital = statesMap.get(housing.State__c).TRHC__Account_Lookup__r.BillingLongitude;
                Location locHospital = Location.newInstance(latHospital,lnghospital);
                
                Double latiHousing = housing.Housing_Location__Latitude__s;
                Double longiHousing = housing.Housing_Location__Longitude__s;
                Location locHousing = Location.newInstance(latiHousing, longiHousing);
                
                Double dist = Location.getDistance(locHospital, locHousing, 'mi');

                //housingMap.get(housing.State__c);

                Double oldlatiHousing = housingMap.get(housing.State__c).Housing_Location__Longitude__s;
                Double oldlongiHousing = housingMap.get(housing.State__c).Housing_Location__Longitude__s;
                Location oldlocHousing = Location.newInstance(oldlatiHousing, oldlongiHousing);
                
                Double distold = Location.getDistance(locHospital, oldlocHousing, 'mi');

                if(dist < distold){
                    housingMap.put(housing.State__c,housing);
                }
            } else {
                housingMap.put(housing.State__c,housing); 
            }
        }
        
        for(TRHC__Job__c job : jobList){
            if(job.TRHC__Stage__c == 'filled' && housingMap.containskey(job.State_pic__c) && jobAppicantMap.containskey(job.id)){
                housingObj = housingMap.get(job.State_pic__c);
                
                string candidatename = jobAppicantMap.get(job.id).TRHC__Contact_Candidate__c;
                string candidateNameForHousing = jobAppicantMap.get(job.id).TRHC__Contact_Candidate__r.Name;
                if(housingObj.Housing_Facility_Used_Time__c == null){
                    housingObj.Housing_Facility_Used_Time__c = 1;
                }else{    
                housingObj.Housing_Facility_Used_Time__c = housingObj.Housing_Facility_Used_Time__c + 1;
                }
                if(housingObj.Candidate_Used_Housing_Facilities__c == null) {
                    housingObj.Candidate_Used_Housing_Facilities__c = candidateNameForHousing;
                } else {
                    housingObj.Candidate_Used_Housing_Facilities__c = housingObj.Candidate_Used_Housing_Facilities__c + ';' + candidateNameForHousing ;    
                }
                
                housingRecordList.add(housingObj);
                
                targetHousing = new TRHC__Housing__c();
                targetHousing.TRHC__Candidate__c = candidatename ;
                targetHousing.TRHC__Housing_Name__c = housingObj.Housing_Name__c;
                targetHousing.TRHC__Housing_Contact__c = job.TRHC__Hiring_Manager__c;
                targetHousing.TRHC__Account__c = job.TRHC__Account_Lookup__c;
                targetHousing.TRHC__State__c = job.State_pic__c;
                targetHousing.TRHC__PostalCode__c = housingObj.Zipcode__c;
                targetHousing.TRHC__Housing_Type__c = housingObj.Type__c;
                targetHousing.TRHC__Address1__c = housingObj.Street__c;
                targetHousing.TRHC__Country__c = housingObj.Country__c;
                targetHousing.TRHC__City__c = housingObj.City__c;
                targetHousing.TRHC__Job_Num__c = job.Id;
                housingList.add(targetHousing);
            }
            
            if(!housingRecordList.isEmpty()){
                update housingRecordList;
            }

            if(!housingList.isEmpty()){
                insert housingList;
            }
        }
    }
}