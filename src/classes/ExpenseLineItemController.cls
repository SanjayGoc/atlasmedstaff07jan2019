public class ExpenseLineItemController{

    public static Final String stemplaterecTypeId = Schema.SObjectType.TRHC__Scratch_Sheet__c.getRecordTypeInfosByName().get('ScratchSheet Template').getRecordTypeId();
       
    public List<TRHC__Expense__c> expenseList {get;set;}
    public String scrId{get;set;}
    public Boolean isDuplicate{get;set;}
    public Boolean hideExpenseButton{get;set;}
    public TRHC__Scratch_Sheet__c scratchObj;
    public String ExpenseToEdit {get;set;}
    public String editableFieds{get;set;}
    public List<String> amountColumns {get;set;}
    public List<String> columns {get;set;}
    public List<TRHC__Expense__c> expenseListRead{get;set;}
    private List<TRHC__Expense__c> lstExpenseDelete;
    public map<id,Decimal> getExpenseValuemap ;
    private String expenseOrder; // Minkesh patel | 04 Jan, 2019

    public ExpenseLineItemController(ApexPages.StandardController stdCon){
        scratchObj = (TRHC__Scratch_Sheet__c )stdCon.getRecord();
        getExpenseValuemap = new map<id,decimal>();
        scrId = scratchObj.Id;
        expenseList = new List<TRHC__Expense__c>();
        expenseListRead = new List<TRHC__Expense__c>();
        lstExpenseDelete = new List<TRHC__Expense__c>();
        amountColumns = new List<String>();
        columns = new List<String>();
        editableFieds ='';
        ExpenseToEdit = System.Label.Editable_Expense;
        hideExpenseButton = scratchObj.RecordTypeId != stemplaterecTypeId ? true: false;
        expenseOrder = System.Label.Expense_Order; // Minkesh patel | 04 Jan, 2019
    }
    
    public void init(){
        Set<String> setEditAccess = new set<String>();
        if(ExpenseToEdit!=Null){
            setEditAccess.addAll(ExpenseToEdit.Split(','));
        }
        If(!setEditAccess.isEmpty()){
            String query = getCreatableFieldsSOQL('TRHC__Expense__c','TRHC__Scratch_Sheet__c = \''+ scratchObj.Id + '\''+' AND TRHC__Expense_Name__c NOT IN:setEditAccess','TRHC__Config__r.Name');
            expenseListRead = Database.query(query);
            query = getCreatableFieldsSOQL('TRHC__Expense__c','TRHC__Scratch_Sheet__c = \''+ scratchObj.Id + '\''+' AND TRHC__Expense_Name__c  IN:setEditAccess','TRHC__Config__r.Name');
            expenseList = Database.query(query);
        }else{ 
            String query = getCreatableFieldsSOQL('TRHC__Expense__c','TRHC__Scratch_Sheet__c = \''+ scratchObj.Id + '\'','TRHC__Config__r.Name');
            expenseList = Database.query(query);
        }
        for(String f : geteditableColumns()){
            editableFieds += f + ',';
        }
        
        //  Start: Minkesh patel | 04 Jan, 2019
        List<ExpenseLineItemUtils.ExpenseLineItemWrapper> expenseWrapList = new List<ExpenseLineItemUtils.ExpenseLineItemWrapper>();
        Map<String,Integer> expenseordermap = new Map<String,Integer>();
        Integer idx = 1;
        if(expenseOrder != '' && expenseOrder != null){
            for(String name : expenseOrder.split(',')) {
                expenseordermap.put(name,idx);
                idx++;
            }
        }
        
        for(TRHC__Expense__c expeObj : expenseList){
            if(expeObj.TRHC__Value__c != null){
                getExpenseValuemap.put(expeObj.id,expeObj.TRHC__Value__c);
            }
            
            idx = expenseordermap.containskey(expeObj.TRHC__Expense_Name__c) ? expenseordermap.get(expeObj.TRHC__Expense_Name__c) : 0;
            expenseWrapList.add(new ExpenseLineItemUtils.ExpenseLineItemWrapper(expeObj,idx));
            
        }
        
        if(!expenseWrapList.isEmpty())
        {
            expenseWrapList.sort();
            
            expenseList.clear();
            for(ExpenseLineItemUtils.ExpenseLineItemWrapper obj : expenseWrapList) {
                
                expenseList.add(obj.expense);
            }
        }
        
        //  End: Minkesh patel | 04 Jan, 2019
        amountColumns = getamountColumns();
        columns = getColumns();
    }
    
    public void updateExpenseFromConfig(){
    
    }
    
    public void addRow(){ system.debug('==inside row==');
    TRHC__Expense__c objExpence = new TRHC__Expense__c(TRHC__Scratch_Sheet__c =scratchObj.Id);
    expenseList.add(objExpence);
    }
    
    public pageReference saveLines(){
        Boolean isUpdate;
        

        if(!expenseList.isEmpty()){

            //update expenseList;
            try{
                For(TRHC__Expense__c exObj : expenseList){
                    if(getExpenseValuemap.get(exObj.id) != exObj.TRHC__Value__c){
                        isUpdate = true;
                        
                        break;
                    } else {
                        isUpdate = false;
                    }
                }
                if(isUpdate){
                    update expenseList;

                    list<TRHC__Scratch_Sheet__c> scrList = new List<TRHC__Scratch_Sheet__c>();
                    list<TRHC__Expense__c> expanseList = new List<TRHC__Expense__c>();
                    expanseList = [select id,name,TRHC__Expense_Name__c,TRHC__Weekly_Amount__c from TRHC__Expense__c where TRHC__Scratch_Sheet__c =: scratchObj.id];
                    Decimal WeeklyAmount = 0;
                    for (TRHC__Scratch_Sheet__c scr: [select id,name,TRHC__Hours_Per_Week__c,TRHC__Pay_Rate__c,over_time__c,TRHC__Overtime_Pay_Rate__c from TRHC__Scratch_Sheet__c where id =: scratchObj.id ]) {
                     Decimal weekAmnt = 0;     
                        if(scr.TRHC__Hours_Per_Week__c!=null && scr.TRHC__Pay_Rate__c!=null){
                            weekAmnt  =  scr.TRHC__Hours_Per_Week__c * scr.TRHC__Pay_Rate__c;
                            If( scr.over_time__c!=null  && scr.TRHC__Overtime_Pay_Rate__c!=null){
                                weekAmnt  =  scr.TRHC__Hours_Per_Week__c * scr.TRHC__Pay_Rate__c + scr.over_time__c * scr.TRHC__Overtime_Pay_Rate__c;
                            }
                        }
                        else{ 
                            If( scr.over_time__c!=null  && scr.TRHC__Overtime_Pay_Rate__c!=null) {
                                weekAmnt  = scr.over_time__c * scr.TRHC__Overtime_Pay_Rate__c;
                            }
                        }

                        for (TRHC__Expense__c expnse: expanseList) {
                            if(expnse.TRHC__Expense_Name__c == 'Housing Stipend' && expnse.TRHC__Expense_Name__c == 'Per Diem'){
                                WeeklyAmount = WeeklyAmount + expnse.TRHC__Weekly_Amount__c;   
                                scr.Nurse_Gross_weekly_pay__c = weekAmnt + WeeklyAmount; 
                                system.debug('============ weekAmnt =============='+weekAmnt);
                                system.debug('----------------- expense name  ---------'+expnse.TRHC__Expense_Name__c);
                                system.debug('----------------- expnse.TRHC__Weekly_Amount__c  ---------'+expnse.TRHC__Weekly_Amount__c);
                                system.debug('----------------- nurse gross pay ---------'+scr.Nurse_Gross_weekly_pay__c);
                            } 
                        }
                        scrList.add(scr);
                    }

                    if(!scrList.isempty()){
                        update scrList;
                    }
                    //update scratchObj;
                }
            }catch(Exception ex){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Scratch Sheet has been submitted and can NOT be edited.'));
            }
        }
        return null;
    }
    public pageReference  deleteRow(){
        /*String index = apexpages.currentpage().getparameters().get('rowIndex');
        delete expenseList[Integer.valueOf(index)]; */
        
         Integer indexVal = Integer.valueof(system.currentpagereference().getparameters().get('rowIn1'));
         System.Debug('==indexVal=='+indexVal);
        if(expenseList[indexVal].Id != null)
            lstExpenseDelete.add(expenseList[indexVal]);
        
        expenseList.remove(indexVal);          
        
        return null;
    }
    
    public pageReference  deleteReadRow(){
        /*String index = apexpages.currentpage().getparameters().get('rowIndex');
        delete expenseList[Integer.valueOf(index)]; */
        
         Integer indexVal = Integer.valueof(system.currentpagereference().getparameters().get('rowIndex'));
        
        if(expenseListRead[indexVal].Id != null)
            lstExpenseDelete.add(expenseListRead[indexVal]);
        
        expenseListRead.remove(indexVal);          
        
        return null;
    }
    
   
    
    public List<String> getcolumns(){
        List<String> fields = new List<String>();
        for(Schema.FieldSetMember f : SObjectType.TRHC__Expense__c.FieldSets.Expense_Columns.getFields()) {
            fields.add(f.getFieldPath());
        }
        return fields;
    }
    
    public List<String> getamountColumns(){
        List<String> fields = new List<String>();
        for(Schema.FieldSetMember f : SObjectType.TRHC__Expense__c.FieldSets.Amount_Columns.getFields()) {
            fields.add(f.getFieldPath());
        }
        return fields;
    }
    

    public set<String> geteditableColumns(){
        set<String> fields = new set<String>();
        for(Schema.FieldSetMember f : SObjectType.TRHC__Expense__c.FieldSets.Editable_fields.getFields()) {
            fields.add(f.getFieldPath());
        }
        return fields;
    }
    
    public static string getCreatableFieldsSOQL(String objectName, String whereClause,String additionalFields)
    {
        system.debug('---objectName---'+objectName);
        system.debug('---whereClause---'+whereClause);
        system.debug('---additionalFields---'+additionalFields);
        String selects = '';
       
        if (whereClause == null || whereClause == ''){ return null; }
      
        Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(objectName.toLowerCase()).getDescribe().Fields.getMap();
        list<string> selectFields = new list<string>();
 

        if (fMap != null){
            for (Schema.SObjectField ft : fMap.values()){ // loop through all field tokens (ft)
                Schema.DescribeFieldResult fd = ft.getDescribe(); // describe each field (fd)
              //  if (fd.isCreateable()){ // field is creatable                    
                    selectFields.add(fd.getName());
              //  }
            }
        }
        
       if(!selectFields.isEmpty()){
            for (string s:selectFields){
                selects += s + ',';
            }
            selects += additionalFields;
                if (selects.endsWith(',')){selects = selects.substring(0,selects.lastIndexOf(','));}
            
        }
        return 'SELECT ' + selects + ' FROM ' + objectName + ' WHERE ' + whereClause;
    }
}