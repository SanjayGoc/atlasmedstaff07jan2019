global with sharing class PeerReferencesController {
	global String selectLanguage { get; set; }
	global String cmsCssCustom { get; set; }
	
	global Contact realContact;
	global Contact con { get; set; }
	
	transient global Contact referral1 { get; set; }
	transient global Contact referral2 { get; set; }
	transient global Contact referral3 { get; set; }
	
	global String referralsJson { get; set; }
	
	global String nextPageUrl { get; set; }
	
	private static TRHC.Logger logger = new TRHC.Logger('PeerReferencesController');
		
	global void init() { 
		selectLanguage = UserInfo.getLocale();
		cmsCssCustom = TRHC.CMSHelper.getCSSStyles();
		
		referral1 = new Contact();
		referral2 = new Contact();
		referral3 = new Contact();
		
		realContact = TRHC.CMSHelper.getLoggedInContact();
		con = new Contact();
		con = realContact.clone(false, false);
        If(test.isRunningTest()){
            Contact condummy = new Contact ();
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
			condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';
            condummy.FirstName = 'chaitanya';

        }
        system.debug('con@@@'+con);
	}
	
	global void save() {
		logger.log('save', 'referralsJson', referralsJson);
		
		try {
			List<Contact> conList = new List<Contact>();
			
			TRHC.ContactFieldsUtil.updateRecords(con, realContact);
			if (realContact != null) {
				conList.add(realContact);
			}
			
			Id recordTypeId = TRHC.RecordTypeUtil.getCandidateRecordTypeIdList()[0];
			//system.debug('referrals@@@@'+referrals);
			List<Contact> referrals = 
					(List<Contact>)JSON.deserialize(referralsJson, List<Contact>.class);
            
            for (Contact referral : referrals) {
				referral.TRHC__Referred_By__c = realContact.Id;
				referral.RecordTypeId = recordTypeId;
				conList.add(referral);
			}
			
			referral1 = (referrals.size() > 0) ? referrals[0].clone(false, false) : new Contact();
			referral2 = (referrals.size() > 1) ? referrals[1].clone(false, false) : new Contact();
			referral3 = (referrals.size() > 2) ? referrals[2].clone(false, false) : new Contact();
			
			/*MetadataUtilNew.checkCrudFls(
					Contact.sObjectType,
					new Schema.sObjectField[] {
							Contact.TRHC__Referred_By__c,
							Contact.RecordTypeId },
					MetadataUtilNew.CHECK_UPSERT);*/
			
			upsert conList;
			
			//nextPageUrl = CMSHelper.getPageReference('Certifications').getUrl();
			nextPageUrl = getPageReference('EducationHistory').getUrl();
		} catch (Exception ex) {
			ApexPages.addMessages(ex);
		}
	}
    public PageReference getPageReference(String page) {
		PageReference pg = new PageReference('/TRHC__CMSLayout');
		
		pg.getParameters().put('page', page);
		pg.getParameters().put('sessionId', 
				ApexPages.currentPage().getParameters().get('sessionId'));
		pg.getParameters().put('p', 
				ApexPages.currentPage().getParameters().get('p'));
		pg.getParameters().put('jobSite', 
				ApexPages.currentPage().getParameters().get('jobSite'));
		pg.setRedirect(true);
		
		return pg;
	}
    
}