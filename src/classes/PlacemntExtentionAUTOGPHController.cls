public with sharing class PlacemntExtentionAUTOGPHController {
    
    	Public Date startDate {get;set;}
    	public Date endDate {get;set;}
    	public String plcID {get;set;}
        public Boolean isCreated {get;set;}
        public Boolean isError{get;set;}
        private static TRHC__Healthcare_Setting__c customSetting = TRHC__Healthcare_Setting__c.getAll().get('Default');
        public string queryString;
        public List<TRHC__Job__c> jobList{get;set;}
        public List<string> jobIdList = new  List<Id>();
        public List<TRHC__Expense__c> expancesList{get;set;}
        private string jobFieldList;
        public string candidateId;
        public TRHC__Scratch_Sheet__c scratchSheet{get;set;}
        public List<TRHC__Expense__c> listExpense{get;set;}
        public decimal payBurdern{get;set;}
        string defaultScratchTemplate;
        string allfields;
        string scrtchSheetRecordTypeId;
        TRHC__Scratch_Sheet__c newScratchSheet;
        public Boolean checkGSAService;
        public Boolean billPayRate {get;set;}
        public id scrid;
        
        public PlacemntExtentionAUTOGPHController(){
            checkGSAService = (Boolean)customSetting.get('TRHC__Enable_GSA_Service_to_Update_M_I_Rate__c');
            isCreated = false;
            isError = false;
            startDate = date.valueOf(ApexPages.currentPage().getParameters().get('startDate'));
            endDate = date.valueOf(ApexPages.currentPage().getParameters().get('endDate'));
            plcID = ApexPages.currentPage().getParameters().get('plcId');
            string jobIds = ApexPages.currentPage().getParameters().get('jId');
            candidateId = ApexPages.currentPage().getParameters().get('cId');
            jobIdList = (jobIds != null) ? jobIds.split(',') : new  List<Id>();
            //get jobFields
            jobFieldList = ObjectFieldsHelper.getAllFields('TRHC__Job__c').get('TRHC__Job__c');
            //query for job
            string jobQuery = 'Select ' + jobFieldList + ' from TRHC__Job__c where ID IN : jobIdList';
            //String jobQuery ='Select id,trhc__state__c+' from TRHC__Job__c where ID IN'+jobIdList;
            jobList = Database.query(jobQuery);
            
            if(jobList.get(0).TRHC__Regular_Bill_Rate__c > 60){
                billPayRate = false;
            } else {
                billPayRate = true;
            }   
        }

        public Void insertScratchsheet() {
            TRHC__Placement__c placment = [SELECT ID, TRHC__start_date__c, TRHC__Contact_Candidate__c, TRHC__End_Date__c, TRHC__Scratch_Sheet__c FROM TRHC__Placement__c WHERE ID =:plcID];
            checkGSAService = (Boolean)customSetting.get('TRHC__Enable_GSA_Service_to_Update_M_I_Rate__c');
            //getting scrtach sheet recordtype
            scrtchSheetRecordTypeId = [SELECT
                                       Id
                                       FROM
                                       RecordType
                                       WHERE
                                       SobjectType = 'TRHC__Scratch_Sheet__c' AND Name = 'ScratchSheet'].Id;
            defaultScratchTemplate = (string)customSetting.get('TRHC__Default_Scratch_sheet_Template_Name__c');
            defaultScratchTemplate = '\'' + defaultScratchTemplate + '\'';
            
            //get All fields of an sobject 
            Map<string, string> scratchSheethelper = ObjectFieldsHelper.getAllFields('TRHC__Scratch_Sheet__c');
            allfields = scratchSheethelper.get('TRHC__Scratch_Sheet__c');
            queryString = 'Select ' + allfields + ' from TRHC__Scratch_Sheet__c ' + ' where name = ' + defaultScratchTemplate;
            if(defaultScratchTemplate == null || defaultScratchTemplate == '') {
                ApexPages.addMessage(new  ApexPages.message(ApexPages.Severity.ERROR, '<span style="font-family: Tahoma;">' + 'Scrtch sheet is not available' + '.</span>'));
                isError = true;
            }

            if(defaultScratchTemplate != null) {
                for(TRHC__Job__c job: jobList) {
                    TRHC__Scratch_Sheet__c scratchSheet = Database.query(queryString);
                    string expnaceFields = ObjectFieldsHelper.getAllFields('TRHC__Expense__c').get('TRHC__Expense__c');
                    //get all expanses of scratch sheet template query

                    Id cid = string.valueOf(scratchSheet.id).subString(0, 15);
                    string sid = '\'' + cid + '\'';
                    string expancesQueryString = 'Select ' + expnaceFields + ' from TRHC__Expense__c ' + ' where TRHC__Scratch_Sheet__c =' + sid;

                    List<TRHC__Expense__c> templateExpnaseList = Database.query(expancesQueryString);
                    newScratchSheet = scratchSheet.clone(true, false);
                    newScratchSheet.TRHC__Pay_Rate__c = 20;
                    newScratchSheet.id = null;
                    if(candidateId != null) {
                        newScratchSheet.TRHC__Candidate__c = candidateId;
                        //mapping fields
                    }
                    List<Job_to_Scratch_Sheet__c> jobToScratchSheet = [SELECT
                                                                       Job_Field_API_Name_Custom__c, 
                                                                       Scratch_Sheet_Field_API_Name__c
                                                                       FROM
                                                                       Job_to_Scratch_Sheet__c];
                    for(Job_to_Scratch_Sheet__c jobToScratch: jobToScratchSheet) {
                        string jobField = jobToScratch.Job_Field_API_Name_Custom__c;
                        string scratchSheetField = jobToScratch.Scratch_Sheet_Field_API_Name__c;
                        if(jobFieldList.contains(jobField) && allfields.contains(scratchSheetField)) {
                            newScratchSheet.put(scratchSheetField, job.get(jobField));
                        }
                    }
                    newScratchSheet.TRHC__County__c = job.TRHC__County__c;
                    newScratchSheet.Name = job.TRHC__Job_Title__c + ' ScratchSheet';
                    newScratchSheet.RecordTypeId = scrtchSheetRecordTypeId;

                    if(checkGSAService == false) {
                        newScratchSheet.TRHC__GSA_M_I_Rate__c = job.GSA_M_I_Rate__c;
                        newScratchSheet.TRHC__GSA_Lodging__c = job.GSA_Lodging__c;
                        newScratchSheet.TRHC__Tax_Rate__c = job.Tax_Rate__c;
                    }
                    newScratchSheet.OT_Pay_rate_Override__c = true;
                    newScratchSheet.TRHC__Pay_Rate__c = 20.00;
                    newScratchSheet.TRHC__Start_Date__c = startDate;
                    newScratchSheet.TRHC__Candidate__c = placment.TRHC__Contact_Candidate__c;
                    newScratchSheet.TRHC__End_Date__c = endDate;
                    newScratchSheet.End_Date_New__c = endDate;
                    placment.TRHC__Start_Date__c = startDate;
                    placment.TRHC__End_Date__c = endDate;
                    Insert newScratchSheet;
                    placment.TRHC__Scratch_Sheet__c = newScratchSheet.id;
                    update placment;
                    
                    expancesList = new  List<TRHC__Expense__c>();
                    for(TRHC__Expense__c expanse: templateExpnaseList) {
                        expanse.id = null;
                        expanse.TRHC__Scratch_Sheet__c = newScratchSheet.id;
                        if(expanse.TRHC__Expense_Name__c == 'Orientation Hours'){
                            if(newScratchSheet.TRHC__No_Bill_Orientation_Hours__c != null && newScratchSheet.TRHC__Pay_Rate__c != null){
                                Decimal value = newScratchSheet.TRHC__No_Bill_Orientation_Hours__c * newScratchSheet.TRHC__Pay_Rate__c;
                                expanse.TRHC__Value__c = value;
                            }

                        }
                        expancesList.add(expanse);
                    }
                    Insert expancesList;
                    job.TRHC__Scratch_Sheet__c = newScratchSheet.id;
                    Update Job;
                    scrid = job.TRHC__Scratch_Sheet__c;
                }
            }

            TRHC__Config_Settings__c configSettings = TRHC__Config_Settings__c.getValues('Default');
            Boolean isEnabled = configSettings == null ? True : (Boolean) configSettings.Trigger_Enable_ScratchSheetAutoAddExp__c;
            List<String> exptIdsList = configSettings == null ? new List<Id>() :configSettings.ExceptConfigIds__c.split(';');
            List<String> exptAccountList = configSettings == null ? new List<String>() :configSettings.Except_Hospitals__c.split(';');
            String configFields = ObjectFieldsHelper.getAllFields('TRHC__Config__c').get('TRHC__Config__c');
            
            String expanseFields = ObjectFieldsHelper.getAllFields('TRHC__Expense__c').get('TRHC__Expense__c');
            
            Set<String> states = new Set<String>();
            
            Set<String> scratchSheetIds = new Set<String>();
            /*
             * @description : modified by chaitanya added Account_Name__c to avoid exception in line number 259
             */ 
            List<TRHC__Scratch_Sheet__c> scrList = [select id,name,State_pic__c,City__c, Account_Name__c From TRHC__Scratch_Sheet__c where id =: newScratchSheet.id ];
            for(TRHC__Scratch_Sheet__c scratchSheet : scrList ) {
                if(scratchSheet.State_pic__c!= null) {
                    states.add('\''+scratchSheet.State_pic__c+'\'');
                }
                scratchSheetIds.add(scratchSheet.Id);
            }
            
            if(states.size() == 0){
                return;
            }
            Map<String, Set<TRHC__Config__c>> configMap = new Map<String, Set<TRHC__Config__c>>();
            
            List<String> statesList = new List<String> (states);
            String query = 'SELECT ' + configFields+ ' FROM TRHC__Config__c WHERE TRHC__State__c IN ' + statesList ;
            
            String configQuery = 'SELECT ID, TRHC__Scratch_Sheet__c,TRHC__Active__c, TRHC__Expense_Name__c, TRHC__Config__c FROM TRHC__Expense__c WHERE TRHC__Scratch_Sheet__c = \''+ newScratchSheet.id +'\'';
            
            if(configSettings != null) {
                String criteria = configSettings.AutoAddExpenseCriteria__c;
                if(criteria != null) {
                    List<String> criterias = criteria.split(',');
                    for(String rule : criterias){
                        query += ' AND ' + rule +' ';
                    }
                }
            }
            
            List<TRHC__Config__c> configs = Database.query(query);
            
            List<TRHC__Expense__c> availableExpenses = Database.query(configQuery);
            if(configs != null && configs.size() > 0) {
                List<Config_To_Expense__c> configToExpense = [Select Config_Field_API_Name_Custom__c, Expanse_Field_API_Name__c From Config_To_Expense__c]; 
                for(TRHC__Config__c config : configs) {
                    Set<TRHC__Config__c> configObjs = configMap.get(config.TRHC__State__c);
                    if(configObjs == null) {
                        configObjs = new Set<TRHC__Config__c>();
                    }
                    configObjs.add(config);
                    configMap.put(config.TRHC__State__c, configObjs);
                }
            
                Set<TRHC__Expense__c> expenses = new Set<TRHC__Expense__c>();
                for(TRHC__Scratch_Sheet__c scratchSheet : scrList) {
                    if(scratchSheet.State_pic__c== null) { 
                        continue;
                    }
                    /*
                    http://gsa.targetrecruit.com/Geocode/getGsaDetails_new.php
                    */
                    Set<TRHC__Config__c> configIds = configMap.get(scratchSheet.State_pic__c);
                    List<String> nameLst = new List<String> ();
                    List<String> scrExpList = new List<String>();
                    for(TRHC__Config__c configId : configIds) {
                        boolean isExist = false;
                        boolean addCityExpense = false;
                        
                        if(exptIdsList.contains(configId.id) && scratchSheet.City__c != configId.TRHC__City__c){
                            isExist = true;
                        }
                        else if(exptIdsList.contains(configId.id) ){
                            nameLst.add(configId.TRHC__Name__c);
                            addCityExpense = true;
                        }
                        
                        for(TRHC__Expense__c avaExp : availableExpenses) {
                            if(avaExp.TRHC__Scratch_Sheet__c == scratchSheet.Id && avaExp.TRHC__Config__c == configId.id) {
                                isExist = true;
                            }
                        }
                        if(isExist) {
                            continue;
                        }

                        TRHC__Expense__c expense = new TRHC__Expense__c();

                        for(Config_To_Expense__c configToExp : configToExpense){
                            String expanseField = configToExp.Expanse_Field_API_Name__c;
                            String configField = configToExp.Config_Field_API_Name_Custom__c;
                            if(configFields.contains(configField)&& expanseFields.contains(expanseField)){
                                if(configField == 'TRHC__Value__c'){
                                    expense.TRHC__Value__c = Decimal.valueOf(configId.TRHC__Value__c);
                                }else{
                                    expense.put(expanseField,configId.get(configField));
                                }
                            }
                        }
                        expense.TRHC__Config__c = configId.id;
                        expense.TRHC__Scratch_Sheet__c  = scratchSheet.Id;
                        
                        expenses.add(expense);
                        scrExpList.add(expense.TRHC__Expense_Name__c);
                    }
                    
                    set<String> setTempString = new set<String>();
                    set<String> duplicateTempString = new set<String>();
                    for(String s:scrExpList){
                        if(!setTempString.add(s)){
                            duplicateTempString .add(s);
                        }
                    }

                    for(TRHC__Expense__c expence:expenses){
                        for(String str:duplicateTempString){
                            if(str == expence.TRHC__Expense_Name__c){
                                if(exptAccountList.contains(scratchSheet.Account_Name__c)){
                                    if(exptIdsList.contains(expence.TRHC__Config__c)){
                                        expenses.remove(expence);  
                                    }
                                    else{
                                        expence.TRHC__Value__c = 0.0;
                                    }
                                }
                                else if(!exptIdsList.contains(expence.TRHC__Config__c)){
                                    expenses.remove(expence); 
                                }
                            }
                        }
                    }
                    
                    for(TRHC__Expense__c exp :expenses){
                        if(exp.TRHC__Expense_Name__c != null && exp.TRHC__Expense_Name__c.contains('Sales Tax') && exptAccountList.contains(scratchSheet.Account_Name__c)){
                            exp.TRHC__Value__c = 0.0;
                        }
                    }
                    
                    If(expenses.size()>0) {
                        insert new List<TRHC__Expense__c>(expenses);
                    }
                }
            }

            if(checkGSAService == false){
                update newScratchSheet;
            }

            List<TRHC__Expense__c> expense = [select TRHC__Expense_Name__c,TRHC__Scratch_Sheet__r.OverTIme_GPH__c,TRHC__Scratch_Sheet__r.Over_Time_Include_Bill__c,TRHC__Scratch_Sheet__r.TRHC__Client_Fee__c,TRHC__Occurence__c, TRHC__Value__c,TRHC__ProRated_Hours__c from TRHC__Expense__c where TRHC__Scratch_Sheet__c =: scrid and TRHC__Expense_Name__c = 'VMS Fees'];
            if(expense.size() > 0){
                system.debug('==OT bill rate==>'+expense[0].TRHC__Scratch_Sheet__r.Over_Time_Include_Bill__c);
                system.debug('===occurrence=>'+expense[0].TRHC__Occurence__c);
                system.debug('===prorated=>'+expense[0].TRHC__ProRated_Hours__c);
                system.debug('===OverTIme_GPH__c=>'+expense[0].TRHC__Scratch_Sheet__r.OverTIme_GPH__c);
                if(expense[0].TRHC__Scratch_Sheet__r.TRHC__Client_Fee__c != null){
                    expense[0].TRHC__Value__c = expense[0].TRHC__Scratch_Sheet__r.TRHC__Client_Fee__c;
                    //(expense[0].TRHC__Scratch_Sheet__r.Over_Time_Include_Bill__c * expense[0].TRHC__Scratch_Sheet__r.TRHC__Client_Fee__c)/(100 * expense[0].TRHC__Occurence__c * expense[0].TRHC__ProRated_Hours__c);
                    Update expense;
                }
            }
        } 

        public Pagereference calcGPH(){   
            scratchSheet = [SELECT
                            Id,
                            OverTime_GPH__c,
                            isGPHCalculated__c,
                            With_OverTime_Gross_Profit__c,
                            TRHC__Assignment_length__c, 
                            TRHC__Hours_Per_Week__c, 
                            TRHC__Start_Date__c,
                            TRHC__End_Date__c,
                            TRHC__Client_Fee__c,
                            State_pic__c,
                            over_time__c,
                            TRHC__Bill_Rate__c
                            FROM
                            TRHC__Scratch_Sheet__c
                            WHERE
                            Id = : newScratchSheet.Id];

            system.debug('======== scratchSheet ========'+scratchSheet);
            string getLabel = System.Label.Editable_Expense;
            Set<string> editAccess = new  Set<string>();
            
            if(getLabel != null) {
                editAccess.addAll(getLabel.Split(','));
            }

            listExpense = [SELECT
                           Id, 
                           TRHC__Expense_Name__c, 
                           TRHC__Amount__c, 
                           TRHC__Occurence__c, 
                           TRHC__Grouping_Total__c, 
                           TRHC__Value__c
                           FROM
                           TRHC__Expense__c
                           WHERE
                           TRHC__Scratch_Sheet__c =: scratchSheet.Id AND TRHC__Expense_Name__c in: editAccess];

            system.debug('======= listExpense ========='+listExpense);
            payBurdern = 0;
            for(TRHC__Expense__c expense: listExpense) {
                if(expense.TRHC__Expense_Name__c != 'Travel Reimbursement') {
                    payBurdern += expense.TRHC__Amount__c;
                    /*if(expense.TRHC__Expense_Name__c == 'VMS Fees'){
                        payBurdern += expense.TRHC__Value__c;
                    }*/
                }
                system.debug('======== payBurdern ========'+payBurdern);
            }

            payBurdern = Math.round(payBurdern);
            if(!scratchSheet.isGPHCalculated__c) {   
                system.debug('======== scratchSheet.isGPHcalculated ========'+scratchSheet.isGPHCalculated__c);
                checkGSAService = (Boolean)customSetting.get('TRHC__Enable_GSA_Service_to_Update_M_I_Rate__c');
                decimal dollar = 0.00;
                if(scratchSheet.TRHC__Bill_Rate__c > 60){
                    system.debug('======== scratchSheet.TRHC__Bill_Rate__c ========'+scratchSheet.TRHC__Bill_Rate__c);
                    if(checkGSAService == false){
                        if(scratchSheet.TRHC__Client_Fee__c == null){
                            dollar = 20.56;
                        } else {
                            dollar = 18.68; ////////////////
                        }
                    } else {
                        if(scratchSheet.TRHC__Client_Fee__c == null){
                            dollar = 19.88;
                        } else {
                            dollar = 17.00; ///////////////
                        }
                    }
                }else {
                    system.debug('======== scratchSheet.TRHC__Bill_Rate__c ========'+scratchSheet.TRHC__Bill_Rate__c);
                    if(checkGSAService == true){
                        if(scratchSheet.TRHC__Client_Fee__c == null){
                            dollar = 16.88;
                        } else {
                            dollar = 15.00; ///////////////
                        }
                    } else {
                        if(scratchSheet.TRHC__Client_Fee__c == null){
                            dollar = 18.56;
                        } else {
                            dollar = 16.68; ///////////////
                        }
                        
                    }
                }
                List<TRHC__Expense__c> listUpdate = new  List<TRHC__Expense__c>();
                
                decimal totalGPHAdd = 0;
                decimal addToNonTaxable = 0;
                if(scratchSheet.over_time__c == null){
                    scratchSheet.over_time__c = 0;
                    system.debug('======== scratchSheet.over_time__c ========'+scratchSheet.over_time__c);
                }
                decimal calculation = (dollar * (scratchSheet.TRHC__Assignment_length__c * (scratchSheet.TRHC__Hours_Per_Week__c + scratchSheet.over_time__c)));
                system.debug('======== dollar ========'+dollar);
                system.debug('======== scratchSheet.TRHC__Assignment_length__c ========'+scratchSheet.TRHC__Assignment_length__c);
                system.debug('======== scratchSheet.TRHC__Hours_Per_Week__c ========'+scratchSheet.TRHC__Hours_Per_Week__c);
                system.debug('======== calculation ========'+calculation);
                system.debug('======== scratchSheet.over_time__c ========'+scratchSheet.over_time__c);
                //if(scratchSheet.OverTime_GPH__c != null){
                    if(dollar > scratchSheet.OverTime_GPH__c) {
                        system.debug('======== scratchSheet.OverTime_GPH__c ========'+scratchSheet.OverTime_GPH__c);
                        totalGPHAdd = (calculation - scratchSheet.With_OverTime_Gross_Profit__c);
                        totalGPHAdd = totalGPHAdd.round() * -1;
                        //if(listExpense != null && scratchSheet != null){
                            calculateGPH(listExpense, scratchSheet, totalGPHAdd, payBurdern);
                            system.debug('========= listExpense If========'+listExpense);
                            system.debug('========= scratchSheet if========'+scratchSheet);
                            system.debug('========= totalGPHAdd  if========'+totalGPHAdd);
                            system.debug('========= payBurdern if ========'+payBurdern);
                        //}
                        
                    } else if(dollar < scratchSheet.OverTime_GPH__c){
                        system.debug('======== scratchSheet.OverTime_GPH__c ========'+scratchSheet.OverTime_GPH__c);
                        totalGPHAdd = (scratchSheet.With_OverTime_Gross_Profit__c - calculation);
                        totalGPHAdd = totalGPHAdd.round();
                        //if(listExpense != null && scratchSheet != null){
                            calculateGPH(listExpense, scratchSheet, totalGPHAdd, payBurdern);       
                            system.debug('========= listExpense else========'+listExpense);
                            system.debug('========= scratchSheet else========'+scratchSheet);
                            system.debug('========= totalGPHAdd  else========'+totalGPHAdd);
                            system.debug('========= payBurdern else========'+payBurdern);
                        //}
                    }
                //}
            }
            return new PageReference('/'+newScratchSheet.id); 
            //return candidateId != null ? new  Pagereference('/' + scratchSheet.id + '/e?retURL=%2F' + scratchSheet.id) : new  Pagereference('/' + scratchSheet.id);
        }

        public void calculateGPH(List<TRHC__Expense__c> listExpenses, TRHC__Scratch_Sheet__c sheet, decimal totalGPH, decimal totalExpense) {
            system.debug('========= listExpenses========'+listExpenses);
            system.debug('========= sheet========'+sheet);
            system.debug('========= totalGPH  ========'+totalGPH);
            system.debug('========= totalExpense ========'+totalExpense);
            List<TRHC__Expense__c> listUpdate = new  List<TRHC__Expense__c>();
            for(TRHC__Expense__c singleExpense: listExpenses) {
                system.debug('===== singleExpense ========'+singleExpense);
                if(singleExpense.TRHC__Expense_Name__c != 'Travel Reimbursement') {   
                    decimal expenseValue = (singleExpense.TRHC__Amount__c + (((singleExpense.TRHC__Amount__c / totalExpense) * 100) * totalGPH) / 100) / singleExpense.TRHC__Occurence__c;
                    system.debug('====== expenseValue ======='+expenseValue);
                    singleExpense.TRHC__Value__c = Math.round(expenseValue);
                    system.debug('====== expenseValue ======='+expenseValue);
                    listUpdate.add(singleExpense);
                }
            }

            if(!listUpdate.isEmpty()) {
                Update listUpdate;
                sheet.isGPHCalculated__c = true;
                Update sheet;
            }
        }
    }