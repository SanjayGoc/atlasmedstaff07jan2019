@isTest
public class CandidateStatusChangerTest {
    
    @isTest
    public Static void candiStatusChangetTest(){
        
        TRHC__Config_Settings__c config = new TRHC__Config_Settings__c();
        config.Name = 'Default';
        config.Enable_Update_Recruiter__c = False;
        config.Enable_RecruitersMailNotificationTrigger__c = False;
        config.Excep_status_for_batch_CandidateStatusCh__c = null;
        insert config;

        Contact contact = new Contact();
        contact.RecordTypeId = [Select ID FROM RecordType Where Name = 'Candidate' Limit 1].id;
        contact.FirstName = 'chaitanya';
        contact.LastName = 'bollamreddy';
        contact.TRHC__Candidate_Status__c = 'InActive';
        insert contact;
        
        Contact contact1 = new Contact();
        contact1.RecordTypeId = [Select ID FROM RecordType Where Name = 'Candidate' Limit 1].id;
        contact1.FirstName = 'chaitanya';
        contact1.LastName = 'b';
        contact1.TRHC__Candidate_Status__c = 'Active';
        insert contact1;
        
        Contact contact2 = new Contact();
        contact2.RecordTypeId = [Select ID FROM RecordType Where Name = 'Candidate' Limit 1].id;
        contact2.FirstName = 'chaitanya';
        contact2.LastName = 'b';
        contact2.TRHC__Candidate_Status__c = 'Pending Review Board';
        insert contact2;
        
        
        TRHC__Placement__c placement  = new TRHC__Placement__c();
        placement.TRHC__Contact_Candidate__c = contact.id;
        placement.TRHC__Start_Date__c = Date.today() - 30;
        placement.TRHC__End_Date__c = Date.today() + 30;
        insert placement;
        
        TRHC__Placement__c placement1  = new TRHC__Placement__c();
        placement1.TRHC__Contact_Candidate__c = contact1.id;
        placement1.TRHC__Start_Date__c = Date.today() - 30;
        placement1.TRHC__End_Date__c = Date.today() -1;
        insert placement1;
        
        TRHC__Placement__c placement2  = new TRHC__Placement__c();
        placement2.TRHC__Contact_Candidate__c = contact2.id;
        placement2.TRHC__Start_Date__c = Date.today() - 30;
        placement2.TRHC__End_Date__c = Date.today() -1;
        insert placement2;
        
        Test.startTest();
        CandidateStatusChanger CSC = new CandidateStatusChanger();
        Database.executeBatch(CSC);
        Test.stopTest();
    }

}