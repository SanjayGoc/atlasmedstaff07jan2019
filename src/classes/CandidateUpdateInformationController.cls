public with Sharing class CandidateUpdateInformationController {
    String conId { get;set; }
    public String firstName { get;set; }
    public String cmsCssCustom { get;set; }
    Integer PhoneNumber { get;set; }
    String email { get;set; }
    public Contact con { get;set; }
    public Contact conOrginal { get;set; }
    String selectedFields;
    String WhereCondition;
    Public boolean success{ get;private set; }
    public Boolean validUser { get;set; }
    public String password { get;set; }
    public void init(){
        cmsCssCustom = TRHC.CMSHelper.getCSSStyles();
        con = new Contact();
        password=null;
    }
    public pagereference  UpdateMypersonalInfoInIt(){
        selectedFields =  '';
        validUser = False;
        success = False;
        system.debug('password>>>>'+password);
        PageReference pg = ApexPages.currentPage();
        if(password != null){
            Atlas_Update_Info_req__c req = Atlas_Update_Info_req__c.getInstance();
            String key1 = req.Crypto__c;
            if(key1 == null) key1 = 'Atlasmedstaff123';
            boolean isencrypt = false;
            String orgPassword = '';
            if(password.length()>32){
            	orgPassword = AtlasCryptohelper.aes128(password, key1, isencrypt);
            }
            conId = Apexpages.currentPage().getParameters().Get('CandidateId');
            String Query = 'SELECT '+selectedFields+' Where '+whereCondition;
            
               List<Contact> contacts =[
                SELECT Id, TRHC__Temporary_Street__c, TRHC__Veteran_Status__c, Name, FirstName, LastName, TRHC__Middle_Name__c, TRHC__Gender__c, Discipline1__c, Phone, Primary_Speciality1__c, Email, Secondary_Speciality__c,
                		HomePhone, TRHC__Shift_Preference__c, TRHC__Referral_Source__c, Recruiters__c, SSN_Text__c, Birthdate, Permanant_Address__c, TRHC__Permanent_City__c,
                		State_pic__c, TRHC__Permanent_Zip_Postal_Code__c, Temporary_Address_1__c, TRHC__Temporary_City__c, temporary_state__c, TRHC__Temporary_Zip_Postal_Code__c
                From Contact 
                Where Id =:conId
                AND TRHC__Password__c =:orgPassword];
                if(contacts.size()>0){
                  conOrginal = contacts[0]; 
                  con = conOrginal.clone();
                  con.id=null;
                }else{
                    ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.error,
                                                   'Please check your password'));
                }
        }
        if(conOrginal != NUll){
            validUser = true;
        }else{
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.error,
                                                   'Please check your password'));
        }
        pg.setRedirect(true);
        return null;
    }
    Public PageReference updateMypersonalInfo(){
        con.TRHC__Veteran_Status__c = 'Updated';
        con.id=conOrginal.id;
        update con;
        success = true;
        PageReference pg = ApexPages.currentPage();
        ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,
                                                   'Thank you for taking the time to complete the survey and confirming your contact details'));
        pg.setRedirect(true);
        validUser = false;
        password = null;
        return null;
    }
    public void savePersonalInformation(){
        
    }
    public PageReference UpdateRecord(){
            conId = Apexpages.currentPage().getParameters().Get('CandidateId');
            String Query = 'SELECT '+selectedFields+' Where '+whereCondition;
            con =[
                SELECT Id, TRHC__Veteran_Status__c, Name, FirstName, LastName, TRHC__Middle_Name__c, TRHC__Gender__c, Discipline1__c, Phone, Primary_Speciality1__c, Email, Secondary_Speciality__c,
                		HomePhone, TRHC__Shift_Preference__c, TRHC__Referral_Source__c, Recruiters__c, SSN_Text__c, Birthdate, Permanant_Address__c, TRHC__Permanent_City__c,
                		State_pic__c, TRHC__Permanent_Zip_Postal_Code__c, Temporary_Address_1__c, TRHC__Temporary_City__c, temporary_state__c, TRHC__Temporary_Zip_Postal_Code__c
                From Contact 
                Where Id =:conId];
        con.TRHC__Veteran_Status__c = 'UpToDate';
        update con;
        PageReference pg = ApexPages.currentPage();
        ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,
                                                   'Thank you for taking the time to complete the survey and confirming your contact details'));
        pg.setRedirect(true);
        validUser = false;
        password = null;
        return null;
    }
}