@isTest(seeAllData=True)
public class DashboardPageControllerTest {
    static testmethod void dashboardpagetest() {
        //Get User Record        
        List<User> usr = [SELECT Id,Name,Email,FirstName,LastName FROM User WHERE IsActive = true LIMIT 3];
        
        //Create Candidate/Nurse Record
        Contact con = new Contact();
        con.FirstName = 'mike'; 
        con.LastName = 'saas';
        con.Email = 'test@test1.gmail.com';
        con.Compliance_Coordinater__c = usr[0].id;
        con.TRHC__Recruiter__c = usr[1].id;
        insert con;
        
        //Create Job Record
        TRHC__Job__c job = new TRHC__Job__c();
        job.TRHC__Stage__c = 'closed';
        job.CreatedDate = Date.today();
        job.TRHC__Number_of_Placements__c = 1;
        insert job;
        
        TRHC__Job__c job1 = new TRHC__Job__c();
        job1.TRHC__Stage__c = 'closed';
        job1.CreatedDate = System.Today().addMonths(-1);
        job1.TRHC__Number_of_Placements__c = 1;
        insert job1;
        
        TRHC__Job_Applicant__c jobApplication = new TRHC__Job_Applicant__c();
        jobApplication.TRHC__Job__c = job.Id;
        jobApplication.TRHC__Contact_Candidate__c = con.Id;
        jobApplication.TRHC__Recruiter__c = usr[1].Id;
        jobApplication.TRHC__Account_Manager__c = usr[2].Id;
        insert jobApplication;
        
        //Create Placement Record 
        Periodically_Date__c periodically = new Periodically_Date__c();
        periodically.Name = 'Period14';
        periodically.Start_Date__c = System.Today().addMonths(-3);
        periodically.End_Date__c = System.Today();
        insert periodically;
        
        Test.startTest();
            DashboardPageController dashPageController = new DashboardPageController();
                dashPageController.getRecruiterdaysList();
                dashPageController.startDat = Date.today() - 7;
                dashPageController.endDat = Date.today() + 7;
                dashPageController.startedDate = System.Today().addMonths(-3);
                dashPageController.endedDate = System.Today();
                dashPageController.singlePickList = 'CM';
                dashPageController.getExtensionData();
                dashPageController.selectedDaysForCM = periodically.Name;
                dashPageController.selectedDaysForRecruiter = periodically.Name;
                dashPageController.selecteddaysforcompliencecordinator = periodically.Name;
                dashPageController.getScore();
                dashPageController.getMonthPlacementcount();
                dashPageController.selectMonth(periodically.Name);
                //dashPageController.todaydate = System.Today();

               /* boolean test1 = dashPageController.getDisablePrevious;
                boolean test2 = dashPageController.getDisableNext;
                integer test3 = dashPageController.getTotal_size;
                integer test4 = dashPageController.getPageNumber;
                integer test5 = dashPageController.getTotalPages;*/

                DashboardPageControllerNew dashPageControllerNew = new DashboardPageControllerNew();
                dashPageControllerNew.getRecruiterdaysList();
                dashPageControllerNew.startDat = Date.today() - 7;
                dashPageControllerNew.endDat = Date.today() + 7;
                dashPageControllerNew.startedDate = System.Today().addMonths(-3);
                dashPageControllerNew.endedDate = System.Today();
                dashPageControllerNew.singlePickList = 'CC';
                dashPageControllerNew.getExtensionData();
                dashPageControllerNew.selectedDaysForCM = periodically.Name;
                dashPageControllerNew.selectedDaysForRecruiter = periodically.Name;
            //    dashPageControllerNew.selecteddaysforcompliencecordinator = periodically.Name;
                dashPageControllerNew.getScore();
            //    dashPageControllerNew.getMonthPlacementcount();
                dashPageControllerNew.selectMonth(periodically.Name);
                  
        Test.stopTest();   
    }

    static testMethod void dashbordpagetest1() {
        
        List<User> usr = [SELECT Id,Name,Email,FirstName,LastName FROM User WHERE IsActive = true LIMIT 3];
    
        //Create Candidate/Nurse Record
        Contact con = new Contact();
        con.FirstName = 'mike'; 
        con.LastName = 'saas';
        con.Email = 'test@test1.gmail.com';
        con.Compliance_Coordinater__c = usr[0].id;
        con.TRHC__Recruiter__c = usr[1].id;
        insert con;
        
        TRHC__Job__c job = new TRHC__Job__c();
        job.TRHC__Stage__c = 'closed';
        job.CreatedDate = System.Today().addMonths(-1);
        job.TRHC__Number_of_Placements__c = 1;
        insert job;
        
        TRHC__Job_Applicant__c jobApplication = new TRHC__Job_Applicant__c();
        jobApplication.TRHC__Job__c = job.Id;
        jobApplication.TRHC__Contact_Candidate__c = con.Id;
        jobApplication.TRHC__Recruiter__c = usr[1].Id;
        jobApplication.TRHC__Account_Manager__c = usr[2].Id;
        insert jobApplication;
        
        Periodically_Date__c periodically = new Periodically_Date__c();
        periodically.Name = 'Period15';
        periodically.Start_Date__c = System.Today().addMonths(-1);
        periodically.End_Date__c = System.Today();
        insert periodically;
        
        Test.startTest();
            DashboardPageController dashPageController1 = new DashboardPageController();
                dashPageController1.getRecruiterdaysList();
                dashPageController1.startDat = Date.today() - 7;
                dashPageController1.endDat = Date.today() + 7;
                dashPageController1.startedDate = System.Today().addMonths(-3);
                dashPageController1.endedDate = System.Today();
                dashPageController1.singlePickList = 'Recruiter';
                dashPageController1.getExtensionData();
                dashPageController1.selectedDaysForCM = periodically.Name;
                dashPageController1.selectedDaysForRecruiter = periodically.Name;
                dashPageController1.selecteddaysforcompliencecordinator = periodically.Name;
                dashPageController1.getScore();
                dashPageController1.getMonthPlacementcount();
                dashPageController1.selectMonth(periodically.Name);
                //dashPageController1.todaydate = System.Today();
                DashboardPageControllerNew dashPageControllerNew = new DashboardPageControllerNew();
                dashPageControllerNew.getRecruiterdaysList();
                dashPageControllerNew.startDat = Date.today() - 7;
                dashPageControllerNew.endDat = Date.today() + 7;
                dashPageControllerNew.startedDate = System.Today().addMonths(-3);
                dashPageControllerNew.endedDate = System.Today();
                dashPageControllerNew.singlePickList = 'CC';
                dashPageControllerNew.getExtensionData();
                dashPageControllerNew.selectedDaysForCM = periodically.Name;
                dashPageControllerNew.selectedDaysForRecruiter = periodically.Name;
             //   dashPageControllerNew.selecteddaysforcompliencecordinator = periodically.Name;
                dashPageControllerNew.getScore();
            //    dashPageControllerNew.getMonthPlacementcount();
                dashPageControllerNew.selectMonth(periodically.Name);

        Test.stopTest();   
    }
    
    static testMethod void dashbordpagetest2() {
        
        List<User> usr = [SELECT Id,Name,Email,FirstName,LastName FROM User WHERE IsActive = true LIMIT 3];
    
        //Create Candidate/Nurse Record
        Contact con = new Contact();
        con.FirstName = 'mike'; 
        con.LastName = 'saas';
        con.Email = 'test@test1.gmail.com';
        con.Compliance_Coordinater__c = usr[0].id;
        con.TRHC__Recruiter__c = usr[1].id;
        insert con;
        
        TRHC__Job__c job = new TRHC__Job__c();
        job.TRHC__Stage__c = 'closed';
        job.CreatedDate = System.Today().addMonths(-2);
        job.TRHC__Number_of_Placements__c = 1;
        insert job;
        
        TRHC__Job_Applicant__c jobApplication = new TRHC__Job_Applicant__c();
        jobApplication.TRHC__Job__c = job.Id;
        jobApplication.TRHC__Contact_Candidate__c = con.Id;
        jobApplication.TRHC__Recruiter__c = usr[1].Id;
        jobApplication.TRHC__Account_Manager__c = usr[2].Id;
        insert jobApplication;
        
        //Create Placement Record 
        TRHC__Placement__c placement  = new TRHC__Placement__c();
        placement.TRHC__Contact_Candidate__c = con.id;
        placement.TRHC__Job__c = job.id;
        placement.TRHC__Status__c = 'Active';
        placement.TRHC__Extension__c = false;
        placement.TRHC__Date_Accepted__c = Date.today() - 7;
        placement.TRHC__Start_Date__c = System.Today().addMonths(-3);
        placement.TRHC__End_Date__c = System.Today();
        placement.TRHC__Recruiter__c = usr[1].id;
        placement.TRHC__Account_Manager__c = usr[2].id;
        placement.TRHC__Job_Applicant__c = jobApplication.Id;
        placement.Compliance_Coordinator__c = usr[0].Id;
        insert placement;
        
        Periodically_Date__c periodically = new Periodically_Date__c();
        periodically.Name = 'Period16';
        periodically.Start_Date__c = System.Today().addMonths(-2);
        periodically.End_Date__c = System.Today();
        insert periodically;
        
        Test.startTest();
            DashboardPageController dashPageController = new DashboardPageController();
                dashPageController.getRecruiterdaysList();
                dashPageController.startDat = Date.today() - 7;
                dashPageController.endDat = Date.today() + 7;
                dashPageController.startedDate = System.Today().addMonths(-3);
                dashPageController.endedDate = System.Today();
                dashPageController.singlePickList = 'CC';
                dashPageController.getExtensionData();
                dashPageController.selectedDaysForCM = periodically.Name;
                dashPageController.selectedDaysForRecruiter = periodically.Name;
                dashPageController.selecteddaysforcompliencecordinator = periodically.Name;
                dashPageController.getScore();
                dashPageController.getMonthPlacementcount();
                dashPageController.selectMonth(periodically.Name);
                //dashPageController.todaydate = System.Today();


            DashboardPageControllerNew dashPageControllerNew = new DashboardPageControllerNew();
            dashPageControllerNew.getRecruiterdaysList();
            dashPageControllerNew.startDat = Date.today() - 7;
            dashPageControllerNew.endDat = Date.today() + 7;
            dashPageControllerNew.startedDate = System.Today().addMonths(-3);
            dashPageControllerNew.endedDate = System.Today();
            dashPageControllerNew.singlePickList = 'CC';
            dashPageControllerNew.getExtensionData();
            dashPageControllerNew.selectedDaysForCM = periodically.Name;
            dashPageControllerNew.selectedDaysForRecruiter = periodically.Name;
        //    dashPageControllerNew.selecteddaysforcompliencecordinator = periodically.Name;
            dashPageControllerNew.getScore();
        //    dashPageControllerNew.getMonthPlacementcount();
            dashPageControllerNew.selectMonth(periodically.Name);
        Test.stopTest();   
    }
}