@isTest
public class ResumeTemplate_deleteEduHistories_TC {

    @isTest
    public static void delEdu(){
        test.startTest();
        TRHC__Config_Settings__c configSettings = new TRHC__Config_Settings__c();
        configSettings.Name = 'Default';
        configSettings.deleteEdu_where__c = 'createddate = LAST_N_DAYS:1';
        insert configSettings;
        
        TRHC__CredentialsCustomSettings__c credSettings =
					new TRHC__CredentialsCustomSettings__c();
        credSettings.Name = 'Default';
        credSettings.TRHC__Disable_CredAnswerCopyAttachmentTrigger__c = true;
        insert credSettings;
        TRHC__Educational_History__c edu = new TRHC__Educational_History__c();
        edu.Candidate_text__c = '0031L00002lpbVy';
        edu.TRHC__Candidate__c = '0031L00002lpbVy';
        //cred.lastmodifieddate = date.today()-2;
        insert edu;
        
        ResumeTemplate_deleteEduHistories rd = new ResumeTemplate_deleteEduHistories();
        Database.executeBatch(rd);
        ResumeTemplate_deleteEduHistories_She rTDS = new ResumeTemplate_deleteEduHistories_She();
        String sch = '0 0 23 * * ?';
        system.schedule('Test Check', sch, rTDS);
        test.stopTest();
    }
}