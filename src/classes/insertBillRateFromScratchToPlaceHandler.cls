public class insertBillRateFromScratchToPlaceHandler {
    public static void insertBillRateOnPlacement(List<TRHC__Placement__c> placementList) {
        System.debug('==Inside Method===');
        Set<Id> scratchSheetIds = new Set<Id>();
        for(TRHC__Placement__c plm : placementList) {
            scratchSheetIds.add(plm.TRHC__Scratch_Sheet__c);
        }
        Map<String,TRHC__Scratch_Sheet__c>  scrachSheetMap = new Map<String,TRHC__Scratch_Sheet__c>();
        for(TRHC__Scratch_Sheet__c scrachSheet : [SELECT Id,Name,TRHC__Bill_Rate__c,TRHC__Overtime_Rate__c from TRHC__Scratch_Sheet__c where Id In:scratchSheetIds]) {
            scrachSheetMap.put(scrachSheet.Id,scrachSheet);
        }
        List<TRHC__Placement__c> placeList = new List<TRHC__Placement__c>();
        
        for(TRHC__Placement__c plm : placementList) {
            plm.TRHC__Regular_Hours_Bill_Rate__c = scrachSheetMap.get(plm.TRHC__Scratch_Sheet__c).TRHC__Bill_Rate__c;
            plm.TRHC__Overtime_Hours_Bill_Rate__c = scrachSheetMap.get(plm.TRHC__Scratch_Sheet__c).TRHC__Overtime_Rate__c;
        }
    }
}