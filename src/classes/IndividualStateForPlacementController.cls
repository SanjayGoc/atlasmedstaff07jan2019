public with sharing class IndividualStateForPlacementController {
    public String selectedState                  {get; set;}
    public Map<String,String> countryStateKey    {get; set;}
    public List<TRHC__Placement__c> listPlacement            {get; set;}
    public String stateName                      {get; set;}
    private Integer counter   = 0;
    private Integer list_Size = 20;
    private Integer total_Size                   {get; set;}
    public  Integer selected_Item                {get; set;}

    public IndividualStateForPlacementController () {
        selectedState = ApexPages.currentPage().getParameters().get('selectedState');
        System.debug('selectedState'+selectedState);
        getcountryStateKey();
        stateName = countryStateKey.get(selectedState);
        System.debug('stateName'+stateName);
       /* if(stateName != null){
            try{
                
                total_Size = [SELECT Count()
                                FROM TRHC__Placement__c

                               WHERE TRHC__Status__c = 'Active'
                                 AND Hospital_State__c =: stateName];          
            }catch(QueryException e){
                System.debug('QueryException:'+e.getMessage());
            }                 

        }*/
    }

    public List<TRHC__Placement__c> getJobList(){
        try{
            listPlacement = [SELECT Id,
                              Name,
                              Active__c,
                              Hospital_State__c,
                              TRHC__Status__c 
                         FROM TRHC__Placement__c
                        WHERE TRHC__Status__c = 'Active'
                          AND Hospital_State__c =: stateName
                          AND Hospital_State__c != null
                        /*LIMIT :list_Size
                       OFFSET :counter*/];    
            return listPlacement;                           
            }catch(QueryException e){
                System.debug('QueryException:'+e.getMessage());
            return null;
            }
    }

    public void getcountryStateKey() {
        countryStateKey = new Map<String,String>();
        countryStateKey.put('AL','Alabama');
        countryStateKey.put('AK','Alaska');
        countryStateKey.put('AB','Alberta');
        countryStateKey.put('AS','American Samoa');
        countryStateKey.put('AZ','Arizona');
        countryStateKey.put('AR','Arkansas');
        countryStateKey.put('AE','Armed Forces (AE)');
        countryStateKey.put('AA','Armed Forces Americas');
        countryStateKey.put('AP','Armed Forces Pacific');
        countryStateKey.put('BC','British Columbia');
        countryStateKey.put('CA','California');
        countryStateKey.put('CO','Colorado');
        countryStateKey.put('CT','Connecticut');
        countryStateKey.put('DE','Delaware');
        countryStateKey.put('DC','District Of Columbia');
        countryStateKey.put('FL','Florida');
        countryStateKey.put('GA','Georgia');
        countryStateKey.put('GU','Guam');
        countryStateKey.put('HI','Hawaii');
        countryStateKey.put('ID','Idaho');
        countryStateKey.put('IL','Illinois');
        countryStateKey.put('IN','Indiana');
        countryStateKey.put('IA','Iowa');
        countryStateKey.put('KS','Kansas');
        countryStateKey.put('KY','Kentucky');
        countryStateKey.put('LA','Louisiana');
        countryStateKey.put('ME','Maine');
        countryStateKey.put('MB','Manitoba');
        countryStateKey.put('MD','Maryland');
        countryStateKey.put('MA','Massachusetts');
        countryStateKey.put('MI','Michigan');
        countryStateKey.put('MN','Minnesota');
        countryStateKey.put('MS','Mississippi');
        countryStateKey.put('MO','Missouri');
        countryStateKey.put('MT','Montana');
        countryStateKey.put('NE','Nebraska');
        countryStateKey.put('NV','Nevada');
        countryStateKey.put('NB','New Brunswick');
        countryStateKey.put('NH','New Hampshire');
        countryStateKey.put('NJ','New Jersey');
        countryStateKey.put('NM','New Mexico');
        countryStateKey.put('NY','New York');
        countryStateKey.put('NF','Newfoundland');
        countryStateKey.put('NC','North Carolina');
        countryStateKey.put('ND','North Dakota');
        countryStateKey.put('NT','Northwest Territories');
        countryStateKey.put('NS','Nova Scotia');
        countryStateKey.put('NU','Nunavut');
        countryStateKey.put('OH','Ohio');
        countryStateKey.put('OK','Oklahoma');
        countryStateKey.put('ON','Ontario');
        countryStateKey.put('OR','Oregon');
        countryStateKey.put('PA','Pennsylvania');
        countryStateKey.put('PE','Prince Edward Island');
        countryStateKey.put('PR','Puerto Rico');
        countryStateKey.put('QC','Quebec');
        countryStateKey.put('RI','Rhode Island');
        countryStateKey.put('SK','Saskatchewan');
        countryStateKey.put('SC','South Carolina');
        countryStateKey.put('SD','South Dakota');
        countryStateKey.put('TN','Tennessee');
        countryStateKey.put('TX','Texas');
        countryStateKey.put('UT','Utah');
        countryStateKey.put('VT','Vermont');
        countryStateKey.put('VI','Virgin Islands');
        countryStateKey.put('VA','Virginia');
        countryStateKey.put('WA','Washington');
        countryStateKey.put('WV','West Virginia');
        countryStateKey.put('WI','Wisconsin');
        countryStateKey.put('WY','Wyoming');
        countryStateKey.put('YT','Yukon Territory');
    }

    /*public PageReference Beginning() { //user clicked beginning
       counter = 0;
       return null;
    }

    public PageReference Previous() { //user clicked previous button
        counter -= list_size;
        return null;
    }

    public PageReference Next() { //user clicked next button
       counter += list_size;
       return null;
    }

    public PageReference End() { //user clicked end
       counter = total_size - math.mod(total_size, list_size);
       return null;
    }

    public Boolean getDisablePrevious() { 
       //this will disable the previous and beginning buttons
       if (counter>0) return false; else return true;
    }

    public Boolean getDisableNext() { //this will disable the next and end buttons
       if (counter + list_size < total_size) return false; else return true;
    }

    public Integer getTotal_size() {
       return total_size;
    }

    public Integer getPageNumber() {
       return counter/list_size + 1;
    }

    public Integer getTotalPages() {
        if (math.mod(total_size, list_size) > 0) {
            return total_size/list_size + 1;
        } else {
            return (total_size/list_size);
        }
    }*/
}