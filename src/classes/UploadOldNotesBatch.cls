public class UploadOldNotesBatch implements Database.Batchable<Note>{
	
    List<Note> notesList = new List<Note>();
    public List<Note> start(Database.BatchableContext ctx){
        Date archiveElgible = Date.today()-180;
        notesList = [SELECT ID, ParentID, Title, CreatedBy.Name 
                     ,CreatedDate, LastModifiedDate, Body FROM Note WHERE CreatedDate < :archiveElgible];
        return notesList;
    }
    public void execute(Database.BatchableContext ctx, List<Note> notes){
        List<Note__b> oldNotes = new List<Note__b>();
        for(Note nt :notes){
            Note__b onb = new Note__b();
            onb.Title__c = nt.Title;
            onb.Last_Modified_Date__c = nt.LastModifiedDate;
            onb.Date_Of_Created__c = nt.CreatedDate;
            onb.Related_Contact__c = nt.ParentId;
            onb.Created_BY__c = UserInfo.getUserId();
            onb.Modified_BY__c = UserInfo.getUserId();
            if(nt.Body != null){
                onb.Note_Body__c = string.valueOf(nt.Body);
            }else{
                onb.Note_Body__c = 'No Data Found';
            }
            oldNotes.add(onb);
        }
        if(!Test.isRunningTest())
            Database.insertImmediate(oldNotes);
        System.debug('oldNotes@@@'+oldNotes);
    }
    public void finish(Database.BatchableContext ctx){
        //delete notesList;
    }
}