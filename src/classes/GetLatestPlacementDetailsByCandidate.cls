public class GetLatestPlacementDetailsByCandidate {
    public TRHC__Placement__c getLatestPlacement(ID id){
        List<TRHC__Placement__c> placements = [
            	SELECT id, Purchase_Request_Act_of_Kindness__c,
            	Purchase_Request_Airfare__c, Purchase_Request_Atlas_Adventures__c,
            	Purchase_Request_Birthday__c, Purchase_Request_Housing__c,
            	CreatedDate, TRHC__Status__c 
            		FROM TRHC__Placement__c 
            	WHERE TRHC__Contact_Candidate__c =:id];
        if(placements.size()>0){
        	return findLatest(placements);
        }else{
         	 return null;  
        }
    }
    public TRHC__Placement__c findLatest(List<TRHC__Placement__c> placements){
        TRHC__Placement__c latestPlacement = null;
        TRHC__Placement__c latestActivePlacement = null;
        for( TRHC__Placement__c placement :placements ){
            if( latestPlacement != null ){
                if( latestPlacement.CreatedDate < placement.CreatedDate ){
                    latestPlacement = placement;
                }
            }
            else{
                latestPlacement = placement;
            }
            if( placement.TRHC__Status__c == 'Active' ) {
                if( latestActivePlacement == null )
                {
                    latestActivePlacement = placement;
                }else if( latestActivePlacement.CreatedDate < placement.CreatedDate ){
                    latestActivePlacement = placement;
                }
			}
        }
        if ( latestActivePlacement != null ) {
            return latestActivePlacement;
        } else {
            return latestPlacement;
        }
    }
}