public class FireBasePushNotificationClass{
    
    @future(callout=true)
    public static void postHTTPFireBaseClass(List<Id> jobList) {

        Http httpREq;
        String url = 'https://fcm.googleapis.com/fcm/send';
        HttpRequest req;
        HttpResponse res;
        List<Id> contactListId;
        Notification__c notificationObj;
        List<Notification__c> notifiList = new List<Notification__c>();
        List<TRHC__Job__c> jobPushList = new List<TRHC__Job__c>([SELECT Id, 
                                                                        Name,
                                                                        TRHC__Hiring_Manager__r.DeviceId__c,
                                                                        TRHC__Hiring_Manager__c,
                                                                        TRHC__Job_Title__c
                                                                FROM TRHC__Job__c 
                                                                WHERE Id IN: jobList]);
        
        FireBaseServerKeyMobile__c server  =  FireBaseServerKeyMobile__c.getValues('Default');
        string serkey = server.serverKey__c;
        
        For(TRHC__Job__c jobObj : jobPushList) {
            httpREq = new Http();
            req = new HttpRequest();
            req.setEndpoint(url);
            req.setMethod('POST');
            req.setTimeout(120000);
            notificationObj = new Notification__c();
            
            // to - device token
            // key = Server key
            
            string deviceId = jobObj.TRHC__Hiring_Manager__r.DeviceId__c;
            system.debug('===== deviceId  ====='+deviceId);
            req.setbody('{"to":"'+deviceId+'","notification":{"body":"We have found job for you '+jobObj.TRHC__Job_Title__c+' that You May interested in ","title":"New Job Created"}}');
            
            req.setHeader('Content-Type', 'application/json');
            
            req.setHeader('Authorization', 'key='+serkey);
            
            res = httpREq.send(req);
            system.debug('res '+res.getBody() );
            if(res.getStatusCode() == 200){
                notificationObj.Contact__c = jobObj.TRHC__Hiring_Manager__c;
                notificationObj.Job__c = jobObj.id;
                notifiList.add(notificationObj);
            } 
        }
        if(!notifiList.isEmpty()){
            insert notifiList;
        }
    }
    
}