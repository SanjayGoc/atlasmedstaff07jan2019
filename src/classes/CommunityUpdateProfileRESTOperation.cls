@RestResource(urlMapping='/mobile/UpdateProfile/*')
global class CommunityUpdateProfileRESTOperation {
    @HttpGet
    global static ResponseHandler CommunityUpdateProfileRESTOperation(){
        String apiToken = RestContext.request.params.get('apiToken');
        String firstName = RestContext.request.params.get('firstName');
        String lastName = RestContext.request.params.get('lastName');
        String email = RestContext.request.params.get('email');
        String phone = RestContext.request.params.get('phone');
        String discipline = RestContext.request.params.get('discipline');
        String primarySpeciality = RestContext.request.params.get('primarySpeciality');
        String secondarySpecialtyList = RestContext.request.params.get('secondarySpecialty');
        String shiftPreferenceList = RestContext.request.params.get('shiftPreference');

        List<String> secSpecList = secondarySpecialtyList.split(';');
        List<String> shiftPreList = shiftPreferenceList.split(';'); 

        ResponseHandler res = new ResponseHandler();

        List<Contact> userList = [Select Id,
                                     Name,
                                     firstName,
                                     lastName,
                                     apiToken__c,
                                     Email,
                                     Phone,
                                     Discipline1__c,
                                     Primary_Speciality1__c,
                                     Secondary_Speciality__c,
                                     TRHC__Shift_Preference__c,
                                     Permanant_Address__c,
                                     TRHC__Permanent_Address_2__c, 
                                     TRHC__Permanent_City__c,
                                     State_pic__c,
                                     TRHC__Permanent_Zip_Postal_Code__c,
                                     Temporary_Address_1__c,
                                     Temporary_Address_2__c,
                                     TRHC__Temporary_City__c,
                                     temporary_state__c,
                                     TRHC__Temporary_Zip_Postal_Code__c
                            From Contact 
                            Where apiToken__c =: apiToken
                            ];

        if(!userList.isEmpty()) {

            userList[0].firstName = firstName;
            userList[0].lastName = lastName;
            userList[0].Email = email;
            userList[0].Phone = phone;

            userList[0].Discipline1__c = discipline;
            userList[0].Primary_Speciality1__c = primarySpeciality;
            userList[0].Secondary_Speciality__c = secondarySpecialtyList;
            userList[0].TRHC__Shift_Preference__c = shiftPreferenceList;
            update userList[0];
            
            res.success = 1;
            res.message = new string[]{'Profile updated Succesfully.'};
            res.firstName = firstName;
            res.lastName = lastName;
            res.email = email;
            res.phone = phone;

            res.discipline = discipline;
            res.primarySpeciality = primarySpeciality;
            if(secSpecList != null){
                res.secondarySpecialty = secSpecList;
            } else {
                res.secondarySpecialty = new List<String>();
            }
            if(shiftPreList != null){
                res.shiftPreference = shiftPreList;
            } else {
                res.shiftPreference = new List<String>();
            }
            return res;
        }

        res.success = 2;
        res.message = new string[]{'Profile Not updated Succesfully.'};
        return res;
    }
    
    global class ResponseHandler {
        global integer success;
        global string[] message;
        global string firstName;
        global string lastName;
        global string email;
        global string phone;
        global string discipline;
        global string primarySpeciality;
        global List<string> secondarySpecialty;
        global List<string> shiftPreference;

        global ResponseHandler () {
        }
    }
}