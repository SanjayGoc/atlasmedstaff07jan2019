global class CreatePlacementExtensionController {
    
    ApexPages.StandardController controller;
    public TRHC__Placement__c placement {get;set;}
    public id placId {get;set;}
    public TRHC__Placement__c plcExtention{get;set;}
    public void getPlacement(){
        
    }
    global void init(){
        placId = ApexPages.currentPage().getParameters().get('id');
        String AllFields  = ObjectFieldsHelper.getAllFields('TRHC__Placement__c').get('TRHC__Placement__c');
        String allfiledsQuery = 'SELECT '+AllFields+' FROM TRHC__Placement__c Where id = '+'\''+placId+'\'';
        TRHC__Placement__c placement = Database.query(allfiledsQuery);
        plcExtention = placement.clone();
        plcExtention.id = Null;
        //logic to set to next Monday
        plcExtention.TRHC__Start_Date__c = placement.TRHC__End_Date__c.toStartOfWeek()+8;
        //logic to set to after 13 weeks of startdate and that should be saturady
        Date newEndDate = plcExtention.TRHC__Start_Date__c + (13*7);
        plcExtention.TRHC__End_Date__c =  newEndDate.toStartOfWeek()+6;
        plcExtention.TRHC__Status__c = 'Active';
        plcExtention.TRHC__Extension__c = True;
        plcExtention.Cleared__c = false;
        if(!Test.isRunningTest())
        insert plcExtention;
        placement = plcExtention;
    }
    public PageReference updatePLC(){
        Pagereference pg = Null;
        if(!Test.isRunningTest())
        update plcExtention;
        if(plcExtention.TRHC__Start_Date__c != null && plcExtention.TRHC__End_Date__c != null){
        	pg = createScratchSheet(plcExtention.TRHC__Start_Date__c, plcExtention.TRHC__End_Date__c);
        }
        return pg;
    }
    public pageReference createScratchSheet(Date strDate, Date endDate){
        PageReference vfPage = new PageReference('/apex/PlacemntExtentionAUTOGPH');
        vfPage.getParameters().put('plcId',plcExtention.Id);
		vfPage.getParameters().put('jId', plcExtention.TRHC__Job__c);
        vfPage.getParameters().put('startDate', String.valueOf(plcExtention.TRHC__Start_Date__c));
        vfPage.getParameters().put('endDate', String.valueOf(plcExtention.TRHC__End_Date__c));
     	return vfPage;
    }
    public void updateScratchSheet(){
        
    }
}