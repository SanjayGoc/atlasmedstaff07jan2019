/**
 * @Author : Chaitanya Bollamreddi
 * @date : 06 April 2018
 * @descrption : Sheduler class for Batch class ExpirationTask
 */
global class ExpirationTaskSheduler implements Schedulable {
    /**
     * @Author Chaitanya Bollamreddi
     * @date 06 Aprial 2018
     * @name execute
     * @param SchedulableContext object
     * @Return void
     */
    global void execute(SchedulableContext sc) {
        ExpirationTask b = new ExpirationTask(); 
        database.executebatch(b);
    }
}