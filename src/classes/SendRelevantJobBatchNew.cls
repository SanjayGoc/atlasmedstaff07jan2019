public with sharing class SendRelevantJobBatchNew implements Database.Batchable<sObject>,database.stateful {
    
    private static Date today = Date.today();
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        
        Id rectypeId =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Candidate').getRecordTypeId();
        
        String query = 
            ' SELECT TRHC__Jobs_Notified_Long__c,Email,Name ' + 
            ' FROM Contact ' + 
            ' WHERE Email != null ' + 
            ' AND Id IN (SELECT TRHC__Contact__c FROM TRHC__Job_Search_Agent__c) ' + 
            ' AND RecordTypeId = :rectypeId ' ;
        
        return Database.getQueryLocator(query);
    }
    public void execute(Database.BatchableContext BC, List<Contact> candidateInList) {
        Contact candidate = candidateInList[0];
        
        sendEmailForCandidate(candidate);
        
    }
    public static void sendEmailForCandidate(Contact candidate) {
		//OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'info@atlasmedstaff.com'];
        // system.debug(candidate.Email);
        Id owea = Label.Organization_Wide_Email_For_Job_Alert;
        set<TRHC__Job__c> jobsSet = NEW set<TRHC__Job__c> ();
        Id templateId = Label.EmailTemplateForJobAlert;
        EmailTemplate template = [Select id,HTMLValue,body,subject from EmailTemplate where id = :templateId ];
        //system.debug('.........................'+template.HTMLValue);
        String TemplateBodyWithSubject = template.HTMLValue;
        system.debug('template is..'+TemplateBodyWithSubject );
        List<TRHC__Job_Search_Agent__c> jobAlerts = [
            SELECT TRHC__Search_Criteria__c, TRHC__Zip_Code__c, TRHC__Max_Distance__c, TRHC__Frequency_Alert__c
            FROM TRHC__Job_Search_Agent__c
            WHERE TRHC__Contact__c = :candidate.Id];
        List<string> address = new List<String>  {candidate.Email};
            if (jobAlerts.size() > 0) {
                List<String> jobInfoLists = new List<String>();
                
                for ( TRHC__Job_Search_Agent__c jobAlert : jobAlerts ) {
                    
                    if (!alertToday(jobAlert)) {
                        continue;
                    }
                    
                    //system.debug('search...'+jobAlert.TRHC__Search_Criteria__c);
                    List<TRHC__Job__c> jobs = JobSearchHelper.getJobs(
                        jobAlert.TRHC__Search_Criteria__c, 
                        jobAlert.TRHC__Zip_Code__c,
                        jobAlert.TRHC__Max_Distance__c,
                        candidate.TRHC__Jobs_Notified_Long__c,
                        jobAlert.TRHC__Frequency_Alert__c);
                    jobsSet.addAll(jobs);
                }
                if ( jobsSet.size() > 0 ) {
                    Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                    email.setSubject(template.subject);
                    String s = Helper_Email.getEmailBody(jobsSet,TemplateBodyWithSubject);
                    System.debug('111111111111111111'+TemplateBodyWithSubject);
                    email.setHtmlBody(s);
                    if ( owea != null || owea != '' ) {
    					email.setOrgWideEmailAddressId(owea);
					}
                    email.setToAddresses(address);
                    Messaging.SendEmailResult[] r =Messaging.sendEmail(NEW Messaging.SingleEmailMessage[] {email});
                    
                }
                
                if (jobInfoLists.size() == 0) {
                    return;
                }
                
            }
        
        
    }
    
    private static Boolean alertToday(TRHC__Job_Search_Agent__c jobAlert) {
        return (jobAlert.TRHC__Frequency_Alert__c == 'Monthly' && today.day() == 1) ||
            (jobAlert.TRHC__Frequency_Alert__c == 'Weekly' && today.toStartOfWeek() == today) ||
            (jobAlert.TRHC__Frequency_Alert__c == 'Daily' 
             || String.isBlank(jobAlert.TRHC__Frequency_Alert__c));
    }
    public void finish(Database.BatchableContext BC) { }
    
}