/****************************************************************
*   Created By  : Hari S
*   Created Date    : 7/27/2016
*   Description : Class is used to retrives the results from prophecy and updates 
            in assessment record or skills answers record by identifying the 
            assessment type
            a. If assessment type is checklist, updates the skill answer record
            b. If assessment type is other than checklist, updates the assessment record
******************************************************************/

global class ProphecyUpdateAssessmentResultsBatch 
        implements 
        Database.Batchable<sObject>,
        Database.Stateful,
        Database.AllowsCallouts {
    
    String objectName;
    public TRHC__Prophecy_Settings__c v_ProphecyCustomSettings;
    public Static Integer timezoneVal;
    global prophecyUpdateAssessmentResultsBatch(String objectName) {
        this.objectName = objectName;
        timezoneVal = 0;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        v_ProphecyCustomSettings = TRHC__Prophecy_Settings__c.getInstance(UserInfo.getUserId());
        timezoneVal = Integer.valueOf(v_ProphecyCustomSettings
                .TRHC__Prophecy_TimeZone_Add_Minute__c);
        String query = '';
        if (objectName.equals('TRHC__Assessment__c')) {
            query = 
                    ' SELECT '+getAllFields('TRHC__Assessment__c') +
                    ' FROM TRHC__Assessment__c' +
                    ' WHERE' +
                    ' TRHC__Results_URL__c = NULL' +
                    ' AND' +
                    ' TRHC__Score__c = NULL AND TRHC__Caregiver_Assessment_ID__c != NULL';
        }
        if (objectName.equals('TRHC__Skill_Answers__c')) {
            query = 
                    ' SELECT '+getAllFields('TRHC__Skill_Answers__c') +
                    ' FROM TRHC__Skill_Answers__c' +
                    ' WHERE' +
                        ' TRHC__Results_URL__c = NULL' +
                        ' AND' + ' TRHC__Overall_Comprehension__c = NULL '+
                        ' AND TRHC__Caregiver_Assessment_ID__c != NULL';
        }
        return DataBase.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List <sObject> scope) {
        String prophecySecurityCode = v_ProphecyCustomSettings.TRHC__Security_Code__c;
        if (objectName.equals('TRHC__Assessment__c')) {
            for (sObject assessment: scope) {
                if (assessment.get('TRHC__Results_URL__c') == null
                        && assessment.get('TRHC__Score__c') == null) {
                    prophecyUpdateAssessmentResultsBatch.sendRequestToProphecy(
                            prophecySecurityCode,
                            double.valueOf(assessment.get('TRHC__Caregiver_Assessment_ID__c')),
                            String.valueOf(assessment.get('TRHC__Assessment_Type__c')));
                }
            }
        }
        if (objectName.equals('TRHC__Skill_Answers__c')) {
            for (sObject assessment :scope) {
                if (assessment.get('TRHC__Results_URL__c') == null
                        && assessment.get('TRHC__Overall_Comprehension__c') == null) {
                    prophecyUpdateAssessmentResultsBatch.sendRequestToProphecy (
                            prophecySecurityCode, 
                            double.valueOf(assessment.get('TRHC__Caregiver_Assessment_ID__c')),
                            String.valueOf(assessment.get('TRHC__Assessment_Type__c')));
                }
            }
        }
    }
    global void finish(Database.BatchableContext BC) {
    }

    public String getAllFields (String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);
        if (objectType == null) {
            return fields;
        }
        for (string f :objectType.getDescribe ().fields.getMap ().keySet ()) {
            fields += f+ ', ';
        }
        return fields.removeEnd(', '); 
        
    }

    public static void sendRequestToProphecy (String SecurityCode,
            Double caregiverAssessmentID, String assessmentType) {
        ProphecyWebService.ProphecyConnectXML_cfc prophecyInstance 
                = new ProphecyWebService.ProphecyConnectXML_cfc();
        String assessmentResultsString = '';
        if ( Test.isRunningTest() ) {
            assessmentResultsString  = '{' +
                    ' "Success":"true", ' +
                    ' "Count":"1",' +
                    ' "Results": [' +
                    ' {' +
                    ' "AssessmentID":"123456",' +
                    ' "AssessmentType":"Test",' +
                    ' "CaregiverID":"123456",' +
                    ' "CaregiverAssessmentID":    "123456",' +
                    ' "DateStarted":"2012-07-01 12:00:21",' +
                    ' "DateFinished":"2012-07-01 12:21:47",' +
                    ' "DateSent":"2012-07-01 09:38:00",' +
                    ' "OverallComprehension":"0",' +
                    ' "OverallFrequency":"0",' +
                    ' "RequestID":"123456",' +
                    ' "ResultsURL":"http://nt.prophecyhealth.com/t/view_test_results.cfm?id=123456",' +
                    ' "Score":"89",' +
                    ' "TotalCorrect":"31",' +
                    ' "TotalQuestions":"35",' +
                    ' "TotalTime":"21 minutes"' +
                    ' },' +
                    ' {' +
                    ' "AssessmentID":"123456",' +
                    ' "AssessmentType":"Checklist",' +
                    ' "CaregiverID":"12345678",' +
                    ' "CaregiverAssessmentID":"12345678",' +
                    ' "DateStarted":"2012-07-01 12:00:21",' +
                    ' "DateFinished":"2012-07-01 12:21:47",' +
                    ' "DateSent":"2012-07-01 09:38:00",' +
                    ' "OverallComprehension":"0",' +
                    ' "OverallFrequency":"0",' +
                    ' "RequestID":"123456",' +
                    ' "ResultsURL":"http://nt.prophecyhealth.com/t/view_test_results.cfm?id=123456",' +
                    ' "Score":"89",' +
                    ' "TotalCorrect":"31",' +
                    ' "TotalQuestions":"35",' +
                    ' "TotalTime":"21 minutes"' +
                    ' } ' +
                    ' ]} ';
        }
        if (!Test.isRunningTest()) {
            assessmentResultsString = prophecyInstance.GetResultsByCaregiverAssessmentID(
                    SecurityCode, 
                    caregiverAssessmentID, 
                    assessmentType);
        }
        ProphecyAssessmentResultHelper jsonData = ProphecyAssessmentResultHelper.parse(
                assessmentResultsString);
        Set <Decimal> caregiverAssessmentIDSet = new Set <Decimal>();
        Set <String> caregiverAssessmentIDsForChecklist = new Set <String>();
        if (jsonData.Success == 'true') {
            for (ProphecyAssessmentResultHelper.cls_Results indivResult: jsonData.Results) {
                if (indivResult.AssessmentType == 'Checklist') {
                    caregiverAssessmentIDsForChecklist.add(indivResult.CaregiverAssessmentID);
                } else {
                    caregiverAssessmentIDSet
                            .add(Decimal.valueOf(indivResult.CaregiverAssessmentID));
                }
            }
            if (caregiverAssessmentIDSet.size() > 0) {
                List <TRHC__Assessment__c> assessmentList = NEW List <TRHC__Assessment__c>(
                        [SELECT 
                            TRHC__Total_Time__c, TRHC__Total_Questions__c,
                            TRHC__Total_Correct__c, TRHC__Results_URL__c,
                            TRHC__Overall_Comprehension__c, TRHC__Overall_Frequency__c,
                            TRHC__Date_Finished__c, TRHC__Date_Started__c,TRHC__Date_Sent__c,
                            TRHC__Caregiver_Assessment_ID__c, 
                            TRHC__Assessment_Type__c,
                            TRHC__Caregiver_ID__c, TRHC__Assessment_Name__c
                        FROM 
                            TRHC__Assessment__c
                        WHERE 
                            TRHC__Caregiver_Assessment_ID__c IN: caregiverAssessmentIDSet
                                AND TRHC__Caregiver_Assessment_ID__c != null
                        ]);
                Map <String, TRHC__Assessment__c> assessmentsMap 
                        = new Map <String, TRHC__Assessment__c>();
                for (TRHC__Assessment__c assessment: assessmentList) {
                    assessmentsMap.put(String.valueOf(assessment.TRHC__Caregiver_Assessment_ID__c),
                            assessment);
                }
                List <TRHC__Assessment__c> updateAssessmentList 
                        = new List<TRHC__Assessment__c>();
                for (ProphecyAssessmentResultHelper.cls_Results indivResult: jsonData.Results) {
                    if (assessmentsMap.get(indivResult.CaregiverAssessmentID) != null) {
                        if (indivResult.DateFinished != null 
                                && indivResult.DateFinished.trim().length() > 0) {
                            TRHC__Assessment__c updateAssessment = new TRHC__Assessment__c();
                            updateAssessment.id = assessmentsMap
                                    .get(indivResult.CaregiverAssessmentID)
                                    .id;
                            updateAssessment.TRHC__Assessment_Name__c
                                    = assessmentsMap.get(indivResult.CaregiverAssessmentID)
                                            .TRHC__Assessment_Name__c;
                            updateAssessment.TRHC__Total_Time__c = indivResult.TotalTime;
                            updateAssessment.TRHC__Total_Questions__c = 
                                    Integer.valueOf(indivResult.TotalQuestions);
                            updateAssessment.TRHC__Total_Correct__c = 
                                    Integer.valueOf(indivResult.TotalCorrect);
                            updateAssessment.TRHC__Results_URL__c = indivResult.ResultsURL + 
                                    '&showdetails=1';
                            updateAssessment.TRHC__Score__c
                                    = double.valueOf(indivResult.Score);
                            if (indivResult.DateFinished != null) {
                                updateAssessment.TRHC__Date_Finished__c
                                        = ProphecyUpdateAssessmentResultsBatch.formatDate(indivResult.DateFinished);
                            }
                            if (indivResult.DateStarted != null) {
                                updateAssessment.TRHC__Date_Started__c 
                                        = ProphecyUpdateAssessmentResultsBatch.formatDate(indivResult.DateStarted);
                            }
                            if (indivResult.DateSent != null) {
                                updateAssessment.TRHC__Date_Sent__c 
                                        = ProphecyUpdateAssessmentResultsBatch.formatDate(indivResult.DateSent);
                            }
                            updateAssessment.TRHC__Overall_Comprehension__c 
                                    = indivResult.OverallComprehension;
                            updateAssessment.TRHC__Overall_Frequency__c 
                                    = indivResult.OverallFrequency;
                            if (indivResult.DateFinished != null 
                                    && indivResult.DateFinished.trim().length() > 0) {
                                updateAssessmentList.add(updateAssessment);
                            }
                        }
                    }
                }
                if (updateAssessmentList.size() > 0) {
                    //update updateAssessmentList;
                    Map <ID, String> assessmentResultsURLMap = NEW Map <ID, String> ();
                    Map <ID, String> assessmentNamesMap = NEW Map <ID, String> ();
                    for (TRHC__Assessment__c assessment: updateAssessmentList) {
                        if (assessment.TRHC__Results_URL__c != NULL) {
                            assessmentResultsURLMap.put(assessment.ID, 
                                    assessment.TRHC__Results_URL__c);
                            assessmentNamesMap.put(assessment.ID,
                                    assessment.TRHC__Assessment_Name__c);
                        }
                    }
            
                    createResultsPDF (assessmentResultsURLMap, assessmentNamesMap);
                        update updateAssessmentList;
                }
            }

            if (caregiverAssessmentIDsForChecklist.size() > 0) {
                List <TRHC__Skill_Answers__c> assessmentList 
                        = NEW List <TRHC__Skill_Answers__c>(
                                [SELECT 
                                    TRHC__Total_Time__c, TRHC__Total_Questions__c,
                                    TRHC__Total_Correct__c, TRHC__Score__c, 
                                    TRHC__Skill_Checklist_Name__c,
                                    TRHC__Results_URL__c, TRHC__Overall_Comprehension__c,
                                    TRHC__Date_Finished__c, TRHC__Date_Started__c,
                                    TRHC__Date_Sent__c,
                                    TRHC__Caregiver_Assessment_Id__c, 
                                    TRHC__Overall_Frequency__c
                                    FROM
                                    TRHC__Skill_Answers__c
                                    WHERE
                                    TRHC__Caregiver_Assessment_Id__c 
                                    IN:
                                    caregiverAssessmentIDsForChecklist
                                    AND TRHC__Caregiver_Assessment_ID__c != null
                                ]);
    
                Map<String, TRHC__Skill_Answers__c> assessmentsMap =
                        new Map<String, TRHC__Skill_Answers__c>();
                for (TRHC__Skill_Answers__c assessment: assessmentList) {
                    assessmentsMap.put(String.valueOf(assessment.TRHC__Caregiver_Assessment_Id__c),
                            assessment);
                }
                List<TRHC__Skill_Answers__c> updateAssessmentList 
                        = new List<TRHC__Skill_Answers__c>();
                for (ProphecyAssessmentResultHelper.cls_Results indivResult: jsonData.Results) {
                    if (assessmentsMap.get(indivResult.CaregiverAssessmentID) != null) {
                        if (indivResult.DateFinished != null 
                                && indivResult.DateFinished.trim().length() > 0) {
                            TRHC__Skill_Answers__c updateAssessment = new TRHC__Skill_Answers__c();
                            updateAssessment.id 
                                = assessmentsMap.get(indivResult.CaregiverAssessmentID).id;
                            updateAssessment.TRHC__Skill_Checklist_Name__c 
                                    = assessmentsMap.get(indivResult.CaregiverAssessmentID)
                                        .TRHC__Skill_Checklist_Name__c;
                            updateAssessment.TRHC__Total_Time__c 
                                = indivResult.TotalTime;
                            updateAssessment.TRHC__Total_Questions__c 
                                    = Integer.valueOf(indivResult.TotalQuestions);
                            updateAssessment.TRHC__Total_Correct__c 
                                    = Integer.valueOf(indivResult.TotalCorrect);
                            updateAssessment.TRHC__Score__c = indivResult.Score;
                            updateAssessment.TRHC__Results_URL__c 
                                    = indivResult.ResultsURL + '&showdetails=1';
                            if (indivResult.DateFinished != null 
                                    && indivResult.DateFinished.trim().length() > 0) {
                                updateAssessment.TRHC__Date_Finished__c 
                                        = ProphecyUpdateAssessmentResultsBatch.formatDate(indivResult.DateFinished);
                            }
                            if (indivResult.DateStarted != null
                                && indivResult.DateStarted.trim().length() > 0) {
                                updateAssessment.TRHC__Date_Started__c
                                        = ProphecyUpdateAssessmentResultsBatch.formatDate(indivResult.DateStarted);
                            }
                            if (indivResult.DateSent != null
                                && indivResult.DateSent.trim().length() > 0) {
                                 updateAssessment.TRHC__Date_Sent__c 
                                        = ProphecyUpdateAssessmentResultsBatch.formatDate(indivResult.DateSent);
                            }
                            updateAssessment.TRHC__Overall_Comprehension__c
                                    = indivResult.OverallComprehension;
                            updateAssessment.TRHC__Overall_Frequency__c 
                                = indivResult.OverallFrequency;
                            if (indivResult.DateFinished != null
                                    && indivResult.DateFinished.trim().length() > 0) {
                                updateAssessmentList.add(updateAssessment);
                            }
                        }
                    }
                }
                if (updateAssessmentList.size() > 0) {
                    Map <ID, String> assessmentResultsURLMap = NEW Map <ID, String> ();
                    Map <ID, String> assessmentNamesMap = NEW Map <ID, String> ();
                    integer i = 0;
                    for (TRHC__Skill_Answers__c assessment: updateAssessmentList) {
                        if (assessment.TRHC__Results_URL__c != NULL) {
                                assessmentResultsURLMap.put(assessment.ID,
                                        assessment.TRHC__Results_URL__c);
                                assessmentNamesMap.put(assessment.ID,
                                        assessment.TRHC__Skill_Checklist_Name__c);
                        }
                        if(assessment.TRHC__Date_Finished__c == null || assessment.TRHC__Score__c == null){
                            updateAssessmentList.remove(updateAssessmentList.indexOf(assessment));
                        }
                        i++;
                    }
                    createResultsPDF(assessmentResultsURLMap, assessmentNamesMap);
                    update updateAssessmentList;
                    //update updateAssessmentList;
                }
            }
        }
    }

    /* Author   : Hari S
    * Description   : converts the datetime into local timezone
    */
    public static dateTime formatDate(String dateTimeString) {
        TRHC__Prophecy_Settings__c v_ProphecyCustomSettings
                = TRHC__Prophecy_Settings__c.getInstance(UserInfo.getUserId());
        timezoneVal 
                = Integer.valueOf(v_ProphecyCustomSettings.TRHC__Prophecy_TimeZone_Add_Minute__c);
        Integer prophecyMinutes = timezoneVal;
        System.Debug ('prophecyMinutes'+prophecyMinutes);
        DateTime customerDateTime = DateTime.valueofGmt(datetimestring);
        System.debug('customerDateTime'+customerDateTime);
        DateTime gmtDateTime = customerDateTime.addHours(math.abs(prophecyMinutes));
        System.debug('customerDateTime'+gmtDateTime);
        TimeZone tz = UserInfo.getTimeZone();
        Integer userTimeZoneOffSet = tz.getOffset(gmtDateTime);
        System.debug('userTimeZoneOffSet '+userTimeZoneOffSet);
        return gmtDateTime.addMinutes(userTimeZoneOffSet/3600000);
    }
    global static void createResultsPDF (Map <ID, String> assessmentResultsURLMap, 
            Map <ID, String> assessmentNamesMap) {

        try {
            // Custom settings to get API key and URL of HTML to PDF rocket 
            TRHC__HTML_to_PDF_Settings__c settings = 
                    TRHC__HTML_to_PDF_Settings__c.getInstance (UserInfo.getUserID());
            List <Attachment> attachmentList = NEW List <Attachment> ();
            for (ID key: assessmentResultsURLMap.keySet ()) {
                HTTPResponse res = NEW HTTPResponse ();
                JSONGenerator gen = JSON.createGenerator(true); 
                gen.writeStartObject();
                gen.writeStringField('html_url',assessmentResultsURLMap.get(key));
                gen.writeNumberField('zoom',3);
                gen.writeEndObject();
                string jsonS = gen.getAsString();
                // Creating HTTP Request, to get results URL as PDF
                HTTPRequest httpReq = NEW HTTPRequest ();
                httpReq.setMethod ('GET');
                httpReq.setEndpoint (settings.TRHC__URL__c);
                httpReq.setMethod('POST');
                httpReq.setbody(jsonS);
                HTTP http = NEW HTTP ();
                System.debug('mmmmmmmmmmmmm'+assessmentResultsURLMap.get(key));
                //HTTPResponse res = NEW HTTPResponse ();
                // Sending HTTP Request, to get results URL as PDF
                if (!Test.isRunningTest ())
                    res = http.send (httpReq);
                // Setting status code and response for test class
                if (Test.isRunningTest()) {
                    res.setStatusCode(200);
                    res.setBodyAsBlob(Blob.valueOf('test'));
                }
                // Creating Attachment based on HTML to PDF rocket Response
                if (res.getStatusCode () == 200) {
                    string JSONString = res.getBody();
                    PDFResponse pdfObject 
                            = (PDFResponse)JSON.deserialize(JSONString, PDFResponse.class);
                    String pdf = pdfObject.body;
                    Attachment attachment = NEW Attachment ();
                    attachment.parentID = key;
                    attachment.Body = Encodingutil.base64decode(pdf);
                    attachment.Name = 'tr__'+assessmentNamesMap.get(key)+'.pdf';
                    attachmentList.add (attachment);
                }
            }
            Database.insert(attachmentList, false);
        } catch (Exception e) {
        }
    }
    public class PDFResponse {
        string statusCode;
        string body;
    }
}