@isTest
public class PeerReferencesControllerTest {
	private static TRHC.Logger logger = new TRHC.Logger('PeerReferencesControllerTest');
	
	@isTest
	private static void test() {
        Test.startTest();
        
		initData();		
		PeerReferencesController controller = new PeerReferencesController();
		controller.init();
		
		//System.assertEquals(null, controller.error);
		controller.con = new Contact();
		controller.con.LastName = 'TestconLast';
		insert controller.con;
		
		controller.referral1 = new Contact();
		controller.referral1.FirstName = 'TestFirst1';
		controller.referral1.LastName = 'TestLast1';
		controller.referral1.TRHC__Referred_By__c = controller.con.Id;
		
		controller.referral2 = new Contact();
		controller.referral2.FirstName = 'TestFirst1';
		controller.referral2.LastName = 'TestLast1';
		controller.referral2.TRHC__Referred_By__c = controller.con.Id;
		
		controller.referral3 = new Contact();
		controller.referral3.FirstName = 'TestFirst1';
		controller.referral3.LastName = 'TestLast1';
		controller.referral3.TRHC__Referred_By__c = controller.con.Id;
		controller.save();
        Test.stopTest();
	}
	
	private static void initData() {
				TRHC__Config_Settings__c config = new TRHC__Config_Settings__c (
				Name = 'Default',
				TRHC__Trigger_Disabled_Trigger_JobApplicants__c = true);
		insert config;
		
		TRHC__CustomSettingsComponent__c customSettings = new TRHC__CustomSettingsComponent__c(
				Name ='Default',
				TRHC__SessionTimeout__c = Decimal.valueOf('9999999'),
				TRHC__ErrorsReceiver__c = 'Test@fax.ru',
				TRHC__isShowPaginationCandidateList__c = true,
				TRHC__Maximum_No_of_References__c = '2');
		insert customSettings;
		
		TRHC__Page__c newPage = new TRHC__Page__c(
				Name = 'CMSDefaultProfilePage',
				TRHC__Name__c = 'CMSDefaultProfilePage',
				TRHC__Active__c = true);
		insert newPage;
		
		Account account = new Account(Name = 'CMSDefaultProfileAccount');
		insert account;
		
		TRHC__CMSProfile__c cmsProfile = new TRHC__CMSProfile__c(
				Name = 'Candidate',
				TRHC__Name__c = 'Candidate',
				TRHC__Profile_Manager__c = UserInfo.getUserId(),
				TRHC__Default_Home_Page__c = newPage.Id,
				TRHC__Profile_Account__c = account.Id,
				TRHC__CMSCss__c = 'CMSCssCustom1',
				TRHC__Portal_Enabled__c = true,
				TRHC__User_Registration__c = true);
		insert cmsProfile;
		
		User user = [
				SELECT Email 
				FROM User 
				WHERE Email = 'ankur824@gmail.com'  
				LIMIT 1];
		MetadataUtilNew md = new MetadataUtilNew(Contact.sObjectType);
		COntact candidate = new Contact(
				LastName = 'Test',
				FirstName = 'Test',
				Email = 'qwe@qwe.qwe',
				Phone = '111111',
				AccountId = account.Id,
				TRHC__CMSProfile__c = cmsProfile.Id,
				TRHC__Source__c = 'Blog',
				TRHC__Publish__c = true,
				TRHC__Recruiter__c = user.Id,
				TRHC__Client_Portal_Super_User__c = true,
				TRHC__UserName__c = 'qwe@qwe.qwe',
				TRHC__Password__c = '123123123',
				TRHC__Registration_Approved__c = true, 
				RecordTypeId = md.getRecordTypeIdByRecordTypeName('Candidate'));
		insert candidate;
		
		Contact clientContact = new Contact(
				LastName = 'Client',
				FirstName = 'Test',
				Email = 'qwe.client@qwe.qwe',
				Phone = '222222',
				AccountId = account.Id,
				TRHC__CMSProfile__c = cmsProfile.Id,
				TRHC__Source__c = 'Blog',
				TRHC__Publish__c = true,
				TRHC__Recruiter__c = user.Id,
				TRHC__UserName__c = 'qwe.client@qwe.qwe',
				TRHC__Password__c = '123123123',
				TRHC__Registration_Approved__c = true, 
				RecordTypeId = md.getRecordTypeIdByRecordTypeName('Client'));
		insert clientContact;
		
		TRHC__Job__c job = new TRHC__Job__c(
				TRHC__Job_Title__c = 'Test',
				TRHC__Start_Date__c = Date.today(),
				TRHC__Account_Lookup__c = account.Id);
		insert job;
		
		TRHC__Job_Applicant__c applicant = new TRHC__Job_Applicant__c(
				TRHC__Job__c = job.Id,
				TRHC__Contact_Candidate__c = candidate.id);
		insert applicant;

		//prepareSession(candidate.Id, '62486944141904');
		
		TRHC__CredentialsCustomSettings__c cs = new TRHC__CredentialsCustomSettings__c(
				Name = 'Default',
				TRHC__Disable_CredAnswerCopyAttachmentTrigger__c = true,
				TRHC__Portal_Certifications_Record_Types__c = 'Credential Master Template');
		insert cs;
		
		TRHC__Job_Applicant__c applicant1 = new TRHC__Job_Applicant__c(
				TRHC__Job__c = job.Id,
				TRHC__Contact_Candidate__c = candidate.id);
		insert applicant1;

		ApexPages.currentPage().getParameters().put('candidateId', candidate.Id);
		ApexPages.currentPage().getParameters().put('p','Candidate');
		ApexPages.currentPage().getParameters().put('Id', job.Id);
		ApexPages.currentPage().getParameters().put('jobId', job.Id);
		
		candidate = [
				SELECT FirstName, LastName, TRHC__Recruiter__r.Email, Email, Phone, 
						AccountId, TRHC__CMSProfile__c, TRHC__Source__c, TRHC__Publish__c, 
						TRHC__Recruiter__c, TRHC__Client_Portal_Super_User__c, 
						TRHC__UserName__c, TRHC__Password__c, RecordTypeId
				FROM Contact 
				WHERE Id = :candidate.id];
	}    
}