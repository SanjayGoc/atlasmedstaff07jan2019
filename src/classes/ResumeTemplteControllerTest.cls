/**
 * @author  chaitanya bollamreddi
 * @date 6 April 2018
 * @description class ResumeTemplteControllerTest is test class for ResumeTemplteController
 */
@isTest
public class ResumeTemplteControllerTest {
    /**
     * @author chaitanya bollamreddi
     * @date 6 April 2018
     * @name resumeTempPdfMethod
     * @param
     * @returntype Void
     * @description this method we are inserting the config settings, user, contact, Education Histories,
     * ------------ Employment Histories, Credentials and certification.
     */
    @isTest
    public static void resumeTempPdfMethod(){
        Id recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Candidate').getRecordTypeId();
        
        Profile profileId = [ SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1 ];
        
        User usr = new User ( LastName = 'LIVESTON',
                             FirstName='JASON',
                             Alias = 'jliv',
                             Email = 'chithu214@gmail.com',
                             Username = 'test@superhero.com',
                             ProfileId = profileId.id,
                             TimeZoneSidKey = 'GMT',
                             LanguageLocaleKey = 'en_US',
                             EmailEncodingKey = 'UTF-8',
                             LocaleSidKey = 'en_US');
        insert usr;
        TRHC__CredentialsCustomSettings__c configSettings =
					new TRHC__CredentialsCustomSettings__c();
        configSettings.Name = 'Default';
        configSettings.TRHC__Disable_CredAnswerCopyAttachmentTrigger__c = true;
        insert configSettings;
        TRHC__Config_Settings__c setting = new TRHC__Config_Settings__c();
        setting.Name = 'Default';
        //setting.dis
        setting.DisabletrackEventActivityonContact__c = false;
        setting.DisabletrackTaskActivityonContact__c = false;
        setting.Enable_Update_Recruiter__c = false;
        setting.DisabletrackNoteonContact__c = false;
        setting.Trigger_Enable_Expected_End_Date_Changer__c = false;
        setting.Enable_Update_Recruiter__c = true;
        setting.purchase_order_approve_users__c = usr.id;
        insert setting;
        TRHC__CustomSettingsComponent__c setting1 = new TRHC__CustomSettingsComponent__c(Name='Default');
        setting1.TRHC__JobListFastSearch__c = true ;
        setting1.TRHC__Count_Jobs_for_Job_List_Page__c = null ;
        setting1.TRHC__isShowPagination__c = true ;
        setting1.TRHC__Enable_Jobs_location_search__c = true ;
        setting1.TRHC__Job_List_Page_fields_for_query__c = 'TRHC__Job_Title__c,TRHC__Job_Term__c,TRHC__Job_Category__c,TRHC__Country_Locale__c,TRHC__Job_Summary__c,TRHC__Job_City__c,TRHC__State__c,Pay_Unit__c,Job_Type__c,TRHC__Account_Job__r.name,Pay_Rate_High__c,Pay_Rate_Low__c' ;
        setting1.TRHC__ErrorsReceiver__c = 'test@test.com';
        setting1.TRHC__SessionTimeout__c = Decimal.valueOf('9999999');
        setting1.TRHC__Job_Search_Agent_Field__c = 'Name,Job_Title__c';        
        setting1.MatchedJobsCriteria__c = 'TRHC__Publish__c = True';
        insert setting1;
        
        Account account = new Account();
        account.Name = 'SwTest';
        insert account;
        
        Contact contact = new Contact();
        contact.FirstName = 'chaitanya';
        contact.LastName = 'bollamreddy';
        contact.TRHC__Recruiter__c = UserInfo.getUserId();
        contact.Discipline1__c = 'RN';
        contact.TRHC__Discipline__c = 'RN';
        contact.TRHC__Candidate_Status__c = 'Active';
        contact.Compliance_Coordinater__c = usr.id;
        contact.OwnerId = usr.id;
        contact.AccountId = account.id;
        contact.Permanant_Address__c = 'Muniseb Street';
        contact.TRHC__Permanent_City__c = 'HYD';
        contact.TRHC__Permanent_Zip_Postal_Code__c = '533262';
        contact.Temporary_Address_1__c = '3rd cross papailayout';
        contact.TRHC__Temporary_City__c = 'Bangalore';
        contact.temporary_state__c = '';
        contact.TRHC__Temporary_Zip_Postal_Code__c = '560043';
        contact.RecordTypeId = recordTypeId;
        contact.Email = 'chaitanya.b@targetrecruit.net';
        contact.TRHC__Account_Manager__c = UserInfo.getUserId();
        insert contact;
        test.startTest();
        system.debug('@@@@@@'+contact.TRHC__Account_Manager__c);
        ResumeTemplateController controllerNew = new ResumeTemplateController();
        PageReference pageRefNew = Page.ResumeTemplateVfPage;
        pageRefNew.getParameters().put('Id', String.valueOf(contact.Id));
        pageRefNew.getParameters().put('EmpId', String.valueOf(contact.Id));
        pageRefNew.getParameters().put('EduId', String.valueOf(0));
        pageRefNew.getParameters().put('CertId', String.valueOf(0));
        pageRefNew.getParameters().put('LicId', String.valueOf(0));
        Test.setCurrentPage(pageRefNew);
        controllerNew.getEmployeedetails();
        controllerNew.updateEmployeeRecord();
        controllerNew.updateCertsRecord();
        controllerNew.updateLicensesRecord();
        controllerNew.updateCureentCandidate();
        controllerNew.updateEducationRecord();
        controllerNew.createNewEducation();
        controllerNew.createNewEmployment();
        controllerNew.createCertCredential();
        controllerNew.createLicenseCredential();
        //controllerNew.ignoreEducations;
        controllerNew.removeEduHist();
        controllerNew.removeCertsRecord();
        controllerNew.removeLicensesRecord();
        //controllerNew.ignoreEmployments;
        controllerNew.removeEmpHist();
        
        //ResumeTemplateHelper rTH =new ResumeTemplateHelper();
        
        String candiId = controllerNew.candiId;
        System.debug('candiId@@@'+candiId);
        System.PageReference strNew1 = controllerNew.generateCandidateCoverage();
        strNew1.getParameters().put('id',candiId);
        ResumeTemplateHelper rTH =new ResumeTemplateHelper();
        rTH.genEmailAttachment();
        test.stopTest();
    } 
}