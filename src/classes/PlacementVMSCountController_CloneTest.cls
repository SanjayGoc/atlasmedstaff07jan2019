@isTest
private class PlacementVMSCountController_CloneTest {
    
    @isTest static void test_method_one() {
        TRHC__Config_Settings__c configSettings = new TRHC__Config_Settings__c();
        configSettings.name='Default';
        configSettings.Enable_AutoMateJobRates__c = true;
        configSettings.Trigger_Enable_ScratchSheetAutoAddExp__c = false;
        configSettings.AutoAddExpenseCriteria__c='TRHC__Margin_Type__c=\'Expense\'';
        configSettings.Except_Hospitals__c = 'Atlas Development';
        configSettings.Trigger_Enable_Expected_End_Date_Changer__c = false;
        configSettings.Trigger_Enable_Applicant_From_SCR_Sheet__c = true;
        configSettings.Update_Pay_Burden__c = false;
        configSettings.Enable_RefferalTrigger__c = true;
        insert configSettings;

        Periodically_Date__c periodicallyDate = new Periodically_Date__c();
        periodicallyDate.Name = 'Period02';
        periodicallyDate.Start_Date__c = System.today()- 100;
        periodicallyDate.End_Date__c =System.today() + 100;
        insert periodicallyDate;

        Periodically_Date__c periodicallyDate1 = new Periodically_Date__c();
        periodicallyDate1.Name = 'Period01';
        periodicallyDate1.Start_Date__c = Date.newInstance(2017, 12, 24);
        periodicallyDate1.End_Date__c = Date.newInstance(2018, 1, 20);
        insert periodicallyDate1;

        Periodically_Date__c periodicallyDate2 = new Periodically_Date__c();
        periodicallyDate2.Name = 'ALL';
        periodicallyDate2.Start_Date__c = System.today()- 100;
        periodicallyDate2.End_Date__c =System.today() + 100;
        insert periodicallyDate2;

        Profile p = [SELECT Id FROM Profile WHERE Name =: 'System Administrator'];

        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com');

        List<Account> accountList = TestUtilityClass.getTestMultipleAccount();
        List<Contact> ContactList = TestUtilityClass.getTestMultipleContact(accountList[0]);
        //  List<TRHC__Contract__c> contractList = TestUtilityClass.getTestMultipleContract(accountList);

        TRHC__Placement__c placement = new TRHC__Placement__c();
        placement.TRHC__Recruiter__c = u.ID;
        placement.TRHC__Hiring_Manager__c = ContactList[2].Id;
        placement.TRHC__Employer__c = accountList[1].Id;
        //  placement.TRHC__Account_Contract__c = contractList[1].Id;
        placement.TRHC__Contact_Candidate__c = ContactList[1].Id;
        placement.TRHC__Start_Date__c = System.Today().addMonths(-3);
        placement.TRHC__End_Date__c = System.today();
        placement.CreatedDate = System.today() -1;
        placement.VMS__c = 'AMN';
        insert placement;

        PageReference myVfPage = Page.ClientManagerDashboardPage;
        myVfPage.getParameters().put('Name','Test ');
        //     myVfPage.getParameters().put('recNameForGPH','Test ');
        Test.setCurrentPage(myVfPage);
        Date startdatetime = System.today();
        string s = string.valueofGmt(startdatetime);


        PlacementVMSCountController_Clone placementObj = new PlacementVMSCountController_Clone();
        //placementObj.getpageParameter();
        //  placementObj.getaverageGphplacList();
        placementObj.numberOfWeek = s;
        placementObj.numberOfMonth = s;
        placementObj.numberOfWeekForRC  = s;
        placementObj.numberOfMonthForRC  = s;
        placementObj.numberOfMonthForRecAndCm  = s;
        placementObj.numberOfWeekForRecAndCm  = s;

        placementObj.getplacList();
        placementObj.showVMSAccountData();
        placementObj.redirectplac();
        placementObj.getMonthsMap();
        placementObj.generateDateFromMonth();
        placementObj.SelectPeriod(periodicallyDate.Name);
        placementObj.singlePickList = 'CM';
        placementObj.startDateOfWeek =  '2018-07-01';
        placementObj.startDateOfMonth = '2018-07-01';
        placementObj.selectedDaysForCM = periodicallyDate.Name;
        placementObj.getCount();

        placementObj.singlePickList = 'CM';
        placementObj.getExtensionData();
        placementObj.getMonthsOptions();
        placementObj.getRecruiterPeriodList();
        //  placementObj.getaverageGphplacList();
        //  placementObj.getGphCountchart();
        placementObj.YesterdaysGPH();

        placementObj.singlePickList = 'CmAndRec';

        placementObj.startDateOfWeekForRecAndCm = s;
        placementObj.startDateOfMonthForRecAndCm = s;
        placementObj.recAndCmPeriod = periodicallyDate.Name;
        placementObj.getCountRecandCm();
        placementObj.showAverageGPHData();
        
        apexpages.currentpage().getparameters().put('isPeriod','true');
        apexpages.currentpage().getparameters().put('averageGphPeriod','Period01');
        apexpages.currentpage().getparameters().put('numberOfMonthForGph','01');
        apexpages.currentpage().getparameters().put('selectedDaysForCM','Period01');
        apexpages.currentpage().getparameters().put('startDateOfWeek','2018-01-01');
        apexpages.currentpage().getparameters().put('startDateOfMonth','2018-01-01');
        apexpages.currentpage().getparameters().put('selectedDaysForRC','Period01');
        apexpages.currentpage().getparameters().put('startDateOfWeekForRC','2018-01-01');
        apexpages.currentpage().getparameters().put('startDateOfMonthForRC','2018-01-01');
        

        PlacementVMSCountController_Clone placementObj2 = new PlacementVMSCountController_Clone();
        placementObj2.singlePickList = 'Recruiter';
        placementObj2.startDateOfWeekForRC =  s;
        placementObj2.startDateOfMonthForRC = s;
        placementObj2.selectedDaysForRC = periodicallyDate.Name;
        placementObj2.getCount();  
        placementObj2.startDateOfMonthForGPH = s; 
        placementObj2.averageGphPeriod = periodicallyDate1.Name;
        placementObj2.numberOfMonthForGph = periodicallyDate1.Name;
        placementObj2.getAverageGph();
        placementObj2.getEndDate(1);
        apexpages.currentpage().getparameters().put('recNameForGPH','%20%27%22test%40%26%28%29%2B%23%2E%2D%2F%2C');
        placementObj2.getpageParameter();
        placementObj2.getaverageGphplacList();
        placementObj2.numberOfMonthForGph = 'All';
        placementObj2.getaverageGphplacList();
        placementObj2.getGphCountchart('12',new PlacementVMSCountController_Clone.GphCountchart(),120.3);
        Date monthdate = System.Today().addMonths(-2);
        placementObj2.SelectWeek(monthdate.year() + '-' + monthdate.month() + '-' + monthdate.day());

        placementObj2.isPeriod = false;
        placementObj2.getAverageGph();

        PlacementVMSCountController_Clone.CMPlacWrapper CMPlacWrapper = new PlacementVMSCountController_Clone.CMPlacWrapper('Rec',1,1,1);
        PlacementVMSCountController_Clone.RCPlacWrapper  RCPlacWrapper  = new PlacementVMSCountController_Clone.RCPlacWrapper ('Rec',1,1,1);
        PlacementVMSCountController_Clone.GphCountWrapper   GphCountWrapper   = new PlacementVMSCountController_Clone.GphCountWrapper  ('Rec',1.1);
        

    }
    
    @isTest static void test_method_two() {
        // Implement test code
    }
    
}