public with sharing class Helper_Email{
    public Static String getEmailBody (set<TRHC__Job__c> jobs,String emailbody) {
        List <String> fieldAPIs = NEW List<String> ();
        Map<string,string> nameValueMap = New Map<String,String> ();
        String emailbody1 = emailbody;
        system.debug(emailbody);
        
        String totalEmailBody = '';
        totalEmailBody = emailbody.substringBetween('<html>','</tr>');
        totalEmailBody = '<html>'+totalEmailBody+'</tr>';
        Integer countoffields = emailbody1.countMatches('{!TRHC__Job__c.');
        for (TRHC__Job__c job : jobs) {
            for (Integer i=0; i<countoffields; i++) {
                String fieldAPI = emailbody1.substringBetween('{!TRHC__Job__c.', '}');
                if (fieldAPI != NULL) {
                    
                    
                    fieldAPIs.add(fieldAPI);
                    
                    emailbody1 = emailbody1.remove('{!TRHC__Job__c.'+fieldAPI+'}');
                    
                }
            }
            emailbody1 = emailbody.substringBetween('<repeat>','</repeat>');
            emailbody1 = '<tr><td><div id="content" style="margin: 20px 0;"><repeat>'+emailbody1+'</repeat></div></td></tr>';
            //system.debug(emailbody1);
            //system.debug(fieldAPIs);
            for (integer i = 0; i < fieldAPIs.size(); i++)
            {
                String fieldAPIToReplace = fieldAPIs[i];
                String fieldValue = String.valueOf(job.get(fieldAPis[i].replace('TRHC__Job__c.', '')));
                //system.debug('values are...'+fieldValue);
                if (fieldValue != null && fieldValue != '') {
                    emailbody1 = emailbody1.replace('{!TRHC__Job__c.'+fieldAPIToReplace+'}', fieldValue );
                    //system.debug('1'+emailbody1);
                } else {
                    emailbody1 = emailbody1.replace('{!TRHC__Job__c.'+fieldAPIToReplace+'}', '' );
                    //system.debug('2'+emailbody1);
                }
                
            }
            
            totalEmailBody += String.isBlank(totalEmailBody)? emailbody1 : '\n\n'+emailbody1;
        }
        String body = emailbody.substringBetween('<div id="footer"','</html>');
            body = '<tr><td><div id="footer"'+body+'</html>';
        totalEmailBody = totalEmailBody + body;
        system.debug(totalEmailBody);
        return totalEmailBody;
    }
    

    
    
       public static String getAllFields(String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);
        
        if (objectType == null) {
            return fields;
        }
        for (string f :objectType.getDescribe ().fields.getMap ().keySet ())
            fields += f+ ', ';
        
        return fields.removeEnd(', '); 
        
    }
}