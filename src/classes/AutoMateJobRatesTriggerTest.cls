@isTest
public class AutoMateJobRatesTriggerTest {
    
    @isTest
    public static void autoRatesTest(){
        
        TRHC__Healthcare_Setting__c customSetting = new TRHC__Healthcare_Setting__c ();
        customSetting.Name = 'Default';
        customSetting.TRHC__Enable_GSA_Service_to_Update_M_I_Rate__c = False;
        insert customSetting;
        
        TRHC__Config_Settings__c configSettings = new TRHC__Config_Settings__c();
        configSettings.name='Default';
        configSettings.Enable_AutoMateJobRates__c = true;
        configSettings.Trigger_Enable_ScratchSheetAutoAddExp__c = false;
        configSettings.AutoAddExpenseCriteria__c='TRHC__Margin_Type__c=\'Expense\'';
        //configSettings.ExceptConfigIds__c = configObj1.Id;
        configSettings.Except_Hospitals__c = 'Atlas Development';
        configSettings.Trigger_Enable_Expected_End_Date_Changer__c = false;
        configSettings.Trigger_Enable_Applicant_From_SCR_Sheet__c = true;
        configSettings.Update_Pay_Burden__c = false;
        insert configSettings;
        
        Account account = new Account();
        account .name ='test';
        insert account;
        
        TRHC__Job__c job = new TRHC__Job__c();
        job.California_OT_After_8__c = true;
        job.TRHC__Account_Lookup__c = account.id;
        job.TRHC__County__c = 'india';
        job.TRHC__State__c = 'California';
        job.TRHC__Job_Title__c = 'indiatest';
        job.state_Pic__c ='California';
        job.TRHC__County__c ='text';
        job.TRHC__Hours_Per_Week__c = 36;
        job.California_OT_After_8__c = false;
        insert job;
        
        TRHC__Job__c job1 = new TRHC__Job__c();
        job1.California_OT_After_8__c = true;
        job1.TRHC__Account_Lookup__c = account.id;
        job1.TRHC__County__c = 'india';
        job1.TRHC__State__c = 'California';
        job1.TRHC__Job_Title__c = 'indiatest';
        job1.state_Pic__c ='California';
        job1.TRHC__County__c ='text';
        job1.TRHC__Hours_Per_Week__c = 40;
        job1.California_OT_After_8__c = false;
        insert job1;
        
		TRHC__Job__c job2 = new TRHC__Job__c();
        job2.California_OT_After_8__c = true;
        job2.TRHC__Account_Lookup__c = account.id;
        job2.TRHC__County__c = 'india';
        job2.TRHC__State__c = 'California';
        job2.TRHC__Job_Title__c = 'indiatest';
        job2.state_Pic__c ='California';
        job2.TRHC__County__c ='text';
        job2.TRHC__Hours_to_be_consider_per_day__c = 12;
        job2.California_OT_After_8__c = false;
        insert job2;

		TRHC__Job__c job3 = new TRHC__Job__c();
        job3.California_OT_After_8__c = true;
        job3.TRHC__Account_Lookup__c = account.id;
        job3.TRHC__County__c = 'india';
        job3.TRHC__State__c = 'California';
        job3.TRHC__Job_Title__c = 'indiatest';
        job3.state_Pic__c ='California';
        job3.TRHC__County__c ='text';
        job3.TRHC__Hours_to_be_consider_per_day__c = 8;
        job3.California_OT_After_8__c = false;
        insert job3;        
        
        TRHC__Job__c job4 = new TRHC__Job__c();
        job4.California_OT_After_8__c = true;
        job4.TRHC__Account_Lookup__c = account.id;
        job4.TRHC__County__c = 'india';
        job4.TRHC__State__c = 'California';
        job4.TRHC__Job_Title__c = 'indiatest';
        job4.state_Pic__c ='California';
        job4.TRHC__County__c ='text';
        job4.TRHC__Number_of_Days_Per_Week__c = 5;
        job4.California_OT_After_8__c = false;
        insert job4;

        TRHC__Job__c job5 = new TRHC__Job__c();
        job5.California_OT_After_8__c = true;
        job5.TRHC__Account_Lookup__c = account.id;
        job5.TRHC__County__c = 'india';
        job5.TRHC__State__c = 'California';
        job5.TRHC__Job_Title__c = 'indiatest';
        job5.state_Pic__c ='California';
        job5.TRHC__County__c ='text';
        job5.TRHC__Number_of_Days_Per_Week__c = 3;
        job5.California_OT_After_8__c = false;
        insert job5;
        
        TRHC__Job__c job6 = new TRHC__Job__c();
        job6.California_OT_After_8__c = true;
        job6.TRHC__Account_Lookup__c = account.id;
        job6.TRHC__County__c = 'india';
        job6.TRHC__State__c = 'California';
        job6.TRHC__Job_Title__c = 'indiatest';
        job6.state_Pic__c ='California';
        job6.TRHC__County__c ='text';
        job6.TRHC__Number_of_Days_Per_Week__c = 3;
        job6.TRHC__Hours_to_be_consider_per_day__c = 12;
        job6.California_OT_After_8__c = false;
        insert job6;
        
        //TRHC__Hours_to_be_consider_per_day__c
        //TRHC__Number_of_Days_Per_Week__c
    }
}