public with sharing class customLookupForEmailctrnl{
    
    public String searchtext { get; set; }
    public List<User> userValues{ get; set; }
    
    public customLookupForEmailctrnl() {
        searchtext = '';
        userValues = new List<User>();
        userValues = [Select id,Name, email FROM User];
    }
    
    public void search(){
        if(searchtext != '' && searchtext.length() > 2)
        {
            userValues = new List<User>();
            String searchStr = '%' + searchtext + '%';
            userValues = [Select id,Name, email FROM User Where Name like: searchStr];
        } else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please Enter atleast 2 character.'));
        }
    }
}