/* For SendEmailTemplate*/
public with sharing class sendEmailController {
    public String emailSubject{set;get;}
    public String emailBody{set;get;}
    public String emailTo{set;get;}
    public String emailCC{set;get;}
    public String emailBCC{set;get;}
    public Id recruiterID{get;set;}
    public String recruiterCCEmail{get;set;}
    public List<user> recruiterCCList {get;set;}
    public String selTempId;
    public List<SelectOption> templateList{get;set;}
    public List<SelectOption> fromSelList{get;set;}
    public String selFolderId{get;set;}
    public String selTemplateId {
        get;
        set;
    }
    
    public String selFromEmail {
        get;
        set;
    }

    public string WhatId {
        get;
        set;
    }

    Public String Id{get;set;}
    public Boolean isError{set;get;}
    public Map<String, String> expenseValuemap;
    public List<TRHC__Job__c> joblist;

    public sendEmailController() {
        Id = Apexpages.currentPage().getParameters().get('Id');
        
        selTemplateId = Apexpages.currentPage().getParameters().get('TemplateID');
        selTempId = selTemplateId;
        
        WhatId = emailTo = emailCC = '';
        WhatId  = Id;   
        isError = false;
        getEmailTemplatesByfolder();
        
        Set<String> expenseName = new Set<String>();
        expenseName.add('Housing Stipend');
        expenseName.add('Per Diem');
        expenseName.add('Travel Reimbursement');
        
        expenseValuemap = new Map<String, String>();
        List<TRHC__Expense__c> expenselist = [Select Id, TRHC__Value__c,TRHC__Expense_Name__c  FROM TRHC__Expense__c Where TRHC__Scratch_Sheet__c =: Id AND TRHC__Expense_Name__c In:expenseName];
        
        if(expenselist != null) {
            for(TRHC__Expense__c e : expenselist) {
                expenseValuemap.Put(e.TRHC__Expense_Name__c , String.valueOf(e.TRHC__Value__c));
            }
        }
        
        joblist = [Select id, Unit_name__c,Shift_Type2__c FROM TRHC__Job__c WHERE TRHC__Scratch_Sheet__c =: Id limit 1];
    }

    public void init(){
        emailSubject = '';
        emailBody = '';
        emailTo = '';
        emailCC = '';
        emailBCC = '';
        isError = false;

        //send dummy email to generate HTML body and Subject      
        List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        mail.setToAddresses(new String[] {'sanjayh.goc@gmail.com'});
        mail.setTemplateId(selTemplateId);
        System.debug('SELECTED TEMPLATE ID :: ' + selTemplateId);
        set<Id> conIdSet = new set<Id>();
        String accountName = '';
        for(TRHC__Scratch_Sheet__c ss :[select id,TRHC__Candidate__r.name,TRHC__Candidate__c,TRHC__Candidate__r.email,Account_name__c  from TRHC__Scratch_Sheet__c where id =: ID]) {
            conIdSet.add(ss.TRHC__Candidate__c);
            accountName = ss.Account_name__c ;
        }
        
        Account hospital = new Account();
        if(accountName != null && accountName != '')
            hospital = [select id,billingcity,billingState,Name from Account where name =: accountName limit 1];
        
        List < Contact > lstContact = [select id, name, email,Account.Direct_Billing_Email_Address__c,TRHC__Account_Name__c,Account.BillingCity,Account.BillingState from contact where id=: conIdSet];
        List < Contact > lstContactDonotDelete = [select id, name, email,Account.Direct_Billing_Email_Address__c from contact where name= 'Do Not Delete'];

        Id conId = null;
        if(lstContact.size()>0){
            conId = lstContact[0].id;
            emailTo = lstContact[0].email;
        //    emailCC = lstContact[0].Account.Direct_Billing_Email_Address__c;
        } else {
           if(lstContactDonotDelete.size() >0) {
               conId = lstContactDonotDelete[0].id;
               
            } else {
                List<Account> lstAcc = [select id from Account where Name='Do Not Delete'];
               
               Id AccId = null;
               if(lstAcc.size() > 0)
                     AccId = lstAcc[0].Id;
               else{
                     Account acc = new Account(Name='Do Not Delete');
                     insert acc;
                     AccId = acc.Id;
               }
                Contact c = new Contact(LastName='Do Not Delete',AccountId=AccId ,Email='ankurgarg@gmail.com');
                insert c;
                conId = c.Id;
            }
        }
        mail.setTargetObjectId(conId);
        mail.setWhatId(WhatId);
        mail.setSaveAsActivity(false);
        mailList.add(mail); 
                            
        try{
            
            Savepoint sp = Database.setSavepoint();
            
            if(!Test.isRunningTest()){
                
                try{
                    Messaging.sendEmail(mailList,true);
                }catch(Exception expp){
                //    System.debug('Error : '+expp);
                }
            }
                    Database.rollback(sp);
            
                
            
            if(mail.getHTMLBody() != null){
                emailBody= mail.getHTMLBody();
             }else{
                emailBody = mail.getPlainTextBody();
            }
            
            if(lstContact.size() == 0){
                emailTo = '';
            } 
            emailSubject = mail.getSubject();
            System.debug('===emailSubject==='+emailSubject);
            
            emailBody = expenseValuemap.containskey('Travel Reimbursement') ? emailBody.replace('!THours','$' + expenseValuemap.get('Travel Reimbursement')) : emailBody.replace('!THours','$0.00');
            emailBody = expenseValuemap.containskey('Housing Stipend') ? emailBody.replace('!HHours','$' +expenseValuemap.get('Housing Stipend')) : emailBody.replace('!HHours','$0.00');
            emailBody = expenseValuemap.containskey('Per Diem') ? emailBody.replace('!MHours','$' + expenseValuemap.get('Per Diem')) : emailBody.replace('!MHours','$0.00');
            
            System.debug('joblist: ' + joblist);
            emailBody = (!joblist.isEmpty() && jobList.get(0).Unit_name__c != null) ? 
                            emailBody.replace('!Job.Unit_name__c',jobList.get(0).Unit_name__c) :
                            emailBody.replace('!Job.Unit_name__c','');
                            
            
            emailBody = (!joblist.isEmpty() && jobList.get(0).Shift_Type2__c!= null) ? 
                            emailBody.replace('!Job.Shift_Type2__c',jobList.get(0).Shift_Type2__c) :
                            emailBody.replace('!Job.Shift_Type2__c','');
            
            emailBody = accountName == null ? emailBody.replace('!Contact.TRHC__Account_Name__c', '') : emailBody.replace('!Contact.TRHC__Account_Name__c',accountName);
            emailBody = hospital == null || hospital.BillingCity == null ? emailBody.replace('!Account.City','') : emailBody.replace('!Account.City',hospital.BillingCity);
            emailBody = hospital == null || hospital.BillingState == null? emailBody.replace(', !Account.State','') : emailBody.replace(', !Account.State',', ' + hospital.BillingState);
            
            
        }catch(Exception exp){
            isError = true;
            system.debug('##### emailBody = '+exp.getMessage());
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,exp.getMessage() ));
        }
  
    }

    public Pagereference cancel(){
        if(WhatId != null){
           return new Pagereference('/'+WhatId);
        }
        return null;
    }

     public Pagereference sendEmail(){
        system.debug('######## emailTo = '+emailTo);
        system.debug('######## emailCC = '+emailCC);
        system.debug('######## emailBCC = '+emailBCC);
        system.debug('######## emailSubject= '+emailSubject);
        system.debug('--emailBody --'+emailBody );
        
        if(emailTo == null || emailTo.trim().length() == 0){
           isError = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Email Address Not Found'));
           return null;
        }

        if(emailSubject == null || emailSubject.trim().length() == 0){
           isError = true;
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Email Subject is Required.'));
           return null;
        }
        
        if(emailBody == null || emailBody.trim().length() == 0){
           isError = true;
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Email body is required.'));
           return null;
        }
        
        // Attach Attachments
        list < Messaging.Singleemailmessage > emails = new list < Messaging.Singleemailmessage > ();
        

        List<String> toAddress = new List<String>();
        if(emailTo != null && emailTo.trim().length() > 0){
            for(string s :emailTo.split(',')){
                if(s!=null && s!='')
                   toAddress.add(s); 
            }
            
        }
        if(toAddress.size() > 0){
            List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            email.setSaveAsActivity(false);
            email.setSubject(emailSubject);
            email.setHtmlBody(emailBody);
            email.setToAddresses(toAddress);
            String attachmentUrl = '';
            System.debug('--select-- == '+ selFromEmail);
         
            if(emailCC != null && emailCC.trim().length() > 0){
                email.setCcAddresses(emailCC.split(','));
            }
            if(emailBCC != null && emailBCC.trim().length() > 0){
                email.setBccAddresses(emailBCC.split(','));
            }

           
            /*if (attachmentUrl != '') {
                attachmentUrl = '<br/><b>Download Attachments Here:</b><br/>' + attachmentUrl;
            }*/
                
            email.setHtmlBody(emailBody + attachmentUrl);
            mailList.add(email);
            System.debug('===mailList==='+mailList);
            
            try{
                //Savepoint sp = Database.setSavepoint();
                if(!Test.isRunningTest()){
                    try{
                        System.debug('===mailList==='+mailList);
                        Messaging.sendEmail(mailList,true);
                    }catch(Exception expp){
                        System.debug('===mailList==='+expp);
                    }
                }
                //Database.rollback(sp);
                if(email.getHTMLBody() != null){
                        emailBody= email.getHTMLBody();
                 }else{
                        emailBody = email.getPlainTextBody();
                }
                
            }catch(Exception exp){
                isError = true;
                Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,exp.getMessage()));
            }
        
            if(WhatId != null)
                return new Pagereference('/'+WhatId);
        }
        return null;
    }


    public void getEmailTemplatesByfolder(){
       templateList = new List<SelectOption>();
       templateList.add(new SelectOption('','--Select Template--'));
       //templateList.add(new SelectOption('00X0q000000aUqpEAE','Atlas MedStaff scratch sheet Summary'));
       
       List<EmailTemplate> emailTemplatelist = [Select Id, Name, Subject, TemplateType, FolderId, Folder.Name From EmailTemplate Where IsActive = true And Developername =: 'Atlas_MedStaff_scratch_sheet_Summary' order by Name];
       for(EmailTemplate et : emailTemplatelist){
          templateList.add(new SelectOption(et.Id, et.Name));
       }
    }

    public PageReference reloadPage(){
       PageReference pg = null;
       
       pg = Page.sendEmail;
       pg.getParameters().put('id', ID);
       pg.getParameters().put('TemplateID', selTemplateId);
       pg.setRedirect(true);
       return pg;
    }

   
}