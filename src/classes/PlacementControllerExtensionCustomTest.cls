@isTest
public class PlacementControllerExtensionCustomTest {
    @isTest
    public Static void coverPlacementController(){
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Accounting'].Id,
            LastName = 'last',
            Email = 'userTest@atlasmedstaff.com',
            Username = 'userTest@atlasmedstaff.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
        );
        
        insert u;
        
        TRHC__Config_Settings__c config = new TRHC__Config_Settings__c(Name = 'Default');
        config.Enable_Update_Recruiter__c = False;
        config.Assignment_Work_Order_Approve_Users__c = u.id;
        insert config;        
        
        Contact candidate_Placemnet = new Contact();
        candidate_Placemnet.RecordTypeId = [SELECT Id FROM RECORDTYPE WHERE NAME = 'Candidate' AND SobjectType ='Contact' Limit 1].id;
        candidate_Placemnet.FirstName = 'TestCandidate';
        candidate_Placemnet.LastName = 'PlacementController';
        insert candidate_Placemnet;
        
        TRHC__Scratch_Sheet__c scratchSheet = new TRHC__Scratch_Sheet__c();
        scratchSheet.Name = 'TestScratchSheet_test_Placement';
        insert scratchSheet;
        
        TRHC__Expense__c expense = new TRHC__Expense__c();
        expense.TRHC__Expense_Name__c = 'Housing Stipend';
        expense.TRHC__Hourly_Amount__c = 40.00;
        expense.TRHC__Scratch_Sheet__c = scratchSheet.id;
        insert expense;
        TRHC__Expense__c expensePer = new TRHC__Expense__c();
        expensePer.TRHC__Expense_Name__c = 'Per Diem';
        expensePer.TRHC__Hourly_Amount__c = 40.00;
        expensePer.TRHC__Scratch_Sheet__c = scratchSheet.id;
        insert expensePer;
        TRHC__Placement__c placement_scratchSheet_expenses = new TRHC__Placement__c();
        placement_scratchSheet_expenses.TRHC__Scratch_Sheet__c =  scratchSheet.id;
        placement_scratchSheet_expenses.TRHC__Contact_Candidate__c = candidate_Placemnet.id;
        placement_scratchSheet_expenses.TRHC__Start_Date__c = Date.today();
        placement_scratchSheet_expenses.TRHC__End_Date__c = placement_scratchSheet_expenses.TRHC__Start_Date__c + 180;
        placement_scratchSheet_expenses.createdDate = Date.today();
        insert placement_scratchSheet_expenses;
        ApexPages.StandardController sc = new ApexPages.StandardController(placement_scratchSheet_expenses);
        PlacementControllerExtensionCustom customPLC = new PlacementControllerExtensionCustom(sc);
        ApexPages.currentPage().getParameters().put('id', String.valueOf(placement_scratchSheet_expenses.id));
        
        AssignmentWorkOrderPdfController asController = new AssignmentWorkOrderPdfController(sc);
        asController.capturePlacementData();
      	customPLC.selectedUser = u.LastName;
        List<String> usersList = new List<String>();
        usersList.add(u.LastName);
        customPLC.selectedUsers = usersList;
        customPLC.rightvalues = new set<String>(usersList);
        customPLC.selectclick();
        customPLC.capturePlacementData();
        customPLC.getNotifyTeam();
        
        //placement_scratchSheet_expenses. 
    }
}