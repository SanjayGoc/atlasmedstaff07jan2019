/**
* @author : Chaitanya Bollamreddi
* @date 13 April 2018
* @descrption : Test class for the controller XMLFeedForJobsController2
*/
@isTest
public class XMLFeedForJobsController2Test {
    /**
* @Author: chaitanya bollamreddi
* @date : 13 April 2018
* @return type : void
* @descrption : test method 
*/
    @istest
    public static void test() {
        Test.startTest();
        TRHC__Healthcare_Setting__c customSetting = new TRHC__Healthcare_Setting__c();
        customSetting.Name = 'Default';
        customSetting.TRHC__Enable_GSA_Service_to_Update_M_I_Rate__c = true;
        insert customSetting;
        TRHC__Config_Settings__c configSettings = new TRHC__Config_Settings__c();
        configSettings.name = 'Default';
        configSettings.Trigger_Enable_Expected_End_Date_Changer__c = false;
        insert configSettings;
        TRHC__CustomSettingsComponent__c CC = new TRHC__CustomSettingsComponent__c();
        CC.Name = 'Default';
        CC.Jobs_Feed_Criteria__c = 0;
        CC.TRHC__ErrorsReceiver__c = 'chaithu@gmail.com';
        insert CC;
        addJobs();
        XMLFeedForJobsController2 controller = new XMLFeedForJobsController2();
        system.Test.setCurrentPage(new pagereference('XMLFeedForJobs2'));
        controller.xmlFeed();
        //controller.xmlFeedFirstChoice();
        
    }    
    
    private static void addJobs() {
        Account account = new Account();
        account.Name = 'Job Account';
        insert account;
        
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'last',
            Email = 'test@gmail.com',
            Phone = '1234567890',
            Username = 'test@gmail.com' + System.currentTimeMillis(),
            CompanyName = 'Jordan Search Consultants',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
            //UserRoleId = r.Id
        );
        insert u;
        List<TRHC__Job__c> jobs = new List<TRHC__Job__c>();
        TRHC__Job__c job = new TRHC__Job__c();
        job.createdDate = system.today();
        job.TRHC__Job_Board_City__c = 'Nashville';
        job.TRHC__State__c = 'Tennessee=TN';
        job.TRHC__Job_Postal_Code__c = '37030';
        job.TRHC__Country_Locale__c = 'US=US';
        job.TRHC__Job_Description_Rich__c = 'The best job ever';
        job.TRHC__Job_Description__c = 'description';
        job.TRHC__Job_Publish_Tiny_Url__c = 'job.com';
        job.TRHC__Job_Term__c = '1 Week';
        
        job.TRHC__Recruiter__c = u.Id;
        job.TRHC__Account_Lookup__c = account.id;
        job.TRHC__Job_Category__c = 'Computers,Other';
        job.TRHC__Start_Date__c = System.today();
        job.TRHC__Stage__c = 'Open';
        job.TRHC__Publish__c = true;
        job.TRHC__Job_Title__c = 'Test';
        job.TRHC__Discipline__c = 'Advanced Practice Provider';
        job.Speciality1__c = 'CRNA';
        job.State_pic__c = 'Alabama';
        job.TRHC__State__c = 'Alabama';
        job.shift_type2__c = '7A-7P';
        job.TRHC__Pay_Rate__c = 20;
        job.TRHC__job_Type__c = 'Permanent';
        job.TRHC__Job_Application_Tiny_URL__c = 'http://test';
        jobs.add(job);
        
        TRHC__Job__c job1 = new TRHC__Job__c();
        job1.createdDate = Date.today();
        job1.TRHC__Account_Lookup__c = account.id;
        job1.TRHC__Job_Title__c = 'test';
        job1.TRHC__Publish__c = true;
        job1.TRHC__Stage__c = 'Open';
        jobs.add(job1);
        insert jobs;
        Test.stopTest();
    }
}