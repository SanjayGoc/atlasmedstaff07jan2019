@istest
public class UpdateGSAServiceBatchTest {
    
    public static testmethod void testUpdateGsaService(){
        TRHC__Config_Settings__c config = new TRHC__Config_Settings__c();
        config.Name = 'Default';
        config.Trigger_Enable_Applicant_From_SCR_Sheet__c = True;
        config.Update_Pay_Burden__c = True;
        config.Nurse_Gross_Weekly_Pay__c = 'Test Custom Setting';
        
        insert config;
        
        TRHC__Job__c job = new TRHC__Job__c();
        job.State_pic__c = 'Alaska';
        job.TRHC__County__c = 'Cook';
        job.TRHC__Stage__c = 'open';
        job.GSA_Lodging__c = 21 ;
        job.TRHC__Rate__c = '23' ;
        job.TRHC__Start_Date__c = System.today();
        job.TRHC__Expected_End_Date__c = System.today() + 30;
        
        insert job;

        Test.startTest();
        UpdateGSAServiceBatch btc = new UpdateGSAServiceBatch();
        DataBase.executeBatch(btc, 1);
        Test.stopTest();
     }
}