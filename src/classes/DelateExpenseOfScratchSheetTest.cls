@istest()
public class DelateExpenseOfScratchSheetTest {
    @istest()
    Public static void testDeleteExpense(){
        TRHC__Config_Settings__c config = new TRHC__Config_Settings__c();
        config.name = 'Default';
        config.Trigger_Enable_Expected_End_Date_Changer__c = false;
        config.Expense_deletion__c = true;
        insert config;
        
        TRHC__Scratch_Sheet__c newScratchSheetTemp = new TRHC__Scratch_Sheet__c();
        newScratchSheetTemp.TRHC__County__c = 'test';   
        newScratchSheetTemp.TRHC__State__c= 'California';    
        newScratchSheetTemp.Name='Test ScratchSheet';
        String scrtchSheetTempRecordTypeId = [Select Id From RecordType Where SobjectType = 'TRHC__Scratch_Sheet__c' and Name = 'ScratchSheet'].Id;
        newScratchSheetTemp.RecordTypeId=scrtchSheetTempRecordTypeId ;
        insert newScratchSheetTemp;
        
        delete newScratchSheetTemp;
    }
}