/***********************************************************
*  @ Author    : Hari
*  @ Descriptiopn  : Test class for ProphecyCheckListController
************************************************************/
@isTest
public class ProphecyCheckListControllerNewTest {
    static testMethod void insertRecords () {
        test.starttest();
        TRHC__Config_Settings__c v_enablecustomsettings = NEW  TRHC__Config_Settings__c ();
        v_enablecustomsettings.TRHC__Enable_Prophecy__c = true;
        v_enablecustomsettings.name = 'Default';
        v_enablecustomsettings.Enable_RecruitersMailNotificationTrigger__c = False;
        v_enablecustomsettings.Enable_Update_Recruiter__c = False;
        v_enablecustomsettings.DisabletrackEventActivityonContact__c = true;
        insert v_enablecustomsettings;
        Contact testContact = NEW Contact ();
        testContact.LastName = 'Test';
        testContact.TRHC__UserName__c = 'test@test.com';
        testContact.TRHC__Password__c = 'test';
        testContact.TRHC__Prophecy_CaregiverID__c = '1233';
        testContact.TRHC__SSN_New__c= 'test';
        testContact.Email = 'sai@test.com';
        testContact.Department = 'test';
        testContact.TRHC__Driver_Licence_Number__c = '123445';
        testContact.Phone = '9876543210';
        testContact.TRHC__Professional_References__c = true;
        testContact.TRHC__Personal_Information__c=true;
        testContact.TRHC__Advanced_Certifications__c=true;
        testContact.TRHC__Education_Histroy__c=true;
        testContact.TRHC__Employment_History__c=true;
        testContact.TRHC__Acceptance_of_Agreement__c=true;
        insert testContact;
        
        TRHC__CMSProfile__c cmsProfile = NEW TRHC__CMSProfile__c ();
        cmsProfile.TRHC__Force_com_Authorization_and_Registration__c = true;
        cmsProfile.TRHC__CMSCss__c = 'test';
        cmsProfile.TRHC__Name__c = 'test';
        insert cmsProfile;
        
        
        user u = [SELECT 
                  id, name 
                  FROM 
                  user 
                  WHERE 
                  id =:UserInfo.getUserId()
                 ];
        
        TRHC__Prophecy_Settings__c v_prophecysettings = NEW TRHC__Prophecy_Settings__c ();
        v_prophecysettings.TRHC__Department_ID__c = '21';
        v_prophecysettings.TRHC__Security_Code__c = 'ss';
        v_prophecysettings.TRHC__User_ID__c = '21';
        v_prophecysettings.SetupOwnerId = u.id;
        insert v_prophecysettings;
        
        ApexPages.currentPage().getParameters().put('id',testContact.id);
        ApexPages.StandardController standardController = NEW ApexPages.StandardController(testContact);
        
        ProphecySendAssessments.DynamicAssessmentObject aass = new ProphecySendAssessments.DynamicAssessmentObject(testContact.lastname, '56', testContact.lastname, '78', '54', '3');
        aass.selected = true;
        aass.costPoints = '50';
        aass.TimeLimit = '60';
        list < ProphecySendAssessments.DynamicAssessmentObject > a = new list < ProphecySendAssessments.DynamicAssessmentObject > ();
        set< ProphecySendAssessments.DynamicAssessmentObject > aSet = new set < ProphecySendAssessments.DynamicAssessmentObject > ();
        a.add(aass);
        List <String> assementsList = New List <String> ();
        assementsList.add('Test');
        ProphecySendAssessments pga = new  ProphecySendAssessments();
        pga.ListOfAllAssessmentsSentToProphecy = a;
        pga.AssessmentList = a;
        pga.Searchforassessments = '<object>' +
            '<success>true</success>' +
            '<count>1</count>' +
            '<assessments>' +
            '<assessment>' +
            '<numberofquestions>5</numberofquestions>' +
            '<costpoints>1</costpoints>' +
            '<assessmentname>Demo Assessment</assessmentname>' +
            '<assessmenttype>Test</assessmenttype>' +
            '<assessmentid>123456</assessmentid>' +
            '<timelimit>1300</timelimit>' +
            '</assessment>' +
            '</assessments>' +
            '</object>';
        pga.sendassessmentresponse = '<object>' +
            '<success>true</success>' +
            '<count>1</count>' +                    
            '<requests>' +
            '<request>' +
            '<caregiverid>123456</caregiverid>' +
            '<requestid>123456</requestid>' +
            '<assessments>' +
            '<assessment>' +
            '<caregiverassessmentid>123456</caregiverassessmentid>' +
            '<hashedexamid>123456</hashedexamid>' +
            '<assessmentname>Demo Assessment</assessmentname>' +
            '<assessmenttype>Test</assessmenttype>' +
            '<assessmentid>123456</assessmentid>' +
            '</assessment>' +
            '</assessments>' +
            '</request>' +
            '</requests>' +
            '</object>';
        pga.assesstypes = '<object><success>true</success><HashedExamId>132211<HashedExamId><clienttype>Clinical</clienttype><assessmenttypes><assessmenttype>Test</assessmenttype>'+
            '<assessmenttype>MandatoryTest</assessmenttype><assessmenttype>Allied</assessmenttype><assessmenttype>Checklist</assessmenttype></assessmenttypes></object>';
        pga.AssessmentCount = '5';
        pga.AssessmentIsActive = 'TRUE';
        List <String> types = NEW List <String> ();
        types.add('Test');
        types.add('Checklist');
        ProphecySendAssessments.Caregiver PAC = new ProphecySendAssessments.Caregiver();
        ProphecySendAssessments.Assessment PAA = new ProphecySendAssessments.Assessment();
        ProphecySendAssessments.ProphecyConnectCDT_cfc PAPC = new ProphecySendAssessments.ProphecyConnectCDT_cfc();
        ProphecySendAssessments.GetAssessmentTypes_element PAGA = new ProphecySendAssessments.GetAssessmentTypes_element();
        ProphecySendAssessments.GetAssessmentTypesResponse_element peophecySendTR = new ProphecySendAssessments.GetAssessmentTypesResponse_element();
        ProphecySendAssessments.SendAssessments_element v_prophecysettingsAER = new ProphecySendAssessments.SendAssessments_element();
        ProphecySendAssessments.SendAssessmentsResponse_element v_prophecysettingsATR = new ProphecySendAssessments.SendAssessmentsResponse_element();
        ProphecySendAssessments.GetCaregivers_element PGCE = new ProphecySendAssessments.GetCaregivers_element();
        ProphecySendAssessments.GetCaregiversResponse_element PGRCE = new ProphecySendAssessments.GetCaregiversResponse_element();
        ProphecySendAssessments.GetAssessmentsResponse_element peophecySendRE = new ProphecySendAssessments.GetAssessmentsResponse_element();
        ProphecySendAssessments.GetAssessments_element peophecySendAE = new ProphecySendAssessments.GetAssessments_element();
        //peophecySend.showassessments();
        
        ProphecyCheckListControllerNew skillsCheckList = NEW ProphecyCheckListControllerNew ();
        skillsCheckList.v_ContactSobj = testContact;
        skillsCheckList.contact = testContact;
        Boolean ren = skillsCheckList.render;      
        skillsCheckList.GetCountValueFromSearchCaregiverResponse = 'test';
        skillsCheckList.GetIsActiveValueFromSearchCaregiverResponse = 'test';
        skillsCheckList.GetCareGiverIdValueFromCreateCaregiverResponse = 'test';
        skillsCheckList.ResponseOfSearchCaregivers = 'test';
        skillsCheckList.ResponseOfCreateCaregivers = 'test';
        skillsCheckList.ResponseOfSearchCaregiversForCountEqualsOne = 'test';
        skillsCheckList.ResponseOfSearchCaregiversForIsActiveEqualsFalse = 'test';
        skillsCheckList.v_ProphecyCustomSettings = v_prophecysettings;
        skillsCheckList.resultantURL = 'test';
        skillsCheckList.responseURL = NEW Map<String, Object> ();
        skillsCheckList.responseURL.put('test',testContact);
        skillsCheckList.resultantURLWithHttps = 'test';
        skillsCheckList.selectLanguage = 'test'; 
        skillsCheckList.AssessmentList = a;
        try{   
            skillsCheckList.init();
        }
        catch (Exception e) {
            
        }
        skillsCheckList.processAssessment();
        test.stoptest();
    }
}