//////////////////////////////////////////////////////////////////////////////
/* JSON : 
        { 
            "crtLists" : {
                "firstName": "Ankur",
                "lastName": "Garg",
                "phoneNumber": "123456789",
                "email": "test@test.com",
                "about": "Admin",
                "discipline": "RN",
                "primarySpecialty": "Cath Lab RN",
                "primarySpecialtyYears": 3,
                "secondarySpecialty": "Cath Lab RN",
                "secondarySpecialtyYears": 2,
                "startDate": "2018-11-01",
                "targetWeeklyPay": "1800.00",
                "shiftPreference": ["12 hour nights","8 hour nights"],
                "licenseLocations": ["10","12"],
                "homeState": "Ohio",
                "documentLists": [{
                    "type" : ["resume"],
                    "firstName" : "documentFile",
                    "fileStr" : "google.com"
                    }],
                "jobList": [{
                    "id" : "1234576789",
                    "hospitalName": "Taj",
                    "hospitalCity": "NJ",
                    "hospitalState": "Ohio"
                    }],
                "recuiterList": [{
                    "firstName": "Ankur",
                    "lastName": "Garg",
                    "email": "ankur.garg@gmail.com"
                    }]
            }
        }
        - Go to Werkbench and login
        - go to Utility and click rest explorer
        - select Post and set URL : /services/apexrest/NurseFlyer/
        - Set Body and execute*/ 
////////////////////////////////////////////////////////////////////////////

@RestResource(urlMapping='/NurseFlyer/*')
global class CreateContact {
    global class CreateDetails {
        global String firstName;
        global String lastName;
        global String phoneNumber;
        global String email;
        global String about;
        global String discipline;
        global String primarySpecialty;
        global Integer primarySpecialtyYears;
        global String secondarySpecialty;
        global Integer secondarySpecialtyYears;
        global String targetWeeklyPay;
        global String startDate;
        global String homeState;
        global List<String> shiftPreference;
        global List<String> licenseLocations;
        global List<CreateDocument> documentLists;
        global List<CreateJob> jobList;
        global List<CreateRecuiter> recuiterList;
    }
    
    //Wrapper Class Create Documents
    global class CreateDocument {
        public String firstName;
        public List<String> type;
        public String fileStr;
    }
    
    //Wrapper Class CreateJob
    global class CreateJob {
        public String id;
        public String hospitalName;
        public String hospitalCity;
        public String hospitalState;
    }
    
    //Wrapper Class CreateRecruiter
    global class CreateRecuiter {
        public String firstName;
        public String lastName;
        public String email;
    }
    
    @HttpPost
    global static String createContactMethod(CreateDetails crtLists) {
        Contact createNurse = new Contact();
        createNurse.FirstName = crtLists.firstName;
        createNurse.LastName = crtLists.lastName;
        createNurse.Phone = crtLists.phoneNumber;
        createNurse.Email = crtLists.email;
        createNurse.Discipline1__c = crtLists.discipline;
        createNurse.Description = crtLists.about;
        createNurse.TRHC__Specialty__c = crtLists.primarySpecialty;
        createNurse.TRHC__Primary_Specialty_Years_Of_Experience__c = String.valueof(crtLists.primarySpecialtyYears);
        createNurse.Secondary_Speciality__c = crtLists.secondarySpecialty;
        createNurse.TRHC__Secondary_Specialty_Years_Of_Experience__c = String.valueof(crtLists.secondarySpecialtyYears);
        createNurse.TRHC__Available_To_Start__c = Date.valueOf(crtLists.startDate);
        createNurse.State_pic__c = crtLists.homeState;
        createNurse.TRHC__Current_Pay__c = double.valueof(crtLists.targetWeeklyPay);
        createNurse.LeadSource = 'Nurse Fly';
        List<String> shifts = crtLists.shiftPreference;
        if(!shifts.isEmpty()) {
            String shifPrefer = '';
            for(Integer i=0; i<shifts.Size();i++) {
                shifPrefer = shifPrefer + shifts[i] + ';';
            }
            shifPrefer=shifPrefer.removeEnd(';');
            createNurse.TRHC__Shift_Preference__c = shifPrefer;
        }
        //List<String> locationList = crtLists.licenseLocations;
        /*if(!locationList.isEmpty()) {
            String locationPrefer = '';
            for(Integer i=0; i<locationList.Size();i++) {
                locationPrefer = locationPrefer + locationList[i] + ';';
            }
            locationPrefer = locationPrefer.removeEnd(';');
            createNurse.Desired_Location2__c = locationPrefer;
        }*/
        insert createNurse;
        List<String> types = new List<String>();
        String str;
        List<String> docString;
        List<CreateDocument> docList = crtLists.documentLists;
        if(!docList.isEmpty()) {
            docString = new List<String>();
            for(CreateDocument doc : docList) {
                str = '';
                types = doc.type;
                str = doc.firstName + ';' + types[0] + ';' + doc.fileStr;
                docString.add(str);
            }
        }
        attachlist(createNurse.Id, docString);
        String fName, lName, fullName, recruiterResponse = '';      
        List<CreateRecuiter> recList = crtLists.recuiterList;
        if(!recList.isEmpty()) {
            for(CreateRecuiter rec : recList) {
                fName = rec.firstName;
                lName = rec.lastName;
            }
            fullName = fName + ' ' + lName;
            List<Contact> recruiter = [SELECT Id, Name FROM Contact WHERE NAME =: fullName LIMIT 1];
            if(recruiter != null && recruiter.size() > 0) {
                recruiterResponse = 'Id: ' + recruiter[0].Id + ', Name: ' + recruiter[0].Name;
                return recruiterResponse;
            } else {
                recruiterResponse = 'No Such type of recruiter';
                return recruiterResponse;
            }
        }
        recruiterResponse = 'No Such type of recruiter';
        return recruiterResponse;
    }
    
    public static void attachlist(Id conId, List<String> conAttachList) {
        List<Attachment__c> attachNurseList = new List<Attachment__c>();
        Attachment__c attachNurse;
        for(String s : conAttachList) {
            List<String> expan = s.split(';');
            attachNurse = new Attachment__c();
            attachNurse.Name = expan[0];
            attachNurse.Contact_Nurse__c = conId;
            attachNurse.Document_File_Source__c = expan[2];
            attachNurse.Document_Type__c = expan[1];
            attachNurseList.add(attachNurse);
        }
        if(!attachNurseList.isEmpty()) {
            insert attachNurseList;
        }
    }
}