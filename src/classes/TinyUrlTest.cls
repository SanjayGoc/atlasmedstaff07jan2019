@isTest
public class TinyUrlTest {
    private static testmethod void testTinyUrl()
    {
        Test.setMock(HttpCalloutMock.class, new TinyUrlTestMock());
        
        TRHC__Config_Settings__c cs = TRHC__Config_Settings__c.getOrgDefaults();
        cs.Name = 'Default';
        cs.Trigger_Enable_Expected_End_Date_Changer__c = false;
        insert cs;
        
        TRHC__Call_Em_All__c trSetting = TRHC__Call_Em_All__c.getOrgDefaults();
        trSetting.TRHC__Enable_Auto_Call__c = false;
        upsert trSetting TRHC__Call_Em_All__c.Id;
        
        List<ID> jobIds = new list<ID>();
        
        TRHC__Job__c job = new TRHC__Job__c(
                TRHC__Job_Title__c = 'Test',
                TRHC__Start_Date__c = Date.today(),
                TRHC__Expected_End_Date__c = Date.today()+7);
        insert job;         
        
        jobIds.add(job.id); 
       
        TRHC__Job__c job1 = new TRHC__Job__c(
                TRHC__Job_Title__c = 'Test-1',
                TRHC__Start_Date__c = Date.today(),
                TRHC__Expected_End_Date__c = Date.today()+15);
        insert job1;        
        
        jobIds.add(job1.id);
        
        Test.startTest();

        TinyUrl.fillTinyUrl(jobIds);
        
        Test.stopTest();
    }

    public class TinyUrlTestMock implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setBody('<response><status_code>200</status_code><status_txt>OK</status_txt><data><url>http://bit.ly/1tOLNSB</url><hash>1tOLNSB</hash><global_hash>1tOLNSC</global_hash><long_url>http://target-recruit-unmanag-developer-edition.na11.force.com/CMSLayout</long_url><new_hash>0</new_hash></data></response>');
            return res;
        }
    }
    }