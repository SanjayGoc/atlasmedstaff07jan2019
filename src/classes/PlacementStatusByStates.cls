public class PlacementStatusByStates {
    public List<TRHC__Placement__c> listPlacement {get;set;}
    public Map<String,String> countryStateKey        {get; set;}
    public Map<String,Integer> hotJobMap             {get; set;}
    public String jsonStateMap                       {get; set;}
    public Integer hotJob                            {get; set;}
    public String jsonHotJob                         {get; set;}
    public Map<String,Integer> stateOfmap             {get; set;}
    public Integer activePlacement = 0;
    public Integer countNormal                       {get; set;}
    public Map<String,Set<String>> mapStateToSpeciality {get;set;}
    public String jsonStateToSpeciality {get;set;}
    public Map<String,Map<String,AvailableJobsWrapper>> jobAvailibilityMap{get;set;}
    
    public PlacementStatusByStates () {
        activePlacement = 0;
        listPlacement = [Select Id,Name,Active__c,Hospital_State__c,TRHC__Specialty__c from TRHC__Placement__c where TRHC__Status__c = 'Active' AND Hospital_State__c != null];
        mapStateToSpeciality = new map<String,Set<String>>();
        jobAvailibilityMap = new Map<String,Map<String,AvailableJobsWrapper>>();
        stateOfmap = new Map<String,Integer>();
        hotJobMap = new Map<String,Integer>();
        getcountryStateKey();

        for(TRHC__Placement__c place : listPlacement) {
            activePlacement = 0;
            if(countryStateKey.containskey(place.Hospital_State__c)){

                if(stateOfmap.containsKey(countryStateKey.get(place.Hospital_State__c))) {
                    activePlacement = stateOfmap.get(countryStateKey.get(place.Hospital_State__c)) + 1;
                    stateOfmap.put(countryStateKey.get(place.Hospital_State__c),activePlacement);
                    system.debug('stateOfmap if'+stateOfmap);
                    system.debug('stateOfmap if'+stateOfmap.keySet());
                    system.debug('stateOfmap if'+stateOfmap.values());
                } else {
                    system.debug('stateOfmap else');    
                    stateOfmap.put(countryStateKey.get(place.Hospital_State__c),1);
                    system.debug('stateOfmap else'+stateOfmap);
                    system.debug('stateOfmap else'+stateOfmap.keySet());
                    system.debug('stateOfmap else'+stateOfmap.values());
                }
            }
        }

        for(String mapKey : countryStateKey.keySet()) {
            if(!stateOfmap.containsKey(countryStateKey.get(mapKey))) {
                stateOfmap.put(countryStateKey.get(mapKey),0);
            }
        }

        jsonStateMap = JSON.serialize(stateOfmap);
        
        jsonHotJob = JSON.serialize(hotJobMap);
      
    }

    public void getcountryStateKey() {
        countryStateKey = new Map<String,String>();
        countryStateKey.put('Alabama','AL');
        countryStateKey.put('Alaska','AK');
        countryStateKey.put('Alberta','AB');
        countryStateKey.put('American Samoa','AS');
        countryStateKey.put('Arizona','AZ');
        countryStateKey.put('Arkansas','AR');
        countryStateKey.put('Armed Forces (AE)','AE');
        countryStateKey.put('Armed Forces Americas','AA');
        countryStateKey.put('Armed Forces Pacific','AP');
        countryStateKey.put('British Columbia','BC');
        countryStateKey.put('California','CA');
        countryStateKey.put('Colorado','CO');
        countryStateKey.put('Connecticut','CT');
        countryStateKey.put('Delaware','DE');
        countryStateKey.put('District Of Columbia','DC');
        countryStateKey.put('Florida','FL');
        countryStateKey.put('Georgia','GA');
        countryStateKey.put('Guam','GU');
        countryStateKey.put('Hawaii','HI');
        countryStateKey.put('Idaho','ID');
        countryStateKey.put('Illinois','IL');
        countryStateKey.put('Indiana','IN');
        countryStateKey.put('Iowa','IA');
        countryStateKey.put('Kansas','KS');
        countryStateKey.put('Kentucky','KY');
        countryStateKey.put('Louisiana','LA');
        countryStateKey.put('Maine','ME');
        countryStateKey.put('Manitoba','MB');
        countryStateKey.put('Maryland','MD');
        countryStateKey.put('Massachusetts','MA');
        countryStateKey.put('Michigan','MI');
        countryStateKey.put('Minnesota','MN');
        countryStateKey.put('Mississippi','MS');
        countryStateKey.put('Missouri','MO');
        countryStateKey.put('Montana','MT');
        countryStateKey.put('Nebraska','NE');
        countryStateKey.put('Nevada','NV');
        countryStateKey.put('New Brunswick','NB');
        countryStateKey.put('New Hampshire','NH');
        countryStateKey.put('New Jersey','NJ');
        countryStateKey.put('New Mexico','NM');
        countryStateKey.put('New York','NY');
        countryStateKey.put('Newfoundland','NF');
        countryStateKey.put('North Carolina','NC');
        countryStateKey.put('North Dakota','ND');
        countryStateKey.put('Northwest Territories','NT');
        countryStateKey.put('Nova Scotia','NS');
        countryStateKey.put('Nunavut','NU');
        countryStateKey.put('Ohio','OH');
        countryStateKey.put('Oklahoma','OK');
        countryStateKey.put('Ontario','ON');
        countryStateKey.put('Oregon','OR');
        countryStateKey.put('Pennsylvania','PA');
        countryStateKey.put('Prince Edward Island','PE');
        countryStateKey.put('Puerto Rico','PR');
        countryStateKey.put('Quebec','QC');
        countryStateKey.put('Rhode Island','RI');
        countryStateKey.put('Saskatchewan','SK');
        countryStateKey.put('South Carolina','SC');
        countryStateKey.put('South Dakota','SD');
        countryStateKey.put('Tennessee','TN');
        countryStateKey.put('Texas','TX');
        countryStateKey.put('Utah','UT');
        countryStateKey.put('Vermont','VT');
        countryStateKey.put('Virgin Islands','VI');
        countryStateKey.put('Virginia','VA');
        countryStateKey.put('Washington','WA');
        countryStateKey.put('West Virginia','WV');
        countryStateKey.put('Wisconsin','WI');
        countryStateKey.put('Wyoming','WY');
        countryStateKey.put('Yukon Territory','YT');
    }
    
    public class AvailableJobsWrapper{
        public Integer hotJob{get;set;}
        public Integer availableJob{get;set;}
        
        
        public AvailableJobsWrapper(){
            hotJob = 0;
            availableJob = 0;
        }
    }
}