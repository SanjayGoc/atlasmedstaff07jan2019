@isTest 
public class PopulateAllowanceOnSubmittalHandler_Test {
	@isTest
	public static void testmethod1() {

        TRHC__Healthcare_Setting__c customSetting =  new TRHC__Healthcare_Setting__c();
        customSetting.name = 'Default';
    	customSetting.TRHC__Enable_GSA_Service_to_Update_M_I_Rate__c = true;
    	insert customSetting;

    	TRHC__Config__c configObj1 = new TRHC__Config__c();
        configObj1.TRHC__Name__c = 'check';
        configObj1.TRHC__Margin_Type__c = 'Expense';
        configObj1.TRHC__Frequency__c = 'Weekly';
        configObj1.TRHC__State__c =   'California';
        configObj1.TRHC__Include_in_payroll__c= True;
        configObj1.TRHC__Pay_Period_to_be_paid__c='First Check';
        configObj1.TRHC__Active__c = True;
        configObj1.TRHC__Value__c = '350.0';
        configObj1.TRHC__Grouping_Total__c ='Pay Burden';
        configObj1.TRHC__City__c = 'test';configObj1.RecordTypeId =[Select Id From RecordType Where SobjectType = 'TRHC__Config__c' and Name = 'Margin calculation Line Item'].Id;
        insert configObj1;

		TRHC__Config_Settings__c configSettings = new TRHC__Config_Settings__c();
        configSettings.name='Default';
        configSettings.Enable_AutoMateJobRates__c = true;
        configSettings.Trigger_Enable_ScratchSheetAutoAddExp__c = false;
        configSettings.AutoAddExpenseCriteria__c='TRHC__Margin_Type__c=\'Expense\'';
        configSettings.ExceptConfigIds__c = configObj1.Id;
        configSettings.Except_Hospitals__c = 'Atlas Development';
        configSettings.Trigger_Enable_Expected_End_Date_Changer__c = false;
        configSettings.Trigger_Enable_Applicant_From_SCR_Sheet__c = true;
        configSettings.Update_Pay_Burden__c = false;
        insert configSettings;

         FireBaseServerKeyMobile__c server  = new FireBaseServerKeyMobile__c();
        server.Name = 'Default';
        server.serverKey__c = 'AAAAr1KJK-k:APA91bFeuugxXEKJwAEKQDbo7yQ76RXAzxH0wpsGAKr0Df6C1eXItHrb6hd6HH_QsYXGgPIPQs3baLLsu6VEDRgTAa366YjGT8_Pg6mHganNtNDl1zRh289wYu6-uWpfo65azYNzqf2D';
        insert server;

		//Get User Record        
        List<User> usr = [SELECT Id,Name,Email,FirstName,LastName FROM User WHERE IsActive = true LIMIT 3];
        
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;

        //Create Candidate/Nurse Record
        Contact con = new Contact();
        con.Accountid = acc.Id;
        con.FirstName = 'mike'; 
        con.LastName = 'saas';
        con.Email = 'test@test1.gmail.com';
        con.Compliance_Coordinater__c = usr[0].id;
        con.TRHC__Recruiter__c = usr[1].id;
        insert con;

        TRHC__Job__c job = new TRHC__Job__c();
        job.TRHC__Stage__c = 'closed';
        job.CreatedDate = Date.today();
        job.TRHC__Number_of_Placements__c = 1;
        insert job;

        TRHC__Scratch_Sheet__c newScratchSheetTemp1 = new TRHC__Scratch_Sheet__c();
        newScratchSheetTemp1.TRHC__County__c = job.TRHC__County__c ;
        newScratchSheetTemp1.TRHC__State__c=job.TRHC__State__c;

        newScratchSheetTemp1.Name='Demo ScratchSheet';
        newScratchSheetTemp1.State_pic__c = job.TRHC__State__c;
        newScratchSheetTemp1.City__c = 'testAGH';
        newScratchSheetTemp1.TRHC__Start_Date__c = system.today();
        newScratchSheetTemp1.End_Date_New__c= system.today() + 5;
        String scrtchSheetTempRecordTypeId1 = [Select Id From RecordType Where SobjectType = 'TRHC__Scratch_Sheet__c' and Name = 'ScratchSheet Template'].Id;
        newScratchSheetTemp1.RecordTypeId=scrtchSheetTempRecordTypeId1 ;
        newScratchSheetTemp1.TRHC__Assignment_length__c = 10;
        newScratchSheetTemp1.TRHC__Hours_Per_Week__c = 10;
        newScratchSheetTemp1.over_time__c = 10;
        newScratchSheetTemp1.TRHC__Bill_Rate__c = 10;
        newScratchSheetTemp1.TRHC__Overtime_Rate__c  = 10;
        newScratchSheetTemp1.TRHC__Total_Overtime_Hours__c  = 10;
        newScratchSheetTemp1.TRHC__Pay_Burden__c = 10;
        newScratchSheetTemp1.Non_Taxable_Expense__c = 10;
        newScratchSheetTemp1.TRHC__Pay_Rate__c = 21;
        newScratchSheetTemp1.TRHC__Overtime_Pay_Rate__c = 1;
        newScratchSheetTemp1.OT_Pay_rate_Override__c = false;
        insert newScratchSheetTemp1;


        TRHC__Job_Applicant__c jobApplication = new TRHC__Job_Applicant__c();
        jobApplication.TRHC__Job__c = job.Id;
        jobApplication.TRHC__Contact_Candidate__c = con.Id;
        jobApplication.TRHC__Recruiter__c = usr[1].Id;
        jobApplication.TRHC__Account_Manager__c = usr[2].Id;
        jobApplication.TRHC__Scratch_Sheet__c = newScratchSheetTemp1.Id;
        insert jobApplication;

        TRHC__Scratch_Sheet__c newScratchSheetTemp = new TRHC__Scratch_Sheet__c();
        newScratchSheetTemp.TRHC__County__c = job.TRHC__County__c ;
        newScratchSheetTemp.TRHC__State__c=job.TRHC__State__c;

        newScratchSheetTemp.Name='Demo ScratchSheet';
        newScratchSheetTemp.State_pic__c = job.TRHC__State__c;
        newScratchSheetTemp.City__c = 'testAGH';
        newScratchSheetTemp.TRHC__Start_Date__c = system.today();
        newScratchSheetTemp.End_Date_New__c= system.today() + 5;
        String scrtchSheetTempRecordTypeId = [Select Id From RecordType Where SobjectType = 'TRHC__Scratch_Sheet__c' and Name = 'ScratchSheet Template'].Id;
        newScratchSheetTemp.RecordTypeId=scrtchSheetTempRecordTypeId ;
        newScratchSheetTemp.TRHC__Assignment_length__c = 10;
        newScratchSheetTemp.TRHC__Hours_Per_Week__c = 10;
        newScratchSheetTemp.over_time__c = 10;
        newScratchSheetTemp.TRHC__Bill_Rate__c = 10;
        newScratchSheetTemp.TRHC__Overtime_Rate__c  = 10;
        newScratchSheetTemp.TRHC__Total_Overtime_Hours__c  = 10;
        newScratchSheetTemp.TRHC__Pay_Burden__c = 10;
        newScratchSheetTemp.Non_Taxable_Expense__c = 10;
        newScratchSheetTemp.TRHC__Pay_Rate__c = 21;
        newScratchSheetTemp.TRHC__Overtime_Pay_Rate__c = 1;
        newScratchSheetTemp.OT_Pay_rate_Override__c = false;
        insert newScratchSheetTemp;
    	System.debug('===newScratchSheetTemp===='+newScratchSheetTemp);

        TRHC__Expense__c expense = new TRHC__Expense__c();
        expense.TRHC__Expense_Name__c = 'Per Diem';
        expense.TRHC__Scratch_Sheet__c = newScratchSheetTemp1.Id;
        expense.TRHC__Amount__c = 10;
        expense.TRHC__Weekly_Amount__c = 10;
        expense.TRHC__Frequency__c = 'Per Assignment';
        insert expense;
        System.debug('===newScratchSheetTemp===='+expense.TRHC__Scratch_Sheet__c);
        
	}
}