@isTest
public class CommunityForgotPasswordRESTOperationTest{
	public static testmethod void CommunityForgotPasswordRESTOperation(){

		TRHC__Config_Settings__c configSettings = new TRHC__Config_Settings__c();
        configSettings.name='Default';
        configSettings.Enable_AutoMateJobRates__c = true;
        configSettings.Trigger_Enable_ScratchSheetAutoAddExp__c = false;
        configSettings.AutoAddExpenseCriteria__c='TRHC__Margin_Type__c=\'Expense\'';
        configSettings.Except_Hospitals__c = 'Atlas Development';
        configSettings.Trigger_Enable_Expected_End_Date_Changer__c = false;
        configSettings.Trigger_Enable_Applicant_From_SCR_Sheet__c = true;
        configSettings.Update_Pay_Burden__c = false;
        configSettings.Enable_RefferalTrigger__c = true;
        insert configSettings;

		List<Account> accountList = TestUtilityClass.getTestMultipleAccount();
		
		String username = 'earthpin@yahoo.com';
		contact conobj = new contact();
		conobj.lastName = 'sharma';
		conobj.TRHC__UserName__c = username;
		conobj.TRHC__Password__c = '12345678';
		conobj.Email = 'HIREN.GOC@GMail.com';
		insert conobj;

		RestRequest req = new RestRequest(); 
    	RestResponse res = new RestResponse();
		
		req.requestURI = '/services/apexrest/mobile/forgotPassword';
		req.addParameter('username', username);
    	req.httpMethod = 'GET';
	    RestContext.request = req;
	    RestContext.response = res;

	    Test.startTest();
	    CommunityForgotPasswordRESTOperation.ResponseHandler wrapperobj = CommunityForgotPasswordRESTOperation.CommunityForgotPasswordRESTOperation();
	    Test.stopTest();
	}
}