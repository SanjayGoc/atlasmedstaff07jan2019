@isTest
public class CommunitychangePasswordRESTOperationTest{
    public static testmethod void changepassword(){
       
        
        TRHC__Config_Settings__c configSettings = new TRHC__Config_Settings__c();
        configSettings.name='Default';
        configSettings.Enable_AutoMateJobRates__c = true;
        configSettings.Trigger_Enable_ScratchSheetAutoAddExp__c = false;
        configSettings.AutoAddExpenseCriteria__c='TRHC__Margin_Type__c=\'Expense\'';
        configSettings.Except_Hospitals__c = 'Atlas Development';
        configSettings.Trigger_Enable_Expected_End_Date_Changer__c = false;
        configSettings.Trigger_Enable_Applicant_From_SCR_Sheet__c = true;
        configSettings.Update_Pay_Burden__c = false;
        configSettings.Enable_RefferalTrigger__c = true;
        insert configSettings;

        List<Account> accountList = TestUtilityClass.getTestMultipleAccount();
        
        contact conobj = new contact();
        conobj.lastName = 'sharma';
        conobj.TRHC__Password__c = '12345678';
        conobj.Email = 'HIREN.GOC@GMail.com';
        conobj.apiToken__c = '123456789123456789';
        insert conobj;
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/mobile/changePassword';
        req.addParameter('apiToken', conobj.apiToken__c);
        req.addParameter('oldPassword', conobj.TRHC__Password__c);
        req.addParameter('newPassword', '0987654321');
        req.addParameter('confirmPassword', '0987654321');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
    
        CommunitychangePasswordRESTOperation.ResponseHandler wrapperobj = CommunitychangePasswordRESTOperation.CommunitychangePasswordRESTOperation();
    
    }
    
    public static testmethod void changepassword2(){
        
        TRHC__Config_Settings__c configSettings = new TRHC__Config_Settings__c();
        configSettings.name='Default';
        configSettings.Enable_AutoMateJobRates__c = true;
        configSettings.Trigger_Enable_ScratchSheetAutoAddExp__c = false;
        configSettings.AutoAddExpenseCriteria__c='TRHC__Margin_Type__c=\'Expense\'';
        configSettings.Except_Hospitals__c = 'Atlas Development';
        configSettings.Trigger_Enable_Expected_End_Date_Changer__c = false;
        configSettings.Trigger_Enable_Applicant_From_SCR_Sheet__c = true;
        configSettings.Update_Pay_Burden__c = false;
        configSettings.Enable_RefferalTrigger__c = true;
        insert configSettings;

        List<Account> accountList = TestUtilityClass.getTestMultipleAccount();
        
        contact conobj = new contact();
        conobj.lastName = 'sharma';
        conobj.TRHC__Password__c = 'asdasdasdasdasd';
        conobj.apiToken__c = '132456789745613213546';
        insert conobj;
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/mobile/forgotPassword';
        req.addParameter('apiToken', conobj.apiToken__c);
        req.addParameter('oldPassword', '12345678');
        req.addParameter('newPassword', '0987654321');
        req.addParameter('confirmPassword', '0987654321');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        CommunitychangePasswordRESTOperation.ResponseHandler wrapperobj = CommunitychangePasswordRESTOperation.CommunitychangePasswordRESTOperation();
    }
}