/**
 * @author Chaitanya bollamreddi
 * @date 22 August 2018
 * @description Auto delete records older then specified date, this class is Scheduler for ResumeTemplate_deleteEduHistories
 */
public class ResumeTemplate_deleteEduHistories_She implements Schedulable {
    /**
     * @author Chaitanya bollamreddi
     * @date 22 August 2018
     * @name execute
     * @parm System.SchedulableContext
     * @return type void
     * @description execute method to shedule the related batch
     */
    public void execute(System.SchedulableContext ctx){
        ResumeTemplate_deleteEduHistories  rTD = new ResumeTemplate_deleteEduHistories ();
        Database.executeBatch(rTD,1000);
    }

}