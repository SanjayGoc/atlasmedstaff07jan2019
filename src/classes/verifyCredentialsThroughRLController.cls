global class verifyCredentialsThroughRLController {
    Webservice static void approveCredentialsOfSelected( Id[] selectedCredIds){
		List<TRHC__Credentials__C> credList = new List<TRHC__Credentials__C> ();
		for(TRHC__Credentials__C cred : [SELECT ID, NAME, Verified__c, TRHC__Is_Responded__c FROM TRHC__Credentials__C WHERE ID IN :selectedCredIds]){
			cred.Verified__c = true;
            credList.add(cred);
    	}
        update credList;
    }
   
}