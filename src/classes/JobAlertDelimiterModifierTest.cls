@isTest
public class JobAlertDelimiterModifierTest {

@isTest
    public static void  delimiterModifierTest(){
        
        TRHC__Config_Settings__c configSet = new TRHC__Config_Settings__c();
        configSet.Name = 'Default';
        configSet.Enable_JobAlertDelimiterModifier__c = TRUE;
        insert configSet;
        
        contact contact = new contact();
        contact.RecordTypeId = [SELECT ID FROM RECORDTYPE WHERE NAME='Candidate' limit 1].id;
        contact.FirstName = 'test';
        contact.LastName = 'test';
        insert contact;
        
        User user = [SELECT Id, Name  from User where  Name = 'Candidate Portal Site Guest User' limit 1];
        
        TRHC__Job_Search_Agent__c jobAlert = new TRHC__Job_Search_Agent__c();
		 jobAlert.TRHC__Search_Criteria__c = 'Discipline : RN; Specialty : Ante Partum;Cath Lab RN;trtttn; State : California'; 
        jobAlert.TRHC__Frequency_Alert__c = 'Daily';
        jobAlert.TRHC__Contact__c = contact.id;
        jobAlert.CreatedById = user.id;
        insert jobAlert;
        
        jobAlert.TRHC__Search_Criteria__c  = 'Discipline : RN; Specialty : Ante Partum;Cath Lab RN;trtttn;';
        update jobAlert;
        
    }
    
}