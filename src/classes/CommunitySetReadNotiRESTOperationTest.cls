@isTest
Public class CommunitySetReadNotiRESTOperationTest{
	public static testmethod void setnotificationTest(){
		TRHC__Config_Settings__c configSettings = new TRHC__Config_Settings__c();
        configSettings.name='Default';
        configSettings.Enable_AutoMateJobRates__c = true;
        configSettings.Trigger_Enable_ScratchSheetAutoAddExp__c = false;
        configSettings.AutoAddExpenseCriteria__c='TRHC__Margin_Type__c=\'Expense\'';
        configSettings.Except_Hospitals__c = 'Atlas Development';
        configSettings.Trigger_Enable_Expected_End_Date_Changer__c = false;
        configSettings.Trigger_Enable_Applicant_From_SCR_Sheet__c = true;
        configSettings.Update_Pay_Burden__c = false;
        configSettings.Enable_RefferalTrigger__c = true;
        insert configSettings;

        List<Account> accountList = TestUtilityClass.getTestMultipleAccount();
        contact conobj = new contact();
        conobj.lastName = 'sharma';
        conobj.TRHC__Password__c = '12345678';
        conobj.Email = 'HIREN.GOC@GMail.com';
        conobj.apiToken__c = '1111111111';
        insert conobj;

        Notification__c note = new Notification__c();
        note.Contact__c = conobj.id;
        insert note;

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/mobile/SetReadNotification';
        req.addParameter('apiToken', '1111111111');
        req.addParameter('isRead','1');
        req.addParameter('NotificationId', '1');
       	req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;

        CommunitySetReadNotiRESTOperation.ResponseHandler wrapperobj = CommunitySetReadNotiRESTOperation.CommunitySetReadNotiRESTOperation();
	}
	public static testmethod void setnotificationTest1(){		

		TRHC__Config_Settings__c configSettings = new TRHC__Config_Settings__c();
        configSettings.name='Default';
        configSettings.Enable_AutoMateJobRates__c = true;
        configSettings.Trigger_Enable_ScratchSheetAutoAddExp__c = false;
        configSettings.AutoAddExpenseCriteria__c='TRHC__Margin_Type__c=\'Expense\'';
        configSettings.Except_Hospitals__c = 'Atlas Development';
        configSettings.Trigger_Enable_Expected_End_Date_Changer__c = false;
        configSettings.Trigger_Enable_Applicant_From_SCR_Sheet__c = true;
        configSettings.Update_Pay_Burden__c = false;
        configSettings.Enable_RefferalTrigger__c = true;
        insert configSettings;

        TRHC__Config_Settings__c config = new TRHC__Config_Settings__c();
        config.Name = 'Default';
        config.Trigger_Enable_Applicant_From_SCR_Sheet__c = True;
        config.Update_Pay_Burden__c = True;
        config.Nurse_Gross_Weekly_Pay__c = 'Test Custom Setting';
        insert config;

        contact conobj = new contact();
        conobj.lastName = 'sharma';
        conobj.TRHC__Password__c = '12345678';
        conobj.Email = 'HIREN.GOC@GMail.com';
        conobj.apiToken__c = '1111111111';
        insert conobj;

        Notification__c notifi = new Notification__c();
	    notifi.Contact__c = conobj.id;
	    insert notifi;

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/mobile/SetReadNotification';
        req.addParameter('apiToken', '1111111111');
        req.addParameter('isRead','1');
        req.addParameter('NotificationId', notifi.id);
       	req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        CommunitySetReadNotiRESTOperation.ResponseHandler wrapperobj = CommunitySetReadNotiRESTOperation.CommunitySetReadNotiRESTOperation();
	
    }
}