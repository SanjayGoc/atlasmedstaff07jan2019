@isTest
public class FluSeasonEmailBatchTest{
    
    @isTest
    public static void getFluCover(){
        TRHC__Config_Settings__c config = new TRHC__Config_Settings__c();
        config.Name = 'Default';
        config.Enable_Update_Recruiter__c = False;
        config.Enable_RecruitersMailNotificationTrigger__c = False;
        config.Excep_status_for_batch_CandidateStatusCh__c = null;
        insert config;
        
        
        Contact contact = new Contact();
        contact.RecordTypeId = [Select ID FROM RecordType Where Name = 'Candidate' Limit 1].id;
        contact.FirstName = 'chaitanya';
        contact.LastName = 'bollamreddy';
        contact.Email = 'chaitanya.b@targetrecruit.net';
        contact.TRHC__Candidate_Status__c = 'InActive';
        insert contact;
        
        TRHC__Placement__c placement  = new TRHC__Placement__c();
        placement.TRHC__Contact_Candidate__c = contact.id;
        placement.TRHC__Status__c = 'Active';
        placement.TRHC__Start_Date__c = Date.today() - 30;
        placement.TRHC__End_Date__c = Date.today() + 30;
        insert placement;
        Test.startTest();
        FluSeasonEmailBatch  flus = new FluSeasonEmailBatch();
        Database.executeBatch(flus);
        Test.stopTest();
    }
    
}