public with sharing class CredentialsExpiryNotificationCustom implements Database.batchable<sObject>{
    
    Public String query;
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        Date expiryNotificationDate = Date.today()+30; 
        String acStatus= 'Active';
        query = 'SELECT TRHC__Expiration_Date__c, TRHC__Contact__c, TRHC__Credentials__c.TRHC__Contact__r.TRHC__Recruiter__c,'+ 
                    'TRHC__Credentials__c.TRHC__Contact__r.TRHC__Candidate_Status__c FROM TRHC__Credentials__c WHERE TRHC__Expiration_Date__c = :expiryNotificationDate and TRHC__Credentials__c.TRHC__Contact__r.TRHC__Candidate_Status__c = :acStatus';
        system.debug('query####'+query);
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext bc, List<TRHC__Credentials__c> records) {
        system.debug('records' +records);
            if (records.size() > 0) {
            List < Messaging.SingleEmailMessage > emails = new List < Messaging.SingleEmailMessage > ();
            
            EmailTemplate et = [Select id from EmailTemplate where name = 'Credential 30day Expiration Notification'];
                for (TRHC__Credentials__c s : records) {
                   //s.Expiration_Notification_Sent__c = True;
                   Messaging.SingleEmailMessage singleMail = new Messaging.SingleEmailMessage();
                   singleMail.setTargetObjectId(s.TRHC__Contact__c);
                   singleMail.setTemplateId(et.Id);
                   singleMail.setWhatId(s.id);
                   singleMail.setSaveAsActivity(false);
                   OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'info@atlasmedstaff.com'];
                   if(owea.size()>0){
                   singleMail.setOrgWideEmailAddressId(owea.get(0).Id);
                   }
                   singleMail.setTreatTargetObjectAsRecipient(FALSE);
                   List<String> toAddList = new List<String>();
                   
                   if(s.TRHC__Contact__c != null){
                    toAddList.add(s.TRHC__Contact__c);
                    if(s.TRHC__Contact__r.TRHC__Recruiter__c != null){
                        toAddList.add(s.TRHC__Contact__r.TRHC__Recruiter__c);
                    }
                    singleMail.toAddresses = toAddList;
                   }
                 
                   emails.add(singleMail);
                }
                system.debug('emails@@@'+emails);
                Messaging.sendEmail(emails);
                //update records; 
            }
            
        }
    
    public void finish(Database.BatchableContext bc) {
        
    }
}