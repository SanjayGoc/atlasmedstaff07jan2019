/**
 * @author : chaitanya bollamreddi
 * @date : 13 April 2018
 * @name :JSONClass
 * @description : Class is refering an VF page JsonFeedTN
 */
public with sharing class JSONClass {
    public list<TRHC__job__c> jobList { get{
        //Fetch the Account List
        jobList = [
            SELECT Id, Name, CreatedDate, TRHC__Job_Title__c, TRHC__State__c, TRHC__Job_City__c, TRHC__Discipline__c, TRHC__Start_Date__c,
            TRHC__Job_Description_Rich__c, TRHC__Recruiter__r.Name, TRHC__Job_Postal_code__c, speciality1__c, shift_type2__c,
            TRHC__Job_Application_Tiny_URL__c, State_pic__c, TRHC__Pay_Rate__c, TRHC__job_Type__c, TRHC__Job_Term__c
            FROM TRHC__Job__c
            WHERE TRHC__Publish__c = true
            AND TRHC__Stage__c = 'Open'];
        
        if(jobList == null){
            jobList = new list<TRHC__JOb__c>();            
        }
        return jobList;
    } set;}   
    
    public string jsonSerial {get{
        //JSON Class method    
        if(jobList == null){
            jsonSerial = '';            
        }else{
            jsonSerial = JSON.serialize(jobList);
        }
        return jsonSerial;  
        
    } set; }
    
    public string jsonGen { get{
        //JSON Generator Class method 
        JSONGenerator gen = JSON.createGenerator(false);//true gives output in pretty print
        
        if(jobList == null){
            return '';          
        }else{
            
            gen.writeStartArray();
            
            for(TRHC__Job__c job: jobList){
                
                gen.writeStartObject();
                if(job.CreatedDate != Null){
                    String Cdate = job.CreatedDate.month() +'/'+job.CreatedDate.day()+'/'+job.CreatedDate.year();
                    gen.writeStringField('date', Cdate);
                    system.debug('string@@'+job.CreatedDate.format('mm/dd/yy'));
                }else{
                    gen.writeStringField('date','');
                }
                if(job.Name != null){
                	gen.writeStringField('referencenumber', job.Name);  
                }else{
                 	gen.writeStringField('referencenumber', '');   
                }
                if(job.TRHC__Job_Title__c != null){
                 gen.writeStringField('title', job.TRHC__Job_Title__c);   
                }else{
                    gen.writeStringField('title', '');
                }
                if(job.TRHC__Job_City__c != Null){
                  gen.writeStringField('city', job.TRHC__Job_City__c);  
                }else{
                  gen.writeStringField('city', ''); 
                }
                if(job.TRHC__State__c != null){
                 	gen.writeStringField('state', job.TRHC__State__c);   
                }else{
                    gen.writeStringField('state', '');
                }
                if(job.TRHC__Job_Postal_code__c != null){
                 gen.writeStringField('postalcode', job.TRHC__Job_Postal_code__c);   
                }
                else{
                    gen.writeStringField('postalcode', ' ');
                }
                gen.writeStringField('license', 'RN, LPN and CNA');
                if(job.speciality1__c != null){
					gen.writeStringField('specialty', job.speciality1__c);
                }else{
                    gen.writeStringField('specialty', '');
                }
                if(job.shift_type2__c != null){
                  gen.writeStringField('shift', job.shift_type2__c);  
                }else{
                  gen.writeStringField('shift', '');  
                }
                if(job.TRHC__Pay_Rate__c != null){
                   gen.writeNumberField('payrate', job.TRHC__Pay_Rate__c); 
                }else{
                   gen.writeStringField('payrate', '');
                }
                if(job.TRHC__job_Type__c != null){
                   gen.writeStringField('positiontype', job.TRHC__job_Type__c); 
                }else{
                    gen.writeStringField('positiontype', ''); 
                }
                if(job.TRHC__Job_Term__c != null){
                    gen.writeStringField('duration', job.TRHC__Job_Term__c);
                }else{
                    gen.writeStringField('duration', '');
                }
                if(job.TRHC__Start_Date__c != null){
                   Date stDate = job.TRHC__Start_Date__c;
                   String stddate = stDate.month()+'/'+stDate.day()+'/'+stDate.year();
                   gen.writeStringField('date',stddate);
                }
                if(job.TRHC__Job_Description_Rich__c != null){
                  gen.writeStringField('description', job.TRHC__Job_Description_Rich__c);  
                }else{
                   gen.writeStringField('description', ''); 
                }
                
                /*>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<*/
                
                /*>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<*/
                if(job.Name==null)
                    gen.writeStringField('Account Number', '');
                else{
                    gen.writeStringField('Account Number',job.Name); 
                }
                
                gen.writeEndObject();
                
            }
            
            gen.writeEndArray();
            return gen.getAsString();
            
        }
        
    } set; } 
    
}