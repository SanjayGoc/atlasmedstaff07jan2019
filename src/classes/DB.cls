public with sharing class DB {
    public static void doSecureInsert(sObject record, Schema.sObjectField[] fieldsToCheck) {
        doSecureInsert(new List<sObject> { record }, fieldsToCheck);
    }
    
    public static void doSecureInsert(List<sObject> records, Schema.sObjectField[] fieldsToCheck) {
        if (records.size() > 0) {
            MetadataUtilNew.checkCrudFls(
                    records[0].getSObjectType(), 
                    fieldsToCheck, 
                    MetadataUtilNew.CHECK_INSERT);
            
            insert records;
        }
    }
    
    public static void doInsert(sObject record) {
        doInsert(new List<sObject> { record });
    }
    
    public static void doInsert(List<sObject> records) {}
    
    public static void doSecureUpdate(sObject record, Schema.sObjectField[] fieldsToCheck) {
        doSecureUpdate(new List<sObject> { record }, fieldsToCheck);
    }
    
    public static void doSecureUpdate(List<sObject> records, Schema.sObjectField[] fieldsToCheck) {
        if (records.size() > 0) {
            MetadataUtilNew.checkCrudFls(
                    records[0].getSObjectType(), 
                    fieldsToCheck, 
                    MetadataUtilNew.CHECK_UPDATE);
            
            update records;
        }
    }
    
    public static void doUpdate(sObject record) {
        doUpdate(new List<sObject> { record });
    }
    
    public static void doUpdate(List<sObject> records) {}
    
    public static void doSecureUpsert(sObject record, Schema.sObjectField[] fieldsToCheck) {
        // If we try to instatiate a generic sObject list and put the record there 
        // then it will throw an error:
        //      "DML on generic List<SObject> only allowed for insert, update or delete".
        // To make it work we should instatiate the list of concrete type.
        // We can do this with dynamic Apex.
        
        String listType = 'List<' + record.getSObjectType() + '>';
        List<sObject> castRecords = (List<sObject>)Type.forName(listType).newInstance();
        castRecords.add(record);
        
        doSecureUpsert(castRecords, fieldsToCheck);
    }
    
    public static void doSecureUpsert(List<sObject> records, Schema.sObjectField[] fieldsToCheck) {
        if (records.size() > 0) {
            MetadataUtilNew.checkCrudFls(
                    records[0].getSObjectType(), 
                    fieldsToCheck, 
                    MetadataUtilNew.CHECK_UPSERT);
            
            upsert records;
        }
    }
    
    public static void doUpsert(sObject record) {
        // If we try to instatiate a generic sObject list and put the record there 
        // then it will throw an error:
        //      "DML on generic List<SObject> only allowed for insert, update or delete".
        // To make it work we should instatiate the list of concrete type.
        // We can do this with dynamic Apex.
        
        String listType = 'List<' + record.getSObjectType() + '>';
        List<sObject> castRecords = (List<sObject>)Type.forName(listType).newInstance();
        castRecords.add(record);
        
        doUpsert(castRecords);
    }
    
    public static void doUpsert(List<sObject> records) {}
    
    public static void doSecureDelete(sObject record) {
        doSecureDelete(new List<sObject> { record });
    }
    
    public static void doSecureDelete(List<sObject> records) {
        if (records.size() > 0) {
            MetadataUtilNew.checkCrudFls(
                    records[0].getSObjectType(), 
                    null, 
                    MetadataUtilNew.CHECK_DELETE);
            
            delete records;
        }
    }
    
    public static void doDelete(sObject record) {
        doDelete(new List<sObject> { record });
    }
    
    public static void doDelete(List<sObject> records) {}
}