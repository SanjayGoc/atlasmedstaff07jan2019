@RestResource(urlMapping='/mobile/allFieldValues/*')
global class CommunityallFieldValuesRESTOperation {
    @HttpGet
    global static ResponseHandler CommunityallFieldValuesRESTOperation (){
        ResponseHandler res = new ResponseHandler();
        String apiToken = RestContext.request.params.get('apiToken');
        List<String> listOfStates  = new List<String>();
        List<String> listOfDiscipline = new List<String>();         
        List<String> listOfPrimSpeciality = new List<String>();
        List<String> listOfSeconSpeciality = new List<String>();
        List<String> listOfShiftPreferance = new List<String>();
        List<String> listOfexp = new List<String>();
        listOfStates = DataUtils.getPickListValues('contact', 'State_pic__c');
        listOfDiscipline = DataUtils.getPickListValues('contact', 'Discipline1__c');
        listOfPrimSpeciality = DataUtils.getPickListValues('contact', 'Primary_Speciality1__c');
        listOfSeconSpeciality = DataUtils.getPickListValues('contact', 'Secondary_Speciality__c'); 
        listOfShiftPreferance = DataUtils.getPickListValues('contact', 'TRHC__Shift_Preference__c'); 
        
        List<Contact> userList = [Select Id,
                                     Name,
                                     firstName,
                                     lastName,
                                     apiToken__c
                                From Contact
                                Where apiToken__c =: apiToken
                                ];

        if(!userList.isEmpty()){
            res.success = 1;
            res.message = new string[]{'Success.'};
            if(listOfStates != null){
                res.stateList = listOfStates;
            } else {
                res.stateList = new List<string>();
            }
            if(listOfDiscipline != null){
                res.disciplineList = listOfDiscipline;
            } else {
                res.disciplineList = new List<string>();
            }
            if(listOfPrimSpeciality != null){
                res.primSpecList = listOfPrimSpeciality;
            } else {
                res.primSpecList = new List<string>();
            }
            if(listOfSeconSpeciality != null){
                res.secondSpecList = listOfSeconSpeciality;
            
            } else {
                res.secondSpecList = new List<string>();
            }
            if(listOfShiftPreferance != null){
                res.shiftPreferanceList = listOfShiftPreferance;
            } else {
                res.shiftPreferanceList = new List<string>();
            }
            return res;
        }
        res.success = 2;
        res.message = new string[]{'Unauthorized User.'};
        return res;
    }
    
    global class ResponseHandler {
        global integer success;
        global string[] message;
        global List<string> stateList;
        global List<string> disciplineList;
        global List<string> primSpecList; 
        global List<string> secondSpecList;
        global List<string> shiftPreferanceList; 
        global ResponseHandler () {

        }
    }
}