@isTest
public class PopolaterecruiterPlacementTest {
    @isTest
    public static void popolaterecruiter(){
        Test.startTest();
        TRHC__config_Settings__c  configSet = new TRHC__config_Settings__c();
        configSet.Name = 'Default';
        configSet.Disable_PopolaterecruiterPlacement__c = true;
        insert configSet;
        
        User usr = [SELECT Id,Name,Email,FirstName,LastName FROM User Limit 1];
        Contact con = new Contact();
        con.FirstName = 'mike'; 
        con.LastName = 'saas';
        con.Email = 'test@test1.gmail.com';
        con.Compliance_Coordinater__c = usr.id;
        con.TRHC__Recruiter__c = usr.id;
        insert con;
        
        TRHC__Job__c job = new TRHC__Job__c();
        job.TRHC__Stage__c = 'closed';
        job.TRHC__Recruiter__c = usr.id;
        job.CreatedDate = Date.today();
        job.TRHC__Number_of_Placements__c = 1;
        insert job;
        
        TRHC__Placement__c plac = new TRHC__Placement__c();
        plac.TRHC__Recruiter__c = usr.id;
        plac.TRHC__Contact_Candidate__c = con.id;
        plac.TRHC__Job__c = job.id;
        insert plac;
            
        Test.stopTest();
    }
}