@istest
public class Trigger_scratchsheet_autoAddExpase_test {
    public static testmethod void testtt(){
        test.startTest();
        
        TRHC__Config__c configObj1 = new TRHC__Config__c();
        configObj1.TRHC__Name__c = 'check';
        configObj1.TRHC__Margin_Type__c = 'Expense';
        configObj1.TRHC__Frequency__c = 'Weekly';
        configObj1.TRHC__State__c =   'California';
        configObj1.TRHC__Include_in_payroll__c= True;
        configObj1.TRHC__Pay_Period_to_be_paid__c='First Check';
        configObj1.TRHC__Active__c = True;
        configObj1.TRHC__Value__c = '350.0';
        configObj1.TRHC__Grouping_Total__c ='Pay Burden';
        configObj1.TRHC__City__c = 'test';
        
        configObj1.RecordTypeId =[Select Id From RecordType Where SobjectType = 'TRHC__Config__c' and Name = 'Margin calculation Line Item'].Id;
        
        insert configObj1;
        
        TRHC__Config_Settings__c configSettings = new TRHC__Config_Settings__c();
        configSettings.name='Default';
        configSettings.Trigger_Enable_ScratchSheetAutoAddExp__c = true;
        configSettings.AutoAddExpenseCriteria__c='TRHC__Margin_Type__c=\'Expense\'';
        configSettings.ExceptConfigIds__c = configObj1.Id;
        configSettings.Except_Hospitals__c = 'Atlas Development';
        insert configSettings;
        TRHC__Config__c configObj = new TRHC__Config__c();
        configObj.TRHC__Name__c = 'check';
        configObj.TRHC__Margin_Type__c = 'Expense';
        configObj.TRHC__Frequency__c = 'Weekly';
        configObj.TRHC__State__c =   'California';
        configObj.TRHC__Include_in_payroll__c= True;
        configObj.TRHC__Pay_Period_to_be_paid__c='First Check';
        configObj.TRHC__Active__c = True;
        configObj.TRHC__Value__c = '350.0';
        configObj.TRHC__Grouping_Total__c ='Pay Burden';
        
        configObj.RecordTypeId =[Select Id From RecordType Where SobjectType = 'TRHC__Config__c' and Name = 'Margin calculation Line Item'].Id;
        
        insert configObj;
        
        
        TRHC__Scratch_Sheet__c newScratchSheetTemp1 = new TRHC__Scratch_Sheet__c();
            newScratchSheetTemp1.TRHC__County__c = 'Kalvin';
            
            newScratchSheetTemp1.TRHC__State__c='California';
            
			newScratchSheetTemp1.city__c ='test';
            
           TRHC__Scratch_Sheet__c newScratchSheetTemp = new TRHC__Scratch_Sheet__c();
            newScratchSheetTemp.TRHC__County__c = 'Kalvin';
            
            newScratchSheetTemp.TRHC__State__c='California';
            newScratchSheetTemp.State_pic__c = 'California';
            newScratchSheetTemp.City__c ='test';
            newScratchSheetTemp.Name='Demo ScratchSheet';
            String scrtchSheetTempRecordTypeId1 = [Select Id From RecordType Where SobjectType = 'TRHC__Scratch_Sheet__c' and Name = 'Scratchsheet'].Id;
            newScratchSheetTemp.RecordTypeId=scrtchSheetTempRecordTypeId1 ;
            newScratchSheetTemp.TRHC__State__c='California';
        	newScratchSheetTemp.Account_Name__c = 'Atlas Development';
            insert newScratchSheetTemp;
            
			      
             
        test.stopTest();
    }
}