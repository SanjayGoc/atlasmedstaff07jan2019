@isTest(SeeAllData=False)
public class JobListControllerCustomTest {
    
    static String jobIds ;
    static List<TRHC__Job__c> jobList ;
    static TRHC__Session__c session ;
    static String sessionString ;
    static Account account, clientAccount1, clientAccount2 ;
    static TRHC__Job__c job1, job2, job3, job4 ;
    static String sessionId;
    static  TRHC__Job_Search_Agent__c jobAgent;
    static TRHC__CustomSettingsComponent__c cC;
    static {
        TRHC__Page__c newPage = new TRHC__Page__c(
            Name = 'CMSDefaultProfilePage',
            TRHC__Name__c = 'CMSDefaultProfilePage',
            TRHC__Active__c = true
        );
        /*
* @author: chaitanya.b@targetrecruit.net
* @description : insert config settings
*/
        
        TRHC__Config_settings__c config = new TRHC__Config_settings__c(Name='Default');
        config.Enable_Update_Recruiter__c = False;
        insert config;
        
       /* cC =  new TRHC__CustomSettingsComponent__c();
        cC.Name = 'Default';
        cC.TRHC__ErrorsReceiver__c ='test@gmail.com';
        cC.TRHC__Query_Fields_for_JobSearch__c = 'Name,TRHC__Job_Title__c,TRHC__Job_City__c,TRHC__Discipline__c,TRHC__Specialty__c,TRHC__State__c';
        cC.TRHC__jobSiteParameterError__c = 'Job Site is not available';
        insert cC; */
        TRHC__CustomSettingsComponent__c config2 = new TRHC__CustomSettingsComponent__c (
            Name = 'Default',
            TRHC__ErrorsReceiver__c ='test@gmail.com',
            TRHC__Query_Fields_for_JobSearch__c = 'Name,TRHC__Job_Title__c,TRHC__Job_City__c,TRHC__Discipline__c,TRHC__Specialty__c,TRHC__State__c',
            TRHC__jobSiteParameterError__c = 'Job Site is not available'
        );
        insert config2;
        
        //insert newPage; 
        Account recruiterPool = new Account(Name = 'Recruiter Pool');
        insert recruiterPool;
        
        TRHC__CMSProfile__c cmsProfile = new TRHC__CMSProfile__c(
            Name = 'Recruiter',
            TRHC__Name__c = 'Recruiter',
            TRHC__Profile_Manager__c = UserInfo.getUserId(),
            TRHC__Default_Home_Page__c = newPage.Id,
            TRHC__Profile_Account__c = recruiterPool.Id,
            TRHC__CMSCss__c = 'CMSCssVMS',
            TRHC__Portal_Enabled__c = true,
            TRHC__User_Registration__c = true,
            TRHC__User_Record_Type__c = 'Contact'
        );
        insert cmsProfile;
        
        // Account RecordTypes, Client and Recruiter
        List<RecordType> accountRecordTypes = [SELECT DeveloperName, Name from RecordType where SObjectType = 'Account' 
                                               and DeveloperName in ('Vendor', 'Customer') order by DeveloperName ];
        
        clientAccount1 = new Account(RecordTypeId = accountRecordTypes[0].Id, Name='Molina Healthcare');
        clientAccount2 = new Account(RecordTypeId = accountRecordTypes[0].Id, Name='Tesla Motors');
        
        account = new Account(Name='Adecco Consulting');
        if(!accountRecordTypes.isEmpty())
            account.RecordTypeId = accountRecordTypes[1].Id ;
        
        insert new Account[]{account, clientAccount1, clientAccount2} ;
            
            Contact cont = new Contact(
                FirstName = 'Ramesh',
                LastName = 'Dhull',
                TRHC__UserName__c = 'dhull.ramesh@gmail.com',
                Email = 'dhull.ramesh@gmail.com',
                TRHC__Password__c = 'avankia1',
                TRHC__CMSProfile__c=cmsProfile.Id,
                AccountId = account.Id
            );
        insert cont;
        
        TRHC__Assigned_Recruiters__c assRec1 = new TRHC__Assigned_Recruiters__c(TRHC__Account__c = clientAccount1.Id, TRHC__Recruiter__c = cont.Id, TRHC__Job_Publish_Site__c = 'Sales',
                                                                                TRHC__Recruiter_Account__c = null);
        TRHC__Assigned_Recruiters__c assRec2 = new TRHC__Assigned_Recruiters__c(TRHC__Account__c = clientAccount2.Id, TRHC__Recruiter__c = null, TRHC__Job_Publish_Site__c = 'Industry', 
                                                                                TRHC__Recruiter_Account__c = account.Id);
        TRHC__Assigned_Recruiters__c assRec3 = new TRHC__Assigned_Recruiters__c(TRHC__Account__c = clientAccount2.Id, TRHC__Recruiter__c = cont.Id, TRHC__Job_Publish_Site__c = 'Sales', 
                                                                                TRHC__Recruiter_Account__c = null);
        insert new TRHC__Assigned_Recruiters__c[]{assRec1, assRec2, assRec3};
            
            sessionId = '62486944141904';
        
        Map<String,String> headerMap = System.currentPageReference().getHeaders();
        
        session = new TRHC__Session__c(
            TRHC__Is_Valid__c = true,
            TRHC__IP_Address__c = headerMap.get('X-Salesforce-SIP'),
            TRHC__Session_For__c = cont.Id,
            TRHC__User_Agent__c = headerMap.get('User-Agent'),
            TRHC__SessionId__c = sessionId
        );
        insert session;
        /*
* @author : chaitanya.b@targetrecruit.net
* @description : Inserting job alert 
*/
        job1 = new TRHC__Job__c();
        job1.TRHC__Account_Lookup__c = clientAccount1.id;
        job1.TRHC__Job_Title__c = 'Java Developer';
        job1.TRHC__AutoPopulated_Skills__c = 'javascript;css;html' ;
        job1.TRHC__Job_Publish_Site__c = 'Sales';
        job1.TRHC__Recruiter__c = [SELECT Id FROM User WHERE IsActive = true LIMIT 1].Id;
        job1.TRHC__Stage__c = 'Open';
        job1.TRHC__Publish__c = true ;
        job1.TRHC__Notify_All_Vendors__c = true ;
        job1.TRHC__Discipline__c = 'LPN';
        
        jobAgent = new TRHC__Job_Search_Agent__c();
        
        job2 = new TRHC__Job__c();
        job2.TRHC__Account_Lookup__c = clientAccount1.id;
        job2.TRHC__Job_Title__c = 'SFDC Admin';
        job2.TRHC__AutoPopulated_Skills__c = 'javascript;css;html' ;
        job2.TRHC__Job_Publish_Site__c = 'Marketing';
        job2.TRHC__Recruiter__c = [SELECT Id FROM User WHERE IsActive = true LIMIT 1].Id;
        job2.TRHC__Stage__c = 'Open';
        job2.TRHC__Publish__c = true ;
        job2.TRHC__Notify_All_Vendors__c = true ;
        
        job3 = new TRHC__Job__c();
        job3.TRHC__Account_Lookup__c = clientAccount2.id;
        job3.TRHC__Job_Title__c = 'SAP Consultant';
        job3.TRHC__AutoPopulated_Skills__c = 'javascript;css;html' ;
        job3.TRHC__Job_Publish_Site__c = 'Software;Sales';
        job3.TRHC__Recruiter__c = [SELECT Id FROM User WHERE IsActive = true LIMIT 1].Id;
        job3.TRHC__Stage__c = 'Open';
        job3.TRHC__Publish__c = true ;
        job3.TRHC__Notify_All_Vendors__c = true ;
        
        job4 = new TRHC__Job__c();
        job4.TRHC__Account_Lookup__c = clientAccount2.id;
        job4.TRHC__Job_Title__c = '.Net Developer';
        job4.TRHC__AutoPopulated_Skills__c = 'javascript;css;html' ;
        job4.TRHC__Job_Publish_Site__c = 'Industry';
        job4.TRHC__Recruiter__c = [SELECT Id FROM User WHERE IsActive = true LIMIT 1].Id;
        job4.TRHC__Stage__c = 'Open';
        job4.TRHC__Publish__c = true ;
        job4.TRHC__Notify_All_Vendors__c = true ;
        
        insert new TRHC__Job__c[]{job1, job2, job3, job4};
        jobAgent.TRHC__Search_Criteria__c = 'Discipline :'+job1.TRHC__Discipline__c;
        jobAgent.TRHC__Contact__c = Cont.Id;
        insert jobAgent;
        
            jobList = new List<TRHC__Job__c>();
        jobList.add(job1);
        jobList.add(job2);
        jobList.add(job3);
        jobList.add(job4);
        
        jobIds = '';
        Boolean count = false;
        if (jobList.size() > 0) {
            for (TRHC__Job__c job : jobList) {
                if (count == false) {
                    jobIds = job.id;
                    count = true;
                    continue;
                }
                else {
                    jobIds = jobIds + ';' + job.id;
                }
            }
        }
    }
    
    static testMethod void testJobListVMSController1() {
        // insert custom setting
        insert new TRHC__Vendor_Portal_Settings__c(
            TRHC__Vendors_Jobs_Criteria__c='TRHC__Notify_All_Vendors__c = true');
        
        TRHC__CustomSettingsComponent__c setting = new TRHC__CustomSettingsComponent__c(Name='Default');
        setting.TRHC__JobListFastSearch__c = true ;
        setting.TRHC__Count_Jobs_for_Job_List_Page__c = '10' ;
        setting.TRHC__isShowPagination__c = true ;
        setting.TRHC__Enable_Jobs_location_search__c = true ;
        setting.TRHC__JobListPageMessage__c = 'Please change your search criteria.' ;
        setting.TRHC__Job_List_Page_fields_for_query__c = 'TRHC__Job_Title__c,TRHC__Job_Term__c,TRHC__Job_Category__c,TRHC__Country_Locale__c,TRHC__Job_Summary__c,TRHC__Job_City__c,TRHC__State__c,Pay_Unit__c,Job_Type__c,TRHC__Account_Lookup__r.name,Pay_Rate_High__c,Pay_Rate_Low__c' ;
        setting.TRHC__ErrorsReceiver__c = 'test@test.com';
        setting.TRHC__SessionTimeout__c = Decimal.valueOf('9999999');
        setting.TRHC__Job_Search_Agent_Field__c = 'Name,Job_Title__c';
        insert setting ;
        
        PageReference pg = new PageReference('apex/JobListPage1');
        pg.getParameters().put('jobSite','Default');
        pg.getParameters().put('jobIds',jobIds);
        pg.getParameters().put('p','Recruiter');
        pg.getParameters().put('sessionId', sessionId);
        pg.getParameters().put('alertid', jobAgent.id);
        
        Cookie sessionCookie = new Cookie(sessionId, sessionId,null,-1,false);
        pg.setCookies(new Cookie[]{sessionCookie});
        
        Test.setCurrentPageReference(pg);
        
        JobListControllerCustom jobListObject = new JobListControllerCustom();
        jobListObject.init();
        
        jobListObject.getSearchedJobBeanListOfList();
        
        jobListObject.setJobSearch('Developer');
        jobListObject.getMetaKeywords();
        jobListObject.setJobCheckBox(true);
        jobListObject.setShowApplyButton(true);
        jobListObject.setShowJobError(true);
        jobListObject.setShowSearchFilterError(true);
        jobListObject.getJob();
        jobListObject.getJobSearch();
        jobListObject.getSearchedJobList();
        jobListObject.getSearchedJobList1();
        jobListObject.getJobCheckBox();
        jobListObject.getShowApplyButton();
        jobListObject.getShowJobError();
        jobListObject.getShowSearchFilterError();
        jobListObject.getCmsCssCustom();
        
        jobListObject.pageChanged();
        
        jobListObject.selectedPage = '2';
        jobListObject.selectedPageNumber();
        jobListObject.pageChanged();
        jobListObject.getpageCountSizeList();
        
        TRHC__CustomSettingsComponent__c config3 = new TRHC__CustomSettingsComponent__c (
            Name = 'Default',
            TRHC__ErrorsReceiver__c ='test@gmail.com',
            TRHC__Query_Fields_for_JobSearch__c = 'Name,TRHC__Job_Title__c,TRHC__Job_City__c,TRHC__Discipline__c,TRHC__Specialty__c,TRHC__State__c',
            TRHC__jobSiteParameterError__c = 'Job Site is not available'
        );
        insert config3;
        
        jobListObject.search();
        jobListObject.getSearchedJobBeanListOfList();
        //jobListObject.jobCheckBoxList.add(true);
        jobListObject.apply();
        
        jobListObject.setJobSearch('a');
        
        jobListObject.getJobSearch();
        System.assertEquals(1, jobListObject.getJobSearch().length());
        jobListObject.getSearchedJobBeanListOfList();
        jobListObject.search();
        
        jobListObject.setJobSearch('');
        jobListObject.getJobSearch();
        System.assertEquals(0, jobListObject.getJobSearch().length());
        jobListObject.getSearchedJobBeanListOfList();
        jobListObject.search();
        
        jobListObject.setJobSearch(null);
        jobListObject.getJobSearch();
        System.assertEquals(null, jobListObject.getJobSearch());
        jobListObject.getSearchedJobBeanListOfList();
        jobListObject.search();
        
        PageReference pg1 = new PageReference('apex/JobListPage1');
        pg1.getParameters().put('jobSite',null);
        jobListObject = new JobListControllerCustom();
        
        jobListObject.getCmsCssCustom();
        JobListControllerCustom.searchedJobBean obj = new JobListControllerCustom.searchedJobBean();
        obj.oddEvenCssClass = 'test';
        obj.jobObject = new TRHC__Job__c();
        obj.checkBox = true;
        obj.jobAppliedBefore = true;
        
        TRHC__Job__c  jobValue = new TRHC__Job__c(
            TRHC__Job_Title__c = 'Test',
            TRHC__Start_Date__c = Date.today(),
            TRHC__Account_Lookup__c = account.Id
        );
        
        TRHC__Job__c  jobTestJob = new TRHC__Job__c(
            TRHC__Job_Title__c = 'Test',
            TRHC__Start_Date__c = Date.today(),
            TRHC__Account_Lookup__c = account.Id
        );
        
        List<TRHC__Job__c> jobListName = new List<TRHC__Job__c>();
        jobListName.add(jobValue);
        jobListName.add(jobTestJob);
        
        jobListObject.searchedJobList2.add(jobListName);
        jobListObject.getSearchedJobBeanListOfList();
        
        //Rakesh
       /* jobListObject.pageSizeList = new List<Integer>();
        pageSizeList.add(1);
        pageSizeList.add(2); */
        jobListObject.pageSize(2,2);
        jobListObject.getPages();
        jobListObject.selectedPageSize = '10';
        jobListObject.selectedPageCountRecord();
        
        jobListObject.apply1();
        
    }
    
    static testMethod void testJobListVMSController2() {
        // insert custom setting        
        insert new TRHC__Vendor_Portal_Settings__c(
            TRHC__Vendors_Jobs_Criteria__c='TRHC__Notify_All_Vendors__c = true');
        
        TRHC__CustomSettingsComponent__c setting = new TRHC__CustomSettingsComponent__c(Name='Default');
        setting.TRHC__JobListFastSearch__c = true ;
        setting.TRHC__Count_Jobs_for_Job_List_Page__c = null ;
        setting.TRHC__isShowPagination__c = true ;
        setting.TRHC__Enable_Jobs_location_search__c = true ;
        setting.TRHC__JobListPageMessage__c = 'Please change your search criteria.' ;
        setting.TRHC__Job_List_Page_fields_for_query__c = 'TRHC__Job_Title__c,TRHC__Job_Term__c,TRHC__Job_Category__c,TRHC__Country_Locale__c,TRHC__Job_Summary__c,TRHC__Job_City__c,TRHC__State__c,Pay_Unit__c,Job_Type__c,TRHC__Account_Lookup__r.name,Pay_Rate_High__c,Pay_Rate_Low__c' ;
        setting.TRHC__ErrorsReceiver__c = 'test@test.com';
        setting.TRHC__SessionTimeout__c = Decimal.valueOf('9999999');
        setting.TRHC__Job_Search_Agent_Field__c = 'Name,Job_Title__c';
        insert setting ;
        
        PageReference pg = new PageReference('apex/JobSearchPageMultiJobCompareCustom');
        pg.getParameters().put('jobSite','Default');
        pg.getParameters().put('p','Recruiter');
        pg.getParameters().put('sessionId', sessionId);
        
        Cookie sessionCookie = new Cookie(sessionId, sessionId,null,-1,false);
        pg.setCookies(new Cookie[]{sessionCookie});
        
        Test.setCurrentPageReference(pg);
        
        JobListControllerCustom jobListObject = new JobListControllerCustom();
        jobListObject.init(); 
        
        jobListObject.job.TRHC__Job_Category__c = 'Engineering' ;
        jobListObject.job.TRHC__Job_Type__c = 'Full Time' ;
        jobListObject.job.TRHC__Job_Term__c = 'Direct Hire' ;
        
        jobListObject.setJobSearch('*Developer*?');
        jobListObject.radius = 20 ;
        jobListObject.search();
        
        jobListObject.job.TRHC__Job_Category__c = null ;
        jobListObject.job.TRHC__Job_Type__c = null ;
        jobListObject.job.TRHC__Job_Term__c = null ;
        
        jobListObject.setJobSearch('*D*');
        jobListObject.radius = null ;
        jobListObject.search();
        
        
        // fix the test result for an SOSL
        Id [] fixedSearchResults = new Id[]{job1.Id, job4.Id};
            Test.setFixedSearchResults(fixedSearchResults);
        
        TRHC__CustomSettingsComponent__c config2 = new TRHC__CustomSettingsComponent__c (
            Name = 'Default',
            TRHC__ErrorsReceiver__c ='test@gmail.com',
            TRHC__Query_Fields_for_JobSearch__c = 'Name,TRHC__Job_Title__c,TRHC__Job_City__c,TRHC__Discipline__c,TRHC__Specialty__c,TRHC__State__c',
            TRHC__jobSiteParameterError__c = 'Job Site is not available'
        );
        insert config2;
        jobListObject.setJobSearch('Developer');
        jobListObject.radius = null ;
        jobListObject.search();
        
        jobListObject.setJobSearch('');
        jobListObject.search();
        
        // with null job list page Developer
        jobListObject.getpageCountSizeList();
    }
    
    static testMethod void testJobListVMSController3() {
        // insert custom setting
        
        insert new TRHC__Vendor_Portal_Settings__c(
            TRHC__Vendors_Jobs_Criteria__c='TRHC__Notify_All_Vendors__c = true');
        
        TRHC__CustomSettingsComponent__c setting = new TRHC__CustomSettingsComponent__c(Name='Default');
        setting.TRHC__JobListFastSearch__c = true ;
        setting.TRHC__Count_Jobs_for_Job_List_Page__c = null ;
        setting.TRHC__isShowPagination__c = true ;
        setting.TRHC__Enable_Jobs_location_search__c = true ;
        setting.TRHC__Job_List_Page_fields_for_query__c = 'TRHC__Job_Title__c,TRHC__Job_Term__c,TRHC__Job_Category__c,TRHC__Country_Locale__c,TRHC__Job_Summary__c,TRHC__Job_City__c,TRHC__State__c,Pay_Unit__c,Job_Type__c,TRHC__Account_Lookup__r.name,Pay_Rate_High__c,Pay_Rate_Low__c' ;
        setting.TRHC__ErrorsReceiver__c = 'test@test.com';
        setting.TRHC__SessionTimeout__c = Decimal.valueOf('9999999');
        setting.TRHC__Job_Search_Agent_Field__c = 'Name,Job_Title__c';
        insert setting ;
        
        PageReference pg = new PageReference('apex/JobListPage1');
        pg.getParameters().put('jobSite','Sales');
        pg.getParameters().put('jobIds',jobIds);
        pg.getParameters().put('p','Recruiter');
        pg.getParameters().put('sessionId', sessionId);
        
        Cookie sessionCookie = new Cookie(sessionId, sessionId,null,-1,false);
        pg.setCookies(new Cookie[]{sessionCookie});
        
        Test.setCurrentPageReference(pg);
        
        JobListControllerCustom jobListObject = new JobListControllerCustom();
        jobListObject.init();
        
        jobListObject.setJobSearch('');
        jobListObject.search();
        
        // with null job list page
        jobListObject.getpageCountSizeList();
    }    
}