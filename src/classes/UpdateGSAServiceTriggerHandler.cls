global class UpdateGSAServiceTriggerHandler{

	@future (callout=true)
	global static void doCalloutForGSA(set<Id> jobList) {

		List<TRHC__Job__c> jobUpdate = [SELECT id, 
												Name,
												TRHC__Start_Date__c,
												TRHC__Expected_End_Date__c,
												State_pic__c,
												GSA_Lodging__c,
												TRHC__County__c,
												GSA_M_I_Rate__c,
												Tax_Rate__c
										FROM TRHC__Job__c
										WHERE Id IN :jobList];

		List<TRHC__Job__c> updateJobList = new List<TRHC__Job__c>();

		try {
			String gsaURL = 'http://gsa.targetrecruit.com/Geocode/getGsaDetails_new.php';   //(String)cs.TRHC__GSA_Service_PHP_URL__c;


			for (TRHC__Job__c jobOBJ : jobUpdate) { 
				if (jobOBJ.State_pic__c != null){ 

					String startDateStr = jobOBJ.TRHC__Start_Date__c != null ? 
					                      jobOBJ.TRHC__Start_Date__c.year() + '-'+
										  jobOBJ.TRHC__Start_Date__c.month() + '-'+
										  jobOBJ.TRHC__Start_Date__c.day() : '';
					String endDate = jobOBJ.TRHC__Expected_End_Date__c != null ?
									 jobOBJ.TRHC__Expected_End_Date__c.year() + '-' +
									 jobOBJ.TRHC__Expected_End_Date__c.month() + '-' +
									 jobOBJ.TRHC__Expected_End_Date__c.day() : ''; 
					String state = '';	 				
					state = jobOBJ.State_pic__c;
					state = state.replace(' ','%20');
					String county = jobOBJ.TRHC__County__c != null ? jobOBJ.TRHC__County__c.replace(' ','%20') :'';

					HttpRequest request = new HttpRequest();
					String GSARquestURL = gsaURL +'?sdate=' + startDateStr +'&edate='+endDate+'&state='+state+'&county='+county;
					request.setEndpoint(GSARquestURL);
					request.setMethod('GET');
					request.setCompressed(false);
					request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
					System.debug(LoggingLevel.ERROR, 'gsa request>>>' + request);
					HttpResponse response = new HttpResponse();

					if(!Test.isRunningTest() && Limits.getCallouts() < Limits.getLimitCallouts()){
						response = new Http().send(request);
					}else{
						response.setBody('[{"GSADestination":"Gulf Shores","State":"AL","GSACounty":"Baldwin","BeginDate":"2010-08-01","EndDate":"2010-09-30","Lodging":"111","MIE":null,"Total":"162","CombinedTaxRate":"8.51"}]');
						response.setStatusCode(200);
					}

					System.debug(LoggingLevel.ERROR, ':::::::::GSA response = ' + response);
					System.debug(LoggingLevel.ERROR, ':::::::::response.getBody() = ' + response.getBody());

					if (response.getStatusCode() == 200) {
						String body = response.getBody();
						String jsonBody = body.substring(1,body.length()-1);
						System.debug(LoggingLevel.ERROR, ':::::::::body' + jsonBody);
						UpdateGSAServiceTriggerHandler.GSAResponse gsaResponse  = parseResponse(jsonBody);

						System.debug(LoggingLevel.ERROR, ':::::::::gsaResponse after parsing = ' + gsaResponse);

						jobOBJ.GSA_M_I_Rate__c = gsaResponse.MIE !=null ? Decimal.Valueof(gsaResponse.MIE):0;
						jobOBJ.GSA_Lodging__c = gsaResponse.Lodging !=null ? Decimal.Valueof(gsaResponse.Lodging):0;
						jobOBJ.Tax_Rate__c =  gsaResponse.CombinedTaxRate !=null ? Decimal.ValueOf(gsaResponse.CombinedTaxRate):0;
						updateJobList.add(jobOBJ);
					}
				}
			}
		} catch(System.CalloutException e) {
			System.debug(LoggingLevel.ERROR, ':::::::::CalloutException  = ' + e);
		}

		MetadataUtilNew.checkCrudFls(
									TRHC__Job__c.sObjectType,
									new Schema.sObjectField[] {
										TRHC__Job__c.GSA_M_I_Rate__c,
										TRHC__Job__c.GSA_Lodging__c,
										TRHC__Job__c.Tax_Rate__c },
									MetadataUtilNew.CHECK_UPDATE);

		if(!updateJobList.isempty()){
			update updateJobList;
		}
	}

	public static UpdateGSAServiceTriggerHandler.GSAResponse parseResponse(String response) {

		UpdateGSAServiceTriggerHandler.GSAResponse gsaResponse = new UpdateGSAServiceTriggerHandler.GSAResponse();

		if (String.isNotBlank(response)) {
			UpdateGSAServiceTriggerHandler.GSAResponse respObj = 
			(UpdateGSAServiceTriggerHandler.GSAResponse)JSON.deserialize(
				response, UpdateGSAServiceTriggerHandler.GSAResponse.class);
				system.debug('respObj after deserialize'+respObj);
				return setGSADetails(gsaResponse, 
					respObj.MIE, respObj.Lodging,respObj.CombinedTaxRate);
		}
		return setGSADetails(gsaResponse, null, null,null);
	}

	public static UpdateGSAServiceTriggerHandler.GSAResponse setGSADetails(
		UpdateGSAServiceTriggerHandler.GSAResponse gsaResponse, String MIE, String Lodging,String taxRate) {
			gsaResponse.MIE = MIE;
			gsaResponse.Lodging = Lodging;
			gsaResponse.CombinedTaxRate = taxRate;
			System.debug(LoggingLevel.ERROR, ':::::::::gsaResponse  = ' + gsaResponse);
		return gsaResponse;
	}   

	public class GSAResponse{
		public String GSADestination {get;set;}
		public String State {get;set;}
		public String GSACounty {get;set;}
		public String BeginDate {get;set;}
		public String EndDate {get;set;}
		public String Lodging {get;set;}
		public String MIE {get;set;}
		public String Total {get;set;}
		public String CombinedTaxRate {get;set;}
	}
}