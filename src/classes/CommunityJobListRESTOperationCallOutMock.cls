@isTest
global class CommunityJobListRESTOperationCallOutMock implements HttpCalloutMock {
    
    global static HTTPResponse respond(HTTPrequest request) {
        HttpResponse  response = new HttpResponse();
        //response.setheader('Authorization', 'authorizationHeader');
        response.setHeader('Content-Type', 'application/json');
        String resbody = '{'+
        '  \"results\": ['+
        '    {'+
        '      \"address_components\": ['+
        '        {'+
        '          \"long_name\": \"7007\",'+
        '          \"short_name\": \"7007\",'+
        '          \"types\": ['+
        '            \"street_number\"'+
        '          ]'+
        '        },'+
        '        {'+
        '          \"long_name\": \"College Boulevard\",'+
        '          \"short_name\": \"College Blvd\",'+
        '          \"types\": ['+
        '            \"route\"'+
        '          ]'+
        '        },'+
        '        {'+
        '          \"long_name\": \"Overland Park Plaza\",'+
        '          \"short_name\": \"Overland Park Plaza\",'+
        '          \"types\": ['+
        '            \"neighborhood\",'+
        '            \"political\"'+
        '          ]'+
        '        },'+
        '        {'+
        '          \"long_name\": \"Overland Park\",'+
        '          \"short_name\": \"Overland Park\",'+
        '          \"types\": ['+
        '            \"locality\",'+
        '            \"political\"'+
        '          ]'+
        '        },'+
        '        {'+
        '          \"long_name\": \"Johnson County\",'+
        '          \"short_name\": \"Johnson County\",'+
        '          \"types\": ['+
        '            \"administrative_area_level_2\",'+
        '            \"political\"'+
        '          ]'+
        '        },'+
        '        {'+
        '          \"long_name\": \"Kansas\",'+
        '          \"short_name\": \"KS\",'+
        '          \"types\": ['+
        '            \"administrative_area_level_1\",'+
        '            \"political\"'+
        '          ]'+
        '        },'+
        '        {'+
        '          \"long_name\": \"United States\",'+
        '          \"short_name\": \"US\",'+
        '          \"types\": ['+
        '            \"country\",'+
        '            \"political\"'+
        '          ]'+
        '        },'+
        '        {'+
        '          \"long_name\": \"66211\",'+
        '          \"short_name\": \"66211\",'+
        '          \"types\": ['+
        '            \"postal_code\"'+
        '          ]'+
        '        }'+
        '      ],'+
        '      \"formatted_address\": \"7007 College Blvd, Overland Park, KS 66211, USA\",'+
        '      \"geometry\": {'+
        '        \"location\": {'+
        '          \"lat\": 38.92663719999999,'+
        '          \"lng\": -94.66484729999999'+
        '        },'+
        '        \"location_type\": \"ROOFTOP\",'+
        '        \"viewport\": {'+
        '          \"northeast\": {'+
        '            \"lat\": 38.92798618029149,'+
        '            \"lng\": -94.66349831970848'+
        '          },'+
        '          \"southwest\": {'+
        '            \"lat\": 38.9252882197085,'+
        '            \"lng\": -94.66619628029149'+
        '          }'+
        '        }'+
        '      },'+
        '      \"place_id\": \"ChIJPTZ9JzjqwIcR0kOfM1L7n9E\",'+
        '      \"plus_code\": {'+
        '        \"compound_code\": \"W8GP+M3 Overland Park, Kansas, United States\",'+
        '        \"global_code\": \"86C7W8GP+M3\"'+
        '      },'+
        '      \"types\": ['+
        '        \"establishment\",'+
        '        \"point_of_interest\"'+
        '      ]'+
        '    }'+
        '  ],'+
        '  \"status\": \"OK\"'+
        '}';
        response.setBody(resbody);      
        response.setStatusCode(200);
        return response;
    }
}