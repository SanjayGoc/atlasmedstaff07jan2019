/**
* @author Mikhail Ivanov
* @date 12/26/2012
* @description Used to get values from configuration records.
* There are 3 types of configuration records in TR:
*       1. Config custom object (Config__c);
*       2. List custom setting (Config_Settings__c);
*       3. Hierarchy custom setting (ConfigSettingsHierarchy__c).
*/
public with sharing class ConfigHelperCustom {
  public static TRHC__CustomSettingsComponent__c getConfigDataSetCustomComponent() {
    return TRHC__CustomSettingsComponent__c.getValues('Default');
  }
}