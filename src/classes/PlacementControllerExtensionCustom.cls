public With sharing class PlacementControllerExtensionCustom{
    
    ApexPages.StandardController controller;
    TRHC__Placement__c placement;
    Public Decimal  perDiemValue { get; set; }
    public Decimal  housing { get; set; }
    Public string selectedUser{get;set;}
    Public  List<String>  selectedUsers{get;set;}
    private List<User> appUser;
    public List<SelectOption> appUsers{get;set;}
    //
    Set<String> originalvalues = new Set<String>();
    Public List<string> leftselected{get;set;}
    Public List<string> rightselected{get;set;}
    Set<string> leftvalues = new Set<string>();
    Public Set<string> rightvalues = new Set<string>();
    
    public PageReference selectclick(){
        rightselected.clear();
        for(String s : leftselected){
            leftvalues.remove(s);
            rightvalues.add(s);
        }
        return null;
    }
    
    public PageReference unselectclick(){
        leftselected.clear();
        for(String s : rightselected){
            rightvalues.remove(s);
            leftvalues.add(s);
        }
        return null;
    }
    
    public List<SelectOption> getunSelectedValues(){
        List<SelectOption> options = new List<SelectOption>();
        List<string> tempList = new List<String>();
        tempList.addAll(leftvalues);
        tempList.sort();
        for(string s : tempList)
            options.add(new SelectOption(s,s));
        return options;
    }
    
    public List<SelectOption> getSelectedValues(){
        List<SelectOption> options1 = new List<SelectOption>();
        List<string> tempList = new List<String>();
        tempList.addAll(rightvalues);
        tempList.sort();
        for(String s : tempList)
            options1.add(new SelectOption(s,s));
        return options1;
    }
    public PlacementControllerExtensionCustom(ApexPages.StandardController standardController) {
        controller = standardController;
        placement = new TRHC__Placement__c();
        placement = (TRHC__Placement__c)standardController.getRecord();
        TRHC__Config_Settings__c config = TRHC__Config_Settings__c.getInstance('Default');
        list<string> userIds = (config.Assignment_Work_Order_Approve_Users__c).split(';');
        appUsers = new List<SelectOption>();
        appUser = [SELECT ID, NAME, EMAIL, USERNAME FROM USER WHERE ID IN :userIds];
        for(User user:appUser)
        {
            appUsers.add(new SelectOption(user.Name,user.Name)); 
            originalvalues.add(user.Name);
        }
        
        appUsers.add(new SelectOption('Placement Group','Placement Group')); 
        originalvalues.add('Placement Group');
        perDiemValue = 0.00;
        housing = 0.00;
        leftselected = new List<String>();
        rightselected = new List<String>();
        leftvalues.addAll(originalvalues);
    }
    
    
    public void capturePlacementData(){
        List<String> expNameList = new List<String> ();
        expNameList.add('Per Diem');
        expNameList.add('Housing Stipend');
        List<TRHC__Expense__c> expList = [SELECT id, TRHC__Expense_Name__c, TRHC__Hourly_Amount__c,TRHC__Weekly_Amount__c FROM TRHC__Expense__c WHERE TRHC__Expense_Name__c IN :expNameList AND TRHC__Scratch_Sheet__c = :placement.TRHC__Scratch_Sheet__c];
        if(expList.size()>0){
            for(TRHC__Expense__c expense :expList){
                if(expense.TRHC__Expense_Name__c == 'Per Diem'){
                    perDiemValue = expense.TRHC__Weekly_Amount__c;
                }
                if(expense.TRHC__Expense_Name__c == 'Housing Stipend'){
                    housing = expense.TRHC__Weekly_Amount__c;
                }
            }
        }
    }
    
    public PageReference getNotifyTeam() {
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        String conId = ApexPages.CurrentPage().getParameters().get('id');
        PageReference pdf = page.AssignmentWorkOrderPDF;
        pdf.getParameters().put('id',conId);
        Blob pdfBlob;
        
        try{
            if (Test.IsRunningTest())
            {
                pdfBlob = Blob.valueOf('UNIT.TEST');
            }
            else
            {
                pdfBlob = pdf.getContent();
            }
        }
        catch(Exception e){
            
        }
        
        Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
        attach.setContentType('application/pdf');
        //attach.setFileName(placement.TRHC__Contact_Candidate__r.Name+'Work_Order.pdf');
        attach.setFileName(String.valueOf('Work order -'+placement.TRHC__Employer__r.Name +'-'+placement.TRHC__Job__r.Name));
        attach.setInline(false);
        attach.Body = pdfBlob;
        
        //Contact contact = [Select Name, Phone, email, Recruiters__c, TRHC__Permanent_City__c FROM Contact Where ID =:conId Limit 1];
        //User user = [Select Id, Name From User Where Username=:submitTo Limit 1];
        string submitedPerson = 'Submitted By : '+Userinfo.getName() +'\n'+'\n';    
        //String taskDescription = 'Contact Name: '+placement. +   '\n'+
        String taskDescription = '<b><u>Employee Information</b></u> '+'\n'+'\n'+
            'EMployeeName  -   '+placement.TRHC__Contact_Candidate__r.Name+'\n'+'\n'+
            'Unit  -   '+'Telemetry'+'\n'+'\n'+
            'Recruiter      -   '+placement.TRHC__Recruiter__r.Name+'\n'+'\n'+
            'Position    -   '+placement.TRHC__Job__r.TRHC__Discipline__c +'\n'+'\n'+
            'Speciality	 -'+placement.TRHC__Job__r.Speciality1__c +'\n'+'\n'+
            'Facility    -   '+placement.TRHC__Employer__r.Name +'\n'+'\n'+
            'Shift  -   '+placement.TRHC__Shift_Name__c+'\n'+'\n'+
            'Address - '+placement.Hospital_Address__c+ ' ' +'\n'+
            'City/State/Zip - '+placement.TRHC__Employer__r.BillingCity+' / ' +placement.TRHC__Employer__r.BillingState+ ' / ' + placement.TRHC__Employer__r.BillingPostalCode+' ' +'\n'+
            'Facility Phone - '+placement.Hospital_Phone__c+'\n'+'\n'+
            'Start Date   -  '+placement.TRHC__Start_Date__c+'\n'+'\n'+
            'End Date - '+placement.TRHC__End_Date__c+'\n'+'\n'+'\n'+
            '<b><u>Salary and Benefits Information</u></b>'+'\n'+'\n'+
            'Sheduled Hours/WK        -   '+placement.TRHC__Scratch_Sheet__r.TRHC__Hours_Per_Week__c+'\n'+'\n'+
            'Regular Hourly Rate        -   '+placement.TRHC__Scratch_Sheet__r.TRHC__Pay_Rate__c+'\n'+'\n'+
            'Rate 2 (over 8 hrs/day)        -   '+placement.TRHC__Scratch_Sheet__r.TRHC__Overtime_Pay_Rate__c+'\n'+'\n'+
            'Holiday Rate       -   '+placement.TRHC__Scratch_Sheet__r.TRHC__Holiday_Pay_Rate__c+'\n'+'\n'+
            'On Call        -   '+placement.TRHC__Scratch_Sheet__r.TRHC__On_Call_Pay_Rate__c+'\n'+'\n'+
            'Per Diem Allowance       -   '+perDiemValue+'\n'+'\n'+
            'Charge Pay        -   '+placement.TRHC__Scratch_Sheet__r.TRHC__Charge_Pay_Rate__c+'\n'+'\n'+
            'Housing Allowance       -   '+housing+'\n'+'\n';
        Messaging.SingleEmailMessage mail = 
            new Messaging.SingleEmailMessage(); 
        List<String> sendTo = new List<String>();
        String body = '<html>'+'<p>'+'Dear ';
        for(User user:appUser) {
            system.debug('rightselected@@'+rightvalues);
            if(rightvalues.contains(user.Name)) {
                body += user.Name + '/'; 
                sendTo.add(user.Email);
            }
        }
        body = body.removeEnd('/') +'<br/><br/>';
        if(rightvalues.contains('Placement Group')){
			sendTo.add(Label.Atlas_Placement_Team);
		}
        mail.setToAddresses(sendTo);
        mail.setSubject('Work Order Submission -'+placement.TRHC__Employer__r.Name +'-'+ placement.TRHC__Job__r.Name);
        
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'info@atlasmedstaff.com'];
        if ( owea.size() > 0 ) {
            mail.setOrgWideEmailAddressId(owea.get(0).Id);
        }else{
            mail.setSenderDisplayName('Assignment Work Order');
        }
        
        body += 'Below is the  Work Order Deatails - '+placement.TRHC__Contact_Candidate__r.Name+'<br/>';
        body += submitedPerson+'<b>'+'<br/>'+ 'Also attached to the email is the complete work order pdf file' +'<br/>' +taskDescription+'<br/>'+'</p>'+'</html>';
        System.debug('body@@'+body);
        body = body.replace('\n', '<br/>');
        mail.setHtmlBody(body);
        mail.setFileAttachments(new Messaging.EmailFileAttachment[] { attach }); 
        //system.debug('test'+test);
        mails.add(mail);
        Attachment attachment = new Attachment();
        attachment.Body = pdfBlob;
        attachment.Name = String.valueOf('Work order -'+placement.TRHC__Employer__r.Name +'-'+ placement.TRHC__Job__r.Name);
        attachment.ParentId = placement.TRHC__Contact_Candidate__c;
        insert attachment;
        Messaging.sendEmail(mails);
        
        return new PageReference('/'+conId);
        
    }
}