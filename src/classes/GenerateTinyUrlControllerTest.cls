@isTest
public class GenerateTinyUrlControllerTest {
public static testMethod void testGeneration() {
        
        TRHC__Config_Settings__c cs = TRHC__Config_Settings__c.getOrgDefaults();
        cs.Name = 'Default';
        cs.Trigger_Enable_Expected_End_Date_Changer__c = false;
        insert cs;

        TRHC__Job__c jobWithActualUrl = new TRHC__Job__c(TRHC__Job_Title__c = 'Test Title', TRHC__Start_Date__c = Date.today());
        insert jobWithActualUrl;
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(jobWithActualUrl);
        GenerateTinyUrlController controller = new GenerateTinyUrlController(stdController);
        
        controller.init();
    }
    }