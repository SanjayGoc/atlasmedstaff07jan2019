@isTest 
public class ProphecyExpiryNotificationCustomTest
{
    @isTest
    static void testMethod1() 
    {
     
        Test.startTest();
            TRHC__Config_Settings__c conf = new TRHC__Config_Settings__c ();
            conf.Name = 'Default';
            conf.Enable_Update_Recruiter__c = false;
            insert conf;
            
            Contact con = new Contact ();
            con.lastName ='chaitanya';
            con.Email ='chaithu@gmail.com';
            con.Recruiters__c = contact.Recruiters__c.getDescribe().getPicklistValues()[0].getValue();
            con.TRHC__Candidate_Status__c = 'Active';
            insert con;
            
            TRHC__Skill_Answers__c skill = new TRHC__Skill_Answers__c ();
            skill.TRHC__Skill_Checklist_Name__c = 'TestSkill';
            skill.TRHC__Contact__c = con.id;
            skill.Expiration_Notification_Sent__c = False;
            skill.TRHC__Date_Finished__c = Date.today()-334;
            //skill.Expiry_date__c = Date.today() + 30;
            insert skill ;
            List<Sobject> Klist = new List<Sobject>();
            Klist.add(skill);
            
            //List<TRHC__Skill_Answers__c> Klist = [SELECT TRHC__Contact__c, Expiry_date__c ,TRHC__Skill_Answers__c.TRHC__Contact__r.id , TRHC__Skill_Answers__c.TRHC__Contact__r.Email, Expiration_Notification_Sent__c FROM TRHC__Skill_Answers__c  WHERE TRHC__Skill_Checklist_Name__c = 'TestSkill']; 
            
            ProphecyExpiryNotificationCustom obj = new ProphecyExpiryNotificationCustom ();
            Date expiryNotificationDate = Date.today()+30; 
            obj.query = 'SELECT TRHC__Contact__c, Expiry_date__c ,TRHC__Skill_Answers__c.TRHC__Contact__r.id , TRHC__Skill_Answers__c.TRHC__Contact__r.Email, Expiration_Notification_Sent__c FROM TRHC__Skill_Answers__c  WHERE Expiry_date__c = :expiryNotificationDate';
            Database.executeBatch(obj);
        Test.stopTest();
    }
}