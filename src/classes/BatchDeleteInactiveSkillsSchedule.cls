/*
============================================================================
Description:   Schedule class for BatchDeleteInactiveSkills class 
         Delete Skills which is older than 7 days and does not have PDF URL.
============================================================================
*/

global class BatchDeleteInactiveSkillsSchedule implements Schedulable {
    global void execute(SchedulableContext sc) {
        BatchDeleteInactiveSkills bch = new BatchDeleteInactiveSkills();
        database.executebatch(bch, 200);
    }  
}