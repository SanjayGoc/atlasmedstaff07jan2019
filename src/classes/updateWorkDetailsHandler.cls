public class updateWorkDetailsHandler {
    public static void updateWorkOrder(List<TRHC__Placement__c> placements) {
        system.debug('===>placements ' + placements);
        Set<Id> jobids = new Set<Id>();
        Set<Id> scrathsheetids = new Set<Id>();
        for(TRHC__Placement__c plm : placements) {
            jobids.add(plm.TRHC__Job__c);
            scrathsheetids.add(plm.TRHC__Scratch_Sheet__c);
        }
        system.debug('===>jobids ' + jobids);
        system.debug('===>scrathsheetids ' + scrathsheetids);
        Map<Id, TRHC__Job__c> jobidToJob = new Map<Id, TRHC__Job__c>();
        List<TRHC__Job__c> jobLists = [SELECT Id, Name, Shift_Type2__c, 
                                            TRHC__Guarantee_Hours_Pay_Rate__c 
                                            FROM TRHC__Job__c WHERE Id IN: jobids];
        for(TRHC__Job__c job : jobLists) {
            jobidToJob.put(job.Id, job);
        }
        Map<Id, TRHC__Scratch_Sheet__c> scrathToScrathsheet = new Map<Id, TRHC__Scratch_Sheet__c>();
        List<TRHC__Scratch_Sheet__c> scrathchLists = [SELECT Id, Name, TRHC__Bill_Rate__c, 
                                                            TRHC__Overtime_Pay_Rate__c, TRHC__Holiday_Pay_Rate__c, 
                                                            TRHC__On_Call_Pay_Rate__c, TRHC__Call_Back_Pay_Rate__c, 
                                                            TRHC__Charge_Pay_Rate__c FROM TRHC__Scratch_Sheet__c 
                                                            WHERE Id IN: scrathsheetids];
        for(TRHC__Scratch_Sheet__c scrathSheet : scrathchLists) {
            scrathToScrathsheet.put(scrathSheet.Id, scrathSheet);
        }
        for(TRHC__Placement__c placement : placements) {
            if(placement.TRHC__Scratch_Sheet__c != null && placement.TRHC__Job__c != null) {
                if(jobidToJob.containskey(placement.TRHC__Job__c)) {
                    placement.Shift_Name__c = jobidToJob.get(placement.TRHC__Job__c).Shift_Type2__c;
                    placement.TRHC__Guaranteed_Hours__c = jobidToJob.get(placement.TRHC__Job__c).TRHC__Guarantee_Hours_Pay_Rate__c;
                }
                if(scrathToScrathsheet.containskey(placement.TRHC__Scratch_Sheet__c)) {
                    placement.TRHC__Bill_Rate__c = String.valueof(scrathToScrathsheet.get(placement.TRHC__Scratch_Sheet__c).TRHC__Bill_Rate__c);
                    placement.TRHC__Overtime_Hours_Pay_Rate__c = scrathToScrathsheet.get(placement.TRHC__Scratch_Sheet__c).TRHC__Overtime_Pay_Rate__c;
                    placement.TRHC__Holiday_Hours_Pay_Rate__c = scrathToScrathsheet.get(placement.TRHC__Scratch_Sheet__c).TRHC__Holiday_Pay_Rate__c;
                    placement.TRHC__On_Call_Hours_Pay_Rate__c = scrathToScrathsheet.get(placement.TRHC__Scratch_Sheet__c).TRHC__On_Call_Pay_Rate__c;
                    placement.TRHC__Call_Back_Hours_Pay_Rate__c = scrathToScrathsheet.get(placement.TRHC__Scratch_Sheet__c).TRHC__Call_Back_Pay_Rate__c;
                    placement.TRHC__Charge_Pay_Rate__c = scrathToScrathsheet.get(placement.TRHC__Scratch_Sheet__c).TRHC__Charge_Pay_Rate__c;
                }
            }
        }
    }
}