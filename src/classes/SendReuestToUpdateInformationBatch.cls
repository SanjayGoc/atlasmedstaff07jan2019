/**
* @author : chaitanya Bollamreddi
* @date : 17 April 2018
* @description : Batch class to send email to candidate using template id
*/
public With sharing class SendReuestToUpdateInformationBatch implements DataBase.Batchable<Contact>{
    /**
* @author : chaitanya Bollamreddi
* @date : 17 April 2018
* @param : Database.BatchableContext ctx
* @return : List<contact> contacts
* @description : performing database operation to get the list of related records(contacts are related records)
*/
    Public List<contact> start(Database.BatchableContext ctx){
        
        Atlas_Update_Info_req__c req = Atlas_Update_Info_req__c.getInstance();
        String status = req.Status__c;
        String fieldsChosen = req.fieldsChosen__c;
        String filterConditions = req.filterConditions__c;
        Id candiRecId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Candidate').getRecordTypeId();
        String requiredStatus = 'Active';
        String query = 'Select '+FieldsChosen+' '+'From Contact Where'+' '+filterConditions;
        System.debug('query@@@>>>>>'+query);
        List<Contact> candidates = Database.query(query);
        return candidates;
        
    }
    /**
* @author : chaitanya Bollamreddi
* @date : 17 April 2018
* @param : Database.BatchableContext ctx
* @return : void
* @description : performing actual sending emails
*/
    public void execute(Database.BatchableContext ctx, List<contact> contacts){
        EmailTemplate em = new EmailTemplate();
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Atlas_Update_Info_req__c req = Atlas_Update_Info_req__c.getInstance();
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'info@atlasmedstaff.com'];
        
        for(Contact contact :contacts){
            String conformUrl = 'https://atlasmedstaff.secure.force.com/TRHC__CMSLayoutNew?sessionId=&page=ConformPage&jobSite=Default&p=Logged%20In%20Candidate&CandidateId='+contact.Id;
            String modifyUrl =  'https://atlasmedstaff.secure.force.com/TRHC__CMSLayoutNew?sessionId=&page=UpdatePersonalInformation&jobSite=Default&p=Logged%20In%20Candidate&CandidateId='+contact.Id; 
            String key1 = 'Atlasmedstaff123';
            Boolean isencrypt = true;
            String newPassword = AtlasCryptohelper.aes128(contact.TRHC__Password__c, key1 , isencrypt);
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setBccSender(false);
            mail.setUseSignature(false);
            mail.setSaveAsActivity(false);
            mail.setSubject(req.mailSubject__c);
            String[] toAddresses = new String[] {contact.Email};
                mail.setToAddresses(toAddresses);
            if ( owea.size() > 0 ) {
                mail.setOrgWideEmailAddressId(owea.get(0).Id);
            }
            String emailbody1 = '<html xmlns="http://www.w3.org/1999/xhtml">'+
                '<head>'+
                '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />'+
                '<title></title>'+
                '<meta name="viewport" content="width=device-width, initial-scale=1.0"/>'+
                '</head>'+
                '<body style="margin: 0; padding: 0;">'+
                '<table align="center" border="0" cellpadding="0" cellspacing="0" width="600">'+
                '<tr style = "border: 0;">'+
                '<td align="center" bgcolor=""  style="padding: 40px 0 30px 0;width:100%; border:0px;">'+
                '<img src="https://atlasmedstaff.secure.force.com/resource/1515851764000/Logo1" alt="Creating Email Magic" width="150" height="115" style="display: block;">'+
                '</td>'+
                '</tr>'+
                '<tr style = "border: 0;font-family: arial;font-size: 12pt;"><td bgcolor="#ffffff" style="padding-left:30px;padding-right:30px; padding-bottom:10px;">'+
                '<p>Dear '+ contact.Name +',<br/><br/>'+
                'In an effort to align you with the best and most suitable jobs please take a moment to confirm'+
                ' or update the two highlighted bullets below. <br/><br/>'+
                'Please ensure to have this survey completed by <b style ="color:red">Friday, September 14th.</b> <br/><br/>'+
                ' As a token of appreciation for completing this survey one lucky candidate'+
                ' will win a cash prize of $500. In order to qualify your survey must be'+
                ' completed by the due date.  Live drawing will take place on <b style="color:green">Friday, '+
                ' September 21st</b> on our ' +'<a href="https://www.facebook.com/groups/462043194241449/" target="_blank">Atlas & Friends Facebook Page</a>'+'</p>'+
                '</td></tr>'+
                '<tr style = "border: 0;font-family: arial;font-size: 12pt;"><td bgcolor="#ffffff" style="padding-left:30px;padding-right:30px;"><b>First Name:</b>'+'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+contact.FirstName+'</td></tr>'+
                '<tr style = "border: 0;font-family: arial;font-size: 12pt;"><td bgcolor="#ffffff" style="padding-left:30px;padding-right:30px;"><b>Last Name:</b>'+'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+contact.LastName+'</td></tr>'+
                '<tr style = "border: 0;font-family: arial;font-size: 12pt;"><td bgcolor="#ffffff" style="padding-left:30px;padding-right:30px;"><b>Email:</b>'+'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+contact.Email+'</td></tr>'+
                '<tr style = "border: 0;font-family: arial;font-size: 12pt;"><td bgcolor="#ffffff" style="padding-left:30px;padding-right:30px;"><b>Phone:</b>'+'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+contact.Phone+'</td></tr>'+
                '<tr style = "border: 0;font-family: arial;font-size: 12pt;"><td bgcolor="#ffffff" style="padding-left:30px;padding-right:30px;"><b>Speciality:</b>'+'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+contact.Primary_Speciality1__c+'</td></tr>'+
                '<tr style = "border: 0;font-family: arial;font-size: 12pt;"><td bgcolor="#ffffff" style="padding-left:30px;padding-right:30px;"><b>Discipline:</b>'+'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+contact.Discipline1__c+'</td></tr>'+
                '<tr style = "border: 0;font-family: arial;font-size: 12pt;"><td bgcolor="#ffffff" style="padding-left:30px;padding-right:30px;"><b>Temporary Address:</b>'+''+contact.Temporary_Address_1__c+','+contact.TRHC__Temporary_Street__c+','+contact.TRHC__Temporary_City__c+','+contact.TRHC__Temporary_State_Province__c+','+contact.TRHC__Temporary_Zip_Postal_Code__c+'</td></tr>'+
                '<tr style = "border: 0;font-family: arial;font-size: 12pt;"><td bgcolor="#ffffff" style="padding-left:30px;padding-right:30px;"><b>Encryption key:</b>'+'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-family: arial;font-size: 12px;">'+newPassword+'<i style="color:red">(copy and paste the encryption key as your password)</i></span></td></tr>'+
                '<tr style = "border: 0;font-family: arial;font-size: 12pt;"><td bgcolor="#ffffff" style="padding-left:30px;padding-right:30px;padding-top:10px;">'+
                'If the above information is correct please <a href='+ '\''+conformUrl+'\''+'> click here'+'</a><br/>'+
                '</td>'+
                '</tr>'+
                '<tr style = "border: 0;font-family: arial;font-size: 12pt;padding-top:10px;"><td bgcolor="#ffffff" style="padding-left:30px;padding-right:30px;padding-top:10px;padding-bottom:0px;">'+
                'To change and update your details please <a href=' + '\''+modifyUrl+'\''+'> click here'+'</a></td></tr><br/>'+
                '</td></tr>'+
                '<tr style = "border: 0;font-family: arial;font-size: 12pt;"><td bgcolor="#ffffff" style="padding-left:30px;padding-right:30px;"> Thank You,<br/>Atlas MedStaff'+
                '<tr style = "border: 0;font-family: arial;font-size: 12pt;"><td bgcolor="#ffffff" style="padding-left:30px;padding-right:30px;padding-top:10px;">Please follow and like us on social media</td></tr>'+
                '<tr style = "border: 0;font-family: arial;font-size: 12pt;"><td bgcolor="#ffffff" style="padding-left:30px;padding-right:30px;padding-top:10px;">'+
                '<p dir="ltr" style="line-height:1.656;margin-top:0pt;margin-bottom:0pt">'+
                '<span style="font-size:9.5pt;font-family:Arial;color:rgb(80,0,80);vertical-align:baseline;white-space:pre-wrap">&#8203; </span>'+
                '<a href="https://twitter.com/AtlasMedStaff" target="_blank">'+
                '<span style="font-size:9.5pt;font-family:Arial;color:rgb(17,85,204);vertical-align:baseline;white-space:pre-wrap">'+
                '<img src="https://atlasmedstaff.secure.force.com/resource/1486111292000/TRHC__tw" width="29" height="29" style="border:none"></span></a>'+
                '<a href="https://www.facebook.com/atlasMedStaff" target="_blank">'+
                '<span style="font-size:9.5pt;font-family:Arial;color:rgb(17,85,204);vertical-align:baseline;white-space:pre-wrap">'+
                '<img src="https://atlasmedstaff.secure.force.com/resource/1486111292000/TRHC__fb" width="29" height="29" style="border:none">'+
                '</span>'+
                '</a>'+
                '<a href="https://www.youtube.com/atlasmedstaffmedia" target="_blank">'+
                '<span style="font-size:9.5pt;font-family:Arial;color:rgb(17,85,204);vertical-align:baseline;white-space:pre-wrap">'+
                '<img src="https://atlasmedstaff.secure.force.com//resource/1525894736000/youtube" width="29" height="29" style="border:none">'+
                '</span>'+
                '</a>'+
                '</p>'+
                '</td></tr>'+
                '</table>'+
                '</body></html>';
            mail.setHtmlBody(emailbody1);
            mails.add(mail);
            System.debug('mail@@'+mail);
        }
        
        Messaging.sendEmail(mails);
    }
    /**
* @author : chaitanya Bollamreddi
* @date : 17 April 2018
* @param : Database.BatchableContexandit ctx
* @return : void
* @description : finish is the method to perform actual end operations
*/
    public void finish(Database.BatchableContext ctx){
        
    }
    
}