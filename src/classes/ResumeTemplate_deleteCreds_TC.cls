@isTest
public class ResumeTemplate_deleteCreds_TC {
    @isTest
    public static void delTest(){
        test.startTest();
        TRHC__Config_Settings__c configSettings = new TRHC__Config_Settings__c();
        configSettings.Name = 'Default';
        configSettings.deleteCreds_where__c = 'createddate = LAST_N_DAYS:1';
        insert configSettings;
        
        TRHC__CredentialsCustomSettings__c credSettings =
					new TRHC__CredentialsCustomSettings__c();
        credSettings.Name = 'Default';
        credSettings.TRHC__Disable_CredAnswerCopyAttachmentTrigger__c = true;
        insert credSettings;
        TRHC__Credentials__c cred = new TRHC__Credentials__c();
        cred.Candidate_text__c = '0031b00000MBqgJ';
        cred.Cred_Custom_Type__c = 'Certification';
        //cred.lastmodifieddate = date.today()-2;
        insert cred;
        
        TRHC__Credentials__c Lcred = new TRHC__Credentials__c();
        Lcred.Candidate_text__c = 'xcvitset';
        Lcred.Cred_Custom_Type__c = 'License';
        insert Lcred;
        ResumeTemplate_deleteCreds rd = new ResumeTemplate_deleteCreds();
        Database.executeBatch(rd);
        ResumeTemplate_deleteCreds_Sheduler rTDS = new ResumeTemplate_deleteCreds_Sheduler();
        String sch = '0 0 23 * * ?';
        system.schedule('Test Check', sch, rTDS);
        test.stopTest();
    }
}