@isTest
public class ClientManagerOpenTaskBatchTest{
    public static String CRON_EXP = '0 0 * * * ?';
    static testmethod void ClientManagerTest() {

        User singleUser = new User(
                         ProfileId = [SELECT Id FROM Profile WHERE Name = 'Sales'].Id,
                         LastName = 'last',
                         Email = 'test@email.com',
                         Username = 'test@email.com' + System.currentTimeMillis(),
                         CompanyName = 'TEST',
                         Title = 'Client Manager',
                         Alias = 'alias',
                         TimeZoneSidKey = 'America/Los_Angeles',
                         EmailEncodingKey = 'UTF-8',
                         LanguageLocaleKey = 'en_US',
                         LocaleSidKey = 'en_US');
        insert singleUser;

        Account singleAccount = new Account(Name='Test Hospital');
        insert singleAccount;

        Contact singleContact = new Contact(FirstName = 'firstName',LastName='LastName',AccountId=singleAccount.Id);
        insert singleContact;

        Task singleTask = new Task(OwnerId = singleUser.Id,WhoId= singleContact.Id,WhatId=singleAccount.Id,Status='Not Started',ActivityDate = System.today(),Subject='Left Message (LM)');
        insert singleTask;

        Test.startTest();
        String scheduledJobId = System.schedule('ScheduledApexTest',
            CRON_EXP, 
            new ClientManagerOpenTaskScheduler());
        System.debug('scheduledJobId:'+scheduledJobId);
        Test.stopTest();

        User matchUser = [SELECT Id,UserName FROM User WHERE Email =:'test@email.com'];
        System.assertEquals(singleUser.Id,matchUser.Id);

        Task matchTask = [SELECT Id,WhoId,WhatId FROM Task WHERE OwnerId =: matchUser.Id];
        System.assertEquals(singleAccount.Id,matchTask.WhatId);
        System.assertEquals(singleContact.Id,matchTask.WhoId);
        System.assertEquals(singleTask.Id,matchTask.Id);
    }
}