@istest
public class MatchedJObsForCandidatesControllerTest {
    @istest
    public static void contactsAndJobs(){
        
        TRHC__Config_Settings__c setting = new TRHC__Config_Settings__c();
        setting.Name = 'Default';
        setting.DisabletrackEventActivityonContact__c = false;
        setting.DisabletrackTaskActivityonContact__c = false;
        setting.Enable_Update_Recruiter__c = false;
        setting.DisabletrackNoteonContact__c = false;
        setting.Trigger_Enable_Expected_End_Date_Changer__c = false;
        setting.Enable_Update_Recruiter__c = true;
        insert setting;
        TRHC__CustomSettingsComponent__c setting1 = new TRHC__CustomSettingsComponent__c(Name='Default');
        setting1.TRHC__JobListFastSearch__c = true ;
        setting1.TRHC__Count_Jobs_for_Job_List_Page__c = null ;
        setting1.TRHC__isShowPagination__c = true ;
        setting1.TRHC__Enable_Jobs_location_search__c = true ;
        setting1.TRHC__Job_List_Page_fields_for_query__c = 'TRHC__Job_Title__c,TRHC__Job_Term__c,TRHC__Job_Category__c,TRHC__Country_Locale__c,TRHC__Job_Summary__c,TRHC__Job_City__c,TRHC__State__c,Pay_Unit__c,Job_Type__c,TRHC__Account_Job__r.name,Pay_Rate_High__c,Pay_Rate_Low__c' ;
        setting1.TRHC__ErrorsReceiver__c = 'test@test.com';
        setting1.TRHC__SessionTimeout__c = Decimal.valueOf('9999999');
        setting1.TRHC__Job_Search_Agent_Field__c = 'Name,Job_Title__c';        
        setting1.MatchedJobsCriteria__c = 'TRHC__Publish__c = True';
        insert setting1;
        
        
        Contact contact = new Contact();
        contact.FirstName = 'chaitanya';
        contact.LastName = 'bollamreddy';
        contact.TRHC__Recruiter__c = UserInfo.getUserId();
        contact.Discipline1__c = 'RN';
        contact.TRHC__Discipline__c = 'RN';
        contact.TRHC__Candidate_Status__c = 'Active';
        contact.RecordTypeId = [SELECT Id FROM RecordType where sobjecttype = 'CONTACT' and name in ('Candidate') LIMIT 1].Id;
        insert contact;

        Account account = new Account();
        account.Name = 'test';
        insert account;
               
        TRHC__Job__c job = new TRHC__job__c();
        job.TRHC__Stage__c = 'void';
        job.TRHC__Closed__c = False;
        job.TRHC__Publish__c = True;
        job.TRHC__Account_Lookup__c = account.Id;
        job.TRHC__Discipline__c = 'RN';
        insert job;
        test.startTest();
        MatchedJObsForCandidatesController controller = new MatchedJObsForCandidatesController();
        controller.getMatchedJobs();
        test.stopTest();
        
    }
}