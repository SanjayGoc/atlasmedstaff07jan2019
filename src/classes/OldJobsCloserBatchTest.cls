@isTest()
public class OldJobsCloserBatchTest {
    
    Public static TestMethod void testOldJobCloser(){
        TRHC__Config_Settings__c config = new TRHC__Config_Settings__c();
        config.name = 'Default';
        config.Trigger_Enable_Expected_End_Date_Changer__c = true;
        insert config;
        Test.startTest(); 
        
        Account account = new Account();
        account.Name = 'test';
        insert account;
        
        Date credDate= Date.today()-35;
        Date lasDate = Date.today()-34;
        
        TRHC__Job__c job = new TRHC__job__c();
        job.TRHC__Stage__c = 'void';
        job.RecordTypeId = Schema.SObjectType.TRHC__Job__c.getRecordTypeInfosByName().get('Job').getRecordTypeId();
        job.CreatedDate = credDate;
        job.TRHC__Start_Date__c = lasDate;
        job.TRHC__Closed__c = False;
        job.TRHC__Publish__c = True;
        job.Lastmodifieddate = lasDate;
        job.TRHC__Account_Lookup__c = account.Id;
        insert job;
        
        OldJobsCloserBatch oldJobBatch = new OldJobsCloserBatch ();
        Database.executeBatch(oldJobBatch);
        test.stopTest();
    }

}