@isTest
public class PlacementVMSCountControllerCloneTest{
	public static testmethod void test1() {
		TRHC__Config_Settings__c configSettings = new TRHC__Config_Settings__c();
        configSettings.name='Default';
        configSettings.Enable_AutoMateJobRates__c = true;
        configSettings.Trigger_Enable_ScratchSheetAutoAddExp__c = false;
        configSettings.AutoAddExpenseCriteria__c='TRHC__Margin_Type__c=\'Expense\'';
        configSettings.Except_Hospitals__c = 'Atlas Development';
        configSettings.Trigger_Enable_Expected_End_Date_Changer__c = false;
        configSettings.Trigger_Enable_Applicant_From_SCR_Sheet__c = true;
        configSettings.Update_Pay_Burden__c = false;
        insert configSettings;

		Periodically_Date__c periodicallyDate = new Periodically_Date__c();
		periodicallyDate.Name = 'Period02';
		periodicallyDate.Start_Date__c = System.today()- 100;
		periodicallyDate.End_Date__c =System.today() + 100;
		insert periodicallyDate;

		Periodically_Date__c periodicallyDate1 = new Periodically_Date__c();
		periodicallyDate1.Name = 'ALL';
		periodicallyDate1.Start_Date__c = System.today()- 100;
		periodicallyDate1.End_Date__c =System.today() + 100;
		insert periodicallyDate1;

		Profile p = [SELECT Id FROM Profile WHERE Name =: 'System Administrator'];

		User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com');

		List<Account> accountList = TestUtilityClass.getTestMultipleAccount();
		List<Contact> ContactList = TestUtilityClass.getTestMultipleContact(accountList[0]);
	//	List<TRHC__Contract__c> contractList = TestUtilityClass.getTestMultipleContract(accountList);
		
		TRHC__Placement__c placement = new TRHC__Placement__c();
		placement.TRHC__Recruiter__c = u.ID;
		placement.TRHC__Hiring_Manager__c = ContactList[2].Id;
		placement.TRHC__Employer__c = accountList[1].Id;
	//	placement.TRHC__Account_Contract__c = contractList[1].Id;
		placement.TRHC__Contact_Candidate__c = ContactList[1].Id;
		placement.TRHC__Start_Date__c = System.Today().addMonths(-3);
		placement.TRHC__End_Date__c = System.today();
		placement.CreatedDate = System.today() -1;
		placement.VMS__c = 'AMN';
		insert placement;
	
	   	ApexPages.currentPage().getParameters().put('recNameForGPH','Eric Scheid');
		ApexPages.currentPage().getParameters().put('month','March');
		ApexPages.currentPage().getParameters().put('period','Period01');

		PageReference myVfPage = Page.ClientManagerDashboardPage;
	   	myVfPage.getParameters().put('Name','Test');
	   	Test.setCurrentPage(myVfPage); 
		
		PlacementVMSCountController placementObj = new PlacementVMSCountController();
	//	placementObj.getpageParameter();
	//	placementObj.getaverageGphplacList();

		placementObj.getplacList();
		placementObj.showVMSAccountData();
		placementObj.redirectplac();
		placementObj.getMonthsMap();
		placementObj.generateDateFromMonth();
		placementObj.SelectPeriod(periodicallyDate.Name);
		placementObj.singlePickList = 'CM';
		placementObj.startDateOfWeek =  '2018-07-01';
		placementObj.startDateOfMonth = '2018-07-01';
		placementObj.selectedDaysForCM = periodicallyDate.Name;
		placementObj.getCount();

		placementObj.singlePickList = 'CM';
		placementObj.getExtensionData();
		placementObj.getMonthsOptions();
		placementObj.getRecruiterPeriodList();
	//	placementObj.getaverageGphplacList();
	//	placementObj.getGphCountchart();
		placementObj.YesterdaysGPH();

		placementObj.singlePickList = 'CmAndRec';
		Date startdatetime = System.today() - 10;
		string s = string.valueofGmt(startdatetime);

		placementObj.startDateOfWeekForRecAndCm = s;
		placementObj.startDateOfMonthForRecAndCm = s;
		placementObj.recAndCmPeriod = periodicallyDate.Name;
		placementObj.getCountRecandCm();
		placementObj.showAverageGPHData();

		PlacementVMSCountController placementObj2 = new PlacementVMSCountController();
		placementObj2.singlePickList = 'Recruiter';
		placementObj2.startDateOfWeekForRC =  s;
		placementObj2.startDateOfMonthForRC = s;
		placementObj2.selectedDaysForRC = periodicallyDate.Name;
		placementObj2.getCount();	

		placementObj2.averageGphPeriod = periodicallyDate1.Name;
		placementObj2.numberOfMonthForGph = periodicallyDate1.Name;
		placementObj2.getAverageGph();

	}
		
}