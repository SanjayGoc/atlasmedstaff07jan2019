/*
*@author : chaitanya.b@targetrecruit.net
*@Desc : Test class for code coverage and testing the cases
*/
@isTest
public class TestRecruiterUpdateOfContact {
	static testMethod void emailTest(){
		//insert the custom settings 
	 	TRHC__Config_Settings__c config = new TRHC__Config_Settings__c();
	 	ID cid = [		
				SELECT id, Name 
				from RecordType 
				where Name = 'Candidate'].Id;
		config.name = 'Default';
		config.Enable_Update_Recruiter__c = true;
		
		insert config;
		//Creation of Contact
		Contact testContact = new Contact();
		testContact.lastName = 'Tr';
		testContact.Recruiters__c = SObjectType.Contact.Fields.Recruiters__c.PicklistValues[0].getValue();
		testContact.RecordTypeId = cid;
		//insert operation
		insert testContact;
		testContact.Recruiters__c = SObjectType.Contact.Fields.Recruiters__c.PicklistValues[0].getValue();
		//update operation
		update testContact;
	}
	 
}