/**
* @author  chaitanya ( chaitanya.b@targetrecruit.net )
* @date  05-03-2018
* @description  Controller is used to get the relevent detarelated to an vfpage called ResumeTemplate
*/
global with sharing  class ResumeTemplateController {

    public List<TRHC__Employment_History__c> employementHistories { get;set; }
    Public List<TRHC__Educational_History__c > educationHistories { get;set; }
    private static string employmentFields = '';
    private static string educationFields = '';
    private static String credentialFields = '';
    private static string conFields = '';
    public String candiId { get;set; }
    public contact con { get;set; }
    public class CandidateCoverageException extends Exception {}
    public Map<id, TRHC__Employment_History__c> empHistoryMap;
    public List<TRHC__Credentials__c> certCreds { get;set; }
    public List<TRHC__Credentials__c> licenseCreds {get;set;}
    /**
* @author Chaitanya Bollamreddy 
* @date 6 Aprial 2018
* @name getEmployeedetails
* @param
* @ReturnType Void
* @description Empolyee details like Education History, Employment History, Credentials
*/
    public void getEmployeedetails(){
        employementHistories = new List< TRHC__Employment_History__c > ();
        educationHistories = new List< TRHC__Educational_History__c > ();
        candiId = ApexPages.currentPage().getParameters().get('Id');
        if(conFields == null || conFields == ''){
            Map<String, Schema.SObjectField> conMap= Schema.SObjectType.Contact.fields.getMap();
            for(String conField :conMap.keySet()){
                conFields = conFields +  conField + ' ,' ;
            }
        }
        
        if(employmentFields == null || employmentFields == ''){
            Map<String, Schema.SObjectField> employmentMap= Schema.SObjectType.TRHC__Employment_History__c.fields.getMap();
            for(String empField :employmentMap.keySet()){
                employmentFields = employmentFields + empField + ' ,' ;
            }
        }
        if(educationFields == null || educationFields == ''){
        	Map<String, Schema.SObjectField> educationalMap= Schema.SObjectType.TRHC__Educational_History__c.fields.getMap();
            for(String eduField :educationalMap.keySet()){
                educationFields = educationFields + eduField + ' ,' ;
            }    
        }
        if(credentialFields == null || credentialFields.trim() == ''){
            Map<String, Schema.SObjectField> credFieldsMap= Schema.SObjectType.TRHC__Credentials__c.fields.getMap();
            for(String credField :credFieldsMap.keySet()){
                credentialFields = credentialFields + credField + ' ,' ;
            } 
        }
        
        String conQuery =
            ' SELECT '+
            conFields.removeEnd(',')+
            ',TRHC__Account_Manager__r.Email, TRHC__Account_Manager__r.Name FROM CONTACT'+
            ' WHERE ID = '+'\''+candiId+'\''+
            ' Limit 1';
        
        con = Database.query(conQuery);
        
        String employementHistoriesQuery = 
            ' SELECT '+ employmentFields.removeEnd(',') +
            ' FROM TRHC__Employment_History__c '+
            ' WHERE TRHC__Candidate__c = '+'\''+candiId+'\''+' Order BY trhc__sdate__c asc';
        
        String educationalQuery =
            ' SELECT '+ educationFields.removeEnd(',') +
            ' FROM TRHC__Educational_History__c '+
            ' WHERE Candidate_text__c = '+'\''+candiId+'\''+' Order BY TRHC__GradDate__c asc';
        educationHistories = Database.query(educationalQuery);
       
        String CredsQuery =
            ' SELECT '+ credentialFields.removeEnd(',') +
            ' FROM TRHC__Credentials__c '+
            ' WHERE Candidate_text__c = '+'\''+candiId+'\''+' Order BY TRHC__Expiration_Date__c asc';
        for(TRHC__Credentials__c cred :Database.query(CredsQuery))
        {
            if(cred.Cred_Custom_Type__c == 'Certification'){
                if(certCreds == null)
                certCreds = new List<TRHC__Credentials__c>();
                certCreds.add(cred);
            }else if(cred.Cred_Custom_Type__c == 'License'){
                if(licenseCreds == null)
                licenseCreds =new List<TRHC__Credentials__c> ();    
                licenseCreds.add(cred);
            }    
        }        
        empHistoryMap = new Map< id, TRHC__Employment_History__c > ((List<TRHC__Employment_History__c>)Database.query( employementHistoriesQuery ));
        for( TRHC__Employment_History__c employmentHistory :empHistoryMap.values()){
            employementHistories.add(employmentHistory);
        }
    }
    
    public PageReference createCertCredential(){
        createCred('Certification');
        return null; 
    }
    
    public PageReference createLicenseCredential(){
        createCred('License');
        return null; 
    }
    
    public void createCred(String Type){
        TRHC__Credentials__c credentials = new TRHC__Credentials__c ();
        credentials.Candidate_text__c = candiId;
        if(Type == 'Certification'){
            credentials.Cred_Custom_Type__c = 'Certification';
            insert credentials;
            if(certCreds == Null)
            certCreds = new List<TRHC__Credentials__c> ();
            
            certCreds.add(credentials);  
        }else if(Type == 'License'){
            credentials.Cred_Custom_Type__c = 'License';
            insert credentials;
            if(licenseCreds==Null)
            licenseCreds = new List<TRHC__Credentials__c> (); 
            
            licenseCreds.add(credentials);  
        }
    }
    
    public PageReference updateEmployeeRecord(){
        update employementHistories;
        return null;
    }
    public pagereference createNewEmployment(){
        
        TRHC__Employment_History__c employment = new TRHC__Employment_History__c();
        employment.TRHC__Candidate__c = candiId;
        insert employment;
        if(employementHistories.size()>0){
            employementHistories.add(0,employment);
        }else{
            employementHistories.add(employment);
        }
        
        empHistoryMap.put(employment.id, employment);
        return null;
    }
    
    public pagereference createNewEducation(){
        TRHC__Educational_History__c education = new TRHC__Educational_History__c();
        education.Candidate_text__c = candiId;
        education.TRHC__Candidate__c = '0031L00002lpbVy';
        insert education;
        educationHistories.add(education);
        return null;
    }
    public Pagereference updateCertsRecord(){
        if(certCreds != null && certCreds.size()>0)
        update certCreds;
        return null;
    }
    
    public Pagereference updateLicensesRecord(){
        if(licenseCreds != null && licenseCreds.size()>0)
        update licenseCreds;
        return null;
    }
    
    public Pagereference removeEmpHist(){
        if(employementHistories != null && employementHistories.size()>0){
            String empId = apexpages.currentpage().getParameters().get('EmpId');
            if(employementHistories.contains(empHistoryMap.get(empId)))
                employementHistories.remove(employementHistories.indexOf(empHistoryMap.get(empId)));
        }
        return null;
    }
    
    public Pagereference removeCertsRecord(){
        if(certCreds != null && certCreds.size()>0){
            String certId = apexpages.currentpage().getParameters().get('CertId');
			certCreds.remove(Integer.valueOf(certId));
        }
        return null;
    }
    
    public Pagereference removeLicensesRecord(){
        if(licenseCreds != null && licenseCreds.size()>0){
            String LicId = apexpages.currentpage().getParameters().get('LicId');
			licenseCreds.remove(Integer.valueOf(LicId));
        }
        return null;
    }

    
    public Pagereference removeEduHist(){
        if(educationHistories != null && educationHistories.size()>0){
            String eduid = apexpages.currentpage().getParameters().get('EduId');
			educationHistories.remove(Integer.valueOf(eduid));
        }
        return null;
    }
    
    public PageReference updateEducationRecord(){
        update educationHistories;
        return null;
    }
    
    public PageReference updateCureentCandidate(){
        update con;
        return null;
    }
    public PageReference generateCandidateCoverage() {
        TRHC__Employment_History__c empTemp = new TRHC__Employment_History__c();
        if(employementHistories != null)
            for(Integer i = 0; i<employementHistories.size(); i++ ){
                for (Integer j = i + 1; j < employementHistories.size(); j++){
                    if (employementHistories[i].TRHC__Sdate__c < employementHistories[j].TRHC__Sdate__c) {
                        empTemp = employementHistories[i];
                        employementHistories[i] = employementHistories[j];
                        employementHistories[j] = empTemp;   
                    }
                }
            }
        TRHC__Educational_History__c eduTemp = new TRHC__Educational_History__c();
        if(educationHistories != null)
            for(Integer i = 0; i<educationHistories.size(); i++ ){
                for (Integer j = i + 1; j < educationHistories.size(); j++){
                    if (educationHistories[i].TRHC__GradDate__c < educationHistories[j].TRHC__GradDate__c) {
                        eduTemp = educationHistories[i];
                        educationHistories[i] = educationHistories[j];
                        educationHistories[j] = eduTemp;   
                    }
                }
            }
        TRHC__Credentials__c certTemp = new TRHC__Credentials__c();
        if(certCreds!= null)
            for(Integer i = 0; i<certCreds.size(); i++ ){
                for (Integer j = i + 1; j < certCreds.size(); j++){
                    if (certCreds[i].TRHC__Expiration_Date__c < certCreds[j].TRHC__Expiration_Date__c) {
                        certTemp = certCreds[i];
                        certCreds[i] = certCreds[j];
                        certCreds[j] = certTemp;   
                    }
                }
            }
        TRHC__Credentials__c liceTemp = new TRHC__Credentials__c();
        if(licenseCreds != null)
            for(Integer i = 0; i<licenseCreds.size(); i++ ){
                for (Integer j = i + 1; j < licenseCreds.size(); j++){
                    if (licenseCreds[i].TRHC__Expiration_Date__c < licenseCreds[j].TRHC__Expiration_Date__c) {
                        liceTemp = licenseCreds[i];
                        licenseCreds[i] = licenseCreds[j];
                        licenseCreds[j] = liceTemp;   
                    }
                }
            }
        
        Cache.SessionPartition sessionPart = Cache.Session.getPartition('local.medstaffcache');
        if(this.certCreds != Null){
            sessionPart.put('credCertifications', this.certCreds);   
        }else{
            sessionPart.put('credCertifications', new List<TRHC__Credentials__c> ());
        }
        
        if(this.licenseCreds != Null){
            sessionPart.put('credLicenses', this.licenseCreds);    
        }else{
            sessionPart.put('credLicenses', new List<TRHC__Credentials__c> ());
        }
        
        if(this.educationHistories != Null){
            sessionPart.put('educationHistories', this.educationHistories);   
        }else{
            sessionPart.put('educationHistories', new List<TRHC__Educational_History__c> ());    
        }
        
        if(this.employementHistories != Null){
            sessionPart.put('employementHistories', this.employementHistories);  
        }else{
            sessionPart.put('employementHistories', new List<TRHC__Employment_History__c> ());
        }
        
        return new PageReference('https://na76.salesforce.com/apex/createNewPdf?Id='+candiId);
        
    }
    
    
}