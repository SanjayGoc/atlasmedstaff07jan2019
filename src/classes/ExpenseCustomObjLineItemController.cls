public class ExpenseCustomObjLineItemController{

    /*public static Final String stemplaterecTypeId = Schema.SObjectType.TRHC__Scratch_Sheet__c.getRecordTypeInfosByName().get('ScratchSheet Template').getRecordTypeId();
       
    public List<Expense__c> expenseList {get;set;}
    public String scrId{get;set;}
    public Boolean isDuplicate{get;set;}
    public Boolean hideExpenseButton{get;set;}
    public TRHC__Scratch_Sheet__c scratchObj;
    public String ExpenseToEdit {get;set;}
    public String editableFieds{get;set;}
    public List<String> amountColumns {get;set;}
    public List<String> columns {get;set;}
    public List<Expense__c> expenseListRead{get;set;}
    private List<Expense__c> lstExpenseDelete;
    public map<id,Decimal> getExpenseValuemap ;

    public ExpenseCustomObjLineItemController(ApexPages.StandardController stdCon){
        scratchObj = (TRHC__Scratch_Sheet__c )stdCon.getRecord();
        getExpenseValuemap = new map<id,decimal>();
        scrId = scratchObj.Id;
        expenseList = new List<Expense__c>();
        expenseListRead = new List<Expense__c>();
        lstExpenseDelete = new List<Expense__c>();
        amountColumns = new List<String>();
        columns = new List<String>();
        editableFieds ='';
        ExpenseToEdit = System.Label.Editable_Expense;
        hideExpenseButton = scratchObj.RecordTypeId != stemplaterecTypeId ? true: false;
    }
    
    public void init(){
        Set<String> setEditAccess = new set<String>();
        if(ExpenseToEdit!=Null){
            setEditAccess.addAll(ExpenseToEdit.Split(','));
        }
        If(!setEditAccess.isEmpty()){
            String query = getCreatableFieldsSOQL('Expense__c','Scratch_Sheet__c = \''+ scratchObj.Id + '\''+' AND Expense_Name__c NOT IN:setEditAccess','Config__r.Name');
            expenseListRead = Database.query(query);
            query = getCreatableFieldsSOQL('Expense__c','Scratch_Sheet__c = \''+ scratchObj.Id + '\''+' AND Expense_Name__c  IN:setEditAccess','Config__r.Name');
            expenseList = Database.query(query);
        }else{ 
            String query = getCreatableFieldsSOQL('Expense__c','Scratch_Sheet__c = \''+ scratchObj.Id + '\'','Config__r.Name');
            expenseList = Database.query(query);
        }
        for(String f : geteditableColumns()){
            editableFieds += f + ',';
        }
        
        for(Expense__c expeObj : expenseList){
            if(expeObj.Value__c != null){
                getExpenseValuemap.put(expeObj.id,expeObj.Value__c);
            }
        }
        amountColumns = getamountColumns();
        columns = getColumns();
    }
    
    public void updateExpenseFromConfig(){
    
    }
    
    public void addRow(){ system.debug('==inside row==');
    Expense__c objExpence = new Expense__c(Scratch_Sheet__c =scratchObj.Id);
    expenseList.add(objExpence);
    }
    
    public pageReference saveLines(){
        Boolean isUpdate;
        if(!expenseList.isEmpty())
            //update expenseList;
            try{
                For(Expense__c exObj : expenseList){
                    if(getExpenseValuemap.get(exObj.id) != exObj.Value__c){
                        isUpdate = true;
                        break;
                    } else {
                        isUpdate = false;
                    }
                }
                if(isUpdate){
                    update expenseList;
                }
            }catch(Exception ex){
                System.debug('==ex=='+ex);
            }
            
        return null;
    }
    public pageReference  deleteRow(){
        String index = apexpages.currentpage().getparameters().get('rowIndex');
        delete expenseList[Integer.valueOf(index)]; */
        
       /* Integer indexVal = Integer.valueof(system.currentpagereference().getparameters().get('rowIn1'));
        System.Debug('==indexVal=='+indexVal);

        if(expenseList[indexVal].Id != null)
            lstExpenseDelete.add(expenseList[indexVal]);
        
        expenseList.remove(indexVal);          
        
        return null;
    }
    */
    /*public pageReference  deleteReadRow(){
        String index = apexpages.currentpage().getparameters().get('rowIndex');
        delete expenseList[Integer.valueOf(index)];*/ 
        
        /* Integer indexVal = Integer.valueof(system.currentpagereference().getparameters().get('rowIndex'));
        
        if(expenseListRead[indexVal].Id != null)
            lstExpenseDelete.add(expenseListRead[indexVal]);
        
        expenseListRead.remove(indexVal);          
        
        return null;
    }*/
    
   
    
   /* public List<String> getcolumns(){
        List<String> fields = new List<String>();
        for(Schema.FieldSetMember f : SObjectType.Expense__c.FieldSets.Expense_Columns.getFields()) {
            fields.add(f.getFieldPath());
        }
        return fields;
    }
    
    public List<String> getamountColumns(){
        List<String> fields = new List<String>();
        for(Schema.FieldSetMember f : SObjectType.Expense__c.FieldSets.Amount_Columns.getFields()) {
            fields.add(f.getFieldPath());
        }
        return fields;
    }
    

    public set<String> geteditableColumns(){
        set<String> fields = new set<String>();
        for(Schema.FieldSetMember f : SObjectType.Expense__c.FieldSets.Editable_fields.getFields()) {
            fields.add(f.getFieldPath());
        }
        return fields;
    }
    
    public static string getCreatableFieldsSOQL(String objectName, String whereClause,String additionalFields)
    {
        system.debug('---objectName---'+objectName);
        system.debug('---whereClause---'+whereClause);
        system.debug('---additionalFields---'+additionalFields);
        String selects = '';
       
        if (whereClause == null || whereClause == ''){ return null; }
      
        Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(objectName.toLowerCase()).getDescribe().Fields.getMap();
        list<string> selectFields = new list<string>();
 
*/
       /* if (fMap != null){
            for (Schema.SObjectField ft : fMap.values()){*/ // loop through all field tokens (ft)
                /*Schema.DescribeFieldResult fd = ft.getDescribe();*/ // describe each field (fd)
              //  if (fd.isCreateable()){ // field is creatable                    
                   /* selectFields.add(fd.getName());*/
              //  }
           /* }
        }
        
       if(!selectFields.isEmpty()){
            for (string s:selectFields){
                selects += s + ',';
            }
            selects += additionalFields;
                if (selects.endsWith(',')){selects = selects.substring(0,selects.lastIndexOf(','));}
            
        }
        return 'SELECT ' + selects + ' FROM ' + objectName + ' WHERE ' + whereClause;
    }*/
}