public class ResumeTemplateHelper {
    private static string conFields = '';
    public contact con { get;set; }
    public Pagereference genEmailAttachment(){
     String candiId =  ApexPages.currentPage().getParameters().get('Id');
        
        if(conFields == null || conFields == ''){
            Map<String, Schema.SObjectField> conMap= Schema.SObjectType.Contact.fields.getMap();
            for(String conField :conMap.keySet()){
                conFields = conFields +  conField + ' ,' ;
            }
        }
        
        String conQuery =
            ' SELECT '+
            conFields.removeEnd(',')+
            ',TRHC__Account_Manager__r.Email, TRHC__Account_Manager__r.Name FROM CONTACT'+
            ' WHERE ID = '+'\''+candiId+'\''+
            ' Limit 1';
        system.debug('conQuery @@@'+conQuery);
        con = Database.query(conQuery);
        //List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        PageReference pdf = page.ResumeTempltePdf;
        pdf.getParameters().put('Id',con.Id);
        Blob pdfBlob;
        
        try{
            if (Test.IsRunningTest())
            {
                pdfBlob = Blob.valueOf('UNIT.TEST');
            }
            else
            {
                pdfBlob = pdf.getContent();
                system.debug('pdfBlob@@@'+pdfBlob);
            }
        }
        catch(Exception e){
            system.debug('Test@@@'+e);
        }
        
        Attachment attachment = new Attachment();
        attachment.Body = pdfBlob;
        attachment.Name = String.valueOf(con.Name + '- Cover Page');
        attachment.ParentId = con.id;
        List<Attachment> attchments = [
            SELECT Name,Parentid, Parent.Type 
            FROM Attachment 
            WHERE Parent.Type = 'Contact' 
            AND Name =:attachment.Name 
            Limit 1];
        try{
            if(attchments.size()>0){
                attachment.Id = attchments[0].id;
            }
            upsert attachment;
        }catch(Exception e){
            //new CandidateCoverageException('Nurse record Client Manager is missing',e);
        }
        update con;
        return new PageReference('https://na76.salesforce.com/'+candiId);
    }
}