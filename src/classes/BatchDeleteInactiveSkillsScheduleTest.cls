/*
========================================================================
Description: Unit Class for BatchDeleteInactiveSkills class 
========================================================================
*/

@istest
public class BatchDeleteInactiveSkillsScheduleTest {
    static testmethod void SendExpNotifyBtcSchTest() {
        Test.StartTest();
        BatchDeleteInactiveSkillsSchedule schTest = new BatchDeleteInactiveSkillsSchedule();      
        String sch = '0 0 23 * * ?';
        system.schedule('Delete InActive Skills', sch, schTest);
        Test.stopTest();
    }
}