public class PopulateAllowanceOnSubmittalHandler {
	public static void populateAllance(List<TRHC__Job_Applicant__c> submittalList){
		Set<Id> scratchSheetIds = new Set<Id>();
		for(TRHC__Job_Applicant__c submittal : submittalList) {
			scratchSheetIds.add(submittal.TRHC__Scratch_Sheet__c);
		}
		System.debug('===scratchSheetIds==='+scratchSheetIds);
		Map<String, TRHC__Expense__c> scratchExpenseMap = new Map<String, TRHC__Expense__c>();
		for(TRHC__Expense__c exp : [SELECT Id, TRHC__Scratch_Sheet__c, 
                                        TRHC__Expense_Name__c, TRHC__Weekly_Amount__c  
                                        FROM TRHC__Expense__c 
                                        WHERE TRHC__Scratch_Sheet__c IN : scratchSheetIds 
                                            AND TRHC__Expense_Name__c IN ('Per Diem','Housing Stipend')] ) {
            scratchExpenseMap.put(exp.TRHC__Expense_Name__c + exp.TRHC__Scratch_Sheet__c, exp);
        }
       
        for(TRHC__Job_Applicant__c submittal : submittalList) {
        	if(scratchExpenseMap.containsKey('Per Diem' + submittal.TRHC__Scratch_Sheet__c)) {
                submittal.Per_Diem_Allowance_Weekly__c = scratchExpenseMap.get('Per Diem' + submittal.TRHC__Scratch_Sheet__c).TRHC__Weekly_Amount__c;
            } 
            if(scratchExpenseMap.containsKey('Housing Stipend' + submittal.TRHC__Scratch_Sheet__c)) {
                submittal.Housing_Allowance_Weekly__c = scratchExpenseMap.get('Housing Stipend' + submittal.TRHC__Scratch_Sheet__c).TRHC__Weekly_Amount__c;
            }
        }
	}
}