@RestResource(urlMapping='/mobile/NotificationList/*')
global class CommunityNotificationListRESTOperation {
    @HttpGet
    global static ResponseHandler CommunityNotificationListRESTOperation(){
        ResponseHandler res = new ResponseHandler();
        String apiToken = RestContext.request.params.get('apiToken');
        
        List<Contact> userList = [Select Id,
                                         Name,
                                         firstName,
                                         lastName,
                                         apiToken__c
                                From Contact
                                Where apiToken__c =: apiToken
                                ];
                                
        if(!userList.isempty()){
            List<Notification__c> listOfnotification  = new List<Notification__c>();
            listOfnotification = [select id,name,Read_Unread__c,Job__r.Name,Job__r.TRHC__Job_Title__c,Job__r.Compact_License_States__c ,Job__r.Speciality1__c,Job__r.TRHC__Job_City__c,Job__r.State_pic__c,Job__r.TRHC__Job_Description_Rich__c,Job__r.Shift_Type2__c,Job__r.PayScale__c from Notification__c where Contact__c =: userList[0].id];
            res.success = 1;
            res.message = new string[]{'Success'};
            if(!listOfnotification.isEmpty()){
                res.notificationList = listOfnotification;
            } else {
                res.notificationList = new List<Notification__c>();
            }
            return res;
        }
            res.success = 2;
            res.message = new string[]{'Unauthorized User.'};
        return res;
    }
    
    global class ResponseHandler {
        global integer success;
        global string[] message;
        global List<Notification__c> notificationList;
        global ResponseHandler (){}
    }
}