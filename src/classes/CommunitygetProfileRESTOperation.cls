@RestResource(urlMapping='/mobile/getProfile/*')
global class CommunitygetProfileRESTOperation {
	@HttpGet
    global static ResponseHandler CommunitygetProfileRESTOperation(){
    	ResponseHandler res = new ResponseHandler();

    	personalInformation perInfoObj = new personalInformation();
        permenentAddress perAddObj = new permenentAddress(); 
        temporaryAddress tempAddObj = new temporaryAddress();
        
    	String apiToken = RestContext.request.params.get('apiToken');
    	string deviceType = RestContext.request.params.get('deviceType');
        String deviceId = RestContext.request.params.get('deviceId');

    	List<Contact> userList = [Select Id,
                                         Name,
                                         firstName,
                                         lastName,
                                         apiToken__c,
                                         Email,
                                         Phone,
                                         Discipline1__c,
                                         Primary_Speciality1__c,
                                         Secondary_Speciality__c,
                                         TRHC__Shift_Preference__c,
                                         Permanant_Address__c,
                                         TRHC__Permanent_Address_2__c, 
                                         TRHC__Permanent_City__c,
                                         State_pic__c,
                                         TRHC__Permanent_Zip_Postal_Code__c,
                                         Temporary_Address_1__c,
                                         Temporary_Address_2__c,
                                         TRHC__Temporary_City__c,
                                         temporary_state__c,
                                         TRHC__Temporary_Zip_Postal_Code__c
                                From Contact 
                                Where apiToken__c =: apiToken 
                                ];
    	if(!userList.isempty()){
            
            if(deviceType == null || deviceId == null){
                res.success = 0;
                res.message = new string[]{'Not Found'};
                return res;
            }
            res.FullName = userList[0].Name;
            res.User_Id = userList[0].Id ;
            res.success = 1;
            res.message = new string[]{'Success'};
            res.User_Id = userList[0].Id ;
            res.deviceId = deviceId;
            res.securityToken = apiToken;
            
            res.deviceType = deviceType == '1' ? 'Anroid' : 'iOS';
    
            perInfoObj.firstName = userList[0].firstName;
            perInfoObj.lastName = userList[0].lastName;
            perInfoObj.email = userList[0].Email;
            perInfoObj.phone = userList[0].Phone;
            perInfoObj.desipline = userList[0].Discipline1__c;
            perInfoObj.primarySpeciality = userList[0].Primary_Speciality1__c;
            
            if(userList[0].Secondary_Speciality__c != null){
                perInfoObj.seconSpeci = userList[0].Secondary_Speciality__c.split(';');
            } else {
                perInfoObj.seconSpeci = new List<String>();
            }
            if(userList[0].TRHC__Shift_Preference__c != null){
                perInfoObj.shiftPrefe = userList[0].TRHC__Shift_Preference__c.split(';');
            } else {
                perInfoObj.shiftPrefe = new List<String>();
            }

            perAddObj.address1 = userList[0].Permanant_Address__c;
            perAddObj.address2 = userList[0].TRHC__Permanent_Address_2__c;
            perAddObj.city = userList[0].TRHC__Permanent_City__c;
            perAddObj.state = userList[0].State_pic__c;
            perAddObj.zipcode = userList[0].TRHC__Permanent_Zip_Postal_Code__c;

            tempAddObj.address1 = userList[0].Temporary_Address_1__c;
            tempAddObj.address2 = userList[0].Temporary_Address_2__c;
            tempAddObj.city = userList[0].TRHC__Temporary_City__c;
            tempAddObj.state = userList[0].temporary_state__c;
            tempAddObj.zipcode = userList[0].TRHC__Temporary_Zip_Postal_Code__c;
            
            res.perInfo = perInfoObj;
            res.perAddress = perAddObj;
            res.tempAddress = tempAddObj;
            return res;   
        }

        res.success = 2;
        res.message = new string[]{'Unauthorized User.'};
        res.deviceId = deviceId;
        res.securityToken = apiToken;
        return res;
    }

    global class ResponseHandler {
        global String FullName;
        global integer success;
        global string[] message;
        global String User_Id;
        global string deviceType;
        global String deviceId;
        global string securityToken;
        global personalInformation perInfo;
        global permenentAddress perAddress;
        global temporaryAddress tempAddress;
        global ResponseHandler (){}
    }

    global class personalInformation {
        global String firstName;
        global String lastName;
        global String email;
        global String phone;
        global String desipline;
        global String primarySpeciality;
        global List<String> seconSpeci;
        global List<string> shiftPrefe;
        global personalInformation (){
        }
    }

    global class permenentAddress {
        global String address1;
        global String address2;
        global String city;
        global String state;
        global String zipcode;
        global permenentAddress (){
        }
    }

    global class temporaryAddress {
        global String address1;
        global String address2;
        global String city;
        global String state;
        global String zipcode;
        global temporaryAddress (){
        }
    }
}