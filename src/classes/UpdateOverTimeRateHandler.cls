public class UpdateOverTimeRateHandler { 
    
    public static void updateOvertimeRateAndPayRate (List<TRHC__Job__c> jobList, Map<Id,TRHC__Job__c> oldMap) {

        Set<Id> accountIds = new Set<Id>();
        set<Id> scratchSheetIdSet = new Set<Id>();
        for (TRHC__Job__c singleJob : jobList) {
            accountIds.add(singleJob.TRHC__Account_LookUp__c);
            scratchSheetIdSet.add(singleJob.TRHC__Scratch_Sheet__c);
            /*if(singleJob.TRHC__Regular_Bill_Rate__c == null) {
                singleJob.TRHC__Regular_Bill_Rate__c = 0.00;
            }*/
        }
        
        Map<Id, TRHC__Scratch_Sheet__c > scratchSheetIdMap = new Map<Id, TRHC__Scratch_Sheet__c >([Select Name, OT_Pay_Rate_OverRide__c from TRHC__Scratch_Sheet__c where id in : scratchSheetIdSet]);
        
        
        Map<Id,Account> mapAccount = new Map<Id,Account>([SELECT Id,Bill_Rate_Multiplier__c, Bill_Rate_Fixed_Dollar_Amount__c FROM Account WHERE Id IN: accountIds]);
        for (TRHC__job__c trhcJob: jobList) {
            if( scratchSheetIdMap.get(trhcJob.TRHC__Scratch_Sheet__c) != null)
                scratchSheetIdMap.get(trhcJob.TRHC__Scratch_Sheet__c).OT_Pay_Rate_OverRide__c = trhcJob.OT_Pay_Rate_OverRide__c;
           
            /*if(trhcJob.TRHC__Regular_Bill_Rate__c == null){
                trhcJob.TRHC__Regular_Bill_Rate__c =0.00;
            }*/
            
            //it should run only job had Account 
            if (trhcJob.TRHC__Account_LookUp__c != null) {
                if (Trigger.isInsert) {
                
                    
                    if (mapAccount.get(trhcJob.TRHC__Account_LookUp__c).Bill_Rate_Multiplier__c != null && 
                            mapAccount.get(trhcJob.TRHC__Account_LookUp__c).Bill_Rate_Fixed_Dollar_Amount__c == null && 
                            trhcJob.TRHC__Regular_Bill_Rate__c != null) {
                        
                        trhcJob.TRHC__Overtime_Bill_Rate__c = (mapAccount.get(trhcJob.TRHC__Account_LookUp__c).Bill_Rate_Multiplier__c) * (trhcJob.TRHC__Regular_Bill_Rate__c);
                        

                    } else {
                        if (mapAccount.get(trhcJob.TRHC__Account_LookUp__c).Bill_Rate_Multiplier__c == null && 
                                mapAccount.get(trhcJob.TRHC__Account_LookUp__c).Bill_Rate_Fixed_Dollar_Amount__c != null && 
                                trhcJob.TRHC__Regular_Bill_Rate__c != null)
                            
                            trhcJob.TRHC__Overtime_Bill_Rate__c = mapAccount.get(trhcJob.TRHC__Account_LookUp__c).Bill_Rate_Fixed_Dollar_Amount__c + trhcJob.TRHC__Regular_Bill_Rate__c;
                            
                        }
                        //Adding OT pay rate to 70% of OT bill rate 
                        /*if(trhcJob.TRHC__Overtime_Bill_Rate__c != null) {
                            trhcJob.TRHC__Overtime_Pay_Rate__c = trhcJob.TRHC__Overtime_Bill_Rate__c * 0.69;
                        }*/
                } else if (trigger.isUpdate) {
                    //TRHC__job__c trOldJob = trigger.oldMap.get(trhcJob.id);
                    //system.debug('===oldmap==='+trOldJob);
                    //check the account field in job got changed or not
                    //either 'Bill_Rate_Multiplier__c' or Bill_Rate_Fixed_Dollar_Amount__c only one should have value and in job Regular bill rate shold not null
                    if (mapAccount.get(trhcJob.TRHC__Account_LookUp__c).Bill_Rate_Multiplier__c != null && 
                            mapAccount.get(trhcJob.TRHC__Account_LookUp__c).Bill_Rate_Fixed_Dollar_Amount__c == null && 
                            trhcJob.TRHC__Regular_Bill_Rate__c != null) {
                        
                        trhcJob.TRHC__Overtime_Bill_Rate__c = (mapAccount.get(trhcJob.TRHC__Account_LookUp__c).Bill_Rate_Multiplier__c) * (trhcJob.TRHC__Regular_Bill_Rate__c);
                        
                    } else {
                        if (mapAccount.get(trhcJob.TRHC__Account_LookUp__c).Bill_Rate_Multiplier__c == null && 
                                mapAccount.get(trhcJob.TRHC__Account_LookUp__c).Bill_Rate_Fixed_Dollar_Amount__c != null && 
                                trhcJob.TRHC__Regular_Bill_Rate__c != null) {
                            
                            trhcJob.TRHC__Overtime_Bill_Rate__c = mapAccount.get(trhcJob.TRHC__Account_LookUp__c).Bill_Rate_Fixed_Dollar_Amount__c + trhcJob.TRHC__Regular_Bill_Rate__c;
                        }
                            
                    }
                    
                    //Adding OT pay rate to 0 if it has no value.
                    /*if(trhcJob.TRHC__Overtime_Pay_Rate__c == null){ 
                        trhcJob.TRHC__Overtime_Pay_Rate__c = 0.00;
                    }*/
                    //system.debug('===overtimebillrate==='+trhcJob.TRHC__Overtime_Bill_Rate__c);
                    //system.debug('===overtimePayrate==='+trhcJob.TRHC__Overtime_Pay_Rate__c);
                    //system.debug('===divideRate==='+trhcJob.TRHC__Overtime_Pay_Rate__c/trhcJob.TRHC__Overtime_Bill_Rate__c);
                    //Adding OT pay rate to 70% of OT bill rate
                    /*if(trhcJob.TRHC__Overtime_Bill_Rate__c != null && trhcJob.TRHC__Overtime_Bill_Rate__c != 0.00 && trhcJob.TRHC__Overtime_Pay_Rate__c/trhcJob.TRHC__Overtime_Bill_Rate__c >= 0.69){
                        trhcJob.TRHC__Overtime_Pay_Rate__c = trhcJob.TRHC__Overtime_Bill_Rate__c * 0.69;  
                        system.debug('===overtimePayrateAfter multiplie==='+trhcJob.TRHC__Overtime_Pay_Rate__c);     
                    }*/
                }
            }
        }
        
        update scratchSheetIdMap.values();
    }
}