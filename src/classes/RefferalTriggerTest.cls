@isTest
public class RefferalTriggerTest {

    @isTest
    public static void refferalTest(){
        test.startTest();
        TRHC__config_Settings__c configSettings = new  TRHC__config_Settings__c();
        configSettings.name ='Default';
        configSettings.Enable_RefferalTrigger__c = true;
        configSettings.disable_UpdateContactOwner__c = false;
        insert configSettings;
        Id recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Candidate').getRecordTypeId();
        System.debug('recordTypeId'+recordTypeId);
        Contact singleContact = new Contact();
        singleContact.RecordTypeId = recordTypeId;
        singleContact.LastName = 'Shah';
        insert singleContact;
        
        Contact secondContact = new Contact();
        secondContact.RecordTypeId = recordTypeId;
        secondContact.LastName = 'Test';
        secondContact.firstName = 'test1';
        secondContact.Email = 'atlas@great.com';
        //secondContact.TRHC__Referral_Source__c = 'Test';
        insert secondContact;
        
        singleContact.TRHC__Referral_Source__c = 'Test';
        update singleContact;
        
        singleContact.TRHC__Referral_Source__c = 'test1';
        update singleContact;
        
        singleContact.TRHC__Referral_Source__c = 'test1 Test';
        update singleContact;
        
        singleContact.TRHC__Referral_Source__c = 'atlas@great.com';
        update singleContact;
        Test.stopTest();
    }
}