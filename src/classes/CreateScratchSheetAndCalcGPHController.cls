public with sharing class CreateScratchSheetAndCalcGPHController {
        public Boolean isCreated {get;set;}
        public Boolean isError{get;set;}
        private static TRHC__Healthcare_Setting__c customSetting = TRHC__Healthcare_Setting__c.getAll().get('Default');
        public string queryString;
        public List<TRHC__Job__c> jobList{get;set;}
        public List<string> jobIdList = new  List<Id>();
        public List<TRHC__Expense__c> expancesList{get;set;}
        private string jobFieldList;
        public string candidateId;
        public TRHC__Scratch_Sheet__c scratchSheet{get;set;}
        public List<TRHC__Expense__c> listExpense{get;set;}
        public decimal payBurdern{get;set;}
        string defaultScratchTemplate;
        string allfields;
        string scrtchSheetRecordTypeId;
        TRHC__Scratch_Sheet__c newScratchSheet;
        public Boolean checkGSAService;
        public Boolean billPayRate {get;set;}
        public id scrid;
        public decimal billRate {get; set;}
        public CreateScratchSheetAndCalcGPHController(){
            // availableBillRate = false;
            checkGSAService = (Boolean)customSetting.get('TRHC__Enable_GSA_Service_to_Update_M_I_Rate__c');
            isCreated = false;
            isError = false;
            billRate = 0.00;
            string jobIds = ApexPages.currentPage().getParameters().get('jId');
            candidateId = ApexPages.currentPage().getParameters().get('cId');
            jobIdList = (jobIds != null) ? jobIds.split(',') : new  List<Id>();
            
            //get jobFields
            jobFieldList = ObjectFieldsHelper.getAllFields('TRHC__Job__c').get('TRHC__Job__c');
            
            //query for job
            string jobQuery = 'Select ' + jobFieldList + ' from TRHC__Job__c where ID IN : jobIdList';
            
            //String jobQuery ='Select id,trhc__state__c+' from TRHC__Job__c where ID IN'+jobIdList;
            scratchSheet = new TRHC__Scratch_Sheet__c();
            
            jobList = Database.query(jobQuery);
            if(jobList.get(0).TRHC__Regular_Bill_Rate__c != null){
                billRate = jobList.get(0).TRHC__Regular_Bill_Rate__c;    
            }
            if(jobList.get(0).TRHC__Regular_Bill_Rate__c > 60){
                billPayRate = false;
            } else {
                billPayRate = true;
            }   
        }

        public Void insertScratchsheet() {
            checkGSAService = (Boolean)customSetting.get('TRHC__Enable_GSA_Service_to_Update_M_I_Rate__c');
            //getting scrtach sheet recordtype
            scrtchSheetRecordTypeId = [SELECT
                                       Id
                                       FROM
                                       RecordType
                                       WHERE
                                       SobjectType = 'TRHC__Scratch_Sheet__c' AND Name = 'ScratchSheet'].Id;
            defaultScratchTemplate = (string)customSetting.get('TRHC__Default_Scratch_sheet_Template_Name__c');
            defaultScratchTemplate = '\'' + defaultScratchTemplate + '\'';
            
            //get All fields of an sobject 
            Map<string, string> scratchSheethelper = ObjectFieldsHelper.getAllFields('TRHC__Scratch_Sheet__c');
            allfields = scratchSheethelper.get('TRHC__Scratch_Sheet__c');
            queryString = 'Select ' + allfields + ' from TRHC__Scratch_Sheet__c ' + ' where name = ' + defaultScratchTemplate;
            if(defaultScratchTemplate == null || defaultScratchTemplate == '') {
                ApexPages.addMessage(new  ApexPages.message(ApexPages.Severity.ERROR, '<span style="font-family: Tahoma;">' + 'Scrtch sheet is not available' + '.</span>'));
                isError = true;
            }

            TRHC__Scratch_Sheet__c scratchSheet = Database.query(queryString);
            if(defaultScratchTemplate != null) {
                
                 string expnaceFields = ObjectFieldsHelper.getAllFields('TRHC__Expense__c').get('TRHC__Expense__c');
                //get all expanses of scratch sheet template query

                Id cid = string.valueOf(scratchSheet.id).subString(0, 15);
                string sid = '\'' + cid + '\'';
                string expancesQueryString = 'Select ' + expnaceFields + ' from TRHC__Expense__c ' + ' where TRHC__Scratch_Sheet__c =' + sid;
                List<TRHC__Expense__c> templateExpnaseList = Database.query(expancesQueryString);

                List<Job_to_Scratch_Sheet__c> jobToScratchSheet = [SELECT
                                                                       Job_Field_API_Name_Custom__c, 
                                                                       Scratch_Sheet_Field_API_Name__c
                                                                       FROM
                                                                       Job_to_Scratch_Sheet__c];

                for(TRHC__Job__c job: jobList) {
  
                    newScratchSheet = scratchSheet.clone(true, false);
                    newScratchSheet.id = null;
                    if(candidateId != null) {
                        newScratchSheet.TRHC__Candidate__c = candidateId;
                        //mapping fields
                    }
                    
                    for(Job_to_Scratch_Sheet__c jobToScratch: jobToScratchSheet) {
                        string jobField = jobToScratch.Job_Field_API_Name_Custom__c;
                        string scratchSheetField = jobToScratch.Scratch_Sheet_Field_API_Name__c;
                        if(jobFieldList.contains(jobField) && allfields.contains(scratchSheetField)) {
                            newScratchSheet.put(scratchSheetField, job.get(jobField));
                        }
                    }
                    newScratchSheet.TRHC__County__c = job.TRHC__County__c;
                    newScratchSheet.Name = job.TRHC__Job_Title__c + ' ScratchSheet';
                    newScratchSheet.RecordTypeId = scrtchSheetRecordTypeId;

                    /*if(checkGSAService == false) {
                        newScratchSheet.TRHC__GSA_M_I_Rate__c = job.GSA_M_I_Rate__c;
                        newScratchSheet.TRHC__GSA_Lodging__c = job.GSA_Lodging__c;
                        newScratchSheet.TRHC__Tax_Rate__c = job.Tax_Rate__c;
                    }*/
                    newScratchSheet.OT_Pay_rate_Override__c = true;
                    
                    Insert newScratchSheet;

                    expancesList = new  List<TRHC__Expense__c>();
                    for(TRHC__Expense__c expanse: templateExpnaseList) {
                        expanse.id = null;
                        expanse.TRHC__Scratch_Sheet__c = newScratchSheet.id;
                        if(expanse.TRHC__Expense_Name__c == 'Orientation Hours' || expanse.TRHC__Expense_Name__c == 'VMS Fees'){
                            expanse.TRHC__Grouping_Total__c = 'Pay Burden';
                        }
                        /*if(expanse.TRHC__Expense_Name__c == 'VMS Fees'){
                            expanse.TRHC__Grouping_Total__c = 'Pay Burden';
                        }*/
                        
                        expancesList.add(expanse);
                    }
                    Insert expancesList;
                    job.TRHC__Scratch_Sheet__c = newScratchSheet.id;
                    Update Job;
                    scrid = job.TRHC__Scratch_Sheet__c;
                }
            }

            TRHC__Config_Settings__c configSettings = TRHC__Config_Settings__c.getValues('Default');
            Boolean isEnabled = configSettings == null ? True : (Boolean) configSettings.Trigger_Enable_ScratchSheetAutoAddExp__c;
            List<String> exptIdsList = configSettings == null ? new List<Id>() :configSettings.ExceptConfigIds__c.split(';');
            List<String> exptAccountList = configSettings == null ? new List<String>() :configSettings.Except_Hospitals__c.split(';');
            String configFields = ObjectFieldsHelper.getAllFields('TRHC__Config__c').get('TRHC__Config__c');
            
            String expanseFields = ObjectFieldsHelper.getAllFields('TRHC__Expense__c').get('TRHC__Expense__c');
            
            Set<String> states = new Set<String>();
            
            Set<String> scratchSheetIds = new Set<String>();
            
            /*
             * @description : modified by medstaff2 added Account_Name__c to avoid exception in line number 259
             */ 

            List<TRHC__Scratch_Sheet__c> scrList = [select id,name,TRHC__Other_Taxable_Expense__c,State_pic__c,City__c, Account_Name__c From TRHC__Scratch_Sheet__c where id =: newScratchSheet.id ];
            for(TRHC__Scratch_Sheet__c scshet : scrList ) {
                if(scshet.State_pic__c!= null) {
                    states.add('\''+scshet.State_pic__c+'\'');
                }
                scratchSheetIds.add(scshet.Id);
            }
            
            if(states.size() == 0){
                return;
            }
            Map<String, Set<TRHC__Config__c>> configMap = new Map<String, Set<TRHC__Config__c>>();
            
            List<String> statesList = new List<String> (states);
            String query = 'SELECT ' + configFields+ ' FROM TRHC__Config__c WHERE TRHC__State__c IN ' + statesList ;
            
            String configQuery = 'SELECT ID, TRHC__Scratch_Sheet__c,TRHC__Active__c,Total_Amount_Custom__c, TRHC__Expense_Name__c, TRHC__Config__c FROM TRHC__Expense__c WHERE TRHC__Scratch_Sheet__c = \''+ newScratchSheet.id +'\'';
            
            if(configSettings != null) {
                String criteria = configSettings.AutoAddExpenseCriteria__c;
                if(criteria != null) {
                    List<String> criterias = criteria.split(',');
                    for(String rule : criterias){
                        query += ' AND ' + rule +' ';
                    }
                }
            }
            
            List<TRHC__Config__c> configs = Database.query(query);
            
            List<TRHC__Expense__c> availableExpenses = Database.query(configQuery);
            if(configs != null && configs.size() > 0) {
                List<Config_To_Expense__c> configToExpense = [Select Config_Field_API_Name_Custom__c, Expanse_Field_API_Name__c From Config_To_Expense__c]; 
                for(TRHC__Config__c config : configs) {
                    Set<TRHC__Config__c> configObjs = configMap.get(config.TRHC__State__c);
                    if(configObjs == null) {
                        configObjs = new Set<TRHC__Config__c>();
                    }
                    configObjs.add(config);
                    configMap.put(config.TRHC__State__c, configObjs);
                }
                
                //Decimal otherExpense = 0.00;
                Set<TRHC__Expense__c> expenses = new Set<TRHC__Expense__c>();
                for(TRHC__Scratch_Sheet__c scobj : scrList) {
                    
                    if(scobj.State_pic__c== null) { 
                        continue;
                    }

                    /*
                    http://gsa.targetrecruit.com/Geocode/getGsaDetails_new.php
                    */
                    Set<TRHC__Config__c> configIds = configMap.get(scobj.State_pic__c);
                    List<String> nameLst = new List<String> ();
                    List<String> scrExpList = new List<String>();
                    for(TRHC__Config__c configId : configIds) {
                        boolean isExist = false;
                        boolean addCityExpense = false;
                        
                        if(exptIdsList.contains(configId.id) && scobj.City__c != configId.TRHC__City__c){
                            isExist = true;
                        }
                        else if(exptIdsList.contains(configId.id) ){
                            nameLst.add(configId.TRHC__Name__c);
                            addCityExpense = true;
                        }
                        
                        for(TRHC__Expense__c avaExp : availableExpenses) {
                            if(avaExp.TRHC__Scratch_Sheet__c == scobj.Id && avaExp.TRHC__Config__c == configId.id) {
                                isExist = true;
                            }
                        }

                        if(isExist) {
                            continue;
                        }

                        TRHC__Expense__c expense = new TRHC__Expense__c();

                        for(Config_To_Expense__c configToExp : configToExpense){
                            String expanseField = configToExp.Expanse_Field_API_Name__c;
                            String configField = configToExp.Config_Field_API_Name_Custom__c;
                            if(configFields.contains(configField)&& expanseFields.contains(expanseField)){
                                if(configField == 'TRHC__Value__c'){
                                    expense.TRHC__Value__c = Decimal.valueOf(configId.TRHC__Value__c);
                                }else{
                                    expense.put(expanseField,configId.get(configField));
                                }
                            }
                        }
                        expense.TRHC__Config__c = configId.id;
                        expense.TRHC__Scratch_Sheet__c  = scobj.Id;
                        //expense.TRHC__Grouping_Total__c = 'Pay Burden';
                        expenses.add(expense);
                        scrExpList.add(expense.TRHC__Expense_Name__c);
                    }
                    
                    set<String> setTempString = new set<String>();
                    set<String> duplicateTempString = new set<String>();
                    for(String s:scrExpList){
                        if(!setTempString.add(s)){
                            duplicateTempString .add(s);
                        }
                    }

                    for(TRHC__Expense__c expence : expenses){
                        /*if(expence.TRHC__Grouping_Total__c == 'Pay Burden'){
                            otherExpense = expence.TRHC__Amount__c;
                        }*/
                        for(String str:duplicateTempString){
                            if(str == expence.TRHC__Expense_Name__c){
                                if(exptAccountList.contains(scobj.Account_Name__c)){
                                    if(exptIdsList.contains(expence.TRHC__Config__c)){
                                        expenses.remove(expence);  
                                    }
                                    else{
                                        expence.TRHC__Value__c = 0.0;
                                    }
                                }
                                else if(!exptIdsList.contains(expence.TRHC__Config__c)){
                                    expenses.remove(expence); 
                                }
                            }
                        }
                    }
                    
                    for(TRHC__Expense__c exp :expenses){
                        if(exp.TRHC__Expense_Name__c != null && exp.TRHC__Expense_Name__c.contains('Sales Tax') && exptAccountList.contains(scobj.Account_Name__c)){
                            exp.TRHC__Value__c = 0.0;
                        }
                    }

                    If(expenses.size()>0) {
                        insert new List<TRHC__Expense__c>(expenses);
                    }
                }
            }



            if(checkGSAService == false){
                update newScratchSheet;
            }

            /*List<TRHC__Expense__c> expense = [select TRHC__Expense_Name__c,TRHC__Scratch_Sheet__r.OverTIme_GPH__c,TRHC__Scratch_Sheet__r.Over_Time_Include_Bill__c,TRHC__Scratch_Sheet__r.TRHC__Client_Fee__c,TRHC__Occurence__c, TRHC__Value__c,TRHC__ProRated_Hours__c from TRHC__Expense__c where TRHC__Scratch_Sheet__c =: scrid and TRHC__Expense_Name__c = 'VMS Fees'];
            if(expense.size() > 0){
                ////System.debug('==OT bill rate==>'+expense[0].TRHC__Scratch_Sheet__r.Over_Time_Include_Bill__c);
                ////System.debug('===occurrence=>'+expense[0].TRHC__Occurence__c);
                ////System.debug('===prorated=>'+expense[0].TRHC__ProRated_Hours__c);
                ////System.debug('===OverTIme_GPH__c=>'+expense[0].TRHC__Scratch_Sheet__r.OverTIme_GPH__c);
                if(expense[0].TRHC__Scratch_Sheet__r.TRHC__Client_Fee__c != null) {
                    expense[0].TRHC__Value__c = expense[0].TRHC__Scratch_Sheet__r.TRHC__Client_Fee__c;
                    //expense[0].TRHC__Grouping_Total__c = 'Pay Burden';
                    //(expense[0].TRHC__Scratch_Sheet__r.Over_Time_Include_Bill__c * expense[0].TRHC__Scratch_Sheet__r.TRHC__Client_Fee__c)/(100 * expense[0].TRHC__Occurence__c * expense[0].TRHC__ProRated_Hours__c);
                    Update expense;
                }
            }*/

           /* List<TRHC__Expense__c> expenseLisr = [select TRHC__Expense_Name__c,TRHC__Scratch_Sheet__r.OverTIme_GPH__c,TRHC__Scratch_Sheet__r.Over_Time_Include_Bill__c,TRHC__Scratch_Sheet__r.TRHC__Client_Fee__c,TRHC__Occurence__c, TRHC__Value__c,TRHC__ProRated_Hours__c from TRHC__Expense__c where TRHC__Scratch_Sheet__c =: scrid and TRHC__Expense_Name__c = 'Orientation Hours'];
            if(expenseLisr.size() > 0){
               
                if(expenseLisr[0].TRHC__Scratch_Sheet__r.TRHC__Client_Fee__c != null) {
                    //expense[0].TRHC__Value__c = expense[0].TRHC__Scratch_Sheet__r.TRHC__Client_Fee__c;
                    //expenseLisr[0].TRHC__Grouping_Total__c = 'Pay Burden';
                    expenseLisr[0].TRHC__Value__c  = (expenseLisr[0].TRHC__Scratch_Sheet__r.Over_Time_Include_Bill__c * expenseLisr[0].TRHC__Scratch_Sheet__r.TRHC__Client_Fee__c)/(100 * expenseLisr[0].TRHC__Occurence__c * expenseLisr[0].TRHC__ProRated_Hours__c);
                    Update expenseLisr;
                }
            }*/

            
        } 

        public Pagereference calcGPH(){   
            scratchSheet = new TRHC__Scratch_Sheet__c();

            scratchSheet = [SELECT
                                    Id,
                                    OverTime_GPH__c,
                                    isGPHCalculated__c,
                                    With_OverTime_Gross_Profit__c,
                                    TRHC__Assignment_length__c, 
                                    TRHC__Hours_Per_Week__c, 
                                    TRHC__Start_Date__c,
                                    TRHC__End_Date__c,
                                    TRHC__Client_Fee__c,
                                    TRHC__Pay_Rate__c,
                                    TRHC__M_I_Weekly__c,
                                    Non_Taxable_Expenses__c,
                                    State_pic__c,
                                    TRHC__OT_Bill_After__c,
                                    OtherExpense__c,
                                    over_time__c,
                                    TRHC__Weekly_Lodging__c,
                                    TRHC__Bill_Rate__c,
                                    TRHC__ProRate_Hours__c,
                                    Over_Time_Include_Bill__c
                                    FROM
                                    TRHC__Scratch_Sheet__c
                                    WHERE
                                    Id = : newScratchSheet.Id];

            string getLabel = System.Label.Editable_Expense;
            Set<string> editAccess = new  Set<string>();
            
            if(getLabel != null) {
                editAccess.addAll(getLabel.Split(','));
            }

            listExpense = [SELECT
                           Id, 
                           TRHC__Expense_Name__c, 
                           TRHC__Amount__c, 
                           Total_Amount_Custom__c,
                           TRHC__Occurence__c, 
                           TRHC__Grouping_Total__c, 
                           TRHC__Value__c
                           FROM
                           TRHC__Expense__c
                           WHERE
                           TRHC__Scratch_Sheet__c =: scratchSheet.Id AND TRHC__Expense_Name__c in: editAccess];

            payBurdern = 0;
            for(TRHC__Expense__c expense: listExpense) {
                if(expense.TRHC__Expense_Name__c != 'Travel Reimbursement') {
                    payBurdern += expense.TRHC__Amount__c;
                    
                }
            }

            payBurdern = payBurdern.setscale(2);
            
            if(!scratchSheet.isGPHCalculated__c) {   
                checkGSAService = (Boolean)customSetting.get('TRHC__Enable_GSA_Service_to_Update_M_I_Rate__c');
                decimal dollar = 0.00;
                if(scratchSheet.TRHC__Bill_Rate__c > 80){
                    dollar = 18.00;
                }else if(scratchSheet.TRHC__Bill_Rate__c >= 60 && scratchSheet.TRHC__Bill_Rate__c <= 80){
                    dollar = 17.00;
                } else if(scratchSheet.TRHC__Bill_Rate__c < 60){
                    dollar = 15.00;
                }
                System.debug('===dollar==='+dollar);
                List<TRHC__Expense__c> listUpdate = new  List<TRHC__Expense__c>();
                
                decimal totalGPHAdd = 0;
                decimal addToNonTaxable = 0;
                if(scratchSheet.over_time__c == null){
                    scratchSheet.over_time__c = 0;
                }

                decimal calculation = dollar * (scratchSheet.TRHC__Assignment_length__c * (scratchSheet.TRHC__Hours_Per_Week__c + scratchSheet.over_time__c )); //
                if(dollar > scratchSheet.OverTime_GPH__c) {
                    totalGPHAdd = (calculation - scratchSheet.With_OverTime_Gross_Profit__c);
                    totalGPHAdd = totalGPHAdd.setscale(2) * -1;
                        calculateGPH(listExpense, scratchSheet, totalGPHAdd, payBurdern);
                } else if(dollar < scratchSheet.OverTime_GPH__c){
                    totalGPHAdd = (scratchSheet.With_OverTime_Gross_Profit__c - calculation);
                    totalGPHAdd = totalGPHAdd.setscale(2);
                    calculateGPH(listExpense, scratchSheet, totalGPHAdd, payBurdern);       
                }
            }    

           /* List<TRHC__Expense__c> allExpenseList = [select TRHC__Expense_Name__c,
                                                            TRHC__Amount__c,
                                                            TRHC__Grouping_Total__c,
                                                            TRHC__Scratch_Sheet__r.OverTIme_GPH__c,
                                                            TRHC__Scratch_Sheet__r.Over_Time_Include_Bill__c,
                                                            TRHC__Scratch_Sheet__r.TRHC__Client_Fee__c,
                                                            TRHC__Occurence__c, 
                                                            TRHC__Value__c,
                                                            TRHC__ProRated_Hours__c,
                                                            TRHC__Scratch_Sheet__r.OtherExpense__c
                                                            from TRHC__Expense__c 
                                                            where TRHC__Scratch_Sheet__c =:  newScratchSheet.Id];*/
            /*Double otherExpense = 0.00;
            for(TRHC__Expense__c expObj : allExpenseList){
                if(expObj.TRHC__Grouping_Total__c == 'Pay Burden'){
                    //System.debug('expObj----------name---------'+expObj.TRHC__Expense_Name__c);
                    otherExpense = otherExpense + expObj.TRHC__Amount__c;
                }
                //System.debug('========== otherExpense in for loop ==========='+otherExpense);
            }
            //System.debug('========== otherExpense ==========='+otherExpense);

            if(scratchSheet != null){
                //System.debug('========== scratchSheet.OtherExpense__c ========'+scratchSheet.OtherExpense__c);
                if(otherExpense != null){
                    scratchSheet.OtherExpense__c = otherExpense;
                    update scratchSheet;
                }
                //System.debug('========== scratchSheet.OtherExpense__c ========'+scratchSheet.OtherExpense__c);
            }*/

            return new PageReference('/'+scratchSheet.Id); 
            //return candidateId != null ? new  Pagereference('/' + scratchSheet.id + '/e?retURL=%2F' + scratchSheet.id) : new  Pagereference('/' + scratchSheet.id);
        }

    public void calculateGPH(List<TRHC__Expense__c> listExpenses, TRHC__Scratch_Sheet__c sheet, decimal totalGPH, decimal totalExpense) {
        List<TRHC__Expense__c> listUpdate = new  List<TRHC__Expense__c>();
        Double housingDifferece = 0.00;
        Double perDiemDifference = 0.00;
        for(TRHC__Expense__c singleExpense: listExpenses) {
            if(singleExpense.TRHC__Expense_Name__c != 'Travel Reimbursement') {   
                decimal expenseValue = (singleExpense.TRHC__Amount__c + (((singleExpense.TRHC__Amount__c / totalExpense) * 100) * totalGPH) / 100) / singleExpense.TRHC__Occurence__c;
                    
                //decimal expenseValue = (singleExpense.Total_Amount_Custom__c  + (((singleExpense.Total_Amount_Custom__c/ totalExpense) * 100) * totalGPH) / 100) / singleExpense.TRHC__Occurence__c;                
                //decimal expenseValue = 0.00; 
                /*if( singleExpense.TRHC__Expense_Name__c == 'Housing Stipend' || singleExpense.TRHC__Expense_Name__c == 'Per diem')
                    expenseValue =  singleExpense.TRHC__Value__c + (totalGPH / (singleExpense.TRHC__Occurence__c * 2)); */              
                if(singleExpense.TRHC__Expense_Name__c == 'Housing Stipend' && expenseValue > sheet.TRHC__Weekly_Lodging__c){
                    singleExpense.TRHC__Value__c = sheet.TRHC__Weekly_Lodging__c;//Math.round(expenseValue);
                    housingDifferece = expenseValue - sheet.TRHC__Weekly_Lodging__c;
                }else if(singleExpense.TRHC__Expense_Name__c == 'Per diem' && expenseValue > sheet.TRHC__M_I_Weekly__c){
                    singleExpense.TRHC__Value__c = sheet.TRHC__M_I_Weekly__c;
                    perDiemDifference = expenseValue - sheet.TRHC__M_I_Weekly__c;
                    
                }else{
                    singleExpense.TRHC__Value__c = expenseValue.setscale(2);
                }
                
                listUpdate.add(singleExpense);
            }
        }



        if(!listUpdate.isEmpty()) {
            Update listUpdate;
            sheet.isGPHCalculated__c = true;
            //nonTaxableExpe = 0.00;
            /*for(TRHC__Expense__c exp : [select id,Total_Amount_Custom__c,TRHC__Expense_Name__c,TRHC__Grouping_Total__c From TRHC__Expense__c where TRHC__Scratch_Sheet__c =: sheet.id ]){
                if(exp.TRHC__Grouping_Total__c == 'Non Taxable Expense'){
                    if(exp.Total_Amount_Custom__c != null)
                        nonTaxableExpe += exp.Total_Amount_Custom__c;
                }
            }*/
            if(sheet.TRHC__Pay_Rate__c != null && sheet.TRHC__Hours_Per_Week__c != null)
                sheet.TRHC__Pay_Rate__c = sheet.TRHC__Pay_Rate__c + (housingDifferece + perDiemDifference)/sheet.TRHC__Hours_Per_Week__c;
            
            //sheet.Non_Taxable_Expenses__c = nonTaxableExpe;
            Update sheet; 
        }
    } 

    public void insertGsaSerValue() {
        String gsaURL = 'http://gsa.targetrecruit.com/Geocode/getGsaDetails_new.php';
        List<TRHC__Scratch_Sheet__c> scrList = [SELECT id, Name, TRHC__Start_Date__c, End_Date_New__c, State_Pic__c, TRHC__GSA_Lodging__c,TRHC__County__c,TRHC__GSA_M_I_Rate__c, TRHC__Tax_Rate__c From TRHC__Scratch_Sheet__c where TRHC__Start_Date__c !=  null and End_Date_New__c != null and id =: scrid];
        List<TRHC__Scratch_Sheet__c> scrUpdateList = new List<TRHC__Scratch_Sheet__c>();
        for(TRHC__Scratch_Sheet__c scrObj : scrList) {
            if (scrObj.State_Pic__c != null){ 

                String startDateStr = scrObj.TRHC__Start_Date__c != null ? 
                scrObj.TRHC__Start_Date__c.year() + '-'+
                scrObj.TRHC__Start_Date__c.month() + '-'+
                scrObj.TRHC__Start_Date__c.day() : '' ;
                String endDate = scrObj.End_Date_New__c != null ?
                scrObj.End_Date_New__c.year() + '-' +
                scrObj.End_Date_New__c.month() + '-' +
                scrObj.End_Date_New__c.day() : ''; 
                String state = '';                  
                state = scrObj.State_pic__c;
                state = state.replace(' ','%20');
                String county = scrObj.TRHC__County__c != null ? scrObj.TRHC__County__c.replace(' ','%20') :'';

                HttpRequest request = new HttpRequest();
                String GSARquestURL = gsaURL +'?sdate=' + startDateStr +'&edate='+endDate+'&state='+state+'&county='+county;
                request.setEndpoint(GSARquestURL);
                System.debug('========= GSARquestURL ==========='+GSARquestURL);
                request.setMethod('GET');
                request.setCompressed(false);
                request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
                System.debug(LoggingLevel.ERROR, 'gsa request>>>' + request);
                HttpResponse response = new HttpResponse();

                if(!Test.isRunningTest() && Limits.getCallouts() < Limits.getLimitCallouts()){
                    response = new Http().send(request);
                }else{
                    response.setBody('[{"GSADestination":"Gulf Shores","State":"AL","GSACounty":"Baldwin","BeginDate":"2010-08-01","EndDate":"2010-09-30","Lodging":"111","MIE":null,"Total":"162","CombinedTaxRate":"8.51"}]');
                    response.setStatusCode(200);
                }

                System.debug(LoggingLevel.ERROR, ':::::::::GSA response = ' + response);
                System.debug(LoggingLevel.ERROR, ':::::::::response.getBody() = ' + response.getBody());

                if (response.getStatusCode() == 200) {
                    String body = response.getBody();
                    String jsonBody = body.substring(1,body.length()-1);
                    System.debug(LoggingLevel.ERROR, ':::::::::body' + jsonBody);
                    CreateScratchSheetAndCalcGPHController.GSAResponse gsaResponse  = parseResponse(jsonBody);
                    System.debug(LoggingLevel.ERROR, ':::::::::gsaResponse after parsing = ' + gsaResponse);
                    scrObj.TRHC__GSA_M_I_Rate__c = gsaResponse.MIE != null && gsaResponse.MIE != '' ? Decimal.Valueof(gsaResponse.MIE):0;
                    scrObj.TRHC__GSA_Lodging__c = gsaResponse.Lodging != null && gsaResponse.Lodging != '' ? Decimal.Valueof(gsaResponse.Lodging):0;
                    scrObj.TRHC__Tax_Rate__c = gsaResponse.CombinedTaxRate != null && gsaResponse.CombinedTaxRate != '' ? Decimal.ValueOf(gsaResponse.CombinedTaxRate):0;
                    scrUpdateList.add(scrObj);
                }
            }
        }
        if(!scrUpdateList.isEmpty()){
            update scrUpdateList;
        }
    }

    public static CreateScratchSheetAndCalcGPHController.GSAResponse parseResponse(String response) {

        CreateScratchSheetAndCalcGPHController.GSAResponse gsaResponse = new CreateScratchSheetAndCalcGPHController.GSAResponse();

        if (String.isNotBlank(response)) {
            UpdateGSAServiceBatch.GSAResponse respObj = 
            (UpdateGSAServiceBatch.GSAResponse)JSON.deserialize(
            response, UpdateGSAServiceBatch.GSAResponse.class);
            System.debug('respObj after deserialize'+respObj);
            return setGSADetails(gsaResponse, 
            respObj.MIE, respObj.Lodging,respObj.CombinedTaxRate);
        }
        return setGSADetails(gsaResponse, null, null,null);
    }

    public static CreateScratchSheetAndCalcGPHController.GSAResponse setGSADetails(
    CreateScratchSheetAndCalcGPHController.GSAResponse gsaResponse, String MIE, String Lodging,String taxRate) {
        gsaResponse.MIE = MIE;
        gsaResponse.Lodging = Lodging;
        gsaResponse.CombinedTaxRate = taxRate;
        System.debug(LoggingLevel.ERROR, ':::::::::gsaResponse  = ' + gsaResponse);
        return gsaResponse;
    }   

    public class GSAResponse{
        public String GSADestination {get;set;}
        public String State {get;set;}
        public String GSACounty {get;set;}
        public String BeginDate {get;set;}
        public String EndDate {get;set;}
        public String Lodging {get;set;}
        public String MIE {get;set;}
        public String Total {get;set;}
        public String CombinedTaxRate {get;set;}
    }
}