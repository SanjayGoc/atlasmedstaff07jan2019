/**
* @description Helper class for testing.Implements method for creating test data for objects. 
*/
@isTest
public with sharing class TestDataGenerator {
    public Account candidatePool;
    public List<Contact> candidates;

    public List<Account> hiringCompanies;
    public List<Contact> hiringManagers;

    public List<TRHC__Job__c> jobs;
    public List<TRHC__Job_Applicant__c> jobApplicants;

    public List<TRHC__Interview__c> interviews;
    public List<TRHC__Placement__c> placements;

    public List<TRHC__Survey_Question__c> surveyQuestions;

    public List<Attachment> attachments;

    // For backward compatibility
    public Account account;
    
    public TRHC__Config_Settings__c configsettings;
    
/**
* @name minDB
* @return void
*/
    public void minDB() {
        //ConfigurationSetup.addInitialConfigSettings();
        //ConfigurationSetup.addInitialCustomSettings();
        
        configsettings = new TRHC__Config_Settings__c();
        configsettings.Name = 'Default';
        configsettings.TRHC__Trigger_Disabled_Trigger_JobApplicants__c = true;
        insert configsettings;
        
        candidatePool = new Account(Name = 'Candidate Pool');
        insert candidatePool;

        candidates = new List<Contact>();
        for (Integer i = 0; i < 3; i++) {
            candidates.add(new Contact(
                    AccountId = candidatePool.Id,
                    LastName = 'Candidate ' + i,
                    Email = 'email' + i + '@mail.com',
                    RecordTypeId = TRHC.RecordTypeUtilNew.getCandidateRecordTypeIdList()[0]));
        }
        insert candidates;

        hiringCompanies = new List<Account>();
        for (Integer i = 0; i < 3; i++) {
            hiringCompanies.add(new Account(Name = 'Hiring Company ' + i));
        }
        insert hiringCompanies;

        hiringManagers = new List<Contact>();
        for (Account hiringCompany : hiringCompanies) {
            for (Integer i = 0; i < 3; i++) {
                hiringManagers.add(new Contact(
                        AccountId = hiringCompany.Id,
                        LastName = 'Hiring Manager ' + i));
            }
        }
        insert hiringManagers;

        jobs = new List<TRHC__Job__c>();
        for (Contact hiringManager : hiringManagers) {
            jobs.add(new TRHC__Job__c(
                    TRHC__Account_Lookup__c = hiringManager.AccountId,
                    TRHC__Hiring_Manager__c = hiringManager.Id,
                    TRHC__Recruiter__c = UserInfo.getUserId()));
        }
        insert jobs;

        jobApplicants = new List<TRHC__Job_Applicant__c>();
        for (TRHC__Job__c job : jobs) {
            for (Contact candidate : candidates) {
                jobApplicants.add(new TRHC__Job_Applicant__c(
                        TRHC__Job__c = job.Id,
                        TRHC__Contact_Candidate__c = candidate.Id));
            }
        }
        insert jobApplicants;

        interviews = new List<TRHC__Interview__c>();
        for (TRHC__Job_Applicant__c jobApplicant : jobApplicants) {
            interviews.add(new TRHC__Interview__c(
                    TRHC__Job_Applicant__c = jobApplicant.Id));
        }
        insert interviews;

        placements = new List<TRHC__Placement__c>();
        for (TRHC__Job_Applicant__c jobApplicant : jobApplicants) {
            for (Contact candidate : candidates) {
            placements.add(new TRHC__Placement__c(
                    TRHC__Contact_Candidate__c= candidate.Id,
                    TRHC__Job_Applicant__c = jobApplicant.Id,
                    TRHC__Start_Date__c = Date.Today()));
                    }
        }
        insert placements;
    }

/**
* @name createAccount
* @return void
*/
    public void createAccount() {
        if (account == null) {
            account = new Account(Name = 'Test Account');
            insert account;
        }
    }

    /**
* @name createCandidates
* @return void
*/
    public void createCandidates() {
        if (candidates == null) {
            //ConfigurationSetup.setupConfigSetting('Last_Candidate_Id__c', 0);
            //ConfigHelper.setConfigSetting('Do_not_heck_Candidates_for_Duplicates__c', true);

            createAccount();

            candidates = new List<Contact>();
            createCandidate(0);
            createCandidate(1);
            insert candidates;
        }
    }

    private void createCandidate(Integer candidateNumber) {
        candidates.add(new Contact(
                LastName = 'Test' + candidateNumber,
                FirstName = 'Test' + candidateNumber,
                Email = 'qwe@qwe.qwe',
                Phone = '111111',
                TRHC__Source__c = 'Blog',
                AccountId = account.Id,
                RecordTypeId = TRHC.RecordTypeUtilNew.getCandidateRecordTypeIdList()[0]));
    }

/**
* @name createJobs
* @param jobsCount Integer 
* @return void
*/
    public void createJobs(Integer jobsCount) {
        if (jobs == null) {
            //ConfigurationSetup.addInitialCustomSettings();
            TRHC__Config__c defaultFilterFields = new TRHC__Config__c(
                    TRHC__Name__c = 'Default',
                    TRHC__Value__c = 'TRHC__Account_Name__c;TRHC__Availability_To_Interview__c;TRHC__Candidate_Status__c;');
            insert defaultFilterFields;

            createAccount();

            jobs = new List<TRHC__Job__c>();
            for (Integer i = 0; i < jobsCount; i++) {
                createJob(i);
            }
            insert jobs;
        }
    }

    private void createJob(Integer jobNumber) {
        jobs.add(new TRHC__Job__c(
                TRHC__Job_Title__c = 'Test Job ' + jobNumber,
                TRHC__Account_Lookup__c = account.Id,
                TRHC__Recruiter__c = UserInfo.getUserId(),
                TRHC__Start_Date__c = Date.today()));
    }

/**
* @name createAttachments
* @param p_parentId Id 
* @param p_count Integer 
* @return void
*/
    public void createAttachments(Id p_parentId, Integer p_count) {
        if (attachments == null) {
            attachments = new List<Attachment>();

            for (Integer i = 0; i < p_count; i++) {
                createAttachment(p_parentId, i+1);
            }

            upsert attachments;
        }
    }

    private void createAttachment(Id p_parentId, Integer p_number) {
        if (attachments != null) {
            attachments.add(new Attachment(
                Name = 'Test Attachment' + p_number,
                Body = Blob.valueOf('Test Attachment Body' + p_number),
                ParentId = p_parentId
            ));
        }
    }

/**
* @name createJobApplicants
* @return void
*/
    public void createJobApplicants() {
        if (jobApplicants == null) {
            createCandidates();
            createJobs(2);

            jobApplicants = new List<TRHC__Job_Applicant__c>();
            createJobApplicant(jobs[0], candidates[0]);
            createJobApplicant(jobs[1], candidates[0]);
            insert jobApplicants;
        }
    }

    private void createJobApplicant(TRHC__Job__c job, Contact candidate) {
        jobApplicants.add(new TRHC__Job_Applicant__c(
            TRHC__Contact_Candidate__c = candidate.Id,
            TRHC__Job__c = job.Id));
    }

/**
* @name createInterviews
* @return void
*/
    public void createInterviews() {
        if (interviews == null) {
            createJobApplicants();

            interviews = new List<TRHC__Interview__c>();
            createInterview(jobs[0], jobApplicants[0], candidates[0]);
            createInterview(jobs[1], jobApplicants[1], candidates[0]);
            insert interviews;
        }
    }

    private void createInterview(
            TRHC__Job__c job,
            TRHC__Job_Applicant__c jobApplicant,
            Contact candidate) {

        interviews.add(new TRHC__Interview__c(
                TRHC__Type_Of_Interview__c = 'Online',
                TRHC__Contact_Candidate__c = candidate.Id,
                TRHC__Start_Date__c = DateTime.now(),
                TRHC__Job_Num__c = job.Id,
                TRHC__Job_Applicant__c = jobApplicant.Id));
    }

/**
* @name createPlacements
* @return void
*/
    public void createPlacements() {
        if (placements == null) {
            createJobApplicants();

            placements = new List<TRHC__Placement__c>();
            createPlacement(jobs[0], jobApplicants[0], candidates[0]);
            createPlacement(jobs[1], jobApplicants[1], candidates[0]);
            insert placements;
        }
    }

    private void createPlacement(
            TRHC__Job__c job,
            TRHC__Job_Applicant__c jobApplicant,
            Contact candidate) {

        placements.add(new TRHC__Placement__c(
                TRHC__Contact_Candidate__c = candidate.Id,
                TRHC__Start_Date__c = Date.today(),
                TRHC__Job__c = job.Id,
                TRHC__Job_Title__c = job.TRHC__Job_Title__c,
                TRHC__Job_Applicant__c = jobApplicant.Id));
    }

/**
* @name createSurveyQuestions
* @return void
*/
    public void createSurveyQuestions() {
        if (surveyQuestions == null) {
            createJobs(1);

            surveyQuestions = new List<TRHC__Survey_Question__c>();
            createSurveyQuestion(jobs[0], 0, System.Label.TRHC.Multi_Select_Vertical, 'one\ntwo\nthree\n');
            createSurveyQuestion(jobs[0], 1, System.Label.TRHC.Single_Select_Vertical, 'one\ntwo\nthree\n');
            createSurveyQuestion(jobs[0], 2, System.Label.TRHC.Free_Text, 'one\ntwo\nthree\n');
            createSurveyQuestion(jobs[0], 3, System.Label.TRHC.Single_Select_Horizontal, 'one\ntwo\nthree\n');
            insert surveyQuestions;
        }
    }

    private void createSurveyQuestion(TRHC__Job__c job, Integer questionNumber,
            String questionType, String questionChoices) {

        surveyQuestions.add(new TRHC__Survey_Question__c(
                Name = 'Testing Question ' + questionNumber,
                TRHC__Job__c = job.Id,
                TRHC__Type__c = questionType,
                TRHC__Choices__c = questionChoices,
                //TRHC__Question__c = 'Testing Question question ' + questionNumber,
                TRHC__OrderNumber__c = questionNumber,
                TRHC__Required__c = true));
    }

    private testmethod static void test() {
        TestDataGenerator gen = new TestDataGenerator();

        gen.minDB();

        gen.createAccount();
        gen.createJobApplicants();
        gen.createInterviews();
        gen.createSurveyQuestions();
    }
}