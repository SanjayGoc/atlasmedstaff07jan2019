public with sharing class PurchaseRequestFormController{
    Public String description {get; set;}
    Public String purchaseItems { get;set; }
    Public String PurchaseLocation { get;set; }
    Public String delvDateTime { get;set; }
    Public String shippingAddress { get;set; }
    Public String message { get;set;} 
    Public String flight { get;set; }
    Public String departing { get;set; }
    public String arrival { get;set; }
    Public String returnDate { get;set; }
    Public String hotel { get;set; }
    public String birthday { get;set; }
    Public String location { get;set; }
    public String requestType { get;set; }
    public List<SelectOption> typeOfRequests { get;set; }
    public List<SelectOption> genders { get;set; }
    public List<SelectOption> appUsers { get;set; }
    Public string selectedUser { get;set; }
    @TestVisible private List <User> appUser;
    public String departureDate { get;set; }
    public String dateHotel { get;set; }
    public String dateofBirth { get;set; }
    public string fullName { get;set; }
    public string travelDocument { get;set; }
    Public String assignmentDate { get;set; }
    public string hospitalName { get;set; }
    public string hospitalAddress { get;set; }
    public String dueDate { get;set; }
    public String contactName { get;set; }
    public String contactEmail { get;set; }
    public String contactPhone { get;set; }
    public String contactAddress { get;set; }
    public String gender { get;set; }
    public String address { get;set; }
    public Decimal cost { get;set; }
    public Contact addcontact { get;set; }
    public Account addAccount { get;set; }
    public PurchaseRequestFormController (ApexPages.StandardController p_controller) {
    }
    public Attachment attachment {
        get {
            if (attachment == null)
                attachment = new Attachment();
            return attachment;
        }
        set;
    }
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Permanent Address','Permanent Address')); 
        options.add(new SelectOption('Temporary Address','Temporary Address')); 
        //options.add(new SelectOption('MEXICO','Mexico'));
        return options; 
    }
    public List<SelectOption> getItemsHospital() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Billing Address','Billing Address')); 
        options.add(new SelectOption('Shipping Address','Shipping Address')); 
        return options; 
    }
    Public void getForm(){
        //update addContact;
        typeOfRequests = new List<SelectOption>();
        typeOfRequests.add(new SelectOption('','-None-'));
        typeOfRequests.add(new SelectOption('Atlas Adventures','Atlas Adventures'));
        typeOfRequests.add(new SelectOption('Act of Kindness','Act of Kindness'));
        typeOfRequests.add(new SelectOption('Airfare','Airfare'));
        typeOfRequests.add(new SelectOption('Housing','Housing'));
        typeOfRequests.add(new SelectOption('Birthday','Birthday'));
        genders = new List<SelectOption>();
        genders.add(new SelectOption('','-None-'));
        genders.add(new SelectOption('Male','Male'));
        genders.add(new SelectOption('Female','Female'));
        TRHC__Config_Settings__c config = TRHC__Config_Settings__c.getInstance('Default');
        list<string> userIds = (config.purchase_order_approve_users__c).split(';');
        appUsers = new List<SelectOption>();
        appUser = [SELECT ID, NAME, EMAIL, USERNAME FROM USER WHERE ID IN :userIds];
        for(User user:appUser){
            appUsers.add(new SelectOption(user.Name,user.Name)); 
        }
        Id sObjId = ApexPages.CurrentPage().getParameters().get('id');
        String sObjName = sObjId.getSObjectType().getDescribe().getName();
        if(sObjName == 'Contact'){
            addContact = [
                SELECT Name, Phone, email, Recruiters__c, Temporary_Address_1__c, Temporary_Address_2__c, Permanant_Address__c,
                TRHC__Permanent_Address_1__c, TRHC__Permanent_Address_2__c, TRHC__Permanent_City__c, State_pic__c, temporary_state__c,
                TRHC__Permanent_Zip_Postal_Code__c, TRHC__Temporary_Street__c, TRHC__Temporary_City__c,
                TRHC__Temporary_State_Province__c, TRHC__Temporary_Zip_Postal_Code__c 
                FROM Contact 
                WHERE ID =:sObjId 
                LIMIT 1]; 
            System.debug('contact'+addContact);
        }
        if(sObjName == 'Account'){
            addAccount = [
                Select Name, Phone, BillingAddress, BillingStreet,
                BillingCity, BillingState, BillingCountry,BillingPostalCode, ShippingCountry,
                ShippingCity, ShippingPostalCode, ShippingState, ShippingStreet, TRHC__Account_Manager__c,
                Purchase_Request_Act_of_Kindness__c, Purchase_Request_Airfare__c, Purchase_Request_Atlas_Adventures__c,
                Purchase_Request_Birthday__c, Purchase_Request_Housing__c
                    From Account 
                Where ID = :sObjId Limit 1];
            system.debug('account'+addAccount);
        }
    }
    public PageReference submitForm(){
        system.debug('address@@@'+address);
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Id sObjId = ApexPages.CurrentPage().getParameters().get('id');
        String sObjName = sObjId.getSObjectType().getDescribe().getName();
        System.debug('sObjName'+sObjName);
        string submitedPerson = 'submitted by : '+Userinfo.getName() +'\n';
        String taskDescription = '';
        if(sObjName == 'Contact'){
            taskDescription = 'Contact Name: '+addContact.Name +   '\n'+
                'Contact Phone  -   '+addContact.Phone+'\n'+
                'Contact Email  -   '+addContact.email+'\n'+
                'Recruiter      -   '+addContact.Recruiters__c+'\n'+'\n';
        }
        if(sObjName == 'Account'){
            taskDescription = 'Hospital Name: '+addAccount.Name +   '\n'+
                'Hospital Phone  -   '+addAccount.Phone+'\n'+   
                'Client Manager      -   '+addAccount.TRHC__Account_Manager__c+'\n'+'\n';
            if(address == 'Billing Address'){
              taskDescription += 'Hopital Billing Address'+'\n'+'\n'+ 
                'Hospital Street  -   '+addAccount.BillingStreet+'\n'+
                'Hospital City  -   '+addAccount.BillingCity+'\n'+
                'Hospital State  -   '+addAccount.BillingState+'\n'+
                'Hospital Country  -   '+addAccount.BillingCountry+'\n';
            }else if(address == 'Shipping Address'){
                taskDescription += 'Hopital Shipping Address'+'\n'+'\n'+ 
                'Hospital Shipping Street  -   '+addAccount.ShippingStreet+'\n'+
                'Hospital Shipping City  -   '+addAccount.ShippingCity+'\n'+
                'Hospital Shipping State  -   '+addAccount.ShippingState+'\n'+
                'Hospital Shipping Country  -   '+addAccount.ShippingCountry+'\n'; 
            }
        }
        String purchaseRequestInfo = 'REQUEST-'+'\n'+'\n'+
            'RequestType    -   '+requestType +'\n'+
            'Description    -   '+Description +'\n'+
            'Due Date       -   '+Date.valueOf(dueDate)+'\n'+
            'purchaseItems  -   '+purchaseItems+'\n'+
            'PurchaseLocation - '+PurchaseLocation+'\n'+
            'DelveryDateTime -  '+DelvDateTime+'\n'+
            'Shipping address - '+shippingAddress+'\n'+
            'Message        -   '+Message+'\n'+'\n'+
            'Cost for '+requestType+'\n'+'\n'+
            'Cost - '+cost+'\n'+'\n';
        if(sObjName == 'Contact'){
            purchaseRequestInfo +=  'WORK INFORMATION -'+'\n'+'\n'+
                'Assignment Date    -   '+AssignmentDate +'\n'+
                'Hospital Name    -   '+HospitalName +'\n'+
                'Hospital Address    -   '+HospitalAddress +'\n'+'\n';
                
        }
        if(sObjName == 'Account'){
            purchaseRequestInfo +=  'Contact INFORMATION -'+'\n'+'\n'+
                'Contact Name    -   '+contactName +'\n'+
                'Contact Email    -   '+contactEmail +'\n'+
                'Contact Phone    -   '+contactPhone +'\n';
        }
        if(sObjName == 'Contact' && address == 'Permanent Address')
        {
            purchaseRequestInfo += 'Permanent Address INFORMATION -'+'\n'+'\n'+
                'Permanent Street -'+addContact.Permanant_Address__c+'\n'+'\n'+
                'Permanent City -'+addContact.TRHC__Permanent_City__c+'\n'+'\n'+
                'Permanent State/Province -'+addContact.State_pic__c+'\n'+'\n'+
                'Permanent Zip/Postal Code -'+addContact.TRHC__Permanent_Zip_Postal_Code__c+'\n'+'\n';
            update addContact;
        }else if(sObjName == 'Contact' && address == 'Temporary Address'){
            purchaseRequestInfo += 'Temporary Address INFORMATION -'+'\n'+'\n'+
                'Temporary Street -'+addContact.Temporary_Address_1__c+'\n'+'\n'+
                'Temporary Address2 -'+addContact.Temporary_Address_2__c+'\n'+'\n'+
                'Temporary City -'+addContact.TRHC__Temporary_City__c+'\n'+'\n'+
                'Temporary State/Province -'+addContact.temporary_state__c+'\n'+'\n'+
                'Temporary Zip/Postal Code -'+addContact.TRHC__Temporary_Zip_Postal_Code__c+'\n'+'\n';
            update addContact;
        }
        purchaseRequestInfo +=    'Flight info'+'\n'+'\n'+
            'Gender         -   '+gender+'\n'+
            'Flight         -   '+Flight+'\n'+
            'Departing      -   '+Departing +'\n'+
            'Departing Date -   '+departureDate+'\n'+
            'Arrival        -   '+Arrival +'\n'+
            'Return Date    -   '+returnDate+'\n'+
            'DateofBirth    -   '+DateofBirth+'\n'+
            'FullName:      -   '+FullName+'\n'+
            'travelDocument -   '+travelDocument+'\n'+'\n'+
            'Hotel info'+'\n'+'\n'+
            'Hotel          -   '+Hotel+'\n'+
            'Location       -   '+Location+'\n'+
            'Date           -   '+dateHotel;
        PageReference p = new PageReference('/'+sObjId);
        Task task = new Task();
        if(sObjName == 'Contact'){
            task.Whoid = sObjId;  
        }
        if(sObjName == 'Account'){
            task.WhatId = sObjId;
        }
        if(requestType!=null &&requestType!='')
        {
            task.Subject = requestType;   
        }
        else{
            task.Subject = 'Purchase Request';
        }
        task.ActivityDate = Date.valueOf(dueDate);        
        task.priority= 'Normal';
        task.status = 'Not Started';
        task.description = submitedPerson +taskDescription+purchaseRequestInfo ;
        for(User user:appUser){
            system.debug('user@@'+user.Name);
            system.debug('selectedUser@@'+selectedUser);
            if(user.Name == selectedUser){
                task.OwnerId= user.Id;
                insert task;
                Messaging.SingleEmailMessage mail = 
                    new Messaging.SingleEmailMessage(); 
                List<String> sendTo = new List<String>();
                try {
                    if(attachment!=null && attachment.Body!=null){
                        attachment.OwnerId = user.id;
                        attachment.ParentId = task.id;
                        insert attachment;
                        Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
                        efa.setFileName(attachment.Name);
                        efa.setBody(attachment.Body);
                        mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
                    }
                } 
                catch (DMLException e) {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
                    return null;
                } 
                finally {
                }
                //sendTo.add(user);
                mail.setTargetObjectId(user.id);
                mail.saveAsActivity = false;
                //mail.setToAddresses(sendTo);
                mail.setSenderDisplayName('Purchase Request');
                mail.setSubject('Purchase Request Form Submission');
                String body = '<html>'+'<image id="Logo" style="background-color: #ffffff;text-align: center;height:100px;width:50px;" src="https://partialcop-atlasmedstaff.cs13.force.com/servlet/servlet.ImageServer?id=01541000001n4P3&oid=00DW0000008xGn7&lastMod=1488469482000"/>'+'<p>'+'Dear ' + user.Name + ', '+'<br/>';
                body += 'Below is the purchse request details '+'<br/>';
                body += submitedPerson+'</br>' +taskDescription+'<br/>'+purchaseRequestInfo+'</p>'+'</html>';
                System.debug('body@@'+body);
                body = body.replace('\n', '<br/>');
                mail.setHtmlBody(body);
                mails.add(mail);
            }
        }
        try{
            if(sObjName == 'Contact'){
                GetLatestPlacementDetailsByCandidate Gpc = new GetLatestPlacementDetailsByCandidate();
                TRHC__placement__c latestPlc = Gpc.getLatestPlacement(sObjId);
                system.debug('latestPlc@@@'+latestPlc);
                if(latestPlc != null) {
                    if( requestType == 'Atlas Adventures' ) {
                        if(cost < 400){
                            latestPlc.Purchase_Request_Atlas_Adventures__c = latestPlc.Purchase_Request_Atlas_Adventures__c-cost;
                        }else{
                            latestPlc.Purchase_Request_Atlas_Adventures__c = latestPlc.Purchase_Request_Atlas_Adventures__c-400;
                        }
                    }
                    else if( requestType == 'Act of Kindness'){
                        latestPlc.Purchase_Request_Act_of_Kindness__c += cost;
                    }
                    else if( requestType == 'Airfare' ){
                        latestPlc.Purchase_Request_Airfare__c += cost;
                    }
                    else if( requestType == 'Housing' ){
                        latestPlc.Purchase_Request_Housing__c += cost;
                    }
                    else if( requestType == 'Birthday' ){
                        if(cost < 50){
                            latestPlc.Purchase_Request_Birthday__c = latestPlc.Purchase_Request_Birthday__c - cost;   
                        }else{
                            latestPlc.Purchase_Request_Birthday__c = latestPlc.Purchase_Request_Birthday__c - 50; 
                        }
                    }
                    update latestPlc;
                }
            }
            else if(sObjName == 'Account'){
                if(addAccount != null) {
                    if( requestType == 'Atlas Adventures' ) {
                        if(cost < 400){
                            addAccount.Purchase_Request_Atlas_Adventures__c = addAccount.Purchase_Request_Atlas_Adventures__c-cost;
                        }else{
                            addAccount.Purchase_Request_Atlas_Adventures__c = addAccount.Purchase_Request_Atlas_Adventures__c-400;
                        }
                    }
                    else if( requestType == 'Act of Kindness'){
                        addAccount.Purchase_Request_Act_of_Kindness__c += cost;
                    }
                    else if( requestType == 'Airfare' ){
                        addAccount.Purchase_Request_Airfare__c += cost;
                    }
                    else if( requestType == 'Housing' ){
                        addAccount.Purchase_Request_Housing__c += cost;
                    }
                    else if( requestType == 'Birthday' ){
                        if(cost < 50){
                            addAccount.Purchase_Request_Birthday__c = addAccount.Purchase_Request_Birthday__c - cost;   
                        }else{
                            addAccount.Purchase_Request_Birthday__c = addAccount.Purchase_Request_Birthday__c - 50; 
                        }
                    }
                    update addAccount;
                }
            }
            
        }
        catch(Exception e){
            
        }
        Messaging.sendEmail(mails);
        return p;
    }
}