public class ExpenseLineItemUtils {

    public class ExpenseLineItemWrapper implements Comparable {
        
        /** The sortorder*/
        public Integer sortorder{
            get;
            private set;
        }
        
         /** The Expense */
        public TRHC__Expense__c expense {
            get;
            private set;
        }
        
        public ExpenseLineItemWrapper() {}
        public ExpenseLineItemWrapper(TRHC__Expense__c expense, Integer sortOrder) {
            this.sortOrder = sortOrder;
            this.expense = expense;
            
        }
        
        public Integer compareTo(Object obj) {
            ExpenseLineItemWrapper expense = (ExpenseLineItemWrapper)(obj);
            
            if (this.sortorder > expense.sortorder) {
                return 1;
            }

            if (this.sortorder == expense.sortorder) {
                return 0;
            }

            return -1;
        }
    }
}