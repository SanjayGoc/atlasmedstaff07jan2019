@isTest private class ProphecySendAssessments_TC
{
 
    static testMethod void testCodeCoverageforResponseClassess() 
    {
        test.starttest();
        TRHC__Config_Settings__c v_enablecustomsettings = new TRHC__Config_Settings__c();
        v_enablecustomsettings.TRHC__Enable_Prophecy__c = true;
        v_enablecustomsettings.name = 'test';
        insert v_enablecustomsettings;
        
        user u =[select id, name from user where id =:UserInfo.getUserId()];
        
        TRHC__Prophecy_Settings__c v_prophecysettings = new TRHC__Prophecy_Settings__c();
        v_prophecysettings.TRHC__Department_ID__c = '12345';
        v_prophecysettings.TRHC__Security_Code__c = 'BF3234F48-985D-457E-9D9B-2A4000CAE539';
        v_prophecysettings.TRHC__User_ID__c = '19465';
        v_prophecysettings.SetupOwnerId = u.id;
        insert v_prophecysettings;
 
        contact v_contact = new contact();
            v_contact.lastname = 'test';
            v_contact.Email = 'sai@yahoo.com';
            v_contact.firstname = 'test';
            v_contact.TRHC__Prophecy_CaregiverID__c = '12';
        insert v_contact;
        
        TRHC__Assessment__c assement =  NEW TRHC__Assessment__c ();
            assement.TRHC__Assessment_Name__c = 'Test';
            assement.TRHC__Total_Questions__c = 5;
            assement.TRHC__Assessment_Type__c = 'Test';
            assement.TRHC__Contact__c = v_contact.Id;
        insert assement;
        
        TRHC__Skill_Answers__c answerRecord = NEW TRHC__Skill_Answers__c ();
            answerRecord.TRHC__Assessment_Type__c = 'Test';
            answerRecord.TRHC__Skill_Checklist_Name__c = 'Test';
            answerRecord.TRHC__Total_Questions__c = 6;
            answerRecord.TRHC__Contact__c = v_contact.Id;
        insert answerRecord;
            
        system.assertequals('12',v_contact.TRHC__Prophecy_CaregiverID__c); 
        ApexPages.currentPage().getParameters().put('id', v_contact.id);
        ApexPages.StandardController stdController = new ApexPages.StandardController(v_contact);
        
        ProphecySendAssessments.DynamicAssessmentObject aass = new ProphecySendAssessments.DynamicAssessmentObject(v_contact.lastname, '56', v_contact.lastname, '78', '54', '3');
        aass.selected = true;
        aass.costPoints = '50';
        aass.TimeLimit = '60';
        list < ProphecySendAssessments.DynamicAssessmentObject > a = new list < ProphecySendAssessments.DynamicAssessmentObject > ();
        set< ProphecySendAssessments.DynamicAssessmentObject > aSet = new set < ProphecySendAssessments.DynamicAssessmentObject > ();
        a.add(aass);
        List <String> assementsList = New List <String> ();
        assementsList.add('Test');
        ProphecySendAssessments pga = new ProphecySendAssessments();
        pga.ListOfAllAssessmentsSentToProphecy = a;
        pga.AssessmentList = a;
        pga.Searchforassessments = '<object>' +
                                     '<success>true</success>' +
                                     '<count>1</count>' +
                                     '<assessments>' +
                                     '<assessment>' +
                                     '<numberofquestions>5</numberofquestions>' +
                                     '<costpoints>1</costpoints>' +
                                     '<assessmentname>Demo Assessment</assessmentname>' +
                                     '<assessmenttype>Test</assessmenttype>' +
                                     '<assessmentid>123456</assessmentid>' +
                                     '<timelimit>1300</timelimit>' +
                                     '</assessment>' +
                                     '</assessments>' +
                                     '</object>';
         pga.sendassessmentresponse = '<object>' +
                                     '<success>true</success>' +
                                     '<count>1</count>' +                                     
                                     '<requests>' +
                                     '<request>' +
                                     '<caregiverid>123456</caregiverid>' +
                                     '<requestid>123456</requestid>' +
                                     '<assessments>' +
                                     '<assessment>' +
                                     '<caregiverassessmentid>123456</caregiverassessmentid>' +
                                     '<hashedexamid>123456</hashedexamid>' +
                                     '<assessmentname>Demo Assessment</assessmentname>' +
                                     '<assessmenttype>Test</assessmenttype>' +
                                     '<assessmentid>123456</assessmentid>' +
                                     '</assessment>' +
                                     '</assessments>' +
                                     '</request>' +
                                     '</requests>' +
                                     '</object>';
        pga.assesstypes = '<object><success>true</success><HashedExamId>132211<HashedExamId><clienttype>Clinical</clienttype><assessmenttypes><assessmenttype>Test</assessmenttype>'+
                          '<assessmenttype>MandatoryTest</assessmenttype><assessmenttype>Allied</assessmenttype><assessmenttype>Checklist</assessmenttype></assessmenttypes></object>';
        pga.AssessmentCount = '5';
        pga.AssessmentIsActive = 'TRUE';
        List <String> types = NEW List <String> ();
            types.add('Test');
            types.add('Checklist');
        pga.AssessmentTypes = types; 
        ProphecySendAssessments.Caregiver PAC = new ProphecySendAssessments.Caregiver();
        ProphecySendAssessments.Assessment PAA = new ProphecySendAssessments.Assessment();
        ProphecySendAssessments.ProphecyConnectCDT_cfc PAPC = new ProphecySendAssessments.ProphecyConnectCDT_cfc();
        ProphecySendAssessments.GetAssessmentTypes_element PAGA = new ProphecySendAssessments.GetAssessmentTypes_element();
        ProphecySendAssessments.GetAssessmentTypesResponse_element pgaTR = new ProphecySendAssessments.GetAssessmentTypesResponse_element();
        ProphecySendAssessments.SendAssessments_element v_prophecysettingsAER = new ProphecySendAssessments.SendAssessments_element();
        ProphecySendAssessments.SendAssessmentsResponse_element v_prophecysettingsATR = new ProphecySendAssessments.SendAssessmentsResponse_element();
        ProphecySendAssessments.GetCaregivers_element PGCE = new ProphecySendAssessments.GetCaregivers_element();
        ProphecySendAssessments.GetCaregiversResponse_element PGRCE = new ProphecySendAssessments.GetCaregiversResponse_element();
        ProphecySendAssessments.GetAssessmentsResponse_element pgaRE = new ProphecySendAssessments.GetAssessmentsResponse_element();
        ProphecySendAssessments.GetAssessments_element pgaAE = new ProphecySendAssessments.GetAssessments_element();
        ProphecySendAssessments.GetCaregiverAuthURL_element pgaAE1 = new ProphecySendAssessments.GetCaregiverAuthURL_element();
        ProphecySendAssessments.GetCaregiverAuthURLResponse_element pgaAE2 = new ProphecySendAssessments.GetCaregiverAuthURLResponse_element();
        //pga.back();
               
        //pga.getByPassURL();
        //pga.reset();
        pga.showassessments();
        pga.processSelectedAssessments();
        //pga.GetAssessmentTypes();
        pga.sendassess();
        try {
            PAPC.GetCaregiverAuthURL('BF3234F48-985D-457E-9D9B-2A4000CAE539',Double.valueOf('123.00'));
            PAPC.GetAssessments('BF3234F48-985D-457E-9D9B-2A4000CAE539',Double.valueOf('123'),'Test');
            pga.getByPassURL();
        }
        catch (Exception e){
        
        }
        
        test.stoptest();
    } 

}