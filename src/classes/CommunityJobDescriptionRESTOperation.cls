@RestResource(urlMapping='/mobile/JobDescription/*')
global class CommunityJobDescriptionRESTOperation {
    @HttpGet
    global static ResponseHandler CommunityJobDescriptionRESTOperation (){
        ResponseHandler res = new ResponseHandler();
        Recruiter recObj = new Recruiter();
        
        String apiToken = RestContext.request.params.get('apiToken');
        String jobId = RestContext.request.params.get('jobId');
        List<Contact> userList = new  List<Contact>();
        if(apiToken  == 'GUEST'){
          userList = [Select  Id,
                              Name,
                              firstName,
                              lastName,
                              apiToken__c
                              From Contact
                      ];

        }
        else if(apiToken != '' && apiToken != 'None') {
           userList = [Select Id,
                              Name,
                              firstName,
                              lastName,
                              apiToken__c
                              From Contact
                              Where apiToken__c =: apiToken
                      ];
        }
                        
        if(!userList.isEmpty()){
            List<TRHC__Job__c> jobList = [select id,
                                                 name,
                                                 TRHC__Job_Title__c,
                                                 Compact_License_States__c,
                                                 Speciality1__c,
                                                 TRHC__Job_City__c,
                                                 State_pic__c,
                                                 PayScale__c,
                                                 TRHC__Job_Description_Rich__c,
                                                 Shift_Type2__c,
                                                 TRHC__Recruiter__r.Name, 
                                                 TRHC__Recruiter__r.Email, 
                                                 TRHC__Recruiter__r.Phone
                                          From TRHC__Job__c 
                                          where id =: jobId];            
            
            if(jobList.isEmpty()){
              res.success = 0;
              res.message = new string[]{'Job Not Found.'};
              return res;
            }
            res.success = 1;
            res.message = new string[]{'Success.'};
            res.jobId = jobList[0].id;
            res.jobTitle = jobList[0].name;
            res.License = jobList[0].Compact_License_States__c;
            if(jobList[0].Speciality1__c != null){
              res.Specialiity = jobList[0].Speciality1__c.split(';');
            } else {
              res.Specialiity = new List<string>();
            }

            res.City = jobList[0].TRHC__Job_City__c;
            res.State = jobList[0].State_pic__c;
            res.Descriptions = jobList[0].TRHC__Job_Description_Rich__c;
            if(jobList[0].Shift_Type2__c != null){
              res.ShiftType =  jobList[0].Shift_Type2__c.split(';');
            } else {
              res.ShiftType = new List<string>();
            }
            res.PayScale = jobList[0].PayScale__c;//jobList[0].
            res.jobName = jobList[0].TRHC__Job_Title__c;

            recObj.recName = jobList[0].TRHC__Recruiter__r.Name;
            recObj.contactNumber = jobList[0].TRHC__Recruiter__r.Phone; 
            recObj.emailId = jobList[0].TRHC__Recruiter__r.Email;
            
            res.recruiterObj = recObj;
            return res;
        }
        res.success = 2;
        res.message = new string[]{'Unauthorized User.'};
        return res;
    }
    
    global class ResponseHandler {
        global integer success;
        global string[] message;
        global string jobId;
        global string jobTitle;
        global string License;
        global String jobName;
        global List<string> Specialiity;
        global string City;
        global string State;
        global string Descriptions;
        global List<string> ShiftType;
        global Decimal PayScale;
        global Recruiter recruiterObj;
        global ResponseHandler () {

        }
    }
    
    global class Recruiter{
        global string recName;
        global string contactNumber;
        global string emailId;
        global Recruiter(){
            
        }
    }
}