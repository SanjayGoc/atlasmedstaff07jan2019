@istest
public class UpdateGSAServiceBatchScheduleTest {
    static testmethod void UpdateGSAServiceTest() {
        Test.StartTest();
        UpdateGSAServiceBatchSchedule schTest = new UpdateGSAServiceBatchSchedule();      
        String sch = '0 0 23 * * ?';
        system.schedule('GSA ServiceJob Update', sch, schTest);
        Test.stopTest();
    }
}