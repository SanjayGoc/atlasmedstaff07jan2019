global class ProphecyUpdateResultsSchedule implements schedulable
{
  global void execute(SchedulableContext sc)
  {
    ProphecyUpdateAssessmentResultsBatch obj 
            = new ProphecyUpdateAssessmentResultsBatch('TRHC__Skill_Answers__c'); 
    database.executebatch(obj, 1);
    
   /* ProphecyUpdateAssessmentResultsBatch obj1 
            = new ProphecyUpdateAssessmentResultsBatch('TRHC__Assessment__c'); 
    database.executebatch(obj1, 1);*/
  }
}