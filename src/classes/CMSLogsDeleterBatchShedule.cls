public class CMSLogsDeleterBatchShedule implements Schedulable{
    public void execute(system.SchedulableContext ctx){
        CMSLogsDeleterBatch btch = new CMSLogsDeleterBatch();
        Database.executeBatch(btch);
    }
}