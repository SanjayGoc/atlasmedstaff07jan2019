/*
* Name: CreateScratchSheetController
* Date : 05/17/2016
* Description : Creating scratchsheet from job list view
*/
public with sharing class  CreateScracthSheetFromTemplate{
    
    public Boolean isCreated {get; set;}
    
    public Boolean isError {get; set;}
    
    private static TRHC__Healthcare_Setting__c customSetting = 
            TRHC__Healthcare_Setting__c.getAll().get('Default');
    
    public String queryString;      
    
    public List<TRHC__Job__c> jobList {get ;set; }
    
    public List<String> jobIdList = new List<Id>();
    
    public List<TRHC__Expense__c> expancesList {get;set;}
    
    private String jobFieldList;

    public String candidateId;
    
    public CreateScracthSheetFromTemplate(){
        system.debug('calling');
        isCreated = false;
        isError = false;
        String jobIds = ApexPages.currentPage().getParameters().get('jId');
        //system.debug()
        candidateId = ApexPages.currentPage().getParameters().get('cId');
        jobIdList = (jobIds != null) ? jobIds.split(',') : new List<Id>();
        
        
        //get jobFields
        jobFieldList = ObjectFieldsHelper.getAllFields('TRHC__Job__c').get('TRHC__Job__c');
        //query for job
        String jobQuery ='Select '+jobFieldList+' from TRHC__Job__c where ID IN '+jobIdList;
        //String jobQuery ='Select id,trhc__state__c+' from TRHC__Job__c where ID IN'+jobIdList;
        system.debug(jobQuery);
        jobList = Database.query(jobQuery);                       
    }
    
    public PageReference createscratchsheet() {
        
        //getting scrtach sheet recordtype
        String scrtchSheetRecordTypeId = [Select Id From RecordType Where SobjectType = 'TRHC__Scratch_Sheet__c' and Name = 'ScratchSheet'].Id;
        
        String defaultScratchTemplate = (String)customSetting.get('TRHC__Default_Scratch_sheet_Template_Name__c');
        
        defaultScratchTemplate = '\''+ defaultScratchTemplate + '\'';
        
        //get All fields of an sobject 
        
        Map<String,String> scratchSheethelper = ObjectFieldsHelper.getAllFields('TRHC__Scratch_Sheet__c');
        
        String allfields  = scratchSheethelper.get('TRHC__Scratch_Sheet__c');
        
        queryString = 'Select '+ allfields + ' from TRHC__Scratch_Sheet__c '+' where name = '+defaultScratchTemplate;
        
        if(defaultScratchTemplate == null || defaultScratchTemplate ==''){
        
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 
                    '<span style="font-family: Tahoma;">' + 
                    'Scrtch sheet is not available' + '.</span>'
            ));
            isError = true;
        }
        if(defaultScratchTemplate !=null ){
            system.debug('inside that ');
            for(TRHC__Job__c job : jobList){
                system.debug('check ');  
                
                TRHC__Scratch_Sheet__c scratchSheet = Database.query(queryString);
            
                String expnaceFields = ObjectFieldsHelper.getAllFields('TRHC__Expense__c').get('TRHC__Expense__c');
        
                //get all expanses of scratch sheet template query
        
                Id cid = string.valueOf(scratchSheet.id).subString(0,15);
                String sid='\''+cid+'\'';
                
                    
                String expancesQueryString = 'Select '+ expnaceFields + ' from TRHC__Expense__c '+' where TRHC__Scratch_Sheet__c ='+sid;
            
                System.debug('expancesQueryString@@'+expancesQueryString);        
                        
                List<TRHC__Expense__c> templateExpnaseList = Database.query(expancesQueryString);
                
                system.debug('scratchsheet@@'+scratchSheet);
                
                TRHC__Scratch_Sheet__c newScratchSheet = scratchSheet.clone(true,false);
                
                system.debug('newScratchsheet@@'+newScratchSheet);
                
                newScratchSheet.id = null;
                if(candidateId != null)
                    newScratchSheet.TRHC__Candidate__c = candidateId;
                //mapping fields
                List<Job_to_Scratch_Sheet__c> jobToScratchSheet = [SELECT Job_Field_API_Name_Custom__c, 
                                                                          Scratch_Sheet_Field_API_Name__c 
                                                                   From Job_to_Scratch_Sheet__c];
                for(Job_to_Scratch_Sheet__c jobToScratch : jobToScratchSheet){
                    string jobField = jobToScratch.Job_Field_API_Name_Custom__c;
                    String scratchSheetField = jobToScratch.Scratch_Sheet_Field_API_Name__c;
                    if(jobFieldList.contains(jobField) && allfields.contains(scratchSheetField)){
                        //newScratchSheet.scratchSheetField= job.jobField;
                        newScratchSheet.put(scratchSheetField,job.get(jobField));
                    }
                }
                newScratchSheet.TRHC__County__c = job.TRHC__County__c ;
            
            /*newScratchSheet.TRHC__State__c=job.TRHC__State__c;
            newScratchSheet.State_Pic__c = job.state_pic__c;
            newScratchSheet.TRHC__Client_Fee__c = job.TRHC__Client_Fee__c;
            newScratchSheet.TRHC__County__c = job.TRHC__County__c;*/
            newScratchSheet.Name=job.TRHC__Job_Title__c+' ScratchSheet';
            
            newScratchSheet.RecordTypeId = scrtchSheetRecordTypeId;
            //newScratchSheet. TRHC__Overtime_Pay_Rate__c = TRHC__Overtime_Rate__c * 0.7;
            system.debug('comming');
            
            newScratchSheet.OT_Pay_rate_Override__c = true;

            insert newScratchSheet;
            expancesList = new List<TRHC__Expense__c>();
            for(TRHC__Expense__c expanse : templateExpnaseList){
                expanse.id = null;
                expanse.TRHC__Scratch_Sheet__c = newScratchSheet.id;
                expancesList.add(expanse);
            }
            insert expancesList;
            /*if(!expancesList.isEmpty()){
                //getDataCalculation(expancesList);
                List<Id> allIds = new List<Id>();
                for(TRHC__Expense__c singleExp : expancesList){
                    allIds.add(singleExp.Id);
                }
                if(!allIds.isEmpty()){
                    ghpCalculate(allIds,newScratchSheet.Id);
                }
            }*/
            job.TRHC__Scratch_Sheet__c = newScratchSheet.id;
            update Job;
                // return new Pagereference('/'+newScratchSheet.id);   
                return candidateId != null ? new Pagereference('/' +newScratchSheet.id +'/e?retURL=%2F'+newScratchSheet.id) : new Pagereference('/'+newScratchSheet.id);
                //return  new Pagereference('/'+job.id);
            }
            
                
            }
        return null;
        
    }

    /*public void getDataCalculation(List<TRHC__Expense__c> parameterList){
        try{
               List<TRHC__Expense__c> listUpdate = new List<TRHC__Expense__c>();
               String stemplaterecTypeId = Schema.SObjectType.TRHC__Scratch_Sheet__c.getRecordTypeInfosByName().get('ScratchSheet Template').getRecordTypeId();
               TRHC__Scratch_Sheet__c sheetSoql = [SELECT Id,Total_Bill_Hrs1__c,OverTime_GPH__c,Over_Time_Include_Bill__c,Pay_Burden_New__c,TRHC__Pay_Burden__c,isGPHCalculated__c,Non_Taxable_Expense__c,With_OverTime_Gross_Profit__c FROM TRHC__Scratch_Sheet__c WHERE Id =: parameterList[0].TRHC__Scratch_Sheet__c AND RecordTypeId !=: stemplaterecTypeId];
               System.debug('sheetSoqlfirst'+sheetSoql);
               Decimal dollar = Decimal.valueOf(System.Label.TotalGPH);
               
               if(sheetSoql.isGPHCalculated__c == false){
                    String getLabel = System.Label.Editable_Expense;
                    Set<String> editAccess = new Set<String>();
                    if(getLabel!=Null){
                     editAccess.addAll(getLabel.Split(','));
                    }
                    AggregateResult [] aggtegateTotalExpence = [Select SUM(TRHC__Amount__c) amount FROM TRHC__Expense__c WHERE Id IN: parameterList AND TRHC__Grouping_Total__c =: 'Non Taxable Expense'];
                    Decimal decimalTotalExpense = Decimal.valueOf(String.valueOf(aggtegateTotalExpence[0].get('amount')));
                    System.debug('decimalTotalExpense'+decimalTotalExpense);
                    List<TRHC__Expense__c> listOfExpenses = [Select Id,TRHC__Amount__c,TRHC__Occurence__c FROM TRHC__Expense__c WHERE Id IN: parameterList AND TRHC__Expense_Name__c IN: editAccess];
                    AggregateResult [] aggtegateEditable = [Select SUM(TRHC__Amount__c) amount FROM TRHC__Expense__c WHERE Id IN: parameterList AND TRHC__Expense_Name__c IN: editAccess];
                    Decimal totalExpenses = Decimal.valueOf(String.valueof(aggtegateEditable[0].get('amount')));
                    Decimal addTOCalculation = (sheetSoql.Over_Time_Include_Bill__c - (sheetSoql.Pay_Burden_New__c + sheetSoql.TRHC__Pay_Burden__c  + decimalTotalExpense));
                    System.debug('addTOCalculation'+addTOCalculation);

                    Decimal totalGPHAdd = ((dollar * sheetSoql.Total_Bill_Hrs1__c) - addTOCalculation);
                    System.debug('totalGPHAdd==>'+totalGPHAdd.round());
                    
                    
                    for(TRHC__Expense__c percentageCalculation : listOfExpenses){
                            //if(singleContain.contains(percentageCalculation.TRHC__Expense_Name__c)){
                                Decimal expenseValue = (percentageCalculation.TRHC__Amount__c  - (((percentageCalculation.TRHC__Amount__c / totalExpenses) * 100) * totalGPHAdd) / 100 ) / percentageCalculation.TRHC__Occurence__c;
                                percentageCalculation.TRHC__Value__c = expenseValue.round();
                                System.debug('expenseValue==>'+ expenseValue.round());
                                listUpdate.add(percentageCalculation);
                            //}            
                    }
                    if(listUpdate.size() > 0){
                        System.debug('listUpdate==>'+listUpdate);
                        System.debug('listUpdate.size==>'+listUpdate.size());
                        update listUpdate;
                        sheetSoql.isGPHCalculated__c = true;
                        sheetSoql.Non_Taxable_Expense__c = decimalTotalExpense - totalGPHAdd;
                        update sheetSoql;
                        System.debug('sheetSoqlFINAL'+sheetSoql);
                    } 
                }     
        }catch(QueryException e){
                System.debug('QueryException'+e.getMessage());
        }
    }*/
    /*@future
    public static void ghpCalculate(List<Id> expenseId,Id scratchSheetId){
        String stemplaterecTypeId = Schema.SObjectType.TRHC__Scratch_Sheet__c.getRecordTypeInfosByName().get('ScratchSheet Template').getRecordTypeId();
        TRHC__Scratch_Sheet__c scratch = [SELECT Id,
                                                 Total_Bill_Hrs1__c,
                                                 OverTime_GPH__c,
                                                 Over_Time_Include_Bill__c,
                                                 Pay_Burden_New__c,
                                                 isGPHCalculated__c,
                                                 TRHC__Assignment_length__c,
                                                 TRHC__Hours_Per_Week__c,
                                                 With_OverTime_Gross_Profit__c,
                                                 over_time__c,
                                            FROM TRHC__Scratch_Sheet__c
                                           WHERE Id =:scratchSheetId
                                             AND RecordTypeId !=: stemplaterecTypeId];
        System.debug('scratch'+scratch);                                             
        Map<Id,TRHC__Expense__c> mapOfExpense = new Map<Id,TRHC__Expense__c>([SELECT Id,
                                                                                     TRHC__Amount__c,
                                                                                     TRHC__Occurence__c,
                                                                                     TRHC__Expense_Name__c,
                                                                                     TRHC__Value__c
                                                                                FROM TRHC__Expense__c
                                                                               WHERE Id IN: expenseId]);
        if(!scratch.isGPHCalculated__c){
            String getLabel = System.Label.Editable_Expense;
            Set<String> editAccess = new Set<String>();
            if(getLabel!=Null){
                editAccess.addAll(getLabel.Split(','));
            }
            String getRemoval = System.Label.Remove_Editable_For_Calculation;
            if(getRemoval != null){
                editAccess.removeAll(getRemoval.split(','));
            }
            //System.debug('editAccess'+editAccess);
            Decimal editableExpenseTotal = 0.00;
            for(Id mapId : mapOfExpense.keySet()){
                if(editAccess.contains(mapOfExpense.get(mapId).TRHC__Expense_Name__c)){
                    //System.debug('TRHC__Expense_Name__c:'+mapOfExpense.get(mapId).TRHC__Expense_Name__c);
                    //System.debug('TRHC__Amount__c:'+mapOfExpense.get(mapId).TRHC__Amount__c);
                    editableExpenseTotal += mapOfExpense.get(mapId).TRHC__Amount__c;
                }
            }
            List<TRHC__Expense__c> listUpdate = new List<TRHC__Expense__c>();
            Decimal dollar = Decimal.valueOf(System.Label.TotalGPH);
            if(scratch.over_time__c == null){
                scratch.over_time__c = 0.00;
            }
            Decimal calculation =  (dollar * ( scratch.TRHC__Assignment_length__c * (scratch.TRHC__Hours_Per_Week__c + scratch.over_time__c )));
            System.debug('calculation'+calculation);
        }

    }*/
    
}