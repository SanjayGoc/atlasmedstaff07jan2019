/**
* @description  class TinyUrl implements methods for updating tiny url in jobs where tiny url is not populated, 
* by making request to bitly.
*/
public with sharing class TinyUrl 
{
    public static String tinyUrl = '';
    private static String mode = 'live';
    
    /**
* @name fillTinyUrl
* @description Fills tiny url on jobs which doesn't have tiny url.
* @param list<id> jobsIdsWithoutTinyUrl
* @return void
*/
    public static void fillTinyUrl(list<id> jobsIdsWithoutTinyUrl) 
    {
        try
        {   
            system.debug(logginglevel.error, '::::::TinyMethod jobsIdsWithoutTinyUrl='+jobsIdsWithoutTinyUrl);
            
            // Selected jobs with emty tiny urls by id
            List<TRHC__Job__c> jobsWithoutTinyUrl = [
                        SELECT JobPublishURLCustom__c, JobPublishTinyURLCustom__c, JobApplicationURLCustom__c, JobApplicationTinyURL__c 
                        FROM TRHC__Job__c
                        WHERE id IN :jobsIdsWithoutTinyUrl];
            System.debug('jobsWithoutTinyUrl '+jobsWithoutTinyUrl);
            for (TRHC__Job__c job : jobsWithoutTinyUrl)
            {
                // fill field Job Publish Tiny Url.
                job.JobPublishTinyURLCustom__c = getTinyUrl(job.JobPublishURLCustom__c);
                
                // fill field Job Application Tiny Url.
                job.JobApplicationTinyURL__c = getTinyUrl(job.JobApplicationURLCustom__c);
            }

            // upsert jobs
            DB.doSecureUpsert(jobsWithoutTinyUrl, new Schema.sObjectField[]{
                    TRHC__Job__c.JobPublishTinyURLCustom__c,
                    TRHC__Job__c.JobApplicationTinyURL__c});
        }
        catch (Exception e)
        {
            System.debug(logginglevel.error,'::::::Exception'  + e);
        }
    }

    private static String getTinyUrl (String url)
    {
        XmlStreamReader reader;
        HttpResponse res;  
        
        // Build the http request
        Http h = new Http();
        HttpRequest req = buildWebServiceRequest(url);
        
        // Invoke web service call 
        if (mode=='live')  
            res = invokeWebService(h, req);   
    
        if (mode =='live')     
            reader = res.getXmlStreamReader();
            
        return readXMLResponse(reader,'url');
    }

    private static HttpRequest buildWebServiceRequest(String purl)
    {
        system.debug('purl is '+ purl);
        
        HttpRequest req = new HttpRequest();
        
        String encoded = EncodingUtil.urlEncode(purl, 'UTF-8');
        
        system.debug('encoded '+encoded);
        
        String apiKey;
        String login;
        TRHC__Config_Settings__c defaultSettings = TRHC__Config_Settings__c.getInstance('Default');
        if (defaultSettings != null)
        {
            apiKey = defaultSettings.BitlyApiKeyCustom__c;
            login = defaultSettings.BitlyLoginCustom__c;
        }
        
        String endpoint = 'http://api.bitly.com/v3/shorten?longUrl=' + encoded + '&login=' + login + '&apiKey=' + apiKey + '&format=xml';
        
        req.setEndpoint(endpoint); 
        
        req.setMethod('GET');
        
        system.debug('request is ' + req);
        
        return req;
    }

    private static HttpResponse invokeWebService(Http h, HttpRequest req)
    {
        //Invoke Web Service
        HttpResponse res = h.send(req);
        
        system.debug('response is ' + res);
        
        return res;
    }

    private static String readXMLResponse(XmlStreamReader reader, String sxmltag)
    {
        String retValue;
        
        // Read through the XML
        while(reader.hasNext()) 
        {
            if (reader.getEventType() == XmlTag.START_ELEMENT) 
            {
                if (reader.getLocalName() == sxmltag) 
                {
                    reader.next();
                    
                    if (reader.getEventType() == XmlTag.characters) 
                        retValue = reader.getText();
                }
            }
            
            reader.next();
        }
        return retValue;
    }
    
   
}