/*
*@author : chaitanya.b@targetrecruit.net
*@description : This controller is fetching the matching jobs for candidates under logged in user 
*/
public  with sharing class MatchedJObsForCandidatesController {
    Public Map<Contact, List<TRHC__job__c>> candiMatchMap{get;set;}
    Public boolean render{get;set;}
    
    public void getMatchedJobs(){
        Id candidateRecortypeId = [Select id from Recordtype where Name = 'Candidate'].id;
        TRHC__CustomSettingsComponent__c config = TRHC__CustomSettingsComponent__c.getInstance('Default');
        String whereCriteria = config.MatchedJobsCriteria__c;
        String Jbquery = 'SELECT Id, TRHC__Discipline__c, TRHC__Job_Title__c, TRHC__Job_Type__c, TRHC__Recruiter__c, Name' +
                ' FROM TRHC__Job__c WHERE ' +
                    whereCriteria + ' order by CreatedDate desc Limit 100';
        System.debug('Jbquery' + Jbquery);
        List<String> statuses = new List<String>();
        statuses.add('Active');
        statuses.add('Prospect');
        String status = 'Active';
        candiMatchMap = new Map<Contact, List<TRHC__job__c>>();
        /*String conQuery = 'Select id, Name, Discipline1__c, TRHC__Discipline__c From Contact'+ 
                ' Where RecordtypeId = :candidateRecortypeId' +
                    ' And TRHC__Recruiter__c = :UserInfo.getUserId()' + 
                    ' And TRHC__Candidate_Status__c = :status' +
                        ' order by CreatedDate Limit 100 Desc';
        List<Contact> contactsList = Database.query(conQuery); */            
        List<Contact> contactsList = [Select id, Name, Discipline1__c, CreatedDate, TRHC__Discipline__c From Contact Where RecordtypeId =:candidateRecortypeId And TRHC__Recruiter__c =:UserInfo.getUserId() And TRHC__Candidate_Status__c In :statuses order by CreatedDate desc Limit 100];
                
        List<TRHC__Job__c> listJobs = Database.query(Jbquery);              
        for(Contact con:contactsList){
            List<TRHC__job__c> jobs = new List<TRHC__job__c> ();
            for(TRHC__job__c job:listJobs){
                if(job.TRHC__Discipline__c == Con.TRHC__Discipline__c){
                    jobs.add(job);
                }
            }
            candiMatchMap.put(con, jobs);
        }
        
       If(candiMatchMap.keySet().Size()>0){
            render = True;
        }
        else{
            render = false;
        }
    }

}