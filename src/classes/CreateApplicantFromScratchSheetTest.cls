@isTest
public class CreateApplicantFromScratchSheetTest{

    static testMethod void applicantCreateFRMSCRTest(){

        TRHC__Healthcare_Setting__c customSetting = new TRHC__Healthcare_Setting__c ();
        customSetting.Name = 'Default';
        customSetting.TRHC__Enable_GSA_Service_to_Update_M_I_Rate__c = true;
        insert customSetting;
              

        /*FireBaseServerKeyMobile__c server  = new FireBaseServerKeyMobile__c();
        server.Name = 'Default';
        server.serverKey__c = 'AAAAr1KJK-k:APA91bFeuugxXEKJwAEKQDbo7yQ76RXAzxH0wpsGAKr0Df6C1eXItHrb6hd6HH_QsYXGgPIPQs3baLLsu6VEDRgTAa366YjGT8_Pg6mHganNtNDl1zRh289wYu6-uWpfo65azYNzqf2D';
        insert server;*/
        
        TRHC__Config__c configObj1 = new TRHC__Config__c();
        configObj1.TRHC__Name__c = 'check';
        configObj1.TRHC__Margin_Type__c = 'Expense';
        configObj1.TRHC__Frequency__c = 'Weekly';
        configObj1.TRHC__State__c =   'California';
        configObj1.TRHC__Include_in_payroll__c= True;
        configObj1.TRHC__Pay_Period_to_be_paid__c='First Check';
        configObj1.TRHC__Active__c = True;
        configObj1.TRHC__Value__c = '350.0';
        configObj1.TRHC__Grouping_Total__c ='Pay Burden';
        configObj1.TRHC__City__c = 'test';

        configObj1.RecordTypeId =[Select Id From RecordType Where SobjectType = 'TRHC__Config__c' and Name = 'Margin calculation Line Item'].Id;

        insert configObj1;

        TRHC__Config_Settings__c config = new TRHC__Config_Settings__c();
        config.ExceptConfigIds__c = configObj1.Id;
        config.Name = 'Default' ;
        config.Trigger_Enable_Applicant_From_SCR_Sheet__c = true;
        config.Update_Pay_Burden__c =true;
        config.Except_Hospitals__c = 'Atlas Development';
        config.Nurse_Gross_Weekly_Pay__c = 'Hosing Stipend,Payburden';
        insert config;

        Account account = new Account ();
        account.Name = 'chaitanya';
        insert account;
        Contact candiContact = new Contact();
        candiContact.LastName ='chaitanya';
        candiContact.RecordTypeId = [Select Id From RecordType Where SobjectType = 'contact' and Name = 'Candidate'].Id;
        //candiContact.TRHC__Account_Name__c=account.id; 
        insert candiContact;

        Contact candiContact1 = new Contact();
        candiContact1.LastName ='hiren';
        candiContact1.RecordTypeId = [Select Id From RecordType Where SobjectType = 'contact' and Name = 'Candidate'].Id;
        //candiContact.TRHC__Account_Name__c=account.id; 
        insert candiContact1;

        TRHC__Job__c job = new TRHC__Job__c();
        job.State_pic__c = 'Alaska';
        job.TRHC__County__c = 'Cook';
        job.GSA_Lodging__c = 21 ;
        job.TRHC__Stage__c = 'open';
        job.TRHC__Rate__c = '23' ;
        job.TRHC__Expected_End_Date__c = System.today() + 30;

        insert job;


        TRHC__Scratch_Sheet__c newScratchSheetTemp = new TRHC__Scratch_Sheet__c();

        newScratchSheetTemp.TRHC__County__c = 'Kalvin';

        newScratchSheetTemp.TRHC__State__c='California';
        newScratchSheetTemp.State_pic__c = 'California';
        newScratchSheetTemp.Name='Demo ScratchSheet';
        String scrtchSheetTempRecordTypeId1 = [Select Id From RecordType Where SobjectType = 'TRHC__Scratch_Sheet__c' and Name = 'Scratchsheet'].Id;
        newScratchSheetTemp.RecordTypeId=scrtchSheetTempRecordTypeId1 ;
        newScratchSheetTemp.TRHC__Candidate__c = candiContact.id;
        newScratchSheetTemp.TRHC__Status__c = 'Submitted';
        newScratchSheetTemp.Root_job__c = job.id;
        newScratchSheetTemp.IsJobApplicant__c = false;
        insert newScratchSheetTemp;

        newScratchSheetTemp.TRHC__Candidate__c  = candiContact1.id;
        Update newScratchSheetTemp;

        newScratchSheetTemp.TRHC__Candidate__c = candiContact.id;
        Update newScratchSheetTemp;


    }

}