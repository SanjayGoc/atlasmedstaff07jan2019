@isTest
Public class UpdateOverTimeRateTest {
    @isTest
    static void OverTimeRateTest() {

        TRHC__Config_Settings__c config = new TRHC__Config_Settings__c();
        config.name = 'Default';
        config.EnableUpdateOverTimeRate__c = true;
        config.Trigger_Enable_Expected_End_Date_Changer__c = false;
        insert config;

        Account account = new Account();
        account.Name = 'Test';
        account.Bill_Rate_Multiplier__c = 2.00;
        //account.Bill_Rate_Fixed_Dollar_Amount__c = 13.00;
        insert account;

        Account account1 = new Account();
        Test.startTest();
        account1.Name = 'Test';
        //account1.Bill_Rate_Multiplier__c =2.00;
        account1.Bill_Rate_Fixed_Dollar_Amount__c = 13.00;
        insert account1;

        RecordType rt = [       
            select id, Name from RecordType 
            where SobjectType = 'TRHC__Job__c' and Name = 'job'
                Limit 1];

        TRHC__job__c job = new TRHC__job__c();
        job.TRHC__Job_Title__c = 'testJob';
        job.TRHC__Account_LookUp__c = account.id;
        job.TRHC__Regular_Bill_Rate__c = 1200;
        job.RecordType = rt;
        insert job;

        job.TRHC__Account_LookUp__c = account1.id;
        update job;


        TRHC__job__c job1 = new TRHC__job__c();
        job1.TRHC__Job_Title__c = 'testJob';
        job1.TRHC__Account_LookUp__c = account1.id;
        job1.TRHC__Regular_Bill_Rate__c = 1200;
        job1.RecordType = rt;
        insert job1;


        job1.TRHC__Account_LookUp__c = account.id;
        update job1;
        Test.stopTest();

    }
}