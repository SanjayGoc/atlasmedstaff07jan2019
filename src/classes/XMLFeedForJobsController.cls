/**
* @author: Chaitanya Bollamreddi
* @date : 13 April 2018
* @description : controller for VFPage XMLFeedForJobs, we are forming xml fro all open and published jobs
*/
public with sharing class XMLFeedForJobsController {
    public String output {get; private set;}
    
    /**
    * @author: Chaitanya Bollamreddi
    * @date : 13 April 2018
    * @name : xmlFeed
    * @return : void
    * @description this will decide what jobs has to come in xml
    */
    
    public void xmlFeed() { 
        List<TRHC__Job__c> jobList = new List<TRHC__Job__c> ();
        TRHC__CustomSettingsComponent__c CC = TRHC__CustomSettingsComponent__c.getInstance('Default');
            Schema.DescribeFieldResult fieldResult = TRHC__Job__c.TRHC__Discipline__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            Schema.DescribeFieldResult fieldResultforspecility = TRHC__Job__c.speciality1__c.getDescribe();
            List<Schema.PicklistEntry> ple1 = fieldResultforspecility.getPicklistValues();
            List<String> notDisciplines = new List<String>();
            List<String> notspecialities = new List<String>();
            for( Schema.PicklistEntry f : ple)
            { 	if(f.getValue().contains('Tech')){
                notDisciplines.add(f.getValue());
            }
            }
            for( Schema.PicklistEntry f : ple1)
            { 	if(f.getValue().contains('Tech')){
                notspecialities.add(f.getValue());
            }
            }
            system.debug('notDisciplines'+notDisciplines);
            system.debug('notspecialities'+notspecialities);
            jobList = [
                SELECT Id, Name, CreatedDate, TRHC__Job_Title__c, TRHC__State__c, TRHC__Job_City__c, TRHC__Discipline__c, TRHC__Start_Date__c,
                TRHC__Job_Description_Rich__c, TRHC__Recruiter__r.Name, TRHC__Job_Postal_code__c, speciality1__c, shift_type2__c,
                TRHC__Job_Application_Tiny_URL__c, State_pic__c, TRHC__job_Type__c, TRHC__Job_Term__c
                FROM TRHC__Job__c
                WHERE TRHC__Publish__c = true
                		AND TRHC__Stage__c = 'Open' 
                		And TRHC__Discipline__c Not IN :notDisciplines 
                		AND speciality1__c Not IN :notspecialities];  
        createXML(jobList);  
    }
    /**
    * @author : chaitanya bollamreddi
    * @name : createXML
    * @parms : List<TRHC__Job__c> jobs
    * @return : void
    * desccrption : this method is used to generate an xml feed for the jobs
    */
    private void createXML(List<TRHC__Job__c> jobListFinal) {
        XmlStreamWriter w = new XmlStreamWriter();
        w.writeStartElement(null, 'source', null);
        for (TRHC__Job__c job : jobListFinal)
        {
            w.writeStartElement(null, 'job', null);
            
            if(job.CreatedDate != null){
                DateTime det = job.CreatedDate;
                String Cdate = det.month()+'/'+det.day()+'/'+det.year();
                w.writeStartElement(null, 'date', null);
                w.writeCharacters(Cdate);
                w.writeEndElement();
            }else{
                w.writeStartElement(null, 'date', null);
                w.writeCharacters('');
                w.writeEndElement();
            }
            if(job.Name != null){
                w.writeStartElement(null, 'referencenumber', null);
                w.writeCharacters(job.Name);
                w.writeEndElement();
            }else{
                w.writeStartElement(null, 'referencenumber', null);
                w.writeCharacters('');
                w.writeEndElement();
            }
            if(job.TRHC__Job_Title__c != null){
                w.writeStartElement(null, 'title', null);
                w.writeCData(job.TRHC__Job_Title__c);
                w.writeEndElement();
            }else{
                w.writeStartElement(null, 'title', null);
                w.writeCData('');
                w.writeEndElement();
            }
            if(job.TRHC__Job_City__c != null){
                w.writeStartElement(null, 'city', null);
                w.writeCharacters(job.TRHC__Job_City__c);
                w.writeEndElement();
            }
            if(job.TRHC__State__c != null){
                w.writeStartElement(null, 'state', null);
                w.writeCharacters(job.TRHC__State__c);
                w.writeEndElement();
            }else{
                w.writeStartElement(null, 'state', null);
                w.writeCharacters('');
                w.writeEndElement();
            }
            if(job.TRHC__Job_Postal_code__c != null){
                w.writeStartElement(null, 'postalcode', null);
                w.writeCharacters(job.TRHC__Job_Postal_code__c);
                w.writeEndElement();
            }else{
                w.writeStartElement(null, 'postalcode', null);
                w.writeCharacters('');
                w.writeEndElement();
            }
            /*license*/
            if(job.TRHC__Discipline__c != null){
                w.writeStartElement(null, 'license', null);
                w.writeCharacters(job.TRHC__Discipline__c);
                w.writeEndElement();  
            }else{
                w.writeStartElement(null, 'license', null);
                w.writeCharacters('');
                w.writeEndElement(); 
            }
            /*license*/
            if(job.speciality1__c != null){
                w.writeStartElement(null, 'specialty', null);
                w.writeCharacters(job.speciality1__c);
                w.writeEndElement();
            }else{
                w.writeStartElement(null, 'specialty', null);
                w.writeCharacters('');
                w.writeEndElement();
            }
            if(job.shift_type2__c != null){
                w.writeStartElement(null, 'shift', null);
                w.writeCData(job.shift_type2__c);
                w.writeEndElement();
            }
            if(job.TRHC__job_Type__c != null){
                w.writeStartElement(null, 'positiontype', null);
                w.writeCData(job.TRHC__job_Type__c);
                w.writeEndElement();
            }
            if(job.TRHC__Job_Term__c != null){
                w.writeStartElement(null, 'duration', null);
                w.writeCData(job.TRHC__Job_Term__c);
                w.writeEndElement();
            }
            if(job.TRHC__Start_Date__c != null){
                DateTime det = job.TRHC__Start_Date__c;
                String Cdate = det.month()+'/'+det.day()+'/'+det.year();
                w.writeStartElement(null, 'startdate', null);
                w.writeCharacters(Cdate);
                w.writeEndElement();
            }
            if(job.TRHC__Job_Description_Rich__c != null){
                w.writeStartElement(null, 'description', null);
                w.writeCData(job.TRHC__Job_Description_Rich__c);
                w.writeEndElement();
            }else{
                w.writeStartElement(null, 'description', null);
                w.writeCData('');
                w.writeEndElement();
            }
            w.writeEndElement();
        }
        
        w.writeEndElement(); //</source>
        
        w.writeEndDocument();
        output = w.getXmlString();
        w.close();
    }
}