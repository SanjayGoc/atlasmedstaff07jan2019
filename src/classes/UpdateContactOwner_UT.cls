/**
 * @Modified author Chaitanya bollamreddi
 * @date 11 April 2018
 * @descrptionn : No modifications pushing from another sanbox
 */
@isTest
private class UpdateContactOwner_UT {
    /**
     * @name UpdateContactOwner
     * @return void
     * @desription testmethod
     */
    @isTest static void UpdateContactOwner() {
        TRHC__config_Settings__c configSettings = new  TRHC__config_Settings__c();
        configSettings.name ='Default';
        configSettings.disable_UpdateContactOwner__c = false;
        insert configSettings;
        Id siteGuestUserId = Id.valueOf(System.Label.Site_Guest_User_Id);
        Id recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Candidate').getRecordTypeId();
        System.debug('recordTypeId'+recordTypeId);
        Contact singleContact = new Contact();
        singleContact.RecordTypeId = recordTypeId;
        singleContact.LastName = 'Shah';
        singleContact.OwnerId  = siteGuestUserId;
        insert singleContact;
    }
}