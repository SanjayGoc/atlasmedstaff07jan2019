public class HospitalsWithNoJobsController
{
    private String sortOrder = ' Name ';
    public String ascendingOrDescending = ' ASC ';
    public List<Account> pdfAcc {get;set;}
    //public List<Account> accountsNoJobs { get;set; }
	
	public List<Account> getNoJobAccounts(){
        
        set<id> jobAccIds = new set<id>();
        
        for(TRHC__Job__c job :[Select ID, TRHC__Account_Lookup__c from TRHC__Job__c WHERE (TRHC__Account_Lookup__R.VMS__C = 'Healthtrust Workforce 2.0' OR TRHC__Account_Lookup__R.VMS__C = 'Medefis/Medefis 5') AND createddate > 2017-05-01T00:00:00Z]){
           jobAccIds.add(job.TRHC__Account_Lookup__c); 
        }
       //List<Account> accountsNoJobs = [select id,Name, VMS__c From Account WHERE ID NOT IN :jobAccIds AND (VMS__C = 'Healthtrust Workforce 2.0' OR VMS__C = 'Medefis/Medefis 5')];            
       List<Account> accountsNoJobs = Database.query(
        ' SELECT Id, NAME, VMS__C ' +
        ' FROM ACCOUNT ' +
        ' WHERE ID NOT IN :jobAccIds ' +
        ' AND (VMS__C ='+ '\''+'Healthtrust Workforce 2.0'+'\''+' OR VMS__C = '+'\''+'Medefis/Medefis 5'+'\''+  
        ') ORDER BY ' + sortOrder + ascendingOrDescending +
        ' LIMIT 900'
        );       
	  return accountsNoJobs;
    }
   
    public void sortByAccName()
    {
        this.sortOrder = 'Name';
       
        if(ascendingOrDescending == ' ASC ')
        {
            ascendingOrDescending = ' DESC ';
        }
        else
        {
            ascendingOrDescending = ' ASC ';
        }   
    }
   
    public void sortByVMS()
    {
        this.sortOrder = ' VMS__c ';
       
        if(ascendingOrDescending == ' ASC ')
        {
            ascendingOrDescending = ' DESC ';
        }
        else
        {
            ascendingOrDescending = ' ASC ';
        }       
    }
    
}