@isTest
private class ExpenseLineItemControllerTest {
    public static Final String stemplaterecTypeId = Schema.SObjectType.TRHC__Scratch_Sheet__c.getRecordTypeInfosByName().get('ScratchSheet Template').getRecordTypeId();
    public static Final String configRecTypeId = Schema.SObjectType.TRHC__Config__c.getRecordTypeInfosByName().get('Margin Calculation Line Item').getRecordTypeId();

    @isTest static void test_check() {

        TRHC__Config__c newConfig = new TRHC__Config__c();
        newConfig.TRHC__Name__c = 'check';
        newConfig.TRHC__Margin_Type__c = 'Expense';
        newConfig.TRHC__Frequency__c = 'Weekly';
        newConfig.TRHC__State__c =   'California';
        newConfig.TRHC__Include_in_payroll__c= True;
        newConfig.TRHC__Pay_Period_to_be_paid__c='First Check';
        newConfig.TRHC__Active__c = True;
        newConfig.TRHC__Value__c = '350.0';
        newConfig.TRHC__Grouping_Total__c ='Pay Burden';
        newConfig.TRHC__City__c = 'test';
        
        newConfig.RecordTypeId =[Select Id From RecordType Where SobjectType = 'TRHC__Config__c' and Name = 'Margin calculation Line Item'].Id;
        
        TRHC__Healthcare_Setting__c sets = new TRHC__Healthcare_Setting__c();
        sets.name = 'Default';
        sets.TRHC__Enable_GSA_Service_to_Update_M_I_Rate__c = false;
        insert sets;
        
        insert newConfig;
        
        TRHC__Config_Settings__c newConfigSetting = new TRHC__Config_Settings__c();
        newConfigSetting.name='Default';
        newConfigSetting.Trigger_Enable_ScratchSheetAutoAddExp__c = true;
        newConfigSetting.AutoAddExpenseCriteria__c='TRHC__Margin_Type__c=\'Expense\'';
        newConfigSetting.ExceptConfigIds__c = newConfig.Id;
        newConfigSetting.Except_Hospitals__c = 'Atlas Development';
        insert newConfigSetting;
      /*  
        TRHC__Config__c newConfig = new TRHC__Config__c();
        newConfig.RecordTypeId = configRecTypeId;
        newConfig.TRHC__Name__c = 'New Config';
        newConfig.TRHC__Grouping_Total__c = 'Pay Burden';
        newConfig.TRHC__Frequency__c = 'Weekly';
        newConfig.TRHC__Calculation_parameter__c = 'Total Revenue';
        newConfig.TRHC__Value__c = '100';
        newConfig.TRHC__Margin_Type__c = 'Expense';
        newConfig.TRHC__Active__c = true;
        insert newConfig;

        TRHC__Config_Settings__c newConfigSetting = new TRHC__Config_Settings__c();
        newConfigSetting.Name = 'Default';
        newConfigSetting.Trigger_Enable_Expected_End_Date_Changer__c = true;
        newConfigSetting.Trigger_Enable_ScratchSheetAutoAddExp__c = true;
        newConfigSetting.ExceptConfigIds__c = newConfig.Id;
        newConfigSetting.Except_Hospitals__c = 'Lovelace Medical Center;Lovelace Westside Hospital;Lovelace Rehabilitation Hospital;Lovelace Regional Hospital Roswell;Test Account';
        insert newConfigSetting;*/

        Account newAccount = new Account();
        newAccount.Name = 'Test Account';
        insert newAccount;
        
        TRHC__Job__c newJob = new TRHC__Job__c();
        newJob.TRHC__Account_Lookup__c = newAccount.Id;
        newJob.TRHC__County__c = 'India';
        newJob.TRHC__State__c = 'Gujarat';
        newJob.TRHC__Job_Title__c = 'New Job';
        newJob.State_Pic__c ='Alabama';
        insert newJob;

        TRHC__Scratch_Sheet__c newScratchSheet = new TRHC__Scratch_Sheet__c();
        newScratchSheet.RecordTypeId = stemplaterecTypeId;
        newScratchSheet.Name = 'Test ScratchSheet';
        newScratchSheet.TRHC__County__c = newJob.TRHC__County__c;
        newScratchSheet.TRHC__State__c = newJob.TRHC__State__c;
        insert newScratchSheet;

        TRHC__Expense__c newExpence = new TRHC__Expense__c();
        newExpence.TRHC__Scratch_Sheet__c = newScratchSheet.Id;
        newExpence.TRHC__Grouping_Total__c = 'Pay Burden';
        newExpence.TRHC__Frequency__c = 'Per Assignment';
        insert newExpence;

        Test.startTest();
        Test.setCurrentPageReference(new PageReference('Page.ExpenseLineItem'));
        System.currentPageReference().getParameters().put('rowIn1', String.valueOf(0));
        ApexPages.StandardController standardController = new ApexPages.StandardController(newScratchSheet);
        ExpenseLineItemController expenseLineItem = new ExpenseLineItemController(standardController);
        expenseLineItem.init();
        expenseLineItem.addRow();
        expenseLineItem.saveLines();
        expenseLineItem.deleteRow();
        expenseLineItem.updateExpenseFromConfig();
        expenseLineItem.ExpenseToEdit = null;
        expenseLineItem.init();
        Test.setCurrentPageReference(new PageReference('Page.ExpenseLineItemRead'));
        System.currentPageReference().getParameters().put('rowIndex', String.valueOf(0));
        ApexPages.StandardController standardControllerNew = new ApexPages.StandardController(newScratchSheet);
        ExpenseLineItemController expenseLineItemNew = new ExpenseLineItemController(standardControllerNew);
        expenseLineItemNew.init();
        expenseLineItemNew.deleteReadRow();
        Test.stopTest();

        TRHC__Config_Settings__c soqlConfiq = [SELECT Id FROM TRHC__Config_Settings__c WHERE Name = 'Default' LIMIT 1];
        System.assertEquals(newConfigSetting.Id,soqlConfiq.Id);

        TRHC__Job__c soqlJob = [SELECT Id,TRHC__Account_Lookup__c FROM TRHC__Job__c WHERE State_Pic__c =:'Alabama' LIMIT 1];
        System.assertEquals(newJob.Id,soqlJob.Id);
        System.assertEquals(newAccount.Id,soqlJob.TRHC__Account_Lookup__c);

        TRHC__Expense__c soqlExpense = [SELECT Id,TRHC__Scratch_Sheet__c FROM TRHC__Expense__c WHERE TRHC__Scratch_Sheet__c =: newScratchSheet.Id LIMIT 1];
        System.assertEquals(newExpence.Id,soqlExpense.Id);
        System.assertEquals(newScratchSheet.Id,soqlExpense.TRHC__Scratch_Sheet__c);
    }
}