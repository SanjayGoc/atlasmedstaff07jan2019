/*
*@author : medstaff2
*@date : 27-09-2018
*Description : Json controller to generate the Json for most recent jobs 
*/
public class JsonControllerNew {
    public list<TRHC__job__c> jobList { get{
        //Fetch the Account List
        jobList = [
            SELECT Id, Name, CreatedDate, Account_Name__c,TRHC__Job_Title__c, TRHC__Overtime_Bill_Rate__c, TRHC__State__c, TRHC__Job_City__c, TRHC__Discipline__c, TRHC__Start_Date__c,TRHC__Contract_Length_weeks__c,TRHC__Street_Address__c,
            TRHC__Job_Description_Rich__c, TRHC__Recruiter__r.Name, TRHC__Job_Postal_code__c, speciality1__c, shift_type2__c, TRHC__Hours_to_be_consider_per_day__c, TRHC__Number_of_Days_Per_Week__c, TRHC__Client_Fee__c, 
            TRHC__Job_Application_Tiny_URL__c, State_pic__c, TRHC__job_Type__c, VMS__c, TRHC__No_Bill_Orientation_Hours__c, TRHC__Job_Term__c, ASSIGNMENT_LENGTH__C, TRHC__Hours_Per_Week__c, TRHC__Regular_Bill_Rate__c     
            FROM TRHC__Job__c
            WHERE TRHC__Publish__c = true
            AND TRHC__Stage__c = 'Open' Order By createddate desc Limit 800];
        
        if(jobList == null){
            jobList = new list<TRHC__JOb__c>();            
        }
        return jobList;
    } set;}   
    
    public string jsonSerial {get{
        
        if(jobList == null){
            jsonSerial = '';            
        }else{
            jsonSerial = JSON.serialize(jobList);
        }
        return jsonSerial;  
        
    } set; }
    
    public string jsonGen { get{
        
        JSONGenerator gen = JSON.createGenerator(true);
        
        if(jobList == null){
            return '';          
        }else{
            
            gen.writeStartArray();
            
            for(TRHC__Job__c job: jobList){
                
                gen.writeStartObject();
                if(job.Name != null){
                    gen.writeStringField('NAME', job.Name);  
                }else{
                    gen.writeStringField('NAME', '');   
                }
                if(job.SPECIALITY1__C != Null){
                    gen.writeStringField('SPECIALITY', job.SPECIALITY1__C);  
                }else{
                    gen.writeStringField('SPECIALITY', ''); 
                }
                if(job.TRHC__Discipline__c != Null){
                    gen.writeStringField('DISCIPLINE', job.TRHC__Discipline__c);  
                }else{
                    gen.writeStringField('DISCIPLINE', ''); 
                }
                //VMS__c
                if(job.TRHC__Client_Fee__c != Null){
                    gen.writeNumberField('VMS/MSP FEE', job.TRHC__Client_Fee__c);  
                }else{
                    gen.writeNumberField('VMS/MSP FEE', 0); 
                }
                //No Bill Orientation hours TRHC__No_Bill_Orientation_Hours__c
                if(job.TRHC__No_Bill_Orientation_Hours__c != null){
                    gen.writeNumberField('NO BILL ORIENTATION HOURS', job.TRHC__No_Bill_Orientation_Hours__c);  
                }else{
                    gen.writeNumberField('NO BILL ORIENTATION HOURS', 0);  
                }
				//TRHC__Regular_Bill_Rate__c
				
				if(job.TRHC__Regular_Bill_Rate__c != null){
                    gen.writeNumberField('REGULAR BILL RATE', job.TRHC__Regular_Bill_Rate__c);  
                }else{
                    gen.writeNumberField('REGULAR BILL RATE', 0);  
                }
                //TRHC__Overtime_Bill_Rate__c
                if(job.TRHC__Overtime_Bill_Rate__c != null){
                    gen.writeNumberField('OVERTIME BILL RATE', job.TRHC__Overtime_Bill_Rate__c);  
                }else{
                    gen.writeNumberField('OVERTIME BILL RATE', 0);  
                }
                
                if(job.TRHC__Contract_Length_weeks__c != Null){
                    gen.writeNumberField('CONTRACT LENGTH', job.TRHC__Contract_Length_weeks__c);  
                }else{
                    gen.writeNumberField('CONTRACT LENGTH', 0); 
                }
               
                if(job.TRHC__Hours_to_be_consider_per_day__c != Null){
                    gen.writeNumberField('HOURS PER DAY', job.TRHC__Hours_to_be_consider_per_day__c);  
                }else{
                    gen.writeNumberField('HOURS PER DAY', 0); 
                }
                
                if(job.TRHC__START_DATE__C != null){
                    String Cdate = job.TRHC__START_DATE__C.month() +'/'+job.TRHC__START_DATE__C.day()+'/'+job.TRHC__START_DATE__C.year();
                    gen.writeStringField('START DATE', Cdate);  
                }else{
                    gen.writeStringField('START DATE', '');
                }
                if(job.SHIFT_TYPE2__C != null){
                    gen.writeStringField('SHIFT', job.SHIFT_TYPE2__C);   
                }
                else{
                    gen.writeStringField('SHIFT', '');
                }
                if(job.TRHC__HOURS_TO_BE_CONSIDER_PER_DAY__C != null){
                    gen.writeNumberField('HOURS TO BE CONSIDER PER DAY', job.TRHC__HOURS_TO_BE_CONSIDER_PER_DAY__C);
                }else{
                    gen.writeNumberField('HOURS TO BE CONSIDER PER DAY', 0);
                }
                if(job.TRHC__NUMBER_OF_DAYS_PER_WEEK__C != null){
                    gen.writeNumberField('NUMBER OF DAYS PER WEEK', job.TRHC__NUMBER_OF_DAYS_PER_WEEK__C);  
                }else{
                    gen.writeNumberField('NUMBER OF DAYS PER WEEK', 0);  
                }
                if(job.TRHC__HOURS_PER_WEEK__C != null){
                    gen.writeNumberField('HOURS PER WEEK', job.TRHC__HOURS_PER_WEEK__C);
                }else{
                    gen.writeNumberField('HOURS PER WEEK', 0);
                }
                if(job.TRHC__STATE__C != null){
                    gen.writeStringField('STATE', job.TRHC__STATE__C);  
                }else{
                    gen.writeStringField('STATE', ''); 
                }
                if(job.TRHC__JOB_CITY__C == null)
                    gen.writeStringField('JOB CITY', '');
                else{
                    gen.writeStringField('JOB CITY',job.TRHC__JOB_CITY__C); 
                }
                if(job.TRHC__Job_Description_Rich__c != null){
                    gen.writeStringField('Description', job.TRHC__Job_Description_Rich__c);  
                }else{
                    gen.writeStringField('DESCRIPTION', ''); 
                }
                gen.writeEndObject();
                
            }
            
            gen.writeEndArray();
            return gen.getAsString();
        }
        
    } set; } 
}