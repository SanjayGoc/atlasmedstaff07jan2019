@isTest
public class CommunityUpdateProfiAddrRESTTest {
    public static testmethod void joblisrtest(){
        TRHC__Config_Settings__c configSettings = new TRHC__Config_Settings__c();
        configSettings.name='Default';
        configSettings.Enable_AutoMateJobRates__c = true;
        configSettings.Trigger_Enable_ScratchSheetAutoAddExp__c = false;
        configSettings.AutoAddExpenseCriteria__c='TRHC__Margin_Type__c=\'Expense\'';
        configSettings.Except_Hospitals__c = 'Atlas Development';
        configSettings.Trigger_Enable_Expected_End_Date_Changer__c = false;
        configSettings.Trigger_Enable_Applicant_From_SCR_Sheet__c = true;
        configSettings.Update_Pay_Burden__c = false;
        configSettings.Enable_RefferalTrigger__c = true;
        insert configSettings;

        List<Account> accountList = TestUtilityClass.getTestMultipleAccount();
        contact conobj = new contact();
        conobj.lastName = 'sharma';
        conobj.TRHC__Password__c = '12345678';
        conobj.Email = 'HIREN.GOC@GMail.com';
        conobj.Permanant_Address__c = '11227 Franklin Plaza Apt 1621';
        conobj.TRHC__Permanent_Address_2__c = 'Sardar choea';
        conobj.TRHC__Permanent_City__c = 'Elk Grove';
        conobj.State_pic__c = 'Africa';
        conobj.TRHC__Permanent_Zip_Postal_Code__c = '95624';
        conobj.Temporary_Address_1__c = '11227 Franklin Plaza Apt 1621';
        conobj.Temporary_Address_2__c = 'ardar choea';
        conobj.TRHC__Temporary_City__c = 'ahd';
        conobj.temporary_state__c = 'Arkansas';
        conobj.TRHC__Temporary_Zip_Postal_Code__c = 'tard';
        conobj.apiToken__c = '1235464879798798456';
        insert conobj;
      
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/mobile/UpdateProfileAddress';
        req.addParameter('apiToken', conobj.apiToken__c);
        req.addParameter('perAddress1', conobj.Temporary_Address_1__c);
        req.addParameter('perAddress2', conobj.TRHC__Permanent_Address_2__c);
        req.addParameter('perCity', conobj.TRHC__Permanent_City__c);
        req.addParameter('perState', conobj.State_pic__c);
        req.addParameter('perZipcode', conobj.TRHC__Permanent_Zip_Postal_Code__c);
        req.addParameter('tmpAddress1', conobj.Temporary_Address_1__c);
        req.addParameter('tmpAddress2', conobj.Temporary_Address_2__c);
        req.addParameter('tmpCity', conobj.TRHC__Temporary_City__c);
        req.addParameter('tmpState', conobj.temporary_state__c);
        req.addParameter('tmpZipcode', conobj.TRHC__Temporary_Zip_Postal_Code__c);
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;

        CommunityUpdateProfiAddrRESTOperation.ResponseHandler wrapperobj = CommunityUpdateProfiAddrRESTOperation.CommunityUpdateProfiAddrRESTOperation();
    }

    public static testmethod void joblisrtest1(){
        TRHC__Config_Settings__c configSettings = new TRHC__Config_Settings__c();
        configSettings.name='Default';
        configSettings.Enable_AutoMateJobRates__c = true;
        configSettings.Trigger_Enable_ScratchSheetAutoAddExp__c = false;
        configSettings.AutoAddExpenseCriteria__c='TRHC__Margin_Type__c=\'Expense\'';
        configSettings.Except_Hospitals__c = 'Atlas Development';
        configSettings.Trigger_Enable_Expected_End_Date_Changer__c = false;
        configSettings.Trigger_Enable_Applicant_From_SCR_Sheet__c = true;
        configSettings.Update_Pay_Burden__c = false;
        configSettings.Enable_RefferalTrigger__c = true;
        insert configSettings;

        List<Account> accountList = TestUtilityClass.getTestMultipleAccount();
        contact conobj = new contact();
        conobj.lastName = 'sharma';
        conobj.TRHC__Password__c = '12345678';
        conobj.Email = 'HIREN.GOC@GMail.com';
        conobj.Permanant_Address__c = '11227 Franklin Plaza Apt 1621';
        conobj.TRHC__Permanent_Address_2__c = 'Sardar choea';
        conobj.TRHC__Permanent_City__c = 'Elk Grove';
        conobj.State_pic__c = 'Africa';
        conobj.TRHC__Permanent_Zip_Postal_Code__c = '95624';
        conobj.Temporary_Address_1__c = '11227 Franklin Plaza Apt 1621';
        conobj.Temporary_Address_2__c = 'ardar choea';
        conobj.TRHC__Temporary_City__c = 'ahd';
        conobj.temporary_state__c = 'Arkansas';
        conobj.TRHC__Temporary_Zip_Postal_Code__c = 'tard';
        conobj.apiToken__c = '1235464879798798456';
        insert conobj;
      
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/mobile/UpdateProfileAddress';
        req.addParameter('apiToken', '23232231321321');
        req.addParameter('perAddress1', conobj.Temporary_Address_1__c);
        req.addParameter('perAddress2', conobj.TRHC__Permanent_Address_2__c);
        req.addParameter('perCity', conobj.TRHC__Permanent_City__c);
        req.addParameter('perState', conobj.State_pic__c);
        req.addParameter('perZipcode', conobj.TRHC__Permanent_Zip_Postal_Code__c);
        req.addParameter('tmpAddress1', conobj.Temporary_Address_1__c);
        req.addParameter('tmpAddress2', conobj.Temporary_Address_2__c);
        req.addParameter('tmpCity', conobj.TRHC__Temporary_City__c);
        req.addParameter('tmpState', conobj.temporary_state__c);
        req.addParameter('tmpZipcode', conobj.TRHC__Temporary_Zip_Postal_Code__c);
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;

        CommunityUpdateProfiAddrRESTOperation.ResponseHandler wrapperobj = CommunityUpdateProfiAddrRESTOperation.CommunityUpdateProfiAddrRESTOperation();
    }
}