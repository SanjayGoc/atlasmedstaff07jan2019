@isTest
public with sharing class ProphecyUpdateAssessmentResultsBatch_TC {
    public testmethod static void testAssessment() {
    Test.startTest();
    TRHC__Config_Settings__c contactSetting = new TRHC__Config_Settings__c ();
        contactSetting.Enable_RecruitersMailNotificationTrigger__c = true;
        contactSetting.Name= 'Default';
    insert contactSetting;
    ProphecyUpdateAssessmentResultsBatch batch = 
      new ProphecyUpdateAssessmentResultsBatch ('TRHC__Assessment__c');
    
    contact v_contact = NEW contact();
    v_contact.recordtypeid = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Candidate').getRecordTypeId();
    v_contact.lastname = 'test';
    v_contact.TRHC__Prophecy_Caregiver_ID__c = NULL;
    v_contact.Email = 'sai@yahoo.com';
    v_contact.firstname = 'test';
    v_contact.TRHC__Prophecy_Caregiver_ID__c = '123456';
    insert v_contact;
    List <TRHC__Assessment__c> assesmentsList = NEW List <TRHC__Assessment__c> ();
    
    TRHC__Assessment__c assement =  NEW TRHC__Assessment__c ();
    assement.TRHC__Assessment_Name__c = 'Test';
    assement.TRHC__Total_Questions__c = 5;
    assement.TRHC__Assessment_Type__c = 'Test';
    assement.TRHC__Contact__c = v_contact.Id;
    assement.TRHC__Caregiver_Assessment_ID__c = 123456;
    assesmentsList.add(assement);
    
    TRHC__Assessment__c assement1 =  NEW TRHC__Assessment__c ();
    assement1.TRHC__Assessment_Name__c = 'Test';
    assement1.TRHC__Total_Questions__c = 5;
    assement1.TRHC__Assessment_Type__c = 'Checklist';
    assement1.TRHC__Contact__c = v_contact.Id;
    assement1.TRHC__Caregiver_Assessment_ID__c = 12345678;
    assement1.TRHC__Score__c = 10;
    assesmentsList.add(assement1);
    
    insert assesmentsList;
    
    TRHC__Skill_Answers__c answerRecord = NEW TRHC__Skill_Answers__c ();
    answerRecord.TRHC__Assessment_Type__c = 'Checklist';
    answerRecord.TRHC__Skill_Checklist_Name__c = 'Test';
    answerRecord.TRHC__Total_Questions__c = 6;
    answerRecord.TRHC__Contact__c = v_contact.Id;
    answerRecord.TRHC__Caregiver_Assessment_Id__c = '12345678';
    insert answerRecord;
    
    TRHC__Prophecy_Settings__c prop = new TRHC__Prophecy_Settings__c();
    prop.TRHC__Security_Code__c = 'BF321F48-985D-457E-9D9B-2A4000CAE539';
    prop.TRHC__Prophecy_TimeZone_Add_Minute__c = -4;
    insert prop;
    
   // ProphecyUpdateResults updateResult = NEW ProphecyUpdateResults();
   // ProphecyUpdateResults.sendRequestToProphecy ('5465-54sdfs-545-sfs2d',8745,'Test');
    
    Database.BatchableContext BC = null;
    batch.v_ProphecyCustomSettings = prop;
    
    Database.executeBatch(batch, 1);
    batch.execute(BC, assesmentsList);
    
    SchedulableContext sc = null;
    
    ProphecyUpdateResultsSchedule schedule = new ProphecyUpdateResultsSchedule();
    schedule.execute(sc);
    
    ProphecyUpdateResultsSchedule skillsSchedule = new ProphecyUpdateResultsSchedule();
    skillsSchedule.execute(sc);
    Test.stopTest();
    
  }
  
  public testmethod static void testSkillResults() {
    Test.startTest();
    TRHC__Config_Settings__c contactSetting = new TRHC__Config_Settings__c ();
        contactSetting.Enable_RecruitersMailNotificationTrigger__c = true;
        contactSetting.Name= 'Default';
    insert contactSetting;
    ProphecyUpdateAssessmentResultsBatch batch = 
      new ProphecyUpdateAssessmentResultsBatch ('TRHC__Skill_Answers__c');
    
    contact v_contact = NEW contact();
    v_contact.recordtypeid = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Candidate').getRecordTypeId();
    v_contact.lastname = 'test';
    v_contact.TRHC__Prophecy_Caregiver_ID__c = NULL;
    v_contact.Email = 'sai@yahoo.com';
    v_contact.firstname = 'test';
    v_contact.TRHC__Prophecy_Caregiver_ID__c = '123456';
    insert v_contact;
    List <TRHC__Assessment__c> assesmentsList = NEW List <TRHC__Assessment__c> ();
    
    TRHC__Assessment__c assement =  NEW TRHC__Assessment__c ();
    assement.TRHC__Assessment_Name__c = 'Test';
    assement.TRHC__Total_Questions__c = 5;
    assement.TRHC__Assessment_Type__c = 'Test';
    assement.TRHC__Contact__c = v_contact.Id;
    assement.TRHC__Caregiver_Assessment_ID__c = 123456;
    assesmentsList.add(assement);
    
    TRHC__Assessment__c assement1 =  NEW TRHC__Assessment__c ();
    assement1.TRHC__Assessment_Name__c = 'Test';
    assement1.TRHC__Total_Questions__c = 5;
    assement1.TRHC__Assessment_Type__c = 'Checklist';
    assement1.TRHC__Contact__c = v_contact.Id;
    assement1.TRHC__Caregiver_Assessment_ID__c = 12345678;
    assement1.TRHC__Score__c = 10;
    assesmentsList.add(assement1);
    
    insert assesmentsList;
    
    List <TRHC__Skill_Answers__c> skillAnswersList = NEW List <TRHC__Skill_Answers__c> ();
    TRHC__Skill_Answers__c answerRecord = NEW TRHC__Skill_Answers__c ();
    answerRecord.TRHC__Assessment_Type__c = 'Checklist';
    answerRecord.TRHC__Skill_Checklist_Name__c = 'Test';
    answerRecord.TRHC__Total_Questions__c = 6;
    answerRecord.TRHC__Contact__c = v_contact.Id;
    answerRecord.TRHC__Overall_Comprehension__c = '1.0';
    answerRecord.TRHC__Caregiver_Assessment_Id__c = '12345678';
    skillAnswersList.add (answerRecord);
    insert skillAnswersList;
    
    TRHC__Prophecy_Settings__c prop = new TRHC__Prophecy_Settings__c();
    prop.TRHC__Security_Code__c = 'BF321F48-985D-457E-9D9B-2A4000CAE539';
    prop.TRHC__Prophecy_TimeZone_Add_Minute__c = -4;
    insert prop;
    
    //ProphecyUpdateResults updateResult = NEW ProphecyUpdateResults();
   // ProphecyUpdateResults.sendRequestToProphecy ('5465-54sdfs-545-sfs2d',8745,'Test');
    
    Database.BatchableContext BC = null;
    batch.v_ProphecyCustomSettings = prop;
    
    Database.executeBatch(batch, 1);
    batch.execute(BC, skillAnswersList);
    
    SchedulableContext sc = null;
    
    ProphecyUpdateResultsSchedule schedule = new ProphecyUpdateResultsSchedule();
    schedule.execute(sc);
    
    ProphecyUpdateResultsSchedule skillsSchedule = new ProphecyUpdateResultsSchedule();
    skillsSchedule.execute(sc);
    
    ProphecyWebService.ProphecyConnectXML_cfc webserv = new ProphecyWebService.ProphecyConnectXML_cfc();
    new ProphecyWebService.GetResultsByCaregiverAssessmentIDResponse_element ();
    webserv.GetResultsByCaregiverAssessmentID('test',double.valueOf(10000.0),'test');
    
    Test.stopTest();
    
  }
}