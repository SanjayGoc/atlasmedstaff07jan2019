global class SkillCommUserToChangeCurrentUser implements Database.Batchable<sObject> {
    
    
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = '';
        
        if(!Test.isRunningTest()){
            DateTime dateTimes = System.today().addDays(-7);
            dateTimes = dateTimes.addHours(23).addMinutes(59).addSeconds(59);
            query = 'SELECT Id, Name,isUpdated__c From TRHC__Skill_Answers__c where TRHC__Results_URL__c = null And PDF_URL__c = null AND TRHC__Overall_Comprehension__c = null AND CreatedDate >=: dateTimes';
        }else{
            query = 'SELECT Id, Name,isUpdated__c From TRHC__Skill_Answers__c limit 1';
            
        }
        return DataBase.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List <sObject> scope) {
        
        
        List<TRHC__Skill_Answers__c> updateList = new List<TRHC__Skill_Answers__c>();
        for(TRHC__Skill_Answers__c singleObject : (List<TRHC__Skill_Answers__c>) scope) {
            if(singleObject.isUpdated__c == true)
                singleObject.isUpdated__c = false;
            else
                singleObject.isUpdated__c = true;
                singleObject.Description__c = 'going in else condition record not get pdf url';
            updateList.add(singleObject);
        }
        if(!updateList.isEmpty()) {
            update updateList;
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        ProphecyUpdateAssessmentResultsBatch obj = new ProphecyUpdateAssessmentResultsBatch('TRHC__Skill_Answers__c'); 
        ProphecyUpdateAssessmentResultsBatch obj1 = new ProphecyUpdateAssessmentResultsBatch('TRHC__Assessment__c');
        if(!Test.isRunningTest()){
            database.executebatch(obj, 1);
            database.executebatch(obj1, 1);
        }
        
        
        
        
    }
}