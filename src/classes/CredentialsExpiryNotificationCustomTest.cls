@isTest 
public class CredentialsExpiryNotificationCustomTest
{
    @isTest
    static void testMethod1() 
    {
     
        
            TRHC__CredentialsCustomSettings__c sett= new TRHC__CredentialsCustomSettings__c();
            sett.Name='Default';
            sett.TRHC__Disable_CredAnswerCopyAttachmentTrigger__c = TRUE;
            insert sett;
            TRHC__Config_Settings__c conf = new TRHC__Config_Settings__c ();
            conf.Name = 'Default';
            conf.Enable_Update_Recruiter__c = false;
            insert conf;
            
            Contact con = new Contact ();
            con.lastName ='chaitanya';
            con.Email ='chaithu@gmail.com';
            con.Recruiters__c = contact.Recruiters__c.getDescribe().getPicklistValues()[0].getValue();
            con.TRHC__Candidate_Status__c = 'Active';
            insert con;
            
            TRHC__CredentialQuestion__c crMaster = new TRHC__CredentialQuestion__c();
            crMaster.TRHC__Credential_Master_Name__c ='Test';
            crMaster.TRHC__Credential_Name_Portal__c = 'Test';
            insert crMaster;
            List<RecordType> recType = [Select Id, name From RecordType  Where SobjectType = 'TRHC__Credentials__c'];
            Id rId = recType[0].Id;
            TRHC__Credentials__c cred = new TRHC__Credentials__c();
            cred.TRHC__Credential_Master__c = crMaster.id;
            cred.RecordTypeId = rId ;
            cred.TRHC__Contact__c = con.id;
            cred.TRHC__Expiration_Date__c = Date.today()+30;
            insert cred;
            
            //List<TRHC__Skill_Answers__c> Klist = [SELECT TRHC__Contact__c, Expiry_date__c ,TRHC__Skill_Answers__c.TRHC__Contact__r.id , TRHC__Skill_Answers__c.TRHC__Contact__r.Email, Expiration_Notification_Sent__c FROM TRHC__Skill_Answers__c  WHERE TRHC__Skill_Checklist_Name__c = 'TestSkill']; 
            Test.startTest();
            CredentialsExpiryNotificationCustom obj = new CredentialsExpiryNotificationCustom();
            Date expiryNotificationDate = Date.today()+30; 
            obj.query = 'SELECT TRHC__Contact__c, TRHC__Expiration_Date__c, TRHC__Credentials__c.TRHC__Contact__r.TRHC__Recruiter__c FROM TRHC__Credentials__c  WHERE TRHC__Expiration_Date__c= :expiryNotificationDate';
            Database.executeBatch(obj);
            Test.stopTest();
    }
}