/**
 * @author Ranjeeth
 * @ Modified By : Hari
 * @ date 14/12/2012
 * @ date 27/01/2016
 * @description Class :This Class is used to get the Xml tree Related to Educational and Employment 
                        History object details and make xml tree and passed to the Page.
 */

Public class BackGroundCheckV2{
    public contact contact { get; set; } 
    public String finalXML { get; set; } 
    public String birthDate { get; set; }
    public TRHC__Config_Settings__c  configSettings { get; set; }
    public TRHC__Universals_Background_Check__c universalBackGroundCheckCustomSetting { get; set; } 
    public boolean readyForRequest { get; set; }
    public integer year;
    public integer expYear ;
    public List<string>  listOfIds {get;set;}
    public boolean credentialRequest {get ;set;} 
    // Condtructor intialization
    public BackGroundCheckV2(ApexPages.StandardController controller) {
        //Constructor body
    }
    //Method to constructs XML tree
    public void init() {
        credentialRequest = false;
        String UserId = UserInfo.getUserId();
        // Returns the contact details of a user
        contact = [
                SELECT name, Firstname, Lastname, Birthdate, TRHC__Referral_Relationship__c,
                        Phone, Email, SSN_Text__c, mailingstreet, mailingcity, Salutation, createddate, 
                        mailingstate, MailingCountry, MailingPostalCode,OtherCity , OtherCountry,
                        OtherPhone, OtherState, OtherStreet, OtherPostalCode,TRHC__Middle_Name__c,  
                        TRHC__Driver_Licence_State__c, TRHC__Driver_Licence_Number__c
                FROM Contact
                WHERE id =: ApexPages.currentpage().getparameters().get('Id')];
        // Converting date of birth into MMddyyyy format
        if (contact.birthDate != null) {
            birthDate = string.valueOf(dateTime.newinstance(contact.Birthdate.year(),
                                            contact.Birthdate.month(),
                                                    contact.Birthdate.day()).format('MMddyyyy'));
        }
        universalBackGroundCheckCustomSetting = TRHC__Universals_Background_Check__c.getInstance(UserId);
        configSettings = TRHC__Config_Settings__c.getInstance(); 
        list<TRHC__Config_Settings__c> AVTRRT = TRHC__Config_Settings__c.getall().values();
        
        for (integer i = 0; i < AVTRRT.Size(); i++) {
            if (AVTRRT[i].TRHC__Enable_Universal_Background_Check__c == true) {
                readyForRequest = true;
            }
        }
        list<TRHC__Credential_Verifications__c> credentialVerificationList = null;
        string queryForVerificationCredentials = '';
        if (ApexPages.currentPage().getParameters().get('list') != null) {
            CredentialRequest = true;
            string crendentialsID = ApexPages.currentPage().getParameters().get( 'list');
            listOfIds = new List<string>();
            listOfIds = crendentialsID.split(',', 0);
            queryForVerificationCredentials = 
                            ' SELECT name, TRHC__Credential_Name__c, TRHC__Object_Name__c, TRHC__Where_Clause__c, ' +
                                    ' TRHC__Root_Tag__c, TRHC__Root_Element__c ' +
                            ' FROM TRHC__Credential_Verifications__c ' +
                            ' WHERE TRHC__Object_Name__c = \'TRHC__Credentials__c\'';
        } else {
            queryForVerificationCredentials = 
                            ' SELECT name, TRHC__Credential_Name__c, TRHC__Object_Name__c, TRHC__Where_Clause__c, ' +
                                    ' TRHC__Root_Tag__c, TRHC__Root_Element__c ' +
                            ' FROM TRHC__Credential_Verifications__c';
        }
        credentialVerificationList = Database.query(queryForVerificationCredentials);
        finalXML = '';
        
        for ( integer i = 0; i < credentialVerificationList.size(); i++) {
            String innerXML = '';
            String queryForFieldName = '';
            queryForFieldName = 
                            ' SELECT name, Field_Name__c ' +
                            ' FROM '+credentialVerificationList[i].TRHC__Credential_Name__c;
            List<sobject> FieldList = getResults(queryForFieldName);
            Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
            Map <String, Schema.SObjectField> fieldMap = 
                                        schemaMap.get(credentialVerificationList[i].TRHC__Object_Name__c)
                                            .getDescribe().fields.getMap();
            string contactRelatedObjQuery = '';
            for (Schema.SObjectField sfield : fieldMap.Values()) {
                schema.describefieldresult dfield = sfield.getDescribe();
                if (contactRelatedObjQuery == '') {
                    contactRelatedObjQuery += string.valueOf(dfield.getname());
                } else {
                    contactRelatedObjQuery += ','+string.valueOf(dfield.getname());
                }
            }
            contactRelatedObjQuery = 
                                    ' SELECT '+contactRelatedObjQuery +
                                    ' FROM '+credentialVerificationList[i].TRHC__Object_Name__c +
                                    ' WHERE '+credentialVerificationList[i].TRHC__Where_Clause__c +'=';
            if (credentialVerificationList[i].TRHC__Where_Clause__c == 'CRDNTLS__Contact__c') {
                contactRelatedObjQuery += '\''+contact.id+'\'';
                if (ApexPages.currentPage().getParameters().get('list') != null) {
                    contactRelatedObjQuery += ' and id in :listOfIds';
                }
            } else {
                contactRelatedObjQuery += '\''+ApexPages.currentpage().getparameters().get('Id')+'\'';
            }
            List<sobject> credentialList=getResults(contactRelatedObjQuery);
            innerXML += '<'+credentialVerificationList[i].TRHC__Root_Tag__c+'>';
            for (sobject IndvCredential:credentialList) {
                innerXML += '<'+credentialVerificationList[i].TRHC__Root_Element__c+'>';
                for (sobject tagWithFieldName:FieldList) {
                String tagValue = '';
                    if (String.valueOf( tagWithFieldName.get('name')).contains('=')) {
                        if (String.valueOf(IndvCredential.get(String.valueOf(
                                    tagWithFieldName.get('Field_Name__c')))) != null && 
                                        !String.valueOf(tagWithFieldName.get('name')).contains('YYYY')) {
                            tagValue = String.valueOf(
                                IndvCredential.get(String.valueOf(tagWithFieldName.get('Field_Name__c'))));
                            if(tagValue == 'null')
                                tagValue = '';
                        } else if (String.valueOf(
                                        IndvCredential.get(String.valueOf(tagWithFieldName.get('Field_Name__c')))) != null && 
                                            String.valueOf(tagWithFieldName.get('name')).contains('YYYY')) {
                            tagValue = String.valueOf(IndvCredential
                                            .get(String.valueOf(tagWithFieldName.get('Field_Name__c')))).split('-')[1]
                                            + '/' +String.valueOf(IndvCredential
                                            .get(String.valueOf(tagWithFieldName.get('Field_Name__c')))).split('-')[0];
                            if (tagValue == 'null')
                                TagValue = '';
                        }
                        innerXML += '<'+string.valueOf( tagWithFieldName.get('name') )+'>' + tagValue +
                                    '</'+string.valueOf( tagWithFieldName.get('name') ).split(' ')[0]+'>';
                    } else {
                        string combinedSTR = (string.valueOf(tagWithFieldName.get('Field_Name__c'))).replaceAll('\\+',' +');
                        if (combinedSTR.contains('+')) {
                            string combinedValue = '';
                            for (Integer s=0; s<combinedSTR.split('\\+').size(); s++) {
                                if (combinedSTR.split('\\+')[s].contains(',') || combinedSTR.split('\\+')[s].contains('-')) {
                                    combinedValue +=String.valueOf(combinedSTR.split('\\+')[s]).trim();
                                } else {
                                    if (String.valueOf(IndvCredential.get((combinedSTR.split('\\+')[s]).trim())) != null)
                                        combinedValue +=' '+ String.valueOf(IndvCredential.get((combinedSTR.split('\\+')[s]).trim()));
                                }
                            }
                            innerXML += '<'+String.valueOf( tagWithFieldName.get('name') )+ '>' + 
                                                combinedValue+'</'+String.valueOf( tagWithFieldName.get('name') )+'>';
                        } else {
                            if(String.valueOf(IndvCredential.get(String.valueOf(tagWithFieldName.get('Field_Name__c')))) != null) {
                                tagValue = String.valueOf(IndvCredential.get(String.valueOf(tagWithFieldName.get('Field_Name__c'))));
                            }
                            if(tagValue == 'null') {
                                tagValue = '';
                            }
                            innerXML += '<'+String.valueOf( tagWithFieldName.get('name') )+'>' + 
                                TagValue+'</'+String.valueOf( tagWithFieldName.get('name') )+'>';
                        }
                    }
                }
                if ((credentialVerificationList[i].TRHC__Root_Element__c).contains('='))
                    innerXML += '</'+(credentialVerificationList[i].TRHC__Root_Element__c).split(' ')[0]+'>';
                else
                    innerXML += '</'+(credentialVerificationList[i].TRHC__Root_Element__c)+'>';
                }
                System.debug('ssssssssssss'+credentialVerificationList[i].TRHC__Root_Tag__c);
            if ((credentialVerificationList[i].TRHC__Root_Tag__c).contains('=')) {
                innerXML += '</'+(credentialVerificationList[i].TRHC__Root_Tag__c).split(' ')[0]+'>';
            } else {
                innerXML += '</'+credentialVerificationList[i].TRHC__Root_Tag__c+'>';
            }
            finalXML = finalXML + innerXML;
        }
        finalXML = finalXML;
        System.debug(':::::FinalXML::::::: '+finalXML);
    }
    //Returns the query results
    public List<sobject> getResults (String queryString) {
        System.debug('queryStringqueryString'+queryString);
        return Database.query(queryString); 
    }
}