@isTest
private class SendRelevantJobBatchNew_TC {
    private static testMethod void myMethod () {
        
        Account account = new Account();
        account.name = 'TestABC';
        insert account;
        
        TRHC__Config_Settings__c conf = new TRHC__Config_Settings__c (Name = 'Default');
        conf.Enable_JobAlertDelimiterModifier__c = False;
        conf.EnableUpdateOverTimeRate__c = False;
        conf.Trigger_Enable_Expected_End_Date_Changer__c =False;
        insert conf;
        
        Id rectypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Candidate').getRecordTypeId();
        LIST<Contact> contactList = NEW  LIST<Contact> ();
        Contact testConact = NEW Contact ();
        testConact.lastname = 'test';
        testConact.FirstName = '1';
        testConact.RecordTypeId = rectypeId;
        testConact.Email = 'test@test.com';
        contactList.add(testConact);
        
        Contact testConact1 = NEW Contact ();
        testConact1.lastname = 'test1';
        testConact1.FirstName = '2';
        testConact1.RecordTypeId = rectypeId;
        testConact1.Email = 'test1@test.com';
        contactList.add(testConact1);
        insert contactList;
        
        LIST<TRHC__Job_Search_Agent__c> jobAlerts = NEW LIST<TRHC__Job_Search_Agent__c> ();
        TRHC__Job_Search_Agent__c testJobAlert = NEW TRHC__Job_Search_Agent__c ();
        testJobAlert.TRHC__Contact__c = testConact1.Id;
        testJobAlert.TRHC__Search_Criteria__c = 'Discipline : CNA, Specialty : Acute Care, City : Bangalore, State : KA,';
        jobAlerts.add(testJobAlert);
        
        TRHC__Job_Search_Agent__c testJobAlert1 = NEW TRHC__Job_Search_Agent__c ();
        testJobAlert1.TRHC__Contact__c = testConact1.Id;
        testJobAlert1.TRHC__Search_Criteria__c = 'Discipline : CNA, Specialty : Acute Care, City : Bangalore, State : Test,';
        jobAlerts.add(testJobAlert1);
        TRHC__Job_Search_Agent__c testJobAlert2 = NEW TRHC__Job_Search_Agent__c ();
        testJobAlert2.TRHC__Contact__c = testConact.Id;
        testJobAlert2.TRHC__Search_Criteria__c = 'Discipline : CNA, Specialty : Acute Care, City : Bangalore, State : KA,';
        jobAlerts.add(testJobAlert2);
        insert       jobAlerts;
        
        LIST<TRHC__Job__c> jobs = NEW LIST<TRHC__Job__c> ();
        TRHC__Job__c testJob  = NEW TRHC__Job__c ();
        testJob.TRHC__Job_Title__c = 'test';
        testJob.TRHC__Discipline__c = 'CNA';
        testJob.TRHC__Specialty__c = 'Acute Care';
        testJob.TRHC__Job_City__c = 'Bangalore';
        testJob.TRHC__State__c = 'KA';
        testJob.TRHC__Account_Lookup__c = account.Id;
        jobs.add(testJob);
        
        TRHC__Job__c testJob1  = NEW TRHC__Job__c ();
        testJob1.TRHC__Job_Title__c = 'test';
        testJob1.TRHC__Discipline__c = 'CNA';
        testJob1.TRHC__Specialty__c = 'Acute Care';
        testJob1.TRHC__Job_City__c = 'Bangalore';
        testJob1.TRHC__State__c = 'Test';
        testJob1.TRHC__Account_Lookup__c = account.id;
        jobs.add(testJob1);
        insert jobs;
        test.startTest();
        
        Database.QueryLocator QL;
        Database.BatchableContext BC;
        SendRelevantJobBatchNew batch = NEW SendRelevantJobBatchNew ();
        batch.start(BC);
        batch.execute(BC,contactList);
        batch.finish(BC);
        Helper_Email.getAllFields('TRHC__Job__c');
        String s = '<html><body> <table style="width: 100%;"><tr><td><div id="header" style="height: 50px; background-color: #9a3232;">'+
            '<div id="title" style="font-family: Tahoma; color: white; padding: 20px 15px;">Job Alert</div> </div></td>'+
            '</tr><tr><td> <div id="content" style="margin: 20px 0;"><repeat> <div class="repeat" style="margin:10px; padding:10px; background-color:#edf0f3; border:1px solid #98adc8; font-family:Tahoma; font-size:12px">'+
            '<div><b>Job Title:</b> {!TRHC__Job__c.TRHC__Job_Title__c}</div>'+
            '<div><b>City:</b> {!TRHC__Job__c.TRHC__Job_City__c}</div>'+
            '<div><b>Expected Start Date:</b> {!TRHC__Job__c.TRHC__Start_Date__c}</div>'+
            '<div><b>Job Country:</b> {!TRHC__Job__c.TRHC__Country_Locale__c}</div>'+
            '<div><b>Job Category:</b> {!TRHC__Job__c.TRHC__Job_Category__c}</div>'+
            '<div><b>Job Application Tiny URL:</b> {!TRHC__Job__c.TRHC__Job_Application_Tiny_URL__c}</div>'+
            '<div><b>Job Description Rich:</b> {!TRHC__Job__c.TRHC__Job_Description_Rich__c}</div>'+
            '</div></repeat></div></td></tr><tr><td><div id="footer" style="height: 25px; background-color: #9a3232;"></div></td></tr></table></body></html>';
        
        SET<TRHC__Job__c> jobsSet = NEW SET<TRHC__Job__c> ();
        jobsSet.addAll(jobs);
        Helper_Email.getEmailBody(jobsSet,s);
        JobSearchHelper.getJobs('Specialty : Acute Care,','',0,'','');
        JobSearchHelper.getJobs('Specialty : Acute Care, Discipline : CNA,','',0,'','Daily');
        ScheduleSendRelevantJobBatch scheduleBatch = NEW ScheduleSendRelevantJobBatch ();
        SchedulableContext SC;
        scheduleBatch.execute(SC);
        //String sch = '0 0 23 * * ?'; 
        //system.schedule('Test ', sch, scheduleBatch); 
        test.stopTest();
        
    }
    
}