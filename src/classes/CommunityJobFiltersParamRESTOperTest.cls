@isTest
Public class CommunityJobFiltersParamRESTOperTest {
    public static testmethod void jobFiltertest(){
        TRHC__Config_Settings__c configSettings = new TRHC__Config_Settings__c();
        configSettings.name='Default';
        configSettings.Enable_AutoMateJobRates__c = true;
        configSettings.Trigger_Enable_ScratchSheetAutoAddExp__c = false;
        configSettings.AutoAddExpenseCriteria__c='TRHC__Margin_Type__c=\'Expense\'';
        configSettings.Except_Hospitals__c = 'Atlas Development';
        configSettings.Trigger_Enable_Expected_End_Date_Changer__c = false;
        configSettings.Trigger_Enable_Applicant_From_SCR_Sheet__c = true;
        configSettings.Update_Pay_Burden__c = false;
        configSettings.Enable_RefferalTrigger__c = true;
        insert configSettings;

        List<Account> accountList = TestUtilityClass.getTestMultipleAccount();
        contact conobj = new contact();
        conobj.lastName = 'sharma';
        conobj.apiToken__c = '1234579897456413';
        insert conobj;

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/mobile/JobFiltersParameters';
        req.addParameter('apiToken', conobj.apiToken__c);
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;

        CommunityJobFiltersParamRESTOperation.ResponseHandler wrapperobj = CommunityJobFiltersParamRESTOperation.CommunityJobFiltersParamRESTOperation();

    }
    public static testmethod void jobFiltertest1(){
        TRHC__Config_Settings__c configSettings = new TRHC__Config_Settings__c();
        configSettings.name='Default';
        configSettings.Enable_AutoMateJobRates__c = true;
        configSettings.Trigger_Enable_ScratchSheetAutoAddExp__c = false;
        configSettings.AutoAddExpenseCriteria__c='TRHC__Margin_Type__c=\'Expense\'';
        configSettings.Except_Hospitals__c = 'Atlas Development';
        configSettings.Trigger_Enable_Expected_End_Date_Changer__c = false;
        configSettings.Trigger_Enable_Applicant_From_SCR_Sheet__c = true;
        configSettings.Update_Pay_Burden__c = false;
        configSettings.Enable_RefferalTrigger__c = true;
        insert configSettings;

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/mobile/JobFiltersParameters';
        req.addParameter('apiToken', 'GUEST');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;

        CommunityJobFiltersParamRESTOperation.ResponseHandler wrapperobj = CommunityJobFiltersParamRESTOperation.CommunityJobFiltersParamRESTOperation();
    }
}