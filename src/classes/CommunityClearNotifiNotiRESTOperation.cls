@RestResource(urlMapping='/mobile/ClearNotification/*')
global class CommunityClearNotifiNotiRESTOperation {
    @HttpGet
    global static ResponseHandler CommunityClearNotifiNotiRESTOperation(){
        ResponseHandler res = new ResponseHandler ();
        String apiToken = RestContext.request.params.get('apiToken');
        
        List<contact> contactList = [Select Id,
                                         Name,
                                         apiToken__c
                                    From Contact 
                                    Where apiToken__c =: apiToken
                                    ];
       if(!contactList.isEmpty()){
           List<Notification__c> notificationList = [select id,name,Read_Unread__c from Notification__c where Contact__c =: contactList[0].id];
           if(notificationList.isEmpty()){
               res.success = 0;
               res.message = new string[]{'Notification not Found.'};
               return res;
           } else {
               res.success = 1;
               res.message = new string[]{'Success.'};
               delete notificationList;
               return res;
    
           }    
       }
       res.success = 2;
       res.message = new string[]{'Unauthorized User.'};
       return res;
    }
    
    global class ResponseHandler {
        global integer success;
        global string[] message;
        global ResponseHandler (){}
    }
}