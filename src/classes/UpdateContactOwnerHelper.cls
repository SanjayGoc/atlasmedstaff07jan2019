public with sharing class UpdateContactOwnerHelper {

	public static void changeOwner(List<Contact> listContact){
		Id siteGuestUserId = Id.valueOf(System.Label.Site_Guest_User_Id);
		Id changeOwnerId = Id.valueOf(System.Label.Change_User_Id);
		for(Contact singleContact : listContact){
			if(singleContact.OwnerId == siteGuestUserId){
				singleContact.OwnerId = changeOwnerId;
			}
		}
	}
}