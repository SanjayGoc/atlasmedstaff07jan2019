/*
===========================================================================================================
Created Date:   Aug 2018
Description:    View Client Mangare, Recruiter based on Accepted Date      
Functionality:  View Page in Dashboard
Developed By : Minkesh Patel(minkesh.patel@getoncrm.com)
Visualforce page : DashboardPageClone
===========================================================================================================
*/
public with sharing class DashboardPageControllerNew {
    private Id placementRecTypeId;
    public Date startDat                                                  {get; set;}
    public Date endDat                                                    {get; set;}
    public Date startedDate                                               {get; set;}
    public Date endedDate                                                 {get; set;}
    public String selectedDaysForRecruiter                                {get; set;} //selected Days picklist for recruiter
    public String selectedDaysForCM                                       {get; set;}
    public String selectedDaysForCC                                       {get; set;}
    public String singlePickList                                          {get; set;}
    
    private List<Periodically_Date__c> finalDate;                             
    private Map<Id, TRHC__Placement__c> finalMap                          {get; set;}
    public List<trhcPlacementWrapper> trhcPlacementWrapperList            {get; set;}
    public List<CMTrhcPlacmentWrapper> cmTrhcPlacementWrapperList         {get; set;}
    public List<CCTrhcPlacementWrapper> ccTrhcPlacementWrapperList        {get; set;}
    public List<TRHC__Placement__c> duplicateCandidateActiveList          {get; set;} 
    public Integer placementCount                                         {get; set;}
    public Integer extensionCount                                         {get; set;}
    public Integer placementTotal                                         {get; set;}
    private Map<String,Double> averageGPHValueForcm;
    private Map<String,Double> averageGPHValueForRecruiter;
    private Map<String,set<Id>> candidateExtensionMap;
    private Map<String,set<String>> placementRecIdMap;
    private Map<String, Periodically_Date__c> periodicallyDateList;
    public string recName{get;set;}
    public string cmName{get;set;}
    
    public DashboardPageControllerNew() {
        trhcPlacementWrapperList = new List<trhcPlacementWrapper>();
        cmTrhcPlacementWrapperList = new List<CMTrhcPlacmentWrapper>();
        ccTrhcPlacementWrapperList = new List<CCTrhcPlacementWrapper>();
        candidateExtensionMap = new Map<String,set<Id>>();
        duplicateCandidateActiveList = new List<TRHC__Placement__c>();
        placementRecIdMap = new Map<String,set<String>>();
        placementCount = 0;
        extensionCount = 0;
        placementTotal = 0;
        periodicallyDateList = Periodically_Date__c.getAll();
        finalDate = new List<Periodically_Date__c>();
        finalDate.addAll(periodicallyDateList.values());
        finalDate.sort();
        placementRecTypeId = Schema.SObjectType.TRHC__Placement__c.getRecordTypeInfosByName().get('Placement').getRecordTypeId();
        List<Periodically_Date__c> perioddateforselectlist = [select Id,
                                                                     Name, 
                                                                     Start_Date__c, 
                                                                     End_Date__c 
                                                                     from Periodically_Date__c 
                                                                     where Start_Date__c <=: system.today()
                                                                     AND End_Date__c >=: system.today()];                                   
        if(perioddateforselectlist.size() > 0) {
            selectedDaysForRecruiter = perioddateforselectlist[0].Name;
            selectedDaysForCM = perioddateforselectlist[0].Name;
            selectedDaysForCC = perioddateforselectlist[0].Name;
            startedDate = perioddateforselectlist[0].Start_Date__c;
            endedDate = perioddateforselectlist[0].End_Date__c;
            getScore();
            startDat = perioddateforselectlist[0].Start_Date__c;
            endDat = perioddateforselectlist[0].End_Date__c;
            getExtensionData();
        } 
    }

    public List<SelectOption> getRecruiterdaysList() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('None','Select Period'));
        for(Periodically_Date__c singlePeriod : finalDate) {
            options.add(new SelectOption(singlePeriod.Name,singlePeriod.Name+' '+'('+singlePeriod.Start_Date__c.format()+' To '+singlePeriod.End_Date__c.format()+')'));
        }
        return options;
    }
    
    //***** Get Score For Recriter, CM & CC (Table data) *****//
    public PageReference getScore() {
        Map<Id, TRHC__Placement__c> cmToPlacement;
        Map<Id,TRHC__Placement__c> ccToPlacement;
        Map<Id, TRHC__Placement__c> idToPlacement;
        trhcPlacementWrapperList = new List<trhcPlacementWrapper>();
        cmTrhcPlacementWrapperList = new List<CMTrhcPlacmentWrapper>();
        ccTrhcPlacementWrapperList = new List<CCTrhcPlacementWrapper>();
        candidateExtensionMap = new Map<String,set<Id>>();
        averageGPHValueForcm = new map<string,Double>();
        averageGPHValueForRecruiter = new Map<String,Double>();
        Map<String, TrhcPlacementWrapper> recruimentwrapperMap = new Map<String, trhcPlacementWrapper>();
        Map<String, CMTrhcPlacmentWrapper> cmWrapperMap = new Map<String, CMTrhcPlacmentWrapper>();
        Map<String, CCTrhcPlacementWrapper> ccWrapperMap = new Map<String, CCTrhcPlacementWrapper>();
        Map<Id,List<TRHC__Placement__c>> duplicateCandidatePlatements = new Map<Id,List<TRHC__Placement__c>>();
        List<TRHC__Placement__c> duplicatePlacementList;
        duplicateCandidateActiveList = new List<TRHC__Placement__c>();
        Set<Id> recCandidateIds = new Set<Id>();
        Set<Id> cmCandidateIds = new Set<Id>();
        Set<Id> ccCandidateIds = new Set<Id>();
        Set<Id> recActiveCandidateIds = new Set<Id>();
        Integer numberOfExtension = 0,numberOfactivePlacement = 0,total = 0,
                numberofCMExtension = 0, numberofCMactivePlacement = 0,totalNumOfCMExtension = 0,
                numberofCCExtension = 0,numberofCCactivePlacement =0,totalNumOfCCExtension = 0;
        TrhcPlacementWrapper tempWrapObj;    
        CMTrhcPlacmentWrapper cmTempWrapObj;
        CCTrhcPlacementWrapper ccTempWrapObj;
        Double averageGPH   = 0.0,averageGPHofCM = 0.0;
        if(startedDate != null && endedDate != null) {
            idToPlacement = new Map<Id, TRHC__Placement__c>([SELECT Id, 
                                                                    Name, 
                                                                    TRHC__Contact_Candidate__c, 
                                                                    TRHC__Recruiter__r.Name,
                                                                    TRHC__Extension__c
                                                                    FROM TRHC__Placement__c 
                                                                    WHERE TRHC__Recruiter__c != null 
                                                                    AND RecordTypeId =: placementRecTypeId
                                                                    AND (TRHC__End_Date__c >=: startedDate 
                                                                    AND TRHC__Start_Date__c <=: endedDate)
                                                                    ORDER BY CreatedDate DESC NULLS FIRST 
                                                                    limit 10000]);
            System.debug(json.serializepretty(idToPlacement));

            cmToPlacement = new Map<Id, TRHC__Placement__c>([SELECT Id, 
                                                                    Name, 
                                                                    TRHC__Account_Manager__r.Name,
                                                                    TRHC__Recruiter__r.Name,
                                                                    TRHC__Extension__c ,
                                                                    TRHC__Contact_Candidate__c,
                                                                    TRHC__Contact_Candidate__r.Name,
                                                                    TRHC__Job__r.Name,
                                                                    Not_Duplicate__c
                                                                    FROM TRHC__Placement__c 
                                                                    WHERE TRHC__Account_Manager__c != null
                                                                    AND RecordTypeId =: placementRecTypeId
                                                                    AND (TRHC__End_Date__c >=: startedDate
                                                                    AND TRHC__Start_Date__c <=: endedDate)
                                                                    ORDER BY CreatedDate DESC NULLS LAST
                                                                    LIMIT 10000 ]);
            System.debug(json.serializepretty(cmToPlacement));
            
            ccToPlacement = new Map<Id, TRHC__Placement__c>([SELECT Id, 
                                                                    Name,
                                                                    Compliance_Coordinator__r.Name,
                                                                    TRHC__Extension__c, 
                                                                    TRHC__Contact_Candidate__c,
                                                                    TRHC__Contact_Candidate__r.Name
                                                                    FROM TRHC__Placement__c 
                                                                    WHERE Compliance_Coordinator__c != null
                                                                    AND RecordTypeId =: placementRecTypeId
                                                                    AND (TRHC__End_Date__c >=: startedDate 
                                                                    AND TRHC__Start_Date__c <=: endedDate)
                                                                    ORDER BY CreatedDate DESC NULLS FIRST
                                                                    limit 10000]);

        }
        if(singlePickList == 'Recruiter' || (startedDate != null && endedDate != null)) {
            trhcPlacementWrapperList.clear();   
            if(startedDate == null && endedDate == null){
                idToPlacement = SelectMonth(selectedDaysForRecruiter);
            } 
            candidateExtensionMap = CandidateExtension(idToPlacement.values(),'Recruiter');

            for(TRHC__Placement__c ja : idToPlacement.values()) {
                numberOfExtension = 0;
                if(!recruimentwrapperMap.containsKey(ja.TRHC__Recruiter__r.Name)) {
                    recruimentwrapperMap.put(ja.TRHC__Recruiter__r.Name,new trhcPlacementWrapper(ja.TRHC__Recruiter__r.Name,numberOfactivePlacement,numberOfExtension,total,averageGPH));
                }

                if(ja.TRHC__Extension__c == true){
                    if(!recCandidateIds.contains(ja.TRHC__Contact_Candidate__c) 
                        && ja.TRHC__Recruiter__r != null
                        && candidateExtensionMap.get(ja.TRHC__Recruiter__r.Name+'-false') != null 
                        && !candidateExtensionMap.get(ja.TRHC__Recruiter__r.Name+'-false').contains(ja.TRHC__Contact_Candidate__c)) {

                        tempWrapObj = recruimentwrapperMap.get(ja.TRHC__Recruiter__r.Name);
                        tempWrapObj.Extension += 1;
                        tempWrapObj.extensionPlacementIds.add(ja.Id);
                        recruimentwrapperMap.put(ja.TRHC__Recruiter__r.Name,tempWrapObj);
                        recCandidateIds.add(ja.TRHC__Contact_Candidate__c);
                    } 
                } else {
                        tempWrapObj = recruimentwrapperMap.get(ja.TRHC__Recruiter__r.Name);
                        tempWrapObj.ActivePlacement += 1;
                        tempWrapObj.activePlacementIds.add(ja.Id);
                        recruimentwrapperMap.put(ja.TRHC__Recruiter__r.Name,tempWrapObj);
                        recCandidateIds.add(ja.TRHC__Contact_Candidate__c);
                }
                
                tempWrapObj = recruimentwrapperMap.get(ja.TRHC__Recruiter__r.Name);
                tempWrapObj.Total = tempWrapObj.ActivePlacement + tempWrapObj.Extension;
                recruimentwrapperMap.put(ja.TRHC__Recruiter__r.Name,tempWrapObj);
            }

            if(startedDate != null && endedDate != null) {
                averageGPHRecruiter(recruimentwrapperMap,cmWrapperMap,startedDate,endedDate);
                trhcPlacementWrapperList.addAll(recruimentwrapperMap.values());
                for(TrhcPlacementWrapper WrapObj : trhcPlacementWrapperList) {
                    WrapObj.AverageGPH = 0;
                    if(averageGPHValueForRecruiter.containsKey(WrapObj.RecruiterName)){
                        WrapObj.AverageGPH = averageGPHValueForRecruiter.get(WrapObj.RecruiterName);        
                    }
                }
            }
            else if(Periodically_Date__c.getInstance(selectedDaysForRecruiter) != null){
                Periodically_Date__c periodicallyDate = Periodically_Date__c.getInstance(selectedDaysForRecruiter);
                Date selectedStartDate = periodicallyDate.Start_Date__c;
                Date endDate = periodicallyDate.End_Date__C;
                averageGPHRecruiter(recruimentwrapperMap,cmWrapperMap,startedDate,endedDate);
                trhcPlacementWrapperList.addAll(recruimentwrapperMap.values());
                for(TrhcPlacementWrapper WrapObj : trhcPlacementWrapperList) {
                    WrapObj.AverageGPH = 0;
                    if(averageGPHValueForRecruiter.containsKey(WrapObj.RecruiterName)){
                        WrapObj.AverageGPH = averageGPHValueForRecruiter.get(WrapObj.RecruiterName);        
                    }
                }   
            }
        idToPlacement.clear();
        candidateExtensionMap.clear();
        }

            

        if(singlePickList == 'CM' || (startedDate != null && endedDate != null)) {
            CMtrhcPlacementWrapperList.clear();
            if(startedDate == null && endedDate == null) {
                cmToPlacement =  SelectMonth(selectedDaysForCM);
            }
            candidateExtensionMap = CandidateExtension(cmToPlacement.values(),'CM');
            for(TRHC__Placement__c ja : cmToPlacement.values()) {
                totalNumOfCMExtension = 0;
                if(!cmWrapperMap.containsKey(ja.TRHC__Account_Manager__r.Name)) {
                    cmWrapperMap.put(ja.TRHC__Account_Manager__r.Name,new CMTrhcPlacmentWrapper(ja.TRHC__Account_Manager__r.Name,numberofCMactivePlacement,numberofCMExtension,totalNumOfCMExtension,averageGPHofCM));
                } 
                if(ja.TRHC__Extension__c == true){

                    if(!cmCandidateIds.contains(ja.TRHC__Contact_Candidate__c) 
                        && ja.TRHC__Account_Manager__r != null
                        && candidateExtensionMap.get(ja.TRHC__Account_Manager__r.Name+'-false') != null 
                        && !candidateExtensionMap.get(ja.TRHC__Account_Manager__r.Name+'-false').contains(ja.TRHC__Contact_Candidate__c)) {

                        cmTempWrapObj = cmWrapperMap.get(ja.TRHC__Account_Manager__r.Name);
                        cmTempWrapObj.NumOfExtension += 1;
                        cmTempWrapObj.extensionPlacementIds.add(ja.Id);
                        cmWrapperMap.put(ja.TRHC__Account_Manager__r.Name,cmTempWrapObj);
                        cmCandidateIds.add(ja.TRHC__Contact_Candidate__c);
                    } 
                } else {
                        cmTempWrapObj = cmWrapperMap.get(ja.TRHC__Account_Manager__r.Name);
                        cmTempWrapObj.NumOfActivePlacement += 1;
                        cmTempWrapObj.activePlacementIds.add(ja.Id);
                        cmWrapperMap.put(ja.TRHC__Account_Manager__r.Name,cmTempWrapObj);
                        cmCandidateIds.add(ja.TRHC__Contact_Candidate__c);
                    if(duplicateCandidatePlatements.containsKey(ja.TRHC__Contact_Candidate__c)){
                        duplicatePlacementList = duplicateCandidatePlatements.get(ja.TRHC__Contact_Candidate__c);
                        duplicatePlacementList.add(ja);
                        duplicateCandidatePlatements.put(ja.TRHC__Contact_Candidate__c, duplicatePlacementList);
                    } else {    
                        duplicatePlacementList = new List<TRHC__Placement__c>();
                        duplicatePlacementList.add(ja);
                        duplicateCandidatePlatements.put(ja.TRHC__Contact_Candidate__c, duplicatePlacementList);
                    }
                }
                cmTempWrapObj = cmWrapperMap.get(ja.TRHC__Account_Manager__r.Name);
                cmTempWrapObj.TotalNumOfExtension = cmTempWrapObj.NumOfActivePlacement + cmTempWrapObj.NumOfExtension;
                cmWrapperMap.put(ja.TRHC__Account_Manager__r.Name,cmTempWrapObj);
            }
            if(startedDate != null && endedDate != null) {
                averageGPHRecruiter(recruimentwrapperMap,cmWrapperMap,startedDate,endedDate);
                cmTrhcPlacementWrapperList.addAll(cmWrapperMap.values());
                for(CMTrhcPlacmentWrapper cmWrapObj : cmTrhcPlacementWrapperList) {
                    cmWrapObj.CMofAverageGPH = 0;
                    if(averageGPHValueForcm.containsKey(cmWrapObj.AccountManagerName)){
                        cmWrapObj.CMofAverageGPH = averageGPHValueForcm.get(cmWrapObj.AccountManagerName);      
                    }
                }
            }
            else if(Periodically_Date__c.getInstance(selectedDaysForCM) != null){
                Periodically_Date__c periodicallyDate = Periodically_Date__c.getInstance(selectedDaysForCM);
                Date selectedStartDate = periodicallyDate.Start_Date__c;
                Date endDate = periodicallyDate.End_Date__C;
                averageGPHRecruiter(recruimentwrapperMap,cmWrapperMap,startedDate,endedDate);
                cmTrhcPlacementWrapperList.addAll(cmWrapperMap.values());
                for(CMTrhcPlacmentWrapper cmWrapObj : cmTrhcPlacementWrapperList) {
                    cmWrapObj.CMofAverageGPH = 0;
                    if(averageGPHValueForcm.containsKey(cmWrapObj.AccountManagerName)){
                        cmWrapObj.CMofAverageGPH = averageGPHValueForcm.get(cmWrapObj.AccountManagerName);      
                    }
                }   
            }
            for(Id candidate : duplicateCandidatePlatements.keySet()){
                if(duplicateCandidatePlatements.get(candidate).size() > 1) {
                    duplicateCandidateActiveList.addAll(duplicateCandidatePlatements.get(candidate));
                }
            }
        cmToPlacement.clear();
        candidateExtensionMap.clear();
        }

        if(singlePickList == 'CC' || (startedDate != null && endedDate != null)) {
            CCtrhcPlacementWrapperList.clear();
            if(startedDate == null && endedDate == null) {
                ccToPlacement =  SelectMonth(selectedDaysForCC);
            }
            candidateExtensionMap = CandidateExtension(ccToPlacement.values(),'CC');
            for(TRHC__Placement__c ja : ccToPlacement.values()) {
                totalNumOfCCExtension = 0;
                if(!ccWrapperMap.containsKey(ja.Compliance_Coordinator__r.Name)) {
                    ccWrapperMap.put(ja.Compliance_Coordinator__r.Name,new CCTrhcPlacementWrapper(ja.Compliance_Coordinator__r.Name,numberofCCactivePlacement,numberofCCExtension,totalNumOfCCExtension));
                } 
                if(ja.TRHC__Extension__c == true){

                    if(!ccCandidateIds.contains(ja.TRHC__Contact_Candidate__c) 
                        && ja.Compliance_Coordinator__r != null
                        && candidateExtensionMap.get(ja.Compliance_Coordinator__r.Name+'-false') != null 
                        && !candidateExtensionMap.get(ja.Compliance_Coordinator__r.Name+'-false').contains(ja.TRHC__Contact_Candidate__c)) {

                        ccTempWrapObj = ccWrapperMap.get(ja.Compliance_Coordinator__r.Name);
                        ccTempWrapObj.NumOfExtensionForCC += 1;
                        ccWrapperMap.put(ja.Compliance_Coordinator__r.Name,ccTempWrapObj);
                        ccCandidateIds.add(ja.TRHC__Contact_Candidate__c);
                    } 
                } else {
                        ccTempWrapObj = ccWrapperMap.get(ja.Compliance_Coordinator__r.Name);
                        ccTempWrapObj.NumOfActivePlacementForCC += 1;
                        ccWrapperMap.put(ja.Compliance_Coordinator__r.Name,ccTempWrapObj);
                        ccCandidateIds.add(ja.TRHC__Contact_Candidate__c);
                }
                 
                ccTempWrapObj = ccWrapperMap.get(ja.Compliance_Coordinator__r.Name);
                ccTempWrapObj.TotalNumOfExtensionForCC = ccTempWrapObj.NumOfActivePlacementForCC + ccTempWrapObj.NumOfExtensionForCC;
                ccWrapperMap.put(ja.Compliance_Coordinator__r.Name,ccTempWrapObj);
            }
            ccTrhcPlacementWrapperList.addAll(ccWrapperMap.values());
        ccToPlacement.clear();
        candidateExtensionMap.clear();
        }
        //candidateExtensionMap.clear();
        duplicateCandidatePlatements.clear();     
        return null;
    }

    public PageReference updateDupliCanListActive() {
        update duplicateCandidateActiveList;
        singlePickList = 'CM';
        getScore();
        return null;
    }
    
    //***** select day for all table *****//
    public Map<Id, TRHC__Placement__c> SelectMonth(String selectedMonth) {
        String query;
        String findQueryString;
        finalMap = new Map<Id, TRHC__Placement__c>();
        if(selectedMonth != null && selectedMonth != '' && selectedMonth != 'None') {
            Periodically_Date__c periodicallyDate = Periodically_Date__c.getInstance(selectedMonth);
            Date selectedStartDate = periodicallyDate.Start_Date__c;
            Date endDate = periodicallyDate.End_Date__C;
            findQueryString = (singlePickList =='Recruiter') ? ' TRHC__Recruiter__c != null ' :
                              (singlePickList =='CM') ? ' TRHC__Account_Manager__c != null ' :
                              (singlePickList =='CC') ? ' Compliance_Coordinator__c != null ' : 'None';
            query = 'SELECT Id, Name, TRHC__Account_Manager__r.Name,Not_Duplicate__c,TRHC__Job__r.Name,TRHC__Contact_Candidate__c,TRHC__Contact_Candidate__r.Name,TRHC__Extension__c,TRHC__Recruiter__r.Name,Compliance_Coordinator__r.Name ';
            query += ' FROM TRHC__Placement__c WHERE  ';
            query +=  findQueryString + ' AND (TRHC__End_Date__c >=: selectedStartDate AND TRHC__Start_Date__c <=: endDate)';
            query += 'AND RecordTypeId =: placementRecTypeId ORDER BY TRHC__Extension__c DESC NULLS FIRST limit 10000';
            if(findQueryString != 'None') {
                List<SObject> fetchValues = Database.query(query);
                for(SObject objectValue : fetchValues){
                    finalMap.put(objectValue.Id,(TRHC__Placement__c) objectValue);
                }
            }
        }
        return finalMap;
    }
    
    public Map<String,set<Id>> CandidateExtension (list<TRHC__Placement__c> placements, String tableName) {
        Map<String,set<Id>> candidateExtensionMap = new Map<String,set<Id>>();
        Set<Id> tempCandidateSet;
       
        String fieldValue = (tableName == 'Recruiter') ? 'TRHC__Recruiter__r' :
                              (tableName == 'CM') ? 'TRHC__Account_Manager__r' :
                              (tableName == 'CC') ? 'Compliance_Coordinator__r' : 'None';
    
        for(TRHC__Placement__c placement : placements) {
            if(!candidateExtensionMap.containsKey(placement.getSObject(fieldValue).get('Name') +'-'+placement.TRHC__Extension__c)){
                tempCandidateSet = new set<Id>();
                tempCandidateSet.add(placement.TRHC__Contact_Candidate__c);
                candidateExtensionMap.put(placement.getSObject(fieldValue).get('Name')  +'-'+ placement.TRHC__Extension__c,tempCandidateSet);
            } else {
                tempCandidateSet = candidateExtensionMap.get(placement.getSObject(fieldValue).get('Name') +'-'+placement.TRHC__Extension__c);
                tempCandidateSet.add(placement.TRHC__Contact_Candidate__c);
                candidateExtensionMap.put(placement.getSObject(fieldValue).get('Name') +'-'+placement.TRHC__Extension__c, tempCandidateSet);
            }
        }
        return candidateExtensionMap;
    }

    public void averageGPHRecruiter(Map<String,trhcPlacementWrapper> recruiterMap,Map<String,CMTrhcPlacmentWrapper> cmMap,Date startDate,Date endDate){
        Map<String,Decimal> recruiterDateGPHmap = new Map<String,Decimal> ();
        Map<String,Integer> recruiterTotalScratchSheetMap = new Map<String,Integer> ();
        Map<String,Decimal> cmDateGPHmap = new Map<String,Decimal> ();
        Map<String,Integer> cmTotalScratchSheetMap = new Map<String,Integer>(); 
        averageGPHValueForRecruiter =  new map<string,Double>();
        averageGPHValueForcm = new map<String,Double>();
        Double gph = 0;
        Integer scratchsheetCount = 0;
        Decimal averageGphTot = 0;
        Set<String> placementSetIds;
        set<id> candidateSetIds = new set<id>();
        placementRecIdMap.clear();
        trhcPlacementWrapper obj;
        Set<String> allPlacSet = new Set<String>();

        for(trhcPlacementWrapper objf: recruiterMap.values()){
            allPlacSet.addall(objf.activePlacementIds);
            allPlacSet.addall(objf.extensionPlacementIds);
        }
        for(CMTrhcPlacmentWrapper objf: cmMap.values()){
            allPlacSet.addall(objf.activePlacementIds);
            allPlacSet.addall(objf.extensionPlacementIds);
        }
        for(TRHC__Placement__c jobObj : [SELECT Id, Name ,TRHC__Contact_Candidate__c,TRHC__Recruiter__c ,TRHC__End_Date__c, TRHC__Scratch_Sheet__c, TRHC__Account_Manager__c, TRHC__Account_Manager__r.name, TRHC__Start_Date__c, TRHC__Recruiter__r.Name, GPH__c  FROM TRHC__Placement__c WHERE id =: allPlacSet]) {
             
            gph = 0;
            scratchsheetCount = 0;

            if(recruiterDateGPHmap.containskey(jobObj.TRHC__Recruiter__r.Name))
                gph = recruiterDateGPHmap.get(jobObj.TRHC__Recruiter__r.Name);
            if(jobObj.GPH__c != NULL)
                gph += jobObj.GPH__c;
            
            if(recruiterDateGPHmap.containskey(jobObj.TRHC__Recruiter__r.Name))
                scratchsheetCount = recruiterTotalScratchSheetMap.get(jobObj.TRHC__Recruiter__r.Name);
            scratchsheetCount += 1;

            recruiterDateGPHmap.put(jobObj.TRHC__Recruiter__r.Name, gph);
            recruiterTotalScratchSheetMap.put(jobObj.TRHC__Recruiter__r.Name,scratchsheetCount);

            gph = 0;
            scratchsheetCount = 0;

            if(cmDateGPHmap.containskey(jobObj.TRHC__Account_Manager__r.Name))
                gph = cmDateGPHmap.get(jobObj.TRHC__Account_Manager__r.Name);
            if(jobObj.GPH__c != NULL)
                gph += jobObj.GPH__c;
            
            if(cmDateGPHmap.containskey(jobObj.TRHC__Account_Manager__r.Name))
                scratchsheetCount = cmTotalScratchSheetMap.get(jobObj.TRHC__Account_Manager__r.Name);
            scratchsheetCount += 1;
                
            cmDateGPHmap.put(jobObj.TRHC__Account_Manager__r.Name, gph);
            cmTotalScratchSheetMap.put(jobObj.TRHC__Account_Manager__r.Name,scratchsheetCount);
            
            if(placementRecIdMap.containsKey(jobObj.TRHC__Recruiter__r.Name)){
                placementSetIds = placementRecIdMap.get(jobObj.TRHC__Recruiter__r.Name);
                placementSetIds.add(jobObj.Id);
                placementRecIdMap.put(jobObj.TRHC__Recruiter__r.Name,placementSetIds);
            }else{
                placementSetIds = new Set<String>();
                placementSetIds.add(jobObj.Id);
                placementRecIdMap.put(jobObj.TRHC__Recruiter__r.Name,placementSetIds);
            }
            
            if(placementRecIdMap.containsKey(jobObj.TRHC__Account_Manager__r.Name)){
                placementSetIds = placementRecIdMap.get(jobObj.TRHC__Account_Manager__r.Name);   
                placementSetIds.add(jobObj.Id);
                placementRecIdMap.put(jobObj.TRHC__Account_Manager__r.Name,placementSetIds);
            }else{

                placementSetIds = new Set<String>();
                placementSetIds.add(jobObj.Id);
                placementRecIdMap.put(jobObj.TRHC__Account_Manager__r.Name,placementSetIds);
            }
        }

        for(String recName : recruiterMap.keyset()){
            averageGphTot = 0;
            if(recruiterDateGPHmap.get(recName) != null && recruiterTotalScratchSheetMap.get(recName) != null){
                averageGphTot = recruiterDateGPHmap.get(recName)/recruiterTotalScratchSheetMap.get(recName);
                decimal rounded = averageGphTot.setScale(2);
                averageGPHValueForRecruiter.put(recName, rounded);
            }
        }

        for(String recName : cmMap.keyset()){
            averageGphTot = 0;
            if(cmDateGPHmap.get(recName) != null && cmTotalScratchSheetMap.get(recName) != null){
                averageGphTot = cmDateGPHmap.get(recName)/cmTotalScratchSheetMap.get(recName);
                decimal rounded = averageGphTot.setScale(2);
                averageGPHValueForcm.put(recName, rounded);
            }
        }
    }

    //***** For Placement and Extension Total Record Count *****//
     public Void getExtensionData() {
        List<TRHC__Placement__c> placementRecord;   
        placementCount = 0;
        extensionCount = 0;
        if(startDat != null && endDat != null) {
            placementRecord = [SELECT Id, 
                                    Name, 
                                    TRHC__Account_Manager__c,
                                    TRHC__Account_Manager__r.Name, 
                                    TRHC__Recruiter__r.Name, 
                                    TRHC__Recruiter__c, 
                                    TRHC__Contact_Candidate__r.Name, 
                                    TRHC__Extension__c,
                                    Not_Duplicate__c
                                    FROM TRHC__Placement__c 
                                    WHERE (TRHC__End_Date__c >=: startDat 
                                    AND TRHC__Start_Date__c <=: endDat)
                                    AND RecordTypeId =: placementRecTypeId  
                                    limit 10000];
            placementTotal = placementRecord.size();
            if(!placementRecord.isEmpty()) {
                for(TRHC__Placement__c  pl : placementRecord) {
                    if (pl.TRHC__Extension__c == false) {
                        placementCount++;
                    } else {
                        extensionCount++;
                    }
                } 
            }
        placementRecord.clear();
        }
    } 

    public List<TRHC__Placement__c> getplacList(){
        Set<String> plcementIds =  recName != null && recName != ''  ? placementRecIdMap.get(recName) : placementRecIdMap.get(cmName);
        List<TRHC__Placement__c> placementList = new List<TRHC__Placement__c>();
        placementList = [SELECT id,Name ,GPH__c,Compliance_Coordinator__r.Name,TRHC__Contact_Candidate__r.name,TRHC__Status__c,TRHC__Recruiter__r.Name,TRHC__Account_Manager__r.Name,TRHC__Extension__c,TRHC__Start_Date__c from TRHC__Placement__c where id in : plcementIds];
        return placementList;
    } 

    public PageReference showPlacementData(){
        PageReference pg = new PageReference('/apex/AverageGPHPageClone?recName='+recName+'&cmName='+cmName);
        /*+'&sdr='+selectedDaysForRecruiter+'&isFiler='+isFiler+'&startdateSearch='+startedDate+'&enddateSearch='+endedDate*/
        pg.setRedirect(false);
        return pg;

    }

    //***** Wrapper class for Recruiter *****//
    public class TrhcPlacementWrapper {
        public String RecruiterName {get;set;}
        public Integer ActivePlacement {get;set;}
        public Integer Extension {get;set;}
        public Integer Total {get;set;}
        public Double AverageGPH {get;set;}
        public set<String> activePlacementIds{get;set;}
        public set<String> extensionPlacementIds{get;set;}

        public TrhcPlacementWrapper(String RecruiterName,Integer ActivePlacement,Integer Extension,Integer Total,Double AverageGPH){
            activePlacementIds = new set<String>();
            extensionPlacementIds = new set<String>();
            this.RecruiterName = RecruiterName;
            this.ActivePlacement = ActivePlacement;
            this.Extension = Extension;
            this.Total = Total;
            this.AverageGPH = AverageGPH;

        }
    }

    //***** Wrapper class for Client Manager *****//
    public class CMTrhcPlacmentWrapper {
        public String AccountManagerName {get; set;}
        public Integer NumOfActivePlacement {get; set;}
        public Integer NumOfExtension {get; set;}
        public Integer TotalNumOfExtension {get; set;}
        public Double CMofAverageGPH {get; set;}
        public Set<String> activePlacementIds{get;set;}
        public Set<String> extensionPlacementIds{get;set;}
    
        public CMTrhcPlacmentWrapper(String AccountManagerName,Integer NumOfActivePlacement,Integer NumOfExtension,Integer TotalNumOfExtension,Double CMofAverageGPH) {
            activePlacementIds = new set<String>();
            extensionPlacementIds = new set<String>();
            this.AccountManagerName = AccountManagerName;
            this.NumOfActivePlacement = NumOfActivePlacement;
            this.NumOfExtension = NumOfExtension;
            this.TotalNumOfExtension = TotalNumOfExtension;
            this.CMofAverageGPH = CMofAverageGPH;
        }
    }

    //***** Wrapper class for Compliance Coordinator *****//
    public class CCTrhcPlacementWrapper {
        public String ComplianceCoordinatorName {get;set;}
        public Integer NumOfActivePlacementForCC {get;set;}
        public Integer NumOfExtensionForCC {get;set;}
        public Integer TotalNumOfExtensionForCC {get;set;}

        public CCTrhcPlacementWrapper( String ComplianceCoordinatorName,Integer NumOfActivePlacementForCC,Integer NumOfExtensionForCC,Integer TotalNumOfExtensionForCC){
            this.ComplianceCoordinatorName = ComplianceCoordinatorName;
            this.NumOfActivePlacementForCC = NumOfActivePlacementForCC;
            this.NumOfExtensionForCC = NumOfExtensionForCC;
            this.TotalNumOfExtensionForCC = TotalNumOfExtensionForCC;
        }
    }
}