@isTest
public class PopolaterecruiterPlacementHandlerTest {
    static testmethod void placementRecPopTest() {
        List<User> usr = [SELECT Id,Name,Email,FirstName,LastName FROM User Limit 3];
        
        TRHC__Config__c configObj1 = new TRHC__Config__c();
        configObj1.TRHC__Name__c = 'check';
        configObj1.TRHC__Margin_Type__c = 'Expense';
        configObj1.TRHC__Frequency__c = 'Weekly';
        configObj1.TRHC__State__c =   'California';
        configObj1.TRHC__Include_in_payroll__c= True;
        configObj1.TRHC__Pay_Period_to_be_paid__c='First Check';
        configObj1.TRHC__Active__c = True;
        configObj1.TRHC__Value__c = '350.0';
        configObj1.TRHC__Grouping_Total__c ='Pay Burden';
        configObj1.TRHC__City__c = 'test';
        
        configObj1.RecordTypeId =[Select Id From RecordType Where SobjectType = 'TRHC__Config__c' and Name = 'Margin calculation Line Item'].Id;
        
        insert configObj1;
        
        TRHC__Config_Settings__c configSettings = new TRHC__Config_Settings__c();
        configSettings.name='Default';
        configSettings.Trigger_Enable_ScratchSheetAutoAddExp__c = true;
        configSettings.AutoAddExpenseCriteria__c='TRHC__Margin_Type__c=\'Expense\'';
        configSettings.ExceptConfigIds__c = configObj1.Id;
        configSettings.Except_Hospitals__c = 'Atlas Development';
        insert configSettings;
        TRHC__Config__c configObj = new TRHC__Config__c();
        configObj.TRHC__Name__c = 'check';
        configObj.TRHC__Margin_Type__c = 'Expense';
        configObj.TRHC__Frequency__c = 'Weekly';
        configObj.TRHC__State__c =   'California';
        configObj.TRHC__Include_in_payroll__c= True;
        configObj.TRHC__Pay_Period_to_be_paid__c='First Check';
        configObj.TRHC__Active__c = True;
        configObj.TRHC__Value__c = '350.0';
        configObj.TRHC__Grouping_Total__c ='Pay Burden';
        
        configObj.RecordTypeId =[Select Id From RecordType Where SobjectType = 'TRHC__Config__c' and Name = 'Margin calculation Line Item'].Id;
        
        insert configObj;
        
        TRHC__Job__c job = new TRHC__Job__c();
        job.TRHC__Stage__c = 'closed';
        job.TRHC__Recruiter__c = usr[0].id;
        job.CreatedDate = Date.today();
        job.TRHC__Number_of_Placements__c = 1;
        insert job;
        
        TRHC__Scratch_Sheet__c newScratchSheetTemp1 = new TRHC__Scratch_Sheet__c();
        newScratchSheetTemp1.TRHC__County__c = 'Kalvin';
        newScratchSheetTemp1.TRHC__State__c='California';
        newScratchSheetTemp1.id = job.TRHC__Scratch_Sheet__c;
        newScratchSheetTemp1.Recruiter__c = usr[0].id;
        
        
        TRHC__Scratch_Sheet__c newScratchSheetTemp = new TRHC__Scratch_Sheet__c();
        newScratchSheetTemp.TRHC__County__c = 'Kalvin';
        newScratchSheetTemp.TRHC__State__c='California';
        newScratchSheetTemp.State_pic__c = 'California';
        newScratchSheetTemp.Name='Demo ScratchSheet';
        String scrtchSheetTempRecordTypeId1 = [Select Id From RecordType Where SobjectType = 'TRHC__Scratch_Sheet__c' and Name = 'Scratchsheet'].Id;
        newScratchSheetTemp.RecordTypeId=scrtchSheetTempRecordTypeId1 ;
        newScratchSheetTemp.TRHC__State__c='California';
        insert newScratchSheetTemp;
        
        Contact con = new Contact();
        con.FirstName = 'mike'; 
        con.LastName = 'saas';
        con.Email = 'test@test1.gmail.com';
        con.Compliance_Coordinater__c = usr[0].id;
        con.TRHC__Recruiter__c = usr[1].id;
        insert con;
        
        TRHC__Scratch_Sheet__c sheet = new TRHC__Scratch_Sheet__c();
        sheet.id = job.TRHC__Scratch_Sheet__c;
        sheet.Recruiter__c = usr[0].id;
        insert sheet;
        
        TRHC__Placement__c plac = new TRHC__Placement__c();
        plac.TRHC__Recruiter__c = usr[0].id;
        plac.TRHC__Contact_Candidate__c = con.id;
        plac.TRHC__Job__c = job.id;
        insert plac;
        
        job.TRHC__Stage__c = 'closed';
        job.TRHC__Recruiter__c = usr[0].id;
        job.TRHC__Scratch_Sheet__c = sheet.id;
        job.TRHC__Number_of_Placements__c = 1;
        update job;
        
        plac.TRHC__Recruiter__c = usr[0].id;
        plac.TRHC__Contact_Candidate__c = con.id;
        plac.TRHC__Job__c = job.id;
        plac.TRHC__End_Date__c = date.today() + 20;
        update plac;
        
        //sheet.id = job.TRHC__Scratch_Sheet__c;
        //sheet.Recruiter__c = usr[0].id;
        //update sheet;
        
        }
}