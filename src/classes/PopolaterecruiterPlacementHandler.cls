public class PopolaterecruiterPlacementHandler {
    
    public void recruiterPopolateJob(List<TRHC__Placement__c> placList) {
    List<TRHC__Job__c> joblist = new List<TRHC__Job__c>();
    
    TRHC__Job__c jobnum;
    
        for(TRHC__Placement__c placement : placList){
            if((Trigger.isInsert && placement.TRHC__Recruiter__c != null && placement.TRHC__Job__c != null) || (Trigger.isUpdate && placement.TRHC__Recruiter__c != null && placement.TRHC__Job__c != null)){
                jobnum = new TRHC__Job__c();
                jobnum.id = placement.TRHC__Job__c;
                jobnum.TRHC__Recruiter__c = placement.TRHC__Recruiter__c;
                joblist.add(jobnum);
            }    
        }
        if(!joblist.isEmpty()){
            update joblist;
        }
    }
}