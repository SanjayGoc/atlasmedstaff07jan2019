@isTest
public class ContactActivityNotesSortTest {
    @isTest
    public static void testm1(){ 
        TRHC__Config_Settings__c setting = new TRHC__Config_Settings__c();
        setting.Name = 'Default';
        setting.DisabletrackEventActivityonContact__c = false;
        setting.DisabletrackTaskActivityonContact__c = false;
        setting.Enable_Update_Recruiter__c = false;
        insert setting;
        
        Account account = new Account(name = 'Test Account');
        insert account;
            
        Contact candidate_Placemnet = new Contact();
        candidate_Placemnet.RecordTypeId = [SELECT Id FROM RECORDTYPE WHERE NAME = 'Candidate' AND SobjectType ='Contact' Limit 1].id;
        candidate_Placemnet.FirstName = 'TestCandidate';
        candidate_Placemnet.LastName = 'PlacementController';
        candidate_Placemnet.AccountId = account.id;
        insert candidate_Placemnet;
        
        Task t = new Task(Subject='Test Subject',Status='New',Priority='Normal',CallType='Outbound',WhoId=candidate_Placemnet.id,Description='test description');
        insert t;
         
        Event e = new Event(Subject='Test Subject', type='Email',WhoId=candidate_Placemnet.id,DurationInMinutes=0,ActivityDateTime=System.today(),Description = 'test dec');        
        insert e;
        Test.startTest();
        ContactActivivtiesSortContr conAct = new ContactActivivtiesSortContr(new ApexPages.StandardController(candidate_Placemnet));
        /*conAct.sortOrder = null;
        conAct.sortOption = null;*/
        conAct.getsortActivivties();
        List<OpenActivity> actHistory = conAct.activivties;
        conAct.sortByActivityDate();
        conAct.sortByAssignedTo();
        conAct.sortByLastModfiedDate();
        conAct.sortByRelatedTo();
        conAct.sortBySubject();
        conAct.sortByActivityDate();
        conAct.sortByAssignedTo();
        conAct.sortByLastModfiedDate();
        conAct.sortByRelatedTo();
        conAct.sortBySubject();
        system.debug('actHistory@@@'+actHistory);
        Test.stopTest();
    }
}