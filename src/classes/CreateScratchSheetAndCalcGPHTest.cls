@isTest
public class CreateScratchSheetAndCalcGPHTest {
    public static testMethod void test() {
       TRHC__Config__c configObj1 = new TRHC__Config__c();
        configObj1.TRHC__Name__c = 'check';
        configObj1.TRHC__Margin_Type__c = 'Expense';
        configObj1.TRHC__Frequency__c = 'Weekly';
        configObj1.TRHC__State__c =   'California';
        configObj1.TRHC__Include_in_payroll__c= True;
        configObj1.TRHC__Pay_Period_to_be_paid__c='First Check';
        configObj1.TRHC__Active__c = True;
        configObj1.TRHC__Value__c = '350.0';
        configObj1.TRHC__Grouping_Total__c ='Pay Burden';
        configObj1.TRHC__City__c = 'test';
        
        configObj1.RecordTypeId =[Select Id From RecordType Where SobjectType = 'TRHC__Config__c' and Name = 'Margin calculation Line Item'].Id;
        
        insert configObj1; 

        Config_To_Expense__c config = new Config_To_Expense__c();
        config.name = 'name';
        config.Expanse_Field_API_Name__c = 'testgt';
        config.Config_Field_API_Name_Custom__c = configObj1.TRHC__Value__c ;
        insert config;

        
        TRHC__Config_Settings__c configSettings = new TRHC__Config_Settings__c();
        configSettings.name='Default';
        configSettings.Trigger_Enable_ScratchSheetAutoAddExp__c = true;
        configSettings.AutoAddExpenseCriteria__c='TRHC__Margin_Type__c=\'Expense\'';
        configSettings.ExceptConfigIds__c = configObj1.Id;
        configSettings.Except_Hospitals__c = 'Atlas Development';
        insert configSettings;

        TRHC__Config__c configObj = new TRHC__Config__c();
        configObj.TRHC__Name__c = 'check';
        configObj.TRHC__Margin_Type__c = 'Expense';
        configObj.TRHC__Frequency__c = 'Weekly';
        configObj.TRHC__State__c =   'California';
        configObj.TRHC__Include_in_payroll__c= True;
        configObj.TRHC__Pay_Period_to_be_paid__c='First Check';
        configObj.TRHC__Active__c = True;
        configObj.TRHC__Value__c = '350.0';

        configObj.TRHC__Grouping_Total__c ='Pay Burden';
        
        configObj.RecordTypeId =[Select Id From RecordType Where SobjectType = 'TRHC__Config__c' and Name = 'Margin calculation Line Item'].Id;
        
        insert configObj;
        
        TRHC__Healthcare_Setting__c customSetting = new TRHC__Healthcare_Setting__c();
        customSetting.name = 'Default';
        customSetting.TRHC__Default_Scratch_sheet_Template_Name__c = 'Demo ScratchSheet';
        insert customSetting;

        Account account = new Account();
        account.Name = 'Test';
        account.Bill_Rate_Multiplier__c = 2.00;
        insert account;
       
        Set<Id> jobSetId = new Set<ID>();
        TRHC__Job__c job = new TRHC__Job__c();
        job.TRHC__Account_Lookup__c = account.id;
        job.TRHC__County__c = 'india';
        job.TRHC__State__c = 'California';
        job.TRHC__Job_Title__c = 'indiatest';
        job.state_Pic__c ='California';
        job.TRHC__Start_Date__c = system.today();
        job.TRHC__Expected_End_Date__c = system.today() + 2;
        job.TRHC__County__c ='text';
        jobSetId.add(job.Id);
        insert job;
  
        Job_to_Scratch_Sheet__c jobToScCus = new Job_to_Scratch_Sheet__c(Name='Test',Job_Field_API_Name_Custom__c='TRHC__County__c',Scratch_Sheet_Field_API_Name__c='TRHC__County__c');
        insert jobToScCus;
        
        TRHC__Scratch_Sheet__c newScratchSheetTemp = new TRHC__Scratch_Sheet__c();
        newScratchSheetTemp.TRHC__County__c = job.TRHC__County__c ;
        newScratchSheetTemp.TRHC__State__c=job.TRHC__State__c;

        newScratchSheetTemp.Name='Demo ScratchSheet';
        newScratchSheetTemp.State_pic__c = job.TRHC__State__c;
        newScratchSheetTemp.City__c = 'testAGH';
        newScratchSheetTemp.TRHC__Start_Date__c = system.today();
        newScratchSheetTemp.End_Date_New__c= system.today() + 5;
        String scrtchSheetTempRecordTypeId = [Select Id From RecordType Where SobjectType = 'TRHC__Scratch_Sheet__c' and Name = 'ScratchSheet Template'].Id;
        newScratchSheetTemp.RecordTypeId=scrtchSheetTempRecordTypeId ;
        newScratchSheetTemp.TRHC__Assignment_length__c = 10;
        newScratchSheetTemp.TRHC__Hours_Per_Week__c = 10;
        newScratchSheetTemp.over_time__c = 10;
        newScratchSheetTemp.TRHC__Bill_Rate__c = 10;
        newScratchSheetTemp.TRHC__Overtime_Rate__c  = 10;
        newScratchSheetTemp.TRHC__Total_Overtime_Hours__c  = 10;
        newScratchSheetTemp.TRHC__Pay_Burden__c = 10;
        newScratchSheetTemp.Non_Taxable_Expense__c = 10;
        newScratchSheetTemp.TRHC__Pay_Rate__c = 21;
        newScratchSheetTemp.TRHC__Overtime_Pay_Rate__c = 1;
        newScratchSheetTemp.OT_Pay_rate_Override__c = false;
        insert newScratchSheetTemp;
    
        TRHC__Expense__c expense = new TRHC__Expense__c();
        expense.TRHC__Expense_Name__c = 'Housing Stipend';
        expense.TRHC__Scratch_Sheet__c = newScratchSheetTemp.Id;
        expense.TRHC__Amount__c = 10;
        expense.TRHC__Frequency__c = 'Per Assignment';
        insert expense;

        TRHC__Expense__c expense2 = new TRHC__Expense__c();
        expense2.TRHC__Expense_Name__c = 'Sales Tax';
        expense2.TRHC__Scratch_Sheet__c = newScratchSheetTemp.Id;
        expense2.TRHC__Amount__c = 10;
        expense2.TRHC__Frequency__c = 'Per Assignment';
        insert expense2;
    
        job.TRHC__Scratch_Sheet__c = newScratchSheetTemp.id;
        update job;

        expense.TRHC__Value__c = 10.00;
        update expense;
        Test.setCurrentPageReference(new PageReference('Page.ScratchSheetCreatorfromjobcustompage'));
        System.currentPageReference().getParameters().put('jId', job.id);
        CreateScratchSheetAndCalcGPHController crs = new  CreateScratchSheetAndCalcGPHController();
        
        crs.insertScratchsheet();
        crs.calcGPH();
        crs.insertGsaSerValue();
    }
}