public class AtlasCryptohelper {
    
    public static String aes128(String value, String key1, boolean isencrypt){
        String result ='';
        //16 byte string. since characters used are ascii, each char is 1 byte.
		Blob key = Blob.valueOf(key1);
        if(isencrypt){
            //encrypted blob
            Blob cipherText = Crypto.encryptWithManagedIV('AES128', key, Blob.valueOf(value));
            //encrypted string
            result = EncodingUtil.base64Encode(cipherText);
        	System.debug(result);
        } else if(!isencrypt){
           //encrypted blob
            Blob encodedEncryptedBlob = EncodingUtil.base64Decode(value);
            //decrypted blob
            Blob decryptedBlob = Crypto.decryptWithManagedIV('AES128', key, encodedEncryptedBlob);
            //decrypted string
            result = decryptedBlob.toString();
            System.debug(result);
        }    
        
        return result;
    }
}