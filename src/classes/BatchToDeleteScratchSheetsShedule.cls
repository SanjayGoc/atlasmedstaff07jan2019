public class BatchToDeleteScratchSheetsShedule implements schedulable{
    public void execute(system.SchedulableContext ctx){
        BatchToDeleteScratchSheetsAndExpenses btch = new BatchToDeleteScratchSheetsAndExpenses();
        Database.executeBatch(btch,10);
    }
}