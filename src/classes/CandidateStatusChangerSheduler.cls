public class CandidateStatusChangerSheduler implements schedulable {
    public void execute(SchedulableContext sc){
        CandidateStatusChanger cStatusChanger = new CandidateStatusChanger();
        Database.executeBatch(cStatusChanger, 50);
    }
}