/*
============================================================================
Description:   Schedule class for SkillCommUserToChangeCurrentUser class 
        Update Skills which does not have PDF URL.
============================================================================
*/

global class SkillCommUserToChangeCurrentUserSchedule implements Schedulable {
    global void execute(SchedulableContext sc) {
        SkillCommUserToChangeCurrentUser bch = new SkillCommUserToChangeCurrentUser();
        database.executebatch(bch, 200);
    }  
}