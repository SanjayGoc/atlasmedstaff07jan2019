public class ObjectFieldSelect {
  	public list<selectoption> nameOfObjects  {get;set;}
  	public map<string,schema.SobjectType> mapOfTokens  {get;set;}
  	public List<String> listToHoldNames  {get;set;}
	public string selectedObject  {get;set;}
	//public List<string> newNameOfFields  {get;set;}
  
	public ObjectFieldSelect(){

	  	mapOfTokens=Schema.getGlobalDescribe();
	  	listToHoldNames= new list<string>();
	  	nameOfObjects= new list<selectoption>();
	  	listToHoldNames.addAll(mapOfTokens.keyset());
	  	listToHoldNames.sort();
	 	nameOfObjects.add(new selectoption('None','--None--'));
	  	for(string s:listToHoldNames){
	  		nameOfObjects.add(new selectoption(s,s));
	  	}
	}
  	
  	public pagereference showField(){
  		system.debug('The selectedObject is :'+selectedObject);
	  	Map <String, Schema.SObjectField> fieldMap = mapOfTokens.get(selectedObject).getDescribe().fields.getMap();
		String fieldNames =  'AccountId';
		for(Schema.SObjectField sfield : fieldMap.Values())
		{
			schema.describefieldresult dfield = sfield.getDescribe();
		//	system.debug('#######' + dfield );	  	
	//	DescribeSObjectResult d = dfield.getDescribe();
		//System.debug(d.fields.getMap().get(fieldNames).getDescribe().getType());	
//		Schema.DisplayType fielddataType = mapOfTokens.get(fieldNames).getDescribe().getType();
	  	//	System.debug('dfield====>'+fielddataType);
	  	}
	  	
		for (String fieldName: fieldMap.keySet()) {
  			Schema.DisplayType fielddataType = fieldMap.get(fieldName).getDescribe().getType();
  			System.debug('---fielddataType---'+fielddataType);
		}	  	
	  	return null;
  	}
  
}