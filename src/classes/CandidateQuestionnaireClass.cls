public with sharing class CandidateQuestionnaireClass {

    public id CandidateId {get;set;}
    public Contact objCan {get;set;}
    public Contact con2 {get; set;}
    public boolean flag {get; set;}
    
    public CandidateQuestionnaireClass(){
        
    }
    
    public void init(){
        objCan = new Contact();
        CandidateId = Apexpages.CurrentPage().GetParameters().Get('CandidateId'); 
        con2 = [SELECT Id, Name, Areyoucurrentlyatravelingnurse__c, Planontravelingwithinthenext__c, Contactedbyoneofourrecruiters__c,
                    CandidateQuestionnaireSubmitted__c
                FROM Contact
                WHERE Id =: CandidateId];
                
       flag = con2.CandidateQuestionnaireSubmitted__c;
    }
   
    public pagereference SaveRecord(){
        CandidateId = Apexpages.CurrentPage().GetParameters().Get('CandidateId'); 
        System.debug('Candidate Id: '+CandidateId);
        if(CandidateId != null){
            
            con2.Areyoucurrentlyatravelingnurse__c = objCan.Areyoucurrentlyatravelingnurse__c;
            con2.Planontravelingwithinthenext__c = objCan.Planontravelingwithinthenext__c;
            con2.Contactedbyoneofourrecruiters__c = objCan.Contactedbyoneofourrecruiters__c;
            System.debug('Candidate Id: '+objCan.id);   
            update con2;
            ApexPages.Message myMsg = new  ApexPages.Message(Apexpages.Severity.info,'Your Form has been Saved, Please sumbit your Record.');
            ApexPages.addMessage(myMsg);         
        } 
        else{
            ApexPages.Message myMsg = new  ApexPages.Message(Apexpages.Severity.info,'Wrong Candidate Id.');
            ApexPages.addMessage(myMsg);    
        }
        return null;
    }
    
    public pagereference Submit(){
        CandidateId = Apexpages.CurrentPage().GetParameters().Get('CandidateId'); 
        System.debug('Candidate Id: '+CandidateId);
        if(CandidateId != null){
            
            con2.Areyoucurrentlyatravelingnurse__c = objCan.Areyoucurrentlyatravelingnurse__c;
            con2.Planontravelingwithinthenext__c = objCan.Planontravelingwithinthenext__c;
            con2.Contactedbyoneofourrecruiters__c = objCan.Contactedbyoneofourrecruiters__c;
            con2.CandidateQuestionnaireSubmitted__c = true;
            System.debug('Candidate Id: '+objCan.id);   
            update con2;
            ApexPages.Message myMsg = new  ApexPages.Message(Apexpages.Severity.info,'Your Form has been Submitted.');
            ApexPages.addMessage(myMsg);         
        } 
        else{
            ApexPages.Message myMsg = new  ApexPages.Message(Apexpages.Severity.info,'Wrong Candidate Id.');
            ApexPages.addMessage(myMsg);    
        }
        pagereference pr = new pagereference('/apex/CandidateQuestionnaire1?CandidateId='+CandidateId);
        return pr;
        }
}