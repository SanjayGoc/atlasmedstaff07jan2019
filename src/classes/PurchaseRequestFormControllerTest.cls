@isTest
public class PurchaseRequestFormControllerTest {
    @isTest
    public static void purchseTest(){
        Profile profileId = [ SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1 ];
        
        User usr = new User ( LastName = 'LIVESTON',
				FirstName='JASON',
				Alias = 'jliv',
				Email = 'jason.liveston@asdf.com',
				Username = 'test@superhero.com',
				ProfileId = profileId.id,
				TimeZoneSidKey = 'GMT',
				LanguageLocaleKey = 'en_US',
				EmailEncodingKey = 'UTF-8',
				LocaleSidKey = 'en_US');
        insert usr;
        TRHC__Config_Settings__c setting = new TRHC__Config_Settings__c();
        setting.Name = 'Default';
        setting.DisabletrackEventActivityonContact__c = false;
        setting.DisabletrackTaskActivityonContact__c = false;
        setting.Enable_Update_Recruiter__c = false;
        setting.DisabletrackNoteonContact__c = false;
        setting.Trigger_Enable_Expected_End_Date_Changer__c = false;
        setting.Enable_Update_Recruiter__c = true;
        setting.purchase_order_approve_users__c = usr.id;
        insert setting;
        TRHC__CustomSettingsComponent__c setting1 = new TRHC__CustomSettingsComponent__c(Name='Default');
        setting1.TRHC__JobListFastSearch__c = true ;
        setting1.TRHC__Count_Jobs_for_Job_List_Page__c = null ;
        setting1.TRHC__isShowPagination__c = true ;
        setting1.TRHC__Enable_Jobs_location_search__c = true ;
        setting1.TRHC__Job_List_Page_fields_for_query__c = 'TRHC__Job_Title__c,TRHC__Job_Term__c,TRHC__Job_Category__c,TRHC__Country_Locale__c,TRHC__Job_Summary__c,TRHC__Job_City__c,TRHC__State__c,Pay_Unit__c,Job_Type__c,TRHC__Account_Job__r.name,Pay_Rate_High__c,Pay_Rate_Low__c' ;
        setting1.TRHC__ErrorsReceiver__c = 'test@test.com';
        setting1.TRHC__SessionTimeout__c = Decimal.valueOf('9999999');
        setting1.TRHC__Job_Search_Agent_Field__c = 'Name,Job_Title__c';        
        setting1.MatchedJobsCriteria__c = 'TRHC__Publish__c = True';
        insert setting1;
        
        Account account = new Account();
        account.Name = 'SwTest';
        insert account;
        
        Contact contact = new Contact();
        contact.FirstName = 'chaitanya';
        contact.LastName = 'bollamreddy';
        contact.TRHC__Recruiter__c = UserInfo.getUserId();
        contact.Discipline1__c = 'RN';
        contact.TRHC__Discipline__c = 'RN';
        contact.TRHC__Candidate_Status__c = 'Active';
        contact.Compliance_Coordinater__c = usr.id;
        contact.TRHC__Account_Manager__c = usr.id;
        contact.OwnerId = usr.id;
        contact.AccountId = account.id;
        contact.Permanant_Address__c = 'Muniseb Street';
       	contact.TRHC__Permanent_City__c = 'HYD';
        contact.State_pic__c = '';
		contact.TRHC__Permanent_Zip_Postal_Code__c = '533262';
		contact.Temporary_Address_1__c = '3rd cross papailayout';
		contact.TRHC__Temporary_City__c = 'Bangalore';
		contact.temporary_state__c = '';
        contact.TRHC__Temporary_Zip_Postal_Code__c = '560043';
		contact.RecordTypeId = [SELECT Id FROM RecordType where sobjecttype = 'CONTACT' and name in ('Candidate') LIMIT 1].Id;
        insert contact;
        
        TRHC__Scratch_Sheet__c scratchSheet = new TRHC__Scratch_Sheet__c();
        scratchSheet.Name = 'TestScratchSheet_test_Placement';
        scratchSheet.TRHC__Bill_Rate__c = 3000;
        scratchSheet.TRHC__Overtime_Rate__c = 80;
        scratchsheet.TRHC__Total_Overtime_Hours__c = 16;
        scratchsheet.TRHC__Pay_Rate__c = 50;
        scratchsheet.TRHC__Overtime_Pay_Rate__c = 80;
        scratchSheet.TRHC__Assignment_length__c  = 13;
        scratchSheet.TRHC__Hours_Per_Week__c = 40;
        scratchSheet.over_time__c = 8;
        insert scratchSheet;
        
        TRHC__Expense__c expense = new TRHC__Expense__c();
        expense.TRHC__Expense_Name__c = 'Housing Stipend';
        expense.TRHC__Hourly_Amount__c = 40.00;
        expense.TRHC__Scratch_Sheet__c = scratchSheet.id;
        insert expense;
        TRHC__Expense__c expensePer = new TRHC__Expense__c();
        expensePer.TRHC__Expense_Name__c = 'Per Diem';
        expensePer.TRHC__Hourly_Amount__c = 40.00;
        expensePer.TRHC__Scratch_Sheet__c = scratchSheet.id;
        insert expensePer;
        TRHC__Placement__c placement_scratchSheet_expenses = new TRHC__Placement__c();
        placement_scratchSheet_expenses.TRHC__Scratch_Sheet__c =  scratchSheet.id;
        placement_scratchSheet_expenses.TRHC__Contact_Candidate__c = contact.id;
        placement_scratchSheet_expenses.TRHC__Start_Date__c = Date.today();
        placement_scratchSheet_expenses.TRHC__End_Date__c = placement_scratchSheet_expenses.TRHC__Start_Date__c + 180;
        placement_scratchSheet_expenses.createdDate = Date.today();
        placement_scratchSheet_expenses.TRHC__Status__c = 'Active';
        placement_scratchSheet_expenses.Purchase_Request_Act_of_Kindness__c = 0.00;
        placement_scratchSheet_expenses.Purchase_Request_Airfare__c = 0.00;
        placement_scratchSheet_expenses.Purchase_Request_Atlas_Adventures__c = 0.00;
        placement_scratchSheet_expenses.Purchase_Request_Birthday__c = 0.00;
        placement_scratchSheet_expenses.Purchase_Request_Housing__c = 0.00;
        insert placement_scratchSheet_expenses;
        
        TRHC__Placement__c placement_scratchSheet_expenses1 = new TRHC__Placement__c();
        placement_scratchSheet_expenses1.TRHC__Scratch_Sheet__c =  scratchSheet.id;
        placement_scratchSheet_expenses1.TRHC__Contact_Candidate__c = contact.id;
        placement_scratchSheet_expenses1.TRHC__Start_Date__c = Date.today();
        placement_scratchSheet_expenses1.TRHC__End_Date__c = placement_scratchSheet_expenses.TRHC__Start_Date__c + 180;
        placement_scratchSheet_expenses1.createdDate = Date.today()-4;
        placement_scratchSheet_expenses1.TRHC__Status__c = 'Active';
        insert placement_scratchSheet_expenses1;
        
        TRHC__Placement__c placement_scratchSheet_expenses2 = new TRHC__Placement__c();
        placement_scratchSheet_expenses2.TRHC__Scratch_Sheet__c =  scratchSheet.id;
        placement_scratchSheet_expenses2.TRHC__Contact_Candidate__c = contact.id;
        placement_scratchSheet_expenses2.TRHC__Start_Date__c = Date.today();
        placement_scratchSheet_expenses2.TRHC__End_Date__c = placement_scratchSheet_expenses.TRHC__Start_Date__c + 180;
        placement_scratchSheet_expenses2.createdDate = Date.today()-10;
        placement_scratchSheet_expenses2.TRHC__Status__c = 'Pending';
        insert placement_scratchSheet_expenses2;
        
        TRHC__Placement__c placement_scratchSheet_expenses3 = new TRHC__Placement__c();
        placement_scratchSheet_expenses3.TRHC__Scratch_Sheet__c =  scratchSheet.id;
        placement_scratchSheet_expenses3.TRHC__Contact_Candidate__c = contact.id;
        placement_scratchSheet_expenses3.TRHC__Start_Date__c = Date.today();
        placement_scratchSheet_expenses3.TRHC__End_Date__c = placement_scratchSheet_expenses.TRHC__Start_Date__c + 180;
        placement_scratchSheet_expenses3.createdDate = Date.today()-1;
        placement_scratchSheet_expenses3.TRHC__Status__c = 'Active';
        insert placement_scratchSheet_expenses3;
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(contact);
        PurchaseRequestFormController pcontroller = new PurchaseRequestFormController(sc); 
        
        PageReference pageRef = Page.PurchaseRequestForm;
        pageRef.getParameters().put('id', String.valueOf(contact.Id));
        Test.setCurrentPage(pageRef);
        pcontroller.Description = 'test';
        pcontroller.purchaseItems = 'test';
        pcontroller.PurchaseLocation = 'test';
        pcontroller.delvDateTime ='Test';
        pcontroller.shippingAddress= 'test';
        pcontroller.message='Test';
        pcontroller.flight ='Test';
        pcontroller.departing ='test';
        pcontroller.arrival='test';
        pcontroller.returnDate ='test';
        pcontroller.hotel ='test';
        pcontroller.location ='test';
        pcontroller.dueDate = '2017-05-05';
        pcontroller.cost = 40.00;
        pcontroller.getItems();
        pcontroller.address = 'Permanent Address';
        pcontroller.contactName = 'test';
        pcontroller.contactEmail = 'chai@gmail.com';
        pcontroller.contactPhone = '9878787878';
        pcontroller.contactAddress = 'chaitanya';
        pcontroller.birthday = '12-01-1993';
        pcontroller.addAccount = account;
        pcontroller.requestType = 'Birthday';
        pcontroller.getItemsHospital();
        List<SelectOption> appUsersList = new List<SelectOption>();
        String Uname = usr.FirstName +' '+usr.LastName;
		appUsersList.add(new SelectOption(Uname,Uname)); 
        pcontroller.appUsers = appUsersList;
        pcontroller.getForm();
        pcontroller.selectedUser = Uname;
        pcontroller.submitForm(); 
        
        ApexPages.StandardController sc1 = new ApexPages.StandardController(account);
        PurchaseRequestFormController pcontroller1 = new PurchaseRequestFormController(sc1); 
        
        PageReference pageRef1 = Page.PurchaseRequestFormHospital;
        pageRef1.getParameters().put('id', String.valueOf(account.Id));
        Test.setCurrentPage(pageRef1);
        pcontroller1.Description = 'test';
        pcontroller1.purchaseItems = 'test';
        pcontroller1.PurchaseLocation = 'test';
        pcontroller1.delvDateTime ='Test';
        pcontroller1.shippingAddress= 'test';
        pcontroller1.message='Test';
        pcontroller1.flight ='Test';
        pcontroller1.departing ='test';
        pcontroller1.arrival='test';
        pcontroller1.returnDate ='test';
        pcontroller1.hotel ='test';
        pcontroller1.location ='test';
        pcontroller1.dueDate = '2017-05-05';
        pcontroller1.cost = 40.00;
        pcontroller1.getItems();
        pcontroller1.address = 'Permanent Address';
        pcontroller1.contactName = 'test';
        pcontroller1.contactEmail = 'chai@gmail.com';
        pcontroller1.contactPhone = '9878787878';
        pcontroller1.contactAddress = 'chaitanya';
        pcontroller1.birthday = '12-01-1993';
        pcontroller1.addAccount = account;
        pcontroller1.requestType = 'Birthday';
        pcontroller1.getItemsHospital();
        List<SelectOption> appUsersList1 = new List<SelectOption>();
        String Uname1 = usr.FirstName +' '+usr.LastName;
		appUsersList1.add(new SelectOption(Uname1,Uname1)); 
        pcontroller.appUsers = appUsersList;
        pcontroller1.getForm();
        pcontroller1.selectedUser = Uname;
        pcontroller1.submitForm(); 
        Test.stopTest();
        
    }
}