global class CalculateGPHBatch implements Database.Batchable<sObject> {
    
    public String query = '';
    private set<id> gphSet = new set<id>();
    public CalculateGPHBatch(set<Id> jobSet){
        gphSet = jobSet;
    }


    global List<TRHC__Scratch_Sheet__c> start(Database.BatchableContext BC) {
        query = 'SELECT id, OverTime_GPH__c,isGPHCalculated__c,TRHC__Client_Fee__c, TRHC__Pay_Rate__c, TRHC__Weekly_Lodging__c , TRHC__M_I_Weekly__c ,With_OverTime_Gross_Profit__c, TRHC__Assignment_length__c, TRHC__Hours_Per_Week__c, TRHC__Start_Date__c,TRHC__End_Date__c,State_pic__c,over_time__c,TRHC__Bill_Rate__c From TRHC__Scratch_Sheet__c where id in: gphSet' ;        
        return DataBase.Query(query);
    }
    
    global void execute(Database.BatchableContext BC, List <TRHC__Scratch_Sheet__c> scrlist) {

        TRHC__Scratch_Sheet__c scratchSheet = scrList[0];

        Boolean checkGSAService;
        TRHC__Healthcare_Setting__c customSetting = TRHC__Healthcare_Setting__c.getAll().get('Default');
        Decimal payBurdern = 0;
        string getLabel = System.Label.Editable_Expense;
        Set<string> editAccess = new  Set<string>();
        
        if(getLabel != null) {
            editAccess.addAll(getLabel.Split(','));
        }

        List<TRHC__Expense__c> listExpense = [SELECT
                                                   Id, 
                                                   TRHC__Expense_Name__c, 
                                                   TRHC__Amount__c, 
                                                   TRHC__Occurence__c, 
                                                   TRHC__Grouping_Total__c, 
                                                   TRHC__Value__c,
                                                   TRHC__ProRated_Hours__c
                                                   FROM
                                                   TRHC__Expense__c
                                                   WHERE
                                                   TRHC__Scratch_Sheet__c =: scratchSheet.id AND 
                                                   TRHC__Expense_Name__c in: editAccess];

        List<TRHC__Expense__c> listexpenseVMS = new List<TRHC__Expense__c>();
        for(TRHC__Expense__c expense: listExpense) {
            if(expense.TRHC__Expense_Name__c != 'Travel Reimbursement') {
                payBurdern += expense.TRHC__Amount__c;
            }
        }

        if(!listexpenseVMS.isEmpty()){
            update listexpenseVMS;
        }

        payBurdern = Math.round(payBurdern);

        if(!scratchSheet.isGPHCalculated__c) {  
            checkGSAService = (Boolean)customSetting.get('TRHC__Enable_GSA_Service_to_Update_M_I_Rate__c');
            decimal dollar = 0.00;
            if(scratchSheet.TRHC__Bill_Rate__c > 60){
               dollar = 17.00;
            }
            else {
                dollar = 15.00;
            }
            List<TRHC__Expense__c> allExpenseList = [select TRHC__Expense_Name__c,
                                                                TRHC__Scratch_Sheet__r.TRHC__Bill_Rate__c,
                                                                TRHC__Scratch_Sheet__r.TRHC__Total_Regular_Hours__c,
                                                                TRHC__Scratch_Sheet__r.TRHC__Assignment_length__c,
                                                                TRHC__Scratch_Sheet__r.TRHC__Total_Overtime_Hours__c,
                                                                TRHC__Scratch_Sheet__r.TRHC__No_Bill_Orientation_Hours__c,
                                                                TRHC__Amount__c,
                                                                TRHC__Grouping_Total__c,
                                                                TRHC__Scratch_Sheet__r.OverTIme_GPH__c,
                                                                TRHC__Scratch_Sheet__r.Over_Time_Include_Bill__c,
                                                                TRHC__Scratch_Sheet__r.TRHC__Client_Fee__c,
                                                                TRHC__Occurence__c, 
                                                                TRHC__Scratch_Sheet__r.TRHC__Total_Bill_Amount__c,
                                                                TRHC__Scratch_Sheet__r.TRHC__Pay_Rate__c,   
                                                                TRHC__Unit_of_Measure__c,
                                                                TRHC__Value__c,
                                                                TRHC__ProRated_Hours__c,
                                                                TRHC__Calculation_parameter__c,
                                                                TRHC__Scratch_Sheet__r.Pay_Burden_New__c,
                                                                TRHC__Scratch_Sheet__r.TRHC__Pay_Burden__c,
                                                                TRHC__Scratch_Sheet__r.Non_Taxable_Expense__c,
                                                                TRHC__Scratch_Sheet__r.TRHC__Hours_Per_Week__c,
                                                                TRHC__Scratch_Sheet__r.over_time__c,
                                                                TRHC__Scratch_Sheet__r.TRHC__Overtime_Pay_Rate__c
                                                        from TRHC__Expense__c 
                                                        where TRHC__Scratch_Sheet__c =:  scratchSheet.Id];
            Double otherExpense = 0.00;
            Double vmsFeesVal = 0.00;
            Double perValue = 0.00; 
            Double billValue = 0.00;
            Double totalHours = 0.00;
            Double hourlAmt = 0.00;
            Double totPayment = 0.00;
            for(TRHC__Expense__c expObj : allExpenseList){
                if(expObj.TRHC__Grouping_Total__c == 'Other Expense') {
                    totalHours = (expObj.TRHC__Scratch_Sheet__r.TRHC__Assignment_length__c * expObj.TRHC__Scratch_Sheet__r.TRHC__Total_Regular_Hours__c) + (expObj.TRHC__Scratch_Sheet__r.TRHC__Assignment_length__c * expObj.TRHC__Scratch_Sheet__r.TRHC__Total_Overtime_Hours__c) - expObj.TRHC__Scratch_Sheet__r.TRHC__No_Bill_Orientation_Hours__c;
                    billValue = expObj.TRHC__Scratch_Sheet__r.TRHC__Bill_Rate__c * totalHours;
                    
                    if(expObj.TRHC__Unit_of_Measure__c == 'Percent'){
                        if(expObj.TRHC__Calculation_parameter__c == 'Total Revenue'){
                            perValue = (expObj.TRHC__Value__c * billValue) / 100;
                            otherExpense += perValue;
                            system.debug('----------otherExpense---------'+otherExpense);
                            system.debug('expObj----------name---------'+expObj.TRHC__Expense_Name__c);
                            //system.debug('----------perValue---------'+perValue);
                        }
                        if(expObj.TRHC__Calculation_parameter__c == 'Total Hourly Pay Amount'){
                            hourlAmt = (totalHours * expObj.TRHC__Value__c * expObj.TRHC__Scratch_Sheet__r.TRHC__Pay_Rate__c ) / 100;
                            otherExpense += hourlAmt;
                            system.debug('----------hourlAmt---------'+hourlAmt);
                            system.debug('expObj----------name---------'+expObj.TRHC__Expense_Name__c);
                            //system.debug('----------otherExpense---------'+otherExpense);
                        }
                        if(expObj.TRHC__Calculation_parameter__c == 'Total Payments'){
                            Double payburden = 0.00;
                            //system.debug('======== expObj.TRHC__Scratch_Sheet__r.TRHC__Assignment_length__c ========'+expObj.TRHC__Scratch_Sheet__r.TRHC__Assignment_length__c);
                            if(expObj.TRHC__Scratch_Sheet__r.TRHC__Assignment_length__c != null && 
                                expObj.TRHC__Scratch_Sheet__r.TRHC__Hours_Per_Week__c != null && 
                                expObj.TRHC__Scratch_Sheet__r.TRHC__Pay_Rate__c != null && 
                                expObj.TRHC__Scratch_Sheet__r.TRHC__Assignment_length__c != null  && 
                                expObj.TRHC__Scratch_Sheet__r.over_time__c != null && 
                                expObj.TRHC__Scratch_Sheet__r.TRHC__Overtime_Pay_Rate__c != null) 
                            payburden = ( expObj.TRHC__Scratch_Sheet__r.TRHC__Assignment_length__c * expObj.TRHC__Scratch_Sheet__r.TRHC__Hours_Per_Week__c * expObj.TRHC__Scratch_Sheet__r.TRHC__Pay_Rate__c ) + ( expObj.TRHC__Scratch_Sheet__r.TRHC__Assignment_length__c * expObj.TRHC__Scratch_Sheet__r.over_time__c * expObj.TRHC__Scratch_Sheet__r.TRHC__Overtime_Pay_Rate__c );
                            totPayment = (expObj.TRHC__Value__c * (expObj.TRHC__Scratch_Sheet__r.Pay_Burden_New__c + payburden + expObj.TRHC__Scratch_Sheet__r.Non_Taxable_Expense__c))/100;
                            otherExpense += totPayment;
                            system.debug('----------payburden---------'+payburden);
                            system.debug('----------totPayment---------'+totPayment);
                        }
                    } else {

                        otherExpense = otherExpense + expObj.TRHC__Amount__c;
                        system.debug('----------otherExpense---------'+otherExpense);
                        system.debug('----------totPayment---------'+totPayment);
                    }
                }
            }
            
            List<TRHC__Expense__c> listUpdate = new  List<TRHC__Expense__c>();
                
            decimal totalGPHAdd = 0;
            decimal addToNonTaxable = 0;
            if(scratchSheet.over_time__c == null){
                scratchSheet.over_time__c = 0;
            }
            decimal calculation = (dollar * (scratchSheet.TRHC__Assignment_length__c * (scratchSheet.TRHC__Hours_Per_Week__c + scratchSheet.over_time__c)));
            if(dollar > scratchSheet.OverTime_GPH__c) {
                totalGPHAdd = (calculation - (scratchSheet.With_OverTime_Gross_Profit__c + otherExpense));
                totalGPHAdd = totalGPHAdd.round() * (- 1);
                calculateGPH(listExpense, scratchSheet, totalGPHAdd, payBurdern);

            } else if(dollar < scratchSheet.OverTime_GPH__c) {

                totalGPHAdd = ((otherExpense + scratchSheet.With_OverTime_Gross_Profit__c) - calculation);
                totalGPHAdd = totalGPHAdd.round();
                
                calculateGPH(listExpense, scratchSheet, totalGPHAdd, payBurdern);       
                
            }
        }    
        /*List<TRHC__Expense__c> allExpenseList = [select TRHC__Expense_Name__c,TRHC__Amount__c,TRHC__Grouping_Total__c,TRHC__Scratch_Sheet__r.OverTIme_GPH__c,TRHC__Scratch_Sheet__r.Over_Time_Include_Bill__c,TRHC__Scratch_Sheet__r.TRHC__Client_Fee__c,TRHC__Occurence__c, TRHC__Value__c,TRHC__ProRated_Hours__c from TRHC__Expense__c where TRHC__Scratch_Sheet__c =:  scratchSheet.id];
        Double otherExpense = 0.00;
        Double vmsFeesVal = 0.00;
        Double billValue = 0.00;
        for(TRHC__Expense__c expObj : allExpenseList) {
            if(expObj.TRHC__Grouping_Total__c == 'Other Expense'){
                otherExpense = otherExpense + expObj.TRHC__Amount__c;
                billValue = expObj.TRHC__Scratch_Sheet__r.TRHC__Bill_Rate__c*((expObj.TRHC__Scratch_Sheet__r.TRHC__Assignment_length__c * expObj.TRHC__Scratch_Sheet__r.TRHC__Total_Regular_Hours__c) + (expObj.TRHC__Scratch_Sheet__r.TRHC__Assignment_length__c * expObj.TRHC__Scratch_Sheet__r.TRHC__Total_Overtime_Hours__c) - expObj.TRHC__Scratch_Sheet__r.TRHC__No_Bill_Orientation_Hours__c);
                if(expObj.TRHC__Expense_Name__c == 'VMS Fees'){

                    vmsFeesVal = (expObj.TRHC__Value__c * billValue) / 100;
                    otherExpense += vmsFeesVal;
                }

            }
            system.debug('expObj ---------- vmsFeesVal ---------'+vmsFeesVal);
            system.debug('expObj ---------- billValue ---------'+billValue);
            system.debug('========== otherExpense in for loop ==========='+otherExpense);
        }
        system.debug('========== otherExpense ==========='+otherExpense);

        if(scratchSheet != null){
            system.debug('========== scratchSheet.OtherExpense__c ========'+scratchSheet.OtherExpense__c);
            if(otherExpense != null){
                scratchSheet.OtherExpense__c = otherExpense;
                //update scratchSheet;
            }
            system.debug('========== scratchSheet.OtherExpense__c ========'+scratchSheet.OtherExpense__c);
        }*/
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }

    public void calculateGPH(List<TRHC__Expense__c> listExpenses, TRHC__Scratch_Sheet__c sheet, decimal totalGPH, decimal totalExpense) {

        List<TRHC__Expense__c> listUpdate = new  List<TRHC__Expense__c>();
        //List<TRHC__Expense__c> expList = new  List<TRHC__Expense__c>(); ////////duplicate ud issue
        Double housingDifferece = 0.00;
        Double perDiemDifference = 0.00;
        for(TRHC__Expense__c singleExpense: listExpenses) {
            if(singleExpense.TRHC__Expense_Name__c != 'Travel Reimbursement') {
                
                decimal expenseValue = (singleExpense.TRHC__Amount__c  + (((singleExpense.TRHC__Amount__c/ totalExpense) * 100) * totalGPH) / 100) / singleExpense.TRHC__Occurence__c;                
                if(singleExpense.TRHC__Expense_Name__c == 'Housing Stipend' && expenseValue > sheet.TRHC__Weekly_Lodging__c){
                    singleExpense.TRHC__Value__c = sheet.TRHC__Weekly_Lodging__c;//Math.round(expenseValue);
                    housingDifferece = expenseValue - sheet.TRHC__Weekly_Lodging__c;
                }else if(singleExpense.TRHC__Expense_Name__c == 'Per diem' && expenseValue > sheet.TRHC__M_I_Weekly__c){
                    singleExpense.TRHC__Value__c = sheet.TRHC__M_I_Weekly__c;
                    perDiemDifference = expenseValue - sheet.TRHC__M_I_Weekly__c;
                    
                }else{
                    singleExpense.TRHC__Value__c = Math.round(expenseValue);
                }
                
                listUpdate.add(singleExpense);

            }

            /*if(singleExpense.TRHC__Value__c < 0.00){
                singleExpense.TRHC__Value__c = 0.00;
                expList.add(singleExpense);
            }*/
        }

        if(!listUpdate.isEmpty()) {
            Update listUpdate;
            sheet.isGPHCalculated__c = true;
            if(sheet.TRHC__Pay_Rate__c != null && sheet.TRHC__Hours_Per_Week__c != null)
            sheet.TRHC__Pay_Rate__c = sheet.TRHC__Pay_Rate__c + (housingDifferece + perDiemDifference)/sheet.TRHC__Hours_Per_Week__c;
            Update sheet;
        }

        /*if(!expList.isEmpty()){
            update expList;    
        }*/
    }
}