@istest
public class UpdateCandidateLastNoteTest {
    /*
     *@description : inserting a note with parent id as contact
     */
     
    @istest
    public static void notesOfCandidate(){
        TRHC__Config_Settings__c setting = new TRHC__Config_Settings__c();
        setting.Name = 'Default';
        setting.DisabletrackEventActivityonContact__c = false;
        setting.DisabletrackTaskActivityonContact__c = false;
        setting.Enable_Update_Recruiter__c = false;
        setting.DisabletrackNoteonContact__c = false;
        insert setting;
        
        Contact contact = new Contact();
        contact.FirstName = 'chaitanya';
        contact.LastName = 'bollamreddy';
        contact.RecordTypeId = [SELECT Id FROM RecordType where sobjecttype = 'CONTACT' and name in ('Candidate') LIMIT 1].Id;
        insert contact;
        Note note = new Note();
        note.parentid = contact.id;
        note.Title = 'test';
        insert note;
    }
}