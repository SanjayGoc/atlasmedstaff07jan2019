@isTest
public class HousingInfoForCandiHandlerTest {
    static testmethod void HousingInfoForCantest() {
        //Get User Record        
        List<User> usr = [SELECT Id,Name,Email,FirstName,LastName FROM User WHERE IsActive = true LIMIT 3];
        
        TRHC__Config_Settings__c config = new TRHC__Config_Settings__c();
        config.name = 'Default';
        config.Trigger_Enable_Expected_End_Date_Changer__c = true;
        insert config;
                
        //Create Candidate/Nurse Record
        Contact con = new Contact();
        con.FirstName = 'mike'; 
        con.LastName = 'saas';
        con.Email = 'test@test1.gmail.com';
        con.Compliance_Coordinater__c = usr[0].id;
        con.TRHC__Recruiter__c = usr[1].id;
        insert con;
        
        //Create Job Record
        TRHC__Job__c job = new TRHC__Job__c();
        //job.Name = '88486';
        job.TRHC__Stage__c = 'open';
        job.CreatedDate = Date.today();
        job.TRHC__Number_of_Placements__c = 1;
        job.State_pic__c = 'Alabama';
        insert job;
        
        
        
        TRHC__Job_Applicant__c jobApplication = new TRHC__Job_Applicant__c();
      
        jobApplication.TRHC__Job__c = job.Id;
        jobApplication.TRHC__Contact_Candidate__c = con.Id;
        jobApplication.TRHC__Recruiter__c = usr[1].Id;
        jobApplication.TRHC__Account_Manager__c = usr[2].Id;
        insert jobApplication;
        
        Housing__c housing = new Housing__c();
        //housing.name = 'H-00000';
        //housing.Housing_Name__c = 'Ashbury Hotel and Suites';
        housing.State__c = 'Alabama';
        housing.Housing_Facility_Used_Time__c = 1;
        housing.Candidate_Used_Housing_Facilities__c = con.name;
        insert housing;
        
        Housing__c housing2 = new Housing__c();
        //housing.name = 'H-00000';
        //housing.Housing_Name__c = 'Ashbury Hotel and Suites';
        housing2.State__c = 'Alabama';
        housing2.Housing_Facility_Used_Time__c = 1;
        housing2.Candidate_Used_Housing_Facilities__c = con.name;
        insert housing2;
        
        job.TRHC__Stage__c = 'filled';
        update job;
        
        TRHC__Housing__c targetHousing = new TRHC__Housing__c();
        targetHousing.TRHC__Candidate__c = con.id;
        targetHousing.TRHC__State__c ='Alabama';
        insert targetHousing; 
    }
}