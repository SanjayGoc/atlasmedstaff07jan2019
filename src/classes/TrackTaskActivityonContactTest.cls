@isTest
public class TrackTaskActivityonContactTest {
  
    public static testMethod void TrackTaskActivityonContactTest(){
        
        TRHC__Config_Settings__c setting = new TRHC__Config_Settings__c();
        setting.Name = 'Default';
        setting.DisabletrackEventActivityonContact__c = false;
        setting.DisabletrackTaskActivityonContact__c = false;
        setting.Enable_Update_Recruiter__c = false;
        insert setting;
        
        Account a = new Account(name='Test Acc');
        insert a;
    
        Contact c = new Contact(LastName='Test Con');
        insert c;
        
        Task t = new Task(Subject='Test Subject',Status='New',Priority='Normal',CallType='Outbound',WhoId=c.id,Description='test description');
        insert t;
         
        Event e = new Event(Subject='Test Subject', type='Email',WhoId=c.id,DurationInMinutes=0,ActivityDateTime=System.today(),Description = 'test dec');        
        insert e;
        
        t.WhatId = a.id;
        update t;
    }
}