@isTest
Public class CommunityClearNotifiNotiRESTOpTest{

    public static testmethod void ClearnotificationTest(){
        
        TRHC__Config_Settings__c configSettings = new TRHC__Config_Settings__c();
        configSettings.name='Default';
        configSettings.Enable_AutoMateJobRates__c = true;
        configSettings.Trigger_Enable_ScratchSheetAutoAddExp__c = false;
        configSettings.AutoAddExpenseCriteria__c='TRHC__Margin_Type__c=\'Expense\'';
        configSettings.Except_Hospitals__c = 'Atlas Development';
        configSettings.Trigger_Enable_Expected_End_Date_Changer__c = false;
        configSettings.Trigger_Enable_Applicant_From_SCR_Sheet__c = true;
        configSettings.Update_Pay_Burden__c = false;
        configSettings.Enable_RefferalTrigger__c = true;
        insert configSettings;


        List<Account> accountList = TestUtilityClass.getTestMultipleAccount();
        
        contact conobj = new contact();
        conobj.lastName = 'sharma';
        conobj.TRHC__Password__c = '12345678';
        conobj.Email = 'HIREN.GOC@GMail.com';
        conobj.apiToken__c = '1111111111';
        insert conobj;

        Notification__c note = new Notification__c();
        note.Contact__c = conobj.id;
        insert note;


        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/mobile/ClearNotification';
        req.addParameter('apiToken', conobj.apiToken__c);
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;

        CommunityClearNotifiNotiRESTOperation.ResponseHandler wrapperobj = CommunityClearNotifiNotiRESTOperation.CommunityClearNotifiNotiRESTOperation();   
    }

     public static testmethod void ClearnotificationTest2(){
        
        TRHC__Config_Settings__c configSettings = new TRHC__Config_Settings__c();
        configSettings.name='Default';
        configSettings.Enable_AutoMateJobRates__c = true;
        configSettings.Trigger_Enable_ScratchSheetAutoAddExp__c = false;
        configSettings.AutoAddExpenseCriteria__c='TRHC__Margin_Type__c=\'Expense\'';
        configSettings.Except_Hospitals__c = 'Atlas Development';
        configSettings.Trigger_Enable_Expected_End_Date_Changer__c = false;
        configSettings.Trigger_Enable_Applicant_From_SCR_Sheet__c = true;
        configSettings.Update_Pay_Burden__c = false;
        configSettings.Enable_RefferalTrigger__c = true;
        insert configSettings;


        List<Account> accountList = TestUtilityClass.getTestMultipleAccount();
        
        contact conobj = new contact();
        conobj.lastName = 'sharma';
        conobj.TRHC__Password__c = '12345678';
        conobj.Email = 'HIREN.GOC@GMail.com';
        conobj.apiToken__c = '1111111111';
        insert conobj;

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/mobile/ClearNotification';
        req.addParameter('apiToken', conobj.apiToken__c);
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;

        CommunityClearNotifiNotiRESTOperation.ResponseHandler wrapperobj = CommunityClearNotifiNotiRESTOperation.CommunityClearNotifiNotiRESTOperation();   
    }
}