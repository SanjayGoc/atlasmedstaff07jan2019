public With sharing class MatchingJobsforCandidateNew {
    Public String searchKeyWord {set;get;}
    Public Map<Contact,List<TRHC__job__c>> candiMatchMap{get;set;}
    List<contact> searchMapContacts {get;set;}
    TRHC__CustomSettingsComponent__c config = TRHC__CustomSettingsComponent__c.getInstance('Default');
    String whereCriteria = config.MatchedJobsCriteria__c;
    
    Public void searchCandidates(){
        String status = 'Active';
        String candiFromCriteria = 'id, Name, Primary_Speciality1__c, TRHC__Specialty__c, Discipline1__c, CreatedDate, TRHC__Discipline__c';
        String candiWhereCriteria = ' TRHC__Candidate_Status__c = '+ '\''+status+'\'' + ' order by createddate desc Limit 2 ';
        if(searchKeyWord != null && searchKeyWord.trim() !=''){
            String serchKeyWhere = 'Where (name LIKE ' + '\''+ '%'+searchKeyWord+'%'+'\'' + ' OR ' + 'lastName LIKE ' + '\''+ '%'+searchKeyWord+'%'+'\'' +') And TRHC__Recruiter__c ='+'\''+UserInfo.getUserId()+ '\''+' And TRHC__Discipline__c!=null And ';
            candiWhereCriteria = serchKeyWhere+candiWhereCriteria;
        }
        else{
            candiWhereCriteria = ' Where TRHC__Recruiter__c =' + '\''+UserInfo.getUserId()+ '\'' + ' And TRHC__Discipline__c!=null And '+candiWhereCriteria;
        }
        String candiquery = 'Select '+
                ' '+candiFromCriteria + 
                ' '+'From Contact' +
                ' '+candiWhereCriteria;
        system.debug('candiquery'+candiquery);
        List<contact> matchedContacts = Database.query(candiquery);
        List<String> candiToJobCriteria = new List<String> ();
        List<String> criteria = new List<String>();
        candiToJobCriteria.add('TRHC__Discipline__c');
        candiToJobCriteria.add('Speciality1__c');
        candiMatchMap = new Map<Contact,List<TRHC__job__c>>();
        for(Contact candi : matchedContacts){
            String temp = '';
            integer count = 0;
            for(String candiField : candiToJobCriteria){
                count++;
                
                if(count != candiToJobCriteria.size()){
                    if(candiField == 'Speciality1__c'){
                        temp += 'Speciality1__c'+' includes ('+ '\'' +(String)candi.get('TRHC__Specialty__c')+ '\'' +') And ';
                    }else{
                        temp += candiField+' = '+ '\'' +(String)candi.get(candiField)+ '\'' +' And ' ;       
                    }
                   
                }
                else{
                    if(candiField == 'Speciality1__c'){
                        temp += 'Speciality1__c'+' includes ('+ '\'' +(String)candi.get('TRHC__Specialty__c')+ '\''+')';  
                    }else{
                        temp += candiField+' = '+ '\'' +(String)candi.get(candiField)+ '\'';  
                    }
                   
                }
                

                
            }
            //List<TRHC__job__c> jobs = searchJobs((String)candi.get('TRHC__Discipline__c'));
            List<TRHC__job__c> jobs = searchJobs(temp);
            candiMatchMap.put(candi,jobs);
            
        }
    }
    
    Public List<Trhc__job__c> searchJobs(String criteria){
        String jobFromCriteria = 'Id, TRHC__Discipline__c, TRHC__Job_Title__c, TRHC__Job_Type__c, TRHC__Recruiter__c, Name, Speciality1__c, TRHC__Specialty__c, TRHC__Start_Date__c  ';
        String jobWhereCriteria = whereCriteria + ' And ' + criteria+ ' Order by createddate desc limit 100' ;    
        String query = 'Select '+ 
                ' '+jobFromCriteria +
                ' From TRHC__Job__c Where'+
                ' '+jobWhereCriteria;
        system.debug('query'+query);
        return Database.query(query);
        
    }

    class CandiJobClass{
        /*String criteria{get;set;}
        List<TRHC__job__c> jobs{get;set;}*/
        
    }

}