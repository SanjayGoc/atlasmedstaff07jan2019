//deleted field on contact [ Annual Review Date - Annual_Review_Date_del__c - Date/Time]
@RestResource(urlMapping='/mobile/changePassword/*')
global class CommunitychangePasswordRESTOperation {
    @HttpGet
    global static ResponseHandler CommunitychangePasswordRESTOperation(){
        String apiToken = RestContext.request.params.get('apiToken');
        String oldPassword = RestContext.request.params.get('oldPassword');
        String newPassword = RestContext.request.params.get('newPassword');
        String confirmPassword = RestContext.request.params.get('confirmPassword');

        ResponseHandler res = new ResponseHandler();

        List<Contact> contactList = [select id,
                                            name,
                                            apiToken__c ,
                                            TRHC__Password__c
                                    From Contact 
                                    where apiToken__c =: apiToken 
                                    ];
        
        if(!contactList.isEmpty()){
            if(oldPassword != contactList[0].TRHC__Password__c){
                res.success = 0;
                res.message = new String[]{'Password not updated.'};
                return res;
            }

            if(newPassword == confirmPassword){
                contactList[0].TRHC__Password__c = newPassword;
            }
            update contactList[0] ;
            res.success = 1;
            res.message = new string[]{'Change Password Succesfully'};
            return res;
        }

        res.success = 2;
        res.message = new string[]{'Unauthorised User'};
        return res;
    }

    global class ResponseHandler {
        global integer success;
        global string[] message;
        global ResponseHandler () {

        }
    }
}