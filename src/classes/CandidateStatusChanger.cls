public class CandidateStatusChanger implements Database.Batchable<sObject> {
    public List<Contact> start(Database.BatchableContext ctx){
        Date tDay = Date.today();
        Integer dayLimit = Integer.valueOf(System.label.DaysLimitForScratchSheet);
        Date deadLinedate = Date.today() - dayLimit;
        String ignoreStatuses = trhc__config_Settings__c.getInstance('Default').Excep_status_for_batch_CandidateStatusCh__c != null?trhc__config_Settings__c.getInstance('Default').Excep_status_for_batch_CandidateStatusCh__c:'Pending Review Board';
        List<String> statusListign = ignoreStatuses.split(';');
        Map<String,TRHC__Placement__c> placeMap = new Map<String,TRHC__Placement__c> ();
        for(TRHC__Placement__c placement : [Select Name, TRHC__Contact_Candidate__c, TRHC__End_Date__c From TRHC__Placement__c Where TRHC__End_Date__c !=Null]) {
            if(placement.TRHC__Contact_Candidate__c != null) {
                if(!placeMap.containsKey(placement.TRHC__Contact_Candidate__c)){
                    placeMap.put(placement.TRHC__Contact_Candidate__c, placement);
                }else{
                    if(placeMap.get(placement.TRHC__Contact_Candidate__c).TRHC__End_Date__c < placement.TRHC__End_Date__c){
                        placeMap.put(placement.TRHC__Contact_Candidate__c, placement);
                    }
                }
            }
        }
        System.debug('placementMap@@@'+placeMap);
        Set<Contact> conatctsSet = new Set<Contact> ([Select  Name, TRHC__Candidate_Status__c From Contact Where id IN : placeMap.keySet()]);
        set<Contact> contactToUpdate = new Set<Contact>();
        for(Contact contact:conatctsSet) {
            if(placeMap.get(contact.id).TRHC__End_Date__c > Date.today()) { 
                system.debug('@@@SSSS'+contact);
                if(contact.TRHC__Candidate_Status__c != 'Active') {
                    system.debug('@@@SSSSIIII'+contact);
                    contact.TRHC__Candidate_Status__c = 'Active';
                    contactToUpdate.add(contact); 
                }           
            }
            else{
                boolean run = true;
                for(String str :statusListign){
                    if(str == contact.TRHC__Candidate_Status__c)
                    run = False;
                }
                if(contact.TRHC__Candidate_Status__c != 'Former Atlas Traveler' && /*contact.TRHC__Candidate_Status__c !='Pending Review Board'*/run){
                    contact.TRHC__Candidate_Status__c = 'Former Atlas Traveler';
                    contactToUpdate.add(contact);
                }
                
            }
            
        }
        return new List<Contact>(contactToUpdate);
    }
    
    public void execute(Database.BatchableContext ctx, List<Contact> contactList){
        
        update contactList;
    }
    
    public void finish(Database.BatchableContext ctx){
        
    }
}