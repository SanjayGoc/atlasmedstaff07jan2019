@isTest
public class PopulateAllowanceHandlerTest {
    @isTest static void PopulateAllowanceMethod() {
        User usr = [SELECT Id,Name,Email,FirstName,LastName FROM User Limit 1];
        
        TRHC__config_Settings__c  configSet = new TRHC__config_Settings__c();
        configSet.Name = 'Default';
        configSet.Disable_PopolaterecruiterPlacement__c = true;
        configSet.EnableUpdateOverTimeRate__c = false;
        insert configSet;
        
        TRHC__Healthcare_Setting__c congHelth = new TRHC__Healthcare_Setting__c();
        congHelth.Name = 'Default';
        congHelth.TRHC__Enable_GSA_Service_to_Update_M_I_Rate__c = true;
        insert congHelth;
        
        Contact con = new Contact();
        con.FirstName = 'mike'; 
        con.LastName = 'saas';
        con.Email = 'test@test1.gmail.com';
        con.Compliance_Coordinater__c = usr.id;
        con.TRHC__Recruiter__c = usr.id;
        insert con;
        
        TRHC__Job__c job = new TRHC__Job__c();
        job.TRHC__Stage__c = 'closed';
        job.TRHC__Recruiter__c = usr.id;
        job.CreatedDate = Date.today();
        job.TRHC__Number_of_Placements__c = 1;
        insert job;
        
        TRHC__Scratch_Sheet__c scratch = new TRHC__Scratch_Sheet__c();
        scratch.Name = 'Test Scratch sheet';
        scratch.TRHC__Bill_Rate__c = 22.00;
        scratch.TRHC__Overtime_Rate__c = 22.00;
        //insert scratch;
        
        TRHC__Expense__c expPer = new TRHC__Expense__c();
        expPer.TRHC__Expense_Name__c = 'Per Diem';
        expPer.TRHC__Weekly_Amount__c = 200.00;
        expPer.TRHC__Scratch_Sheet__c = scratch.Id;
        insert expPer;
        
        TRHC__Expense__c expHouse = new TRHC__Expense__c();
        expHouse.TRHC__Expense_Name__c = 'Housing Stipend';
        expHouse.TRHC__Weekly_Amount__c = 300.00;
        expHouse.TRHC__Scratch_Sheet__c = scratch.Id;
        insert expHouse;
        
        TRHC__Placement__c plac = new TRHC__Placement__c();
        plac.TRHC__Recruiter__c = usr.id;
        plac.TRHC__Contact_Candidate__c = con.id;
        plac.TRHC__Job__c = job.id;
        plac.TRHC__Scratch_Sheet__c = scratch.Id;
        insert plac; 
    }
}