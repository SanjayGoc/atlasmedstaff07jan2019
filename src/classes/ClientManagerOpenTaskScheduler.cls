global class ClientManagerOpenTaskScheduler implements Schedulable {
	global void execute(SchedulableContext sc) {
		ClientManagerOpenTaskBatch clientManager = new ClientManagerOpenTaskBatch();
		Database.executebatch(clientManager,200);
	}
}