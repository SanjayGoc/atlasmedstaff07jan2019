global class UpdateGSAServiceBatchSchedule implements Schedulable {
    global void execute(SchedulableContext sc) {
        UpdateGSAServiceBatch sch = new UpdateGSAServiceBatch();
        database.executebatch(sch, 1);
    }  
}