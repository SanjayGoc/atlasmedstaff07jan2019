@isTest
public class RestrictDelationTest {
    @isTest
    public static void restrictTest(){
        Test.startTest();
        TRHC__config_Settings__c configSettings = new  TRHC__config_Settings__c();
        configSettings.name ='Default';
        configSettings.disable_UpdateContactOwner__c = false;
        insert configSettings;
        Id recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Candidate').getRecordTypeId();
        System.debug('recordTypeId'+recordTypeId);
        Contact singleContact = new Contact();
        singleContact.RecordTypeId = recordTypeId;
        singleContact.LastName = 'Shah';
        insert singleContact;
        configSettings.con_not_delete_coverpage__c = singleContact.id;
        update configSettings;
        try{
        delete singleContact;
        }catch(Exception e){
            System.debug('e'+e.getCause());
        }
        Test.stopTest();
        
    }
}