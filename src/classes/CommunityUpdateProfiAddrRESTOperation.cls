@RestResource(urlMapping='/mobile/UpdateProfileAddress/*')
global class CommunityUpdateProfiAddrRESTOperation {
    @HttpGet
    global static ResponseHandler CommunityUpdateProfiAddrRESTOperation (){
        ResponseHandler res = new ResponseHandler();

        permenentAddress perAddObj = new permenentAddress(); 
        temporaryAddress tempAddObj = new temporaryAddress();

        String apiToken = RestContext.request.params.get('apiToken');
        string perAddress1 = RestContext.request.params.get('perAddress1');
        string perAddress2 = RestContext.request.params.get('perAddress2');
        string perCity = RestContext.request.params.get('perCity');
        string perState = RestContext.request.params.get('perState');
        string perZipcode = RestContext.request.params.get('perZipcode');
        string tmpAddress1 = RestContext.request.params.get('tmpAddress1');
        string tmpAddress2 = RestContext.request.params.get('tmpAddress2');
        string tmpCity = RestContext.request.params.get('tmpCity');
        string tmpState = RestContext.request.params.get('tmpState');
        string tmpZipcode = RestContext.request.params.get('tmpZipcode');
        
        List<Contact> userList = [Select Id,
                                         Name,
                                         firstName,
                                         lastName,
                                         apiToken__c,
                                         Email,
                                         Phone,
                                         Discipline1__c,
                                         Primary_Speciality1__c,
                                         Secondary_Speciality__c,
                                         TRHC__Shift_Preference__c,
                                         Permanant_Address__c,
                                         TRHC__Permanent_Address_2__c, 
                                         TRHC__Permanent_City__c,
                                         State_pic__c,
                                         TRHC__Permanent_Zip_Postal_Code__c,
                                         Temporary_Address_1__c,
                                         Temporary_Address_2__c,
                                         TRHC__Temporary_City__c,
                                         temporary_state__c,
                                         TRHC__Temporary_Zip_Postal_Code__c
                                From Contact 
                                Where apiToken__c =: apiToken 
                                ];

        if(!userList.isempty()){
            userList[0].Permanant_Address__c = perAddress1;
            userList[0].TRHC__Permanent_Address_2__c = perAddress2;
            userList[0].TRHC__Permanent_City__c = perCity;
            userList[0].State_pic__c = perState;
            userList[0].TRHC__Permanent_Zip_Postal_Code__c = perZipcode;

            userList[0].Temporary_Address_1__c = tmpAddress1;
            userList[0].Temporary_Address_2__c = tmpAddress2;
            userList[0].TRHC__Temporary_City__c = tmpCity;
            userList[0].temporary_state__c = tmpState;
            userList[0].TRHC__Temporary_Zip_Postal_Code__c = tmpZipcode;

            update userList[0];
            perAddObj.address1 = perAddress1;
            perAddObj.address2 = perAddress2;
            perAddObj.city = perCity;
            perAddObj.state = perState;
            perAddObj.zipcode = perZipcode;

            tempAddObj.address1 = tmpAddress1;
            tempAddObj.address2 = tmpAddress2;
            tempAddObj.city = tmpCity;
            tempAddObj.state = tmpState;
            tempAddObj.zipcode = tmpZipcode;

            res.success = 1;
            res.message = new string[]{'Updated Succesfully.'};
            res.perAddress = perAddObj;
            res.tempAddress = tempAddObj;
            return res;
        }

        perAddObj.address1 = perAddress1;
        perAddObj.address2 = perAddress2;
        perAddObj.city = perCity;
        perAddObj.state = perState;
        perAddObj.zipcode = perZipcode;

        tempAddObj.address1 = tmpAddress1;
        tempAddObj.address2 = tmpAddress2;
        tempAddObj.city = tmpCity;
        tempAddObj.state = tmpState;
        tempAddObj.zipcode = tmpZipcode;

        res.success = 2;
        res.message = new string[]{'Unauthorized User.'};
        res.perAddress = perAddObj;
        res.tempAddress = tempAddObj;
        return res;
    }
    
    global class ResponseHandler {
        global integer success;
        global string[] message;
        global permenentAddress perAddress;
        global temporaryAddress tempAddress;
        global ResponseHandler () {

        }
    }
    
    global class permenentAddress {
        global String address1;
        global String address2;
        global String city;
        global String state;
        global String zipcode;
        global permenentAddress (){
            
        }
    }

    global class temporaryAddress {
        global String address1;
        global String address2;
        global String city;
        global String state;
        global String zipcode;
        global temporaryAddress (){
            
        }
    }
}