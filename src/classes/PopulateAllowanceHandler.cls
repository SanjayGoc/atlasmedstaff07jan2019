public class PopulateAllowanceHandler {
    public static boolean isafterInsert;
    public static void updateAllowance(List<TRHC__Placement__c> placementList) {
        Set<Id> scratchSheetIds = new Set<Id>();
        for(TRHC__Placement__c plm : placementList) {
            scratchSheetIds.add(plm.TRHC__Scratch_Sheet__c);
        }
        Map<String, TRHC__Expense__c> scratchExpenseMap = new Map<String, TRHC__Expense__c>();
        for(TRHC__Expense__c exp : [SELECT Id, TRHC__Scratch_Sheet__c, 
                                        TRHC__Expense_Name__c, TRHC__Weekly_Amount__c  
                                        FROM TRHC__Expense__c 
                                        WHERE TRHC__Scratch_Sheet__c IN : scratchSheetIds 
                                            AND TRHC__Expense_Name__c IN ('Per Diem','Housing Stipend')] ) {
            scratchExpenseMap.put(exp.TRHC__Expense_Name__c + exp.TRHC__Scratch_Sheet__c, exp);
        }
        for(TRHC__Placement__c placement : placementList) {
            if(scratchExpenseMap.containsKey('Per Diem' + placement.TRHC__Scratch_Sheet__c)) {
                placement.Per_Diem_Allowance_Weekly__c = scratchExpenseMap.get('Per Diem' + placement.TRHC__Scratch_Sheet__c).TRHC__Weekly_Amount__c;
            } 
            if(scratchExpenseMap.containsKey('Housing Stipend' + placement.TRHC__Scratch_Sheet__c)) {
                placement.Housing_Allowance_Weekly__c = scratchExpenseMap.get('Housing Stipend' + placement.TRHC__Scratch_Sheet__c).TRHC__Weekly_Amount__c;
            }
        }
    }
}