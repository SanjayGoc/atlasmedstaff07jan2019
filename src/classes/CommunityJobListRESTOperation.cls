@RestResource(urlMapping='/mobile/JobList/*')
global class CommunityJobListRESTOperation {
    @HttpGet
    global static ResponseHandler CommunityJobListRESTOperation(){
        ResponseHandler res = new ResponseHandler();
        String apiToken = RestContext.request.params.get('apiToken');
        String discipline = RestContext.request.params.get('discipline');
        String speciality = RestContext.request.params.get('speciality');
        String city = RestContext.request.params.get('city');
        String state = RestContext.request.params.get('state');
        String zipCode = RestContext.request.params.get('zipCode');
        String maxDistance = RestContext.request.params.get('maxDistance');
        String payScale = RestContext.request.params.get('payScale');
        String page = RestContext.request.params.get('page');
        String query;
        Integer pagenumber = ( ( Integer.valueOf(page) - 1) * 10 ) + 1;
        Integer count;
        List<string> statemulti = state.split(';');
        integer l = 10;
        integer o = 10;

        List<Contact> userList = new List<Contact>();
        if(apiToken == 'guest') {
            userList = [Select Id,
                             Name,
                             Temporary_Address_1__c,
                             firstName,
                             lastName,
                             apiToken__c
                        From Contact LIMIT 1];
        } else if (apiToken != '' && apiToken != null){
            userList = [Select Id,
                         Name,
                         Temporary_Address_1__c,
                         firstName,
                         lastName,
                         apiToken__c
                From Contact
                Where apiToken__c =: apiToken];
        }
        
        if(!userList.isEmpty()) {

            query = 'Select id,name,TRHC__Discipline__c,TRHC__Account_Lookup__r.BillingLatitude,TRHC__Account_Lookup__r.BillingLongitude,TRHC__Start_Date__c,CreatedDate,TRHC__Job_Title__c,Compact_License_States__c,';
            
            query += 'TRHC__Job_Description_Rich__c, Shift_Type2__c, Speciality1__c,TRHC__Job_City__c,State_pic__c,TRHC__Job_Postal_Code__c,PayScale__c From TRHC__Job__c where TRHC__Stage__c = \'Open\' and';  
            string statepick; 
            if(discipline != null && discipline != '')
                query += ' TRHC__Discipline__c = \''+discipline+'\' and '; 
            if(speciality != null  && speciality != '')
                query += 'Speciality1__c = \''+speciality+'\' and ';
            if(city != null  && city != '')
                query += 'TRHC__Job_City__c = \''+city+'\' and ';
            if(state != null  && state != ''){
                query += '(';
                if(statemulti.size() > 0){
                    for(string str : statemulti){
                        statepick += 'State_pic__c = \''+str+'\' or '; 
                    }
                }
                statepick = statepick.remove('null');
                statepick = statepick.removeEnd('or ');
                query += statepick + ' ) and ';
            }
            if(zipCode != null  && zipCode != '')
                query += 'TRHC__Job_Postal_Code__c = \''+zipCode+'\' and ';
            if(payScale != null && payScale != '')
                query += 'PayScale__c = '+payScale+ ' and ';
             
            query = query.removeEnd('and ');
            
            query = query.removeEnd('where ');

            query += 'ORDER BY CreatedDate DESC';
            
            List<TRHC__Job__c> jobListForDist = Database.query(query);
            
            List<TRHC__Job__c> jobListForDis = new List<TRHC__Job__c>();
            
            List<TRHC__Job__c> jobCountList = new List<TRHC__Job__c>();


            if(maxDistance != null && maxDistance != '') {
                
                Double contLattitude;
                Double contLongitude;

                if(userList[0].Temporary_Address_1__c != null) { 
                    GeoCodeApiDistanceForMobileApp__c server  =  GeoCodeApiDistanceForMobileApp__c.getValues('Default');
                    string key = server.Key__c;

                    //string key = 'AIzaSyAYp5E38JutdjkaTH8gSrVj1EA06lC53Ac';
                    String endpoint = 'https://maps.googleapis.com/maps/api/geocode/json?address=' ;
                    endpoint +=  EncodingUtil.urlEncode(userList[0].Temporary_Address_1__c, 'UTF-8')+ '&key='+key ;
                    
                    Http http = new Http();
                    HttpRequest req = new HttpRequest();
                    req.setEndpoint(endpoint);
                    req.setMethod('GET');
                    req.setTimeout(12000);
                    HttpResponse response ;
                    if(!Test.isRunningTest())
                        response = http.send(req);
                    else{
                        response = CommunityJobListRESTOperationCallOutMock.respond(req);
                    }

                    if(response.getStatusCode() == 200) {   
                        Map<String, Object> resultJSON = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
                
                        List<Object> objlist =  (List<Object>)resultJSON.get('results');
                        Map<String, Object> resultJSON1 = (Map<String, Object>)objlist[0];
                        Map<String, Object> objlist1 =  (Map<String, Object>)resultJSON1.get('geometry');
                        Map<String, Object> resultJSON3 = (Map<String, Object>)objlist1.get('location');

                        contLattitude = (Decimal)resultJSON3.get('lat');
                        contLongitude = (Decimal)resultJSON3.get('lng');
                    }
                }

                For(TRHC__Job__c jobObj : jobListForDist) {

                    //if(jobObj.TRHC__Account_Lookup__c != null){
                        
                        Double latHospital = jobObj.TRHC__Account_Lookup__r.BillingLatitude;
                        Double lnghospital = jobObj.TRHC__Account_Lookup__r.BillingLongitude;
                        Location locHospital = Location.newInstance(latHospital,lnghospital);
                        Location contLocat = Location.newInstance(contLattitude,contLongitude);
                
                        Double dist = Location.getDistance(locHospital, contLocat, 'mi');
                        
                        if(Double.valueOf(maxDistance) >= dist) {
                            jobListForDis.add(jobObj);
                        }
                    //}
                }

            } else {
                jobListForDis.addAll(jobListForDist);
            }
            
            Integer lowerlimit = (Integer.valueof(page) - 1) * 10;
            Integer highlimit = Integer.valueof(page) * 10 > jobListForDis.size() ? jobListForDis.size() : Integer.valueof(page) * 10;
            
            for(integer i = lowerlimit ;  i < highlimit  ; i++) {
                jobCountList.add(jobListForDis[i]);
            }

            count = jobListForDis.size()/10;

            integer Remainder = Math.Mod(jobListForDis.size(),10);
            if(Remainder >= 1 && Remainder <= 10){
                count += 1;
            }
            
            res.totalPage = count;
            res.totalListSize = jobListForDis.size();
            res.selectedPage = Integer.valueOf(page);
            res.success = 1;
            res.message = new string[]{'Success'};
            if(jobCountList != null){
                res.jobreslist = jobCountList;
            } else {
                res.jobreslist = new list<TRHC__Job__c>();
            }
            return res;
        }
        res.success = 2;
        res.message = new string[]{'Unauthorized User.'};
        return res;
    }

    global class ResponseHandler {
        global integer success;
        global string[] message;
        global List<TRHC__Job__c> jobreslist ;
        global Integer totalListSize;
        global Integer totalPage;
        global Integer selectedPage;
        
        global ResponseHandler () {

        }
    }
}