public with sharing class CandidateQuestionnaireExtn {

    public Contact con;
    public Contact con1 {get; set;}
    //public boolean show {get; set;}
    public boolean hide {get; set;}
    public boolean show1 {get; set;}   
    public CandidateQuestionnaireExtn(ApexPages.StandardController controller) {
        this.con = (Contact)controller.getRecord();
        con1 = new Contact();
        
        //con1.Id = this.con.Id;
        //hide = true;
        //show = false;
    }
    public pageReference save1(){
        con1.Id = con.Id;
        update con1;
        ApexPages.Message msg = new Apexpages.Message(ApexPages.Severity.CONFIRM,System.Label.Form_Saved_Msg);
        ApexPages.addmessage(msg);
        show1 = true;
        //hide = false;
        return null;
    }
    public pageReference submit(){
        con1.Id = con.Id;
        update con1;
        ApexPages.Message msg = new Apexpages.Message(ApexPages.Severity.CONFIRM,System.Label.Form_Submitted_Msg);
        ApexPages.addmessage(msg);
        //show = true;
        show1 = true;
        hide = false;
        return null;
    }
    
    
}