global class CreateScratchSheetBatch implements Database.Batchable<sObject>,database.stateful {
    public String query = '';
    global set<id> strParameter;
    global Set<id> scrid = new set<id>();
    public CreateScratchSheetBatch(set<id> strParam){
        strParameter = strParam;
    }


    global List<TRHC__Job__c> start(Database.BatchableContext BC) {
        query = 'SELECT id,Name,TRHC__Start_Date__c,TRHC__Expected_End_Date__c,State_pic__c,GSA_Lodging__c,TRHC__County__c,GSA_M_I_Rate__c,Tax_Rate__c FROM TRHC__Job__c WHERE id in: strParameter and TRHC__Start_Date__c !=  null and TRHC__Expected_End_Date__c != null' ;        
        return DataBase.Query(query);
    }
    
    global void execute(Database.BatchableContext BC, List <TRHC__Job__c> jobList) {
        List<TRHC__Job__c> jobListForScratch = new List<TRHC__Job__c>();
        TRHC__Scratch_Sheet__c newScratchSheet;                
        
        try {
            //String gsaURL = 'http://gsa.targetrecruit.com/Geocode/getGsaDetails_new.php';   //(String)cs.TRHC__GSA_Service_PHP_URL__c;
            TRHC__Healthcare_Setting__c customSetting = TRHC__Healthcare_Setting__c.getAll().get('Default');
            string jobFieldList = ObjectFieldsHelper.getAllFields('TRHC__Job__c').get('TRHC__Job__c');
            string jobQuery = 'Select ' + jobFieldList + ' from TRHC__Job__c where ID IN : jobList';
            jobListForScratch = Database.query(jobQuery);

            Boolean isError = false;
            Boolean checkGSAService = (Boolean)customSetting.get('TRHC__Enable_GSA_Service_to_Update_M_I_Rate__c');
            
            //getting scrtach sheet recordtype
            string scrtchSheetRecordTypeId = [SELECT
                                       Id
                                       FROM
                                       RecordType
                                       WHERE
                                       SobjectType = 'TRHC__Scratch_Sheet__c' AND Name = 'ScratchSheet'].Id;
            string defaultScratchTemplate = (string)customSetting.get('TRHC__Default_Scratch_sheet_Template_Name__c');
            defaultScratchTemplate = '\'' + defaultScratchTemplate + '\'';
            
            //get All fields of an sobject 
            Map<string, string> scratchSheethelper = ObjectFieldsHelper.getAllFields('TRHC__Scratch_Sheet__c');
            string allfields = scratchSheethelper.get('TRHC__Scratch_Sheet__c');
            string queryString = 'Select ' + allfields + ' from TRHC__Scratch_Sheet__c ' + ' where name = ' + defaultScratchTemplate;
            if(defaultScratchTemplate == null || defaultScratchTemplate == '') {
                ApexPages.addMessage(new  ApexPages.message(ApexPages.Severity.ERROR, '<span style="font-family: Tahoma;">' + 'Scrtch sheet is not available' + '.</span>'));
                isError = true;
            }

            if(defaultScratchTemplate != null) {
                TRHC__Scratch_Sheet__c scratchSheet = Database.query(queryString);
                string expnaceFields = ObjectFieldsHelper.getAllFields('TRHC__Expense__c').get('TRHC__Expense__c');
                
                Id cid = string.valueOf(scratchSheet.id).subString(0, 15);
                string scrname = string.valueOf(scratchSheet.Name);
                string scrtName = '\'' + scrname + '\'';
                string sid = '\'' + cid + '\'';
                string expancesQueryString = 'Select ' + expnaceFields + ' from TRHC__Expense__c ' + ' where TRHC__Scratch_Sheet__c =' + sid;
                List<TRHC__Expense__c> templateExpnaseList = Database.query(expancesQueryString);

                For(TRHC__Job__c jobObj : jobListForScratch){
                    newScratchSheet = scratchSheet.clone(true, false);
                    newScratchSheet.id = null;
                    
                    List<Job_to_Scratch_Sheet__c> jobToScratchSheet = [SELECT
                                                                   Job_Field_API_Name_Custom__c, 
                                                                   Scratch_Sheet_Field_API_Name__c
                                                                   FROM
                                                                   Job_to_Scratch_Sheet__c];

                    for(Job_to_Scratch_Sheet__c jobToScratch: jobToScratchSheet) {
                        string jobField = jobToScratch.Job_Field_API_Name_Custom__c;
                        string scratchSheetField = jobToScratch.Scratch_Sheet_Field_API_Name__c;
                        if(jobFieldList.contains(jobField) && allfields.contains(scratchSheetField)) {
                            newScratchSheet.put(scratchSheetField, jobObj.get(jobField));
                        }
                    }

                    newScratchSheet.TRHC__County__c = jobObj.TRHC__County__c;
                    newScratchSheet.Name = jobObj.TRHC__Job_Title__c + ' ScratchSheet';
                    newScratchSheet.RecordTypeId = scrtchSheetRecordTypeId;
                    newScratchSheet.TRHC__Pay_Rate__c = jobObj.TRHC__Pay_Rate__c;
                                           newScratchSheet.TRHC__Total_Regular_Hours__c = jobObj.TRHC__Hours_Per_Week__c;
                    newScratchSheet.TRHC__Total_Pay_Regular_Hours__c  = jobObj.TRHC__Hours_Per_Week__c;
                    newScratchSheet.TRHC__GSA_M_I_Rate__c = jobObj.GSA_M_I_Rate__c;
                    newScratchSheet.TRHC__GSA_Lodging__c = jobObj.GSA_Lodging__c;
                    newScratchSheet.TRHC__Tax_Rate__c = jobObj.Tax_Rate__c;
                    newScratchSheet.OT_Pay_rate_Override__c = true;
                
                    Insert newScratchSheet;

                    List<TRHC__Expense__c> expancesList = new  List<TRHC__Expense__c>();
                    for(TRHC__Expense__c expanse : templateExpnaseList) {
                        expanse.id = null;
                        expanse.TRHC__Scratch_Sheet__c = newScratchSheet.id;
                        
                        /*if(expanse.TRHC__Expense_Name__c == 'VMS Fees'){
                            system.debug('=====value====>'+value);
                            if(newScratchSheet.TRHC__Client_Fee__c != null)  {
                                Decimal value =  (newScratchSheet.TRHC__Client_Fee__c * newScratchSheet.Over_Time_Include_Bill__c)/100;  
                                expanse.TRHC__Value__c = value;
                                system.debug('=====value====>'+value);
                                
                            }
                            
                            system.debug('===VMS Fees==>');
                        }*/

                        if(expanse.TRHC__Expense_Name__c == 'Orientation Hours'){
                            expanse.TRHC__Grouping_Total__c = 'Other Expense';
                        }
                        if(expanse.TRHC__Expense_Name__c == 'VMS Fees'){
                            expanse.TRHC__Grouping_Total__c = 'Other Expense';
                        }

                            /*if(expanse.TRHC__Expense_Name__c == 'Orientation Hours'){
                                if(newScratchSheet.TRHC__No_Bill_Orientation_Hours__c != null && newScratchSheet.TRHC__Pay_Rate__c != null){
                                    system.debug('newScratchSheet.TRHC__No_Bill_Orientation_Hours__c'+ newScratchSheet.TRHC__No_Bill_Orientation_Hours__c);
                                    if(newScratchSheet.TRHC__No_Bill_Orientation_Hours__c == 0.00){
                                        Decimal value = 1.00 * newScratchSheet.TRHC__Pay_Rate__c;
                                        expanse.TRHC__Value__c = value;
                                    } else {
                                        //system.debug('====== expanse.TRHC__Value__c before calculations ======='+expanse.TRHC__Value__c);
                                        Decimal value = newScratchSheet.TRHC__No_Bill_Orientation_Hours__c * newScratchSheet.TRHC__Pay_Rate__c;
                                        //expanse.TRHC__Value__c = value;
                                        system.debug('====== expanse.TRHC__Value__c after calculations ======='+expanse.TRHC__Value__c);
                                    
                                }

                            }
*/
                        expancesList.add(expanse);

                    }
                    Insert expancesList;
                    
                    jobObj.TRHC__Scratch_Sheet__c = newScratchSheet.id;
                    system.debug('1111111111'+ newScratchSheet.id);
                    Update jobObj;
                    
                    if(jobObj.TRHC__Scratch_Sheet__c != null) {
                        scrid.add(jobObj.TRHC__Scratch_Sheet__c);
                    
                    }
                }  
            }

            TRHC__Config_Settings__c configSettings = TRHC__Config_Settings__c.getValues('Default');
            Boolean isEnabled = configSettings == null ? True : (Boolean) configSettings.Trigger_Enable_ScratchSheetAutoAddExp__c;
            List<String> exptIdsList = configSettings == null ? new List<Id>() :configSettings.ExceptConfigIds__c.split(';');
            List<String> exptAccountList = configSettings == null ? new List<String>() :configSettings.Except_Hospitals__c.split(';');
            String configFields = ObjectFieldsHelper.getAllFields('TRHC__Config__c').get('TRHC__Config__c');
            
            String expanseFields = ObjectFieldsHelper.getAllFields('TRHC__Expense__c').get('TRHC__Expense__c');
            
            Set<String> states = new Set<String>();
            
            Set<String> scratchSheetIds = new Set<String>();
            List<TRHC__Scratch_Sheet__c> scrList = [select id,name,Account_Name__c,State_pic__c,City__c From TRHC__Scratch_Sheet__c where id =: newScratchSheet.id ];
            for(TRHC__Scratch_Sheet__c scratchSheet : scrList ) {
                if(scratchSheet.State_pic__c!= null) {
                    states.add('\''+scratchSheet.State_pic__c+'\'');
                }
                scratchSheetIds.add(scratchSheet.Id);
            }
            
            if(states.size() == 0){
                return;
            }

            Map<String, Set<TRHC__Config__c>> configMap = new Map<String, Set<TRHC__Config__c>>();
            
            List<String> statesList = new List<String> (states);
            String query = 'SELECT ' + configFields+ ' FROM TRHC__Config__c WHERE TRHC__State__c IN ' + statesList ;
            
            String configQuery = 'SELECT ID, TRHC__Scratch_Sheet__c,TRHC__Active__c, TRHC__Expense_Name__c, TRHC__Config__c FROM TRHC__Expense__c WHERE TRHC__Scratch_Sheet__c = \''+ newScratchSheet.id +'\'';
            
            if(configSettings != null) {
                String criteria = configSettings.AutoAddExpenseCriteria__c;
                if(criteria != null) {
                    List<String> criterias = criteria.split(',');
                    for(String rule : criterias){
                        query += ' AND ' + rule +' ';
                    }
                }
            }
            
            List<TRHC__Config__c> configs = Database.query(query);
            
            List<TRHC__Expense__c> availableExpenses = Database.query(configQuery);
            if(configs != null && configs.size() > 0) {
                List<Config_To_Expense__c> configToExpense = [Select Config_Field_API_Name_Custom__c, Expanse_Field_API_Name__c From Config_To_Expense__c]; 
                for(TRHC__Config__c config : configs) {
                    Set<TRHC__Config__c> configObjs = configMap.get(config.TRHC__State__c);
                    if(configObjs == null) {
                        configObjs = new Set<TRHC__Config__c>();
                    }
                    configObjs.add(config);
                    configMap.put(config.TRHC__State__c, configObjs);
                }
            
                Set<TRHC__Expense__c> expenses = new Set<TRHC__Expense__c>();
                for(TRHC__Scratch_Sheet__c scratchSheet : scrList) {
                    if(scratchSheet.State_pic__c== null) { 
                        continue;
                    }
                    
                    Set<TRHC__Config__c> configIds = configMap.get(scratchSheet.State_pic__c);
                    List<String> nameLst = new List<String> ();
                    List<String> scrExpList = new List<String>();
                    for(TRHC__Config__c configId : configIds) {
                        boolean isExist = false;
                        boolean addCityExpense = false;
                        
                        if(exptIdsList.contains(configId.id) && scratchSheet.City__c != configId.TRHC__City__c){
                            isExist = true;
                        }
                        else if(exptIdsList.contains(configId.id) ){
                            nameLst.add(configId.TRHC__Name__c);
                            addCityExpense = true;
                        }
                        
                        for(TRHC__Expense__c avaExp : availableExpenses) {
                            if(avaExp.TRHC__Scratch_Sheet__c == scratchSheet.Id && avaExp.TRHC__Config__c == configId.id) {
                                isExist = true;
                            }
                        }
                        if(isExist) {
                            continue;
                        }

                        TRHC__Expense__c expense = new TRHC__Expense__c();

                        for(Config_To_Expense__c configToExp : configToExpense){
                            String expanseField = configToExp.Expanse_Field_API_Name__c;
                            String configField = configToExp.Config_Field_API_Name_Custom__c;
                            if(configFields.contains(configField)&& expanseFields.contains(expanseField)){
                                if(configField == 'TRHC__Value__c'){
                                    expense.TRHC__Value__c = Decimal.valueOf(configId.TRHC__Value__c);
                                }else{
                                    expense.put(expanseField,configId.get(configField));
                                }
                            }
                        }
                        expense.TRHC__Config__c = configId.id;
                        expense.TRHC__Scratch_Sheet__c  = scratchSheet.Id;
                        expense.TRHC__Grouping_Total__c = 'Other Expense';
                        expenses.add(expense);
                        scrExpList.add(expense.TRHC__Expense_Name__c);
                    }
                    
                    set<String> setTempString = new set<String>();
                    set<String> duplicateTempString = new set<String>();
                    for(String s:scrExpList){
                        if(!setTempString.add(s)){
                            duplicateTempString .add(s);
                        }
                    }

                    for(TRHC__Expense__c expence:expenses){
                        for(String str:duplicateTempString){
                            if(str == expence.TRHC__Expense_Name__c){
                                if(exptAccountList.contains(scratchSheet.Account_Name__c)){
                                    if(exptIdsList.contains(expence.TRHC__Config__c)){
                                        expenses.remove(expence);  
                                    }
                                    else{
                                        expence.TRHC__Value__c = 0.0;
                                    }
                                }
                                else if(!exptIdsList.contains(expence.TRHC__Config__c)){
                                    expenses.remove(expence); 
                                }
                            }
                        }
                    }
                    
                    for(TRHC__Expense__c exp :expenses){
                        if(exp.TRHC__Expense_Name__c != null && exp.TRHC__Expense_Name__c.contains('Sales Tax') && exptAccountList.contains(scratchSheet.Account_Name__c)){
                            exp.TRHC__Value__c = 0.0;
                        }
                    }
                    
                    If(expenses.size()>0) {
                        insert new List<TRHC__Expense__c>(expenses);
                    }
                }
            }
        }
        catch(System.CalloutException e) {
            System.debug(LoggingLevel.ERROR, ':::::::::CalloutException  = ' + e);
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        
        List<TRHC__Expense__c> expense = [select TRHC__Expense_Name__c,TRHC__Amount__c,TRHC__Scratch_Sheet__r.OverTIme_GPH__c,TRHC__Scratch_Sheet__r.Over_Time_Include_Bill__c,TRHC__Scratch_Sheet__r.TRHC__Client_Fee__c,TRHC__Occurence__c, TRHC__Value__c,TRHC__ProRated_Hours__c from TRHC__Expense__c where TRHC__Scratch_Sheet__c =: scrid and TRHC__Expense_Name__c = 'VMS Fees'];
        
        if(expense.size() > 0){
            if(expense[0].TRHC__Scratch_Sheet__r.TRHC__Client_Fee__c != null){
                //expense[0].TRHC__Value__c = expense[0].TRHC__Scratch_Sheet__r.TRHC__Client_Fee__c;
                expense[0].TRHC__Grouping_Total__c = 'Other Expense';
                Update expense; 
                
            }
        }

        List<TRHC__Expense__c> expenseLst = [select TRHC__Expense_Name__c,TRHC__Amount__c,TRHC__Scratch_Sheet__r.OverTIme_GPH__c,TRHC__Scratch_Sheet__r.Over_Time_Include_Bill__c,TRHC__Scratch_Sheet__r.TRHC__Client_Fee__c,TRHC__Occurence__c, TRHC__Value__c,TRHC__ProRated_Hours__c from TRHC__Expense__c where TRHC__Scratch_Sheet__c =: scrid and TRHC__Expense_Name__c = 'Orientation Hours'];
        
        if(expenseLst.size() > 0){
            if(expenseLst[0].TRHC__Scratch_Sheet__r.TRHC__Client_Fee__c != null){
                //expense[0].TRHC__Value__c = expense[0].TRHC__Scratch_Sheet__r.TRHC__Client_Fee__c;
                expenseLst[0].TRHC__Grouping_Total__c = 'Other Expense';
                Update expenseLst; 
                
            }
        }


        /*List<TRHC__Expense__c> expList = new List<TRHC__Expense__c>();
        for(TRHC__Expense__c objexp : expense){
            if(objexp.TRHC__Scratch_Sheet__r.TRHC__Client_Fee__c != null){
                objexp.TRHC__Value__c = objexp.TRHC__Scratch_Sheet__r.TRHC__Client_Fee__c;
                objexp.TRHC__Amount__c = (objexp.TRHC__Scratch_Sheet__r.Over_Time_Include_Bill__c * objexp.TRHC__Scratch_Sheet__r.TRHC__Client_Fee__c)/100 ;
                 
                //system.debug('======= TRHC__Value__c ========'+expense[0].TRHC__Value__c);
                //system.debug('======= TRHC__Amount__c ========'+expense[0].TRHC__Amount__c);
                
                //Update expense;
                
            }
            expList.add(objexp);
        }

        if(!expList.isEmpty()){
            Update expList;
        }*/


        CalculateGPHBatch gph = new CalculateGPHBatch(scrid);
        database.executebatch(gph,1);
        system.debug('========== scrid =========='+scrid);
    }
}