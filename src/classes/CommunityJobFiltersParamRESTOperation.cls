@RestResource(urlMapping='/mobile/JobFiltersParameters/*')
global class CommunityJobFiltersParamRESTOperation {

    @HttpGet
    global static ResponseHandler CommunityJobFiltersParamRESTOperation (){
        String apiToken = RestContext.request.params.get('apiToken');
        Map<string, List<string>> getValues = new Map<string, List<string>>();
        
        List<String> listOfStates = new List<String>();
        
        ResponseHandler res = new ResponseHandler();
        List<Contact> userList = new List<Contact>();
        if(apiToken  == 'GUEST'){
             userList = [Select Id,
                                Name,
                                firstName,
                                lastName,
                                apiToken__c
                                From Contact
                        ];

        }
        else if(apiToken != '' && apiToken != 'None'){
                 userList = [Select Id,
                                    Name,
                                    firstName,
                                    lastName,
                                    apiToken__c
                                    From Contact
                                    Where apiToken__c =: apiToken
                                    ];
        }
                                    
        getValues = DataUtils.getFieldDependencies('TRHC__Job__c', 'TRHC__Discipline__c' ,'Speciality1__c');
        listOfStates = DataUtils.getPickListValues('contact', 'State_pic__c');
        
        if(!userList.isEmpty()){
            res.success = 1;
            res.message = new string[]{'Success.'};
            if(getValues != null){
                res.DisiplineWithSpeciality = getValues;
            } else  {
                res.DisiplineWithSpeciality = new Map<string, List<string>>();
            }
            if(listOfStates != null){
                res.stateList = listOfStates;
            } else {
                res.stateList = new List<string>();
            }
            return res;
        }
        res.success = 2;
        res.message = new string[]{'Unauthorized User.'};
        return res;
    }
    
    global class ResponseHandler {
        global integer success;
        global string[] message;
        global map<string,List<String>> DisiplineWithSpeciality;
        global List<string> stateList;  
        global ResponseHandler () {

        }
    }
}