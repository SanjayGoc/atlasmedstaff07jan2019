/*
===========================================================
Description: Unit class for SendExpireNotificationBatchTest
===========================================================
*/

@istest(seeAlldata=false)
public class SendExpireNotificationBatchTest {
    static testmethod void SendExpNotifyBtcTest() {
    
        User singleUser = new User(
                         ProfileId = [SELECT Id FROM Profile WHERE Name = 'Sales'].Id,
                         LastName = 'last',
                         Email = 'test@email.com',
                         Username = 'test@email.com' + System.currentTimeMillis(),
                         CompanyName = 'TEST',
                         Title = 'Client Manager',
                         Alias = 'alias',
                         TimeZoneSidKey = 'America/Los_Angeles',
                         EmailEncodingKey = 'UTF-8',
                         LanguageLocaleKey = 'en_US',
                         LocaleSidKey = 'en_US');
        insert singleUser;
    
        TRHC__Config_Settings__c config = new TRHC__Config_Settings__c(Name = 'Default');
        config.Enable_RefferalTrigger__c = false;
        insert config;
        
        TRHC__CredentialsCustomSettings__c config2 = new TRHC__CredentialsCustomSettings__c (Name = 'Default');
        config2.TRHC__Disable_CredentialsAnswerTrigger__c = false;
        insert config2;
        
        //Get User record        
        //List<User> usr = [SELECT Id, Email FROM User WHERE IsActive = true LIMIT 2];
        //Create Candidate/Nurse record
        Contact con = new Contact();
        con.FirstName = 'mike'; 
        con.LastName = 'Shah';
        con.Email = 'test@test1.gmail.com';
        con.Compliance_Coordinater__c = singleUser.id;
        con.TRHC__Candidate_Status__c = 'Active';
        con.TRHC__Recruiter__c = singleUser.id;
        insert con;
        //Create Placement record
        TRHC__Placement__c placement = new TRHC__Placement__c();
        placement.TRHC__Contact_Candidate__c = con.Id;
        placement.TRHC__Status__c = 'Active';
        insert placement;
        //Create Credential record
        TRHC__Credentials__c credential = new TRHC__Credentials__c();
        credential.TRHC__Contact__c = con.id;
        credential.TRHC__Expiration_Date__c = system.today() + 1;
        insert credential;
        
        Test.startTest();
        SendExpireNotificationBatch btc = new SendExpireNotificationBatch();
        DataBase.executeBatch(btc, 1);
        Test.stopTest();
    }
}