@RestResource(urlMapping='/mobile/forgotPassword/*')
global class CommunityForgotPasswordRESTOperation {
    @HttpGet
    global static ResponseHandler CommunityForgotPasswordRESTOperation(){
    	String username = RestContext.request.params.get('username');
    	ResponseHandler res = new ResponseHandler();

    	List<Contact> conList = [select id,name,TRHC__UserName__c,TRHC__Password__c From Contact where TRHC__UserName__c =: username];
    	List<EmailTemplate> emailTempList = [SELECT Id,Body FROM EmailTemplate where name = 'CommunityForgotPasswordTemplate' limit 1];

    	List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
    	List<String> toAddresses = new List<String>();

    	if(!conList.isEmpty()){
    		toAddresses.add(conList[0].TRHC__UserName__c);
    		res.username = username;
    		res.success = 1;
    		
    		if(conList[0].TRHC__UserName__c != null && conList[0].TRHC__Password__c != null){
    			//initiallize messaging method
                Messaging.SingleEmailMessage singleMail = new Messaging.SingleEmailMessage();
                
                // To address
                singleMail.setToAddresses(toAddresses);
                //singleMail.setToAddresses(new string[]{'hiren.goc@gmail.com','harshad.mobio@gmail.com','vicky.mobiosolutions@gmail.com'});

                //set object Id
                singleMail.setTargetObjectId(conList[0].id);

                //set template Id
                singleMail.setTemplateId(emailTempList[0].id);

                //flag to false to stop inserting activity history
                singleMail.setSaveAsActivity(false);

                //add mail
                emails.add(singleMail);

               //This will not send email to contact  
            }

    		//send mail
    		Messaging.sendEmail(emails);
            res.message = new string[]{'Password reset instructions have been successfully sent.'};
            return res;
    	}

    	res.username = username;
    	res.success = 0;
    	res.message = new string[]{'Email id is not registered.'};

    	return res;
    }

    global class ResponseHandler {
    	global String username;
    	global integer success;
        global string[] message;
    	global ResponseHandler (){
    	}
    }
}