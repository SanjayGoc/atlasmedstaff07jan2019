/*
============================================================================================
Description:   Batch claas for Send Email to Candidate, Recruiter and Complaince coordinator      
Functionality: Notify When Expire 90/60/30/15/7 0r expire day
============================================================================================
*/
global class SendExpireNotificationBatch implements Database.batchable<sObject>, Database.Stateful {
    String query;
    global List<String> emails;
    global List<EmailTemplate> emailTemplate;
    global Set<Id> candidateId;
    global OrgWideEmailAddress[] owea;
    global Map<String,User> recruiterUsers;
    global set<Id> placementCandidateId;

    global SendExpireNotificationBatch() {
        placementCandidateId = new set<id>();
        emails = new List<String>();
        candidateId = new Set<Id>();
        recruiterUsers = new Map<String,User>();
        emailTemplate = new List<EmailTemplate>([SELECT Id, Subject, body, HTMLValue FROM EmailTemplate 
                                                    WHERE developerName = 'Candidate_Nurse_Expire_Notification_Template']);
        owea = [select Id from OrgWideEmailAddress where Address = 'info@atlasmedstaff.com'];
        for(User u : [SELECT FirstName, LastName, Email FROM User WHERE isActive = true]) {
            recruiterUsers.put(u.firstname+' '+u.lastName,u);
        }

        for(TRHC__Placement__c place : [select id,name,TRHC__Contact_Candidate__c from TRHC__Placement__c]){
            placementCandidateId.add(place.TRHC__Contact_Candidate__c);
        }

        if(Test.isRunningTest()) {
            query = 'SELECT Id, Name, TRHC__Credential_Master_Name__c, Candidate_Name__c,TRHC__Contact__r.Recruiters__c,TRHC__Contact__r.Email,TRHC__Contact__r.TRHC__Candidate_Status__c, ' + 
                        'TRHC__Contact__c, TRHC__Contact__r.Compliance_Coordinater__r.Email, ' +
                        'TRHC__Contact__r.TRHC__Recruiter__c, TRHC__Contact__r.TRHC__Recruiter__r.Email, ' +
                        'No_Of_Days_Till_Expiration__c, TRHC__Expiration_Date__c FROM TRHC__Credentials__c WHERE '+
                        '( '+
                        'TRHC__Credential_Master_Name__c != \'5-Annual Performance Eval\' AND TRHC__Credential_Master_Name__c != \'5-Performance Evaluation\' AND  TRHC__Credential_Master_Name__c != \'2-Criminal Background Check\' AND TRHC__Credential_Master_Name__c != \'2-SAM\' AND TRHC__Credential_Master_Name__c != \'2-OIG\' AND '+
                        'TRHC__Credential_Master_Name__c != \'5-Annual Performance Evaluation\' AND TRHC__Credential_Master_Name__c != \'5-Reference 2\'  AND ' + 
                        'TRHC__Credential_Master_Name__c != \'5-Annual Evaluation-Recruiter\' ) AND TRHC__Contact__r.TRHC__Candidate_Status__c = \'Active\' AND ' +
                        '(No_Of_Days_Till_Expiration__c = \'90\' OR No_Of_Days_Till_Expiration__c = \'60\' OR ' +
                        'No_Of_Days_Till_Expiration__c = \'30\' OR No_Of_Days_Till_Expiration__c = \'15\' OR ' +
                        'No_Of_Days_Till_Expiration__c = \'7\' OR No_Of_Days_Till_Expiration__c = \'1\') LIMIT 1';
        } else {
            query = 'SELECT Id, Name, TRHC__Credential_Master_Name__c,Candidate_Name__c, TRHC__Contact__r.Recruiters__c,TRHC__Contact__r.Email,TRHC__Contact__r.TRHC__Candidate_Status__c, ' +
                        'TRHC__Contact__c, TRHC__Contact__r.Compliance_Coordinater__r.Email, ' +
                        'TRHC__Contact__r.TRHC__Recruiter__c, TRHC__Contact__r.TRHC__Recruiter__r.Email, ' +
                        'No_Of_Days_Till_Expiration__c, TRHC__Expiration_Date__c FROM TRHC__Credentials__c WHERE '+
                        '(' + 
                        'TRHC__Credential_Master_Name__c != \'5-Annual Performance Eval\' AND  TRHC__Credential_Master_Name__c != \'5-Performance Evaluation\' AND  TRHC__Credential_Master_Name__c != \'2-Criminal Background Check\' AND TRHC__Credential_Master_Name__c != \'2-SAM\' AND TRHC__Credential_Master_Name__c != \'2-OIG\' AND '+
                        'TRHC__Credential_Master_Name__c != \'5-Annual Performance Evaluation\' AND TRHC__Credential_Master_Name__c != \'5-Reference 2\' AND ' +
                        'TRHC__Credential_Master_Name__c != \'5-Annual Evaluation-Recruiter\' ) AND ' +
                        'TRHC__Contact__r.TRHC__Candidate_Status__c = \'Active\' AND ' +
                        '(No_Of_Days_Till_Expiration__c = \'90\' OR No_Of_Days_Till_Expiration__c = \'60\' OR ' +
                        'No_Of_Days_Till_Expiration__c = \'30\' OR No_Of_Days_Till_Expiration__c = \'15\' OR ' +
                        'No_Of_Days_Till_Expiration__c = \'7\' OR No_Of_Days_Till_Expiration__c = \'1\') ' + 
                        'ORDER BY TRHC__Contact__c';
        }
    }
    // Start Method
    global Database.QueryLocator start(Database.BatchableContext BC) {         
        return Database.getQueryLocator(query);
    }
    // Execute Method
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        Messaging.SingleEmailMessage mail;
        String htmlBody, expDay, expMonth;
        Map<String,List<TRHC__Credentials__c>> credentialsMap = new Map<String,List<TRHC__Credentials__c>>();
        for(TRHC__Credentials__c credentials : (List<TRHC__Credentials__c>)scope) 
        {
            if(!String.isBlank(credentials.TRHC__Contact__c ) && placementCandidateId.contains(credentials.TRHC__Contact__c) 
                && credentials.TRHC__Contact__r.Email != null && credentials.TRHC__Contact__r.Email != '')
            {
                if(!credentialsMap.containsKey(credentials.TRHC__Contact__c  + '~~' + credentials.No_Of_Days_Till_Expiration__c))
                    credentialsMap.put(credentials.TRHC__Contact__c + '~~' + credentials.No_Of_Days_Till_Expiration__c,new List<TRHC__Credentials__c>{credentials});
                else
                    credentialsMap.get(credentials.TRHC__Contact__c + '~~' + credentials.No_Of_Days_Till_Expiration__c).add(credentials);

            }
        }

        Integer index = 0;
        List<Messaging.singleEmailmessage> mailList = new List<Messaging.singleEmailmessage>();

        for(String key : credentialsMap.keySet())
        {
            mail = new Messaging.SingleEmailMessage();
            String[] ccAddresses = new String[]{};
            String[] nurseEmail = new String[]{};
            index = 1;
            String credentialsLstStr = '';

            if(emailTemplate.size() > 0 ) {
                mail.setSubject(emailTemplate[0].Subject.replace('{!TRHC__Credentials__c.No_Of_Days_Till_Expiration__c}',key.split('~~')[1]));
                //Body of email
                if(emailTemplate[0].HTMLValue != null && emailTemplate[0].HTMLValue != '') {
            
                    htmlBody = emailTemplate[0].HTMLValue;   
                    for(TRHC__Credentials__c credentials : credentialsMap.get(key))
                    {   
                        if(credentials.TRHC__Contact__r.Email != null && credentials.TRHC__Contact__r.Email != '')
                        {
                            nurseEmail.add(credentials.TRHC__Contact__r.Email);
                            //nurseEmail.add('minkesh.patel@getoncrm.com');
                        }

                        if(credentials.TRHC__Contact__r.Compliance_Coordinater__c != null) 
                        {
                            ccAddresses.add(credentials.TRHC__Contact__r.Compliance_Coordinater__r.Email);
                            //ccAddresses.add('minkesh422000@gmail.com');
                        }

                        if(credentials.TRHC__Contact__r.Recruiters__c != null && recruiterUsers.get(credentials.TRHC__Contact__r.Recruiters__c) != null) 
                        {
                            ccAddresses.add(recruiterUsers.get(credentials.TRHC__Contact__r.Recruiters__c).Email);
                            //ccAddresses.add('minkesh_patel@yahoo.com');
                        }

                        //Set Date Formate mm/dd/yyyy
                        expDay = String.valueOf(credentials.TRHC__Expiration_Date__c.Day());
                        expMonth = String.valueOf(credentials.TRHC__Expiration_Date__c.month());
                        if(expMonth.length() == 1) {
                            expMonth = '0' + expMonth;
                        } 
                        if(expDay.length() == 1) {
                            expDay = '0' + expDay;
                        }
                        String credentialExpDate = expMonth + '/' + expDay + '/' + credentials.TRHC__Expiration_Date__c.year();
                        String credMaster = credentials.TRHC__Credential_Master_Name__c != null ?  credentials.TRHC__Credential_Master_Name__c : '';

                        if(index > 1){
                           credentialsLstStr += '<br/>';
                        }

                        credentialsLstStr += index + ') The <b>' + credMaster + '</b> we have on file for you expires <b>' + credentialExpDate + '</b>.';
                        index ++;
                    }

                    htmlBody = htmlBody.replace('{!Body}',credentialsLstStr);

                    if(nurseEmail.size() > 0)
                        mail.setToAddresses(nurseEmail);
                    if(ccAddresses.size() > 0)
                        mail.setCcAddresses(ccAddresses);
                        mail.setBCcAddresses(new String[] { 'info@atlasmedstaff.com ' });
                        //mail.setReplyTo('info@atlasmedstaff.com');
                    if(owea.size() > 0) {
                        mail.setOrgWideEmailAddressId(owea.get(0).Id);
                    }

                    if(!String.isBlank(credentialsMap.get(key)[0].Candidate_Name__c)){
                        htmlBody = htmlBody.replace('{!TRHC__Credentials__c.Candidate_Name__c}',credentialsMap.get(key)[0].Candidate_Name__c);
                    }

                    mail.setHtmlBody(htmlBody);
                    mailList.add(mail);
                }

            }

        } 

        if(mailList != null && !mailList.isEmpty()){
            Messaging.sendemailResult[] r = Messaging.sendemail(mailList);
            if (r[0].success) {
                System.debug('The email was sent successfully.');
            } else{
                System.debug('The email failed to send: ' + r[0].errors[0].message);
            }
        }

    }
    // Finish Method
    global void finish(Database.BatchableContext BC) {
    }
   
}