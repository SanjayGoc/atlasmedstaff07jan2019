public with sharing class OldJobsCloserBatch implements Database.Batchable<sObject>{
    
    Public String jobQuery;
    Public String stage;
    public Database.QueryLocator start(Database.BatchableContext batch){
        Date expiryDate = Date.today()-15;
        stage = 'Closed';
        Id jobRecId = Schema.SObjectType.TRHC__Job__c.getRecordTypeInfosByName().get('Job').getRecordTypeId();
        jobQuery =
            ' SELECT '+
            ' TRHC__Closed__c, TRHC__Stage__c, CreatedDate, TRHC__Publish__c, lastmodifieddate, TRHC__Start_Date__c '+
            ' FROM TRHC__Job__c '+
            ' WHERE  TRHC__Start_Date__c <= :expiryDate '+
            ' and RecordtypeId = :jobRecId '+
            ' and (TRHC__Closed__c = False or TRHC__Publish__c = True or TRHC__Stage__c !=: stage ) '+
            ' order by CreatedDate Asc';        
        return Database.getQueryLocator(jobQuery);
    }
    
    public void execute(Database.BatchableContext bc, List<TRHC__Job__c> records){
        if(records.size()>0){
            for(TRHC__Job__c job : records){
                job.TRHC__Stage__c = 'closed';
                job.TRHC__Closed__c = True;
                job.TRHC__Publish__c = False;
            }
        }
        update records;
        
    }
    
    public void finish(Database.BatchableContext bc) {
        
    }
}