public with sharing class JobSearchHelper {
    /*
     * @Modified by :chaitanya.b@targetrecruit.net 
     * @Date 		:31/07/2018
     * @description	: added new where condition(trhc__stage__c != 'Closed') for job query(LINE -94 && LINE -101)
     */
    public static List<SObject> getJobs (String searchCriteria, String zipCode, Decimal maxDistance, String jobSent,String frequency) {
        MAP <String,String> labelValueMap = NEW MAP <String,String> ();
        if (searchCriteria.contains(',')) {
            List<String> fieldNameValues = searchCriteria.trim().split(',');
            for (String s:fieldNameValues) {
                if(s.contains(':') && s.substringAfter(':').trim() != '' && s.substringAfter(':').trim() != null) {
                    labelValueMap.put(s.substringBefore(':').trim(),s.substringAfter(':').trim());
                }
            }
        }
        system.debug('label value map..'+labelValueMap);
        List<Schema.SObjectField> ObjectFieldMap = Schema.getGlobalDescribe().get('TRHC__Job__c').getDescribe().fields.getMap().values();
        Map<String, String> labelNameMap = new Map<String, String>();
        Map<String, String> nameValueMap = new Map<String, String>();
        for (Schema.SObjectField field : ObjectFieldMap) {
            labelNameMap.put(field.getDescribe().getLabel(),field.getDescribe().getName());
        }
        system.debug('label name map..'+labelNameMap);
        for (String s:labelValueMap.keyset()) {
            nameValueMap.put(labelNameMap.get(s),labelValueMap.get(s));
        }
        
        
        List<String>jobNameList = new List<String>();
        //system.debug(labelNameMap.get('Specialty'));
        //system.debug(labelValueMap.get('Specialty'));
        //system.debug(nameValueMap);
        if (jobSent != null && jobSent != '') {
            jobNameList = jobSent.split(',');
        }
        
        
        String fieldsForQuery = '';
        for (String key : nameValueMap.keyset()){
            String fieldType = getFieldType(key);
            system.debug(fieldType);
            if (nameValueMap.size() == 1){
                if(fieldType == 'MULTIPICKLIST') {
                    fieldsForQuery = key +' INCLUDES (\''+String.escapeSingleQuotes(nameValueMap.get(key).trim()) + '\')';
                }
                else {
                    fieldsForQuery = key + ' = \'' +
                        String.escapeSingleQuotes(nameValueMap.get(key).trim()) + '\'';
                }
            } else {
                if (fieldsForQuery != null && fieldsForQuery != ''){
                    if (fieldType == 'MULTIPICKLIST') {
                        
                        fieldsForQuery = fieldsForQuery + key + ' INCLUDES(\'' +
                            String.escapeSingleQuotes(nameValueMap.get(key).trim()) +
                            '\') AND ';
                    }else {
                        fieldsForQuery = fieldsForQuery + key + ' = \'' +
                            String.escapeSingleQuotes(nameValueMap.get(key).trim()) +
                            '\' AND ';
                    }
                } else {
                    if (fieldType == 'MULTIPICKLIST') {
                        
                        fieldsForQuery = key + ' INCLUDES(\'' +
                            String.escapeSingleQuotes(nameValueMap.get(key).trim()) +
                            '\') AND ';
                    } else {
                        fieldsForQuery = key + ' = \'' +
                            String.escapeSingleQuotes(nameValueMap.get(key).trim()) +
                            '\' AND '; 
                    }
                }
            }
           // system.debug('>>>>>>>>>>fieldsForQuery in loop='+fieldsForQuery);
        } 
        
        if (nameValueMap.size() > 1) {
            fieldsForQuery = fieldsForQuery.removeEnd('AND '); 
        }
        
        system.debug('>>>>>>>>>>fieldsForQuery='+fieldsForQuery);
        if(maxDistance != null && String.isNotBlank(zipCode)) {
            
            
        }
        string query = '';
        if (frequency == 'Daily') {
            Date createddt = system.today();
            query ='SELECT '+ getAllFields('TRHC__job__c') +
                ' FROM TRHC__job__c ' +
                ' WHERE ' + fieldsForQuery +
                ' AND TRHC__Closed__c = false AND TRHC__Stage__c !=\''+'closed'+'\'' +
                ' AND Createddate = TODAY AND Name NOT IN :jobNameList';
        } 
        else {
            query ='SELECT '+ getAllFields('TRHC__job__c') +
                ' FROM TRHC__job__c ' +
                ' WHERE ' + fieldsForQuery +
                ' AND TRHC__Closed__c = false AND TRHC__Stage__c !=\''+'closed'+'\'' +
                ' AND Name NOT IN :jobNameList';
        }
        system.debug('query is..'+query);
        List<TRHC__job__c> relevantJobs = NEW List<TRHC__job__c> ();
        try {
            relevantJobs = Database.query(query);
            system.debug(relevantJobs.size()+'..'+relevantJobs);
        } catch (exception e) {
            system.debug('error is..'+e.getMessage());
        }
        
        
        return relevantJobs; 
        
        
    }
    public static String getAllFields(String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);
        
        if (objectType == null) {
            return fields;
        }
        for (string f :objectType.getDescribe ().fields.getMap ().keySet ())
            fields += f+ ', ';
        
        return fields.removeEnd(', '); 
        
    }
    
    public static string getFieldType (String fieldAPI) {
        Map<String, Schema.SObjectField> schemaMap = Schema.SObjectType.TRHC__job__c.fields.getMap();
        Schema.SObjectField fieldDetails = schemaMap.get(fieldAPI);
        Schema.DisplayType FieldType = fieldDetails.getDescribe().getType();
        return String.valueOf (FieldType);
    }
}