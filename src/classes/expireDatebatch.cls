global class expireDatebatch implements Database.Batchable<sObject>{

    public string query = '';
    global List<Contact> start(Database.BatchableContext BC){
        query = 'select id,apiToken__c,expireTokenDate__c from Contact where expireTokenDate__c = today'  ;
        return Database.Query(query);
    }

    global void execute(Database.BatchableContext BC, List<contact> conlist){
        system.debug('conlist'+conlist);
        List<Contact> updateConList = new List<Contact>();
        for(contact conObj : conlist){
            if(conObj.apiToken__c != null && conObj.expireTokenDate__c != null) {
                conObj.apiToken__c = '';
                conObj.expireTokenDate__c = null;
            }
            updateConList.add(conObj);
        }
        
        if(!updateConList.isEmpty()){
            update updateConList;
        }
    }

    global void finish(Database.BatchableContext BC){
    
    }
}