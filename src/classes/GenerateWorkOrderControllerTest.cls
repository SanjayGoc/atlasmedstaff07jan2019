@isTest
public class GenerateWorkOrderControllerTest {
    @isTest static void contactTestMethod() {
        User usr = [SELECT Id,Name,Email,FirstName,LastName FROM User Limit 1];
        
        Account a = new Account();
        a.Name = 'Test Account';
        a.Fyre__c = 'Yes';
        a.TRHC__Client_Fee__c = 3.00;
        a.billingStreet = 'Test Street';
        a.billingCity = 'Test City';
        a.billingState = 'Ohio';
        a.billingPostalCode = '380009';
        a.Phone = '9999999999';
        insert a;
        
        TRHC__config_Settings__c  configSet = new TRHC__config_Settings__c();
        configSet.Name = 'Default';
        configSet.Disable_PopolaterecruiterPlacement__c = true;
        configSet.EnableUpdateOverTimeRate__c = false;
        insert configSet;
        
        TRHC__Healthcare_Setting__c congHelth = new TRHC__Healthcare_Setting__c();
        congHelth.Name = 'Default';
        congHelth.TRHC__Enable_GSA_Service_to_Update_M_I_Rate__c = true;
        insert congHelth;
        
        Contact con = new Contact();
        con.FirstName = 'mike'; 
        con.LastName = 'saas';
        con.Email = 'test@test1.gmail.com';
        con.Compliance_Coordinater__c = usr.id;
        con.TRHC__Recruiter__c = usr.id;
        insert con;
        
        TRHC__Job__c job = new TRHC__Job__c();
        job.TRHC__Stage__c = 'closed';
        job.TRHC__Recruiter__c = usr.id;
        job.CreatedDate = Date.today();
        job.TRHC__Number_of_Placements__c = 1;
        insert job;
        
        TRHC__Placement__c plac = new TRHC__Placement__c();
        plac.TRHC__Recruiter__c = usr.id;
        plac.TRHC__Contact_Candidate__c = con.id;
        plac.TRHC__Job__c = job.id;
        plac.TRHC__Employer__c = a.Id;
        plac.TRHC__Start_Date__c = date.today();
        plac.TRHC__End_Date__c = date.today()+10;
        plac.VMS__c = 'Aya';
        plac.TRHC__Guaranteed_Hours__c = 2;
        plac.TRHC__Regular_Hours_Pay_Rate__c = 20.00;
        plac.TRHC__Overtime_Hours_Pay_Rate__c = 10.00;
        plac.TRHC__On_Call_Hours_Pay_Rate__c = 10.00;
        plac.TRHC__Holiday_Hours_Pay_Rate__c = 20.00;
        plac.TRHC__Call_Back_Hours_Pay_Rate__c = 10.00;
        plac.Additional_Required_Certs__c = 'BDL';
        plac.TRHC__Account_Manager__c = usr.Id;
        plac.TRHC__Recruiter__c = usr.Id;
        plac.TRHC__Guaranteed_Hours__c = 2;
        plac.TRHC__Regular_Hours_Pay_Rate__c = 20.00;
        plac.TRHC__Overtime_Hours_Pay_Rate__c = 10.00;
        plac.TRHC__On_Call_Hours_Pay_Rate__c = 10.00;
        insert plac; 
        
        PageReference pageRef = Page.GenrateWorkOrder;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(con);
        ApexPages.currentPage().getParameters().put('Id',con.id);
        
        Opportunity o = new Opportunity(
        Name = 'Test Opp',
        CloseDate = System.Today(),
        AccountId = a.Id,
        StageName = 'Signed / Closed Sale',
        Amount = decimal.valueof('6995'));
        insert o;
        
        dsfs__DocuSign_Status__c  dsfs = new dsfs__DocuSign_Status__c(
        dsfs__Company__c = o.AccountId,
        dsfs__Opportunity__c = o.Id,
        dsfs__Envelope_Status__c = 'Sent',
        dsfs__DocuSign_Envelope_ID__c = '1001A123-1234-5678-1D84-F8D44652A382',
        dsfs__Contact__c = con.Id,
        Placement__c = plac.Id,
        dsfs__Subject__c = 'Document for eSignature');
        insert dsfs;
        
        Attachment attach = new Attachment();     
        attach.Name = 'Unit Test Attachment Work Order';
        Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body');
        attach.body = bodyBlob;
        attach.parentId = dsfs.Id;
        insert attach;
        
        dsfs.dsfs__Envelope_Status__c = 'Completed';
        dsfs.dsfs__Contact__c = con.Id;
        dsfs.Placement__c = plac.Id;
        update dsfs;
       
        Test.StartTest();
        GenerateWorkOrderController gen = new GenerateWorkOrderController(sc);
        gen.latestPlacement();
        gen.returnContact();
        Test.StopTest();
    }
}