public without sharing class ScratchSheetExtn {
    
    private final String TEMPLATE_NAME = 'Atlas_MedStaff_scratch_sheet_Summary';
    
    public TRHC__Scratch_Sheet__c scratchSheetObj { get; set;}
    public TRHC__Scratch_Sheet__c scratchSheetrec {get; set;}
    public List<TRHC__Job_Applicant__c> JobApplicants {get;set;}
    public String templateId { get; set; }
    public String Id { get; set; }
    public Map<String, String> expenseValuemap { get; set; }
    public List<TRHC__Job__c> joblist { get; set; }
    public Account hospital {get;set;}
    public String expenseKey {get;set;}

    public ScratchSheetExtn(ApexPages.StandardController stdCon) {

        Id = Apexpages.currentPage().getParameters().get('Id');
        hospital = new Account();
        expenseKey = '';
        if(Id != null && Id != '') {
            scratchSheetObj = (TRHC__Scratch_Sheet__c) stdCon.getRecord();
            scratchSheetrec = new TRHC__Scratch_Sheet__c();
            List<TRHC__Scratch_Sheet__c> scratchSheetObjList = new List<TRHC__Scratch_Sheet__c>();
            scratchSheetObjList = [select id,CreatedBy.Name,
                                            CreatedDate,
                                            LastModifiedBy.Name,
                                            LastModifiedDate,
                                            Account_name__c,
                                            (select Name,
                                                TRHC__Account_Job__r.Name,
                                                TRHC__Job_Title__c,
                                                TRHC__Job_Num__c,
                                                TRHC__Job__r.Name,
                                                TRHC__Contact_Candidate__r.Name
                                                ,TRHC__Stage__c,
                                                TRHC__Available_To_Start__c,
                                                TRHC__Scratch_Sheet__c 
                                           from TRHC__Job_Applicants__r),
                                        (select id,Name,
                                                TRHC__Job__r.Name,
                                                TRHC__Job_Title__c,
                                                TRHC__Employer__r.Name,
                                                TRHC__Contact_Candidate__r.Name,
                                                TRHC__Start_Date__c,
                                                TRHC__Specialty__c,
                                                TRHC__Status__c,
                                                TRHC__Contract__c
                                           from TRHC__Placements__r),
                                        (select id
                                           from Histories)
                                        from TRHC__Scratch_Sheet__c 
                                       where id =: scratchSheetObj.Id];
            if(scratchSheetObjList != null && !scratchSheetObjList.isEmpty())
                scratchSheetrec = scratchSheetObjList.get(0);
            
            if(scratchSheetrec.Account_name__c != null)
                hospital = [select id,billingcity,billingState,Name from Account where name =: scratchSheetrec.Account_name__c limit 1];
            List<EmailTemplate> emailTemplatelist = [Select Id From EmailTemplate Where IsActive = true and DeveloperName =: TEMPLATE_NAME];
            templateId = '';
            if(emailTemplatelist != null && !emailTemplatelist.isEmpty()){
                templateId = emailTemplatelist.get(0).Id;
            }
            
            
            Set<String> expenseName = new Set<String>();
            expenseName.add('Housing Stipend');
            expenseName.add('Per Diem');
            expenseName.add('Travel Reimbursement');
            
            expenseValuemap = new Map<String, String>();
            List<TRHC__Expense__c> expenselist = [Select Id, TRHC__Value__c,TRHC__Expense_Name__c  FROM TRHC__Expense__c Where TRHC__Scratch_Sheet__c =: Id AND TRHC__Expense_Name__c In:expenseName];
            
            if(expenselist != null) {
                for(TRHC__Expense__c e : expenselist) {
                    expenseValuemap.Put(e.TRHC__Expense_Name__c , String.valueOf(e.TRHC__Value__c));
                    expenseKey +=  e.TRHC__Expense_Name__c + ',';
                }
            }

            
            joblist = [Select id, Unit_name__c,Shift_Type2__c FROM TRHC__Job__c WHERE TRHC__Scratch_Sheet__c =: Id limit 1];
        }
    }

    public PageReference Cancel(){
        return new PageReference('/apex/ScratchSheetOverride?id=' + scratchSheetObj.Id);
    }
    
    public PageReference Save() {
        try {
            System.debug('===save==='+scratchSheetObj);
            Update scratchSheetObj;
            
            
        } Catch (DmlException e) {
            //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity, e.getMessage());
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getMessage()));
            System.debug('===save==='+e);
            return null;
        } Catch (Exception e) {
            //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity, e.getMessage());
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getMessage()));
            System.debug('===save==='+e);
            return null;
        }
        
        return new PageReference('/apex/ScratchSheetOverride?id=' + scratchSheetObj.Id);
    }

    public PageReference submitforApprove() {
    
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setObjectId(scratchSheetObj.Id);
        req1.setSubmitterId(UserInfo.getUserId());
        req1.setProcessDefinitionNameOrId('GPH_IS_NOT_IN_NORMAL_LIMIT_NEW');
        req1.setSkipEntryCriteria(true);

        Approval.ProcessResult result = Approval.process(req1);
        System.debug('===result==='+result.isSuccess());
        
        return new PageReference('/' + scratchSheetObj.Id);
    }
}