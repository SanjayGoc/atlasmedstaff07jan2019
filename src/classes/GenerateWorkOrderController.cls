public class GenerateWorkOrderController {
    public Id ContactId;
    public List<TRHC__Placement__c> latestPlms;
    public GenerateWorkOrderController(ApexPages.StandardController controller) {
        latestPlms = new List<TRHC__Placement__c>();
        ContactId = ApexPages.CurrentPage().getparameters().get('Id');
    }
    public PageReference latestPlacement() {
        latestPlms = [SELECT Id, Name, TRHC__Employer__c 
                        FROM TRHC__Placement__c 
                        WHERE TRHC__Contact_Candidate__c =: ContactId ORDER BY LastModifiedDate DESC LIMIT 1];
        String sessionId = UserInfo.getSessionId();
        PageReference pageRef;
        if(latestPlms.size() > 0) {
            pageRef = new PageReference('/apex/loop__looplus?eid=' + latestPlms[0].Id + '&accountId=' + latestPlms[0].TRHC__Employer__c + '&sessionId=' + sessionId + '&hidecontact=true' + '&ddpIds=a2y0q0000002bvU&autorun=true&retURL=/' + latestPlms[0].Id);
            return pageRef;
        } else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,System.Label.No_Placement_Message));
        }
        return pageRef;
    }
    public PageReference returnContact() {
        PageReference pageRef = new PageReference('/' + ContactId);
        return pageRef;
    }
}