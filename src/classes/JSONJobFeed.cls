/**
 * @author : chaitanya bollamreddi
 * @date : 13 April 2018
 * @name :JSONClass
 * @description : Class is refering an VF page JsonFeedTN
 */
public with sharing class JSONJobFeed{
    public list<TRHC__job__c> jobList { get{
        //Fetch the Account List
        jobList = [
            SELECT Id, NAME, TRHC__REGULAR_BILL_RATE__C, SPECIALITY1__C, TRHC__START_DATE__C, SHIFT_TYPE2__C,
					TRHC__HOURS_TO_BE_CONSIDER_PER_DAY__C, TRHC__NUMBER_OF_DAYS_PER_WEEK__C, ASSIGNMENT_LENGTH__C,
            		TRHC__HOURS_PER_WEEK__C ,TRHC__STREET_ADDRESS__C,TRHC__STATE__C, TRHC__JOB_CITY__C,
            		TRHC__CLIENT_FEE__C,ACCOUNT_NAME__C, TRHC__JOB_DESCRIPTION_RICH__C, CreatedDate
            FROM TRHC__Job__c
            WHERE TRHC__Publish__c = true
            AND TRHC__Stage__c = 'Open' Order By createddate desc Limit 800];
        
        if(jobList == null){
            jobList = new list<TRHC__JOb__c>();            
        }
        return jobList;
    } set;}   
    
    public string jsonSerial {get{
        //JSON Class method    
        if(jobList == null){
            jsonSerial = '';            
        }else{
            jsonSerial = JSON.serialize(jobList);
        }
        return jsonSerial;  
        
    } set; }
    
    public string jsonGen { get{
        //JSON Generator Class method 
        JSONGenerator gen = JSON.createGenerator(true);//true gives output in pretty print
        
        if(jobList == null){
            return '';          
        }else{
            
            gen.writeStartArray();
            
            for(TRHC__Job__c job: jobList){
                
                gen.writeStartObject();
                if(job.Name != null){
                	gen.writeStringField('NAME', job.Name);  
                }else{
                 	gen.writeStringField('NAME', '');   
                }
                if(job.TRHC__REGULAR_BILL_RATE__C != null){
                 gen.writeNumberField('REGULAR BILL RATE', job.TRHC__REGULAR_BILL_RATE__C); 
                }else{
                    gen.writeNumberField('REGULAR BILL RATE', 0);
                }
                if(job.SPECIALITY1__C != Null){
                  gen.writeStringField('SPECIALITY', job.SPECIALITY1__C);  
                }else{
                  gen.writeStringField('SPECIALITY', ''); 
                }
                if(job.TRHC__START_DATE__C != null){
                 	String Cdate = job.TRHC__START_DATE__C.month() +'/'+job.TRHC__START_DATE__C.day()+'/'+job.TRHC__START_DATE__C.year();
                    gen.writeStringField('START DATE', Cdate);  
                }else{
                    gen.writeStringField('START DATE', '');
                }
                if(job.SHIFT_TYPE2__C != null){
                 gen.writeStringField('SHIFT', job.SHIFT_TYPE2__C);   
                }
                else{
                    gen.writeStringField('SHIFT', '');
                }
                if(job.TRHC__HOURS_TO_BE_CONSIDER_PER_DAY__C != null){
					gen.writeNumberField('HOURS TO BE CONSIDER PER DAY', job.TRHC__HOURS_TO_BE_CONSIDER_PER_DAY__C);
                }else{
                    gen.writeNumberField('HOURS TO BE CONSIDER PER DAY', 0);
                }
                if(job.TRHC__NUMBER_OF_DAYS_PER_WEEK__C != null){
                  gen.writeNumberField('NUMBER OF DAYS PER WEEK', job.TRHC__NUMBER_OF_DAYS_PER_WEEK__C);  
                }else{
                  gen.writeNumberField('NUMBER OF DAYS PER WEEK', 0);  
                }
                if(job.ASSIGNMENT_LENGTH__C != null){
                   gen.writeNumberField('ASSIGNMENT LENGTH', job.ASSIGNMENT_LENGTH__C); 
                }else{
                   gen.writeNumberField('ASSIGNMENT LENGTH', 0);
                }
                if(job.ACCOUNT_NAME__C != null){
                   gen.writeStringField('ACCOUNT NAME', job.ACCOUNT_NAME__C); 
                }else{
                    gen.writeStringField('ACCOUNT NAME', ''); 
                }
                if(job.TRHC__HOURS_PER_WEEK__C != null){
                    gen.writeNumberField('HOURS PER WEEK', job.TRHC__HOURS_PER_WEEK__C);
                }else{
                    gen.writeNumberField('HOURS PER WEEK', 0);
                }
                if(job.TRHC__STREET_ADDRESS__C != null){
                   gen.writeStringField('STREET ADDRESS',job.TRHC__STREET_ADDRESS__C);
                }else{
                   gen.writeStringField('STREET ADDRESS','');
                }
                if(job.TRHC__STATE__C != null){
                  gen.writeStringField('STATE', job.TRHC__STATE__C);  
                }else{
                   gen.writeStringField('STATE', ''); 
                }
                if(job.TRHC__JOB_CITY__C == null)
                    gen.writeStringField('JOB CITY', '');  
                else{
                    gen.writeStringField('JOB CITY',job.TRHC__JOB_CITY__C); 
                }
                if(job.TRHC__Job_Description_Rich__c != null){
                  gen.writeStringField('Description', job.TRHC__Job_Description_Rich__c);  
                }else{
                   gen.writeStringField('DESCRIPTION', ''); 
                }
                if(job.TRHC__CLIENT_FEE__C != null){
                    gen.writeNumberField('CLIENT FEE',job.TRHC__CLIENT_FEE__C);
                }else{
                    gen.writeNumberField('CLIENT FEE',0);
                }
                gen.writeEndObject();
                
            }
            
            gen.writeEndArray();
            return gen.getAsString();
        }
        
    } set; } 
    
}