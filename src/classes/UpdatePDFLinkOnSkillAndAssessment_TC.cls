@isTest
public class UpdatePDFLinkOnSkillAndAssessment_TC {
    private static testMethod void updateSkills () {
        PDF_URL_Update_Setting__c newSetting = new PDF_URL_Update_Setting__c ();
            newSetting.Enable__c = true;
        insert newSetting;
        TRHC__Config_Settings__c contactSetting = new TRHC__Config_Settings__c ();
            contactSetting.Enable_RecruitersMailNotificationTrigger__c = true;
            contactSetting.Name= 'Default';
        insert contactSetting;
        Contact newContact = new Contact ();
            newContact.LastName = 'Test';
            newContact.Email = 'test@test.com';
        insert newContact;
        
        TRHC__Skill_Answers__c newSkill = new TRHC__Skill_Answers__c ();
            newSkill.TRHC__Contact__c = newContact.Id;
        insert newSkill;
        
        TRHC__Assessment__c newAssessment = new TRHC__Assessment__c ();
            newAssessment.TRHC__Contact__c = newContact.Id;
        insert newAssessment;
        
        Attachment skillAttchment = new Attachment ();
            skillAttchment.ParentId = newSkill.id;
            skillAttchment.Name = 'Test.PDF';
            skillAttchment.Body = Blob.ValueOf ('Test');
        insert skillAttchment;
        
        Attachment assessmentAttachment = new Attachment ();
            assessmentAttachment.ParentId = newAssessment.Id;
            assessmentAttachment.Name = 'Test';
            assessmentAttachment.Body = Blob.ValueOf('Test');
        insert assessmentAttachment;
    }
}