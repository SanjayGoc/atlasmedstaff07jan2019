global class BatchDeleteInactiveSkills implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query;
        if(!Test.isRunningTest()){
            //Date d = Date.today().addDays(-7);
            DateTime dateTimes = System.today().addDays(-30);
            dateTimes = dateTimes.addHours(23).addMinutes(59).addSeconds(59);
            query = 'SELECT Id FROM TRHC__Skill_Answers__c WHERE PDF_URL__c = null AND TRHC__Results_URL__c = null AND TRHC__Overall_Comprehension__c = null AND CreatedDate <=: dateTimes';
        }else{
            Date d = Date.today();
            query = 'SELECT Id FROM TRHC__Skill_Answers__c WHERE PDF_URL__c = null AND TRHC__Results_URL__c = null AND TRHC__Overall_Comprehension__c = null AND CreatedDate <: d Limit 1';
        }
        return Database.getQueryLocator(query);
    }
    
    global Void execute(Database.BatchableContext BC, List<sObject> scope) {
        List<TRHC__Skill_Answers__c> skillsToDelete = new List<TRHC__Skill_Answers__c>();
        
        for(TRHC__Skill_Answers__c skill : (List<TRHC__Skill_Answers__c>) scope) {
            skillsToDelete.add(skill);
        }
        
        if(!skillsToDelete.isEmpty()) {
            delete skillsToDelete;
        }
    }
    /*,Database.AllowsCallouts, Database.Stateful*/
    global Void finish(Database.BatchableContext BC) {
    
    }
}