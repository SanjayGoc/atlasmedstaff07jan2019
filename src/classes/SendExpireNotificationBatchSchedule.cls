/*
============================================================================
Description:   Schedule class for SendExpireNotificationBatch class 
               Send Email to Candidate, Recruiter and Complaince coordinator      
Functionality: Run batch SendExpireNotificationBatch
============================================================================
*/

global class SendExpireNotificationBatchSchedule implements Schedulable {
    global void execute(SchedulableContext sc) {
        SendExpireNotificationBatch sch = new SendExpireNotificationBatch();
        database.executebatch(sch, 200);
    }  
}