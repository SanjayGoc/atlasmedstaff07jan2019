@isTest
public class sendEmailTest{
	static testmethod void EmailTest() {
     	
        
        TRHC__Config__c configObj1 = new TRHC__Config__c();
        configObj1.TRHC__Name__c = 'check';
        configObj1.TRHC__Margin_Type__c = 'Expense';
        configObj1.TRHC__Frequency__c = 'Weekly';
        configObj1.TRHC__State__c =   'California';
        configObj1.TRHC__Include_in_payroll__c= True;
        configObj1.TRHC__Pay_Period_to_be_paid__c='First Check';
        configObj1.TRHC__Active__c = True;
        configObj1.TRHC__Value__c = '350.0';
        configObj1.TRHC__Grouping_Total__c ='Other Expense';
        configObj1.TRHC__City__c = 'test';
        
        configObj1.RecordTypeId =[Select Id From RecordType Where SobjectType = 'TRHC__Config__c' and Name = 'Margin calculation Line Item'].Id;
        
        insert configObj1;

        TRHC__Config_Settings__c configSettings = new TRHC__Config_Settings__c();
        configSettings.name='Default';
        configSettings.Enable_AutoMateJobRates__c = true;
        configSettings.Trigger_Enable_ScratchSheetAutoAddExp__c = true;
        configSettings.AutoAddExpenseCriteria__c='TRHC__Margin_Type__c=\'Expense\'';
        configSettings.Except_Hospitals__c = 'Atlas Development';
        configSettings.Trigger_Enable_Expected_End_Date_Changer__c = false;
        configSettings.Trigger_Enable_Applicant_From_SCR_Sheet__c = true;
        configSettings.Update_Pay_Burden__c = false;
        configSettings.ExceptConfigIds__c = configObj1.Id;
        configSettings.Enable_RefferalTrigger__c = true;
        configSettings.TRHC__Enable_Geosearch_for_Account__c = false;
        configSettings.Nurse_Gross_Weekly_Pay__c = 'Test Custom Setting';
        insert configSettings;

        TRHC__Healthcare_Setting__c customSetting = NEW TRHC__Healthcare_Setting__c(Name = 'Default');
        customSetting.TRHC__Enable_GSA_Service_to_Update_M_I_Rate__c = true;
        insert customSetting;

       


        
        
       /* TRHC__Config__c configObj = new TRHC__Config__c();
        configObj.TRHC__Name__c = 'check';
        configObj.TRHC__Margin_Type__c = 'Expense';
        configObj.TRHC__Frequency__c = 'Weekly';
        configObj.TRHC__State__c =   'California';
        configObj.TRHC__Include_in_payroll__c= True;
        configObj.TRHC__Pay_Period_to_be_paid__c='First Check';
        configObj.TRHC__Active__c = True;
        configObj.TRHC__Value__c = '350.0';
        configObj.TRHC__Grouping_Total__c ='Pay Burden';

        TRHC__Config_Settings__c configSettings = new TRHC__Config_Settings__c();
        configSettings.name='Default';
        configSettings.Enable_AutoMateJobRates__c = true;
        configSettings.Trigger_Enable_ScratchSheetAutoAddExp__c = true;
        configSettings.AutoAddExpenseCriteria__c='TRHC__Margin_Type__c=\'Expense\'';
        configSettings.Except_Hospitals__c = 'Atlas Development';
        configSettings.Trigger_Enable_Expected_End_Date_Changer__c = false;
        configSettings.Trigger_Enable_Applicant_From_SCR_Sheet__c = true;
        configSettings.Update_Pay_Burden__c = false;
        configSettings.ExceptConfigIds__c = configObj.Id;
        configSettings.Enable_RefferalTrigger__c = true;
        configSettings.TRHC__Enable_Geosearch_for_Account__c = false;
        configSettings.Nurse_Gross_Weekly_Pay__c = 'Test Custom Setting';
        insert configSettings;
*/
        


        account ac = new account();
        ac.Name = 'SharmaMedical';
        ac.BillingLatitude = 29.708360;
        ac.BillingLongitude = -95.401928;
        insert ac;

        contact conobj = new contact();
        conobj.lastName = 'sharma';
        conobj.TRHC__Password__c = '12345678';
        conobj.Email = 'HIREN.GOC@GMail.com';
        conobj.Temporary_Address_1__c = '11227 Franklin Plaza Apt 1621';
        conobj.apiToken__c = '123546789879456123';
        insert conobj;


        TRHC__Scratch_Sheet__c newScratchSheetTemp = new TRHC__Scratch_Sheet__c();
        newScratchSheetTemp.TRHC__County__c = 'Kalvin';
        newScratchSheetTemp.TRHC__State__c='California';
        newScratchSheetTemp.TRHC__Candidate__c = conobj.Id;
        newScratchSheetTemp.State_pic__c = 'California';
        newScratchSheetTemp.Name='Demo ScratchSheet';
        String scrtchSheetTempRecordTypeId1 = [Select Id From RecordType Where SobjectType = 'TRHC__Scratch_Sheet__c' and Name = 'Scratchsheet'].Id;
        newScratchSheetTemp.RecordTypeId=scrtchSheetTempRecordTypeId1 ;
        newScratchSheetTemp.TRHC__State__c='California';
        newScratchSheetTemp.LastModifiedDate = date.today()-40;
        newScratchSheetTemp.CreatedDate = date.today()-41;
        newScratchSheetTemp.Non_Taxable_Expense__c = 11953.00;
        newScratchSheetTemp.Nurse_Gross_weekly_pay__c = 851.00;
        newScratchSheetTemp.TRHC__Pay_Rate__c = 12.00;
        insert newScratchSheetTemp;
        
        newScratchSheetTemp.TRHC__Pay_Rate__c = 25.00;
        update newScratchSheetTemp;
        
		/*TRHC__Healthcare_Setting__c customSetting = new TRHC__Healthcare_Setting__c();
		customSetting.Name = 'Default';
		customSetting.TRHC__Enable_GSA_Service_to_Update_M_I_Rate__c = true;
		insert customSetting;*/

		FireBaseServerKeyMobile__c server  = new FireBaseServerKeyMobile__c();
		server.Name = 'Default';
		server.serverKey__c = 'AAAAr1KJK-k:APA91bFeuugxXEKJwAEKQDbo7yQ76RXAzxH0wpsGAKr0Df6C1eXItHrb6hd6HH_QsYXGgPIPQs3baLLsu6VEDRgTAa366YjGT8_Pg6mHganNtNDl1zRh289wYu6-uWpfo65azYNzqf2D';
		insert server;

//		List<id> jobList = new List<id>();

		TRHC__Job__c job = new TRHC__Job__c();
		job.TRHC__Stage__c = 'closed';
		job.TRHC__Hiring_Manager__c = conobj.id;
		job.CreatedDate = Date.today();
		job.TRHC__Number_of_Placements__c = 1;
		job.Unit_name__c = 'Test name';
		insert job;

        TRHC__Expense__c expense = new TRHC__Expense__c();
        expense.TRHC__Expense_Name__c = 'Travel Reimbursement';
        expense.TRHC__Scratch_Sheet__c = newScratchSheetTemp.Id;
        expense.TRHC__Amount__c = 800.00;
        expense.TRHC__Value__c = 15.00;
        expense.TRHC__Grouping_Total__c = 'Non Taxable Expense';
        expense.TRHC__Frequency__c = 'Per Assignment';
        insert expense;

        TRHC__Expense__c expense1 = new TRHC__Expense__c();
        expense1.TRHC__Expense_Name__c = 'Housing Stipend';
        expense1.TRHC__Scratch_Sheet__c = newScratchSheetTemp.Id;
        expense1.TRHC__Amount__c = 800.00;
        expense1.TRHC__Value__c = 15.00;
        expense1.TRHC__Grouping_Total__c = 'Non Taxable Expense';
        expense1.TRHC__Frequency__c = 'Per Assignment';
        insert expense1;

        TRHC__Expense__c expense2 = new TRHC__Expense__c();
        expense2.TRHC__Expense_Name__c = 'Per Diem';
        expense2.TRHC__Scratch_Sheet__c = newScratchSheetTemp.Id;
        expense2.TRHC__Amount__c = 800.00;
        expense2.TRHC__Value__c = 15.00;
        expense2.TRHC__Grouping_Total__c = 'Non Taxable Expense';
        expense2.TRHC__Frequency__c = 'Per Assignment';
        insert expense2;

        EmailTemplate email = new EmailTemplate (Subject = 'emailsend',  FolderId = UserInfo.getUserId() , TemplateType= 'Text', Name = 'test',body = 'hreasdads', Developername = 'Atlas_MedStaff_scratch_sheet_Summary');

        insert email;
 
      /*  List<EmailTemplate> emailTemplatelist = [Select Id, Name, Subject, TemplateType, FolderId, Folder.Name From EmailTemplate Where IsActive = true And Developername =: 'Atlas_MedStaff_scratch_sheet_Summary' order by Name];
      */ 

		//Test.setMock(HttpCalloutMock.class, new MockHttpResponseFireBasePush ());
        
            
		/*System.RunAs() {
            EmailTemplate email = new EmailTemplate (Subject = 'emailsend',  FolderId = UserInfo.getUserId() , TemplateType= 'Text', Name = 'test',body = 'hreasdads', Developername = 'Atlas_MedStaff_scratch_sheet_Summary');
 
            insert email;
        }
*/
        Test.startTest();

            
			
			Test.setMock(HttpCalloutMock.class, new MockHttpResponseFireBasePush ());

			PageReference pageRef = Page.sendEmail; // Add your VF page Name here
			pageRef.getParameters().put('Id', newScratchSheetTemp.id);
			pageRef.getParameters().put('TemplateID', email.id);
			Test.setCurrentPage(pageRef);

	        sendEmailController emcont = new sendEmailController();

	        emcont.emailTo = 'hiren.goc@gmail.com';
	        emcont.emailBody = 'hiren sharma';
            emcont.emailCC = 'rest';
            emcont.emailBCC  = 'rest';
            emcont.emailSubject = 'rest';
            emcont.selFromEmail = 'rest';
            
            emcont.getEmailTemplatesByfolder();
            
            emcont.init();
	        emcont.cancel();
	        emcont.sendEmail();
	        emcont.reloadPage();


     	Test.stopTest();   
	}
}