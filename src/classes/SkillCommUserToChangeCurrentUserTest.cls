@isTest
public class SkillCommUserToChangeCurrentUserTest {
    static testmethod void SkillCommUserToChangeCurrentUserTestMethod() {
        List<User> usr = [SELECT Id,Name,Email,FirstName,LastName FROM User WHERE IsActive = true LIMIT 3];
        Contact con = new Contact();
        con.FirstName = 'mike'; 
        con.LastName = 'saas';
        con.Email = 'test@test1.gmail.com';
        con.Compliance_Coordinater__c = usr[0].id;
        con.TRHC__Recruiter__c = usr[1].id;
        insert con;
        TRHC__Skill_Answers__c skill = new TRHC__Skill_Answers__c();
        skill.TRHC__Contact__c = con.Id;
        skill.isUpdated__c = true;
        insert skill;
        Test.startTest();
        SkillCommUserToChangeCurrentUser btc = new SkillCommUserToChangeCurrentUser();
        DataBase.executeBatch(btc, 1);
        Test.stopTest();
    }
    static testmethod void SkillCommUserToChangeCurrentUserTestSecond() {
        List<User> usr = [SELECT Id,Name,Email,FirstName,LastName FROM User WHERE IsActive = true LIMIT 3];
        Contact con = new Contact();
        con.FirstName = 'mike'; 
        con.LastName = 'saas';
        con.Email = 'test@gmail.com';
        con.Compliance_Coordinater__c = usr[0].id;
        con.TRHC__Recruiter__c = usr[1].id;
        insert con;
        TRHC__Skill_Answers__c skill = new TRHC__Skill_Answers__c();
        skill.TRHC__Contact__c = con.Id;
        skill.isUpdated__c = false;
        insert skill;
        Test.startTest();
        SkillCommUserToChangeCurrentUser btc = new SkillCommUserToChangeCurrentUser();
        DataBase.executeBatch(btc, 1);
        Test.stopTest();
    }
}