public class ResumeTemplate_deleteEduHistories implements Database.Batchable<Sobject> {
    private String eduWhere = '';
    private String eduQuery = '';
    public List<TRHC__Educational_History__c> start(Database.BatchableContext ctx){
        
        string candiId ='0031L00002lpbVy';
        TRHC__config_Settings__c config = TRHC__config_Settings__c.getInstance('Default');
        if(config != null)
        eduWhere = config.deleteEdu_where__c != null?config.deleteEdu_where__c:'';
        eduQuery =	'SELECT Candidate_text__c, createdDate, lastmodifieddate'+
            			' FROM TRHC__Educational_History__c'+ 
            			' Where candidate_text__c != null And TRHC__Candidate__c = '+ '\''+candiId+'\''+
            			' And candidate_text__c !='+'\''+'\'';
        if(eduWhere != '')
        eduQuery += ' AND ' +eduWhere;
        return (List<TRHC__Educational_History__c>)Database.query(eduQuery);
    }
    public void execute(Database.BatchableContext ctx, List<TRHC__Educational_History__c> edHist){
        if(edHist.size()>0)
    	delete edHist;   
    }
    public void finish(Database.BatchableContext ctx)
    {
        
    }
}