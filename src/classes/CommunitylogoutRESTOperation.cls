@RestResource(urlMapping='/mobile/logout/*')
global class CommunitylogoutRESTOperation {
    @HttpGet
    global static ResponseHandler CommunitylogoutRESTOperation (){
        String apiToken = RestContext.request.params.get('apiToken');

        ResponseHandler res = new ResponseHandler();

        List<Contact> userList = [Select Id,
                                         Name,
                                         apiToken__c
                                         
                                From Contact 
                                Where apiToken__c =: apiToken
                                ];

        if(!userList.isEmpty()){

            if(userList[0].apiToken__c != null){
                userList[0].apiToken__c = ' ';
                update userList[0];
            }
            res.success = 1;
            res.message = new string[]{'Succesfully logout.'};
            return res;
        }
        res.success = 2;
        res.message = new string[]{'Unauthorized User.'};
        return res;
    }
    
     global class ResponseHandler {
        global integer success;
        global string[] message;
        
        global ResponseHandler () {
        }
    }
}