public with sharing class CreateScrShtAndCalcGPHcusObjExpCont {
   /* public Boolean isCreated {get;set;}
    public Boolean isError{get;set;}
    private static TRHC__Healthcare_Setting__c customSetting = TRHC__Healthcare_Setting__c.getAll().get('Default');
    public string queryString;
    public List<TRHC__Job__c> jobList{get;set;}
    public List<string> jobIdList = new  List<Id>();
    public List<Expense__c> expancesList{get;set;}
    private string jobFieldList;
    public string candidateId;
    public TRHC__Scratch_Sheet__c scratchSheet{get;set;}
    public List<Expense__c> listExpense{get;set;}
    public decimal payBurdern{get;set;}
    string defaultScratchTemplate;
    string allfields;
    string scrtchSheetRecordTypeId;
    TRHC__Scratch_Sheet__c newScratchSheet;
    public Boolean checkGSAService;
    public Boolean billPayRate {get;set;}
    public id scrid;
    public string jobIds;

    public CreateScrShtAndCalcGPHcusObjExpCont (){
        checkGSAService = (Boolean)customSetting.get('TRHC__Enable_GSA_Service_to_Update_M_I_Rate__c');
        isCreated = false;
        isError = false;
        jobIds = ApexPages.currentPage().getParameters().get('jId');
        candidateId = ApexPages.currentPage().getParameters().get('cId');
        jobIdList = (jobIds != null) ? jobIds.split(',') : new  List<Id>();

        //get jobFields
        jobFieldList = ObjectFieldsHelper.getAllFields('TRHC__Job__c').get('TRHC__Job__c');

        //query for job
        string jobQuery = 'Select ' + jobFieldList + ' from TRHC__Job__c where ID IN : jobIdList';

        //String jobQuery ='Select id,trhc__state__c+' from TRHC__Job__c where ID IN'+jobIdList;
        jobList = Database.query(jobQuery);

        if(jobList.get(0).TRHC__Regular_Bill_Rate__c > 60){
            billPayRate = false;
        } else {
            billPayRate = true;
        }   */
        //insertGsaSerValue();
    //}

    /*public void insertGsaSerValue(){
    String gsaURL = 'http://gsa.targetrecruit.com/Geocode/getGsaDetails_new.php';
    List<TRHC__Job__c> jobList = [SELECT id, Name, TRHC__Start_Date__c, TRHC__Expected_End_Date__c, State_pic__c, GSA_Lodging__c,TRHC__County__c,GSA_M_I_Rate__c, Tax_Rate__c From TRHC__Job__c where TRHC__Start_Date__c !=  null and TRHC__Expected_End_Date__c != null and id =: jobIds];
    //system.debug('==== jobList ===='+jobList);
    List<TRHC__Job__c> jobUpdateList = new List<TRHC__Job__c>();
    for(TRHC__Job__c jobOBJ : jobList) {
    if (jobOBJ.State_pic__c != null){ 

    String startDateStr = jobOBJ.TRHC__Start_Date__c != null ? 
    jobOBJ.TRHC__Start_Date__c.year() + '-'+
    jobOBJ.TRHC__Start_Date__c.month() + '-'+
    jobOBJ.TRHC__Start_Date__c.day() : '' ;
    String endDate = jobOBJ.TRHC__Expected_End_Date__c != null ?
    jobOBJ.TRHC__Expected_End_Date__c.year() + '-' +
    jobOBJ.TRHC__Expected_End_Date__c.month() + '-' +
    jobOBJ.TRHC__Expected_End_Date__c.day() : ''; 
    String state = '';                  
    state = jobOBJ.State_pic__c;
    state = state.replace(' ','%20');
    String county = jobOBJ.TRHC__County__c != null ? jobOBJ.TRHC__County__c.replace(' ','%20') :'';

    HttpRequest request = new HttpRequest();
    String GSARquestURL = gsaURL +'?sdate=' + startDateStr +'&edate='+endDate+'&state='+state+'&county='+county;
    request.setEndpoint(GSARquestURL);
    //system.debug('========= GSARquestURL ==========='+GSARquestURL);
    request.setMethod('GET');
    request.setCompressed(false);
    request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
    //system.debug(LoggingLevel.ERROR, 'gsa request>>>' + request);
    HttpResponse response = new HttpResponse();

    if(!Test.isRunningTest() && Limits.getCallouts() < Limits.getLimitCallouts()){
    response = new Http().send(request);
    }else{
    response.setBody('[{"GSADestination":"Gulf Shores","State":"AL","GSACounty":"Baldwin","BeginDate":"2010-08-01","EndDate":"2010-09-30","Lodging":"111","MIE":null,"Total":"162","CombinedTaxRate":"8.51"}]');
    response.setStatusCode(200);
    }

    //system.debug(LoggingLevel.ERROR, ':::::::::GSA response = ' + response);
    //system.debug(LoggingLevel.ERROR, ':::::::::response.getBody() = ' + response.getBody());

    if (response.getStatusCode() == 200) {
    String body = response.getBody();
    String jsonBody = body.substring(1,body.length()-1);
    //system.debug(LoggingLevel.ERROR, ':::::::::body' + jsonBody);
    CreateScrShtAndCalcGPHcusObjExpCont.GSAResponse gsaResponse  = parseResponse(jsonBody);

    //system.debug(LoggingLevel.ERROR, ':::::::::gsaResponse after parsing = ' + gsaResponse);

    jobOBJ.GSA_M_I_Rate__c = gsaResponse.MIE != null && gsaResponse.MIE != '' ? Decimal.Valueof(gsaResponse.MIE):0;
    jobOBJ.GSA_Lodging__c = gsaResponse.Lodging != null && gsaResponse.Lodging != '' ? Decimal.Valueof(gsaResponse.Lodging):0;
    jobOBJ.Tax_Rate__c =  gsaResponse.CombinedTaxRate != null && gsaResponse.CombinedTaxRate != '' ? Decimal.ValueOf(gsaResponse.CombinedTaxRate):0;
    jobUpdateList.add(jobOBJ);
    }
    }
    }
    if(!jobUpdateList.isEmpty()){
    update jobUpdateList;
    }
    }*/

   /* public Void insertScratchsheet() {


        checkGSAService = (Boolean)customSetting.get('TRHC__Enable_GSA_Service_to_Update_M_I_Rate__c');
       */ //getting scrtach sheet recordtype
       /* scrtchSheetRecordTypeId = [SELECT
                                        Id
                                        FROM
                                        RecordType
                                        WHERE
                                        SobjectType = 'TRHC__Scratch_Sheet__c' AND Name = 'ScratchSheet'].Id;
        defaultScratchTemplate = (string)customSetting.get('TRHC__Default_Scratch_sheet_Template_Name__c');
        defaultScratchTemplate = '\'' + defaultScratchTemplate + '\'';*/

        //get All fields of an sobject 
        /*Map<string, string> scratchSheethelper = ObjectFieldsHelper.getAllFields('TRHC__Scratch_Sheet__c');
        allfields = scratchSheethelper.get('TRHC__Scratch_Sheet__c');
        queryString = 'Select ' + allfields + ' from TRHC__Scratch_Sheet__c ' + ' where name = ' + defaultScratchTemplate;
        if(defaultScratchTemplate == null || defaultScratchTemplate == '') {
            ApexPages.addMessage(new  ApexPages.message(ApexPages.Severity.ERROR, '<span style="font-family: Tahoma;">' + 'Scrtch sheet is not available' + '.</span>'));
            isError = true;
        }*/

       /* if(defaultScratchTemplate != null) {
            for(TRHC__Job__c job: jobList) {
                TRHC__Scratch_Sheet__c scratchSheet = Database.query(queryString);
                string expnaceFields = ObjectFieldsHelper.getAllFields('Expense__c').get('Expense__c');
            */    //get all expanses of scratch sheet template query
/*
                Id cid = string.valueOf(scratchSheet.id).subString(0, 15);
                string sid = '\'' + cid + '\'';
                string expancesQueryString = 'Select ' + expnaceFields + ' from Expense__c ' + ' where Scratch_Sheet__c =' + sid;

                List<Expense__c> templateExpnaseList = Database.query(expancesQueryString);
                newScratchSheet = scratchSheet.clone(true, false);
                newScratchSheet.id = null;
                if(candidateId != null) {
                    newScratchSheet.TRHC__Candidate__c = candidateId;
                    //mapping fields
                }

                List<Job_to_Scratch_Sheet__c> jobToScratchSheet = [SELECT
                                                Job_Field_API_Name_Custom__c, 
                                                Scratch_Sheet_Field_API_Name__c
                                                FROM
                                                Job_to_Scratch_Sheet__c];

                for(Job_to_Scratch_Sheet__c jobToScratch: jobToScratchSheet) {
                    string jobField = jobToScratch.Job_Field_API_Name_Custom__c;
                    string scratchSheetField = jobToScratch.Scratch_Sheet_Field_API_Name__c;
                    if(jobFieldList.contains(jobField) && allfields.contains(scratchSheetField)) {
                        newScratchSheet.put(scratchSheetField, job.get(jobField));
                    }
                }
                newScratchSheet.TRHC__County__c = job.TRHC__County__c;
                newScratchSheet.Name = job.TRHC__Job_Title__c + ' ScratchSheet';
                newScratchSheet.RecordTypeId = scrtchSheetRecordTypeId;

                //if(checkGSAService == false) {
                /*
                newScratchSheet.TRHC__GSA_M_I_Rate__c = job.GSA_M_I_Rate__c;
                newScratchSheet.TRHC__GSA_Lodging__c = job.GSA_Lodging__c;
                newScratchSheet.TRHC__Tax_Rate__c = job.Tax_Rate__c;
                */
                //}
                /*newScratchSheet.OT_Pay_rate_Override__c = true;

                Insert newScratchSheet;*/

                /*expancesList = new List<Expense__c>();
                for(Expense__c expanse: templateExpnaseList) {
                    expanse.id = null;
                    expanse.Scratch_Sheet__c = newScratchSheet.id;
                    if(expanse.Expense_Name__c == 'Orientation Hours'){
                        if(newScratchSheet.TRHC__No_Bill_Orientation_Hours__c != null && newScratchSheet.TRHC__Pay_Rate__c != null){
                            Decimal value = newScratchSheet.TRHC__No_Bill_Orientation_Hours__c * newScratchSheet.TRHC__Pay_Rate__c;
                            expanse.Value__c = value;*/
                            //system.debug('====== expanse.Value__c ======='+expanse.Value__c);
                       /* }
                    }
                    expancesList.add(expanse);
                }


                Insert expancesList;

                job.TRHC__Scratch_Sheet__c = newScratchSheet.id;

                Update Job;
*/
                /*  if(job.GSA_M_I_Rate__c != null && job.GSA_Lodging__c != null && job.Tax_Rate__c != null){

                newScratchSheet.TRHC__GSA_M_I_Rate__c = job.GSA_M_I_Rate__c;
                newScratchSheet.TRHC__GSA_Lodging__c = job.GSA_Lodging__c;
                newScratchSheet.TRHC__Tax_Rate__c = job.Tax_Rate__c;
                update newScratchSheet;
                }
                //system.debug('===== GSA_M_I_Rate__c  ====='+job.GSA_M_I_Rate__c);
                //system.debug('===== GSA_Lodging__c  ====='+job.GSA_Lodging__c);
                //system.debug('===== Tax_Rate__c  ====='+job.Tax_Rate__c);*/

              /*  scrid = job.TRHC__Scratch_Sheet__c;
            }
        }


        TRHC__Config_Settings__c configSettings = TRHC__Config_Settings__c.getValues('Default');

        Boolean isEnabled = configSettings == null ? True : (Boolean) configSettings.Trigger_Enable_ScratchSheetAutoAddExp__c;

        List<String> exptIdsList = configSettings == null ? new List<Id>() : configSettings.ExceptConfigIds__c.split(';');
        List<String> exptAccountList = configSettings == null ? new List<String>() : configSettings.Except_Hospitals__c.split(';');

       */ //system.debug('===== exptIdsList ====='+exptIdsList);
        //system.debug('===== exptAccountList ====='+exptAccountList);

        //String configFields = ObjectFieldsHelper.getAllFields('TRHC__Config__c').get('TRHC__Config__c');
        //system.debug('===== configFields ====='+configFields);
        //String expanseFields = ObjectFieldsHelper.getAllFields('Expense__c').get('Expense__c');
        //system.debug('===== expanseFields ====='+expanseFields);

        //Set<String> states = new Set<String>();

        //Set<String> scratchSheetIds = new Set<String>();
        /*
        * @description : modified by medstaff2 added Account_Name__c to avoid exception in line number 259
        */ 
        //List<TRHC__Scratch_Sheet__c> scrList = [select id,name,State_pic__c,City__c, Account_Name__c From TRHC__Scratch_Sheet__c where id =: newScratchSheet.id ];
        //system.debug('======= scrList  ========'+scrList);
        /*for(TRHC__Scratch_Sheet__c scratchSheet : scrList ) {
            if(scratchSheet.State_pic__c!= null) {
                states.add('\''+scratchSheet.State_pic__c+'\'');*/
                //system.debug('======= states  ========'+states);
           /* }
            scratchSheetIds.add(scratchSheet.Id);*/
            //system.debug('======= scratchSheetIds  ========'+scratchSheetIds);
        //}

        /*if(states.size() == 0){
            return;
        }

        Map<String, Set<TRHC__Config__c>> configMap = new Map<String, Set<TRHC__Config__c>>();

        List<String> statesList = new List<String> (states);
        String query = 'SELECT ' + configFields+ ' FROM TRHC__Config__c WHERE TRHC__State__c IN ' + statesList ;
       */ //system.debug('======= query  ========'+query);
        //String configQuery = 'SELECT ID, Scratch_Sheet__c,Active__c, Expense_Name__c, Config__c FROM Expense__c WHERE Scratch_Sheet__c = \''+ newScratchSheet.id +'\'';
        //system.debug('======= configQuery  ========'+configQuery);
        /*if(configSettings != null) {
            String criteria = configSettings.AutoAddExpenseCriteria__c;
           */ //system.debug('======= criteria  ========'+criteria);
            //if(criteria != null) {
                //system.debug('======= criteria  ========'+criteria);
               /* List<String> criterias = criteria.split(',');
                for(String rule : criterias){
                    query += ' AND ' + rule +' ';
                  */  //system.debug('======= rule  ========'+rule);
               /* }
            }
        }

        List<TRHC__Config__c> configs = Database.query(query);*/
        //system.debug('======= configs  ========'+configs);

        //List<Expense__c> availableExpenses = Database.query(configQuery);
        //system.debug('======= availableExpenses  ========'+availableExpenses);

        /*if(configs != null && configs.size() > 0) {
        List<Config_To_ExpenseCO__c> configToExpense = [Select Config_Field_API_Name_Custom__c, Expanse_Field_API_Name__c From Config_To_ExpenseCO__c]; 
       */ //system.debug('======= configToExpense  ========'+configToExpense);
            /*for(TRHC__Config__c config : configs) {
                Set<TRHC__Config__c> configObjs = configMap.get(config.TRHC__State__c);
               */ //system.debug('======= configObjs  ========'+configObjs);
                /*if(configObjs == null) {
                    configObjs = new Set<TRHC__Config__c>();
                }
                configObjs.add(config);
                configMap.put(config.TRHC__State__c, configObjs);
               */ //system.debug('======= configMap  ========'+configMap);
           // }
/*
            Set<Expense__c> expenses = new Set<Expense__c>();
            for(TRHC__Scratch_Sheet__c scratchSheet : scrList) {
                if(scratchSheet.State_pic__c== null) { 
                continue;
                }*/
                //system.debug('========= scratchSheet.State_pic__c ==========='+scratchSheet.State_pic__c);
                /*
                http://gsa.targetrecruit.com/Geocode/getGsaDetails_new.php
                */
                /*Set<TRHC__Config__c> configIds = configMap.get(scratchSheet.State_pic__c);
                List<String> nameLst = new List<String> ();
                List<String> scrExpList = new List<String>();
                for(TRHC__Config__c configId : configIds) {
*/
                    //system.debug('======= configId ========='+configId);
                    /*boolean isExist = false;
                    boolean addCityExpense = false;

                    if(exptIdsList.contains(configId.id) && scratchSheet.City__c != configId.TRHC__City__c){
                        isExist = true;
                    }
                    else if(exptIdsList.contains(configId.id) ){
                        nameLst.add(configId.TRHC__Name__c);*/
                        //system.debug('====== configId ========'+configId);
                        /*addCityExpense = true;
                    }

                    for(Expense__c avaExp : availableExpenses) {
                        if(avaExp.Scratch_Sheet__c == scratchSheet.Id && avaExp.Config__c == configId.id) {
                            isExist = true;
                        }
                    }
                    if(isExist) {
                        continue;
                    }

                    Expense__c expense = new Expense__c();

                    for(Config_To_ExpenseCO__c configToExp : configToExpense){*/
                        //system.debug('===== configToExp ========'+configToExp);
                        //String expanseField = configToExp.Expanse_Field_API_Name__c;
                        //system.debug('===== expanseField ========'+expanseField);
                        //String configField = configToExp.Config_Field_API_Name_Custom__c;
                        //system.debug('===== configField ========'+configField);
/*
                        if(configFields.contains(configField)&& expanseFields.contains(expanseField)){
                            if(configField == 'TRHC__Value__c'){
                                expense.Value__c = Decimal.valueOf(configId.TRHC__Value__c);
*/
                                //system.debug('===== expense.Value__c ========'+expense.Value__c);
                                
                           /* }else{
                                expense.put(expanseField,configId.get(configField));
                                //system.debug('===== expense ========'+expense);
                            }
                        }
                    }
                    expense.Config__c = configId.id;*/
                    //system.debug('====== expense.Config__c ========'+expense.Config__c);
                    //expense.Scratch_Sheet__c  = scratchSheet.Id;
                    //system.debug('========exp.Grouping_Total__c======='+expense.Grouping_Total__c);
                    //expense.Grouping_Total__c = 'Other Expense';
                    //system.debug('====== expense.Scratch_Sheet__c ========'+expense.Scratch_Sheet__c);
                    //expenses.add(expense);
                    //scrExpList.add(expense.Expense_Name__c);
                    //system.debug('====== scrExpList ========'+scrExpList);
                //}

                /*set<String> setTempString = new set<String>();
                set<String> duplicateTempString = new set<String>();
                for(String s:scrExpList){*/
                    //system.debug('====== s ========'+s);
                    /*if(!setTempString.add(s)){
                        duplicateTempString .add(s);
                        //system.debug('====== duplicateTempString ========'+duplicateTempString);
                    }
                }
*/
                /*for(Expense__c expence:expenses){
                    for(String str:duplicateTempString){
                        if(str == expence.Expense_Name__c){
                            if(exptAccountList.contains(scratchSheet.Account_Name__c)){
                                if(exptIdsList.contains(expence.Config__c)){
                                    expenses.remove(expence);  
                                    //system.debug('====== expenses ========'+expenses);
                                }
                                else{
                                    expence.Value__c = 0.0;
                                    //system.debug('====== expence.Value__c  ========'+expence.Value__c );
                                }
                            }*/
                            /*else if(!exptIdsList.contains(expence.Config__c)){
                                expenses.remove(expence); 
                                //system.debug('====== expenses ========'+expenses);
                            }
                        }
                    }
                }

                for(Expense__c exp :expenses){
                    if(exp.Expense_Name__c != null && exp.Expense_Name__c.contains('Sales Tax') && exptAccountList.contains(scratchSheet.Account_Name__c)){
                        exp.Value__c = 0.0;
                        system.debug('========exp.Grouping_Total__c======='+exp.Grouping_Total__c);
                        exp.Grouping_Total__c = 'Other Expense';
                        //system.debug('====== exp.Value__c ====='+exp.Value__c);
                    }
                }*/
/*
                If(expenses.size()>0) {
                    insert new List<Expense__c>(expenses);
                    //system.debug('inside insert');
                }
            }
        }

        if(checkGSAService == false){
            update newScratchSheet;
        }

        List<Expense__c> expense = [select Expense_Name__c,Scratch_Sheet__r.OverTIme_GPH__c,Scratch_Sheet__r.Over_Time_Include_Bill__c,Scratch_Sheet__r.TRHC__Client_Fee__c,Occurence__c,Value__c,ProRated_Hours__c from Expense__c where Scratch_Sheet__c =: scrid and Expense_Name__c = 'VMS Fees'];
        if(expense.size() > 0){

            if(expense[0].Scratch_Sheet__r.TRHC__Client_Fee__c != null){
                expense[0].Value__c = expense[0].Scratch_Sheet__r.TRHC__Client_Fee__c;
                expense[0].Grouping_Total__c = 'Other Expense';
                //system.debug('========= expense[0].Value__c ======'+expense[0].Value__c);
                //(expense[0].TRHC__Scratch_Sheet__r.Over_Time_Include_Bill__c * expense[0].TRHC__Scratch_Sheet__r.TRHC__Client_Fee__c)/(100 * expense[0].TRHC__Occurence__c * expense[0].TRHC__ProRated_Hours__c);
                Update expense;
            }*/
       /*}
        //insertGsaSerValue();
    } 

    public void insertGsaSerValue(){
        String gsaURL = 'http://gsa.targetrecruit.com/Geocode/getGsaDetails_new.php';
        List<TRHC__Scratch_Sheet__c> scrList = [SELECT id, Name, TRHC__Start_Date__c, End_Date_New__c, State_Pic__c, TRHC__GSA_Lodging__c,TRHC__County__c,TRHC__GSA_M_I_Rate__c, TRHC__Tax_Rate__c From TRHC__Scratch_Sheet__c where TRHC__Start_Date__c !=  null and End_Date_New__c != null and id =: scrid];
        //system.debug('==== scrList ===='+scrList);
        //system.debug('===== inside for gsa service ======');*/
        /*List<TRHC__Scratch_Sheet__c> scrUpdateList = new List<TRHC__Scratch_Sheet__c>();
        for(TRHC__Scratch_Sheet__c scrObj : scrList) {
            if (scrObj.State_Pic__c != null){ 

                String startDateStr = scrObj.TRHC__Start_Date__c != null ? 
                scrObj.TRHC__Start_Date__c.year() + '-'+
                scrObj.TRHC__Start_Date__c.month() + '-'+
                scrObj.TRHC__Start_Date__c.day() : '' ;
                String endDate = scrObj.End_Date_New__c != null ?
                scrObj.End_Date_New__c.year() + '-' +
                scrObj.End_Date_New__c.month() + '-' +
                scrObj.End_Date_New__c.day() : ''; 
                String state = '';                  
                state = scrObj.State_pic__c;
                state = state.replace(' ','%20');
                String county = scrObj.TRHC__County__c != null ? scrObj.TRHC__County__c.replace(' ','%20') :'';
*//*
                HttpRequest request = new HttpRequest();
                String GSARquestURL = gsaURL +'?sdate=' + startDateStr +'&edate='+endDate+'&state='+state+'&county='+county;
                request.setEndpoint(GSARquestURL);
                //system.debug('========= GSARquestURL ==========='+GSARquestURL);
                request.setMethod('GET');
                request.setCompressed(false);
                request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
                //system.debug(LoggingLevel.ERROR, 'gsa request>>>' + request);
                HttpResponse response = new HttpResponse();

                if(!Test.isRunningTest() && Limits.getCallouts() < Limits.getLimitCallouts()){
                    response = new Http().send(request);
                }else{
                    response.setBody('[{"GSADestination":"Gulf Shores","State":"AL","GSACounty":"Baldwin","BeginDate":"2010-08-01","EndDate":"2010-09-30","Lodging":"111","MIE":null,"Total":"162","CombinedTaxRate":"8.51"}]');
                    response.setStatusCode(200);
                }*/

                //system.debug(LoggingLevel.ERROR, ':::::::::GSA response = ' + response);
                //system.debug(LoggingLevel.ERROR, ':::::::::response.getBody() = ' + response.getBody());
/*
                if (response.getStatusCode() == 200) {
                    String body = response.getBody();
                    String jsonBody = body.substring(1,body.length()-1);
                    //system.debug(LoggingLevel.ERROR, ':::::::::body' + jsonBody);
                    CreateScrShtAndCalcGPHcusObjExpCont.GSAResponse gsaResponse  = parseResponse(jsonBody);

                    //system.debug(LoggingLevel.ERROR, ':::::::::gsaResponse after parsing = ' + gsaResponse);

                    scrObj.TRHC__GSA_M_I_Rate__c = gsaResponse.MIE != null && gsaResponse.MIE != '' ? Decimal.Valueof(gsaResponse.MIE):0;
                    scrObj.TRHC__GSA_Lodging__c = gsaResponse.Lodging != null && gsaResponse.Lodging != '' ? Decimal.Valueof(gsaResponse.Lodging):0;
                    scrObj.TRHC__Tax_Rate__c = gsaResponse.CombinedTaxRate != null && gsaResponse.CombinedTaxRate != '' ? Decimal.ValueOf(gsaResponse.CombinedTaxRate):0;
                    scrUpdateList.add(scrObj);
                }
            }
        }*/
        /*if(!scrUpdateList.isEmpty()){
            update scrUpdateList;
        }
    }


    public Pagereference calcGPH(){   

        scratchSheet = [SELECT
                                Id,
                                OverTime_GPH__c,
                                isGPHCalculated__c,
                                With_OverTime_Gross_Profit__c,
                                TRHC__Assignment_length__c, 
                                TRHC__Hours_Per_Week__c, 
                                TRHC__Start_Date__c,
                                TRHC__End_Date__c,
                                TRHC__Client_Fee__c,
                                State_pic__c,
                                over_time__c,
                                TRHC__Bill_Rate__c
                                FROM
                                TRHC__Scratch_Sheet__c
                                WHERE
                                Id = : newScratchSheet.Id];*/

        /*if(scratchSheet.TRHC__Bill_Rate__c > 0){
        continue;
        } else {
        break;
        return null;
        }*/
        //system.debug('======== scratchSheet ========'+scratchSheet);
        /*string getLabel = System.Label.Editable_Expense;
        Set<string> editAccess = new  Set<string>();

        if(getLabel != null) {
            editAccess.addAll(getLabel.Split(','));
        }

        listExpense = [SELECT
                            Id, 
                            Expense_Name__c, 
                            Amount__c, 
                            Occurence__c, 
                            Grouping_Total__c, 
                            Value__c
                            FROM
                            Expense__c
                            WHERE
                            Scratch_Sheet__c =: scratchSheet.Id AND Expense_Name__c in: editAccess];

        //system.debug('======= listExpense ========='+listExpense);
        payBurdern = 0;
        for(Expense__c expense: listExpense) {
            if(expense.Expense_Name__c != 'Travel Reimbursement') {
                payBurdern += expense.Amount__c;*/
                /*if(expense.TRHC__Expense_Name__c == 'VMS Fees'){
                payBurdern += expense.TRHC__Value__c;
                }*/
            //}
            //system.debug('======== payBurdern ========'+payBurdern);
        //}

        /*payBurdern = Math.round(payBurdern);

        if(!scratchSheet.isGPHCalculated__c) {   
            //system.debug('======== scratchSheet.isGPHcalculated ========'+scratchSheet.isGPHCalculated__c);
            checkGSAService = (Boolean)customSetting.get('TRHC__Enable_GSA_Service_to_Update_M_I_Rate__c');
            decimal dollar = 0.00;
            if(scratchSheet.TRHC__Bill_Rate__c > 60){
                //system.debug('======== scratchSheet.TRHC__Bill_Rate__c ========'+scratchSheet.TRHC__Bill_Rate__c);
                dollar = 17.00;
                 if(checkGSAService == false){
                if(scratchSheet.TRHC__Client_Fee__c == null){
                dollar = 20.56;
                } else {
                dollar = 18.68;////////////18.68; ////////////////
                }
                } else {
                if(scratchSheet.TRHC__Client_Fee__c == null){
                dollar = 19.88;
                } else {
                dollar = 17.00; ///////////////
                }
                }
            }else {
                dollar = 15.00;*/
                /* //system.debug('======== scratchSheet.TRHC__Bill_Rate__c ========'+scratchSheet.TRHC__Bill_Rate__c);
                if(checkGSAService == true){
                if(scratchSheet.TRHC__Client_Fee__c == null){
                dollar = 16.88;
                } else {
                dollar = 15.00; ///////////////
                }
                } else {
                if(scratchSheet.TRHC__Client_Fee__c == null){
                dollar = 18.56;
                } else {
                dollar = 16.68; ///////////////
                }

                }*/
           /* }
            
            decimal totalGPHAdd = 0;
            decimal addToNonTaxable = 0;
            if(scratchSheet.over_time__c == null){
                scratchSheet.over_time__c = 0;
                //system.debug('======== scratchSheet.over_time__c ========'+scratchSheet.over_time__c);
            }*/

            /*decimal calculation = (dollar * (scratchSheet.TRHC__Assignment_length__c * (scratchSheet.TRHC__Hours_Per_Week__c + scratchSheet.over_time__c)));
            //system.debug('======== dollar ========'+dollar);
            //system.debug('======== scratchSheet.TRHC__Assignment_length__c ========'+scratchSheet.TRHC__Assignment_length__c);
            //system.debug('======== scratchSheet.TRHC__Hours_Per_Week__c ========'+scratchSheet.TRHC__Hours_Per_Week__c);
            //system.debug('======== calculation ========'+calculation);
            //system.debug('======== scratchSheet.over_time__c ========'+scratchSheet.over_time__c);
            //if(scratchSheet.OverTime_GPH__c != null){
            if(dollar > scratchSheet.OverTime_GPH__c) {
                //system.debug('======== scratchSheet.OverTime_GPH__c ========'+scratchSheet.OverTime_GPH__c);
                totalGPHAdd = (calculation - scratchSheet.With_OverTime_Gross_Profit__c);
                totalGPHAdd = totalGPHAdd.round() * -1;
                //if(listExpense != null && scratchSheet != null){
                calculateGPH(listExpense, scratchSheet, totalGPHAdd, payBurdern);
                //system.debug('========= listExpense If========'+listExpense);
                //system.debug('========= scratchSheet if========'+scratchSheet);
                //system.debug('========= totalGPHAdd  if========'+totalGPHAdd);
                //system.debug('========= payBurdern if ========'+payBurdern);
                //}

            } else if(dollar < scratchSheet.OverTime_GPH__c){
                //system.debug('======== scratchSheet.OverTime_GPH__c ========'+scratchSheet.OverTime_GPH__c);
                totalGPHAdd = (scratchSheet.With_OverTime_Gross_Profit__c - calculation);
                totalGPHAdd = totalGPHAdd.round();
                //if(listExpense != null && scratchSheet != null){
                calculateGPH(listExpense, scratchSheet, totalGPHAdd, payBurdern);       
                //system.debug('========= listExpense else========'+listExpense);
                //system.debug('========= scratchSheet else========'+scratchSheet);
                //system.debug('========= totalGPHAdd  else========'+totalGPHAdd);
                //system.debug('========= payBurdern else========'+payBurdern);
                //}
            }

        }

        return new PageReference('/'+scratchSheet.Id); 
        //return candidateId != null ? new  Pagereference('/' + scratchSheet.id + '/e?retURL=%2F' + scratchSheet.id) : new  Pagereference('/' + scratchSheet.id);
    }

    public void calculateGPH(List<Expense__c> listExpenses, TRHC__Scratch_Sheet__c sheet, decimal totalGPH, decimal totalExpense) {
        //system.debug('========= listExpenses========'+listExpenses);
        //system.debug('========= sheet========'+sheet);
        //system.debug('========= totalGPH  ========'+totalGPH);
        //system.debug('========= totalExpense ========'+totalExpense);
        List<Expense__c> listUpdate = new  List<Expense__c>();
        for(Expense__c singleExpense: listExpenses) {*/
            //system.debug('===== singleExpense ========'+singleExpense);
            /*if(singleExpense.Expense_Name__c != 'Travel Reimbursement') {   
                decimal expenseValue = (singleExpense.Amount__c + (((singleExpense.Amount__c / totalExpense) * 100) * totalGPH) / 100) / singleExpense.Occurence__c;
                //system.debug('====== expenseValue ======='+expenseValue);
                singleExpense.Value__c = Math.round(expenseValue);
                //system.debug('====== expenseValue ======='+expenseValue);
                listUpdate.add(singleExpense);
            }*/

            /*  if(singleExpense.TRHC__Value__c < 0.00) {   
            //system.debug('======= if inside value is less then 0 =======');
            singleExpense.TRHC__Value__c = 0.00;
            //listUpdate.add(singleExpense);
            }
            listUpdate.add(singleExpense);*/
        /*}

        if(!listUpdate.isEmpty()) {
            Update listUpdate;
            sheet.isGPHCalculated__c = true;
            Update sheet;
        }
    }

    public static CreateScrShtAndCalcGPHcusObjExpCont.GSAResponse parseResponse(String response) {

        CreateScrShtAndCalcGPHcusObjExpCont.GSAResponse gsaResponse = new CreateScrShtAndCalcGPHcusObjExpCont.GSAResponse();

        if (String.isNotBlank(response)) {
            UpdateGSAServiceBatch.GSAResponse respObj = 
            (UpdateGSAServiceBatch.GSAResponse)JSON.deserialize(
            response, UpdateGSAServiceBatch.GSAResponse.class);
            //system.debug('respObj after deserialize'+respObj);
            return setGSADetails(gsaResponse, 
            respObj.MIE, respObj.Lodging,respObj.CombinedTaxRate);
        }
        return setGSADetails(gsaResponse, null, null,null);
    }

    public static CreateScrShtAndCalcGPHcusObjExpCont.GSAResponse setGSADetails(
    CreateScrShtAndCalcGPHcusObjExpCont.GSAResponse gsaResponse, String MIE, String Lodging,String taxRate) {
        gsaResponse.MIE = MIE;
        gsaResponse.Lodging = Lodging;
        gsaResponse.CombinedTaxRate = taxRate;
        //system.debug(LoggingLevel.ERROR, ':::::::::gsaResponse  = ' + gsaResponse);
        return gsaResponse;
    }   

    public class GSAResponse{
        public String GSADestination {get;set;}
        public String State {get;set;}
        public String GSACounty {get;set;}
        public String BeginDate {get;set;}
        public String EndDate {get;set;}
        public String Lodging {get;set;}
        public String MIE {get;set;}
        public String Total {get;set;}
        public String CombinedTaxRate {get;set;}
    }*/
}