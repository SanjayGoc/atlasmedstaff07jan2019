/*

Annual_Review_Date__c
Bwid__c
CA_Grantaccess__c

DeviceType :
1- Android
2- iOS

Response Status Code
0- Error
1- Sucess
2- UnAuthorized User

Response

{
    "username": "earthpin@yahoo.com",
    "User_Id": "0034100001OgPnSAAV",
    "tempAddress": {
        "zipcode": null,
        "state": null,
        "city": null,
        "address2": null,
        "address1": null
    },
    "success": 1,
    "securityToken": "fb636e8dddbbdb149de6b0be2ce5cd31",
    "perInfo": {
        "shiftPrefe": [
            "7A - 3P",
            "7A - 7P",
            "11A - 7P"
        ],
        "seconSpeci": [
            "OR"
        ],
        "primarySpeciality": "OR",
        "phone": "(518) 929-5366",
        "lastName": "Allison",
        "firstName": "Teresa",
        "email": "earthtin@yahoo.com",
        "desipline": "Teresa Allison"
    },
    "perAddress": {
        "zipcode": "95624",
        "state": "California",
        "city": "Elk Grove",
        "address2": null,
        "address1": "9509 Grasmeer Way"
    },
    "password": "jakesgypsys",
    "message": "Login Succesfully",
    "FullName": "Teresa Allison",
    "deviceType": "Anroid",
    "deviceId": "123456789"
}

*/



@RestResource(urlMapping='/mobile/communitylogin/*')
global class CommunityLoginRESTOperation {
    @HttpGet
    global static ResponseHandler CommunityLoginRESTOperation(){
        String username = RestContext.request.params.get('username');
        String password = RestContext.request.params.get('password');
        string deviceType = RestContext.request.params.get('deviceType');
        String deviceId = RestContext.request.params.get('deviceId');
        ResponseHandler res = new ResponseHandler();
        personalInformation perInfoObj = new personalInformation();
        permenentAddress perAddObj = new permenentAddress(); 
        temporaryAddress tempAddObj = new temporaryAddress();
        
        if(username != null && username != '' && password != null && password != ''){
            List<Contact> userList = [Select Id,
                                             Name,
                                             firstName,
                                             lastName,
                                             Email,
                                             apiToken__c,
                                             Phone,
                                             Discipline1__c,
                                             Primary_Speciality1__c,
                                             expireTokenDate__c,
                                             Secondary_Speciality__c,
                                             TRHC__Shift_Preference__c,
                                             Permanant_Address__c,
                                             TRHC__Permanent_Address_2__c, 
                                             TRHC__Permanent_City__c,
                                             State_pic__c,
                                             TRHC__Permanent_Zip_Postal_Code__c,
                                             Temporary_Address_1__c,
                                             Temporary_Address_2__c,
                                             TRHC__Temporary_City__c,
                                             temporary_state__c,
                                             TRHC__Temporary_Zip_Postal_Code__c
                                    From Contact 
                                    Where TRHC__UserName__c =: username 
                                    And TRHC__Password__c =: password];

            Blob blobKey = crypto.generateAesKey(128);
            String key = EncodingUtil.convertToHex(blobKey);

            if(!userList.isempty()) {
                
                DateTime expireTokenDate =  DateTime.Now();
                expireTokenDate = expireTokenDate.addDays(15);
                userList[0].expireTokenDate__c = expireTokenDate;
                userList[0].apiToken__c = key;
                if(deviceId != null && deviceId != '')
                    userList[0].DeviceId__c = deviceId;
                update userList[0];

                res.username = username;
                res.FullName = userList[0].Name;
                res.password = password;
                res.User_Id = userList[0].Id ;
                res.success = 1;
                res.message = new string[]{'Login Succesfully'};
                res.User_Id = userList[0].Id ;
                res.deviceId = deviceId;
                res.securityToken = key;
                
                if(deviceType == '1'){
                    res.deviceType = 'Anroid';
                }
                if(deviceType == '2'){
                    res.deviceType = 'iOS';
                }


                perInfoObj.firstName = userList[0].firstName;
                perInfoObj.lastName = userList[0].lastName;
                perInfoObj.email = userList[0].Email;
                perInfoObj.phone = userList[0].Phone;
                perInfoObj.desipline = userList[0].Name;
                perInfoObj.primarySpeciality = userList[0].Primary_Speciality1__c;
                
                if(userList[0].Secondary_Speciality__c != null){
                    perInfoObj.seconSpeci = userList[0].Secondary_Speciality__c.split(';');
                } else {
                    perInfoObj.seconSpeci = new List<String>();
                }

                if(userList[0].TRHC__Shift_Preference__c != null){
                    perInfoObj.shiftPrefe = userList[0].TRHC__Shift_Preference__c.split(';');
                } else {
                    perInfoObj.shiftPrefe = new List<String>();
                }

                perAddObj.address1 = userList[0].Permanant_Address__c;
                perAddObj.address2 = userList[0].TRHC__Permanent_Address_2__c;
                perAddObj.city = userList[0].TRHC__Permanent_City__c;
                perAddObj.state = userList[0].State_pic__c;
                perAddObj.zipcode = userList[0].TRHC__Permanent_Zip_Postal_Code__c;

                tempAddObj.address1 = userList[0].Temporary_Address_1__c;
                tempAddObj.address2 = userList[0].Temporary_Address_2__c;
                tempAddObj.city = userList[0].TRHC__Temporary_City__c;
                tempAddObj.state = userList[0].temporary_state__c;
                tempAddObj.zipcode = userList[0].TRHC__Temporary_Zip_Postal_Code__c;
                
                res.perInfo = perInfoObj;
                res.perAddress = perAddObj;
                res.tempAddress = tempAddObj;
                return res;   
            }
        }
        res.username = username;
        res.password = password;
        res.success = 0;
        res.message = new string[]{'UserName or Password incorrect.'};
        res.deviceId = deviceId;
        return res;
    } 
    
    global class ResponseHandler {
        global String username;
        global String FullName;
        global String password;
        global integer success;
        global string[] message;
        global String User_Id;
        global string deviceType;
        global String deviceId;
        global string securityToken;
        global personalInformation perInfo;
        global permenentAddress perAddress;
        global temporaryAddress tempAddress;
        global ResponseHandler (){}
    }

    global class personalInformation {
        global String firstName;
        global String lastName;
        global String email;
        global String phone;
        global String desipline;
        global String primarySpeciality;
        global List<String> seconSpeci;
        global List<string> shiftPrefe;
        global personalInformation (){
        }
    }

    global class permenentAddress {
        global String address1;
        global String address2;
        global String city;
        global String state;
        global String zipcode;
        global permenentAddress (){
        }
    }

    global class temporaryAddress {
        global String address1;
        global String address2;
        global String city;
        global String state;
        global String zipcode;
        global temporaryAddress (){
        }
    }
}