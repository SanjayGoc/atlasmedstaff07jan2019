global class ScheduleSendRelevantJobBatch implements schedulable {
    global void execute(SchedulableContext SC) {
        SendRelevantJobBatchNew jobBatch = NEW SendRelevantJobBatchNew ();
        if(!test.isRunningTest()) {
        database.executeBatch(jobBatch, 1);
           
        }
    }

}