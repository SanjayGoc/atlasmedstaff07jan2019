public with sharing class UpdateSkillToGetProphecyHandler {
    
    public static void updateModifiedID (List<TRHC__Skill_Answers__c> skilllist) {
        set<Id> contactIds = new set<Id>();
        
        for(TRHC__Skill_Answers__c skill : skilllist) {
            contactIds.add(skill.TRHC__Contact__c);
        }
        
        Map<Id,Contact> contactsOwnerMap = new Map<Id,Contact>([select Id,OwnerId from Contact where Id in : contactIds]);
        for(TRHC__Skill_Answers__c skill : skilllist) {
            skill.LastModifiedById = contactsOwnerMap.get(skill.TRHC__Contact__c).OwnerId;
        }
    }
}