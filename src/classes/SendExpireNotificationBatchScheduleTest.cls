/*
========================================================================
Description: Unit Class for SendExpireNotificationBatchSchedule class 
========================================================================
*/

@istest
public class SendExpireNotificationBatchScheduleTest {
    static testmethod void SendExpNotifyBtcSchTest() {
        Test.StartTest();
        SendExpireNotificationBatchSchedule schTest = new SendExpireNotificationBatchSchedule();      
        String sch = '0 0 23 * * ?';
        system.schedule('Send Email', sch, schTest);
        Test.stopTest();
    }
}