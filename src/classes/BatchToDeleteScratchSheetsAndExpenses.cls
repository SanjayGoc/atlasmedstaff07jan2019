public class BatchToDeleteScratchSheetsAndExpenses implements Database.Batchable<sObject> {
    public List<TRHC__scratch_Sheet__c> start(Database.BatchableContext ctx){
        Date tDay = Date.today();
        Integer dayLimit = Integer.valueOf(System.label.DaysLimitForScratchSheet);
        Date deadLinedate = Date.today() - dayLimit;
        Id scrRecordTypeId = [Select id From RecordType where Name = 'ScratchSheet' Limit 1].id;
        List<TRHC__scratch_Sheet__c> scrLIst = [
                Select id, Name From TRHC__Scratch_Sheet__c
                    Where LastModifiedDate < :deadLinedate 
                        And createdDate< :deadLinedate 
                        And TRHC__Status__c !='Submitted' And RecordTypeId = :scrRecordTypeId];
        return  scrList;
    }
    
    public void execute(Database.BatchableContext ctx, List<TRHC__Scratch_Sheet__c> scrList){
        delete scrList;
    }
    
    public void finish(Database.BatchableContext ctx){
        
    }
}