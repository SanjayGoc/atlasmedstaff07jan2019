public class GenWorkOrderForJobAppController {
    public Id ContactId;
    public Boolean selectedJob {get; set;}
    public List<GenWorkOrderForJobAppContWrapper> wrapperList {get;set;}

    public GenWorkOrderForJobAppController(ApexPages.StandardController controller) {
        ContactId = ApexPages.CurrentPage().getparameters().get('Id');
        selectedJob = false;
    }

    public List<GenWorkOrderForJobAppContWrapper> getlatestPlacement() {
        if(wrapperList == null) {
            wrapperList = new List<GenWorkOrderForJobAppContWrapper>();
            for(TRHC__Job_Applicant__c jobApp: [SELECT Id, Name, TRHC__Account_Job__c ,TRHC__Job__r.Name,TRHC__Account_Job__r.Name
                        FROM TRHC__Job_Applicant__c 
                        WHERE TRHC__Contact_Candidate__c =: ContactId ORDER BY LastModifiedDate DESC]) {
    
                wrapperList.add(new GenWorkOrderForJobAppContWrapper(jobApp));
            }
        }
        return wrapperList;
    }

    public PageReference jobApplicantSelected() {

        List<TRHC__Job_Applicant__c> selectedContacts = new List<TRHC__Job_Applicant__c>();
        for(GenWorkOrderForJobAppContWrapper workOrderObj: getlatestPlacement()) {
            if(workOrderObj.selected == true) {
                selectedContacts.add(workOrderObj.jobAppl);
            }
        }

        List<String> jobAppId = new List<String>();

        for(TRHC__Job_Applicant__c jobAppObj: selectedContacts) {
            jobAppId.add(jobAppObj.id);
        }

        List<TRHC__Job_Applicant__c> listjobapp = [SELECT Id, Name, TRHC__Account_Job__c ,TRHC__Job__r.Name,TRHC__Account_Job__r.Name
                        FROM TRHC__Job_Applicant__c 
                        WHERE id  in : jobAppId];
        String sessionId = UserInfo.getSessionId();
        PageReference pageRef;
        if(listjobapp.size() > 0) {
            pageRef = new PageReference('/apex/loop__looplus?eid=' + listjobapp[0].Id + '&accountId=' + listjobapp[0].TRHC__Account_Job__c + '&sessionId=' + sessionId + '&hidecontact=true' + '&ddpIds=a2y0q000000Kfkw,a2y0q000000Kg6v,a2y0q000000Kfkm&autorun=true&retURL=/' + listjobapp[0].Id);
            return pageRef;
        } else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,System.Label.No_Placement_Message));
        }
        

        wrapperList = null; 
        return pageRef;
    }


    public PageReference returnContact() {
        PageReference pageRef = new PageReference('/' + ContactId);
        return pageRef;
    }

    public class GenWorkOrderForJobAppContWrapper {
        public TRHC__Job_Applicant__c jobAppl {get; set;}
        public Boolean selected {get; set;}
        public GenWorkOrderForJobAppContWrapper(TRHC__Job_Applicant__c jobAppl) {
            this.jobAppl = jobAppl;
            selected = false;
        }
    }
}