public class DeleteSessionControllerNew implements Database.Batchable<contact>{ 
    
    public List<contact> start(Database.BatchableContext ctx){
        List<Contact> contactListAct = [SELECT Id, Name, Email FROM CONTACT Where RecordTypeID = '01241000000y2YC' AND TRHC__UserName__c != NULL AND TRHC__Password__c != NULL];
        return contactListAct;
    }
    
    public void execute(Database.BatchableContext ctx, List<contact> contacts){
        List<string> conIds = new List<String>();
        for(Contact con:contacts){
            conIds.add(con.id);
        }
        List<TRHC__Session__c> eSessionDelList = new List<TRHC__Session__c>();
        Map<String, List<TRHC__Session__c>> parentMap = new Map<String, List<TRHC__Session__c>> ();
        
        for(TRHC__Session__c session :[SELECT Id, OwnerId, IsDeleted, Name, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById,
                                       SystemModstamp, TRHC__CurrentDateTime__c, TRHC__Data_Source__c, TRHC__IP_Address__c, TRHC__Is_Valid__c,
                                       TRHC__Migration_External_ID__c, TRHC__SessionId__c, TRHC__Session_For__c, TRHC__Session_Time_Out__c, 
                                       TRHC__User_Agent__c FROM TRHC__Session__c WHERE TRHC__Session_For__c IN :conIds order by createddate desc]){
                                           
                                        List<TRHC__Session__c> sessions = new List<TRHC__Session__c>();
                                        if(parentMap.containsKey(session.TRHC__Session_For__c)){
                                            sessions = parentMap.get(session.TRHC__Session_For__c);
                                            sessions.add(session);
                                            parentMap.put(session.TRHC__Session_For__c, sessions);
                                            System.debug('sessions@@@'+sessions);
                                        }
                                        else{
                                            sessions.add(session);
                                            parentMap.put(session.TRHC__Session_For__c, sessions);
                                            System.debug('sessions###@@@'+sessions);
                                        }
                                        
                                    }
        
        for(String str : parentMap.keySet()){
            System.debug('parentMap.get(str).size()'+parentMap.get(str).size());
            if(parentMap.get(str).size()>5)
                for(integer i = parentMap.get(str).size()-1; i>4; i--){
                    eSessionDelList.add(parentMap.get(str)[i]);
                }
        }
        delete eSessionDelList;
    }
    
    public void finish(Database.BatchableContext ctx){
    }
    
}