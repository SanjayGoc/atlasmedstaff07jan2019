public class PopulateRecruiterScratchSheetHandler {
    public void PopolateRecruiter(List<TRHC__Job__c > joblist) {
        TRHC__Scratch_Sheet__c scratchsheetName;
        List<TRHC__Scratch_Sheet__c> scratchSheetList = new List<TRHC__Scratch_Sheet__c>();
        
        for(TRHC__Job__c job : joblist){
            if((job.TRHC__Recruiter__c != null && job.TRHC__Scratch_Sheet__c != null) || ( job.TRHC__Recruiter__c != null && job.TRHC__Scratch_Sheet__c != null)){
                scratchsheetName = new TRHC__Scratch_Sheet__c();
                scratchsheetName.id = job.TRHC__Scratch_Sheet__c;
                scratchsheetName.Recruiter__c = job.TRHC__Recruiter__c;
                scratchSheetList.add(scratchsheetName);
            }    
        }
        if(!scratchSheetList.isEmpty()){
            update scratchSheetList;
        }
    }

    public void getDistanceFromAccount(set<Id> joblist) {
        List<TRHC__Job__c> joblistUpdate = new List<TRHC__Job__c>();
        For(TRHC__Job__c job : [SELECT TRHC__Account_Lookup__r.BillingLatitude,TRHC__Account_Lookup__r.BillingLongitude,TRHC__Lat__c,TRHC__Lon__c FROM TRHC__Job__c WHERE ID In : joblist]) {
            if(job.TRHC__Account_Lookup__c != null) {
                Double latHospital = job.TRHC__Account_Lookup__r.BillingLatitude;
                system.debug('===== latHospital  ======='+latHospital );
                system.debug('===== job.TRHC__Account_Lookup__r.BillingLatitude ======='+job.TRHC__Account_Lookup__r.BillingLatitude);
                Double lnghospital = job.TRHC__Account_Lookup__r.BillingLongitude;
                            system.debug('===== lnghospital ======='+lnghospital);
                Location locHospital = Location.newInstance(latHospital,lnghospital);
                            system.debug('===== locHospital  ======='+locHospital );
                
                //Double dist = Location.getDistance(locHospital, locHousing, 'mi');
                
                Double jobLat = job.TRHC__Lat__c;
                            system.debug('===== jobLat  ======='+jobLat );
                Double jobLong = job.TRHC__Lon__c;
                            system.debug('===== jobLong ======='+jobLong);
                Location jobLocation = Location.newInstance(jobLat, jobLong); 
                            system.debug('===== jobLocation ======='+jobLocation);
                
                Double TotalDistance = Location.getDistance(locHospital, jobLocation, 'mi');
                
                system.debug('==== TotalDistance ==== '+TotalDistance);
                job.Distance__c = TotalDistance;
            }
            joblistUpdate.add(job);
            
        }
        
        
        if(!joblistUpdate.isEmpty()){
            update joblistUpdate;
        }
       
    }
}