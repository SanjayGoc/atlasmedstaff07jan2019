/**
 * @author Chaitanya bollamreddi
 * @date 06 April 2018
 * @description AutoDeleteInappropriateJobsSheduler class is Sheduler for AutoDeleteInappropriateJobsBatch
 */
public class SendReuestToUpdateInformationSheduler implements Schedulable {
    /**
     * @author Chaitanya bollamreddi
     * @date 06 April 2018
     * @name execute
     * @parm System.SchedulableContext
     * @return type void
     * @description execute method to shedule the related batch
     */
    public void execute(System.SchedulableContext ctx){
        SendReuestToUpdateInformationBatch sendRequestBatch = new SendReuestToUpdateInformationBatch();
        Database.executeBatch(sendRequestBatch,50);
    }

}