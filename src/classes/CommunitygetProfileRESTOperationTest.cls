@isTest
Public class CommunitygetProfileRESTOperationTest{
    public static testmethod void profiletest(){
        TRHC__Config_Settings__c configSettings = new TRHC__Config_Settings__c();
        configSettings.name='Default';
        configSettings.Enable_AutoMateJobRates__c = true;
        configSettings.Trigger_Enable_ScratchSheetAutoAddExp__c = false;
        configSettings.AutoAddExpenseCriteria__c='TRHC__Margin_Type__c=\'Expense\'';
        configSettings.Except_Hospitals__c = 'Atlas Development';
        configSettings.Trigger_Enable_Expected_End_Date_Changer__c = false;
        configSettings.Trigger_Enable_Applicant_From_SCR_Sheet__c = true;
        configSettings.Update_Pay_Burden__c = false;
        configSettings.Enable_RefferalTrigger__c = true;
        insert configSettings;

        List<Account> accountList = TestUtilityClass.getTestMultipleAccount();
        contact conobj = new contact();
        conobj.lastName = 'sharma';
        conobj.apiToken__c = '13214564654564877979879';
        insert conobj;


        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/mobile/getProfile';
        req.addParameter('apiToken', conobj.apiToken__c);
        req.addParameter('deviceType', '1');
        req.addParameter('deviceId', 'fUbAO6qWlx8:APA91bHZayPMbzGeaam9OnVdume8iQbB3Q92JdyvvtgKsKiG1PJK52F5TH3AqfILp-en551n91xErpjiQp4LFY--5CCsJA0CSxGi-gHmoD8IilHqPsTy-fc-gV3uPv61HCaclS7CSaRe');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;

        CommunitygetProfileRESTOperation.ResponseHandler wrapperobj = CommunitygetProfileRESTOperation.CommunitygetProfileRESTOperation();


    }
}