/**
* @description class fills tiny url in job object.
*/
public with sharing class GenerateTinyUrlController {
    TRHC__Job__c job = new TRHC__Job__c();
    
    public GenerateTinyUrlController(ApexPages.StandardController record) {
        this.job = (TRHC__Job__c)record.getRecord();
    }
    
    /**
* @name init
* @description Intiates tiny url creation in jobs.
* @return void
*/
    public void init() {
        TRHC__Job__c checkJob = [
                SELECT JobPublishTinyURLCustom__c
                FROM TRHC__Job__c
                WHERE Id = :job.Id
                LIMIT 1];
        
        if (checkJob != null && checkJob.JobPublishTinyURLCustom__c== null) {
            TinyUrl.fillTinyUrl(new List<id> { checkJob.Id });
        }
    }
}