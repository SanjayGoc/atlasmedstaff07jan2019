@isTest
public class TestUtilityClass {
    public static List<Account> getTestMultipleAccount(){
        List<Account> accList = new List<Account>();
        
        for(integer i=0;i<=10;i++){
            account acc = new account();
            acc.Name = 'Test'+i+'Account';
            accList.add(acc);
        }
        insert accList;
        return accList;
    }
    public static List<Contact> getTestMultipleContact(Account acc){
        List<Contact> conList = new List<Contact>();
        
        for(integer i=0;i<=10;i++){
            Contact con = new Contact();
            con.FirstName = 'Test';
            con.LastName =  'Contact '+ i;
            con.AccountId = acc.Id;
           /* con.MailingStreet = 'Test Street';
            con.MailingCity = 'Test City';
            con.MailingState = 'ON';
            con.MailingPostalCode = '123';
            con.MailingCountry = 'CA';*/
           // con.Phone = '1232434445';
            con.Email = 'Test'+ i + '@gmail.com';
            con.TRHC__Referral_Source__c = 'contact referral'+i;
            con.TRHC__Password__c = '12345678';
            con.TRHC__UserName__c = 'Test'+ i + '@gmail.com';
            conList.add(con);
        }
        insert conList;
        return conList;
    }

   
}