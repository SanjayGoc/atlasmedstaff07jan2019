@isTest
public class PlacementStatusByStatesTest {
    
    public static testmethod void test1() {
    
        TRHC__Config_Settings__c config = new TRHC__Config_Settings__c(Name = 'Default');
        config.Enable_RefferalTrigger__c = false;
        insert config;
        
        Profile p = [SELECT Id FROM Profile WHERE Name =: 'System Administrator'];

        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com');

        List<Account> accountList = TestUtilityClass.getTestMultipleAccount();
        List<Contact> ContactList = TestUtilityClass.getTestMultipleContact(accountList[0]);
        
        TRHC__Placement__c placement = new TRHC__Placement__c();
        placement.TRHC__Recruiter__c = u.ID;
        placement.TRHC__Hiring_Manager__c = ContactList[2].Id;
        placement.TRHC__Employer__c = accountList[1].Id;
        //  placement.TRHC__Account_Contract__c = contractList[1].Id;
        placement.TRHC__Contact_Candidate__c = ContactList[1].Id;
        placement.TRHC__Start_Date__c = System.Today().addMonths(-3);
        placement.TRHC__End_Date__c = System.today();
        placement.CreatedDate = System.today() -1;
        placement.VMS__c = 'AMN';
        insert placement;
        
        PlacementStatusByStates placementbystate = new PlacementStatusByStates();
        placementbystate.getcountryStateKey();
    }
}