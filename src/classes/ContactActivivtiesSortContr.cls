public with sharing class ContactActivivtiesSortContr {
    Contact contact;
    Id pid;
    String sortOption { get;set; }
    String sortOrder { get;set; }
    boolean b = false;
    public void sortBySubject() {
        this.sortOption = 'Subject';
        if(this.sortOrder == 'desc'){
            this.sortOrder = 'Asc';
        }else{
            this.sortOrder = 'desc';   
        }
        this.getsortActivivties();
        
    }
    
    public void sortByActivityDate() {
        this.sortOption = 'ActivityDate';
        if(this.sortOrder == 'desc'){
            this.sortOrder = 'Asc';
        }else{
            this.sortOrder = 'desc';   
        }
        this.getsortActivivties();
    }
    
    public void sortByAssignedTo() {
        this.sortOption = 'OwnerId';
        if(this.sortOrder == 'desc'){
            this.sortOrder = 'Asc';
        }else{
            this.sortOrder = 'desc';   
        }
        this.getsortActivivties();
    }
    
    public void sortByRelatedTo() {
        this.sortOption = 'WhoId';
        if(this.sortOrder == 'desc'){
            this.sortOrder = 'Asc';
        }else{
            this.sortOrder = 'desc';   
        }
        this.getsortActivivties();
    }
    
    public void sortByLastModfiedDate() {
        this.sortOption = 'LastModifiedDate';
        if(this.sortOrder == 'desc'){
            this.sortOrder = 'Asc';
        }else{
            this.sortOrder = 'desc';   
        }
        this.getsortActivivties();
    }
    
    public List<OpenActivity> activivties { get;set; }
    public ContactActivivtiesSortContr(ApexPages.StandardController controller){
        contact = (Contact)controller.getRecord();
        system.debug('test');
        pid = contact.id;
        sortOption = 'ActivityDate';
        sortOrder = 'Desc';
    }
    
    public PageReference getsortActivivties(){
        if(sortOption == Null){
            sortOption = 'ActivityDate';
        }
        if(sortOrder == Null){
            sortOrder  = 'Desc';  
        }
        String cQuery = 
            'SELECT id, (SELECT ActivitySubtype, ActivityDate, ActivityType, LastModifiedDate, '+
            'AlternateDetailId, CallDisposition, Description, EndDateTime, IsAllDayEvent, Subject, isTask, Status, Priority, WhatId, WhoId, OwnerId  '+
            ' FROM OpenActivities'+
            ' ORDER BY '+sortOption+ '  '+sortOrder +')'+
            ' FROM Contact '+
            ' WHERE ID = '+'\''+pid+'\''+'  LIMIT 1';
        System.debug('>>>>>query'+cQuery);
        contact contact = database.query(cQuery);
        system.debug('>>>>contact'+contact);
        activivties = contact.OpenActivities;
        system.debug('activivties@@'+activivties);
        return null;
    }
    
}