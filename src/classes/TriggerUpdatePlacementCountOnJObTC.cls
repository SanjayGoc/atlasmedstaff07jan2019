@isTest
public class TriggerUpdatePlacementCountOnJObTC {
    @isTest
    public static void trTest(){
        test.startTest();
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Accounting'].Id,
            LastName = 'last',
            Email = 'userTest1@atlasmedstaff.com',
            Username = 'userTest1@atlasmedstaff.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
        );
        
        insert u;
         TRHC__Config__c configObj1 = new TRHC__Config__c();
        configObj1.TRHC__Name__c = 'check';
        configObj1.TRHC__Margin_Type__c = 'Expense';
        configObj1.TRHC__Frequency__c = 'Weekly';
        configObj1.TRHC__State__c =   'California';
        configObj1.TRHC__Include_in_payroll__c= True;
        configObj1.TRHC__Pay_Period_to_be_paid__c='First Check';
        configObj1.TRHC__Active__c = True;
        configObj1.TRHC__Value__c = '350.0';
        configObj1.TRHC__Grouping_Total__c ='Pay Burden';
        configObj1.TRHC__City__c = 'test';
        
        configObj1.RecordTypeId =[Select Id From RecordType Where SobjectType = 'TRHC__Config__c' and Name = 'Margin calculation Line Item'].Id;
        
        insert configObj1;
        TRHC__Config_Settings__c config = new TRHC__Config_Settings__c(Name = 'Default');
        config.Enable_Update_Recruiter__c = False;
        config.Enable_NotifyRecruitersOnPlacementCreati__c = True;
        config.Trigger_Enable_ScratchSheetAutoAddExp__c = true;
        config.Except_Hospitals__c = 'Atlas Development';
        config.Assignment_Work_Order_Approve_Users__c = u.id;
        config.ExceptConfigIds__c = configObj1.id;
        insert config;  
        
        Account account = new Account();
        account.Name = 'SwTest';
        insert account;
        
        Contact candidate_Placemnet = new Contact();
        candidate_Placemnet.RecordTypeId = [SELECT Id FROM RECORDTYPE WHERE NAME = 'Candidate' AND SobjectType ='Contact' Limit 1].id;
        candidate_Placemnet.FirstName = 'TestCandidate';
        candidate_Placemnet.LastName = 'PlacementController';
        candidate_Placemnet.Compliance_Coordinater__c = u.id;
        candidate_Placemnet.TRHC__Account_Manager__c = u.id;
        candidate_Placemnet.OwnerId = u.id;
        candidate_Placemnet.AccountId = account.id;
        candidate_Placemnet.TRHC__Recruiter__c = u.id;
        insert candidate_Placemnet;
        
        Contact candidate_Placemnet1 = new Contact();
        candidate_Placemnet1.RecordTypeId = [SELECT Id FROM RECORDTYPE WHERE NAME = 'Candidate' AND SobjectType ='Contact' Limit 1].id;
        candidate_Placemnet1.FirstName = 'TestCandidateDup';
        candidate_Placemnet1.LastName = 'PlacementControllerDup';
        candidate_Placemnet1.Compliance_Coordinater__c = u.id;
        candidate_Placemnet1.TRHC__Account_Manager__c = u.id;
        candidate_Placemnet1.TRHC__Recruiter__c = u.id;
        candidate_Placemnet1.OwnerId = u.id;
        candidate_Placemnet1.AccountId = account.id;
        insert candidate_Placemnet1;
        
        TRHC__job__c job = new TRHC__job__c();
        job.TRHC__Job_Title__c = 'testJob';
        job.TRHC__Account_LookUp__c = account.id;
        job.TRHC__Regular_Bill_Rate__c = 1200;
        job.RecordTypeId =  Schema.SObjectType.TRHC__Job__c.getRecordTypeInfosByName().get('Job').getRecordTypeId();
        insert job;
        
        TRHC__Job_Applicant__c jobApp1 = new TRHC__Job_Applicant__c();
        jobApp1.TRHC__Contact_Candidate__c = candidate_Placemnet.id;
        jobApp1.TRHC__Job__c = job.id;
        jobApp1.TRHC__Recruiter__c = u.id;
        insert jobApp1;
        
        TRHC__Job_Applicant__c jobApp2 = new TRHC__Job_Applicant__c();
        jobApp2.TRHC__Contact_Candidate__c = candidate_Placemnet1.id;
        jobApp2.TRHC__Job__c = job.id;
        jobApp2.TRHC__Stage__c = 'Offered';
        jobApp2.TRHC__Recruiter__c = u.id;
        insert jobApp2;
        
        TRHC__Scratch_Sheet__c scratchSheet = new TRHC__Scratch_Sheet__c();
        scratchSheet.Name = 'TestScratchSheet_test_Placement';
        scratchSheet.TRHC__Bill_Rate__c = 3000;
        scratchSheet.TRHC__Overtime_Rate__c = 80;
        scratchsheet.TRHC__Total_Overtime_Hours__c = 16;
        scratchsheet.TRHC__Pay_Rate__c = 50;
        scratchsheet.TRHC__Overtime_Pay_Rate__c = 80;
        scratchSheet.TRHC__Assignment_length__c  = 13;
        scratchSheet.TRHC__Hours_Per_Week__c = 40;
        scratchSheet.over_time__c = 8;
        insert scratchSheet;
        
        TRHC__Expense__c expense = new TRHC__Expense__c();
        expense.TRHC__Expense_Name__c = 'Housing Stipend';
        expense.TRHC__Hourly_Amount__c = 40.00;
        expense.TRHC__Scratch_Sheet__c = scratchSheet.id;
        insert expense;
        TRHC__Expense__c expensePer = new TRHC__Expense__c();
        expensePer.TRHC__Expense_Name__c = 'Per Diem';
        expensePer.TRHC__Hourly_Amount__c = 40.00;
        expensePer.TRHC__Scratch_Sheet__c = scratchSheet.id;
        insert expensePer;
        TRHC__Placement__c placement_scratchSheet_expenses = new TRHC__Placement__c();
        placement_scratchSheet_expenses.TRHC__Scratch_Sheet__c =  scratchSheet.id;
        placement_scratchSheet_expenses.TRHC__Contact_Candidate__c = candidate_Placemnet.id;
        placement_scratchSheet_expenses.TRHC__Start_Date__c = Date.today();
        placement_scratchSheet_expenses.TRHC__End_Date__c = placement_scratchSheet_expenses.TRHC__Start_Date__c + 180;
        placement_scratchSheet_expenses.createdDate = Date.today()-2;
        placement_scratchSheet_expenses.TRHC__Status__c = 'Pending';
        insert placement_scratchSheet_expenses;
        
        TRHC__Placement__c placement_scratchSheet_expenses1 = new TRHC__Placement__c();
        placement_scratchSheet_expenses1.TRHC__Scratch_Sheet__c =  scratchSheet.id;
        placement_scratchSheet_expenses1.TRHC__Contact_Candidate__c = candidate_Placemnet.id;
        placement_scratchSheet_expenses1.TRHC__Start_Date__c = Date.today();
        placement_scratchSheet_expenses1.TRHC__End_Date__c = placement_scratchSheet_expenses.TRHC__Start_Date__c + 180;
        placement_scratchSheet_expenses1.createdDate = Date.today()-4;
        placement_scratchSheet_expenses1.TRHC__Status__c = 'Active';
        insert placement_scratchSheet_expenses1;
        
        TRHC__Placement__c placement_scratchSheet_expenses2 = new TRHC__Placement__c();
        placement_scratchSheet_expenses2.TRHC__Scratch_Sheet__c =  scratchSheet.id;
        placement_scratchSheet_expenses2.TRHC__Contact_Candidate__c = candidate_Placemnet.id;
        placement_scratchSheet_expenses2.TRHC__Start_Date__c = Date.today();
        placement_scratchSheet_expenses2.TRHC__End_Date__c = placement_scratchSheet_expenses.TRHC__Start_Date__c + 180;
        placement_scratchSheet_expenses2.createdDate = Date.today()-10;
        placement_scratchSheet_expenses2.TRHC__Status__c = 'Pending';
        insert placement_scratchSheet_expenses2;
        TRHC__Placement__c placement_scratchSheet_expenses3 = new TRHC__Placement__c();
        placement_scratchSheet_expenses3.TRHC__Scratch_Sheet__c =  scratchSheet.id;
        placement_scratchSheet_expenses3.TRHC__Contact_Candidate__c = candidate_Placemnet.id;
        placement_scratchSheet_expenses3.TRHC__Job__c = job.id;
        placement_scratchSheet_expenses3.TRHC__Start_Date__c = Date.today();
        placement_scratchSheet_expenses3.TRHC__End_Date__c = placement_scratchSheet_expenses.TRHC__Start_Date__c + 180;
        placement_scratchSheet_expenses3.createdDate = Date.today()-1;
        placement_scratchSheet_expenses3.TRHC__Status__c = 'Active';
        placement_scratchSheet_expenses3.TRHC__Job_Applicant__c = jobApp1.id;
        insert placement_scratchSheet_expenses3;
        test.stopTest();
    }
}