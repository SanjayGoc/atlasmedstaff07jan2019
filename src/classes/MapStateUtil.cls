Public class MapStateUtil {
    
    Public static String getStateForCode(String code){
        
        Map <String, String> stateForCode = new Map <String,String>();
        stateForCode.put('AL','Alabama');
        stateForCode.put('AK','Alaska');
        //stateForCode.put('AS','American Samoa');
        stateForCode.put('AZ','Arizona');
        stateForCode.put('AR','Arkansas');
        stateForCode.put('CA','California');
        stateForCode.put('CO','Colorado');
        stateForCode.put('CT','Connecticut');
        stateForCode.put('DE','Delaware');
        stateForCode.put('DC','Dist. of Columbia');
        stateForCode.put('FL','Florida');
        stateForCode.put('GA','Georgia');
        //stateForCode.put('GU','Guam');
        stateForCode.put('HI','Hawaii');
        stateForCode.put('ID','Idaho');
        stateForCode.put('IL','Illinois');
        stateForCode.put('IN','Indiana');
        stateForCode.put('IA','Iowa');
        stateForCode.put('KS','Kansas');
        stateForCode.put('KY','Kentucky');
        stateForCode.put('LA','Louisiana');
        stateForCode.put('ME','Maine');
        stateForCode.put('MD','Maryland');
        //stateForCode.put('MH','Marshall Islands');
        stateForCode.put('MA','Massachusetts');
        stateForCode.put('MI','Michigan');
        stateForCode.put('FM','Micronesia');
        stateForCode.put('MN','Minnesota');
        stateForCode.put('MS','Mississippi');
        stateForCode.put('MO','Missouri');
        stateForCode.put('MT','Montana');
        stateForCode.put('NE','Nebraska');
        stateForCode.put('NV','Nevada');
        stateForCode.put('NH','New Hampshire');
        stateForCode.put('NJ','New Jersey');
        stateForCode.put('NM','New Mexico');
        stateForCode.put('NY','New York');
        stateForCode.put('NC','North Carolina');
        stateForCode.put('ND','North Dakota');
        //stateForCode.put('MP','Northern Marianas');
        stateForCode.put('OH','Ohio');
        stateForCode.put('OK','Oklahoma');
        stateForCode.put('OR','Oregon');
        stateForCode.put('PW','Palau');
        stateForCode.put('PA','Pennsylvania');
        //stateForCode.put('PR','Puerto Rico');
        stateForCode.put('RI','Rhode Island');
        stateForCode.put('SC','South Carolina');
        stateForCode.put('SD','South Dakota');
        stateForCode.put('TN','Tennessee');
        stateForCode.put('TX','Texas');
        stateForCode.put('UT','Utah');
        stateForCode.put('VT','Vermont');
        stateForCode.put('VA','Virginia');
        //stateForCode.put('VI','Virgin Islands');
        stateForCode.put('WA','Washington');
        stateForCode.put('WV','West Virginia');
        stateForCode.put('WI','Wisconsin');
        stateForCode.put('WY','Wyoming');
        Set <String> valueSet =new Set<String>(stateForCode.values());
        if(stateForCode.containsKey(code)){
            return stateForCode.get(code);
        }
        else{
            if(valueSet.contains(code)){
                return code;
            }
            else{
                return null;
            }
        }
        
    }
    
}