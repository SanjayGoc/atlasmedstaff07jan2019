@isTest
private class MetadataUtilNewTest {
    
    private static testMethod void test() {
        TestDataGenerator gen = new TestDataGenerator();
        gen.createCandidates();
        
        MetadataUtilNew md = new MetadataUtilNew(Account.sObjectType);
        md = new MetadataUtilNew('Account');
        md = new MetadataUtilNew(gen.account);
        
        System.assertEquals('Account', md.getObjectName());
        
        md.getAllFieldNames();
        md.getAllFieldsSObject();
        
        String fieldsString = md.getAllFieldsString();
        
        Account v_account = new Account(Name = 'Test Parent');
        insert v_account;
        
        gen.account.ParentId = v_account.Id;
        update gen.account;
        
        v_account = (Account)md.getRecords(
                new Id[] {gen.account.Id}, 
                new List<Schema.sObjectField> { Account.ParentId },
                new List<Schema.sObjectField> { Contact.AccountId })[0];
        
        Account clonedAccount = (Account)md.cloneById(gen.account.Id, null);
        clonedAccount = (Account)md.cloneById(gen.account.Id, new String[] {'Contacts'});
        System.assertEquals('Test Account', clonedAccount.Name);
        System.assertEquals(2, clonedAccount.Contacts.size());

        Account targetAccount = new Account();
        md.updateRecords(gen.account, targetAccount);
        
        String queryWhere = md.makeWhereStatement(new Account(
                Name = 'Test Account', 
                NumberOfEmployees = 3, 
                Type = 'Test'));

        md.GetFieldLabel('Name');

        md.getFieldValue('Name', gen.account);
        
        System.assertEquals(false, md.isFieldExists('TestFieldName'));
        
        md.getParentObjectDescribe('Name');

        md.getAllChildRelationshipsStrings();

        MetadataUtilNew.getWhereConstraint('Name', '=', '', new Account());
        MetadataUtilNew.getWhereConstraint('Name', '=', 'Test', new Account());
        MetadataUtilNew.getWhereConstraint('NumberOfEmployees', '=', '1', new Account());
        MetadataUtilNew.getWhereConstraint('TRMNG__Credentials_Pack__c', '=', 'Test', new Account());
        MetadataUtilNew.getWhereConstraint('TRMNG__Credentials_Pack__c', '!=', 'Test', new Account());
        MetadataUtilNew.getWhereConstraint('Name', 'contains', '', new Account());
        MetadataUtilNew.getWhereConstraint('Name', 'startswith', '', new Account());
        MetadataUtilNew.getWhereConstraint('Name', 'not contains', '', new Account());

        md.getValueByFieldType('Name', 'Test');
        md.getValueByFieldType('NumberOfEmployees', '1');

        MetadataUtilNew mdContact = new MetadataUtilNew(Contact.sObjectType);
        mdContact.getRecordTypeByRecordId(gen.candidates[0].Id);
        mdContact.getRecordTypeIdByRecordTypeName('Candidate');
        mdContact.getAllRecordTypesAsSelectOptions(true);
        //mdContact.getObjectType();
        
    }
}