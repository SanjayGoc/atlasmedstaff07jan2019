@isTest
public class CommunityUpdateProfileRESTOperationTest{
	public static testmethod void profiletest(){
        TRHC__Config_Settings__c configSettings = new TRHC__Config_Settings__c();
        configSettings.name='Default';
        configSettings.Enable_AutoMateJobRates__c = true;
        configSettings.Trigger_Enable_ScratchSheetAutoAddExp__c = false;
        configSettings.AutoAddExpenseCriteria__c='TRHC__Margin_Type__c=\'Expense\'';
        configSettings.Except_Hospitals__c = 'Atlas Development';
        configSettings.Trigger_Enable_Expected_End_Date_Changer__c = false;
        configSettings.Trigger_Enable_Applicant_From_SCR_Sheet__c = true;
        configSettings.Update_Pay_Burden__c = false;
        configSettings.Enable_RefferalTrigger__c = true;
        insert configSettings;

        List<Account> accountList = TestUtilityClass.getTestMultipleAccount();
        contact conobj = new contact();
        conobj.firstName = 'hiren';
        conobj.lastName = 'sharma';
        conobj.phone = '(406) 863-3574';
        conobj.Discipline1__c = 'RN' ;
        conobj.Primary_Speciality1__c = 'IR';
        conobj.Secondary_Speciality__c = 'ICU';
        conobj.TRHC__Password__c = '12345678';
        conobj.TRHC__Shift_Preference__c = '7A-3P';
        conobj.Email = 'HIREN.GOC@GMail.com';
        conobj.apiToken__c = '123546789123456789';
        insert conobj;

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/mobile/forgotPassword';
        req.addParameter('apiToken', conobj.apiToken__c);
        req.addParameter('firstName', conobj.firstName);
        req.addParameter('lastName', conobj.lastName);
        req.addParameter('email', conobj.Email);
        req.addParameter('phone', conobj.phone);
        req.addParameter('discipline', conobj.Discipline1__c);
        req.addParameter('primarySpeciality', conobj.Primary_Speciality1__c);
        req.addParameter('secondarySpecialty', conobj.Secondary_Speciality__c);
        req.addParameter('shiftPreference', conobj.TRHC__Shift_Preference__c);
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;

        CommunityUpdateProfileRESTOperation.ResponseHandler wrapperobj = CommunityUpdateProfileRESTOperation.CommunityUpdateProfileRESTOperation();
	}
}