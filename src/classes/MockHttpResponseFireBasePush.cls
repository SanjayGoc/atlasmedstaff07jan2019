@isTest
public class MockHttpResponseFireBasePush implements HttpCalloutMock 
{
    public HTTPResponse respond(HTTPRequest req) 
    {

        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/xml');
        res.setBody('{"multicast_id":7370265098895500141,"success":1,"failure":0,"canonical_ids":0,"results":[{"message_id":"0:1543847720639971%967654b2967654b2"}]}');
        res.setStatusCode(200);
        //System.assertEquals('POST', req.getMethod());
      
        return res;
    }
}