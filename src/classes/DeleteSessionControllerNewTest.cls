@isTest
public class DeleteSessionControllerNewTest {
    
    @isTest
    public static void testSession(){
        //Trigger_Switches_Medstaff__c medstaff = new Trigger_Switches_Medstaff__c();
		//medstaff.RestrictEmploymentChangesFromPortal__c = true;
        //insert medstaff;
        Profile profileId = [ SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1 ];
        
        User usr = new User ( LastName = 'LIVESTON',
                             FirstName='JASON',
                             Alias = 'jliv',
                             Email = 'jason.liveston@asdf.com',
                             Username = 'test@superhero.com',
                             ProfileId = profileId.id,
                             TimeZoneSidKey = 'GMT',
                             LanguageLocaleKey = 'en_US',
                             EmailEncodingKey = 'UTF-8',
                             LocaleSidKey = 'en_US');
        insert usr;
        
		Id recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Candidate').getRecordTypeId();
         TRHC__Config_Settings__c setting = new TRHC__Config_Settings__c();
        setting.Name = 'Default';
        setting.DisabletrackEventActivityonContact__c = false;
        setting.DisabletrackTaskActivityonContact__c = false;
        setting.Enable_Update_Recruiter__c = false;
        setting.DisabletrackNoteonContact__c = false;
        setting.Trigger_Enable_Expected_End_Date_Changer__c = false;
        setting.Enable_Update_Recruiter__c = true;
        //setting.purchase_order_approve_users__c = usr.id;
        insert setting;
        
        
        Account account = new Account();
        account.Name = 'SwTest';
        insert account;
        
        Contact contact = new Contact();
        contact.FirstName = 'chaitanya';
        contact.LastName = 'bollamreddy';
        contact.TRHC__Recruiter__c = UserInfo.getUserId();
        contact.Discipline1__c = 'RN';
        contact.TRHC__Discipline__c = 'RN';
        contact.TRHC__Candidate_Status__c = 'Active';
        contact.Compliance_Coordinater__c = usr.id;
        contact.TRHC__Account_Manager__c = usr.id;
        contact.OwnerId = usr.id;
        contact.TRHC__UserName__c = 'chai@tr.com';
        contact.TRHC__Password__c = 'passNo';
        contact.AccountId = account.id;
        contact.Permanant_Address__c = 'Muniseb Street';
        contact.TRHC__Permanent_City__c = 'HYD';
        contact.State_pic__c = '';
        contact.TRHC__Permanent_Zip_Postal_Code__c = '533262';
        contact.Temporary_Address_1__c = '3rd cross papailayout';
        contact.TRHC__Temporary_City__c = 'Bangalore';
        contact.temporary_state__c = '';
        contact.TRHC__Temporary_Zip_Postal_Code__c = '560043';
        contact.RecordTypeId = recordTypeId;
        contact.Email = 'Chaitanya.b@targetrecruit.net';
        contact.EMR__c = 'Cerner';
        contact.RecordTypeId = '01241000000y2YC';
        contact.TRHC__Certifications__c = 'ACLS';
        insert contact;
        TRHC__Session__c session = new TRHC__Session__c();
        session.TRHC__Session_For__c = contact.id;
        insert session;
        
        TRHC__Session__c session1 = new TRHC__Session__c();
        session1.TRHC__Session_For__c = contact.id;
        insert session1;
        
        TRHC__Session__c session2 = new TRHC__Session__c();
        session2.TRHC__Session_For__c = contact.id;
        insert session2;
        
        TRHC__Session__c session3 = new TRHC__Session__c();
        session3.TRHC__Session_For__c = contact.id;
        insert session3;
        
        TRHC__Session__c session4 = new TRHC__Session__c();
        session4.TRHC__Session_For__c = contact.id;
        insert session4;
        
        TRHC__Session__c session5 = new TRHC__Session__c();
        session5.TRHC__Session_For__c = contact.id;
        insert session5;
        
        TRHC__Session__c session6 = new TRHC__Session__c();
        session6.TRHC__Session_For__c = contact.id;
        insert session6;
        
        Test.startTest();
        
        DeleteSessionControllerNew dscn = new DeleteSessionControllerNew();
        Database.executeBatch(dscn);
        
        Test.stopTest();
    }

}