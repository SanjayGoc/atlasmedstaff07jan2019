/* Main portal test data helper. */
public with sharing class TestGenerateData {
    
    public Account account;
    public Contact candidate;
    public TRHC__Job__c job;
    public TRHC__Job_Applicant__c applicant;
    
    public void initData() {
        TRHC__Config_Settings__c config = new TRHC__Config_Settings__c (
                Name = 'Default',
                TRHC__Trigger_Disabled_Trigger_JobApplicants__c = true);
        insert config;
        
        TRHC__CustomSettingsComponent__c customSettings = new TRHC__CustomSettingsComponent__c(
                Name ='Default',
                TRHC__SessionTimeout__c = Decimal.valueOf('9999999'),
                TRHC__ErrorsReceiver__c = 'Test@fax.ru',
                TRHC__isShowPaginationCandidateList__c = true,
                TRHC__Maximum_No_of_References__c = '2');
        insert customSettings;
        
        TRHC__Page__c newPage = new TRHC__Page__c(
                Name = 'CMSDefaultProfilePage',
                TRHC__Name__c = 'CMSDefaultProfilePage',
                TRHC__Active__c = true);
        insert newPage;
        
        account = new Account(Name = 'CMSDefaultProfileAccount');
        insert account;
        
        TRHC__CMSProfile__c cmsProfile = new TRHC__CMSProfile__c(
                Name = 'Logged In Candidate',
                TRHC__Name__c = 'Logged In Candidate',
                TRHC__Profile_Manager__c = UserInfo.getUserId(),
                TRHC__Default_Home_Page__c = newPage.Id,
                TRHC__Profile_Account__c = account.Id,
                TRHC__CMSCss__c = 'Twolayoutcss',
                TRHC__Portal_Enabled__c = true,
                TRHC__User_Registration__c = true);
        insert cmsProfile;
        
        User user = [
                SELECT Email 
                FROM User 
                WHERE Email = 'rsmith@atlasmedstaff.com'  
                LIMIT 1];
        
        candidate = new Contact(
                LastName = 'Test',
                FirstName = 'Test',
                Email = 'qwe@qwe.qwe',
                Phone = '111111',
                AccountId = account.Id,
                TRHC__CMSProfile__c = cmsProfile.Id,
                TRHC__Source__c = 'Blog',
                TRHC__Publish__c = true,
                TRHC__Recruiter__c = user.Id,
                TRHC__Client_Portal_Super_User__c = true,
                TRHC__UserName__c = 'qwe@qwe.qwe',
                TRHC__Password__c = '123123123',
                TRHC__Registration_Approved__c = true, 
                RecordTypeId = TRHC.RecordTypeUtilNew.getCandidateRecordTypeIdList()[0]);
        insert candidate;
        
        job = new TRHC__Job__c(
                TRHC__Job_Title__c = 'Test',
                TRHC__Start_Date__c = Date.today(),
                TRHC__Account_Lookup__c = account.Id);
        insert job;
        
        applicant = new TRHC__Job_Applicant__c(
                TRHC__Job__c = job.Id,
                TRHC__Contact_Candidate__c = candidate.id);
        insert applicant;

        String sessionId = '11288978345235';
        
        Cookie sessionCookie = new Cookie(sessionId, sessionId,null,-1,false);
        
        ApexPages.currentPage().setCookies(new Cookie[]{sessionCookie});
        
        Map<String,String> headerMap = System.currentPageReference().getHeaders();

        TRHC__Session__c session = new TRHC__Session__c(
                TRHC__Is_Valid__c  = true,
                TRHC__IP_Address__c = headerMap.get('X-Salesforce-SIP'),
                TRHC__Session_For__c = candidate.Id,
                TRHC__User_Agent__c = headerMap.get('User-Agent'),
                TRHC__SessionId__c = sessionId);
        insert session;
        
        TRHC__CredentialsCustomSettings__c cs = new TRHC__CredentialsCustomSettings__c(
                Name = 'Default',
                TRHC__Disable_CredAnswerCopyAttachmentTrigger__c = true,
                TRHC__Portal_Certifications_Record_Types__c = 'Credential Master Template');
        insert cs;
        
        TRHC__Job_Applicant__c applicant = new TRHC__Job_Applicant__c(
                TRHC__Job__c = job.Id,
                TRHC__Contact_Candidate__c = candidate.id);
        insert applicant;

        ApexPages.currentPage().getParameters().put('sessionId', sessionId);
        ApexPages.currentPage().getParameters().put('candidateId', candidate.Id);
        ApexPages.currentPage().getParameters().put('p','Candidate');
        ApexPages.currentPage().getParameters().put('Id', job.Id);
        ApexPages.currentPage().getParameters().put('jobId', job.Id);
        
        candidate = [
                SELECT FirstName, LastName, TRHC__Recruiter__r.Email, Email, Phone, 
                        AccountId, TRHC__CMSProfile__c, TRHC__Source__c, TRHC__Publish__c, 
                        TRHC__Recruiter__c, TRHC__Client_Portal_Super_User__c, 
                        TRHC__UserName__c, TRHC__Password__c, RecordTypeId
                FROM Contact 
                WHERE Id = :candidate.id];
    }
}