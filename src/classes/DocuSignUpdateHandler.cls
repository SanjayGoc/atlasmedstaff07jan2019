public with sharing class DocuSignUpdateHandler {
    public static boolean isAfterUpdate;
    public static void updateContact(List<dsfs__DocuSign_Status__c> DocuSignStatusRecords, 
                                        Map<Id, dsfs__DocuSign_Status__c> DocuSignRecordMap) {
        List<dsfs__DocuSign_Status__c> updateDocSignList = new List<dsfs__DocuSign_Status__c>();
        for(dsfs__DocuSign_Status__c docSign : [SELECT Id, Name, Placement__c, dsfs__Contact__c, 
                                                    Placement__r.TRHC__Contact_Candidate__c 
                                                    FROM dsfs__DocuSign_Status__c 
                                                    WHERE Placement__c != NULL AND Id IN: DocuSignRecordMap.keyset()]) {
            if(docSign.Placement__r.TRHC__Contact_Candidate__c != Null) {
                docSign.dsfs__Contact__c = docSign.Placement__r.TRHC__Contact_Candidate__c;
                updateDocSignList.add(docSign);
            }
        }
        if(!updateDocSignList.isEmpty()) {
            update updateDocSignList;
        }
    }
    public static void sendEmail(List<dsfs__DocuSign_Status__c> DocuSignStatusRecords, 
                                        Map<Id, dsfs__DocuSign_Status__c> DocuSignRecordMap) {
        List<Messaging.SingleEmailMessage> mail_list = new List<Messaging.SingleEmailMessage>();
        List<Messaging.EmailFileAttachment> attachList;
        List<Messaging.EmailFileAttachment> csvAttc;
        Messaging.SingleEmailMessage email;
        Map<Id, Id> docWithPlacementId = new Map<Id, Id>();
        Map<Id, TRHC__Placement__c> docWithPlacement = new Map<Id, TRHC__Placement__c>();
        Map<Id, List<Messaging.EmailFileAttachment>> attachmentmap = new Map<Id, List<Messaging.EmailFileAttachment>>();
        List<EmailTemplate> template = new List<EmailTemplate>([SELECT id, Subject, body, HTMLValue FROM EmailTemplate 
                                                                WHERE DeveloperName = 'Placement_Group_Notification']);
        OrgWideEmailAddress[] owea = [SELECT Id FROM OrgWideEmailAddress WHERE Address = 'info@atlasmedstaff.com'];
        //Create Map for docuSign Id with Placement Id
        for(dsfs__DocuSign_Status__c doc : DocuSignStatusRecords) {
            docWithPlacementId.put(doc.Placement__c, doc.Id);
        }
        for(TRHC__Placement__c plm : [SELECT Id, TRHC__Start_Date__c, TRHC__End_Date__c, 
                                        VMS__c, VMS_Fee__c, TRHC__Guaranteed_Hours__c, 
                                        TRHC__Regular_Hours_Pay_Rate__c, TRHC__Overtime_Hours_Pay_Rate__c, 
                                        TRHC__On_Call_Hours_Pay_Rate__c, TRHC__Holiday_Hours_Pay_Rate__c, 
                                        TRHC__Call_Back_Hours_Pay_Rate__c, Additional_Required_Certs__c, 
                                        TRHC__Account_Manager__r.Name, TRHC__Recruiter__r.Name, Fyre__c, 
                                        Hospital_Address__c, Hospital_City__c, Hospital_State__c, 
                                        Hospital_Postal_Code__c, Hospital_Phone__c, TRHC__Contact_Candidate__r.Name, 
                                        TRHC__Job__r.Name FROM TRHC__Placement__c 
                                        WHERE Id IN : docWithPlacementId.keyset()]) {
            if(docWithPlacementId.containskey(plm.Id)) {
                docWithPlacement.put(docWithPlacementId.get(plm.Id), plm);
            }
        }
        for(Attachment att : [SELECT Id, Name, parentId, body FROM Attachment WHERE parentId IN : DocuSignRecordMap.keyset() AND Name LIKE : '%Work Order%']) {
            if(!attachmentmap.containsKey(att.parentId)) {
                attachList = new List<Messaging.EmailFileAttachment>();
                Messaging.EmailFileAttachment mailAttach = new Messaging.EmailFileAttachment();
                mailAttach.fileName = att.Name;
                mailAttach.body = att.body;
                attachList.add(mailAttach);
                attachmentmap.put(att.parentId, attachList);
            } else {
                attachList = attachmentmap.get(att.parentId);
                Messaging.EmailFileAttachment mailAtt = new Messaging.EmailFileAttachment();
                mailAtt.fileName = att.Name;
                mailAtt.body = att.body;
                attachList.add(mailAtt);
                attachmentmap.put(att.parentId, attachList);
            }
        }
        for(dsfs__DocuSign_Status__c docSign : DocuSignStatusRecords) {
            if(docSign.dsfs__Envelope_Status__c == 'Completed' && attachmentmap.containskey(docSign.Id) && docWithPlacement.containskey(docSign.Id)) {
                //Sending Mail
                email = new Messaging.SingleEmailMessage();
                if(owea.size()>0) {
                    email.setOrgWideEmailAddressId(owea.get(0).Id);
                }
                String htmlbody = '';
                htmlbody = template[0].HTMLValue;
                htmlbody = htmlbody.replace('{!startDate}', String.valueof(docWithPlacement.get(docSign.Id).TRHC__Start_Date__c));
                htmlbody = htmlbody.replace('{!endDate}', String.valueof(docWithPlacement.get(docSign.Id).TRHC__End_Date__c));
                htmlbody = htmlbody.replace('{!vms}', docWithPlacement.get(docSign.Id).VMS__c);
                htmlbody = htmlbody.replace('{!vmsFee}', String.valueof(docWithPlacement.get(docSign.Id).VMS_Fee__c));
                htmlbody = htmlbody.replace('{!guaranteedHours}', String.valueof(docWithPlacement.get(docSign.Id).TRHC__Guaranteed_Hours__c));
                htmlbody = htmlbody.replace('{!billRate}', String.valueof(docWithPlacement.get(docSign.Id).TRHC__Regular_Hours_Pay_Rate__c));
                htmlbody = htmlbody.replace('{!otRate}', String.valueof(docWithPlacement.get(docSign.Id).TRHC__Overtime_Hours_Pay_Rate__c));
                htmlbody = htmlbody.replace('{!oncallRate}', String.valueof(docWithPlacement.get(docSign.Id).TRHC__On_Call_Hours_Pay_Rate__c));
                htmlbody = htmlbody.replace('{!holidayRate}', String.valueof(docWithPlacement.get(docSign.Id).TRHC__Holiday_Hours_Pay_Rate__c));
                htmlbody = htmlbody.replace('{!callbackRate}', String.valueof(docWithPlacement.get(docSign.Id).TRHC__Call_Back_Hours_Pay_Rate__c));
                htmlbody = htmlbody.replace('{!addCert}', docWithPlacement.get(docSign.Id).Additional_Required_Certs__c);
                htmlbody = htmlbody.replace('{!clientManager}', docWithPlacement.get(docSign.Id).TRHC__Account_Manager__r.Name);
                htmlbody = htmlbody.replace('{!recruiter}', docWithPlacement.get(docSign.Id).TRHC__Recruiter__r.Name);
                htmlbody = htmlbody.replace('{!fyre}', (docWithPlacement.get(docSign.Id).Fyre__c != null ? docWithPlacement.get(docSign.Id).Fyre__c : ''));
                htmlbody = htmlbody.replace('{!hospitalContact}', docWithPlacement.get(docSign.Id).Hospital_Address__c + ', ' + docWithPlacement.get(docSign.Id).Hospital_City__c + ' ' + docWithPlacement.get(docSign.Id).Hospital_State__c + '-' + docWithPlacement.get(docSign.Id).Hospital_Postal_Code__c + ', ' + docWithPlacement.get(docSign.Id).Hospital_Phone__c);
                List<String> toAddresses = new List<String>();
                toAddresses.add(System.Label.Placement_Group_Email);
                if(attachmentmap.get(docSign.Id).size() > 0) {
                    email.setFileAttachments(attachmentmap.get(docSign.Id));
                    email.setHtmlBody(htmlbody);
                    email.setPlainTextBody(htmlbody);
                }
                String subject = docWithPlacement.get(docSign.Id).TRHC__Contact_Candidate__r.Name + ' - ' + docWithPlacement.get(docSign.Id).TRHC__Job__r.Name + ' - ' + String.valueof(docWithPlacement.get(docSign.Id).TRHC__Start_Date__c) + ' - Work Order';
                email.setSubject(subject);
                email.setToAddresses(toAddresses);
                mail_list.add(email);    
            }
        }
        if(!mail_list.isEmpty()) {
            Messaging.sendEmail(mail_list);
        }
        isAfterUpdate = true;
    }
}