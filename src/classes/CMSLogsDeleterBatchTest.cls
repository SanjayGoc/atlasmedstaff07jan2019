@istest()
public class CMSLogsDeleterBatchTest {
    @istest()
    public static void testCMSLogs(){
        TRHC__CMSSiteSetup__c cmsSite = new TRHC__CMSSiteSetup__c();
        cmsSite.TRHC__Name__c = 'Test';
        cmsSite.TRHC__Value__c = 'Test';
        cmsSite.createdDate = Date.today() - 40;
        insert cmsSite;
        Test.startTest();
        CMSLogsDeleterBatch  batchCMS = new CMSLogsDeleterBatch();
        Database.executeBatch(batchCMS);
        Test.stopTest();
    }
}