@isTest
public class CreateScracthSheetFromTemplateTest {
    public static testMethod void test(){
         TRHC__Config__c configObj1 = new TRHC__Config__c();
        configObj1.TRHC__Name__c = 'check';
        configObj1.TRHC__Margin_Type__c = 'Expense';
        configObj1.TRHC__Frequency__c = 'Weekly';
        configObj1.TRHC__State__c =   'California';
        configObj1.TRHC__Include_in_payroll__c= True;
        configObj1.TRHC__Pay_Period_to_be_paid__c='First Check';
        configObj1.TRHC__Active__c = True;
        configObj1.TRHC__Value__c = '350.0';
        configObj1.TRHC__Grouping_Total__c ='Pay Burden';
        configObj1.TRHC__City__c = 'test';
        
        configObj1.RecordTypeId =[Select Id From RecordType Where SobjectType = 'TRHC__Config__c' and Name = 'Margin calculation Line Item'].Id;
        
        insert configObj1;
        
        TRHC__Config_Settings__c configSettings = new TRHC__Config_Settings__c();
        configSettings.name='Default';
        configSettings.Trigger_Enable_ScratchSheetAutoAddExp__c = true;
        configSettings.AutoAddExpenseCriteria__c='TRHC__Margin_Type__c=\'Expense\'';
        configSettings.ExceptConfigIds__c = configObj1.Id;
        configSettings.Except_Hospitals__c = 'Atlas Development';
        insert configSettings;
        
        TRHC__Config__c configObj = new TRHC__Config__c();
        configObj.TRHC__Name__c = 'check';
        configObj.TRHC__Margin_Type__c = 'Expense';
        configObj.TRHC__Frequency__c = 'Weekly';
        configObj.TRHC__State__c =   'California';
        configObj.TRHC__Include_in_payroll__c= True;
        configObj.TRHC__Pay_Period_to_be_paid__c='First Check';
        configObj.TRHC__Active__c = True;
        configObj.TRHC__Value__c = '350.0';
        configObj.TRHC__Grouping_Total__c ='Pay Burden';
        
        configObj.RecordTypeId =[Select Id From RecordType Where SobjectType = 'TRHC__Config__c' and Name = 'Margin calculation Line Item'].Id;
        
        insert configObj;
    
    
        TRHC__Healthcare_Setting__c customSetting = 
            new TRHC__Healthcare_Setting__c();
        customSetting.name = 'Default';
        customSetting.TRHC__Default_Scratch_sheet_Template_Name__c = 'Demo ScratchSheet';
        insert customSetting;
        
        /*TRHC__Config_Settings__c config = new TRHC__Config_Settings__c();
            config.name = 'Default';
            config.Trigger_Enable_Expected_End_Date_Changer__c = true;
            insert config;*/
        
        Map<String,String> scratchSheethelper = ObjectFieldsHelper.getAllFields('TRHC__Scratch_Sheet__c');
        Map<String,String> scratchSheethelper1 = ObjectFieldsHelper.getAllFields('TRHC__job__c');
        Account account = new Account();
        account .name ='test';
        
        insert account;
        
        TRHC__Job__c job = new TRHC__Job__c();
        job.TRHC__Account_Lookup__c = account.id;
        job.TRHC__County__c = 'india';
        job.TRHC__State__c = 'California';
        job.TRHC__Job_Title__c = 'indiatest';
        job.state_Pic__c ='California';
        job.TRHC__County__c ='text';
        insert job;
            Job_to_Scratch_Sheet__c jobToScCus = new Job_to_Scratch_Sheet__c(Name='Test',Job_Field_API_Name_Custom__c='TRHC__County__c',Scratch_Sheet_Field_API_Name__c='TRHC__County__c');
            insert jobToScCus;
            TRHC__Scratch_Sheet__c newScratchSheetTemp = new TRHC__Scratch_Sheet__c();
            newScratchSheetTemp.TRHC__County__c = job.TRHC__County__c ;
            
            newScratchSheetTemp.TRHC__State__c=job.TRHC__State__c;
            
            newScratchSheetTemp.Name='Demo ScratchSheet';
            String scrtchSheetTempRecordTypeId = [Select Id From RecordType Where SobjectType = 'TRHC__Scratch_Sheet__c' and Name = 'ScratchSheet Template'].Id;
            newScratchSheetTemp.RecordTypeId=scrtchSheetTempRecordTypeId ;
            insert newScratchSheetTemp;
        
            
            TRHC__Scratch_Sheet__c newScratchSheet = new TRHC__Scratch_Sheet__c();
            newScratchSheet.TRHC__County__c = job.TRHC__County__c ;
            
            newScratchSheet.TRHC__State__c=job.TRHC__State__c;
            
            newScratchSheet.Name=job.TRHC__Job_Title__c+' ScratchSheet';
            String scrtchSheetRecordTypeId = [Select Id From RecordType Where SobjectType = 'TRHC__Scratch_Sheet__c' and Name = 'ScratchSheet'].Id;
            newScratchSheet.RecordTypeId=scrtchSheetRecordTypeId;
            insert newScratchSheet;
            Test.setCurrentPageReference(new PageReference('Page.ScratchSheetCreatorfromjobcustompage')); 
            string jjid = '\''+job.id+'\'';
            System.currentPageReference().getParameters().put('jId', jjid);
            CreateScracthSheetFromTemplate crs = new CreateScracthSheetFromTemplate();
            
            
            crs.createscratchsheet();
            
    }
}