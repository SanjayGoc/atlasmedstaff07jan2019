/**
* @Sandeep Rayavarapu
* 05-02-2013
* This controller is used to get the assessment types from prophecy and selet assessments list
* based on the assessment type selected and inserts those assessments that are sent into
* assessment object in salesforce with request id caregiver assessment id and assessments details
*/

global with sharing class ProphecySendAssessments {
    public ProphecySendassessments.ProphecyConnectCDT_cfc v_WebserviceCalloutClass { get; private set; } 
    public List< DynamicAssessmentObject > listOfAllAssessmentsSentToProphecy { get; set; }
    public List< DynamicAssessmentObject > assessmentList { get; set; }
    public String responseForGetCareGivers { get; set; }
    public String searchForAssessments { get; set; }
    public String SelectedAssessmentType { get; set; }
    public List< SelectOption > options { get; set; }
    public String AssessmentCount { get; set; }
    public String AssessmentIsActive { get; set; }
    public List< String > AssessmentTypes { get; set; }
    public TRHC__Prophecy_Settings__c v_ProphecyCustomSettings { get; set; }
    public Contact v_ContactSobj { get; set; }
    public String assesstypes { get; set; }
    public String sendAssessmentResponse {get; set; }
    public TRHC__Config_Settings__c v_ProphacyEnableCustomSettings { get; set; }
    public PageReference refresh = ApexPages.currentPage(); 
    public List< TRHC__Skill_Answers__c > skillAnswers { get; set; }
    public Boolean fromPortal = false;
    List< TRHC__Skill_Answers__c > skillsCheckList = new List< TRHC__Skill_Answers__c >();
    global String candidateByPassLoginURL { get; set; }
    global String candidateCareGiverId = '';
    
    global ProphecySendAssessments() {
        candidateByPassLoginURL = '';
        skillAnswers = NEW List <TRHC__Skill_Answers__c> ();
        v_ProphacyEnableCustomSettings=[SELECT 
                                            TRHC__Enable_Prophecy__c 
                                        FROM 
                                            TRHC__Config_Settings__c 
                                        LIMIT 1];
        v_ProphecyCustomSettings = TRHC__Prophecy_Settings__c.getinstance(UserInfo.getUserId());
       
        listOfAllAssessmentsSentToProphecy = new List<DynamicAssessmentObject>(); 
    }
  
    
    global pagereference Showassessments() {
        AssessmentList = new List< DynamicAssessmentObject >();
        String v_SecurityCode1 = v_ProphecyCustomSettings.TRHC__Security_Code__c;
        Double v_AssessmentID1 = -1;
        String v_AssessmentType11 = 'Checklist';
        System.debug(',,,,,,,,,,,,,,,,,,,,,,,,,' + v_AssessmentType11 );
        v_WebserviceCalloutClass = new ProphecySendassessments.ProphecyConnectCDT_cfc();
        if (!Test.isRunningtest()) {
            searchForAssessments = v_WebserviceCalloutClass.GetAssessments(v_securitycode1, v_AssessmentID1, v_AssessmentType11);
        }
        System.debug(',,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,' + searchForAssessments);
        String encoded = EncodingUtil.urlEncode(searchForAssessments,'UTF-8');
        encoded = encoded.replaceAll('%3C%21%5BCDATA%5B', ''); // i.e. '<![CDATA['
        encoded = encoded.replaceAll('%5D%5D%3E', ''); // i.e. ']]>'
        String formatedXMLString = EncodingUtil.urlDecode(encoded, 'UTF-8');
        formatedXMLString = formatedXMLString.replaceAll('&', 'ampEncoded');
        DOM.Document assessmentDetails = new DOM.Document();
        assessmentDetails.load(formatedXMLString);
        DOM.XMLNode groupRoot = assessmentDetails.getRootElement();
        System.debug(groupRoot.getChildElement('assessments', null).getChildElements());
        for (dom.XMLNode indivAssessment : groupRoot.getChildElement('assessments', null).getChildElements()) {
            System.debug('ssssssssssssssss' + indivAssessment);
            String assessmentId = indivAssessment.getChildElement('assessmentid', null).gettext();
            String assessmentName = indivAssessment.getChildElement('assessmentname', null).gettext();
            String assessmentType = indivAssessment.getChildElement('assessmenttype', null).gettext();
            String costPoints = indivAssessment.getChildElement('costpoints', null).gettext();
            String numberOfQuestions = indivAssessment.getChildElement('numberofquestions', null).gettext();
            String timeLimit = indivAssessment.getChildElement('timelimit', null).gettext();
            assessmentName = assessmentName.replaceAll('ampEncoded', '&');
            DynamicAssessmentObject objWrap = new DynamicAssessmentObject(assessmentName, assessmentId , assessmentType, costPoints, numberOfQuestions, timeLimit);
            AssessmentList.add(objWrap);
        }
        System.debug('AssessmentListAssessmentList'+AssessmentList);       
        return null;
    }
    /**
    * @Sandeep Rayavarapu Rayavarapu
    * @05-02-2013
    * @Test.isRunningTest() used for test case purpose
    * @This method sorts those assessments that are send to prophecy
    * @returns null
    */ 
    global PageReference ProcessSelectedAssessments() {
        System.debug('AssessmentListAssessmentList' + AssessmentList);
        for (DynamicAssessmentObject indivAssessment: AssessmentList) {
            if (indivAssessment.selected == true) {
                listOfAllAssessmentsSentToProphecy.add(indivAssessment);
            }
        }
        Set< DynamicAssessmentObject > selectedAssessmentSet = new Set< DynamicAssessmentObject >();
        selectedAssessmentSet.addAll(listOfAllAssessmentsSentToProphecy);
        listOfAllAssessmentsSentToProphecy.clear();
        listOfAllAssessmentsSentToProphecy.addAll(selectedAssessmentSet);
        return null;
    }
    
    global PageReference SendAssess() {
        v_ContactSobj =[SELECT
                                id, name, MobilePhone, Department, Email, TRHC__Prophecy_Caregiver_ID__c,
                                TRHC__SSN_New__c, FirstName, LastName, TRHC__Driver_Licence_Number__c, Phone 
                            FROM 
                                Contact 
                            WHERE 
                                id =:ApexPages.currentPage().getParameters().get('id')];
        ApexPages.StandardController sc = new ApexPages.StandardController(v_ContactSobj);
        TRHC.ProphecyCaregiver caregiverInstance = new TRHC.ProphecyCaregiver(sc);
        if ( !Test.isRunningTest() ) {
            caregiverInstance.CreateCaregiverId();
            candidateCareGiverId = caregiverInstance.candidateCareGiverId;
        }
        else {
            candidateCareGiverId = '12345';
        }
        if (listOfAllAssessmentsSentToProphecy.size() == null || listOfAllAssessmentsSentToProphecy.size() == 0) {
            ProcessSelectedAssessments();
        }
        if (listOfAllAssessmentsSentToProphecy.size() != null && listOfAllAssessmentsSentToProphecy.size() != 0) {
            List< ProphecySendassessments.Assessment > selectedIndivAssessmentList = new List< ProphecySendassessments.Assessment >();
            for (DynamicAssessmentObject SetOfSelectedAssessments9:listOfAllAssessmentsSentToProphecy) {
                ProphecySendassessments.Assessment indivSelectedAssessment = new ProphecySendassessments.Assessment();
                indivSelectedAssessment.AssessmentID = Double.valueof(SetOfSelectedAssessments9.ID);
                indivSelectedAssessment.AssessmentName = String.valueof(SetOfSelectedAssessments9.Name);
                indivSelectedAssessment.AssessmentType = SetOfSelectedAssessments9.Type;
                indivSelectedAssessment.CostPoints = Double.valueof(SetOfSelectedAssessments9.CostPoints);
                indivSelectedAssessment.NumberOfQuestions = Double.valueof(SetOfSelectedAssessments9.NumberOfQuestions);
                indivSelectedAssessment.TimeLimit = Double.valueof(SetOfSelectedAssessments9.TimeLimit);
                indivSelectedAssessment.BehavioralJobCategoryID = -1;
                selectedIndivAssessmentList.add(indivSelectedAssessment);
            }
            List< ProphecySendassessments.Caregiver > prophecyCareGiverIdList = new List< ProphecySendassessments.Caregiver >();
            v_ContactSobj =[SELECT 
                                id, name, ownerid, Department, Email, TRHC__Prophecy_Caregiver_ID__c, 
                                TRHC__SSN_New__c, FirstName, LastName, TRHC__Driver_Licence_Number__c, Phone 
                            FROM  
                                Contact
                            WHERE
                                id =:ApexPages.currentPage().getParameters().get('id')];
            ProphecySendassessments.Caregiver prophecyCareGiverId = new ProphecySendassessments.Caregiver();
            prophecyCareGiverId.caregiverid = Double.valueof(candidateCareGiverId);
            prophecyCareGiverIdList.add(prophecyCareGiverId);
            String SecurityCode = v_ProphecyCustomSettings.TRHC__Security_Code__c;
            ProphecySendassessments.Caregiver[]CaregiverIDs = prophecyCareGiverIdList;
            ProphecySendassessments.Assessment[]Assessments = selectedIndivAssessmentList;
            Double DepartmentID = Double.valueof(v_ProphecyCustomSettings.TRHC__Department_ID__c);
            Double UserID = Double.valueof(v_ProphecyCustomSettings.TRHC__User_ID__c);
            Double BillToDepartmentID = Double.valueof(v_ProphecyCustomSettings.TRHC__Department_ID__c);
            Double SendResultsToDepartmentID = Double.valueof(v_ProphecyCustomSettings.TRHC__Department_ID__c);
            v_WebserviceCalloutClass = new ProphecySendassessments.ProphecyConnectcdt_cfc();
            if(!test.isrunningtest()) {
                sendAssessmentResponse = v_WebserviceCalloutClass.SendAssessments(SecurityCode, CaregiverIDs, Assessments, DepartmentID, 
                                                                                    UserID, BillToDepartmentID, SendResultsToDepartmentID);
            }
            System.debug(',,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,' + sendAssessmentResponse);
            String encoded = EncodingUtil.urlEncode(sendAssessmentResponse, 'UTF-8');
            encoded = encoded.replaceAll('%3C%21%5BCDATA%5B', ''); // i.e. '<![CDATA['
            encoded = encoded.replaceAll('%5D%5D%3E', ''); // i.e. ']]>'
            String formatedXMLString = EncodingUtil.urlDecode(encoded, 'UTF-8');
            formatedXMLString = formatedXMLString.replaceAll('&', 'ampEncoded');
            DOM.Document assessmentDetails = new DOM.Document();
            assessmentDetails.load(formatedXMLString);
            DOM.XMLNode groupRoot = assessmentDetails.getRootElement();
            skillsCheckList = new List< TRHC__Skill_Answers__c >();
            for (dom.XMLNode indivRequest : groupRoot.getChildElement('requests', null).getChildElements()) {
                String requestId = indivRequest.getChildElement('requestid', null).gettext();
                for (dom.XMLNode indivAssessment : indivRequest.getChildElement('assessments', null).getChildElements()) {
                    System.debug('ssssssssssssssss' + indivAssessment);
                    String assessmentId = indivAssessment.getChildElement('assessmentid', null).gettext();
                    String caregiverAssessmentId = indivAssessment.getChildElement('caregiverassessmentid', null).gettext();
                    String hashedExamId = indivAssessment.getChildElement('hashedexamid', null).gettext();
                    String assessmentName = indivAssessment.getChildElement('assessmentname', null).gettext();
                    String assessmentType = indivAssessment.getChildElement('assessmenttype', null).gettext();
                    assessmentName = assessmentName.replaceAll('ampEncoded', '&');
                    TRHC__Skill_Answers__c skillsCheck = new TRHC__Skill_Answers__c();
                    skillsCheck.TRHC__Assessment_ID__c = integer.valueOf(assessmentId);
                    skillsCheck.TRHC__Skill_Checklist_Name__c = assessmentName;
                    skillsCheck.TRHC__Assessment_Type__c = assessmentType;
                    skillsCheck.TRHC__Caregiver_Assessment_ID__c = caregiverAssessmentId;
                    skillsCheck.TRHC__Request_ID__c = requestId;
                    skillsCheck.TRHC__Contact__c = v_ContactSobj.id;
                    skillsCheck.TRHC__Hashed_Exam_Id__c = hashedExamId; 
                    skillsCheckList.add(skillsCheck);
                    
                }
            }
            v_ContactSobj.TRHC__Prophecy_Caregiver_ID__c = caregiverInstance.candidateCareGiverId;
            if (fromPortal == true) {
                candidateByPassLoginURL = getByPassURL();
                updateRecords();
            } else {
                updateRecords();
            }
            AssessmentIsActive = 'true';
            return null;
        }
        else {
            return null;
        }
    }
    /**
    * @Sandeep Rayavarapu 
    * @05-02-2013
    * @This class holds the elements of Caregiver from Prophecy
    */
    global class Caregiver {
        public Double CaregiverID;
        private String[] CaregiverID_type_info = new String[]{ 'CaregiverID', 'http://www.w3.org/2001/XMLSchema', 'double', '1', '1', 'true' };
        private String[] apex_schema_type_info = new String[]{ 'http://prophecyconnect', 'true', 'false' };
        private String[] field_order_type_info = new String[]{ 'CaregiverID' };
    }
    /**
    * @Sandeep Rayavarapu 
    * @05-02-2013
    * @This class holds the elements of Assessment from Prophecy
    */
    global class Assessment {
        public Double AssessmentID;
        public String AssessmentName;
        public String AssessmentType;
        public Double BehavioralJobCategoryID;
        public Double CostPoints;
        public Double NumberOfQuestions;
        public Double TimeLimit;
        private String[] AssessmentID_type_info = new String[]{ 'AssessmentID', 'http://www.w3.org/2001/XMLSchema', 'double', '1', '1', 'true' };
        private String[] AssessmentName_type_info = new String[]{ 'AssessmentName', 'http://www.w3.org/2001/XMLSchema', 'string', '1', '1', 'true' };
        private String[] AssessmentType_type_info = new String[]{ 'AssessmentType', 'http://www.w3.org/2001/XMLSchema', 'string', '1', '1', 'true' };
        private String[] BehavioralJobCategoryID_type_info = new String[]{ 'BehavioralJobCategoryID', 'http://www.w3.org/2001/XMLSchema', 'double', '1', '1', 'true' };
        private String[] CostPoints_type_info = new String[]{ 'CostPoints', 'http://www.w3.org/2001/XMLSchema', 'double', '1', '1', 'true' };
        private String[] NumberOfQuestions_type_info = new String[]{ 'NumberOfQuestions', 'http://www.w3.org/2001/XMLSchema', 'double', '1', '1', 'true' };
        private String[] TimeLimit_type_info = new String[]{ 'TimeLimit', 'http://www.w3.org/2001/XMLSchema', 'double', '1', '1', 'true' };
        private String[] apex_schema_type_info = new String[]{ 'http://prophecyconnect', 'true', 'false' };
        private String[] field_order_type_info = new String[]{ 'AssessmentID', 'AssessmentName', 'AssessmentType', 'BehavioralJobCategoryID', 'CostPoints', 'NumberOfQuestions', 'TimeLimit' };
    }
    /**
    * @Sandeep Rayavarapu 
    * @05-02-2013
    * @This clas holds the elements of SearchCaregivers from Prophecy
    */
    global class ProphecyConnectCDT_cfc {
        TRHC__Prophecy_Settings__c  v_ProphecyCustomSettings = TRHC__Prophecy_Settings__c.getinstance(UserInfo.getUserId());
        public String endpoint_x = v_ProphecyCustomSettings.TRHC__EndPointUrl_assessments__c;
        public Map < String, String > inputHttpHeaders_x;
        public Map < String, String > outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{ 'http://rpc.xml.coldfusion', 'rpcXmlColdfusion2', 'http://prophecyconnect', 'ProphecySendassessments' };
        /**
        * @Sandeep Rayavarapu 
        * @05-02-2013
        * @ webservice callout for SendAssessments to Prophecy
        * @Retuns string response for SendAssessments from Prophecy in xmlformat
        */
        public String SendAssessments(String SecurityCode, ProphecySendassessments.Caregiver[]Caregivers, ProphecySendassessments.Assessment[]Assessments, Double DepartmentID, Double UserID, 
                                        Double BillToDepartmentID, Double SendResultsToDepartmentID) {
                                        ProphecySendassessments.SendAssessments_element request_x = new ProphecySendassessments.SendAssessments_element();
            ProphecySendassessments.SendAssessmentsResponse_element response_x;
            request_x.SecurityCode = SecurityCode;
            request_x.Caregivers = Caregivers;
            request_x.Assessments = Assessments;
            request_x.DepartmentID = DepartmentID;
            request_x.UserID = UserID;
            request_x.BillToDepartmentID = BillToDepartmentID;
            request_x.SendResultsToDepartmentID = SendResultsToDepartmentID;
            Map < String, ProphecySendassessments.SendAssessmentsResponse_element > response_map_x = new Map < String, ProphecySendassessments.SendAssessmentsResponse_element > ();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(this, request_x, response_map_x, new String[] {
                    endpoint_x, '', 'http://prophecyconnect', 'SendAssessments', 'http://prophecyconnect', 'SendAssessmentsResponse', 'ProphecySendassessments.SendAssessmentsResponse_element' }
            );
            response_x = response_map_x.get('response_x');
            return response_x.SendAssessmentsReturn;
        }
        /**
        * @Sandeep Rayavarapu 
        * @05-02-2013
        * @ webservice callout for GetCaregivers to Prophecy
        * @Retuns string response for GetCaregivers from Prophecy in xmlformat
        */
        global String GetCaregivers(String SecurityCode, Double CaregiverID, Double DepartmentID) {
            ProphecySendassessments.GetCaregivers_element request_x = new ProphecySendassessments.GetCaregivers_element();
            ProphecySendassessments.GetCaregiversResponse_element response_x;
            request_x.SecurityCode = SecurityCode;
            request_x.CaregiverID = CaregiverID;
            request_x.DepartmentID = DepartmentID;
            Map < String, ProphecySendassessments.GetCaregiversResponse_element > response_map_x = new Map < String, ProphecySendassessments.GetCaregiversResponse_element > ();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(this, request_x, response_map_x, new String[] {
                        endpoint_x, '', 'http://prophecyconnect', 'GetCaregivers', 'http://prophecyconnect', 'GetCaregiversResponse', 'ProphecySendassessments.GetCaregiversResponse_element' }
            );
            response_x = response_map_x.get('response_x');
            return response_x.GetCaregiversReturn;
        }
        /**
        * @Sandeep Rayavarapu 
        * @05-02-2013
        * @ webservice callout for GetAssessments to Prophecy
        * @Retuns string response for GetAssessments from Prophecy in xmlformat
        */ 
        global string GetAssessments(String SecurityCode, Double AssessmentID, String AssessmentType) {
            ProphecySendassessments.GetAssessments_element request_x = new ProphecySendassessments.GetAssessments_element();
            ProphecySendassessments.GetAssessmentsResponse_element response_x;
            request_x.SecurityCode = SecurityCode;
            request_x.AssessmentID = AssessmentID;
            request_x.AssessmentType = AssessmentType;
            Map < String, ProphecySendassessments.GetAssessmentsResponse_element > response_map_x = new Map < String, ProphecySendassessments.GetAssessmentsResponse_element > ();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(this, request_x, response_map_x, new String[] {
                    endpoint_x, '', 'http://prophecyconnect', 'GetAssessments', 'http://prophecyconnect', 'GetAssessmentsResponse', 'ProphecySendassessments.GetAssessmentsResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.GetAssessmentsReturn;
        }
        
        
        global String GetCaregiverAuthURL(String SecurityCode, Double CaregiverID) {
            ProphecySendassessments.GetCaregiverAuthURL_element request_x = new ProphecySendassessments.GetCaregiverAuthURL_element();
            request_x.SecurityCode = SecurityCode;
            request_x.CaregiverID = CaregiverID;
            ProphecySendassessments.GetCaregiverAuthURLResponse_element response_x;
            Map< String, ProphecySendassessments.GetCaregiverAuthURLResponse_element > response_map_x = new Map< String, ProphecySendassessments.GetCaregiverAuthURLResponse_element >();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
            this,
            request_x,
            response_map_x,
            new String[]{endpoint_x,
                '',
                'http://prophecyconnect',
                'GetCaregiverAuthURL',
                'http://prophecyconnect',
                'GetCaregiverAuthURLResponse',
                'ProphecySendassessments.GetCaregiverAuthURLResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.GetCaregiverAuthURLReturn;
        }
    }
    /**
    * @Sandeep Rayavarapu 
    * @05-02-2013
    * @This class holds the elements of SearchCaregivers from Prophecy
    */
    global class GetAssessmentTypes_element {
        public String SecurityCode;
        private String[] SecurityCode_type_info = new String[]{ 'SecurityCode', 'http://www.w3.org/2001/XMLSchema', 'string', '1', '1', 'false' };
        private String[] apex_schema_type_info = new String[]{ 'http://prophecyconnect', 'true', 'false' };
        private String[] field_order_type_info = new String[]{ 'SecurityCode' };
    }
    /**
    * @Sandeep Rayavarapu Rayavarapu
    * @05-02-2013
    * @This clas holds the elements of SearchCaregivers from Prophecy
    */
    public class GetAssessmentTypesResponse_element {
        public String GetAssessmentTypesReturn;
        private String[] GetAssessmentTypesReturn_type_info = new String[]{ 'GetAssessmentTypesReturn', 'http://www.w3.org/2001/XMLSchema', 'string', '1', '1', 'false' };
        private String[] apex_schema_type_info = new String[]{ 'http://prophecyconnect', 'true', 'false' };
        private String[] field_order_type_info = new String[]{ 'GetAssessmentTypesReturn' };
    }
    /**
    * @Sandeep Rayavarapu 
    * @05-02-2013
    * @This class holds the elements of SendAssessments from Prophecy
    */
    global class SendAssessments_element {
        public String SecurityCode;
        public ProphecySendassessments.Caregiver[] Caregivers;
        public ProphecySendassessments.Assessment[] Assessments;
        public Double DepartmentID;
        public Double UserID;
        public Double BillToDepartmentID;
        public Double SendResultsToDepartmentID;
        private String[] SecurityCode_type_info = new String[]{ 'SecurityCode', 'http://www.w3.org/2001/XMLSchema', 'string', '1', '1', 'false' };
        private String[] Caregivers_type_info = new String[]{ 'Caregivers', 'http://prophecyconnect', 'Caregiver', '1', '-1', 'false' };
        private String[] Assessments_type_info = new String[]{ 'Assessments', 'http://prophecyconnect', 'Assessment', '1', '-1', 'false' };
        private String[] DepartmentID_type_info = new String[]{ 'DepartmentID', 'http://www.w3.org/2001/XMLSchema', 'double', '1', '1', 'false' };
        private String[] UserID_type_info = new String[]{ 'UserID', 'http://www.w3.org/2001/XMLSchema', 'double', '1', '1', 'false' };
        private String[] BillToDepartmentID_type_info = new String[]{ 'BillToDepartmentID', 'http://www.w3.org/2001/XMLSchema', 'double', '1', '1', 'false' };
        private String[] SendResultsToDepartmentID_type_info = new String[]{ 'SendResultsToDepartmentID', 'http://www.w3.org/2001/XMLSchema', 'double', '1', '1', 'false' };
        private String[] apex_schema_type_info = new String[]{ 'http://prophecyconnect', 'true', 'false' };
        private String[] field_order_type_info = new String[]{ 'SecurityCode', 'Caregivers', 'Assessments', 'DepartmentID', 'UserID', 'BillToDepartmentID', 'SendResultsToDepartmentID' };
    }
    /**
    * @Sandeep Rayavarapu 
    * @05-02-2013
    * @This class holds the response element of SendAssessments from Prophecy
    */
    global class SendAssessmentsResponse_element {
        public String SendAssessmentsReturn;
        private String[] SendAssessmentsReturn_type_info = new String[]{ 'SendAssessmentsReturn', 'http://www.w3.org/2001/XMLSchema', 'string', '1', '1', 'false' };
        private String[] apex_schema_type_info = new String[]{ 'http://prophecyconnect', 'true', 'false' };
        private String[] field_order_type_info = new String[]{ 'SendAssessmentsReturn' };
    }
    /**
    * @Sandeep Rayavarapu 
    * @05-02-2013
    * @This class holds the elements of GetCaregivers from Prophecy
    */
    global class GetCaregivers_element {
        public String SecurityCode;
        public Double CaregiverID;
        public Double DepartmentID;
        private String[] SecurityCode_type_info = new String[]{ 'SecurityCode', 'http://www.w3.org/2001/XMLSchema', 'string', '1', '1', 'false' };
        private String[] CaregiverID_type_info = new String[]{ 'CaregiverID', 'http://www.w3.org/2001/XMLSchema', 'double', '1', '1', 'false' };
        private String[] DepartmentID_type_info = new String[]{ 'DepartmentID', 'http://www.w3.org/2001/XMLSchema', 'double', '1', '1', 'false' };
        private String[] apex_schema_type_info = new String[]{ 'http://prophecyconnect', 'true', 'false' };
        private String[] field_order_type_info = new String[]{ 'SecurityCode', 'CaregiverID', 'DepartmentID' };
    }
    /**
    * @Sandeep Rayavarapu 
    * @05-02-2013
    * @This class holds the response element of GetCaregivers from Prophecy
    */
    global class GetCaregiversResponse_element {
        public String GetCaregiversReturn;
        private String[] GetCaregiversReturn_type_info = new String[]{ 'GetCaregiversReturn', 'http://www.w3.org/2001/XMLSchema', 'string', '1', '1', 'false' };
        private String[] apex_schema_type_info = new String[]{ 'http://prophecyconnect', 'true', 'false' };
        private String[] field_order_type_info = new String[]{ 'GetCaregiversReturn' };
    }
    /**
    * @Sandeep Rayavarapu 
    * @05-02-2013
    * @This class holds the response element of GetAssessments from Prophecy
    */
    global class GetAssessmentsResponse_element {
        public String GetAssessmentsReturn;
        private String[] GetAssessmentsReturn_type_info = new String[]{ 'GetAssessmentsReturn', 'http://www.w3.org/2001/XMLSchema', 'string', '1', '1', 'false' };
        private String[] apex_schema_type_info = new String[]{ 'http://prophecyconnect', 'true', 'false' };
        private String[] field_order_type_info = new String[]{ 'GetAssessmentsReturn' };
    }
    /**
    * @Sandeep Rayavarapu 
    * @05-02-2013
    * @This class holds the elements of GetAssessments from Prophecy
     */
    global class GetAssessments_element {
        public String SecurityCode;
        public Double AssessmentID;
        public String AssessmentType;
        private String[] SecurityCode_type_info = new String[]{ 'SecurityCode', 'http://www.w3.org/2001/XMLSchema', 'string', '1', '1', 'false' };
        private String[] AssessmentID_type_info = new String[]{ 'AssessmentID', 'http://www.w3.org/2001/XMLSchema', 'double', '1', '1', 'false' };
        private String[] AssessmentType_type_info = new String[]{ 'AssessmentType', 'http://www.w3.org/2001/XMLSchema', 'string', '1', '1', 'false' };
        private String[] apex_schema_type_info = new String[]{ 'http://prophecyconnect', 'true', 'false' };
        private String[] field_order_type_info = new String[]{ 'SecurityCode', 'AssessmentID', 'AssessmentType' };
    }
    /**
    * @Author        : Hari S
    * @Date          : 07/10/2016
    * @Description   : Holds the request data to send to prophecy to generate the bypass URL.
    **/
    public class GetCaregiverAuthURL_element {
        public String SecurityCode;
        public Double CaregiverID;
        private String[] SecurityCode_type_info = new String[]{ 'SecurityCode', 'http://prophecyconnect' ,null, '1', '1', 'false' };
        private String[] CaregiverID_type_info = new String[]{ 'CaregiverID', 'http://prophecyconnect', null, '1', '1', 'false' };
        private String[] apex_schema_type_info = new String[]{ 'http://prophecyconnect', 'true', 'false' };
        private String[] field_order_type_info = new String[]{ 'SecurityCode', 'CaregiverID'};
    }
    /**
    * @Author        : Hari S
    * @Date          : 07/10/2016
    * @Description   : Holds the response data of bypass URL.
    **/
    public class GetCaregiverAuthURLResponse_element {
        public String GetCaregiverAuthURLReturn;
        private String[] GetCaregiverAuthURLReturn_type_info = new String[]{ 'GetCaregiverAuthURLReturn', 'http://prophecyconnect', null, '1', '1', 'false' };
        private String[] apex_schema_type_info = new String[]{ 'http://prophecyconnect', 'true', 'false'};
        private String[] field_order_type_info = new String[]{ 'GetCaregiverAuthURLReturn' };
    }
    /**
    * @Sandeep Rayavarapu 
    * @05-02-2013
    * @This Wrapper class store the dynamic values of the Assesments list from Prophecy
    */
    global class DynamicAssessmentObject {
        public String name { get; set; }
        public String id { get; set; }
        public String type { get; set; }
        public String CostPoints { get; set; }
        public String numberOfQuestions { get; set; }
        public String timeLimit { get; set; }
        public Boolean selected { get; set; }
        /**
        * @Sandeep Rayavarapu 
        * @05-02-2013
        * @This is the contructor method. 
        When we create a new DynamicAssessment Object we pass a strings that is set to the name,
        id,type,costpoints,NumberOfQuestions,TimeLimit,selected property. 
        We also set the selected value to false
        */
        global DynamicAssessmentObject(String assessmentName, String assessmentId, String assessmentType, String costPoints, String noOfQuestions, String timeLimit) {
            this.name = assessmentName;
            this.id = assessmentId;
            this.type = assessmentType;
            this.costPoints = costPoints;
            this.numberOfQuestions = noOfQuestions;
            this.timeLimit = timeLimit;
            selected = false;
        }
    } 
    /**
    * @Author        : Hari S
    * @Date          : 07/10/2016
    * @Description   : Generates the bypass login URL for candidate .
    **/
    public String getByPassURL() {
        System.debug('bbbbbbbbbb');
        ProphecySendAssessments.prophecyconnectCDT_cfc prophecyInstance = new ProphecySendAssessments.prophecyconnectCDT_cfc();
        String response = prophecyInstance.GetCaregiverAuthURL(v_ProphecyCustomSettings.TRHC__Security_Code__c, 
                                                                    double.valueof(candidateCareGiverId));
        System.debug('responseresponse'+prophecyInstance.GetCaregiverAuthURL(v_ProphecyCustomSettings.TRHC__Security_Code__c, 
                                                                    double.valueof(candidateCareGiverId)));
                                                                    
        String resultantUrl = response.substringBetween('<caregiverauthurl>', '</caregiverauthurl>');
        String resultantURLWithHttps = resultantUrl; //.replaceFirst('http', 'https');
        System.debug('resultantURLWithHttps'+resultantURLWithHttps);
        return resultantURLWithHttps;
    }
    /**
    * @Author        : Hari S
    * @Date          : 07/10/2016
    * @Description   : Updates the caregiver id and creates the assessment record and skills record.
    **/
    public void updateRecords() {
        update v_ContactSobj;
        insert skillsCheckList;
    }
}