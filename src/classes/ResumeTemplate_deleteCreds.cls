/*
 * @author chaitanya bollamreddi
 * @Description : using to delate the credentilas using for the coverpage functionality on condition basis
 * @components : ResumeTemplate_deleteCreds_TC(test class)
 */ 
public class ResumeTemplate_deleteCreds implements database.Batchable<Sobject>{
    
    private string credWhereCondition { get;set; }
    private String credQuery { get;set; }
    public List<TRHC__Credentials__c> start(Database.BatchableContext ctx){
        credWhereCondition = '';
        TRHC__config_Settings__c config = TRHC__config_Settings__c.getInstance('Default');
        if(config != null)
        credWhereCondition = config.deleteCreds_where__c != null?config.deleteCreds_where__c:'';
        
        	credQuery =	'SELECT Candidate_text__c, createdDate, lastmodifieddate'+
            			' FROM TRHC__Credentials__c'+ 
            			' Where candidate_text__c != null And TRHC__Contact__c = null And candidate_text__c !='+'\''+'\'';
        	if(credWhereCondition.trim() != '')
               credQuery  += ' AND '+credWhereCondition;
        return (List<TRHC__Credentials__c>)Database.query(credQuery);
    }
    public void execute(Database.BatchableContext ctx, List<TRHC__Credentials__c> lst)
    {
        if(lst!= null && lst.size()>0)
		delete lst;
        
    }
    public void finish(Database.BatchableContext ctx){
        
    }
}